# DuskGull

DuskGull is an old abandoned standalone Pokemon World Server I wrote in 2017 to power the backend of my PokeStory project, with only a few patches applied to get it
running again. Most of it wasn't properly version controlled, and this is a combination of multiple versions I had running on PokeStory.

It simulates battles and a bunch of other aspects of the Pokemon games, such as catching wild pokemon, saving/loading, (optionally) use items in battle, breeding, and so on. 

# Currently Supports:
- Simple HTTP service to invoke from any language!
- Pokedex queries (Pokemon, Items, Moves)
- Leveling up, evolution, breeding and using (some) items
- Saving and Loading (using MongoDB)

# Battle Support: 
- Simple, but working, battle AI
- Almost the entire Pokemon Showdown protocol wrapped in aptly labeled JSON.
- In-Battle item usage
- Catching by throwing Pokeballs at 'wild' Pokemon (formula is broken though)
- The same configurable options as 'vanilla' showdown (mods, format, generation, validation etc.)
- Persistent battle results and starts (if chosen) - You can start with half your team fainted.
- Singles and Doubles battles, Triples are untested. 
- Co-Op for pretty much any kind of weird combination, a team can consist of pretty much anything. 

# Code/API examples

## Pokedex queries: 
```javascript
 //Pokemon, identified by number or name
 dex.load("pokemon","mewtwo");
 dex.load("pokemon",151);
 dex.load("pokemon","ho-oh")
 dex.load("pokemon","mR. miME")
 
 //Items
 dex.load("item","masterball")
 
 //Moves
 dex.load("move","helpinghand");
```


## Pokemon/Trainer object creation: 
```javascript
 
 //Default object, randomizes almost everything. 
 Pokemon.create();
 
 //Add some creation parameters
 Pokemon.create({species:"pikachu",level:25})
 
 //Add randomized creation parameters
 Pokemon.create({species:GenList("pikachu","voltorb","pichu"),level:GenNumber(10,15)})


 //Trainer example
 Trainer.create({
  party:[
    {species:"pikachu",level:GenNumber(25,35)},
    {species:GenList("pikachu","raichu","pichu")
  ]
 });
```

## Battle creation and interaction
```javascript
 Battle.create(Pokemon.create(),Pokemon.create()) //Battle between two wild Pokemon.
 Battle.create(Trainer.create(),Pokemon.create()) //Battle between new trainer and wild Pokemon.
 Battle.create(Trainer.create(),[Pokemon.create(),Pokemon.create()]) //Battle between trainer and two wild Pokemon.
 Battle.create([Trainer.create(),Trainer.create(),Pokemon.create()],Pokemon.create()) //Battle between Two trainers with a wild pokemon and another wild Pokemon.
 
 //Battle interaction (assuming 'thundershock' is a valid move at that point)
 var pokemon = Pokemon.create()
 var battle = Battle.create(pokemon,Pokemon.create())
 Battle.choose(battle.id,pokemon._id,"move","thundershock")
 Battle.choose("dispose") //destroys the battle object
```