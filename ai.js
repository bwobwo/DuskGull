/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ai.js - An extremely barebones system for battle ai. 
 */

const Util = require('./util');

/**
 * Checks entire team for pokemon that can be switched into battle.
 */
function addables(pokemon,request){
	return Util.filter(pokemon,(
		x=>x.sideid==request.sideid&&x.hp>0&&!x.chosen&&!x.active
	));
}

function handleRequest(pokemon,request,callback){
	let poke = pokemon[request.pokemon];
	let valid = addables(pokemon,request);
	
	//1. Switch handling
	if(request.forceSwitch){
		if(Util.count(valid)==0){
			if(request.passable>0){
				callback(request.rqid,"pass");
				return;
			} else {
				throw InternalError("AIError","AI got non-passable forceSwitch request, but cannot find any valid switches");
			}
		}	
		let ca = Util.randomElem(valid);
		let ch = ca._id;
		callback(request.rqid,"switch",ch)
		return;	
	}

	//2. Normal move handling
	for(let i in poke.activeMoves){
		let move = poke.activeMoves[i];
		if((move.pp==undefined||move.pp>0)&&!move.disabled){
			if(move.target==null){
				throw InternalError("AIError","AI received move with NULL target (they shouldn't be null)");
			}
			
			let poketargets = poke.targets[move.target];
			
			if(poketargets.hasOwnProperty("auto")){
				callback(request.rqid,"move",move.id);
				return;
			} else {
				if(poketargets.single[0]==null){
					continue;
				}
				callback(request.rqid,"move",move.id,poketargets.single[0]);
				return;
			}
		}
	}

	console.log("Error incoming: ",poke.activeMoves)
	throw InternalError("AIError","No valid acts in request for "+poke.name);
}
exports.handleRequest = handleRequest