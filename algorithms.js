//I honestly do not know what to name this file
//I just want to have these functions in one place to easily test them. 

const Gen = require('./generation');

//@todo this is gen3, not gen7. 
function run(runningSpeed,opponentSpeed,runCount){
	let F = ((runningSpeed*128/opponentSpeed)+(30*runCount))%256
	let r1 = Gen.Random.Int(0,255);
	return r1<F;
}

//@todo: currently gen3, change to gen7.
function obedience(level,obedience){
	if(obedience>=level) return;
	let A = (level+obedience)*Math.random();
    if(A<obedience) return;

	let B = (level+obedience)*Math.random();
	

    if(B<obedience){
		return "move";
	}

	let R3 = Gen.Random.Int(0,255);

	if(R3<obedience-level){
		return "nap";
	}

	if(R3<(obedience-level)*2){
		return "confusion";
	}

	return "pass";
}


//@todo: currently gen3, change to gen7 
function catching(battle,user,item,target){
	if(!target.wild) return -1;
    if(item._id=='masterball'){
        return 4;
    }

    let maxhpmod = 3*target.maxhp
    let hpmod = 2*target.hp

    let catchmod = item.catchrate(battle,user,target);
    
    let statusbonus = 1;
    if(target.status == "PSN" || target.status=="PAR" || target.status == "BURN"){
        statusbonus = 1.5;
    }
    if(target.status == "SLP" || target.status=="FRZ"){
        statusbonus=2;
    }

    let a = (((maxhpmod-hpmod)*catchmod)/maxhpmod)*statusbonus;
    
    if(a>=255) return 4;

    let b = Math.floor(1048560 / Math.floor(Math.sqrt(Math.floor(Math.sqrt(Math.floor(16711680/a))))))

    let r1 = Gen.Random.Int(0,65535);
    let r2 = Gen.Random.Int(0,65535);
    let r3 = Gen.Random.Int(0,65535);
    let r4 = Gen.Random.Int(0,65535);

    if(r1>=b) return 0;
    if(r2>=b) return 1;
    if(r3>=b) return 2;
    if(r4>=b) return 3;
    return 4;
}

exports.run = run;
exports.obedience = obedience;
exports.catching = catching;