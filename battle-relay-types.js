/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * battle-relay-types.js - Deprecated but is apparently used in battle-updates.
 * Removing this asap
 */

class BattleSwitch{
	constructor(guid,index,ownerid){
		this.guid=guid;
		this.index=index;
		this.ownerid=ownerid;
	}
}

/**
 * Represents the move of a pokemon.
 * @constructor
 * @param {string} name - for quick reference
 * @param {number} id - numeric id of the move (can be used for lookups)
 * @param {number} pp
 * @param {number} maxpp
 * @param {boolean} disabled - if the move has been disabled  
*/
class BattleMove{
	constructor(_id,num,pp,maxpp,disabled,target,index){
		this._id=_id;
		this.num=num;
		this.pp=pp;
		this.maxpp=maxpp;
		this.disabled=disabled;
		this.target=target;
		this.index=index;
	}
}

/**
 * Represents a pokemon in battle, either on the field or in the party.
 * @constructor
 * @param {number} guid - The pokemons globally unique identifier (i.e. it's server unique)
 * @param {string} name - The pokemons nickname
 * @param {number} curhp
 * @param {number} maxhp
 * @param {string} status - The pokemons current status (SLP, PAR, PSN) 
 * @param {string[]} ailments - Other ailments (confusion, pumped, evasiveness)
 * @param {string} forme - The forme of this pokemon.
 * @param {Stat} - stats - Effective stats in form ['atk','def','hp','spa','spd','spe']
 * @param {Move[]} moves - All the pokemons moves (including disabled ones - be careful)
 * @param {number[]} switchids - Ids for all the pokemon this pokemon can switch into (unless someone else on the team switches first)
 * @param {boolean} active - Whether the pokemon is active in battle.
 * @param {number} index - The pokemons party or field index, depending on whether it's active or not. 
 */
class BattleMon{
	constructor(guid,sideid,forme,name,curhp,maxhp,status,ailments,ownerid,level,experience,moves,switches,active,forceSwitch,index){
		this.guid=guid;
		this.sideid=sideid;
		this.forme=forme;
		this.name=name;
		this.curhp=curhp;
		this.maxhp=maxhp;
		this.status=status;
		this.ailments=ailments;
		this.ownerid=ownerid;
		this.level=level;
		this.experience=experience;
		this.moves=moves;
		this.switches =switches;
		this.active = active;
		this.forceSwitch=forceSwitch;
		this.index=index;
	}
}



/**
 * @constructor 
 * @param {number} updateIndex - The (state-local) index of this update.  
 * @param {string} type - Describes the type of this update (see Updates.js to see how to use additional params!)
 */
class BattleUpdate{
	constructor(updateid,type){
		this.updateid=updateid;
		this.type=type;
	}
}

/**
 * Represents an entire state of battle. These are created as results of full or partial rounds. 
 * Note that state id does NOT change on request changes (they don't impact the battle at all)
 * @constructor 
 * @param {number} [stateid] - Lists this states index in all states taken place in the battle.  
 * @param {Side[]} [sides]
 * @param {Update[]} [updates] - Contains all updates taken place since the last state. 
 */
class BattleState{
	constructor(stateid,battleid,pokemon,requests,updates){
		this.stateid=stateid;
		this.battleid=battleid;
	 	this.pokemon=pokemon;
	 	this.requests=requests;
		this.updates=updates;
	}
}

/*
 * Represents a single choice made by a single pokemon or free slot. 
 * @constructor
 * @param {string} type - 'move' or 'switch'.'
 * @param {number} concern - If a move choice: pokemon guid, if a switch choice: position index. 
 * @param {string} status - null if not selected or invalid. GUID if type is 'add', otherwise choice string. 
 */
class BattleRequest{
	constructor(type,concern,player){
		this.type=type;
		this.concern=concern;
		this.status=null;
		this.internalStatus=null;
		this.player=player;
	}
}

exports.BattleMove = BattleMove;
exports.BattleMon = BattleMon;
exports.BattleUpdate = BattleUpdate;
exports.BattleState = BattleState;
exports.BattleSwitch = BattleSwitch;
exports.BattleRequest = BattleRequest;