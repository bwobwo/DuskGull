/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~battle-updates.js~
 * This is the main file for protocol conversion between Showdown and DuskGull.
 * We don't modify the internal showdown format because we couldn't possibly maintain that through updates. 
 * 
 * Showdown sends two types of messages of interest to its clients:  requests and updates.
 * 
 * Requests consists of some kind of newline-separated list
 * which itself contains a JSON object at the bottom. 
 *
 * Updates are CSV lists separated with pipes ('|') that are sent after each battle turn.
 * The update type is always stored at the second column,
 * After that the schema varies completely on the type of update. 
 * 
 * This file converts both these to completely valid and properly named JSON objects to be sent externally.
 * 
 * Update parsing is handled by parse(...)
 * Request parsing is handled by parseRequest(...)
 * 
 */

//deprecated....
const BattleRelays = require("./battle-relay-types");
const Log = require('./logga');
const cJSON = require('circular-json');
const Util = require('./util');
const DuskGull = require('./duskgull');


//Parses a pair of pokemon/trainer
function pair(stringin){
	let values = stringin.split("a: ");
	return {'player': values[0], 'pokemon': values[1]};
}

//Finds the first hp row that's not relative.
function hp(rows){
	let cand = null;
	for(let i=0;i<rows.length;i++){
		let row = rows[i];
		let hp = rows[i].split("|")[4].split("/");
		cand = hp;
		if(hp[1]!="100"){
			return hp;
		}
	}
	return cand;
}

function toId(text) {
	// this is a duplicate of Dex.getId, for performance reasons
	if (text && text.id) {
		text = text.id;
	} else if (text && text.userid) {
		text = text.userid;
	}
	if (typeof text !== 'string' && typeof text !== 'number') return '';
	return ('' + text).toLowerCase().replace(/[^a-z0-9]+/g, '');
}

function handleStart(update,values,pokemon){
	update.event = "end";
	let pokename = pokemon[values[2]].name;
	update.id = values[2]
	update.name = pokename;

	switch(values[3]){
		case 'Attract':
			update.type = 'attract';
			break;
		case 'Substitute':
			update.type = 'substitute';
			break;
		case 'Foresight':
			update.type = "foresight";
			break;
		case 'Focus Energy':
			update.type = 'focusenergy';
			break;
		case 'Encore':
			update.type = 'encore';
			break;
		default:
			update.data = values;
			break;

	}
	return update
}

function handleFieldStart(update,values,pokemon){
	update.event = "fieldstart";

	switch(values[2]){
		case 'move: Mud Sport':
			update.type="mudsport";
			break;
		case 'move: Water Sport':
			update.type="watersport";
			break;

		default:
			Log.warn("Received unhandled 'fieldStart': "+JSON.stringify(values));
			update["data"] = values;
			break;
		break;
	}
}

//Happens when something ends
function handleEnd(update,values,pokemon){
	update.event = "end";
	update.id = values[2];
	update.name = pokemon[values[2]].name;
	switch(values[3]){
		case 'Encore':
			update.type="encore";
			break;
		case 'Attract':
			update.type ="attract"
			break;
		case 'move: Yawn':
			update.type="yawn";
			break;
		case 'Substitute':
			update.type = "substitute";
			break;
		case 'move: Future Sight':
			update.type = "futuresight";
			break;
		default:
			Log.warn("Received unhandled 'end': "+JSON.stringify(values));
			update["event"]="start";
			update["data"] = values;
		break;
	}
	return update;
}

function handleSideEnd(update,values,pokemon){
	let side = values[2].split(":")[0];
	update.side = side;
	switch(values[3]){
		case 'move: Light Screen':
			update.type="lightscreen";
			break;
		default:
			Log.warn("Received unhandled 'sideEnd': "+JSON.stringify(values));
			update["data"] = values;
			break;
	}
	return update;
}

function handleSingleTurn(update,values,pokemon){
	update.event = "singleturn";
	update.id = values[2]
	let pokename = pokemon[values[2]].name;
	update.name = pokename;

	switch(values[3]){
		case 'move: Endure':
			update.type = 'endure';
			break;
		case 'Snatch':
			update.type= 'snatch';
			break;
		case 'move: Magic Coat':
			update.type = 'magiccoat';
			update.message = pokename+ " shrouded itself in MAGIC COAT";
			break;
		default:
			Log.warn("Received unhandled 'singleturn'"+JSON.stringify(values));
			update.data = values;
	}
	return update;
}

function handleEndAbility(update,values,pokemon){
	update.event = "endability";
	update.id = values[2];
	update.name = pokemon[values[2]].name;

	switch(values[4]){
		case '[from] move: Worry Seed':
			update.type = 'worryseed';
			break;
		default:
			Log.warn("Received unhandled 'endAbility: "+JSON.stringify(values));
			break;
	}

	return update;
}

function handleCureStatus(update,values,pokemon){
	update.event = "curestatus"
	update.id = values[2];
	update.name = pokemon[values[2]].name;
	switch(values[3]){
		case 'slp':
			update.type="slp";
			break;
		default:
			break;
	}
}

//Happens when a pokemon fails to use a move (often due to status)
function handleCant(update,values,pokemon){
	update.event = "cant"
	update.id = values[2];
	update.name = pokemon[values[2]].name;

	switch(values[3]){
		case 'slp':
			update.type = "slp";
			break;
		case 'disable':
			update.type ="disable";
			update.move = values[4]
			break;
		case 'par':
			update.type = "par"
			break;
		case 'frz':
			update.type = "frz";
			break;
		default:
			update.type = values[3];
			Log.warn("Received unhandled 'cant': "+JSON.stringify(values));
			update.data= values;
			break;
	}
	return update

}

function handleActivate(update,values,pokemon){
	update.event = "activate"
	update.id = values[2];
	update.name = pokemon[values[2]].name;

	switch(values[3]){
		case 'move: Wide Guard':
			update.type="wideguard";
			break;
		case 'move: Protect':
			update.type = 'protect';
			break;
		case 'move: Power Split':
			update.type='powersplit'
			break;
		case 'ability: Synchronize':
			update.type = 'synchronize';
			break;
		case 'move: Protect':
			update.type ="protect";
			break;
		case 'move: Struggle':
			update.type = "struggle";
			break;
		case 'move: Destiny Bond':
			update.type = "destinybond";
			break;
		case 'move: Endure':
			update.type = "endure";
			break;
		case 'move: Spite':
			update.type = "spite";
			update.move = values[4];
			update.amount = values[5];
			break;
		case 'confusion':
			update.type = "confusion"
			break;
		case 'move: Guard Split':
			update.type = "guard split";
			break;
		case 'move: Magnitude':
			update.type = "magnitude";
			update.amount = values[4];
			break;
		default:
			Log.warn("Received unhandled 'activate: '"+JSON.stringify(values));
			update.data=values;
	}

	return update;
}
function handleFieldActivate(update,values,pokemon){
	update.event="fieldactivate";

	switch(update[2]){
		case 'move: Perish Song':
			update.type ="perishsong"
		break;
		default:
			update.data=values;
	}
}



function handlePrepare(update,values,pokemon){
	update.event = "prepare"
	update.id = values[2]
	update.name = pokemon[values[2]].name;


	switch(values[3]){
		case 'Fly':
			update.type = "fly"
			break;
		case 'Dig':
			update.type = "dig";
			break;
		case 'Dive':
			update.type = "dive";
			break;
		case 'Solar Beam':
			update.type = "solarbeam";
			break;
		case 'Razor Wind':
			update.type = "razorwind";
			break;
		case 'Bounce':
			update.type = "bounce";
		default:
			Log.warn("Received unhandled 'prepare': "+JSON.stringify(values));
			update["event"] = "prepare";
			update["data"]=values;

	}
	return update;
}
function handleWeather(update,values,pokemon){
	update.event = "weather";
	switch(values[2]){
		default:
			Log.warn("Received unhandled weather: "+JSON.stringify(values));
			update.data=values;
			break;
	}
	return update;
}

//Parses pokemon information
function mondata(col){
	let vars = col.split(", ");
	let data = {};
	data["species"] = vars[0];
	data["speciesid"] = pokedex.BattlePokedex[toId(vars[0])]["num"];
	data["level"] = vars[1];
	if(vars.length>2){
		data["gender"] = vars[2];
	} else {
		data["gender"] = "none";
	}
	return data;
}

ignoredUpdates = [
	'upkeep',
	'start',
	'player',
	'gametype',
	'gen',
	'tier',
	'split',
	'seed',
	'rule',
	'',
	' ',
	'-nothing',
	'-notarget',
	'choice',
	'-singleturn'
];

function getPlayer(_id,pokemon,players){
	return players[pokemon[_id].ownerid].name;
}
function getPokemon(_id,pokemon){
	return pokemon[_id].name;
}
function parse(updates,manager){
	let i = 0;
	let cleanupdates = []
	let pokemon = manager.pokemon;
	let players = manager.players;
	
	while(i<updates.length){
		let values = updates[i].split("|");
		let id = values[1];

		if(ignoredUpdates.indexOf(id)!=-1){
			i++;
			continue;
		}

		let update = {'event':'unknown'};

		switch(id){
			case 'useitemtrainer':
				update.event = 'traineritem';
				update.item = values[2];
				update.itemname = values[5];
				update.itemcategory = values[6];
				update.user = values[3];
				update.username = pokemon[values[3]].name;
				
				//Not all items have targets?
				try{
					update.target = values[4];
					update.targetname = pokemon[values[4]].name;					
				} catch(error){

				}
				update.trainer = pokemon[values[3]].ownerid;
				update.trainername = players[update.trainer].name;

				break;
			case 'clearchoices':
				update.event = 'clearchoices';
				break;
			case 'learnmove':
				Log.temp("Learnmove is not implemented yet");
				//manager.addSpecialRequest(cleanupdates.length-1,values[2],"learnmove")
				i++;
				continue;
				break;
			case 'return':
				update.event = "return";
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				break;
			case 'switch':
				if(values[2].startsWith("__ghost")){
					i++;
					continue;
				}
				update.event = "switch"
				update.id = values[2]
				update.name=pokemon[values[2]].name;
				i+=3;
				break;
			case '-hint':
				update.event="hint";
				update.message = values[2];
				break;
			case 'drag':
				//No message
				update.event = "drag"
				update.id = values[2]
				update.name=pokemon[values[2]].name;
				i+=3;
				break;
			case 'turn':
				update.event = "turn";
				update.turn = values[2];
				break;
			case 'move':
				update.event="move";
				update.attacker = values[2];
				update.attackername = pokemon[values[2]].name;
			 	update.move=values[3];
				update.target=values[4];
				update.targetname = pokemon[values[4]].name;
				update.missed=false;
				
			 	if(update.move=="Struggle"){
			 		update.movetype = "normal";
			 		update.category = "physical";
			 		update.damaging = true;
			 	} else {
					let movedex = manager.dex.moves[toId(update.move)];
					if(movedex==null){
						movedex = DuskGull.Dex.load("move",toId(update.move));
					}

					if(movedex!=null){
						manager.dex.moves[toId(update.move)] = movedex;
						update.movetype = movedex.type;
						update.category = movedex.category; //physical/special
						update.damaging = movedex.basePower!=0;	
					} else {
						let ks = []
						for(let i in manager.dex.moves){
							ks.push(i);
						}
						throw new Error("Failed loading movedex for move "+update.move+", our movedex: "+ks);
					}
				}
							 		
				break;
			case '-miss':
				//'miss' always directly follows a move, so we just set the last one to miss if this appears.
				//This seem to get called alot in the mods, so I don't want to touch it. 
				cleanupdates[cleanupdates.length-1].missed=true;
				update.event = "miss";
				break;
			case 'win':
				manager.finished = true;
				update.event="win";
				update.side=values[2];
				update.winners = []
				for(player in players){
					player = players[player];
					if(player.side==update.side){
						update.winners.push(player._id);
					}
				}
				break;
			case '-damage':
				update.event="damage";
				update.id=values[2];
				update.name=pokemon[values[2]].name;
				update.newhp = parseInt(values[3]);
				update.oldhp = parseInt(values[4]);
				update.maxhp = parseInt(values[5]);

				update.crit=false;
				update.effectiveness="normal";
				let j = i-1;

				//Effectiveness data is sent in separate updates,
				//So we need to search for them. 
				while(j>0){
					try{
						let oldupdate = updates[j].split("|");
						let type = oldupdate[1].replace(" ","");
						if(type=="split"){
							j--;
							continue;
						}

						switch(type){
							case '-supereffective':
								update.effectiveness="super";
								break;
							case '-resisted':
								update.effectiveness="resist";
								break;
							case '-crit':
								update.crit=true;
								break;
							default:
								j=-1;
								break;
						}
						j--;						
					} catch(error){
						j--;
					}
				}
				break;
			case '-immune':
				update.event = "immune";
				
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				update.message = "It had no effect on "+getPokemon(update.id,pokemon);
				break;

			//These are pretty large. 
			case '-start':
				update = handleStart(update,values,pokemon);
				break;
			case '-prepare':
				update=handlePrepare(update,values,pokemon);
				break;
			case '-activate':
				update = handleActivate(update,values,pokemon);
				break;
			case 'cant':
				update = handleCant(update,values,pokemon);
				break;
			case '-end':
				update = handleEnd(update,values,pokemon);
				break;
			case '-endability':
				update = handleEndAbility(update,values,pokemon);
				break;
			case '-curestatus':
				update = handleCureStatus(update,values,pokemon);
				break;
			case '-fieldstart':
				update = handleFieldStart(update,values,pokemon);
				break;
			case '-fieldactivate':
				update = handleFieldActivate(update,values,pokemon);
				break;
			case '-copyboost':
				update.event = "copyboost";
				update.from = values[2];
				update.fromname=pokemon[values[2]].name;
				update.to = values[3];
				update.toname=pokemon[values[3]].name;
				break;
			case '-status':
				update.event = "status";
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				update.status = values[3];
				let statusmon = getPokemon(update.id,pokemon);
				break;
			case 'disobey':
				update.event ="disobey";
				update.id = values[2];
				update.type = values[3];
				break;
			case '-singlemove':
				update.event = "singlemove";
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				update.move = values[3];
				break;
			case '-sidestart':
				update.event="sidestart";
				update.id = values[2];
				update.move = values[3];
				break;
			case '-ability':
				update.event = "ability";
				update.id = values[2]
				update.name=pokemon[values[2]].name;
				update,ability = values[3];
				update.effect = values[4];
				break;
			case '-unboost':
				update.event = "unboost",
				update.id = values[2]
				update.name=pokemon[values[2]].name;
				update.stat = values[3];
				update.amount = values[4]
				if(update.amount==1){
					update.message = getPokemon(update.id,pokemon)+"s "+update.stat+" fell!";
				} else {
					update.message = getPokemon(update.id,pokemon)+"s "+update.stat+" harshly fell!";					
				}
				break;
			case '-boost':
				update.event = "boost";
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				update.stat = values[3];
				update.amount = values[4];
				if(update.amount==1){
					update.message = getPokemon(update.id,pokemon)+"s "+update.stat+" rose!";
				} else {
					update.message = getPokemon(update.id,pokemon)+"s "+update.stat+" sharply rose!";					
				}
				break;
			//@Fixme: Probably wrong
			case '-sethp':
				update.event = "sethp";
				update.id1 = values[2];
				update.id2 = values[4];
				update.hp1 = values[3];
				update.hp2 = values[5];
				updates.source = values[6];	
				break;
			case '-enditem':
				update.event = "enditem";
				update.target = values[2]
				update.targetname=pokemon[values[2]].name;
				update.item = values[3];
				update.sourcetype = values[4];
				update.source = values[5]
				break;

			case '-transform':
				update.event = "transform";
				update.source = values[2];
				update.sourcename=pokemon[values[2]].name;
				update.target = values[3];
				update.targetspecies=pokemon[values[3]].species;
				update.targetname=pokemon[values[3]].name;
				update.message = getPokemon(update.source,pokemon)+" transformed into " + pokemon[update.target].species;
				break;
			case '-weather':
				update = handleWeather(update,values,pokemon);
				break;
			case 'faint':
				update.event = "faint";
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				update.message = getPokemon(update.id,pokemon)+" fainted!";
				break;
			case '-fail':
				update.event='fail';
				update.id = values[2];
				update.name=pokemon[values[2]].name;
				break;
			case '-heal':
				update.event="heal";
				update.id=values[2];
				update.name=pokemon[values[2]].name;
				update.newhp = values[3];
				update.oldhp = values[4];
				update.maxhp = values[5]
				update.cause = values[6];
				//i+=3;
				break;
			case 'expgain':
				update.event = "expgain";
				update.amount = values[2];
				update.id = values[3];
				update.name=pokemon[values[3]].name;
				break;
			case 'exppercent':
				update.event = 'exppercent';
				update.id = values[2]
				update.name=pokemon[values[2]].name;
				update.oldpercent = values[3];
				update.percent = values[4];
				break;
			case 'levelup':
				update.event = "levelup";
				update.id = values[3];
				update.name=pokemon[values[3]].name;
				update.oldpercent = values[2]
				update.newlevel= values[4];
				update.stats = values[5];
				break;
			case 'catch':
				update.event = "catch";
				update.source = values[2];
				update.sourcename=pokemon[values[2]].name;
				update.target = values[3];		
				update.targetname=pokemon[values[2]].name;		
				update.rolls = values[4];
				update.blocked = values[5];
				update.success = values[6];
				break;
			case 'run':
				update.event = "run";
				update.id = values[2];
				update.success = values[3];
				if(update.success=='false'){
					update.fail = values[4];
				}
				break;
			case 'tie':
				manager.finished = true;
				update.event = "tie";
				break;
			default:
			 	mode="unhandled"
				update["event"] = "unknown";
				update["raw"] = cJSON.stringify(values);
				break;
		}
		i++
		cleanupdates.push(update);
	}
	return cleanupdates;
}

/**
 * This is used to represent the 'pokemon' objects and requests. 
 * Showdown uses kind of a format like:
 	* side:{mustSwitch:true,active[],pokemon[],trainername}
 	*
 	* It's really weird and inconsistent, so i've settled on using: 
 	*
 	* state:
 	* 	battleid: in case you need to reference the battle from the state. 
 	* 	stateid: the unique id for when this state was generated.
 	* 	pokemon: represents ALL pokemon in battle (both in party and on the field)
 	* 	updates: contains updates from between this and the LAST state.
 	* 	requests: contains requests and the concerned player for easy sorting. 
 	* 
 * @param  {BattleSimulator.BattleSide}
 * @param  {BattleSimulator.Request}
 * @return {BattleRelays.BattleSide}
 */

function pushRequest(relaySide,request){
	request.rqid = request.position+"/"+request.sideid
	relaySide.requests.push(request);
}

function parseRequest(showdownBattle,showdownRequest){
	let relaySide = {requests:[]}
	let active;
	let validSwitches = []
	relaySide.sideid = showdownRequest.side.id;
	let players = []

	/**
	 * Switch Setup
	 */
	for(let i in showdownRequest.side.pokemon){
		let pokemon = showdownRequest.side.pokemon[i];
		if(players.indexOf(pokemon.ownerid)==-1){
			players.push(pokemon.ownerid);
		}
		if((!pokemon.active	
			//all pokemon are internally visible during team preview in showdown, should not be 'active'
			||showdownRequest.hasOwnProperty("teamPreview")) 
			&& pokemon.hp!=0){
				if(validSwitches.indexOf(pokemon._id)==-1){
					validSwitches.push({_id:pokemon._id,ownerid:pokemon.ownerid,chosen:false});
				};
			}
		}
	

	/**
	 * TeamPreview Setup
	 */
	if(showdownRequest.hasOwnProperty("teamPreview")){
		let reqs = Math.min(showdownBattle.activeCount,validSwitches.length);
		for(let i=0;i<reqs;i++){
			let ownerno = players[i%players.length];
			let req = {
				position: i+1,
				sideid : relaySide.sideid,
				pokemon: null,
				forceSwitch: true,
				player: ownerno,
				passable: 0,
				trapped:false
			}
			pushRequest(relaySide,req);
		}
	} else {
		relaySide.internalFormat="normal"
	}

	/**
	 * Pokemon Setup
	 */
	relaySide.pokemon = []
	for(let i=0;i<showdownRequest.side.pokemon.length;i++){
		let poke = showdownRequest.side.pokemon[i];
		poke.partyIndex = i+1;
		/**
		 * Trap/Position check
		 */
		
		let trapped=false;
		if(poke.active && showdownRequest.active!=null){
			for(let j=0;j<showdownRequest.active.length;j++){
				if(showdownRequest.active[j]._id==poke._id){
					if(showdownRequest.active[j].trapped){
						trapped=true;
					}
					poke.position = j+1;
				}
			}
		}

		/**
		 * Normal Active Requests
		 */
		if(poke.active&&
		!showdownRequest.hasOwnProperty("teamPreview")&&
		!showdownRequest.hasOwnProperty("wait")&&
		!showdownRequest.hasOwnProperty("state")&&
		!showdownRequest.hasOwnProperty("forceSwitch")){
			if(poke.hp!=0&&!poke._id.startsWith("__ghost")){
				let req = {
					position: poke.position,
					pokemon: poke._id,
					forceSwitch: false,
					trapped:trapped,
					passable: 0,
					sideid : relaySide.sideid,
					player: poke.ownerid,
				}
				pushRequest(relaySide,req);									
			}
		}		
		

		/**
		 * Party Move Handling (i.e. the pokemons DEFAULT moveset)
		 * (In showdown, this is separate from its ACTIVE moveset)
		 */
		let moves = []
		for(let moveidx in poke.moves){
			let showdownMove = poke.moves[moveidx];
			moves.push({
				_id:showdownMove.id,
				num:showdownMove.num,
				pp:showdownMove.pp,
				maxpp:showdownMove.maxpp,
				disabled:showdownMove.disabled,
				target:showdownMove.target,
				index:parseInt(moveidx)+1
			})
		}

		/**
		 * Active Move Handling
		 */
		if(showdownRequest.active){
			for(let b in showdownRequest.active){
				let ac = showdownRequest.active[b];
				if(ac._id==poke._id){
					poke.activeMoves = ac.moves;

					for(let i in poke.activeMoves){
						//Fixes null targets for struggle. 
						if(poke.activeMoves[i].target==null){
							poke.activeMoves[i].target = "randomNormal";

						}
					}
					poke.activeIndex=parseInt(b)+1;
				}
			}
		}

		//Nobody's active in teampreview		
		if(showdownRequest.teamPreview!=null)
			poke.active=false;

		/**
		 * ForceSwitch Handling
		 */
		if(poke.active && showdownRequest.forceSwitch!=null){
			let fccount=0;
			for(let j in showdownRequest.forceSwitch){
				if(showdownRequest.forceSwitch[j]) fccount++;
			}
			
			for(let j=0;j<showdownRequest.forceSwitch.length;j++){
				if(i==j&&showdownRequest.forceSwitch[j]){
					let passable = Math.max(0,fccount-validSwitches.length);
					
					let raq = {
						pokemon: poke._id,
						position: i+1,
						passable: passable,
						forceSwitch: true,
						trapped: false,
						sideid : relaySide.sideid,
						player: poke.ownerid
					}
					poke.position = i+1;
					pushRequest(relaySide,raq);			
				}
			}
		}

		poke.sideid = relaySide.sideid;

		//why?
		poke.ailments = [];
		
		poke.moves = moves;
		poke.maxhp=poke.maxhp;

		relaySide.pokemon.push(poke);

	}

	relaySide.active = showdownRequest.active;
	return relaySide
}

exports.parseRequest = parseRequest
exports.parse = parse;