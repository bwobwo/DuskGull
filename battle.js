/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * battle.js - Exposes Showdown battles to the DuskGull system through functions create() and get().
 * Deals with most utility surrounding conversion from the internal Showdown protocol to the DuskGull format. 
 * 
 * For actual protocol conversion, see battle-updates.js
 * 
 * Important functions:
 * choose() - To externally query individual pokemon choices.
 * get()    - Accesses a non-finished battle object. 
 * create(team1,team2,settings) - Creates a new BattleRelay and returns it. 
 *
 * Hierarchical structure of the battle system. The BattleRelay class mostly exists to prepare for a multi-process setup (battles are discrete and processor heavy)
 * Exported create()/get() -> BattleRelay instance -> Battle instance -> (Showdown) Battle instance (see showdown/battle-engine.js).
 */
const Settings = require("./settings");
const BattleEngine = require('./showdown/battle-engine');
const SINGLETARGETS = ['any','adjacentAllyOrSelf','adjacentAlly','adjacentFoe','normal']
const Updates = require('./battle-updates')
const Util = require('./util');
const ai = require('./ai');
const BattleRelays = require('./battle-relay-types');
const Gen = require('./generation');
const Dex = require('./dex');
const Item = require('./item');
const Validate = require('./validation-wrapper');
const Algorithms = require('./algorithms');
battles = {};

/**
 * @class 
 */
class Battle{
	constructor(id,p1,p2,settings){
		//contains player/controller mapping. 
		//Should not change during battle even if a player should 'disconnect'
		this.players = {}

		//Special object used when single pokemon are sent into a doubles or triples battle. 
		this.ghosts=0;

		
		//Requests contains the CURRENT requests and responses. 
		//Pokemon contains the CURRENT state of all pokemons in battle (including those not on the battlefield)
		//Both these are cleared after each turn. 
		this.requests = []
		this.pokemon = {}

		this.settings = settings;
		this.dex = {moves:{},pokemon:{}};
		this.involvedMoves=[]

		//this is the queue that's been formatted by battle-updates.js. 
		this.updateQueue = [];
		this.queuePosition = 0;
		this.updateCache = [];

		//this is pretty much only a wait-list because showdown sends its requests per-side. 
		//and we abstract away the entire concept of 'sides' as anything else than a property on pokemon and players.  
		this.sides = {'p1':null,'p2':null};

		this.addGhosts(p1);
		this.addGhosts(p2);

		this.parsePlayersAndDex(p1,"p1");
		this.parsePlayersAndDex(p2,"p2");

		//Set by showdown when we receive a win/tie update. 
		this.finished=false;
		this._id=id;		

		for(let i in p1.pokemon) this.getDexes(p1.pokemon[i]);
		for(let i in p2.pokemon) this.getDexes(p2.pokemon[i]);

		//Actual battle invocation happens here. 
		this.battle = BattleEngine.construct(
			settings,false,this);
		
		this.updateQueue.push({"event":"battlestart",p1:this.sidePlayers(p1),p2:this.sidePlayers(p2)});
		
		this.battle.join(0,""+"p1",1,p1.pokemon);
		this.battle.join(1,""+"p2",1,p2.pokemon);
		
		this.lastUpdate=[]
		this.updateState();
	}

	//loads dexdata at battlestart
	getDexes(pokemon){
		this.dex.pokemon[pokemon.species.id] = pokemon.species;
		for(let i in pokemon.moves){
			this.dex.moves[pokemon.moves[i].movedex.id] = pokemon.moves[i].movedex;
		}
	}

	//This method is only called if we have 'obedience' enabled in our settings. 
	//https://bulbapedia.bulbagarden.net/wiki/Obedience
	calculateObedience(pokemon,request){
		let res = Algorithms.obedience(pokemon.level,pokemon.obedience);
		if(res){
			request.internalStatus = "disobey "+res;
		}
		return request;
	}

	/**
	 * Returns a list of 'faint' and 'alive' statuses for a players Pokemon
	 * This can be used to decide the colors of Pokeballs when a battle starts
	 *
	 * @todo Method name is very cryptic
	 *
	 * @param {object} player - A valid player object (those stored in 'players')
	 * @returns {array} list containing
	 */
	sidePlayers(pl){
		let pls={}
		for(let i in pl.pokemon){
			if(pls[pl.pokemon[i].ownerid]==null) pls[pl.pokemon[i].ownerid]=[]
			if(pl.pokemon[i].hp>0) pls[pl.pokemon[i].ownerid].push("alive");
			else pls[pl.pokemon[i].ownerid].push("faint");
		}
		return pls;
	}

	/**
	 * A fake Pokemon added to a players team
	 * if the player joins a non-ranked double battle 
	 * with only a single Pokemon.
	 *
	 * It will immediately and silently get killed once battle starts,
	 * and will be ignored by all updates. 
	 * 
	 * This is necessary because Showdown won't even internally 
	 * accept single-pokemon teams in doubles. 
	 * 
	 * @return {object} - Pokemon object. 
	 */
	ghost(){
		let ghost = {}
		ghost.level = 1;
		ghost.name = "ghost";
		ghost.num = 0;
		ghost.hp = 1;
		ghost.moves = [];
		ghost.wild = false;
		ghost.ownerid = -1;
		ghost.catchrate = 0;
		ghost.baseExperience = 0;

		ghost.species = "bulbasaur";
		ghost.types = ['grass'];
		ghost.outsider=false;
		ghost._id = '__ghost'+this.ghosts;
		this.ghosts++;
		ghost.experience = 0;
		ghost.gainsExp = false;
		return ghost;
	}

	/**
	 * Checks through a teams Pokemon
	 * Adds ghosts until the side is large enough for the settings active count.
	 * @param {[type]} side [description]
	 */
	addGhosts(side){
		let active =0;
		if(this.settings.gameType=="singles") active=1;
		if(this.settings.gameType=="doubles") active=2;
		if(this.settings.gameType=="triples") active=3;

		while(side.pokemon.length<active){
			side.pokemon.push(this.ghost());
		}
	}

	/**
	 * @todo: Not implemented, but serves as a blueprint for choosing stuff like moves mid-battle. 
	 */
	addSpecialRequest(updatePos,concern,typename,choices){
		this.specialRequests.push({pos:updatePos,concern:concern,typename:typename,choices:choices});	
	}

	/**
	 * Returns a single active pokemon on the opposite side of the calling pokemon.
	 * Returns null if there are multiple or 0 active opponents. 
	 *
	 * Currently only used when checking or using Pokeballs.
	 * 
	 * @param  {object} pokemon 	 The requesting Pokemon.
	 * @return {object|null}         A single opponent if there is only one.
	 */
	getSingleOpponent(pokemon){
		let side;
		if(pokemon.side!=null){
			side =pokemon.side.name
		} else {
			side = pokemon.sideid;
		}
		side = this.oppositeSide(side);
		let activeEnemies = Util.findAll(this.pokemon,{sideid: side,active: true});
		if(activeEnemies.length!=1){
			return null;
		}
		return activeEnemies[0];
	}

	/**
	 * Finds all valid targets for the provided Pokemon and type. 
	 * @param  {object} pokemon    [description]
	 * @param  {string} targetType [description]
	 * @return {array}             [description]
	 */
	getTargets(pokemon,targetType){
		if(Util.isPointer(pokemon)){
			pokemon = this.pokemon[pokemon];
		}
		let rs = []
		switch(targetType){
			case 'none':
				rs.push("%none%")
				break;
			case 'party':
				return Util.findAll(this.pokemon,{sideid:pokemon.sideid});
			case 'active':
				return Util.findAll(this.pokemon,{active:true,hp:{'$gt':0}});
				break;
			case 'activeAlly':
				return Util.findAll(this.pokemon,{active:true,hp:{'$gt':0},sideid:pokemon.sideid});
				break;
			case 'activeFoe':
				let opposite = this.oppositeSide(pokemon.sideid);
				return Util.findAll(this.pokemon,{active:true,hp:{'$gt':0},sideid:opposite});
		}
		return rs;
	}

	/**
	 * Parses players and their controllers during battle creation.
	 * @param  {array} team       [description]
	 * @param  {string} sidename  p1 or p2
	 */
	parsePlayersAndDex(team,sidename){
		for(let i in team.players){
			this.players[team.players[i]._id] = team.players[i];
		}

		for(let i in team.dex.pokedex){
			this.dex.pokedex[i]=team.dex.pokedex[i];
		}

		for(let i in team.dex.movedex){
			this.dex.movedex[i] = team.dex.movedex[i];
		}
	}

	/**
	 * Returns the running battles current state.
	 * This method is never called internally.
	 * Updates in the returned state are all queued updates since the last call to this method.
	 * Requests are always the latest requests, even if the callee missed a round
	 * (in the case of, for example, internal ai taking up an entire turn)
	 * @return {BattleState}     
	 */
	state(startpos,endpos){
		let state = new BattleRelays.BattleState();
		state.battleid = this._id;
		state.updates=this.updateQueue.slice(this.queuePosition,this.updateQueue.length);
		this.lastUpdate=state.updates;
		this.queuePosition = this.updateQueue.length;
		state.players = this.players;
		state.requests = this.requests;
		state.pokemon = this.pokemon;
		return state;
	}
	
	/**
	 * Returns p1 if input is p2, p2 if input is p1. 
	 * @param  {string} side [description]
	 * @return {string}      [description]
	 */
	oppositeSide(side){
		if(side=="p1") return "p2";
		if(side=="p2") return "p1";
		throw FormatError("IncorrectSide","No such side: "+side+"(the only valid sides are 'p1' and 'p2')");
	}

	/**
	 * Returns all requests intended for provided playerid.
	 * @param  {string} playerid 
	 * @return {BattleRequest[]}
	 */
	requestsForPlayer(playerid){
		let requests = []
		for(let i in this.requests){
			if(this.requests[i].player==playerid||this.requests[i].player==this.players[playerid].side){
				requests.push(this.requests[i])
			}
		}
		return requests;
	}

	/**
	 * Returns all unanswered requests in this battle.
	 * @return {BattleRequest[]}
	 */
	holdups(){
		let unhandled = []
		for(let i in this.requests){
			if(this.requests[i].status==null){
				unhandled.push(this.requests[i]);
			}
		}
		return unhandled;
	}

	//Cloned from PokeDex for in-battle use	
	getMovesLearntAtLevel(level,speciesid){
		let species = this.dex.pokedex[speciesid];
		let validMoves = []
		for(let i in species.moveset){
			let methods = species.moveset[i].methods;
			for(let j in methods){
				let cap = methods[j].match("L(\\d+)")
				if(cap==null||cap[1]==null) continue;
				let num = parseInt(cap[1])
				if(isNaN(num)) continue;
				if(level==num){
					validMoves.push(species.moveset[i]);
					break;
				}
			}
		}
		return validMoves;
	}

	//Cloned from PokeDex for in-battle use
	levelAndPercentageForExp(exp,speciesid){
		Log.temp("speciesid: ",speciesid);

		let d = Dex.load("pokemon",speciesid);

		return 2;

		//@fixme: need new thinking around the new dex system.
		//let growth = growths[pokedex.growthrate]
		for(let i=2;i<100;i++){
			if(exp<growth[i]){				
				let level = i-1;
				let lastExp = growth[i-1];
				let expOnLevel = exp-lastExp;
				let newLevelExp = growth[i]-lastExp;
				let percent = expOnLevel/newLevelExp;
				let lo = {'level':level,'percent':percent}
				return lo;
			}
		}
		return {'level':100,'percent':0};
	}

	/**
	 * Makes a single choice 
	 * @param  {Array} choice for moves: [type,_id,choiceno,[choicetarget]] for forceSwitch: [_id,slot]
	 * @return {} 
	 */
	choose(rqid,choicetype,choice="",target=-1){
		let request = this.requests.find(x=>x.rqid==rqid);
		
		if(request==null){
			throw MissingError("MissingRequest","Missing Request: "+rqid);
		}
		
		if(['move','item','switch','pass','run'].indexOf(choicetype)==-1){
			throw FormatError("MalformedChoiceType: "+choicetype);
		}
		
		let choosermon = Util.find(this.pokemon,(x=>x._id==request.pokemon));

		/**
		 * Pass Handling
		 */
		if(choicetype=="pass"){
			if(request.passable==0){
				throw IllegalError("NotPassable","Tried to pass on non-passable request: "+request);
			}

			let alreadyPassed = this.requests.filter(x=>x.status=="pass"&&x.sideid==request.sideid);
			if(alreadyPassed>=request.passable){
				throw IllegalError("TooManyPasses","Your side already passed "+request.passable+" times on this request.")
			}
			request.internalStatus = "pass";
			request.status = "pass";	
		}

		else if(choicetype=='run'){
			let opp = this.getSingleOpponent(choosermon);

			if(!this.settings.allowRunning){
				throw PokemonError("cantescape")
			}

			if(!opp){
				throw PokemonError("cantescape");
			}

			//Wild pokemon can still escape from trainer battles. 
			if(this.players[opp.ownerid].classname=="Trainer"&&this.players[choosermon.ownerid].classname=="Trainer"){
				throw PokemonError("runfromtrainer");
			}

			request.internalStatus ="run";
			request.status = {type:"run"};
		}

		/**
		 * Item Handling
		 */
		else if(choicetype=='item'){
			//Success (automatic for now)
			let targetType = Dex.load("item",choice).battleTarget;
			let targetObj = this.pokemon[target];
			if(targetType!='none'&&(target==-1||targetObj==null)){
				throw IllegalError("MissingTargetField","Item "+choice+" requires a target, but none was provided.");
			}

			/**
			 * Item Target Validation
			 */
			switch(targetType){
				case 'party':
					if(Util.findAll(this.pokemon,{sideid:choosermon.sideid,_id:target}).size==0){
						throw IllegalError("MissingTarget","Target "+target+" is not in your party.");
					}
				case 'active':
					if(Util.findAll(this.pokemon,{active:true,hp:{'$gt':0,_id:target}}).size==0){
						throw IllegalError("MissingTarget","Target "+target+" is not active in this battle");
					}
					break;
				case 'activeAlly':
					if(Util.findAll(this.pokemon,{active:true,hp:{'$gt':0},sideid:choosermon.sideid}).size==0){
						throw IllegalError("MissingTarget","Target "+target+" is not an active ally in this battle");
					};
					break;
				case 'activeFoe':
					if(Util.findAll(this.pokemon,{active:true,hp:{'$gt':0},sideid:this.oppositeSide(choosermon.sideid)}).size==0){
						throw IllegalError("MissingTarget","Target "+target+" is not an active foe in this battle");
					}
					break;
			}

			Item.canUseBattle(this.battle,choice,choosermon,target);
			request.internalStatus = "item "+choice+" "+target;
			request.status = {type:"item ",choice:choice, target:target};
		}
	
		/**
		 * Move Handling
		 */
		else if(choicetype=='move'){
			if(choice=='struggle'){
				//Success
				//Struggle has no target.
				request.internalStatus = "move 1"
				request.status = "move struggle";
			} else {
				//Finds the move either by numkey or stringkey.
				let move = choosermon.activeMoves.find(
					x => x.num == choice 
					|| Util.toId(x.id)==Util.toId(choice));

				if(move==null){
					throw IllegalError("MissingAction",choosermon._id+" does not have move "+choice)
				}

				//handle targeting first so we can use it with both normal moves and struggle.
				let maybeTarget ="";
				let maybeTargetInternal = "";

				if(SINGLETARGETS.indexOf(move.target)!=-1&&Util.count(this.activemons())>2){
					if(target==null){
						throw IllegalError("MissingTargetField","Move "+move.id+" must have a target in non-single battles");
					}
					if(target!=null){
						let targetobj = this.activemons()[target];
						if(targetobj==null){
							throw IllegalError("MissingTarget","Target "+target+" is not a valid active target in this battle.");
						}

						maybeTarget = " "+targetobj._id;
						let pos = targetobj.position;
						maybeTargetInternal = " "+pos;
						if(targetobj.sideid==choosermon.sideid){
							maybeTargetInternal = " "+ (-pos);
						}
					}
				}
				if(move.pp!=null&&move.pp<1){
					throw PokemonError("NoPP",{move:move.id});
				}
				if(move.disabled){
					throw PokemonError("MoveDisabled",{move:move.id});
				}
				let index =1;					
				for(let i=0;i<choosermon.activeMoves.length;i++){
					if(choosermon.activeMoves[i].id==move.id){
						index = i+1;
					}
				}

				//Success
				request.internalStatus = "move "+index+maybeTargetInternal
				request.status = {type:"move",choice:move.id,target:maybeTarget};
			}	
		}

		/**
		 * Switch Handling
		 */
		else if(choicetype=='switch'){
			let switchObj = this.pokemon[choice];

			if(switchObj==null){
				throw IllegalError("InvalidSwitch",choice+" is not a valid switch at this time!");
			}

			//If a pokemon is already picked, another request has referenced this.
			let r = Util.find(this.requests,(r=> r.status!=null&&r.status.choice==choice));
			if(r!=null) throw PokemonError("AlreadySummoned",{id:switchObj._id});
	
			//Success
			switchObj.chosen=true; //ai uses this instead of inspecting other choices. 
			//it's an add, not a switch
			if(request.pokemon==null){
				request.status = {type:"switch",choice:switchObj._id};
				request.internalStatus = switchObj.partyIndex;
			} else {
				request.status = {type:"switch",choice:switchObj._id};
				request.internalStatus = "switch "+switchObj.partyIndex;				
			}
		}
		this.updateBattle();
	}

	sidemons(side){
		let m = []
		for(let i in this.pokemon){
			if(this.pokemon[i].sideid==side){
				m.push(this.pokemon[i])
			}
		}
		return m;
	}

	activemons(side){
		let m = {}
		for(let i in this.pokemon){
			if(side==null||this.pokemon[i].sideid==side){
				if(this.pokemon[i].active){
					m[this.pokemon[i]._id] = this.pokemon[i];
				}
			}
		}
		return m;
	}

	/**
	 * Checks through all received requests, and if they're all valid, queue them up in showdown.
	 * @return {[type]} [description]
	 */
	updateBattle(){
		let sideRequests = {'p1':{type:'',req:[]},'p2':{type:'',req:[]}}

		//1. Read requests over to their appropriate side. 
		for(let i in this.requests){
			if(this.requests[i].status==null){
				return;
			}
			sideRequests[this.requests[i].sideid].isAdd = this.requests[i].pokemon==null
			sideRequests[this.requests[i].sideid].req.push(this.requests[i]);				
		}

		//Obedience is applied AFTER all choices are in to avoid rng abuse.
		if(this.settings.obedience){
			for(let request in this.requests){
				request = this.requests[request];
				//Don't disobey during special conditions. 
				//@TODO: This actually means that if you somehow manage to trap your Pokémon,
				//This will suddenly mean that it always obeys instead. That's probably not good.
				if(request.forceSwitch||request.pokemon==null||request.trapped){
					continue;
				}
				
				request = this.calculateObedience(this.pokemon[request.pokemon],request)
			}
		}

		//2. Sort choices internally by position (showdown takes them in order)
		for(let i in sideRequests){
			sideRequests[i].req.sort(function(a,b){
				return a.position>b.position;
			})
		}

		//3. Finally, serialize 
		//(showdown takes two formats, one specifically for teamPreview)
		let choicestrings = {'p1':"",'p2':""}
		for(let i in sideRequests){
			if(sideRequests[i].isAdd){
				choicestrings[i] = "team ";
				for(let j in sideRequests[i].req){
					choicestrings[i]+=sideRequests[i].req[j].internalStatus;
				}
			} else {
				for(let j in sideRequests[i].req){
					choicestrings[i]+=sideRequests[i].req[j].internalStatus+", ";
				}
				choicestrings[i] = choicestrings[i].substring(0,choicestrings[i].length-2);
			}
		}

		//4. With the serialized choices, we can make all requests to showdown at once. 
		for(let i in choicestrings){
			let logPos = this.battle.log.length; //emulates the 'receive' sub
			if(choicestrings[i]!=""){
				this.battle.add("clearchoices");
				let status = this.battle.choose(i,choicestrings[i]);
				//If this breaks here, we fucked up internally and the battle should crash. 
				if(status!==true){
					throw InternalError("InternalChoiceFailure","Showdown did not accept our choices, this bug is DuskGulls fault: "+status);
					return;
				}
				//Have to manually send updates,
				//Because we directly call 'choose' instead of 'receive'
				this.battle.sendUpdates(logPos, this.battle.ended);		
			}
		}
		//Don't want to keep updating state if battle is over. 
		if(this.finished){
			return;
		}
		//Battle should at this point have sent all new requests and updates.
		this.updateState();
	}

	/**
	 * Callback from the showdown battle, updates and requests are received here. 
	 * @param  {string} type  [description]
	 * @param  {string} input [description]
	 * @return {[type]}       [description]
	 */
	engineMessage(type,input){
			if(type=="update"||type=="winupdate"){
				if(type=="winupdate"){
					this.manager.finished=true;
				}
				//We don't parse this until we have all pokemon ready
				this.manager.updateCache = this.manager.updateCache.concat(input);
		}
		else if(type=="request"){
			//Showdown sends its updates as weird strings, we must reparse it. 
			let concernedPlayer = "p"+input[1];
			let jsonStart = input.indexOf("{");
			let json = input.substring(input.indexOf("{")-1,input.length);
			let data = JSON.parse(json);
			let choicedata = Updates.parseRequest(this,data);			
			this.manager.sides[concernedPlayer] = choicedata;
		}
	}

	/**
	 * Checks if two pokemon are adjacent 
	 * (on either side of the battlefield)
	 */
	adjacent(p1,p2){
		p1 = p1.position;
		p2 = p2.position;
		return p1==p2+1||p1==p2-1||p1==p2;
	}


	/**
	 * Called once requests have been sent for both sides from the battle engine.
	 * It automatically queries AI, and can so end up recursively calling itself 
	 */
	updateState(){
		this.pokemon = {}
		this.requests = []
		this.active={}
		let requestId=0;

		//1. Side Handling (merges side data into the pokemon and request objects)
		for(let i in this.sides){
			this.active[i] = this.sides[i].active
			if(this.sides[i].hasOwnProperty("pokemon")){
				for(let j in this.sides[i].pokemon){					
					let pokemon = this.sides[i].pokemon[j];	
					this.pokemon[pokemon._id] = pokemon;
				}
			}
			if(this.sides[i].hasOwnProperty("requests")){
				for(let j in this.sides[i].requests){
					let request = this.sides[i].requests[j];
					request=requestId;
					requestId++;
					this.requests.push(this.sides[i].requests[j]);
				}
			}
			this.sides[i] = {}
		}


		//2. Target Handling
		

		for(let i in this.pokemon){
			let pmon = this.pokemon[i];
			pmon.targets = {
				allAdjacentFoes:[],
				self:[],
				any:[],
				adjacentAllyOrSelf:[],
				allyTeam:[],
				adjacentAlly:[],
				allySide:[],
				allAdjacent:[],
				scripted:[],
				all:[],
				normal:[],
				adjacentFoe:[],
				randomNormal:[],
				singleEnemy:[], //only gets populated if there is a single enemy (used by balls)
				foeSide:[],
				party:[],
				partyfnt:[]
			}

			for(let i in this.pokemon){
				if(this.pokemon[i].sideid==pmon.sideid){
					if(this.pokemon[i].hp==0) pmon.targets.partyfnt.push(pmon._id);
					else pmon.targets.party.push(pmon._id);
				}
			}

			let active = this.activemons();
			for(let j in active){
				let tmon = active[j];
				if(tmon.hp==0) continue;

				if(tmon._id==pmon._id){
					pmon.targets.adjacentAllyOrSelf.push(tmon._id);
					pmon.targets.self.push(tmon._id);
					continue;
				}
				if(tmon.sideid==pmon.sideid){
					pmon.targets.allyTeam.push(tmon._id);
					pmon.targets.allySide.push(tmon._id);
					if(this.adjacent(pmon,tmon)){
						pmon.targets.adjacentAllyOrSelf.push(tmon._id);
						pmon.targets.adjacentAlly.push(tmon._id);
						pmon.targets.allAdjacent.push(tmon._id);
					}
				} else {
					pmon.targets.foeSide.push(tmon._id);
					if(this.adjacent(pmon,tmon)){
						pmon.targets.allAdjacentFoes.push(tmon._id);
						pmon.targets.adjacentFoe.push(tmon._id);
						pmon.targets.allAdjacent.push(tmon._id);
					}
				}

				pmon.targets.any.push(tmon._id);
				pmon.targets.all.push(tmon._id);
				pmon.targets.randomNormal.push(tmon._id);
				pmon.targets.normal.push(tmon._id);
			}

			if(this.settings.gameType!='singles'){
				pmon.targets.party 				= {'single': pmon.targets.party}
				pmon.targets.partyfnt 			= {'single': pmon.targets.partyfnt};
				pmon.targets.allAdjacentFoes 	= {'auto':pmon.targets.allAdjacentFoes};
				pmon.targets.self 				= {'auto':pmon.targets.self};
				pmon.targets.any 				= {'single':pmon.targets.any};
				pmon.targets.adjacentAllyOrSelf = {'single':pmon.targets.adjacentAllyOrSelf};
				pmon.targets.allyTeam 			= {'auto':pmon.targets.allyTeam};
				pmon.targets.adjacentAlly 		= {'single':pmon.targets.adjacentAlly}
				pmon.targets.allySide 			= {'auto':pmon.targets.allySide};
				pmon.targets.allAdjacent 		= {'auto':pmon.targets.allAdjacent};
				pmon.targets.scripted 			= {'auto':pmon.targets.scripted};
				pmon.targets.all 				= {'auto':pmon.targets.all};
				pmon.targets.adjacentFoe 		= {'single':pmon.targets.adjacentFoe};
				pmon.targets.normal 			= {'single':pmon.targets.normal};
				pmon.targets.randomNormal 		= {'auto':pmon.targets.randomNormal};
				pmon.targets.foeSide 			= {'auto':pmon.targets.foeSide};
			} else {
				pmon.targets.party 				= {'auto': pmon.targets.party}
				pmon.targets.partyfnt 			= {'auto': pmon.targets.partyfnt};
				pmon.targets.allAdjacentFoes 	= {'auto':pmon.targets.allAdjacentFoes};
				pmon.targets.self 				= {'auto':pmon.targets.self};
				pmon.targets.any 				= {'auto':pmon.targets.any};
				pmon.targets.adjacentAllyOrSelf = {'auto':pmon.targets.adjacentAllyOrSelf};
				pmon.targets.allyTeam 			= {'auto':pmon.targets.allyTeam};
				pmon.targets.adjacentAlly 		= {'auto':pmon.targets.adjacentAlly}
				pmon.targets.allySide 			= {'auto':pmon.targets.allySide};
				pmon.targets.allAdjacent 		= {'auto':pmon.targets.allAdjacent};
				pmon.targets.scripted 			= {'auto':pmon.targets.scripted};
				pmon.targets.all 				= {'auto':pmon.targets.all};
				pmon.targets.adjacentFoe 		= {'auto':pmon.targets.adjacentFoe};
				pmon.targets.normal 			= {'auto':pmon.targets.normal};
				pmon.targets.randomNormal 		= {'auto':pmon.targets.randomNormal};
				pmon.targets.foeSide 			= {'auto':pmon.targets.foeSide};
			}
			
		}
		

		

		//We can now parse updates with proper pokemon and player names. 
		this.updateQueue = this.updateQueue.concat(Updates.parse(this.updateCache,this));
		
		this.updateCache = []


		//3. AI Handling
		for(let i in this.requests){
			let player = Util.find(this.players,player=>(
					player.side==this.requests[i].sideid&&
					player.controller=="internal")
				);
			if(player!=null) ai.handleRequest(
				this.pokemon,this.requests[i],this.choose.bind(this),this.settings.gameType=="singles"
			);
		} 

		//isn't this COMPLETELY redundant?
		if(this.finished){
			return;
		}
	}	
}

function escape(activeSpeed,opposingSpeed,escapeAttempts){

}



/**
 * @class Basic wrapper around a Battle instance. 
 * Contains a single Battle, and Pokemon/Trainer objects for updating after a battle. 
 * It consists mostly of shortcuts to Battle functions. 
 */
class BattleRelay{
	/**
	 * Creates a single Battle instance, which itself starts a battle. 
	 * @param  {number} battleid 	[description]
	 * @param  {object} t1       	[description]
	 * @param  {object} t2       	[description]
	 * @param  {array} growths  	[description]
	 * @param  {Trainer[]} players  [description]
	 * @param  {Pokemon[]} pokemon  [description]
	 * @return {[type]}          	[description]
	 */
	constructor(id,side1,side2,settings){
		this.id=id;
		this.pokemon = []
		this.players = {}
		this.finished=false;
		let t1 = this.getBattleSide(side1,"p1");
		let t2 = this.getBattleSide(side2,"p2");
		this.battlemanager = new Battle(id,t1,t2,settings);

		if(this.battlemanager.finished){
			this.onFinish();
		}
	}


	getTargets(pokemon,type){
		return this.battlemanager.getTargets(pokemon,type);
	}

	state(){
		return this.battlemanager.state();	
	}

	onFinish(){
		if(this.battlemanager.settings.affectsWorld){
			if(this.battlemanager.catch){
				let pokemon = this.pokemon.find(x=>x._id==this.battlemanager.catch.user);

				let trainer = this.pokemon.find(x=>x._id==this.battlemanager.catch.user).trainer;
				let caught = this.pokemon.find(x=>x._id==this.battlemanager.catch.target);
				trainer.gainPoke(caught);
			}



			let battlepokemon = this.battlemanager.pokemon;
			for(let i in this.pokemon){
				let poke = this.pokemon[i];
				let battlepoke = battlepokemon[poke._id];
				if(battlepoke==null)continue;
				if(poke.gainsExp()){
					if(battlepoke.experience>poke.experience){
						poke.gainExp(battlepoke.experience-poke.experience,false)
					}
				}
				poke.setHp(battlepoke.hp);
			}
		}
		this.finished=true;
	}

	choose(chooser,choiceType,choiceNo,target){
		if(chooser=='dispose'){
			delete battles[this.id];
			return;
		}

		this.battlemanager.choose(chooser,choiceType,choiceNo,target);
		if(this.battlemanager.finished){
			this.onFinish()
		}
	}

	/**
	 * Converts a trainer, pokemon or trainer array to a battle side for storing in the battle object.
	 * @param  {[type]} player [description]
	 * @return {[type]}        [description]
	 */
	getBattleSide(players,side){

		let sideobj = {pokemon:[], players:{},dex:{pokedex:{},movedex:{}}}
		//Deal with all types of arguments as an array
		if(!Array.isArray(players)){
			players = [players];
		}

		let playable =0;
		for(let i in players){
			if(players[i].canBattle()){
				playable++;
			}

			//1. Set up the player object
			let player = players[i];
			let bplayer = {}
			bplayer._id = player._id;
			bplayer.name = player.name;
			bplayer.side = side;
			bplayer.controller = 
				player.controller!=null?player.controller:"internal";
			sideobj.players[bplayer._id]=bplayer;
			//bplayer.isTrainer = player.isTrainer==null?false:player.isTrainer;

			this.players[player._id] = bplayer;

			//2. Load party if it's a trainer. 
			let party = [];
			if(player.classname=="Trainer"){
				party = player.party;
			} else {
				party = []
				bplayer.obedience = Settings.maxlevel;
			}

			//Add the player if it's a pokemon
			if(player.classname=="Pokemon"&&party.length<6){
				party.push(player);
			}

			//3. Set up pokemon
			for(let i in party){
				let poke = {}
				poke.level = party[i].getLevel();
				poke.name = party[i].name;
				poke.num = party[i].species.num;
				poke.hp = party[i].hp;
				poke.moves = party[i].moves;
				poke.wild = party[i].isWild();


				//Owner defaults to itself
				poke.ownerid = party[i].trainer!=null?party[i].trainer._id:party[i]._id;
				poke.obedience = party[i].trainer==null?Settings.maxlevel:party[i].trainer.obedientLevel();

				poke.catchrate = party[i].species.captureRate;
				
				poke.baseExperience = party[i].species.baseExperience;
				poke.species = party[i].species._id;
				poke.types = party[i].species.types;
				poke.outsider=party[i].isOutsider();
				poke._id = party[i]._id
				poke.experience = party[i].experience;
				poke.gainsExp = party[i].gainsExp();
				sideobj.pokemon.push(poke);
				this.pokemon.push(party[i]);
			}
		}

		if(playable==0||sideobj.pokemon.length==0){
			throw IllegalError("InEligibleTeam"," Team "+players.map(x=>(x._id))+" cannot battle because they have no battle-ready pokemon")
		}
		return sideobj;
	}
}

function defaultFormat(){
	let def={}
	//this overrides everything except 'rated' if not null. 
	def.effectType = "Format";
	def.category="Effect";
	def.id="customgame";
	def.name = "Custom DuskGull Game";
	def.fullname="Custom DuskGull Game";
	def.exists=true;
	def.cached=true;

	def.ignoreStatus = false;
	def.ratedFormat=null;
	def.rated=false; //will only work if a rated format is selected. 
	def.allowRunning=true; // still won't work in trainer battles.
	def.validationFormat=null;
	def.ruleset = ['Pokemon'];
	
	def.gameType="singles";
	def.banlist=[];	
	def.mod = "gen"+Settings.generation;
	def.allowedItemTypes = ['all'];
	def.obedience=true;
	def.affectsWorld = true; //I.e. does battle changes get written to the actual pokemon	
	return def;
}


//Simple test function
function createTemp(p1,p2){
	return create(p1,p2,{})
}

function create(p1,p2,usersettings={}){	
	//1. Read in default settings and apply ours. 
	let settings = defaultFormat();
	for(let i in usersettings){
		settings[i]=usersettings[i];
	}

	//2. If we have a rated format, some settings should be overridden.
	if(settings.format!=null){
		//Some settings 'survive' overwriting, so we keep those temporarily.
		let rated = settings.rated;
		let affectsWorld = settings.affectsWorld;
		let obedience = settings.obedience;

		settings = Dex.load("format",settings.format);

		settings.rated=rated;
		settings.obedience=obedience;
		settings.affectsWorld=affectsWorld;


		settings.allowedItemTypes = [];
		settings.affectsWorld=false;
		settings.allowRunning=false;

		let p1res = Validate(p1,settings.validationFormat);
		let p2res = Validate(p2,settings.validationFormat);
	
		if(p1res||p2res){
			throw PokemonError("ValidationError",{team1issues:p1res,team2issues:p2res});
		}
	}

	//4. Find a battle id
	let i=0;
	while(battles[i]!=null){
		i++;
	}

	//5. Start battle and store it. 
	let battle = new BattleRelay(i,p1,p2,settings);
	battles[i] = battle;
	return battle;
}

function get(id){
	let k =  battles[id];
	if(k==null) throw MissingError("MissingBattle","Battle "+id+" is not available.");
	return k;
}
function remove(i){
	delete battles[i];
}
function choose(id,choice){
	this.battles[id].choose(choice);
}

exports.create = create;
exports.get = get;
exports.remove = remove;