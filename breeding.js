/*
 * DuskGull - Pokemon World Simulator
 * 
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~breeding.js~
 * Handles direct breeding of two Pokemon.
 * The important function is breed(...)
 */

/**
 * Checks through anything that could go wrong with a breed
 * Tells you with an exception what went wrong, if anything. 
 * @param  {Pokemon} p1 [description]
 * @param  {Pokemon} p2 [description]
 */
function checkBreed(p1,p2){
	if(p1.eggGroups[0]=='Undiscovered') throw PokemonError("UndiscoveredBreed");
	if(p2.eggGroups[0]=='Undiscovered') throw PokemonError("UndiscoveredBreed");
	
	if(!Util.anyMatch(p1.eggGroups,p2.eggGroups)){
		throw PokemonError("NoBreedingMatch");
	}
	if(p1._id=="ditto"&&p2._id=="ditto"){
		throw PokemonError("DittoBreedingDitto");
	}
	if(p1.gender=='N' &&!p2._id=="ditto"){
		throw PokemonError("GenderlessWithoutDitto");
	}
	if(p2.gender=='N' &&!p1._id=="ditto"){
		throw PokemonError("GenderlessWithoutDitto");
	}

	//sorry but they really can't...
	if(p1.gender!=p2.gender&&(p1._id!="ditto")||(p2._id!="ditto")){
		throw PokemonError("HomoBreeding");
	}
}

function decideParents(mon1,mon2){
	if(mon2.species._id=="ditto"){
		return {f:mon1,m:mon2}
	}

	if(mon1.species._id=="ditto"){
		return {f:mon2,m:mon1}
	}

	if(mon1.gender=="F"){
		return {f:mon1,m:mon2}
	}

	return {f:mon2,m:mon1};
}

/**
 * The main function that breeds two Pokemon (if valid)
 * And creates a baby the same way that the games does it. 
 * @todo : Stat breeding algorithm is gen3. 
 */
function breed(p1,p2,temp=false){
	//Throws exception if not compatible
	checkBreed(p1.species,p2.species);
	let parents = decideParents(p1,p2);

	//Create child.
	let child = Pokemon({
		species:parents.f.species.finalPrevo()._id,
		level:Settings.babyLevel
	})

	//Breed stats
	let stats = Util.shuffle(['spa','spe','atk','spd','def','hp']);
	let dadiv = Util.random(0,3);
	let momiv = 3-dadiv;
	for(let i=0;i<dadiv;i++){
		child.ivs[stats[i]] = parents.m.ivs[stats[i]];
		delete stats[i];
	}

	for(let i=0;i<momiv;i++){
		child.ivs[stats[i]] = parents.m.ivs[stats[i]];
		delete stats[i];
	}

	//Parse temp map
	if(temp||(p1.temp&&p2.temp)){
		child.temp=true;
	}
	return child;
}

exports.checkBreed = checkBreed;
exports.decideParents = decideParents;
exports.breed = breed;
