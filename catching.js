/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~catching.js~
 * Does everything related to catch-calculations.
 * One would typically call 'catchPokemon' from within a Showdown simulation. 
 */

/**
 * Attempts to catch a pokemon, choosing a generation-specific algorithm depending on battle settings. 
 * @param {Battle} battle
 * @param {BattlePokemon} battletarget
 * @param {BattlePokemon} battlesource
 * @param {ItemDex} ball
 * @{@link http://bulbapedia.bulbagarden.net/wiki/Catch_rate}
 * @returns {int} -1 if it's not a wild pokemon [i.e. 'Trainer blocked the ball!'], 0-3 for rolls and 4 for 3 rolls and a catch. 
 */

function catchPokemon(battle,target,source,ball){
	//@todo: Currently only support gen3-style catching. 
	return GEN3(battle,target,source,ball);
}

/**
 * Catches pokemon based on generation 1 games algorithm 
 * @WIP 
 */
function GEN1(battle,battletarget,source,ball){
	let num
	let M = Math.random(0,255);
	let f = Math.floor(Math.floor(battletarget.maxhp*255*4)/Math.floor(battletarget.curhp*ballScalar));
	let catchrate = battletarget.set.speciesdata.catchrate;
	let status = battletarget.status;

	if(ball==POKEBALL){
		num = Math.random(0,255);
	}
	
	else if(ball==GREATBALL){
		num = Math.random(0,200);
	}
	
	else if(ball==ULTRABALL){
		num = Math.random(0,150)
	}
	
	let ballScalar = ball._id=="greatball"?8:12;
	let target = 12;
	if(target.status=="PAR"||target.status=="SLP"||target.status=="PSN"){
		let target = 25;
	}
	
	//pokemon was caught. 
	if(num-target>catchrate&&f>=M){
		return -1;
	}

	let d = catchrate*100/num;
	if(d>=256) return 3;

	let s = 0;
	if(status=="SLP"){
		s=10;
	}

	if(status=="PAR"||status=="PSN"||status=="BRN"){
		s = 5;
	}
	
	let x = d*f/255+s;
	if(x<10) return 0;
	if(x<30) return 1;
	if(x<70) return 2;
	return 3;
}


/**
 * Catches pokemon based on generation 2 games algorithm
 * @WIP
 */
function GEN2(battletarget,ball){
	let statusMod = 0;

	let hpMaxMod = 3*battletarget.maxhp;
	let hpMod = 2*battletarget.curhp;

	let modRate = battletarget.speciesdata.catchrate;

	//clamp.
	modRate = Util.clamp(modRate,1,255);

	//simulates the games handling of values above 255 
	if(hpMaxMod>255){
		hpMaxMod=Math.floor(hpMaxMod/2)
		hpMaxMod=Math.floor(hpMaxMod/2)
		hpMod = Math.floor(hpMod/2)
		hpMod = Math.floor(hpMod/2);
	}

	if(battletarget.status=="SLP"||battletarget.status=="FRZ") statusMod=10;
	let catchrate = Math.max((hpMaxMod-hpMod) 
		* modRate(battle,battletarget,battlesource,ball) 
		/ (3*battletarget.maxhp),1)
		+statusMod;
	
	catchrate = Util.clamp(catchrate,1,255);
	let b;

	if(catchrate==255) b = 255;
	if(catchrate<255) b = 241;
	if(catchrate<221) b = 246;
	if(catchrate<200) b = 240;
	if(catchrate<181) b = 234;
	if(catchrate<161) b = 227;
	if(catchrate<141) b = 220;
	if(catchrate<121) b = 211;
	if(catchrate<101) b = 201;
	if(catchrate<81) b = 191;
	if(catchrate<61) b = 177;
	if(catchrate<51) b = 169;
	if(catchrate<41) b = 160;
	if(catchrate<31) b = 149;
	if(catchrate<21) b = 134;
	if(catchrate<16) b = 126;
	if(catchrate<11) b = 113;
	if(catchrate<8) b = 103;
	if(catchrate<6) b = 95;
	if(catchrate<5) b = 90;
	if(catchrate<4) b = 84;
	if(catchrate<3) b = 75;
	if(catchrate<2) b = 63;


	if(Math.random(0,255)<=a){
		return -1;		
	}

	let attempt = 1;
	while(attempt<=3){
		if(Math.random(0,255)<=b){
			return attempt;
		}
		attempt++;
 	}
 	return 3;
}
/**
 * @{@link http://bulbapedia.bulbagarden.net/wiki/Catch_rate#Capture_method_.28Generation_III-IV.29}
 */
function GEN3(battle,battletarget,battlesource,ball){
	let maxhpmod = 3*battletarget.maxhp
	let hpmod = 2*battletarget.curhp
	let catchmod = item.catchrate(source,target);

	let statusbonus = 1;
	if(target.status == "PSN" || target.status=="PAR" || target.status == "BURN"){
		statusbonus = 1.5;
	}

	if(target.status == "SLP" || target.status=="FRZ"){
		statusbonus=2;
	}
	
	let a = (((maxhpmod-hpmod)*catchmod)/maxhpmod)*statusbonus;
	if(a>=255) return 4;
	let b = Math.floor(1048560 / Math.floor(Math.sqrt(Math.floor(Math.sqrt(Math.floor(16711680/a))))))

	if(Math.random(0,65535)>=b) return 0;
	if(Math.random(0,65535)>=b) return 1;
	if(Math.random(0,65535)>=b) return 2;
	if(Math.random(0,65535)>=b) return 3;
	return 4;
}


exports.catchPokemon = catchPokemon
exports.GEN3 = GEN3;