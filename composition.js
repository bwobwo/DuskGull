/**
 * Simple alternative to Object.assign
 * that's more similar to classy inheritence
 *
 * It all the passed objects properties overwrite one anothers,
 * but none overwrite the objects properties.
 *
 * Passed objects can also have a special key called '__abstract__'
 * 
 * @return {[type]}         [description]
 */

function implement(thiz, ...objs){
	let pratatop = {}

	for(let obj in objs){
		obj = objs[obj];
		if(typeof(obj)!='object'){
			throw InternalError("Object is not an object")
		}

		for(let i in obj){
			pratatop[i] = obj[i];
		}
	}

	for(let i in pratatop){
		if(thiz[i]==null){
			thiz[i] = pratatop[i];
		}
	}
}