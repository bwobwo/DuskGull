exports.formes = {
	'unown':[
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','?','!'
	],

	'castform':[
		'','Sunny','Rainy','Snowy',''
	],

	'deoxys':[
		'Normal','Attack','Defense','Speed',''
	],

	'burmy':[
		'Plant','Sandy','Trash',''
	],

	'wormadam':[
		'Plant','Sandy','Trash',''
	],

	'cherrim':[
		'Overcast','Sunshine',''
	],

	'shellos':[
		'West Sea','East Sea',''
	],

	'gastrodon':[
		'West Sea','East Sea',''
	],

	'rotom':[
		'','Heat','Wash','Frost','Fan','Mow',''
	],

	'giratina':[
		'Altered','Origin',''
	],

	'shaymin':[
		'Land','Sky',''
	],

	'arceus':[
		'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',
	],

	'basculin':[
		'Red-Striped','Blue-Striped',''
	],

	'darmanitan':[
		'Normal','Zen-Mode',''
	],

	'sawsbuck':[
		'Spring','Autumn','Summer','Winter',''
	],

	'deerling':[
		'Spring','Autumn','Summer','Winter',''
	],

	'tornadus':[
		'Incarnate','Therian',''
	],

	'landorus':[
		'Incarnate','Therian',''
	],

	'thundurus':[
		'Incarnate','Therian',''
	],

	'kyurem':[
		'','','','','','','','','','','','','','','',''
	],

	'genesect':[
		'Shock','Burn','Chill','Douse',''
	],

	'meloetta':[
		'Aria','Pirouette',''
	],

	'keldeo':[
		'Ordinary','Resolute',''
	]
}