exports.items = {
    abomasite: {
        num: 1055,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ユキノオナイト",
            korean: "눈설왕나이트",
            french: "Blizzarite",
            german: "Rexblisarnit",
            spanish: "Abomasnowita",
            italian: "Abomasnowite",
            english: "Abomasite"
        }
    },
    absolite: {
        num: 1058,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "アブソルナイト",
            korean: "앱솔나이트",
            french: "Absolite",
            german: "Absolnit",
            spanish: "Absolita",
            italian: "Absolite",
            english: "Absolite"
        }
    },
    absorbbulb: {
        num: 932,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "きゅうこん",
            korean: "구근",
            french: "Bulbe",
            german: "Knolle",
            spanish: "Tubérculo",
            italian: "Bulbo",
            english: "Absorb Bulb"
        }
    },
    adamantorb: {
        num: 145,
        cost: 10000,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "こんごうだま",
            korean: "금강옥",
            french: "Orbe Adamant",
            german: "Adamant-Orb",
            spanish: "Diamansfera",
            italian: "Adamasfera",
            english: "Adamant Orb"
        }
    },
    adrenalineorb: {
        num: 846
    },
    aerodactylite: {
        num: 1053,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "プテラナイト",
            korean: "프테라나이트",
            french: "Ptéraïte",
            german: "Aerodactylonit",
            spanish: "Aerodactylita",
            italian: "Aerodactylite",
            english: "Aerodactylite"
        }
    },
    aggronite: {
        num: 1048,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ボスゴドラナイト",
            korean: "보스로라나이트",
            french: "Galekingite",
            german: "Stollossnit",
            spanish: "Aggronita",
            italian: "Aggronite",
            english: "Aggronite"
        }
    },
    aguavberry: {
        num: 331,
        cost: 20,
        itemcategory: "berrypicky",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 64,
            growthtime: 5,
            soildryness: 10,
            smoothness: 25,
            berryid: 14
        },
        names: {
            japanese: "バンジのみ",
            korean: "아바열매",
            french: "Baie Gowav",
            german: "Gauvebeere",
            spanish: "Baya Guaya",
            italian: "Baccaguava",
            english: "Aguav Berry"
        }
    },
    airballoon: {
        num: 928,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "ふうせん",
            korean: "풍선",
            french: "Ballon",
            german: "Luftballon",
            spanish: "Globo Helio",
            italian: "Palloncino",
            english: "Air Balloon"
        }
    },
    alakazite: {
        num: 1060,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "フーディナイト",
            korean: "후디나이트",
            french: "Alakazamite",
            german: "Simsalanit",
            spanish: "Alakazamita",
            italian: "Alakazamite",
            english: "Alakazite"
        }
    },
    aloraichiumz: {
        num: 803
    },
    altarianite: {
        num: 755
    },
    ampharosite: {
        num: 1039,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "デンリュウナイト",
            korean: "전룡나이트",
            french: "Pharampite",
            german: "Ampharosnit",
            spanish: "Ampharosita",
            italian: "Ampharosite",
            english: "Ampharosite"
        }
    },
    apicotberry: {
        num: 374,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 75,
            growthtime: 24,
            soildryness: 4,
            smoothness: 40,
            berryid: 57
        },
        names: {
            japanese: "ズアのみ",
            korean: "규살열매",
            french: "Baie Abriko",
            german: "Apikobeere",
            spanish: "Baya Aricoc",
            italian: "Baccacocca",
            english: "Apicot Berry"
        }
    },
    armorfossil: {
        num: 137,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "たてのカセキ",
            korean: "방패의화석",
            french: "Fossile Armure",
            german: "Panzerfossil",
            spanish: "Fósil Coraza",
            italian: "Fossilscudo",
            english: "Armor Fossil"
        }
    },
    aspearberry: {
        num: 319,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 50,
            growthtime: 3,
            soildryness: 15,
            smoothness: 25,
            berryid: 5
        },
        names: {
            japanese: "ナナシのみ",
            korean: "배리열매",
            french: "Baie Willia",
            german: "Wilbirbeere",
            spanish: "Baya Perasi",
            italian: "Baccaperina",
            english: "Aspear Berry"
        }
    },
    assaultvest: {
        num: 1025,
        cost: 0,
        itemcategory: "helditems",
        names: {
            japanese: "とつげきチョッキ",
            korean: "돌격조끼",
            french: "Veste de Combat",
            german: "Offensivweste",
            spanish: "Chaleco Asalto",
            italian: "Corpetto assalto",
            english: "Assault Vest"
        }
    },
    audinite: {
        num: 757
    },
    babiriberry: {
        num: 368,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 265,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 51
        },
        names: {
            japanese: "リリバのみ",
            korean: "바리비열매",
            french: "Baie Babiri",
            german: "Babiribeere",
            spanish: "Baya Baribá",
            italian: "Baccababiri",
            english: "Babiri Berry"
        }
    },
    banettite: {
        num: 1049,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ジュペッタナイト",
            korean: "다크펫나이트",
            french: "Branettite",
            german: "Banetteonit",
            spanish: "Banettita",
            italian: "Banettite",
            english: "Banettite"
        }
    },
    beastball: {
        num: 851,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
                if(Util.equalsAny(
                    battletarget.speciesid,
                    'nihilego',
                    'buzzwole',
                    'pheromosa',
                    'xurkitree',
                    'celesteela',
                    'kartana',
                    'guzzlord')){
                return target.catchrate*5;
            }
            return target.catchrate*0.1;
        },
    },
    beedrillite: {
        num: 770
    },
    belueberry: {
        num: 352,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 1,
            size: 300,
            growthtime: 15,
            soildryness: 8,
            smoothness: 35,
            berryid: 35
        },
        names: {
            japanese: "ベリブのみ",
            korean: "루베열매",
            french: "Baie Myrte",
            german: "Myrtilbeere",
            spanish: "Baya Andano",
            italian: "Baccartillo",
            english: "Belue Berry"
        }
    },
    berryjuice: {
        num: 61,
        cost: 100,
        itemcategory: "healing",
        heal: 20,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "きのみジュース",
            korean: "나무열매쥬스",
            french: "Jus de Baie",
            german: "Beerensaft",
            spanish: "Zumo de Baya",
            italian: "Succodibacca",
            english: "Berry Juice"
        }
    },
    bigroot: {
        num: 465,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "おおきなねっこ",
            korean: "큰뿌리",
            french: "Grosse Racine",
            german: "Großwurzel",
            spanish: "Raíz Grande",
            italian: "Granradice",
            english: "Big Root"
        }
    },
    bindingband: {
        num: 931,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "しめつけバンド",
            korean: "조임밴드",
            french: "Bande Étreinte",
            german: "Klammerband",
            spanish: "Banda Atadura",
            italian: "Legafascia",
            english: "Binding Band"
        }
    },
    blackbelt: {
        num: 410,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "くろおび",
            korean: "검은띠",
            french: "Ceinture Noire",
            german: "Schwarzgurt",
            spanish: "Cinturón Negro",
            italian: "Cinturanera",
            english: "Black Belt"
        }
    },
    blacksludge: {
        num: 450,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "くろいヘドロ",
            korean: "검은진흙",
            french: "Boue Noire",
            german: "Giftschleim",
            spanish: "Lodo Negro",
            italian: "Fangopece",
            english: "Black Sludge"
        }
    },
    blackglasses: {
        num: 409,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "くろいメガネ",
            korean: "검은안경",
            french: "Lunettes Noires",
            german: "Schattenglas",
            spanish: "Gafas de Sol",
            italian: "Occhialineri",
            english: "Black Glasses"
        }
    },
    blastoisinite: {
        num: 1042,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "カメックスナイト",
            korean: "거북왕나이트",
            french: "Tortankite",
            german: "Turtoknit",
            spanish: "Blastoisita",
            italian: "Blastoisite",
            english: "Blastoisinite"
        }
    },
    blazikenite: {
        num: 1045,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "バシャーモナイト",
            korean: "번치코나이트",
            french: "Braségalite",
            german: "Lohgocknit",
            spanish: "Blazikenita",
            italian: "Blazikenite",
            english: "Blazikenite"
        }
    },
    blueorb: {
        num: 741,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "あいいろのたま",
            korean: "쪽빛구슬",
            french: "Orbe Bleu",
            german: "Blaue Kugel",
            spanish: "Esfera Azul",
            italian: "Sfera Blu",
            english: "Blue Orb"
        }
    },
    blukberry: {
        num: 334,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 2,
            size: 108,
            growthtime: 2,
            soildryness: 35,
            smoothness: 20,
            berryid: 17
        },
        names: {
            japanese: "ブリーのみ",
            korean: "블리열매",
            french: "Baie Remu",
            german: "Morbbeere",
            spanish: "Baya Oram",
            italian: "Baccamora",
            english: "Bluk Berry"
        }
    },
    brightpowder: {
        num: 382,
        cost: 10,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "ひかりのこな",
            korean: "반짝가루",
            french: "Poudre Claire",
            german: "Blendpuder",
            spanish: "Polvo Brillo",
            italian: "Luminpolvere",
            english: "Bright Powder"
        }
    },
    buggem: {
        num: 945,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "むしのジュエル",
            korean: "벌레주얼",
            french: "Joyau Insecte",
            german: "Käferjuwel",
            spanish: "Gema Bicho",
            italian: "Bijoucoleottero",
            english: "Bug Gem"
        }
    },
    bugmemory: {
        num: 909
    },
    buginiumz: {
        num: 787
    },
    burndrive: {
        num: 892,
        cost: 1000,
        itemcategory: "speciesspecific",
        names: {
            japanese: "ブレイズカセット",
            korean: "블레이즈카세트",
            french: "Module Pyro",
            german: "Flammenmodul",
            spanish: "PiroROM",
            italian: "Piromodulo",
            english: "Burn Drive"
        }
    },
    cameruptite: {
        num: 767
    },
    cellbattery: {
        num: 933,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "じゅうでんち",
            korean: "충전지",
            french: "Pile",
            german: "Akku",
            spanish: "Pila",
            italian: "Ricaripila",
            english: "Cell Battery"
        }
    },
    charcoal: {
        num: 418,
        cost: 9800,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "もくたん",
            korean: "목탄",
            french: "Charbon",
            german: "Holzkohle",
            spanish: "Carbón",
            italian: "Carbonella",
            english: "Charcoal"
        }
    },
    charizarditex: {
        num: 1041,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "リザードナイトＸ",
            korean: "리자몽나이트X",
            french: "Dracaufite X",
            german: "Gluraknit X",
            spanish: "Charizardita X",
            italian: "Charizardite X",
            english: "Charizardite X"
        }
    },
    charizarditey: {
        num: 1059,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "リザードナイトＹ",
            korean: "리자몽나이트Y",
            french: "Dracaufite Y",
            german: "Gluraknit Y",
            spanish: "Charizardita Y",
            italian: "Charizardite Y",
            english: "Charizardite Y"
        }
    },
    chartiberry: {
        num: 364,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 1,
            size: 28,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 47
        },
        names: {
            japanese: "ヨロギのみ",
            korean: "루미열매",
            french: "Baie Charti",
            german: "Chiaribeere",
            spanish: "Baya Alcho",
            italian: "Baccaciofo",
            english: "Charti Berry"
        }
    },
    cheriberry: {
        num: 261,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 20,
            growthtime: 3,
            soildryness: 15,
            smoothness: 25,
            berryid: 1
        },
        names: {
            japanese: "クラボのみ",
            korean: "버치열매",
            french: "Baie Ceriz",
            german: "Amrenabeere",
            spanish: "Baya Zreza",
            italian: "Baccaliegia",
            english: "Cheri Berry"
        }
    },
    cherishball: {
        num: 32,
        cost: 200,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {
            japanese: "プレシャスボール",
            korean: "프레셔스볼",
            french: "Mémoire Ball",
            german: "Jubelball",
            spanish: "Gloria Ball",
            italian: "Pregio Ball",
            english: "Cherish Ball"
        }
    },
    chestoberry: {
        num: 262,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 80,
            growthtime: 3,
            soildryness: 15,
            smoothness: 25,
            berryid: 2
        },
        names: {
            japanese: "カゴのみ",
            korean: "유루열매",
            french: "Baie Maron",
            german: "Maronbeere",
            spanish: "Baya Atania",
            italian: "Baccastagna",
            english: "Chesto Berry"
        }
    },
    chilanberry: {
        num: 369,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 1,
            size: 34,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 52
        },
        names: {
            japanese: "ホズのみ",
            korean: "카리열매",
            french: "Baie Zalis",
            german: "Latchibeere",
            spanish: "Baya Chilan",
            italian: "Baccacinlan",
            english: "Chilan Berry"
        }
    },
    chilldrive: {
        num: 893,
        cost: 1000,
        itemcategory: "speciesspecific",
        names: {
            japanese: "フリーズカセット",
            korean: "프리즈카세트",
            french: "Module Cryo",
            german: "Gefriermodul",
            spanish: "CrioROM",
            italian: "Gelomodulo",
            english: "Chill Drive"
        }
    },
    choiceband: {
        num: 389,
        cost: 100,
        itemcategory: "choiceitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "こだわりハチマキ",
            korean: "구애머리띠",
            french: "Bandeau Choix",
            german: "Wahlband",
            spanish: "Cinta Elegida",
            italian: "Bendascelta",
            english: "Choice Band"
        }
    },
    choicescarf: {
        num: 456,
        cost: 200,
        itemcategory: "choiceitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "こだわりスカーフ",
            korean: "구애스카프",
            french: "Mouchoir Choix",
            german: "Wahlschal",
            spanish: "Pañuelo Elegido",
            italian: "Stolascelta",
            english: "Choice Scarf"
        }
    },
    choicespecs: {
        num: 466,
        cost: 200,
        itemcategory: "choiceitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "こだわりメガネ",
            korean: "구애안경",
            french: "Lunettes Choix",
            german: "Wahlglas",
            spanish: "Gafas Elegid",
            italian: "Lentiscelta",
            english: "Choice Specs"
        }
    },
    chopleberry: {
        num: 358,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 77,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 41
        },
        names: {
            japanese: "ヨプのみ",
            korean: "로플열매",
            french: "Baie Pomroz",
            german: "Rospelbeere",
            spanish: "Baya Pomaro",
            italian: "Baccarosmel",
            english: "Chople Berry"
        }
    },
    clawfossil: {
        num: 131,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "ツメのカセキ",
            korean: "발톱화석",
            french: "Fossile Griffe",
            german: "Klauenfossil",
            spanish: "Fósil Garra",
            italian: "Fossilunghia",
            english: "Claw Fossil"
        }
    },
    cobaberry: {
        num: 361,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 278,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 44
        },
        names: {
            japanese: "バコウのみ",
            korean: "바코열매",
            french: "Baie Cobaba",
            german: "Kobabeere",
            spanish: "Baya Kouba",
            italian: "Baccababa",
            english: "Coba Berry"
        }
    },
    colburberry: {
        num: 367,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 39,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 50
        },
        names: {
            japanese: "ナモのみ",
            korean: "마코열매",
            french: "Baie Lampou",
            german: "Burleobeere",
            spanish: "Baya Dillo",
            italian: "Baccaxan",
            english: "Colbur Berry"
        }
    },
    cornnberry: {
        num: 344,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 3,
            size: 75,
            growthtime: 6,
            soildryness: 10,
            smoothness: 30,
            berryid: 27
        },
        names: {
            japanese: "モコシのみ",
            korean: "수숙열매",
            french: "Baie Siam",
            german: "Saimbeere",
            spanish: "Baya Mais",
            italian: "Baccavena",
            english: "Cornn Berry"
        }
    },
    coverfossil: {
        num: 957,
        cost: 1000,
        itemcategory: "dexcompletion",
        names: {
            japanese: "ふたのカセキ",
            korean: "덮개화석",
            french: "Fossile Plaque",
            german: "Schildfossil",
            spanish: "Fósil Tapa",
            italian: "Fossiltappo",
            english: "Cover Fossil"
        }
    },
    custapberry: {
        num: 379,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 267,
            growthtime: 24,
            soildryness: 7,
            smoothness: 60,
            berryid: 62
        },
        names: {
            japanese: "イバンのみ",
            korean: "애슈열매",
            french: "Baie Chérim",
            german: "Eipfelbeere",
            spanish: "Baya Chiri",
            italian: "Baccacrela",
            english: "Custap Berry"
        }
    },
    damprock: {
        num: 454,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "しめったいわ",
            korean: "축축한바위",
            french: "Roche Humide",
            german: "Nassbrocken",
            spanish: "Roca Lluvia",
            italian: "Rocciaumida",
            english: "Damp Rock"
        }
    },
    darkgem: {
        num: 948,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "あくのジュエル",
            korean: "악주얼",
            french: "Joyau Ténèbres",
            german: "Unlichtjuwel",
            spanish: "Gema Siniestra",
            italian: "Bijoubuio",
            english: "Dark Gem"
        }
    },
    darkmemory: {
        num: 919
    },
    darkiniumz: {
        num: 791
    },
    decidiumz: {
        num: 798
    },
    deepseascale: {
        num: 396,
        cost: 200,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "しんかいのウロコ",
            korean: "심해의비늘",
            french: "Écaille Océan",
            german: "Abyssplatte",
            spanish: "Escama Marina",
            italian: "Squamabissi",
            english: "Deep Sea Scale"
        }
    },
    deepseatooth: {
        num: 395,
        cost: 200,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "しんかいのキバ",
            korean: "심해의이빨",
            french: "Dent Océan",
            german: "Abysszahn",
            spanish: "Diente Marino",
            italian: "Dente Abissi",
            english: "Deep Sea Tooth"
        }
    },
    destinyknot: {
        num: 449,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "あかいいと",
            korean: "빨간실",
            french: "Nœud Destin",
            german: "Fatumknoten",
            spanish: "Lazo Destino",
            italian: "Destincomune",
            english: "Destiny Knot"
        }
    },
    diancite: {
        num: 764
    },
    diveball: {
        num: 23,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            if(battle.location=="dive"){
                return target.catchrate*4;
            }
            return target.catchrate;
        },
        names: {
            japanese: "ダイブボール",
            korean: "다이브볼",
            french: "Scuba Ball",
            german: "Tauchball",
            spanish: "Buceo Ball",
            italian: "Sub Ball",
            english: "Dive Ball"
        }
    },
    domefossil: {
        num: 133,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "こうらのカセキ",
            korean: "껍질화석",
            french: "Fossile Dôme",
            german: "Domfossil",
            spanish: "Fósil Domo",
            italian: "Domofossile",
            english: "Dome Fossil"
        }
    },
    dousedrive: {
        num: 890,
        cost: 1000,
        itemcategory: "speciesspecific",
        names: {
            japanese: "アクアカセット",
            korean: "아쿠아카세트",
            french: "Module Aqua",
            german: "Aquamodul",
            spanish: "HidroROM",
            italian: "Idromodulo",
            english: "Douse Drive"
        }
    },
    dracoplate: {
        num: 480,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "りゅうのプレート",
            korean: "용의플레이트",
            french: "Plaque Draco",
            german: "Dracotafel",
            spanish: "Tabla Draco",
            italian: "Lastradrakon",
            english: "Draco Plate"
        }
    },
    dragonfang: {
        num: 419,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "りゅうのキバ",
            korean: "용의이빨",
            french: "Croc Dragon",
            german: "Drachenzahn",
            spanish: "Colmillo Dragón",
            italian: "Dentedidrago",
            english: "Dragon Fang"
        }
    },
    dragongem: {
        num: 1012,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "ドラゴンジュエル",
            korean: "드래곤주얼",
            french: "Joyau Dragon",
            german: "Drakojuwel",
            spanish: "Gema Dragón",
            italian: "Bijoudrago",
            english: "Dragon Gem"
        }
    },
    dragonmemory: {
        num: 918
    },
    dragoniumz: {
        num: 790
    },
    dreadplate: {
        num: 481,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "こわもてプレート",
            korean: "공포플레이트",
            french: "Plaque Ombre",
            german: "Furchttafel",
            spanish: "Tabla Oscura",
            italian: "Lastratimore",
            english: "Dread Plate"
        }
    },
    dreamball: {
        num: 961,
        cost: 0,
        itemcategory: 33,
        names: {
            japanese: "ドリームボール",
            korean: "드림볼",
            french: "Rêve Ball",
            german: "Traumball",
            spanish: "Ensueño Ball",
            italian: "Dream Ball",
            english: "Dream Ball"
        }
    },
    durinberry: {
        num: 351,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 3,
            size: 280,
            growthtime: 15,
            soildryness: 8,
            smoothness: 35,
            berryid: 34
        },
        names: {
            japanese: "ドリのみ",
            korean: "두리열매",
            french: "Baie Durin",
            german: "Durinbeere",
            spanish: "Baya Rudion",
            italian: "Baccadurian",
            english: "Durin Berry"
        }
    },
    duskball: {
        num: 29,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            if(Util.isNight()||battle.location=="cave"){
                return target.catchrate*3.5
            }
            return target.catchrate;
        },
        names: {
            japanese: "ダークボール",
            korean: "다크볼",
            french: "Sombre Ball",
            german: "Finsterball",
            spanish: "Ocaso Ball",
            italian: "Scuro Ball",
            english: "Dusk Ball"
        }
    },
    earthplate: {
        num: 474,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "だいちのプレート",
            korean: "대지플레이트",
            french: "Plaque Terre",
            german: "Erdtafel",
            spanish: "Tabla Terrax",
            italian: "Lastrageo",
            english: "Earth Plate"
        }
    },
    eeviumz: {
        num: 805
    },
    ejectbutton: {
        num: 934,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "だっしゅつボタン",
            korean: "탈출버튼",
            french: "Bouton Fuite",
            german: "Fluchtknopf",
            spanish: "Botón Escape",
            italian: "Pulsantefuga",
            english: "Eject Button"
        }
    },
    electirizer: {
        num: 491,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "エレキブースター",
            korean: "에레키부스터",
            french: "Électriseur",
            german: "Stromisierer",
            spanish: "Electrizador",
            italian: "Elettritore",
            english: "Electirizer"
        }
    },
    electricgem: {
        num: 937,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "でんきのジュエル",
            korean: "전기주얼",
            french: "Joyau Électrik",
            german: "Elektrojuwel",
            spanish: "Gema Eléctrica",
            italian: "Bijouelettro",
            english: "Electric Gem"
        }
    },
    electricmemory: {
        num: 915
    },
    electricseed: {
        num: 881
    },
    electriumz: {
        num: 779
    },
    energypowder: {
        num: 52,
        cost: 500,
        itemcategory: "healing",
        heal: 60,
        herb: true,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ちからのこな",
            korean: "힘의가루",
            french: "Poudrénergie",
            german: "Energiestaub",
            spanish: "Polvo Energía",
            italian: "Polvenergia",
            english: "Energy Powder"
        }
    },
    enigmaberry: {
        num: 377,
        cost: 20,
        itemcategory: 4,
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 155,
            growthtime: 24,
            soildryness: 7,
            smoothness: 60,
            berryid: 60
        },
        names: {
            japanese: "ナゾのみ",
            korean: "의문열매",
            french: "Baie Enigma",
            german: "Enigmabeere",
            spanish: "Baya Enigma",
            italian: "Baccaenigma",
            english: "Enigma Berry"
        }
    },
    eviolite: {
        num: 925,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "しんかのきせき",
            korean: "진화의휘석",
            french: "Évoluroc",
            german: "Evolith",
            spanish: "Mineral Evol",
            italian: "Evolcondensa",
            english: "Eviolite"
        }
    },
    expertbelt: {
        num: 437,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "たつじんのおび",
            korean: "달인의띠",
            french: "Ceinture Pro",
            german: "Expertengurt",
            spanish: "Cinta Experto",
            italian: "Abilcintura",
            english: "Expert Belt"
        }
    },
    fairiumz: {
        num: 793
    },
    fairygem: {
        num: 1073,
        cost: 0,
        itemcategory: "jewels",
        names: {
            japanese: "ようせいジュエル",
            korean: "페어리주얼",
            french: "Joyau Fée",
            german: "Feenjuwel",
            spanish: "Gema Hada",
            italian: "Bijoufolletto",
            english: "Fairy Gem"
        }
    },
    fairymemory: {
        num: 920
    },
    fastball: {
        num: 726,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
           if(battle.gen==2){
                if(Util.equalsAny(target.species,"magnemite","tangela","grimer")){
                    return target.catchrate*4;               
                }
            } else {
                if(target.speed>=100){
                    return target.catchrate*4;
                }
            }
        },
        names: {
            japanese: "スピードボール",
            korean: "스피드볼",
            french: "Speed Ball",
            german: "Turboball",
            spanish: "Rapid Ball",
            italian: "Rapid Ball",
            english: "Fast Ball"
        }
    },
    fightinggem: {
        num: 940,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "かくとうジュエル",
            korean: "격투주얼",
            french: "Joyau Combat",
            german: "Kampfjuwel",
            spanish: "Gema Lucha",
            italian: "Bijoulotta",
            english: "Fighting Gem"
        }
    },
    fightingmemory: {
        num: 904
    },
    fightiniumz: {
        num: 782
    },
    figyberry: {
        num: 328,
        cost: 20,
        itemcategory: "berrypicky",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 100,
            growthtime: 5,
            soildryness: 10,
            smoothness: 25,
            berryid: 11
        },
        names: {
            japanese: "フィラのみ",
            korean: "무화열매",
            french: "Baie Figuy",
            german: "Giefebeere",
            spanish: "Baya Higog",
            italian: "Baccafico",
            english: "Figy Berry"
        }
    },
    firegem: {
        num: 935,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "ほのおのジュエル",
            korean: "불꽃주얼",
            french: "Joyau Feu",
            german: "Feuerjuwel",
            spanish: "Gema Fuego",
            italian: "Bijoufuoco",
            english: "Fire Gem"
        }
    },
    firememory: {
        num: 912
    },
    firiumz: {
        num: 777
    },
    fistplate: {
        num: 472,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "こぶしのプレート",
            korean: "주먹플레이트",
            french: "Plaque Poing",
            german: "Fausttafel",
            spanish: "Tabla Fuerte",
            italian: "Lastrapugno",
            english: "Fist Plate"
        }
    },
    flameorb: {
        num: 442,
        cost: 100,
        itemcategory: "effortitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "かえんだま",
            korean: "화염구슬",
            french: "Orbe Flamme",
            german: "Heiß-Orb",
            spanish: "Llamasfera",
            italian: "Fiammosfera",
            english: "Flame Orb"
        }
    },
    flameplate: {
        num: 467,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "ひのたまプレート",
            korean: "불구슬플레이트",
            french: "Plaque Flamme",
            german: "Feuertafel",
            spanish: "Tabla Llama",
            italian: "Lastrarogo",
            english: "Flame Plate"
        }
    },
    floatstone: {
        num: 926,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "かるいし",
            korean: "가벼운돌",
            french: "Pierrallégée",
            german: "Leichtstein",
            spanish: "Piedra Pómez",
            italian: "Pietralieve",
            english: "Float Stone"
        }
    },
    flyinggem: {
        num: 943,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "ひこうのジュエル",
            korean: "비행주얼",
            french: "Joyau Vol",
            german: "Flugjuwel",
            spanish: "Gema Voladora",
            italian: "Bijouvolante",
            english: "Flying Gem"
        }
    },
    flyingmemory: {
        num: 905
    },
    flyiniumz: {
        num: 785
    },
    focusband: {
        num: 399,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "きあいのハチマキ",
            korean: "기합의머리띠",
            french: "Bandeau",
            german: "Fokus-Band",
            spanish: "Cinta Focus",
            italian: "Bandana",
            english: "Focus Band"
        }
    },
    focussash: {
        num: 444,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "きあいのタスキ",
            korean: "기합의띠",
            french: "Ceinture Force",
            german: "Fokusgurt",
            spanish: "Banda Focus",
            italian: "Focalnastro",
            english: "Focus Sash"
        }
    },
    friendball: {
        num: 727,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {
            japanese: "フレンドボール",
            korean: "프렌드볼",
            french: "Copain Ball",
            german: "Freundesball",
            spanish: "Amigo Ball",
            italian: "Friend Ball",
            english: "Friend Ball"
        }
    },
    fullincense: {
        num: 485,
        cost: 9600,
        itemcategory: "effortitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "まんぷくおこう",
            korean: "만복향로",
            french: "Encens Plein",
            german: "Lahmrauch",
            spanish: "Incienso Lento",
            italian: "Gonfioaroma",
            english: "Full Incense"
        }
    },
    galladite: {
        num: 756
    },
    ganlonberry: {
        num: 371,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 33,
            growthtime: 24,
            soildryness: 4,
            smoothness: 40,
            berryid: 54
        },
        names: {
            japanese: "リュガのみ",
            korean: "용아열매",
            french: "Baie Lingan",
            german: "Linganbeere",
            spanish: "Baya Gonlan",
            italian: "Baccalongan",
            english: "Ganlon Berry"
        }
    },
    garchompite: {
        num: 1064,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ガブリアスナイト",
            korean: "한카리아스나이트",
            french: "Carchacrokite",
            german: "Knakracknit",
            spanish: "Garchompita",
            italian: "Garchompite",
            english: "Garchompite"
        }
    },
    gardevoirite: {
        num: 1038,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "サーナイトナイト",
            korean: "가디안나이트",
            french: "Gardevoirite",
            german: "Guardevoirnit",
            spanish: "Gardevoirita",
            italian: "Gardevoirite",
            english: "Gardevoirite"
        }
    },
    gengarite: {
        num: 1037,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ゲンガナイト",
            korean: "팬텀나이트",
            french: "Ectoplasmite",
            german: "Gengarnit",
            spanish: "Gengarita",
            italian: "Gengarite",
            english: "Gengarite"
        }
    },
    ghostgem: {
        num: 947,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "ゴーストジュエル",
            korean: "고스트주얼",
            french: "Joyau Spectre",
            german: "Geistjuwel",
            spanish: "Gema Fantasma",
            italian: "Bijouspettro",
            english: "Ghost Gem"
        }
    },
    ghostmemory: {
        num: 910
    },
    ghostiumz: {
        num: 789
    },
    glalitite: {
        num: 763
    },
    grassgem: {
        num: 938,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "くさのジュエル",
            korean: "풀주얼",
            french: "Joyau Plante",
            german: "Pflanzjuwel",
            spanish: "Gema Planta",
            italian: "Bijouerba",
            english: "Grass Gem"
        }
    },
    grassmemory: {
        num: 914
    },
    grassiumz: {
        num: 780
    },
    grassyseed: {
        num: 884
    },
    greatball: {
        num: 19,
        cost: 600,
        itemcategory: "ball",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        
        catchrate: function(battle,source,target){
            return 1.5*target.catchrate;
        },

        names: {
            japanese: "スーパーボール",
            korean: "수퍼볼",
            french: "Super Ball",
            german: "Superball",
            spanish: "Super Ball",
            italian: "Mega Ball",
            english: "Great Ball"
        }
    },
    grepaberry: {
        num: 342,
        cost: 20,
        itemcategory: "evdropberry",
        ev: 'spd',
        berry: {
            firmness: 2,
            size: 149,
            growthtime: 8,
            soildryness: 8,
            smoothness: 20,
            berryid: 25
        },
        names: {
            japanese: "ウブのみ",
            korean: "또뽀열매",
            french: "Baie Résin",
            german: "Labrusbeere",
            spanish: "Baya Uvav",
            italian: "Baccauva",
            english: "Grepa Berry"
        }
    },
    gripclaw: {
        num: 455,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "ねばりのかぎづめ",
            korean: "끈기갈고리손톱",
            french: "Accro Griffe",
            german: "Griffklaue",
            spanish: "Garra Garfio",
            italian: "Presartigli",
            english: "Grip Claw"
        }
    },
    griseousorb: {
        num: 714,
        cost: 10000,
        itemcategory: "speciesspecific",
        names: {
            japanese: "はっきんだま",
            korean: "백금옥",
            french: "Orbe Platiné",
            german: "Platinum-Orb",
            spanish: "Griseosfera",
            italian: "Grigiosfera",
            english: "Griseous Orb"
        }
    },
    groundgem: {
        num: 942,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "じめんのジュエル",
            korean: "땅주얼",
            french: "Joyau Sol",
            german: "Bodenjuwel",
            spanish: "Gema Tierra",
            italian: "Bijouterra",
            english: "Ground Gem"
        }
    },
    groundmemory: {
        num: 907
    },
    groundiumz: {
        num: 784
    },
    gyaradosite: {
        num: 1057,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ギャラドスナイト",
            korean: "갸라도스나이트",
            french: "Léviatorite",
            german: "Garadosnit",
            spanish: "Gyaradosita",
            italian: "Gyaradosite",
            english: "Gyaradosite"
        }
    },
    habanberry: {
        num: 366,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 23,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 49
        },
        names: {
            japanese: "ハバンのみ",
            korean: "하반열매",
            french: "Baie Fraigo",
            german: "Terirobeere",
            spanish: "Baya Anjiro",
            italian: "Baccahaban",
            english: "Haban Berry"
        }
    },
    hardstone: {
        num: 407,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "かたいいし",
            korean: "딱딱한돌",
            french: "Pierre Dure",
            german: "Granitstein",
            spanish: "Piedra Dura",
            italian: "Pietradura",
            english: "Hard Stone"
        }
    },
    healball: {
        num: 30,
        cost: 300,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {
            japanese: "ヒールボール",
            korean: "힐볼",
            french: "Soin Ball",
            german: "Heilball",
            spanish: "Sana Ball",
            italian: "Cura Ball",
            english: "Heal Ball"
        }
    },
    heatrock: {
        num: 453,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "あついいわ",
            korean: "뜨거운바위",
            french: "Roche Chaude",
            german: "Heißbrocken",
            spanish: "Roca Calor",
            italian: "Rocciacalda",
            english: "Heat Rock"
        }
    },
    heavyball: {
        num: 725,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
            if(target.weight<225.8 && battle.gen==2){
                return target.catchrate-20;
            }
            if(target.weight<451.5){
                if(battle.gen==2){
                    return target.catchrate;
                }
                return target.catchrate-20;
            }
            //same for both gen 2 and 4.
            if(target.weight<=677.3){
                return target.catchrate+20;
            }
            if(battle.gen==2 || target.weight<=903.0){
                return target.catchrate+30;
            }
            return target.catchrate+40;
        },
        names: {
            japanese: "ヘビーボール",
            korean: "헤비볼",
            french: "Masse Ball",
            german: "Schwerball",
            spanish: "Peso Ball",
            italian: "Peso Ball",
            english: "Heavy Ball"
        }
    },
    helixfossil: {
        num: 132,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "かいのカセキ",
            korean: "조개화석",
            french: "Nautile",
            german: "Helixfossil",
            spanish: "Fósil Hélix",
            italian: "Helixfossile",
            english: "Helix Fossil"
        }
    },
    heracronite: {
        num: 1061,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ヘラクロスナイト",
            korean: "헤라크로스나이트",
            french: "Scarhinoïte",
            german: "Skarabornit",
            spanish: "Heracrossita",
            italian: "Heracrossite",
            english: "Heracronite"
        }
    },
    hondewberry: {
        num: 341,
        cost: 20,
        itemcategory: "evdropberry",
        ev: 'spa',
        berry: {
            firmness: 3,
            size: 162,
            growthtime: 8,
            soildryness: 8,
            smoothness: 20,
            berryid: 24
        },
        names: {
            japanese: "ロメのみ",
            korean: "로매열매",
            french: "Baie Lonme",
            german: "Honmelbeere",
            spanish: "Baya Meluce",
            italian: "Baccamelon",
            english: "Hondew Berry"
        }
    },
    houndoominite: {
        num: 1047,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ヘルガナイト",
            korean: "헬가나이트",
            french: "Démolossite",
            german: "Hundemonit",
            spanish: "Houndoomita",
            italian: "Houndoomite",
            english: "Houndoominite"
        }
    },
    iapapaberry: {
        num: 332,
        cost: 20,
        itemcategory: "berrypicky",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 223,
            growthtime: 5,
            soildryness: 10,
            smoothness: 25,
            berryid: 15
        },
        names: {
            japanese: "イアのみ",
            korean: "파야열매",
            french: "Baie Papaya",
            german: "Yapabeere",
            spanish: "Baya Pabaya",
            italian: "Baccapaia",
            english: "Iapapa Berry"
        }
    },
    icegem: {
        num: 939,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "こおりのジュエル",
            korean: "얼음주얼",
            french: "Joyau Glace",
            german: "Eisjuwel",
            spanish: "Gema Hielo",
            italian: "Bijoughiaccio",
            english: "Ice Gem"
        }
    },
    icememory: {
        num: 917
    },
    icicleplate: {
        num: 471,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "つららのプレート",
            korean: "고드름플레이트",
            french: "Plaque Glace",
            german: "Frosttafel",
            spanish: "Tabla Helada",
            italian: "Lastragelo",
            english: "Icicle Plate"
        }
    },
    iciumz: {
        num: 781
    },
    icyrock: {
        num: 451,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "つめたいいわ",
            korean: "차가운바위",
            french: "Roche Glace",
            german: "Eisbrocken",
            spanish: "Roca Helada",
            italian: "Rocciafredda",
            english: "Icy Rock"
        }
    },
    inciniumz: {
        num: 799
    },
    insectplate: {
        num: 477,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "たまむしプレート",
            korean: "비단벌레플레이트",
            french: "Plaque Insecte",
            german: "Käfertafel",
            spanish: "Tabla Bicho",
            italian: "Lastrabaco",
            english: "Insect Plate"
        }
    },
    ironball: {
        num: 447,
        cost: 200,
        itemcategory: "effortitem",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "くろいてっきゅう",
            korean: "검은철구",
            french: "Balle Fer",
            german: "Eisenkugel",
            spanish: "Bola Férrea",
            italian: "Ferropalla",
            english: "Iron Ball"
        }
    },
    ironplate: {
        num: 482,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "こうてつプレート",
            korean: "강철플레이트",
            french: "Plaque Fer",
            german: "Eisentafel",
            spanish: "Tabla Acero",
            italian: "Lastraferro",
            english: "Iron Plate"
        }
    },
    jabocaberry: {
        num: 380,
        cost: 20,
        itemcategory: 4,
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 33,
            growthtime: 24,
            soildryness: 7,
            smoothness: 60,
            berryid: 63
        },
        names: {
            japanese: "ジャポのみ",
            korean: "자보열매",
            french: "Baie Jaboca",
            german: "Jabocabeere",
            spanish: "Baya Jaboca",
            italian: "Baccajaba",
            english: "Jaboca Berry"
        }
    },
    kasibberry: {
        num: 365,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 144,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 48
        },
        names: {
            japanese: "カシブのみ",
            korean: "수불열매",
            french: "Baie Sédra",
            german: "Zitarzbeere",
            spanish: "Baya Drasi",
            italian: "Baccacitrus",
            english: "Kasib Berry"
        }
    },
    kebiaberry: {
        num: 359,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 90,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 42
        },
        names: {
            japanese: "ビアーのみ",
            korean: "으름열매",
            french: "Baie Kébia",
            german: "Grarzbeere",
            spanish: "Baya Kebia",
            italian: "Baccakebia",
            english: "Kebia Berry"
        }
    },
    keeberry: {
        num: 1066,
        cost: 0,
        itemcategory: 4,
        names: {
            japanese: "アッキのみ",
            korean: "악키열매",
            french: "Baie Éka",
            german: "Akibeere",
            spanish: "Baya Biglia",
            italian: "Baccalighia",
            english: "Kee Berry"
        }
    },
    kelpsyberry: {
        num: 339,
        cost: 20,
        itemcategory: "evdropberry",
        ev: 'atk',
        berry: {
            firmness: 3,
            size: 150,
            growthtime: 8,
            soildryness: 8,
            smoothness: 20,
            berryid: 22
        },
        names: {
            japanese: "ネコブのみ",
            korean: "시마열매",
            french: "Baie Alga",
            german: "Setangbeere",
            spanish: "Baya Algama",
            italian: "Baccalga",
            english: "Kelpsy Berry"
        }
    },
    kangaskhanite: {
        num: 1056,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ガルーラナイト",
            korean: "캥카나이트",
            french: "Kangourexite",
            german: "Kangamanit",
            spanish: "Kangaskhanita",
            italian: "Kangaskhanite",
            english: "Kangaskhanite"
        }
    },
    kingsrock: {
        num: 390,
        cost: 100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "おうじゃのしるし",
            korean: "왕의징표석",
            french: "Roche Royale",
            german: "King-Stein",
            spanish: "Roca del Rey",
            italian: "Roccia di Re",
            english: "King's Rock"
        }
    },
    laggingtail: {
        num: 448,
        cost: 200,
        itemcategory: "effortitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "こうこうのしっぽ",
            korean: "느림보꼬리",
            french: "Ralentiqueue",
            german: "Schwerschwf.",
            spanish: "Cola Plúmbea",
            italian: "Rallentocoda",
            english: "Lagging Tail"
        }
    },
    lansatberry: {
        num: 375,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 97,
            growthtime: 24,
            soildryness: 4,
            smoothness: 50,
            berryid: 58
        },
        names: {
            japanese: "サンのみ",
            korean: "랑사열매",
            french: "Baie Lansat",
            german: "Lansatbeere",
            spanish: "Baya Zonlan",
            italian: "Baccalangsa",
            english: "Lansat Berry"
        }
    },
    latiasite: {
        num: 684
    },
    latiosite: {
        num: 685
    },
    laxincense: {
        num: 424,
        cost: 9600,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "のんきのおこう",
            korean: "무사태평향로",
            french: "Encens Doux",
            german: "Laxrauch",
            spanish: "Incienso Suave",
            italian: "Distraroma",
            english: "Lax Incense"
        }
    },
    leftovers: {
        num: 403,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "たべのこし",
            korean: "먹다남은음식",
            french: "Restes",
            german: "Überreste",
            spanish: "Restos",
            italian: "Avanzi",
            english: "Leftovers"
        }
    },
    leppaberry: {
        num: 320,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 28,
            growthtime: 4,
            soildryness: 15,
            smoothness: 20,
            berryid: 6
        },
        names: {
            japanese: "ヒメリのみ",
            korean: "과사열매",
            french: "Baie Mepo",
            german: "Jonagobeere",
            spanish: "Baya Zanama",
            italian: "Baccamela",
            english: "Leppa Berry"
        }
    },
    levelball: {
        num: 723,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
            if(source.level>target.level*4){
                return target.catchrate*8;
            }

            if(source.level>target.level*2){
                return target.catchrate*4;
            }

            if(source.level>target.level){
                return target.catchrate*2;
            }

            return target.catchrate;
        },
        names: {
            japanese: "レベルボール",
            korean: "레벨볼",
            french: "Niveau Ball",
            german: "Levelball",
            spanish: "Nivel Ball",
            italian: "Level Ball",
            english: "Level Ball"
        }
    },
    liechiberry: {
        num: 370,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 111,
            growthtime: 24,
            soildryness: 4,
            smoothness: 40,
            berryid: 53
        },
        names: {
            japanese: "チイラのみ",
            korean: "치리열매",
            french: "Baie Lichii",
            german: "Lydzibeere",
            spanish: "Baya Lichi",
            italian: "Baccalici",
            english: "Liechi Berry"
        }
    },
    lifeorb: {
        num: 439,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "いのちのたま",
            korean: "생명의구슬",
            french: "Orbe Vie",
            german: "Leben-Orb",
            spanish: "Vidasfera",
            italian: "Assorbisfera",
            english: "Life Orb"
        }
    },
    lightball: {
        num: 405,
        cost: 100,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "でんきだま",
            korean: "전기구슬",
            french: "Balle Lumière",
            german: "Kugelblitz",
            spanish: "Bolaluminosa",
            italian: "Elettropalla",
            english: "Light Ball"
        }
    },
    lightclay: {
        num: 438,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "ひかりのねんど",
            korean: "빛의점토",
            french: "Lumargile",
            german: "Lichtlehm",
            spanish: "Refleluz",
            italian: "Creta Luce",
            english: "Light Clay"
        }
    },
    lopunnite: {
        num: 768
    },
    loveball: {
        num: 728,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
            if(target.gender!=source.gender){
                return target.catchrate*8;
            }

            return target.catchrate;
        },
        names: {
            japanese: "ラブラブボール",
            korean: "러브러브볼",
            french: "Love Ball",
            german: "Sympaball",
            spanish: "Amor Ball",
            italian: "Love Ball",
            english: "Love Ball"
        }
    },
    lucarionite: {
        num: 1054,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ルカリオナイト",
            korean: "루카리오나이트",
            french: "Lucarite",
            german: "Lucarionit",
            spanish: "Lucarita",
            italian: "Lucarite",
            english: "Lucarionite"
        }
    },
    luckypunch: {
        num: 425,
        cost: 10,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "ラッキーパンチ",
            korean: "럭키펀치",
            french: "Poing Chance",
            german: "Lucky Punch",
            spanish: "Puño Suerte",
            italian: "Fortunpugno",
            english: "Lucky Punch"
        }
    },
    lumberry: {
        num: 324,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 34,
            growthtime: 12,
            soildryness: 8,
            smoothness: 20,
            berryid: 9
        },
        names: {
            japanese: "ラムのみ",
            korean: "리샘열매",
            french: "Baie Prine",
            german: "Prunusbeere",
            spanish: "Baya Ziuela",
            italian: "Baccaprugna",
            english: "Lum Berry"
        }
    },
    luminousmoss: {
        num: 1030,
        cost: 0,
        itemcategory: "helditems",
        names: {
            japanese: "ひかりごけ",
            korean: "빛이끼",
            french: "Lichen Lumineux",
            german: "Leuchtmoos",
            spanish: "Musgo Brillante",
            italian: "Muschioluce",
            english: "Luminous Moss"
        }
    },
    lureball: {
        num: 722,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
            if(battle.location!=null&&battle.location=="fish"){
                return target.catchrate*3;
            }
            return target.catchrate;
        },
        names: {
            japanese: "ルアーボール",
            korean: "루어볼",
            french: "Appât Ball",
            german: "Köderball",
            spanish: "Cebo Ball",
            italian: "Esca Ball",
            english: "Lure Ball"
        }
    },
    lustrousorb: {
        num: 146,
        cost: 10000,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "しらたま",
            korean: "백옥",
            french: "Orbe Perlé",
            german: "Weiß-Orb",
            spanish: "Lustresfera",
            italian: "Splendisfera",
            english: "Lustrous Orb"
        }
    },
    luxuryball: {
        num: 27,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {
            japanese: "ゴージャスボール",
            korean: "럭셔리볼",
            french: "Luxe Ball",
            german: "Luxusball",
            spanish: "Lujo Ball",
            italian: "Chic Ball",
            english: "Luxury Ball"
        }
    },
    machobrace: {
        num: 384,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "きょうせいギプス",
            korean: "교정깁스",
            french: "Bracelet Macho",
            german: "Machoband",
            spanish: "Brazal Firme",
            italian: "Crescicappa",
            english: "Macho Brace"
        }
    },
    magnet: {
        num: 411,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "じしゃく",
            korean: "자석",
            french: "Aimant",
            german: "Magnet",
            spanish: "Imán",
            italian: "Calamita",
            english: "Magnet"
        }
    },
    magoberry: {
        num: 330,
        cost: 20,
        itemcategory: "berrypicky",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 126,
            growthtime: 5,
            soildryness: 10,
            smoothness: 25,
            berryid: 13
        },
        names: {
            japanese: "マゴのみ",
            korean: "마고열매",
            french: "Baie Mago",
            german: "Magobeere",
            spanish: "Baya Ango",
            italian: "Baccamango",
            english: "Mago Berry"
        }
    },
    magostberry: {
        num: 345,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 3,
            size: 140,
            growthtime: 6,
            soildryness: 10,
            smoothness: 30,
            berryid: 28
        },
        names: {
            japanese: "ゴスのみ",
            korean: "고스티열매",
            french: "Baie Mangou",
            german: "Magostbeere",
            spanish: "Baya Aostan",
            italian: "Baccagostan",
            english: "Magost Berry"
        }
    },
    mail: {},
    manectite: {
        num: 1063,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ライボルトナイト",
            korean: "썬더볼트나이트",
            french: "Élecsprintite",
            german: "Voltensonit",
            spanish: "Manectricita",
            italian: "Manectricite",
            english: "Manectite"
        }
    },
    marangaberry: {
        num: 1067,
        cost: 0,
        itemcategory: 4,
        names: {
            japanese: "タラプのみ",
            korean: "타라프열매",
            french: "Baie Rangma",
            german: "Tarabeere",
            spanish: "Baya Maranga",
            italian: "Baccapane",
            english: "Maranga Berry"
        }
    },
    marshadiumz: {
        num: 802
    },
    masterball: {
        num: 17,
        cost: 0,
        itemcategory: "ball",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "マスターボール",
            korean: "마스터볼",
            french: "Master Ball",
            german: "Meisterball",
            spanish: "Master Ball",
            italian: "Master Ball",
            english: "Master Ball"
        }
    },
    mawilite: {
        num: 1062,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "クチートナイト",
            korean: "입치트나이트",
            french: "Mysdibulite",
            german: "Flunkifernit",
            spanish: "Mawilita",
            italian: "Mawilite",
            english: "Mawilite"
        }
    },
    meadowplate: {
        num: 470,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "みどりのプレート",
            korean: "초록플레이트",
            french: "Plaque Herbe",
            german: "Wiesentafel",
            spanish: "Tabla Pradal",
            italian: "Lastraprato",
            english: "Meadow Plate"
        }
    },
    medichamite: {
        num: 1046,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "チャーレムナイト",
            korean: "요가램나이트",
            french: "Charminite",
            german: "Meditalisnit",
            spanish: "Medichamita",
            italian: "Medichamite",
            english: "Medichamite"
        }
    },
    mentalherb: {
        num: 388,
        cost: 100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "メンタルハーブ",
            korean: "멘탈허브",
            french: "Herbe Mental",
            german: "Mentalkraut",
            spanish: "Hierba Mental",
            italian: "Mentalerba",
            english: "Mental Herb"
        }
    },
    metagrossite: {
        num: 758
    },
    metalcoat: {
        num: 402,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "メタルコート",
            korean: "금속코트",
            french: "Peau Métal",
            german: "Metallmantel",
            spanish: "Revest. Metálico",
            italian: "Metalcoperta",
            english: "Metal Coat"
        }
    },
    metalpowder: {
        num: 426,
        cost: 10,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "メタルパウダー",
            korean: "금속파우더",
            french: "Poudre Métal",
            german: "Metallstaub",
            spanish: "Polvo Metálico",
            italian: "Metalpolvere",
            english: "Metal Powder"
        }
    },
    metronome: {
        num: 446,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "メトロノーム",
            korean: "메트로놈",
            french: "Métronome",
            german: "Metronom",
            spanish: "Metrónomo",
            italian: "Plessimetro",
            english: "Metronome"
        }
    },
    mewniumz: {
        num: 806
    },
    mewtwonitex: {
        num: 1043,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ミュウツナイトＸ",
            korean: "뮤츠나이트X",
            french: "Mewtwoïte X",
            german: "Mewtunit X",
            spanish: "Mewtwoita X",
            italian: "Mewtwoite X",
            english: "Mewtwonite X"
        }
    },
    mewtwonitey: {
        num: 1044,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ミュウツナイトＹ",
            korean: "뮤츠나이트Y",
            french: "Mewtwoïte Y",
            german: "Mewtunit Y",
            spanish: "Mewtwoita Y",
            italian: "Mewtwoite Y",
            english: "Mewtwonite Y"
        }
    },
    micleberry: {
        num: 378,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 41,
            growthtime: 24,
            soildryness: 7,
            smoothness: 60,
            berryid: 61
        },
        names: {
            japanese: "ミクルのみ",
            korean: "미클열매",
            french: "Baie Micle",
            german: "Wunfrubeere",
            spanish: "Baya Lagro",
            italian: "Baccaracolo",
            english: "Micle Berry"
        }
    },
    mindplate: {
        num: 476,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "ふしぎのプレート",
            korean: "이상한플레이트",
            french: "Plaque Esprit",
            german: "Hirntafel",
            spanish: "Tabla Mental",
            italian: "Lastramente",
            english: "Mind Plate"
        }
    },
    miracleseed: {
        num: 408,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "きせきのタネ",
            korean: "기적의씨",
            french: "Grain Miracle",
            german: "Wundersaat",
            spanish: "Semilla Milagro",
            italian: "Miracolseme",
            english: "Miracle Seed"
        }
    },
    mistyseed: {
        num: 883
    },
    moonball: {
        num: 724,
        cost: 300,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
           if(Util.equalsAny(target.speciesid,"nidoran","clefairy","jigglypuff","skitty","munna")){
                return target.catchrate*4;
            }
            return target.catchrate;
        },
        names: {
            japanese: "ムーンボール",
            korean: "문볼",
            french: "Lune Ball",
            german: "Mondball",
            spanish: "Luna Ball",
            italian: "Luna Ball",
            english: "Moon Ball"
        }
    },
    muscleband: {
        num: 435,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "ちからのハチマキ",
            korean: "힘의머리띠",
            french: "Bandeau Muscle",
            german: "Muskelband",
            spanish: "Cinta Fuerte",
            italian: "Muscolbanda",
            english: "Muscle Band"
        }
    },
    mysticwater: {
        num: 412,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "しんぴのしずく",
            korean: "신비의물방울",
            french: "Eau Mystique",
            german: "Zauberwasser",
            spanish: "Agua Mística",
            italian: "Acqua Magica",
            english: "Mystic Water"
        }
    },
    nanabberry: {
        num: 335,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 4,
            size: 77,
            growthtime: 2,
            soildryness: 35,
            smoothness: 20,
            berryid: 18
        },
        names: {
            japanese: "ナナのみ",
            korean: "나나열매",
            french: "Baie Nanab",
            german: "Nanabbeere",
            spanish: "Baya Latano",
            italian: "Baccabana",
            english: "Nanab Berry"
        }
    },
    nestball: {
        num: 24,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            if(battle.gen<5){
                return target.catchrate*Math.max(1,(40-target.level)/10);
            }

            if(battle.gen<7){
                return target.catchrate*Math.max(1,(41-target.level)/10);
            }

            //gen7 is weird
            return target.catchrate*Math.max(1,8-0.2*(target.level-1))
        },
        names: {
            japanese: "ネストボール",
            korean: "네스트볼",
            french: "Faiblo Ball",
            german: "Nestball",
            spanish: "Nido Ball",
            italian: "Minor Ball",
            english: "Nest Ball"
        }
    },
    netball: {
        num: 22,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            if(Util.equalsAny(target.types,"bug","water")){
                return target.catchrate*3;
            }
            return target.catchrate;
        },
        names: {
            japanese: "ネットボール",
            korean: "넷트볼",
            french: "Filet Ball",
            german: "Netzball",
            spanish: "Malla Ball",
            italian: "Rete Ball",
            english: "Net Ball"
        }
    },
    nevermeltice: {
        num: 415,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "とけないこおり",
            korean: "녹지않는얼음",
            french: "Glace Éternelle",
            german: "Ewiges Eis",
            spanish: "Antiderretir",
            italian: "Gelomai",
            english: "Never-Melt Ice"
        }
    },
    nomelberry: {
        num: 347,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 5,
            size: 285,
            growthtime: 6,
            soildryness: 10,
            smoothness: 30,
            berryid: 30
        },
        names: {
            japanese: "ノメルのみ",
            korean: "노멜열매",
            french: "Baie Tronci",
            german: "Tronzibeere",
            spanish: "Baya Monli",
            italian: "Baccalemon",
            english: "Nomel Berry"
        }
    },
    normalgem: {
        num: 1013,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "ノーマルジュエル",
            korean: "노말주얼",
            french: "Joyau Normal",
            german: "Normaljuwel",
            spanish: "Gema Normal",
            italian: "Bijounormale",
            english: "Normal Gem"
        }
    },
    normaliumz: {
        num: 776
    },
    occaberry: {
        num: 353,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 90,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 36
        },
        names: {
            japanese: "オッカのみ",
            korean: "오카열매",
            french: "Baie Chocco",
            german: "Koakobeere",
            spanish: "Baya Caoca",
            italian: "Baccacao",
            english: "Occa Berry"
        }
    },
    
    oddincense: {
        num: 483,
        cost: 9600,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "あやしいおこう",
            korean: "괴상한향로",
            french: "Encens Bizarre",
            german: "Schrägrauch",
            spanish: "Incienso Raro",
            italian: "Bizzoaroma",
            english: "Odd Incense"
        }
    },
    oldamber: {
        num: 134,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "ひみつのコハク",
            korean: "비밀의호박",
            french: "Vieil Ambre",
            german: "Altbernstein",
            spanish: "Ámbar Viejo",
            italian: "Ambra Antica",
            english: "Old Amber"
        }
    },
    oranberry: {
        num: 321,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 35,
            growthtime: 4,
            soildryness: 15,
            smoothness: 20,
            berryid: 7
        },
        names: {
            japanese: "オレンのみ",
            korean: "오랭열매",
            french: "Baie Oran",
            german: "Sinelbeere",
            spanish: "Baya Aranja",
            italian: "Baccarancia",
            english: "Oran Berry"
        }
    },
    pamtreberry: {
        num: 349,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 1,
            size: 244,
            growthtime: 15,
            soildryness: 8,
            smoothness: 35,
            berryid: 32
        },
        names: {
            japanese: "シーヤのみ",
            korean: "자야열매",
            french: "Baie Palma",
            german: "Pallmbeere",
            spanish: "Baya Plama",
            italian: "Baccapalma",
            english: "Pamtre Berry"
        }
    },
    parkball: {
        num: 729,
        cost: 0,
        itemcategory: "ball",
        names: {
            japanese: "パークボール",
            korean: "파크볼",
            french: "Parc Ball",
            german: "Parkball",
            spanish: "Parque Ball",
            italian: "Parco Ball",
            english: "Park Ball"
        }
    },
    passhoberry: {
        num: 354,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 33,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 37
        },
        names: {
            japanese: "イトケのみ",
            korean: "꼬시개열매",
            french: "Baie Pocpoc",
            german: "Foepasbeere",
            spanish: "Baya Pasio",
            italian: "Baccapasflo",
            english: "Passho Berry"
        }
    },
    payapaberry: {
        num: 362,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 252,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 45
        },
        names: {
            japanese: "ウタンのみ",
            korean: "야파열매",
            french: "Baie Yapap",
            german: "Pyapabeere",
            spanish: "Baya Payapa",
            italian: "Baccapayapa",
            english: "Payapa Berry"
        }
    },
    pechaberry: {
        num: 263,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 1,
            size: 40,
            growthtime: 3,
            soildryness: 15,
            smoothness: 25,
            berryid: 3
        },
        names: {
            japanese: "モモンのみ",
            korean: "복슝열매",
            french: "Baie Pêcha",
            german: "Pirsifbeere",
            spanish: "Baya Meloc",
            italian: "Baccapesca",
            english: "Pecha Berry"
        }
    },
    persimberry: {
        num: 323,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 47,
            growthtime: 4,
            soildryness: 15,
            smoothness: 20,
            berryid: 8
        },
        names: {
            japanese: "キーのみ",
            korean: "시몬열매",
            french: "Baie Kika",
            german: "Persimbeere",
            spanish: "Baya Caquic",
            italian: "Baccaki",
            english: "Persim Berry"
        }
    },
    petayaberry: {
        num: 373,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 237,
            growthtime: 24,
            soildryness: 4,
            smoothness: 40,
            berryid: 56
        },
        names: {
            japanese: "ヤタピのみ",
            korean: "야타비열매",
            french: "Baie Pitaye",
            german: "Tahaybeere",
            spanish: "Baya Yapati",
            italian: "Baccapitaya",
            english: "Petaya Berry"
        }
    },
    pidgeotite: {
        num: 762
    },
    pikaniumz: {
        num: 794
    },
    pikashuniumz: {
        num: 836
    },
    pinapberry: {
        num: 337,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 3,
            size: 80,
            growthtime: 2,
            soildryness: 35,
            smoothness: 20,
            berryid: 20
        },
        names: {
            japanese: "パイルのみ",
            korean: "파인열매",
            french: "Baie Nanana",
            german: "Sananabeere",
            spanish: "Baya Pinia",
            italian: "Baccananas",
            english: "Pinap Berry"
        }
    },
    pinsirite: {
        num: 1052,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "カイロスナイト",
            korean: "쁘사이저나이트",
            french: "Scarabruite",
            german: "Pinsirnit",
            spanish: "Pinsirita",
            italian: "Pinsirite",
            english: "Pinsirite"
        }
    },
    pixieplate: {
        num: 1026,
        cost: 0,
        itemcategory: "plates",
        names: {
            japanese: "せいれいプレート",
            korean: "정령플레이트",
            french: "Plaque Pixie",
            german: "Feentafel",
            spanish: "Tabla Duende",
            italian: "Lastraspiritello",
            english: "Pixie Plate"
        }
    },
    plumefossil: {
        num: 958,
        cost: 1000,
        itemcategory: "dexcompletion",
        names: {
            japanese: "はねのカセキ",
            korean: "날개화석",
            french: "Fossile Plume",
            german: "Federfossil",
            spanish: "Fósil Pluma",
            italian: "Fossilpiuma",
            english: "Plume Fossil"
        }
    },
    poisonbarb: {
        num: 414,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "どくバリ",
            korean: "독바늘",
            french: "Pic Venin",
            german: "Giftstich",
            spanish: "Flecha Venenosa",
            italian: "Velenaculeo",
            english: "Poison Barb"
        }
    },
    poisongem: {
        num: 941,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "どくのジュエル",
            korean: "독주얼",
            french: "Joyau Poison",
            german: "Giftjuwel",
            spanish: "Gema Veneno",
            italian: "Bijouveleno",
            english: "Poison Gem"
        }
    },
    poisonmemory: {
        num: 906
    },
    poisoniumz: {
        num: 783
    },
    pokeball: {
        num: 20,
        cost: 200,
        itemcategory: "ball",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {
            japanese: "モンスターボール",
            korean: "몬스터볼",
            french: "Poké Ball",
            german: "Pokéball",
            spanish: "Poké Ball",
            italian: "Poké Ball",
            english: "Poké Ball"
        }
    },
    pomegberry: {
        num: 338,
        cost: 20,
        itemcategory: "evdropberry",
        ev: 'hp',
        berry: {
            firmness: 4,
            size: 135,
            growthtime: 8,
            soildryness: 8,
            smoothness: 20,
            berryid: 21
        },
        names: {
            japanese: "ザロクのみ",
            korean: "유석열매",
            french: "Baie Grena",
            german: "Granabeere",
            spanish: "Baya Grana",
            italian: "Baccagrana",
            english: "Pomeg Berry"
        }
    },
    poweranklet: {
        num: 462,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワーアンクル",
            korean: "파워앵클릿",
            french: "Chaîne Pouvoir",
            german: "Machtkette",
            spanish: "Franja Recia",
            italian: "Vigorgliera",
            english: "Power Anklet"
        }
    },
    powerband: {
        num: 461,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワーバンド",
            korean: "파워밴드",
            french: "Bandeau Pouvoir",
            german: "Machtband",
            spanish: "Banda Recia",
            italian: "Vigorbanda",
            english: "Power Band"
        }
    },
    powerbelt: {
        num: 459,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワーベルト",
            korean: "파워벨트",
            french: "Ceinture Pouvoir",
            german: "Machtgurt",
            spanish: "Cinto Recio",
            italian: "Vigorfascia",
            english: "Power Belt"
        }
    },
    powerbracer: {
        num: 458,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワーリスト",
            korean: "파워리스트",
            french: "Poignée Pouvoir",
            german: "Machtreif",
            spanish: "Brazal Recio",
            italian: "Vigorcerchio",
            english: "Power Bracer"
        }
    },
    powerherb: {
        num: 440,
        cost: 100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワフルハーブ",
            korean: "파워풀허브",
            french: "Herbe Pouvoir",
            german: "Energiekraut",
            spanish: "Hierba Única",
            italian: "Vigorerba",
            english: "Power Herb"
        }
    },
    powerlens: {
        num: 460,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワーレンズ",
            korean: "파워렌즈",
            french: "Lentille Pouvoir",
            german: "Machtlinse",
            spanish: "Lente Recia",
            italian: "Vigorlente",
            english: "Power Lens"
        }
    },
    powerweight: {
        num: 463,
        cost: 3000,
        itemcategory: 14,
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "パワーウエイト",
            korean: "파워웨이트",
            french: "Poids Pouvoir",
            german: "Machtgewicht",
            spanish: "Pesa Recia",
            italian: "Vigorpeso",
            english: "Power Weight"
        }
    },
    premierball: {
        num: 28,
        cost: 200,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {
            japanese: "プレミアボール",
            korean: "프레미어볼",
            french: "Honor Ball",
            german: "Premierball",
            spanish: "Honor Ball",
            italian: "Premier Ball",
            english: "Premier Ball"
        }
    },
    primariumz: {
        num: 800
    },
    protectivepads: {
        num: 880
    },
    psychicgem: {
        num: 944,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "エスパージュエル",
            korean: "에스퍼주얼",
            french: "Joyau Psy",
            german: "Psychojuwel",
            spanish: "Gema Psíquica",
            italian: "Bijoupsico",
            english: "Psychic Gem"
        }
    },
    psychicmemory: {
        num: 916
    },
    psychicseed: {
        num: 882
    },
    psychiumz: {
        num: 786
    },
    qualotberry: {
        num: 340,
        cost: 20,
        itemcategory: "evdropberry",
        ev: 'def',
        berry: {
            firmness: 3,
            size: 110,
            growthtime: 8,
            soildryness: 8,
            smoothness: 20,
            berryid: 23
        },
        names: {
            japanese: "タポルのみ",
            korean: "파비열매",
            french: "Baie Qualot",
            german: "Qualotbeere",
            spanish: "Baya Ispero",
            italian: "Baccaloquat",
            english: "Qualot Berry"
        }
    },
    quickball: {
        num: 31,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            if(battle.turn==1){
                if(battle.gen==4){
                    return target.catchrate*4;
                }
                return target.catchrate*5;
            }
            return target.catchrate*1;
        },
        names: {
            japanese: "クイックボール",
            korean: "퀵볼",
            french: "Rapide Ball",
            german: "Flottball",
            spanish: "Veloz Ball",
            italian: "Velox Ball",
            english: "Quick Ball"
        }
    },
    quickclaw: {
        num: 386,
        cost: 100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "せんせいのツメ",
            korean: "선제공격손톱",
            french: "Vive Griffe",
            german: "Flinkklaue",
            spanish: "Garra Rápida",
            italian: "Rapidartigli",
            english: "Quick Claw"
        }
    },
    quickpowder: {
        num: 443,
        cost: 10,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "スピードパウダー",
            korean: "스피드파우더",
            french: "Poudre Vite",
            german: "Flottstaub",
            spanish: "Polvo Veloz",
            italian: "Velopolvere",
            english: "Quick Powder"
        }
    },
    rabutaberry: {
        num: 346,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 2,
            size: 226,
            growthtime: 6,
            soildryness: 10,
            smoothness: 30,
            berryid: 29
        },
        names: {
            japanese: "ラブタのみ",
            korean: "라부탐열매",
            french: "Baie Rabuta",
            german: "Rabutabeere",
            spanish: "Baya Rautan",
            italian: "Baccambutan",
            english: "Rabuta Berry"
        }
    },
    rarebone: {
        num: 139,
        cost: 10000,
        itemcategory: "loot",
        underground: true,
        names: {
            japanese: "きちょうなホネ",
            korean: "귀중한뼈",
            french: "Os Rare",
            german: "Steinknochen",
            spanish: "Hueso Raro",
            italian: "Osso Raro",
            english: "Rare Bone"
        }
    },
    rawstberry: {
        num: 264,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 32,
            growthtime: 3,
            soildryness: 15,
            smoothness: 25,
            berryid: 4
        },
        names: {
            japanese: "チーゴのみ",
            korean: "복분열매",
            french: "Baie Fraive",
            german: "Fragiabeere",
            spanish: "Baya Safre",
            italian: "Baccafrago",
            english: "Rawst Berry"
        }
    },
    razorclaw: {
        num: 504,
        cost: 2100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "するどいツメ",
            korean: "예리한손톱",
            french: "Griffe Rasoir",
            german: "Scharfklaue",
            spanish: "Garra Afilada",
            italian: "Affilartigli",
            english: "Razor Claw"
        }
    },
    razorfang: {
        num: 505,
        cost: 2100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "するどいキバ",
            korean: "예리한이빨",
            french: "Croc Rasoir",
            german: "Scharfzahn",
            spanish: "Colmillo Agudo",
            italian: "Affilodente",
            english: "Razor Fang"
        }
    },
    razzberry: {
        num: 333,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 4,
            size: 120,
            growthtime: 2,
            soildryness: 35,
            smoothness: 20,
            berryid: 16
        },
        names: {
            japanese: "ズリのみ",
            korean: "라즈열매",
            french: "Baie Framby",
            german: "Himmihbeere",
            spanish: "Baya Frambu",
            italian: "Baccalampon",
            english: "Razz Berry"
        }
    },
    redcard: {
        num: 929,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "レッドカード",
            korean: "레드카드",
            french: "Carton Rouge",
            german: "Rote Karte",
            spanish: "Tarjeta Roja",
            italian: "Cartelrosso",
            english: "Red Card"
        }
    },
    redorb: {
        num: 740,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "べにいろのたま",
            korean: "주홍구슬",
            french: "Orbe Rouge",
            german: "Rote Kugel",
            spanish: "Esfera Roja",
            italian: "Sfera Rossa",
            english: "Red Orb"
        }
    },
    repeatball: {
        num: 25,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            //@FIXME: What's a smart way to properly implement 'seen' data in battles? I seriously don't know. 
            return target.catchrate;
        },
        names: {
            japanese: "リピートボール",
            korean: "리피드볼",
            french: "Bis Ball",
            german: "Wiederball",
            spanish: "Acopio Ball",
            italian: "Bis Ball",
            english: "Repeat Ball"
        }
    },
    rindoberry: {
        num: 356,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 156,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 39
        },
        names: {
            japanese: "リンドのみ",
            korean: "린드열매",
            french: "Baie Ratam",
            german: "Grindobeere",
            spanish: "Baya Tamar",
            italian: "Baccarindo",
            english: "Rindo Berry"
        }
    },
    ringtarget: {
        num: 930,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "ねらいのまと",
            korean: "겨냥표적",
            french: "Point de Mire",
            german: "Zielscheibe",
            spanish: "Blanco",
            italian: "Facilsaglio",
            english: "Ring Target"
        }
    },
    rockgem: {
        num: 946,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "いわのジュエル",
            korean: "바위주얼",
            french: "Joyau Roche",
            german: "Gesteinjuwel",
            spanish: "Gema Roca",
            italian: "Bijouroccia",
            english: "Rock Gem"
        }
    },
    rockincense: {
        num: 484,
        cost: 9600,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "がんせきおこう",
            korean: "암석향로",
            french: "Encens Roc",
            german: "Steinrauch",
            spanish: "Incienso Roca",
            italian: "Roccioaroma",
            english: "Rock Incense"
        }
    },
    rockmemory: {
        num: 908
    },
    rockiumz: {
        num: 788
    },
    rockyhelmet: {
        num: 927,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "ゴツゴツメット",
            korean: "울퉁불퉁멧",
            french: "Casque Brut",
            german: "Beulenhelm",
            spanish: "Casco Dentado",
            italian: "Bitorzolelmo",
            english: "Rocky Helmet"
        }
    },
    rootfossil: {
        num: 130,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "ねっこのカセキ",
            korean: "뿌리화석",
            french: "Fossile Racine",
            german: "Wurzelfossil",
            spanish: "Fósil Raíz",
            italian: "Radifossile",
            english: "Root Fossil"
        }
    },
    roseincense: {
        num: 487,
        cost: 9600,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "おはなのおこう",
            korean: "꽃향로",
            french: "Encens Fleur",
            german: "Rosenrauch",
            spanish: "Incienso Floral",
            italian: "Rosaroma",
            english: "Rose Incense"
        }
    },
    roseliberry: {
        num: 1065,
        cost: 0,
        itemcategory: "berryTypeProtection",
        names: {
            japanese: "ロゼルのみ",
            korean: "로셀열매",
            french: "Baie Selro",
            german: "Hibisbeere",
            spanish: "Baya Hibis",
            italian: "Baccarcadè",
            english: "Roseli Berry"
        }
    },
    rowapberry: {
        num: 381,
        cost: 20,
        itemcategory: 4,
        holdableactive: true,
        berry: {
            firmness: 1,
            size: 52,
            growthtime: 24,
            soildryness: 7,
            smoothness: 60,
            berryid: 64
        },
        names: {
            japanese: "レンブのみ",
            korean: "애터열매",
            french: "Baie Pommo",
            german: "Roselbeere",
            spanish: "Baya Magua",
            italian: "Baccaroam",
            english: "Rowap Berry"
        }
    },
    sablenite: {
        num: 754
    },
    safariball: {
        num: 21,
        cost: 0,
        itemcategory: "ball",
        countable: true,
        consumable: true,
        usablebattle: true,
        catchrate: function(battle,source,target){
            return 1.5*target.catchrate;
        },
        holdable: true,
        names: {
            japanese: "サファリボール",
            korean: "사파리볼",
            french: "Safari Ball",
            german: "Safariball",
            spanish: "Safari Ball",
            italian: "Safari Ball",
            english: "Safari Ball"
        }
    },
    safetygoggles: {
        num: 1032,
        cost: 0,
        itemcategory: "helditems",
        names: {
            japanese: "ぼうじんゴーグル",
            korean: "방진고글",
            french: "Lunettes Filtre",
            german: "Schutzbrille",
            spanish: "Gafa Protectora",
            italian: "Visierantisabbia",
            english: "Safety Goggles"
        }
    },
    salacberry: {
        num: 372,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 95,
            growthtime: 24,
            soildryness: 4,
            smoothness: 40,
            berryid: 55
        },
        names: {
            japanese: "カムラのみ",
            korean: "캄라열매",
            french: "Baie Sailak",
            german: "Salkabeere",
            spanish: "Baya Aslac",
            italian: "Baccasalak",
            english: "Salac Berry"
        }
    },
    salamencite: {
        num: 769
    },
    sceptilite: {
        num: 753
    },
    scizorite: {
        num: 1051,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "ハッサムナイト",
            korean: "핫삼나이트",
            french: "Cizayoxite",
            german: "Scheroxnit",
            spanish: "Scizorita",
            italian: "Scizorite",
            english: "Scizorite"
        }
    },
    scopelens: {
        num: 401,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "ピントレンズ",
            korean: "초점렌즈",
            french: "Lentilscope",
            german: "Scope-Linse",
            spanish: "Periscopio",
            italian: "Mirino",
            english: "Scope Lens"
        }
    },
    seaincense: {
        num: 423,
        cost: 9600,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "うしおのおこう",
            korean: "바닷물향로",
            french: "Encens Mer",
            german: "Seerauch",
            spanish: "Incienso Marino",
            italian: "Marearoma",
            english: "Sea Incense"
        }
    },
    sharpbeak: {
        num: 413,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "するどいくちばし",
            korean: "예리한부리",
            french: "Bec Pointu",
            german: "Hackattack",
            spanish: "Pico Afilado",
            italian: "Beccaffilato",
            english: "Sharp Beak"
        }
    },
    sharpedonite: {
        num: 759
    },
    shedshell: {
        num: 464,
        cost: 100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "きれいなぬけがら",
            korean: "아름다운허물",
            french: "Carapace Mue",
            german: "Wechselhülle",
            spanish: "Muda Concha",
            italian: "Disfoguscio",
            english: "Shed Shell"
        }
    },
    shellbell: {
        num: 422,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "かいがらのすず",
            korean: "조개껍질방울",
            french: "Grelot Coque",
            german: "Seegesang",
            spanish: "Campana Concha",
            italian: "Conchinella",
            english: "Shell Bell"
        }
    },
    shockdrive: {
        num: 891,
        cost: 1000,
        itemcategory: "speciesspecific",
        names: {
            japanese: "イナズマカセット",
            korean: "번개카세트",
            french: "Module Choc",
            german: "Blitzmodul",
            spanish: "FulgoROM",
            italian: "Voltmodulo",
            english: "Shock Drive"
        }
    },
    shucaberry: {
        num: 360,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 2,
            size: 42,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 43
        },
        names: {
            japanese: "シュカのみ",
            korean: "슈캐열매",
            french: "Baie Jouca",
            german: "Schukebeere",
            spanish: "Baya Acardo",
            italian: "Baccanaca",
            english: "Shuca Berry"
        }
    },
    silkscarf: {
        num: 420,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "シルクのスカーフ",
            korean: "실크스카프",
            french: "Mouchoir Soie",
            german: "Seidenschal",
            spanish: "Pañuelo Seda",
            italian: "Sciarpa Seta",
            english: "Silk Scarf"
        }
    },
    silverpowder: {
        num: 391,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "ぎんのこな",
            korean: "은빛가루",
            french: "Poudre Argentée",
            german: "Silberstaub",
            spanish: "Polvo Plata",
            italian: "Argenpolvere",
            english: "Silver Powder"
        }
    },
    sitrusberry: {
        num: 325,
        cost: 20,
        itemcategory: "berrymedicine",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 95,
            growthtime: 8,
            soildryness: 7,
            smoothness: 20,
            berryid: 10
        },
        names: {
            japanese: "オボンのみ",
            korean: "자뭉열매",
            french: "Baie Sitrus",
            german: "Tsitrubeere",
            spanish: "Baya Zidra",
            italian: "Baccacedro",
            english: "Sitrus Berry"
        }
    },
    skullfossil: {
        num: 138,
        cost: 1000,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "ずがいのカセキ",
            korean: "두개의화석",
            french: "Fossile Crâne",
            german: "Kopffossil",
            spanish: "Fósil Cráneo",
            italian: "Fossilcranio",
            english: "Skull Fossil"
        }
    },
    skyplate: {
        num: 475,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "あおぞらプレート",
            korean: "푸른하늘플레이트",
            french: "Plaque Ciel",
            german: "Wolkentafel",
            spanish: "Tabla Cielo",
            italian: "Lastracielo",
            english: "Sky Plate"
        }
    },
    slowbronite: {
        num: 760
    },
    smoothrock: {
        num: 452,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "さらさらいわ",
            korean: "보송보송바위",
            french: "Roche Lisse",
            german: "Glattbrocken",
            spanish: "Roca Suave",
            italian: "Roccialiscia",
            english: "Smooth Rock"
        }
    },
    snorliumz: {
        num: 804
    },
    snowball: {
        num: 1031,
        cost: 0,
        itemcategory: "helditems",
        names: {
            japanese: "ゆきだま",
            korean: "눈덩이",
            french: "Boule de Neige",
            german: "Schneeball",
            spanish: "Bola de Nieve",
            italian: "Palla di neve",
            english: "Snowball"
        }
    },
    softsand: {
        num: 406,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "やわらかいすな",
            korean: "부드러운모래",
            french: "Sable Doux",
            german: "Pudersand",
            spanish: "Arena Fina",
            italian: "Sabbia Soffice",
            english: "Soft Sand"
        }
    },
    souldew: {
        num: 394,
        cost: 200,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "こころのしずく",
            korean: "마음의물방울",
            french: "Rosée Âme",
            german: "Seelentau",
            spanish: "Rocío Bondad",
            italian: "Cuorugiada",
            english: "Soul Dew"
        }
    },
    spelltag: {
        num: 416,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "のろいのおふだ",
            korean: "저주의부적",
            french: "Rune Sort",
            german: "Bannsticker",
            spanish: "Hechizo",
            italian: "Spettrotarga",
            english: "Spell Tag"
        }
    },
    spelonberry: {
        num: 348,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 2,
            size: 133,
            growthtime: 15,
            soildryness: 8,
            smoothness: 35,
            berryid: 31
        },
        names: {
            japanese: "ノワキのみ",
            korean: "메호키열매",
            french: "Baie Kiwan",
            german: "Kiwanbeere",
            spanish: "Baya Wikano",
            italian: "Baccamelos",
            english: "Spelon Berry"
        }
    },
    splashplate: {
        num: 468,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "しずくプレート",
            korean: "물방울플레이트",
            french: "Plaque Hydro",
            german: "Wassertafel",
            spanish: "Tabla Linfa",
            italian: "Lastraidro",
            english: "Splash Plate"
        }
    },
    spookyplate: {
        num: 479,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "もののけプレート",
            korean: "원령플레이트",
            french: "Plaque Fantô",
            german: "Spuktafel",
            spanish: "Tabla Terror",
            italian: "Lastratetra",
            english: "Spooky Plate"
        }
    },
    sportball: {
        num: 730,
        cost: 0,
        itemcategory: "ball",
        catchrate: function(battle,source,target){
            return target.catchrate;
        },
        names: {            
            japanese: "コンペボール",
            korean: "콤페볼",
            french: "Compét'Ball",
            german: "Turnierball",
            spanish: "Competi Ball",
            italian: "Gara Ball",
            english: "Sport Ball"
        }
    },
    starfberry: {
        num: 376,
        cost: 20,
        itemcategory: "berryother",
        holdableactive: true,
        berry: {
            firmness: 5,
            size: 153,
            growthtime: 24,
            soildryness: 4,
            smoothness: 50,
            berryid: 59
        },
        names: {
            japanese: "スターのみ",
            korean: "스타열매",
            french: "Baie Frista",
            german: "Krambobeere",
            spanish: "Baya Arabol",
            italian: "Baccambola",
            english: "Starf Berry"
        }
    },
    steelixite: {
        num: 761
    },
    steelgem: {
        num: 949,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "はがねのジュエル",
            korean: "강철주얼",
            french: "Joyau Acier",
            german: "Stahljuwel",
            spanish: "Gema Acero",
            italian: "Bijouacciaio",
            english: "Steel Gem"
        }
    },
    steelmemory: {
        num: 911
    },
    steeliumz: {
        num: 792
    },
    stick: {
        num: 428,
        cost: 200,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "ながねぎ",
            korean: "대파",
            french: "Bâton",
            german: "Lauchstange",
            spanish: "Palo",
            italian: "Gambo",
            english: "Stick"
        }
    },
    stickybarb: {
        num: 457,
        cost: 200,
        itemcategory: "effortitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "くっつきバリ",
            korean: "끈적끈적바늘",
            french: "Piquants",
            german: "Klettdorn",
            spanish: "Toxiestrella",
            italian: "Vischiopunta",
            english: "Sticky Barb"
        }
    },
    stoneplate: {
        num: 478,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "がんせきプレート",
            korean: "암석플레이트",
            french: "Plaque Roc",
            german: "Steintafel",
            spanish: "Tabla Pétrea",
            italian: "Lastrapietra",
            english: "Stone Plate"
        }
    },
    swampertite: {
        num: 752
    },
    tamatoberry: {
        num: 343,
        cost: 20,
        itemcategory: "evdropberry",
        ev: 'spe',
        berry: {
            firmness: 2,
            size: 200,
            growthtime: 8,
            soildryness: 8,
            smoothness: 30,
            berryid: 26
        },
        names: {
            japanese: "マトマのみ",
            korean: "토망열매",
            french: "Baie Tamato",
            german: "Tamotbeere",
            spanish: "Baya Tamate",
            italian: "Baccamodoro",
            english: "Tamato Berry"
        }
    },
    tangaberry: {
        num: 363,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 1,
            size: 42,
            growthtime: 18,
            soildryness: 6,
            smoothness: 35,
            berryid: 46
        },
        names: {
            japanese: "タンガのみ",
            korean: "리체열매",
            french: "Baie Panga",
            german: "Tanigabeere",
            spanish: "Baya Yecana",
            italian: "Baccaitan",
            english: "Tanga Berry"
        }
    },
    tapuniumz: {
        num: 801
    },
    terrainextender: {
        num: 879
    },
    thickclub: {
        num: 427,
        cost: 500,
        itemcategory: "speciesspecific",
        holdable: true,
        names: {
            japanese: "ふといホネ",
            korean: "굵은뼈",
            french: "Masse Os",
            german: "Kampfknochen",
            spanish: "Hueso Grueso",
            italian: "Ossospesso",
            english: "Thick Club"
        }
    },
    timerball: {
        num: 26,
        cost: 1000,
        itemcategory: 33,
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            if(battle.gen==7){
                return target.catchrate*Math.min(1+0.3*battle.turn,4); 
            }

            if(battle.gen>=5){
                return target.catchrate*Math.min(1+battle.turn*1229/4096,4);
            }

            return target.catchrate*Math.min((battle.turn+10)/10)
        },
        names: {
            japanese: "タイマーボール",
            korean: "타이마볼",
            french: "Chrono Ball",
            german: "Timerball",
            spanish: "Turno Ball",
            italian: "Timer Ball",
            english: "Timer Ball"
        }
    },
    toxicorb: {
        num: 441,
        cost: 100,
        itemcategory: "effortitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "どくどくだま",
            korean: "맹독구슬",
            french: "Orbe Toxique",
            german: "Toxik-Orb",
            spanish: "Toxisfera",
            italian: "Tossicsfera",
            english: "Toxic Orb"
        }
    },
    toxicplate: {
        num: 473,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "もうどくプレート",
            korean: "맹독플레이트",
            french: "Plaque Toxik",
            german: "Gifttafel",
            spanish: "Tabla Tóxica",
            italian: "Lastrafiele",
            english: "Toxic Plate"
        }
    },
    twistedspoon: {
        num: 417,
        cost: 100,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "まがったスプーン",
            korean: "휘어진스푼",
            french: "Cuiller Tordue",
            german: "Krummlöffel",
            spanish: "Cuchara Torcida",
            italian: "Cucchiaio Torto",
            english: "Twisted Spoon"
        }
    },
    tyranitarite: {
        num: 1050,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "バンギラスナイト",
            korean: "마기라스나이트",
            french: "Tyranocivite",
            german: "Despotarnit",
            spanish: "Tyranitarita",
            italian: "Tyranitarite",
            english: "Tyranitarite"
        }
    },
    ultraball: {
        num: 18,
        cost: 1200,
        itemcategory: "ball",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        catchrate: function(battle,source,target){
            return 2*target.catchrate;
        },
        names: {
            japanese: "ハイパーボール",
            korean: "하이퍼볼",
            french: "Hyper Ball",
            german: "Hyperball",
            spanish: "Ultra Ball",
            italian: "Ultra Ball",
            english: "Ultra Ball"
        }
    },
    venusaurite: {
        num: 1040,
        cost: 0,
        itemcategory: "megastone",
        names: {
            japanese: "フシギバナイト",
            korean: "이상해꽃나이트",
            french: "Florizarrite",
            german: "Bisaflornit",
            spanish: "Venusaurita",
            italian: "Venusaurite",
            english: "Venusaurite"
        }
    },
    wacanberry: {
        num: 355,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 1,
            size: 250,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 38
        },
        names: {
            japanese: "ソクノのみ",
            korean: "초나열매",
            french: "Baie Parma",
            german: "Kerzalbeere",
            spanish: "Baya Gualot",
            italian: "Baccaparmen",
            english: "Wacan Berry"
        }
    },
    watergem: {
        num: 936,
        cost: 200,
        itemcategory: "jewels",
        names: {
            japanese: "みずのジュエル",
            korean: "물주얼",
            french: "Joyau Eau",
            german: "Wasserjuwel",
            spanish: "Gema Agua",
            italian: "Bijouacqua",
            english: "Water Gem"
        }
    },
    watermemory: {
        num: 913
    },
    wateriumz: {
        num: 778
    },
    watmelberry: {
        num: 350,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 2,
            size: 250,
            growthtime: 15,
            soildryness: 8,
            smoothness: 35,
            berryid: 33
        },
        names: {
            japanese: "カイスのみ",
            korean: "슈박열매",
            french: "Baie Stekpa",
            german: "Wasmelbeere",
            spanish: "Baya Sambia",
            italian: "Baccacomero",
            english: "Watmel Berry"
        }
    },
    waveincense: {
        num: 486,
        cost: 9600,
        itemcategory: "typeenhancement",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "さざなみのおこう",
            korean: "잔물결향로",
            french: "Encens Vague",
            german: "Wellenrauch",
            spanish: "Incienso Aqua",
            italian: "Ondaroma",
            english: "Wave Incense"
        }
    },
    weaknesspolicy: {
        num: 1024,
        cost: 0,
        itemcategory: "helditems",
        names: {
            japanese: "じゃくてんほけん",
            korean: "약점보험",
            french: "Vulné-Assurance",
            german: "Schwächenschutz",
            spanish: "Seguro Debilidad",
            italian: "Vulneropolizza",
            english: "Weakness Policy"
        }
    },
    wepearberry: {
        num: 336,
        cost: 20,
        itemcategory: "baking",
        berry: {
            firmness: 5,
            size: 74,
            growthtime: 2,
            soildryness: 35,
            smoothness: 20,
            berryid: 19
        },
        names: {
            japanese: "セシナのみ",
            korean: "서배열매",
            french: "Baie Repoi",
            german: "Nirbebeere",
            spanish: "Baya Peragu",
            italian: "Baccapera",
            english: "Wepear Berry"
        }
    },
    whiteherb: {
        num: 383,
        cost: 100,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "しろいハーブ",
            korean: "하양허브",
            french: "Herbe Blanche",
            german: "Schlohkraut",
            spanish: "Hierba Blanca",
            italian: "Erbachiara",
            english: "White Herb"
        }
    },
    widelens: {
        num: 434,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "こうかくレンズ",
            korean: "광각렌즈",
            french: "Loupe",
            german: "Großlinse",
            spanish: "Lupa",
            italian: "Grandelente",
            english: "Wide Lens"
        }
    },
    wikiberry: {
        num: 329,
        cost: 20,
        itemcategory: "berrypicky",
        holdableactive: true,
        berry: {
            firmness: 3,
            size: 115,
            growthtime: 5,
            soildryness: 10,
            smoothness: 25,
            berryid: 12
        },
        names: {
            japanese: "ウイのみ",
            korean: "위키열매",
            french: "Baie Wiki",
            german: "Wikibeere",
            spanish: "Baya Wiki",
            italian: "Baccakiwi",
            english: "Wiki Berry"
        }
    },
    wiseglasses: {
        num: 436,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "ものしりメガネ",
            korean: "박식안경",
            french: "Lunettes Sages",
            german: "Schlauglas",
            spanish: "Gafas Especiales",
            italian: "Saviocchiali",
            english: "Wise Glasses"
        }
    },
    yacheberry: {
        num: 357,
        cost: 20,
        itemcategory: "berryTypeProtection",
        holdableactive: true,
        berry: {
            firmness: 4,
            size: 135,
            growthtime: 18,
            soildryness: 6,
            smoothness: 30,
            berryid: 40
        },
        names: {
            japanese: "ヤチェのみ",
            korean: "플카열매",
            french: "Baie Nanone",
            german: "Kiroyabeere",
            spanish: "Baya Rimoya",
            italian: "Baccamoya",
            english: "Yache Berry"
        }
    },
    zapplate: {
        num: 469,
        cost: 1000,
        itemcategory: "plates",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "いかずちプレート",
            korean: "우뢰플레이트",
            french: "Plaque Volt",
            german: "Blitztafel",
            spanish: "Tabla Trueno",
            italian: "Lastrasaetta",
            english: "Zap Plate"
        }
    },
    zoomlens: {
        num: 445,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "フォーカスレンズ",
            korean: "포커스렌즈",
            french: "Lentille Zoom",
            german: "Zoomlinse",
            spanish: "Telescopio",
            italian: "Zoomlente",
            english: "Zoom Lens"
        }
    },
    berserkgene: {},
    berry: {
        num: 155
    },
    bitterberry: {
        num: 156
    },
    burntberry: {
        num: 153
    },
    dragonscale: {
        num: 404,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "りゅうのウロコ",
            korean: "용의비늘",
            french: "Écaille Draco",
            german: "Drachenhaut",
            spanish: "Escamadragón",
            italian: "Squama Drago",
            english: "Dragon Scale"
        }
    },
    goldberry: {
        num: 158
    },
    iceberry: {
        num: 152
    },
    mintberry: {
        num: 150
    },
    miracleberry: {
        num: 157
    },
    mysteryberry: {
        num: 154
    },
    pinkbow: {
        num: 251
    },
    polkadotbow: {
        num: 251
    },
    przcureberry: {
        num: 149
    },
    psncureberry: {
        num: 151
    },
    crucibellite: {
        num: -1
    },
    potion: {
        num: 33,
        cost: 300,
        itemcategory: "healing",
        heal: 20,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "キズぐすり",
            korean: "상처약",
            french: "Potion",
            german: "Trank",
            spanish: "Poción",
            italian: "Pozione",
            english: "Potion"
        }
    },
    antidote: {
        num: 35,
        cost: 100,
        itemcategory: "healing",
        status: "psn",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "どくけし",
            korean: "해독제",
            french: "Antidote",
            german: "Gegengift",
            spanish: "Antídoto",
            italian: "Antidoto",
            english: "Antidote"
        }
    },
    burnheal: {
        num: 36,
        cost: 250,
        itemcategory: "healing",
        status: "brn",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "やけどなおし",
            korean: "화상치료제",
            french: "Anti-Brûle",
            german: "Feuerheiler",
            spanish: "Antiquemar",
            italian: "Antiscottatura",
            english: "Burn Heal"
        }
    },
    iceheal: {
        num: 37,
        cost: 250,
        itemcategory: "healing",
        status: "frz",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "こおりなおし",
            korean: "얼음상태치료제",
            french: "Antigel",
            german: "Eisheiler",
            spanish: "Antihielo",
            italian: "Antigelo",
            english: "Ice Heal"
        }
    },
    awakening: {
        num: 38,
        cost: 250,
        itemcategory: "healing",
        status: "slp",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ねむけざまし",
            korean: "잠깨는약",
            french: "Réveil",
            german: "Aufwecker",
            spanish: "Despertar",
            italian: "Sveglia",
            english: "Awakening"
        }
    },
    paralyzeheal: {
        num: 39,
        cost: 200,
        itemcategory: "healing",
        status: "par",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "まひなおし",
            korean: "마비치료제",
            french: "Anti-Para",
            german: "Para-Heiler",
            spanish: "Antiparalizador",
            italian: "Antiparalisi",
            english: "Paralyze Heal"
        }
    },
    fullrestore: {
        num: 40,
        cost: 3000,
        itemcategory: "healing",
        heal: -1,
        status: "all",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "かいふくのくすり",
            korean: "회복약",
            french: "Guérison",
            german: "Top-Genesung",
            spanish: "Restaurar Todo",
            italian: "Ricarica Totale",
            english: "Full Restore"
        }
    },
    maxpotion: {
        num: 41,
        cost: 2500,
        itemcategory: "healing",
        heal: -1,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "まんたんのくすり",
            korean: "풀회복약",
            french: "Potion Max",
            german: "Top-Trank",
            spanish: "Poción Máxima",
            italian: "Pozione Max",
            english: "Max Potion"
        }
    },
    hyperpotion: {
        num: 42,
        cost: 1200,
        itemcategory: "healing",
        heal: 200,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "すごいキズぐすり",
            korean: "고급상처약",
            french: "Hyper Potion",
            german: "Hypertrank",
            spanish: "Hiperpoción",
            italian: "Iperpozione",
            english: "Hyper Potion"
        }
    },
    superpotion: {
        num: 44,
        cost: 700,
        itemcategory: "healing",
        heal: 150,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "いいキズぐすり",
            korean: "좋은상처약",
            french: "Super Potion",
            german: "Supertrank",
            spanish: "Superpoción",
            italian: "Superpozione",
            english: "Super Potion"
        }
    },
    fullheal: {
        num: 45,
        cost: 600,
        itemcategory: "healing",
        status: "all",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "なんでもなおし",
            korean: "만병통치제",
            french: "Total Soin",
            german: "Hyperheiler",
            spanish: "Cura Total",
            italian: "Cura Totale",
            english: "Full Heal"
        }
    },
    revive: {
        num: 46,
        cost: 1500,
        itemcategory: "healing",
        revive: 0.5,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        underground: true,
        names: {
            japanese: "げんきのかけら",
            korean: "기력의조각",
            french: "Rappel",
            german: "Beleber",
            spanish: "Revivir",
            italian: "Revitalizzante",
            english: "Revive"
        }
    },
    maxrevive: {
        num: 47,
        cost: 4000,
        itemcategory: "healing",
        revive: 1,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        underground: true,
        names: {
            japanese: "げんきのかたまり",
            korean: "기력의덩어리",
            french: "Rappel Max",
            german: "Top-Beleber",
            spanish: "Revivir Máximo",
            italian: "Revitalizz. Max",
            english: "Max Revive"
        }
    },
    freshwater: {
        num: 48,
        cost: 200,
        itemcategory: "healing",
        heal: 30,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "おいしいみず",
            korean: "맛있는물",
            french: "Eau Fraîche",
            german: "Tafelwasser",
            spanish: "Agua Fresca",
            italian: "Acqua Fresca",
            english: "Fresh Water"
        }
    },
    sodapop: {
        num: 49,
        cost: 300,
        itemcategory: "healing",
        heal: 50,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "サイコソーダ",
            korean: "미네랄사이다",
            french: "Soda Cool",
            german: "Sprudel",
            spanish: "Refresco",
            italian: "Gassosa",
            english: "Soda Pop"
        }
    },
    lemonade: {
        num: 50,
        cost: 350,
        itemcategory: "healing",
        heal: 70,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ミックスオレ",
            korean: "후르츠밀크",
            french: "Limonade",
            german: "Limonade",
            spanish: "Limonada",
            italian: "Lemonsucco",
            english: "Lemonade"
        }
    },
    moomoomilk: {
        num: 51,
        cost: 500,
        itemcategory: "healing",
        heal: 100,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "モーモーミルク",
            korean: "튼튼밀크",
            french: "Lait Meumeu",
            german: "Kuhmuh-Milch",
            spanish: "Leche Mu-mu",
            italian: "Latte Mumu",
            english: "Moomoo Milk"
        }
    },
    energyroot: {
        num: 53,
        cost: 800,
        itemcategory: "healing",
        heal: 120,
        herb: true,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ちからのねっこ",
            korean: "힘의뿌리",
            french: "Racinénergie",
            german: "Kraftwurzel",
            spanish: "Raíz Energía",
            italian: "Radicenergia",
            english: "Energy Root"
        }
    },
    healpowder: {
        num: 54,
        cost: 450,
        itemcategory: "healing",
        status: "all",
        herb: true,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ばんのうごな",
            korean: "만능가루",
            french: "Poudre Soin",
            german: "Heilpuder",
            spanish: "Polvo Curación",
            italian: "Polvocura",
            english: "Heal Powder"
        }
    },
    revivalherb: {
        num: 55,
        cost: 2800,
        itemcategory: "healing",
        revive: 1,
        herb: true,
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ふっかつそう",
            korean: "부활초",
            french: "Herbe Rappel",
            german: "Vitalkraut",
            spanish: "Hierba Revivir",
            italian: "Vitalerba",
            english: "Revival Herb"
        }
    },
    ether: {
        num: 56,
        cost: 1200,
        itemcategory: "pprecovery",
        target: "move",
        use: function(pokemon,move){
            move.pp = Math.min(move.maxpp,move.pp+10);
        },
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ピーピーエイド",
            korean: "PP에이드",
            french: "Huile",
            german: "Äther",
            spanish: "Éter",
            italian: "Etere",
            english: "Ether"
        }
    },
    maxether: {
        num: 57,
        cost: 2000,
        itemcategory: "pprecovery",
        target: "move",
        use: function(pokemon,move){
            move.pp = move.maxpp;
        },

        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ピーピーリカバー",
            korean: "PP회복",
            french: "Huile Max",
            german: "Top-Äther",
            spanish: "Éter Máximo",
            italian: "Etere Max",
            english: "Max Ether"
        }
    },
    elixir: {
        num: 58,
        cost: 3000,
        itemcategory: "pprecovery",
        target: "party",
        use: function(pokemon){
            for(let i in pokemon.moves){
                pokemon.moves[i].pp = Math.min(pokemon.moves[i].maxpp,pokemon.moves[i].pp+10);
            }
        },
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ピーピーエイダー",
            korean: "PP에이더",
            french: "Élixir",
            german: "Elixier",
            spanish: "Elixir",
            italian: "Elisir",
            english: "Elixir"
        }
    },
    maxelixir: {
        num: 59,
        cost: 4500,
        itemcategory: "pprecovery",
        target: "party",
        use: function(pokemon){
            for(let i in pokemon.moves){
                pokemon.moves[i].pp = moves[i].maxpp
            }
        },
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ピーピーマックス",
            korean: "PP맥스",
            french: "Max Élixir",
            german: "Top-Elixier",
            spanish: "Elixir Máximo",
            italian: "Elisir Max",
            english: "Max Elixir"
        }
    },
    lavacookie: {
        num: 60,
        cost: 200,
        itemcategory: "healing",
        status: "all",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "フエンせんべい",
            korean: "용암전병",
            french: "Lava Cookie",
            german: "Lavakeks",
            spanish: "Galleta Lava",
            italian: "Lavottino",
            english: "Lava Cookie"
        }
    },
    sacredash: {
        num: 62,
        cost: 200,
        itemcategory: "healing",
        target: ['none'],
        useOw(trainer){
            //@fixme
            for(let i in trainer.party){
                trainer.party[i].heal(-1);
            }
        },

        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "せいなるはい",
            korean: "성스러운분말",
            french: "Cendre Sacrée",
            german: "Zauberasche",
            spanish: "Ceniza Sagrada",
            italian: "Ceneremagica",
            english: "Sacred Ash"
        }
    },
    hpup: {
        num: 63,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "hp",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "マックスアップ",
            korean: "맥스업",
            french: "PV Plus",
            german: "KP-Plus",
            spanish: "Más PS",
            italian: "PS-Su",
            english: "HP Up"
        }
    },
    protein: {
        num: 64,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "atk",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "タウリン",
            korean: "타우린",
            french: "Protéine",
            german: "Protein",
            spanish: "Proteína",
            italian: "Proteina",
            english: "Protein"
        }
    },
    iron: {
        num: 65,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "def",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ブロムヘキシン",
            korean: "사포닌",
            french: "Fer",
            german: "Eisen",
            spanish: "Hierro",
            italian: "Ferro",
            english: "Iron"
        }
    },
    carbos: {
        num: 66,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "spe",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "インドメタシン",
            korean: "알칼로이드",
            french: "Carbone",
            german: "Carbon",
            spanish: "Carburante",
            italian: "Carburante",
            english: "Carbos"
        }
    },
    calcium: {
        num: 67,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "spa",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "リゾチウム",
            korean: "리보플라빈",
            french: "Calcium",
            german: "Kalzium",
            spanish: "Calcio",
            italian: "Calcio",
            english: "Calcium"
        }
    },
    rarecandy: {
        num: 68,
        cost: 4800,
        itemcategory: "vitamins",
        useOw: function(trainer,pokemon){
            pokemon.levelup();
        },
        stat: "level",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ふしぎなアメ",
            korean: "이상한사탕",
            french: "Super Bonbon",
            german: "Sonderbonbon",
            spanish: "Caramelo Raro",
            italian: "Caramella Rara",
            english: "Rare Candy"
        }
    },
    ppup: {
        num: 69,
        cost: 9800,
        itemcategory: "vitamins",
        targets: ['partyMove'],
        useOw(trainer,pokemon,move){
            //@fixme
        },
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ポイントアップ",
            korean: "포인트업",
            french: "PP Plus",
            german: "AP-Plus",
            spanish: "Más PP",
            italian: "PP-Su",
            english: "PP Up"
        }
    },
    zinc: {
        num: 70,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "spd",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "キトサン",
            korean: "키토산",
            french: "Zinc",
            german: "Zink",
            spanish: "Zinc",
            italian: "Zinco",
            english: "Zinc"
        }
    },
    ppmax: {
        num: 71,
        cost: 9800,
        itemcategory: "vitamins",
        stat: "ppmax",
        targets: ['partyMove'],
        useOw(trainer,pokemon,move){
            //@fixme
        },
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ポイントマックス",
            korean: "포인트맥스",
            french: "PP Max",
            german: "AP-Top",
            spanish: "PP Máximos",
            italian: "PP-Max",
            english: "PP Max"
        }
    },
    oldgateau: {
        num: 72,
        cost: 200,
        itemcategory: "healing",
        status: "all",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "もりのヨウカン",
            korean: "숲의양갱",
            french: "Vieux Gâteau",
            german: "Spezialität",
            spanish: "Barrita Plus",
            italian: "Dolce Gateau",
            english: "Old Gateau"
        }
    },
    guardspec: {
        num: 73,
        cost: 700,
        itemcategory: "statboosts",
        stat: "guard",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "エフェクトガード",
            korean: "이펙트가드",
            french: "Défense Spéciale",
            german: "Megablock",
            spanish: "Protec. Especial",
            italian: "Superguardia",
            english: "Guard Spec."
        }
    },
    direhit: {
        num: 74,
        cost: 650,
        stat: "crit",
        itemcategory: "statboosts",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "クリティカット",
            korean: "크리티컬커터",
            french: "Muscle +",
            german: "Angriffplus",
            spanish: "Directo",
            italian: "Supercolpo",
            english: "Dire Hit"
        }
    },
    xattack: {
        num: 75,
        cost: 500,
        itemcategory: "statboosts",
        stat: 'atk',
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "プラスパワー",
            korean: "플러스파워",
            french: "Attaque +",
            german: "X-Angriff",
            spanish: "Ataque X",
            italian: "Attacco X",
            english: "X Attack"
        }
    },
    xdefense: {
        num: 76,
        cost: 550,
        itemcategory: "statboosts",
        stat: 'def',
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ディフェンダー",
            korean: "디펜드업",
            french: "Défense +",
            german: "X-Abwehr",
            spanish: "Defensa X",
            italian: "Difesa X",
            english: "X Defense"
        }
    },
    xspeed: {
        num: 77,
        cost: 350,
        itemcategory: "statboosts",
        stat: 'spe',
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "スピーダー",
            korean: "스피드업",
            french: "Vitesse +",
            german: "X-Tempo",
            spanish: "Velocidad X",
            italian: "Velocità X",
            english: "X Speed"
        }
    },
    xaccuracy: {
        num: 78,
        cost: 950,
        itemcategory: "statboosts",
        stat: 'acc',
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ヨクアタール",
            korean: "잘-맞히기",
            french: "Précision +",
            german: "X-Treffer",
            spanish: "Precisión X",
            italian: "Precisione X",
            english: "X Accuracy"
        }
    },
    xspatk: {
        num: 79,
        cost: 350,
        itemcategory: "statboosts",
        stat: 'spa',
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "スペシャルアップ",
            korean: "스페셜업",
            french: "Spécial +",
            german: "X-Spezial",
            spanish: "Especial X",
            italian: "Special X",
            english: "X Sp. Atk"
        }
    },
    xspdef: {
        num: 80,
        cost: 350,
        itemcategory: "statboosts",
        stat: 'spd',
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "スペシャルガード",
            korean: "스페셜가드",
            french: "Déf. Spé. +",
            german: "X-SpezialVer",
            spanish: "Def. Especial X",
            italian: "Dif. Speciale X",
            english: "X Sp. Def"
        }
    },
    pokedoll: {
        num: 81,
        cost: 1000,
        itemcategory: "escapeitem",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "ピッピにんぎょう",
            korean: "삐삐인형",
            french: "Poképoupée",
            german: "Poképuppe",
            spanish: "Poké Muñeco",
            italian: "Poké Bambola",
            english: "Poké Doll"
        }
    },
    fluffytail: {
        num: 82,
        cost: 1000,
        itemcategory: "escapeitem",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "エネコのシッポ",
            korean: "에나비꼬리",
            french: "Queue Skitty",
            german: "Eneco-Rute",
            spanish: "Cola Skitty",
            italian: "Coda Skitty",
            english: "Fluffy Tail"
        }
    },
    blueflute: {
        num: 83,
        cost: 100,
        itemcategory: "flutes",
        countable: true,
        consumable: true,
        usableoverworld: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "あおいビードロ",
            korean: "파랑비드로",
            french: "Flûte Bleue",
            german: "Blaue Flöte",
            spanish: "Flauta Azul",
            italian: "Flauto Blu",
            english: "Blue Flute"
        }
    },
    yellowflute: {
        num: 84,
        cost: 200,
        itemcategory: "flutes",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "きいろビードロ",
            korean: "노랑비드로",
            french: "Flûte Jaune",
            german: "Gelbe Flöte",
            spanish: "Flauta Amarilla",
            italian: "Flauto Giallo",
            english: "Yellow Flute"
        }
    },
    redflute: {
        num: 85,
        cost: 300,
        itemcategory: "flutes",
        countable: true,
        consumable: true,
        usablebattle: true,
        holdable: true,
        names: {
            japanese: "あかいビードロ",
            korean: "빨강비드로",
            french: "Flûte Rouge",
            german: "Rote Flöte",
            spanish: "Flauta Roja",
            italian: "Flauto Rosso",
            english: "Red Flute"
        }
    },
    blackflute: {
        num: 86,
        cost: 400,
        itemcategory: "repelflute",
        amount: 50,
        countable: true,
        consumable: true,
        usableoverworld: true,
        holdable: true,
        names: {
            japanese: "くろいビードロ",
            korean: "검정비드로",
            french: "Flûte Noire",
            german: "Schw. Flöte",
            spanish: "Flauta Negra",
            italian: "Flauto Nero",
            english: "Black Flute"
        }
    },
    whiteflute: {
        num: 87,
        cost: 500,
        itemcategory: "repelflute",
        amount: -50,
        countable: true,
        consumable: true,
        usableoverworld: true,
        holdable: true,
        names: {
            japanese: "しろいビードロ",
            korean: "하양비드로",
            french: "Flûte Blanche",
            german: "Weiße Flöte",
            spanish: "Flauta Blanca",
            italian: "Flauto Bianco",
            english: "White Flute"
        }
    },
    shoalsalt: {
        num: 88,
        cost: 20,
        itemcategory: "collectibles",
        countable: true,
        names: {
            japanese: "あさせのしお",
            korean: "여울소금",
            french: "Sel Tréfonds",
            german: "Küstensalz",
            spanish: "Sal Cardumen",
            italian: "Sale Ondoso",
            english: "Shoal Salt"
        }
    },
    shoalshell: {
        num: 89,
        cost: 20,
        itemcategory: "collectibles",
        countable: true,
        names: {
            japanese: "あさせのかいがら",
            korean: "여울조개껍질",
            french: "CoquilleTréfonds",
            german: "Küstenschale",
            spanish: "Concha Cardumen",
            italian: "Gusciondoso",
            english: "Shoal Shell"
        }
    },
    redshard: {
        num: 90,
        cost: 200,
        itemcategory: "collectibles",
        underground: true,
        names: {
            japanese: "あかいかけら",
            korean: "빨강조각",
            french: "Tesson Rouge",
            german: "Purpurstück",
            spanish: "Parte Roja",
            italian: "Coccio Rosso",
            english: "Red Shard"
        }
    },
    blueshard: {
        num: 91,
        cost: 200,
        itemcategory: "collectibles",
        underground: true,
        names: {
            japanese: "あおいかけら",
            korean: "파랑조각",
            french: "Tesson Bleu",
            german: "Indigostück",
            spanish: "Parte Azul",
            italian: "Coccio Blu",
            english: "Blue Shard"
        }
    },
    yellowshard: {
        num: 92,
        cost: 200,
        itemcategory: "collectibles",
        underground: true,
        names: {
            japanese: "きいろいかけら",
            korean: "노랑조각",
            french: "Tesson Jaune",
            german: "Gelbstück",
            spanish: "Parte Amarilla",
            italian: "Coccio Giallo",
            english: "Yellow Shard"
        }
    },
    greenshard: {
        num: 93,
        cost: 200,
        itemcategory: "collectibles",
        underground: true,
        names: {
            japanese: "みどりのかけら",
            korean: "초록조각",
            french: "Tesson Vert",
            german: "Grünstück",
            spanish: "Parte Verde",
            italian: "Coccio Verde",
            english: "Green Shard"
        }
    },
    superrepel: {
        num: 94,
        cost: 500,
        itemcategory: "repels",
        steps: 200,
        names: {
            japanese: "シルバースプレー",
            korean: "실버스프레이",
            french: "Superepousse",
            german: "Superschutz",
            spanish: "Superrepelente",
            italian: "Superrepellente",
            english: "Super Repel"
        }
    },
    maxrepel: {
        num: 95,
        cost: 700,
        steps: 250,
        itemcategory: "repels",
        names: {
            japanese: "ゴールドスプレー",
            korean: "골드스프레이",
            french: "Max Repousse",
            german: "Top-Schutz",
            spanish: "Repelente Máximo",
            italian: "Repellente Max",
            english: "Max Repel"
        }
    },
    escaperope: {
        num: 96,
        cost: 550,
        itemcategory: "escaperope",
        names: {
            japanese: "あなぬけのヒモ",
            korean: "동굴탈출로프",
            french: "Corde Sortie",
            german: "Fluchtseil",
            spanish: "Cuerda Huida",
            italian: "Fune di Fuga",
            english: "Escape Rope"
        }
    },
    repel: {
        num: 97,
        cost: 350,
        itemcategory: "repels",
        steps: 100,
        names: {
            japanese: "むしよけスプレー",
            korean: "벌레회피스프레이",
            french: "Repousse",
            german: "Schutz",
            spanish: "Repelente",
            italian: "Repellente",
            english: "Repel"
        }
    },
    sunstone: {
        num: 98,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        underground: true,
        names: {
            japanese: "たいようのいし",
            korean: "태양의돌",
            french: "Pierre Soleil",
            german: "Sonnenstein",
            spanish: "Piedra Solar",
            italian: "Pietrasolare",
            english: "Sun Stone"
        }
    },
    moonstone: {
        num: 107,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        underground: true,
        names: {
            japanese: "つきのいし",
            korean: "달의돌",
            french: "Pierre Lune",
            german: "Mondstein",
            spanish: "Piedra Lunar",
            italian: "Pietralunare",
            english: "Moon Stone"
        }
    },
    firestone: {
        num: 108,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        underground: true,
        names: {
            japanese: "ほのおのいし",
            korean: "불꽃의돌",
            french: "Pierre Feu",
            german: "Feuerstein",
            spanish: "Piedra Fuego",
            italian: "Pietrafocaia",
            english: "Fire Stone"
        }
    },
    thunderstone: {
        num: 109,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,

        underground: true,
        names: {
            japanese: "かみなりのいし",
            korean: "천둥의돌",
            french: "Pierre Foudre",
            german: "Donnerstein",
            spanish: "Piedra Trueno",
            italian: "Pietratuono",
            english: "Thunder Stone"
        }
    },
    waterstone: {
        num: 110,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        underground: true,
        names: {
            japanese: "みずのいし",
            korean: "물의돌",
            french: "Pierre Eau",
            german: "Wasserstein",
            spanish: "Piedra Agua",
            italian: "Pietraidrica",
            english: "Water Stone"
        }
    },
    leafstone: {
        num: 111,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        underground: true,
        names: {
            japanese: "リーフのいし",
            korean: "리프의돌",
            french: "Pierre Plante",
            german: "Blattstein",
            spanish: "Piedra Hoja",
            italian: "Pietrafoglia",
            english: "Leaf Stone"
        }
    },
    tinymushroom: {
        num: 113,
        cost: 500,
        itemcategory: "loot",
        names: {
            japanese: "ちいさなキノコ",
            korean: "작은버섯",
            french: "Petit Champi",
            german: "Minipilz",
            spanish: "Mini Seta",
            italian: "Minifungo",
            english: "Tiny Mushroom"
        }
    },
    bigmushroom: {
        num: 114,
        cost: 5000,
        itemcategory: "loot",
        names: {
            japanese: "おおきなキノコ",
            korean: "큰버섯",
            french: "Gros Champi",
            german: "Riesenpilz",
            spanish: "Seta Grande",
            italian: "Grande Fungo",
            english: "Big Mushroom"
        }
    },
    pearl: {
        num: 115,
        cost: 1400,
        itemcategory: "loot",
        names: {
            japanese: "しんじゅ",
            korean: "진주",
            french: "Perle",
            german: "Perle",
            spanish: "Perla",
            italian: "Perla",
            english: "Pearl"
        }
    },
    bigpearl: {
        num: 120,
        cost: 7500,
        itemcategory: "loot",
        names: {
            japanese: "おおきなしんじゅ",
            korean: "큰진주",
            french: "Grande Perle",
            german: "Riesenperle",
            spanish: "Perla Grande",
            italian: "Grande Perla",
            english: "Big Pearl"
        }
    },
    stardust: {
        num: 121,
        cost: 2000,
        itemcategory: "loot",
        names: {
            japanese: "ほしのすな",
            korean: "별의모래",
            french: "Poussière Étoile",
            german: "Sternenstaub",
            spanish: "Polvoestelar",
            italian: "Polvostella",
            english: "Stardust"
        }
    },
    starpiece: {
        num: 122,
        cost: 9800,
        itemcategory: "loot",
        underground: true,
        names: {
            japanese: "ほしのかけら",
            korean: "별의조각",
            french: "Morceau d'Étoile",
            german: "Sternenstück",
            spanish: "Trozo Estrella",
            italian: "Pezzo Stella",
            english: "Star Piece"
        }
    },
    nugget: {
        num: 123,
        cost: 10000,
        itemcategory: "loot",
        names: {
            japanese: "きんのたま",
            korean: "금구슬",
            french: "Pépite",
            german: "Nugget",
            spanish: "Pepita",
            italian: "Pepita",
            english: "Nugget"
        }
    },
    heartscale: {
        num: 124,
        cost: 100,
        itemcategory: "collectibles",
        underground: true,
        names: {
            japanese: "ハートのウロコ",
            korean: "하트비늘",
            french: "Écaille Cœur",
            german: "Herzschuppe",
            spanish: "Escama Corazón",
            italian: "Squama Cuore",
            english: "Heart Scale"
        }
    },
    honey: {
        num: 125,
        cost: 100,
        itemcategory: "dexcompletion",
        names: {
            japanese: "あまいミツ",
            korean: "달콤한꿀",
            french: "Miel",
            german: "Honig",
            spanish: "Miel",
            italian: "Miele",
            english: "Honey"
        }
    },
    growthmulch: {
        num: 126,
        cost: 200,
        itemcategory: "mulch",
        names: {
            japanese: "すくすくこやし",
            korean: "무럭무럭비료",
            french: "Fertipousse",
            german: "Wachsmulch",
            spanish: "Abono Rápido",
            italian: "Fertilrapido",
            english: "Growth Mulch"
        }
    },
    dampmulch: {
        num: 127,
        cost: 200,
        itemcategory: "mulch",
        names: {
            japanese: "じめじめこやし",
            korean: "축축이비료",
            french: "Fertihumide",
            german: "Feuchtmulch",
            spanish: "Abono Lento",
            italian: "Fertilidro",
            english: "Damp Mulch"
        }
    },
    stablemulch: {
        num: 128,
        cost: 200,
        itemcategory: "mulch",
        names: {
            japanese: "ながながこやし",
            korean: "오래오래비료",
            french: "Fertistable",
            german: "Stabilmulch",
            spanish: "Abono Fijador",
            italian: "Fertilsaldo",
            english: "Stable Mulch"
        }
    },
    gooeymulch: {
        num: 129,
        cost: 200,
        itemcategory: "mulch",
        names: {
            japanese: "ねばねばこやし",
            korean: "끈적끈적비료",
            french: "Fertiglu",
            german: "Neumulch",
            spanish: "Abono Brote",
            italian: "Fertilcolla",
            english: "Gooey Mulch"
        }
    },
    shinystone: {
        num: 140,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "ひかりのいし",
            korean: "빛의돌",
            french: "Pierre Éclat",
            german: "Leuchtstein",
            spanish: "Piedra Día",
            italian: "Pietrabrillo",
            english: "Shiny Stone"
        }
    },
    duskstone: {
        num: 141,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "やみのいし",
            korean: "어둠의돌",
            french: "Pierre Nuit",
            german: "Finsterstein",
            spanish: "Piedra Noche",
            italian: "Neropietra",
            english: "Dusk Stone"
        }
    },
    dawnstone: {
        num: 142,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "めざめいし",
            korean: "각성의돌",
            french: "Pierre Aube",
            german: "Funkelstein",
            spanish: "Piedra Alba",
            italian: "Pietralbore",
            english: "Dawn Stone"
        }
    },
    ovalstone: {
        num: 143,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        underground: true,
        names: {
            japanese: "まんまるいし",
            korean: "동글동글돌",
            french: "Pierre Ovale",
            german: "Ovaler Stein",
            spanish: "Piedra Oval",
            italian: "Pietraovale",
            english: "Oval Stone"
        }
    },
    oddkeystone: {
        num: 144,
        cost: 2100,
        itemcategory: "dexcompletion",
        underground: true,
        names: {
            japanese: "かなめいし",
            korean: "쐐기돌",
            french: "Clé de Voûte",
            german: "Spiritkern",
            spanish: "Piedra Espíritu",
            italian: "Roccianima",
            english: "Odd Keystone"
        }
    },
    grassmail: {
        num: 147,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "グラスメール",
            french: "Lettre Herbe",
            german: "Wiesenbrief",
            spanish: "Carta Hierba",
            italian: "Mess. Erba",
            english: "Grass Mail"
        }
    },
    flamemail: {
        num: 148,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "フレイムメール",
            french: "Lettre Feu",
            german: "Feuerbrief",
            spanish: "Carta Fuego",
            italian: "Mess. Fiamma",
            english: "Flame Mail"
        }
    },
    bubblemail: {
        num: 216,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブルーメール",
            french: "Lettre Mer",
            german: "Wasserbrief",
            spanish: "Carta Pompas",
            italian: "Mess. Bolla",
            english: "Bubble Mail"
        }
    },
    bloommail: {
        num: 218,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブルームメール",
            french: "Lett. Pétale",
            german: "Blütenbrief",
            spanish: "Carta Flores",
            italian: "Mess. Petalo",
            english: "Bloom Mail"
        }
    },
    tunnelmail: {
        num: 223,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "トンネルメール",
            french: "Lettre Mine",
            german: "Minenbrief",
            spanish: "Carta Mina",
            italian: "Mess. Tunnel",
            english: "Tunnel Mail"
        }
    },
    steelmail: {
        num: 224,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "スチールメール",
            french: "Lettre Acier",
            german: "Stahlbrief",
            spanish: "Carta Acero",
            italian: "Mess. Lega",
            english: "Steel Mail"
        }
    },
    heartmail: {
        num: 228,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ラブラブメール",
            french: "Lettre Coeur",
            german: "Rosabrief",
            spanish: "Car. Corazón",
            italian: "Mess. Cuore",
            english: "Heart Mail"
        }
    },
    snowmail: {
        num: 229,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリザードメール",
            french: "Lettre Neige",
            german: "Schneebrief",
            spanish: "Carta Nieve",
            italian: "Mess. Neve",
            english: "Snow Mail"
        }
    },
    spacemail: {
        num: 231,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "スペースメール",
            french: "Lettre Cosmo",
            german: "Sternbrief",
            spanish: "Car. Sideral",
            italian: "Mess. Spazio",
            english: "Space Mail"
        }
    },
    airmail: {
        num: 235,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "エアメール",
            french: "Lettre Avion",
            german: "Luftbrief",
            spanish: "Carta Aérea",
            italian: "Mess. Aereo",
            english: "Air Mail"
        }
    },
    mosaicmail: {
        num: 252,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "モザイクメール",
            french: "Lettremosaïk",
            german: "Mosaikbrief",
            spanish: "Car. Mosaico",
            italian: "Mess. Iride",
            english: "Mosaic Mail"
        }
    },
    brickmail: {
        num: 260,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリックメール",
            french: "Lettre Brik",
            german: "Ziegelbrief",
            spanish: "Carta Pared",
            italian: "Mess. Muro",
            english: "Brick Mail"
        }
    },
    expshare: {
        num: 385,
        cost: 3000,
        itemcategory: "trainingitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "がくしゅうそうち",
            korean: "학습장치",
            french: "Multi Exp",
            german: "EP-Teiler",
            spanish: "Repartir Exp",
            italian: "Condividi Esp.",
            english: "Exp. Share"
        }
    },
    soothebell: {
        num: 387,
        cost: 100,
        itemcategory: "trainingitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "やすらぎのすず",
            korean: "평온의방울",
            french: "Grelot Zen",
            german: "Sanftglocke",
            spanish: "Campana Alivio",
            italian: "Calmanella",
            english: "Soothe Bell"
        }
    },
    amuletcoin: {
        num: 392,
        cost: 100,
        itemcategory: "trainingitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "おまもりこばん",
            korean: "부적금화",
            french: "Pièce Rune",
            german: "Münzamulett",
            spanish: "Moneda Amuleto",
            italian: "Monetamuleto",
            english: "Amulet Coin"
        }
    },
    cleansetag: {
        num: 393,
        cost: 200,
        itemcategory: "trainingitem",
        holdable: true,
        names: {
            japanese: "きよめのおふだ",
            korean: "순결의부적",
            french: "Rune Purifiante",
            german: "Schutzband",
            spanish: "Amuleto",
            italian: "Velopuro",
            english: "Cleanse Tag"
        }
    },
    smokeball: {
        num: 397,
        cost: 200,
        itemcategory: "helditems",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "けむりだま",
            korean: "연막탄",
            french: "Boule Fumée",
            german: "Rauchball",
            spanish: "Bola Humo",
            italian: "Palla Fumo",
            english: "Smoke Ball"
        }
    },
    everstone: {
        num: 398,
        cost: 200,
        itemcategory: "trainingitem",
        holdable: true,
        holdableactive: true,
        underground: true,
        names: {
            japanese: "かわらずのいし",
            korean: "변함없는돌",
            french: "Pierre Stase",
            german: "Ewigstein",
            spanish: "Piedra Eterna",
            italian: "Pietrastante",
            english: "Everstone"
        }
    },
    luckyegg: {
        num: 400,
        cost: 200,
        itemcategory: "trainingitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "しあわせタマゴ",
            korean: "행복의알",
            french: "Œuf Chance",
            german: "Glücks-Ei",
            spanish: "Huevo Suerte",
            italian: "Fortunuovo",
            english: "Lucky Egg"
        }
    },
    upgrade: {
        num: 421,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "アップグレード",
            korean: "업그레이드",
            french: "Améliorator",
            german: "Up-Grade",
            spanish: "Mejora",
            italian: "Upgrade",
            english: "Up-Grade"
        }
    },
    redscarf: {
        num: 429,
        cost: 100,
        itemcategory: "scarves",
        holdableactive: true,
        names: {
            japanese: "あかいバンダナ",
            korean: "빨강밴드",
            french: "Foulard Rouge",
            german: "Roter Schal",
            spanish: "Pañuelo Rojo",
            italian: "Fascia Rossa",
            english: "Red Scarf"
        }
    },
    bluescarf: {
        num: 430,
        cost: 100,
        itemcategory: "scarves",
        holdableactive: true,
        names: {
            japanese: "あおいバンダナ",
            korean: "파랑밴드",
            french: "Foulard Bleu",
            german: "Blauer Schal",
            spanish: "Pañuelo Azul",
            italian: "Fascia Blu",
            english: "Blue Scarf"
        }
    },
    pinkscarf: {
        num: 431,
        cost: 100,
        itemcategory: "scarves",
        holdableactive: true,
        names: {
            japanese: "ピンクのバンダナ",
            korean: "분홍밴드",
            french: "Foulard Rose",
            german: "Rosa Schal",
            spanish: "Pañuelo Rosa",
            italian: "Fascia Rosa",
            english: "Pink Scarf"
        }
    },
    greenscarf: {
        num: 432,
        cost: 100,
        itemcategory: "scarves",
        holdableactive: true,
        names: {
            japanese: "みどりのバンダナ",
            korean: "초록밴드",
            french: "Foulard Vert",
            german: "Grüner Schal",
            spanish: "Pañuelo Verde",
            italian: "Fascia Verde",
            english: "Green Scarf"
        }
    },
    yellowscarf: {
        num: 433,
        cost: 100,
        itemcategory: "scarves",
        holdableactive: true,
        names: {
            japanese: "きいろのバンダナ",
            korean: "노랑밴드",
            french: "Foulard Jaune",
            german: "Gelber Schal",
            spanish: "Pañuelo Amarillo",
            italian: "Fascia Gialla",
            english: "Yellow Scarf"
        }
    },
    luckincense: {
        num: 488,
        cost: 9600,
        itemcategory: "trainingitem",
        holdable: true,
        holdableactive: true,
        names: {
            japanese: "こううんのおこう",
            korean: "행운의향로",
            french: "Encens Veine",
            german: "Glücksrauch",
            spanish: "Incienso Duplo",
            italian: "Fortunaroma",
            english: "Luck Incense"
        }
    },
    pureincense: {
        num: 489,
        cost: 9600,
        itemcategory: "trainingitem",
        holdable: true,
        names: {
            japanese: "きよめのおこう",
            korean: "순결의향로",
            french: "Encens Pur",
            german: "Scheuchrauch",
            spanish: "Incienso Puro",
            italian: "Puroaroma",
            english: "Pure Incense"
        }
    },
    protector: {
        num: 490,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "プロテクター",
            korean: "프로텍터",
            french: "Protecteur",
            german: "Schützer",
            spanish: "Protector",
            italian: "Copertura",
            english: "Protector"
        }
    },
    magmarizer: {
        num: 501,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "マグマブースター",
            korean: "마그마부스터",
            french: "Magmariseur",
            german: "Magmaisierer",
            spanish: "Magmatizador",
            italian: "Magmatore",
            english: "Magmarizer"
        }
    },
    dubiousdisc: {
        num: 502,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "あやしいパッチ",
            korean: "괴상한패치",
            french: "CD Douteux",
            german: "Dubiosdisc",
            spanish: "Disco Extraño",
            italian: "Dubbiodisco",
            english: "Dubious Disc"
        }
    },
    reapercloth: {
        num: 503,
        cost: 2100,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "れいかいのぬの",
            korean: "영계의천",
            french: "Tissu Fauche",
            german: "Düsterumhang",
            spanish: "Tela Terrible",
            italian: "Terrorpanno",
            english: "Reaper Cloth"
        }
    },
    tm01: {
        num: 506,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 1,
                move: 5
            },
            yellow: {
                tm: 1,
                move: 5
            },
            goldsilver: {
                tm: 1,
                move: 223
            },
            crystal: {
                tm: 1,
                move: 223
            },
            rubysapphire: {
                tm: 1,
                move: 264
            },
            emerald: {
                tm: 1,
                move: 264
            },
            fireredleafgreen: {
                tm: 1,
                move: 264
            },
            diamondpearl: {
                tm: 1,
                move: 264
            },
            platinum: {
                tm: 1,
                move: 264
            },
            heartgoldsoulsilver: {
                tm: 1,
                move: 264
            },
            blackwhite: {
                tm: 1,
                move: 468
            },
            colosseum: {
                tm: 1,
                move: 264
            },
            xd: {
                tm: 1,
                move: 264
            },
            black2white2: {
                tm: 1,
                move: 468
            },
            pokemonxy: {
                tm: 1,
                move: 468
            },
            omegarubyalphasapphire: {
                tm: 1,
                move: 468
            }
        },
        names: {
            japanese: "わざマシン０１",
            korean: "기술머신01",
            french: "CT01",
            german: "TM01",
            spanish: "MT01",
            italian: "MT01",
            english: "TM01"
        }
    },
    tm02: {
        num: 507,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 2,
                move: 13
            },
            yellow: {
                tm: 2,
                move: 13
            },
            goldsilver: {
                tm: 2,
                move: 29
            },
            crystal: {
                tm: 2,
                move: 29
            },
            rubysapphire: {
                tm: 2,
                move: 337
            },
            emerald: {
                tm: 2,
                move: 337
            },
            fireredleafgreen: {
                tm: 2,
                move: 337
            },
            diamondpearl: {
                tm: 2,
                move: 337
            },
            platinum: {
                tm: 2,
                move: 337
            },
            heartgoldsoulsilver: {
                tm: 2,
                move: 337
            },
            blackwhite: {
                tm: 2,
                move: 337
            },
            colosseum: {
                tm: 2,
                move: 337
            },
            xd: {
                tm: 2,
                move: 337
            },
            black2white2: {
                tm: 2,
                move: 337
            },
            pokemonxy: {
                tm: 2,
                move: 337
            },
            omegarubyalphasapphire: {
                tm: 2,
                move: 337
            }
        },
        names: {
            japanese: "わざマシン０２",
            korean: "기술머신02",
            french: "CT02",
            german: "TM02",
            spanish: "MT02",
            italian: "MT02",
            english: "TM02"
        }
    },
    tm03: {
        num: 508,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 3,
                move: 14
            },
            yellow: {
                tm: 3,
                move: 14
            },
            goldsilver: {
                tm: 3,
                move: 174
            },
            crystal: {
                tm: 3,
                move: 174
            },
            rubysapphire: {
                tm: 3,
                move: 352
            },
            emerald: {
                tm: 3,
                move: 352
            },
            fireredleafgreen: {
                tm: 3,
                move: 352
            },
            diamondpearl: {
                tm: 3,
                move: 352
            },
            platinum: {
                tm: 3,
                move: 352
            },
            heartgoldsoulsilver: {
                tm: 3,
                move: 352
            },
            blackwhite: {
                tm: 3,
                move: 473
            },
            colosseum: {
                tm: 3,
                move: 352
            },
            xd: {
                tm: 3,
                move: 352
            },
            black2white2: {
                tm: 3,
                move: 473
            },
            pokemonxy: {
                tm: 3,
                move: 473
            },
            omegarubyalphasapphire: {
                tm: 3,
                move: 473
            }
        },
        names: {
            japanese: "わざマシン０３",
            korean: "기술머신03",
            french: "CT03",
            german: "TM03",
            spanish: "MT03",
            italian: "MT03",
            english: "TM03"
        }
    },
    tm04: {
        num: 509,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 4,
                move: 18
            },
            yellow: {
                tm: 4,
                move: 18
            },
            goldsilver: {
                tm: 4,
                move: 205
            },
            crystal: {
                tm: 4,
                move: 205
            },
            rubysapphire: {
                tm: 4,
                move: 347
            },
            emerald: {
                tm: 4,
                move: 347
            },
            fireredleafgreen: {
                tm: 4,
                move: 347
            },
            diamondpearl: {
                tm: 4,
                move: 347
            },
            platinum: {
                tm: 4,
                move: 347
            },
            heartgoldsoulsilver: {
                tm: 4,
                move: 347
            },
            blackwhite: {
                tm: 4,
                move: 347
            },
            colosseum: {
                tm: 4,
                move: 347
            },
            xd: {
                tm: 4,
                move: 347
            },
            black2white2: {
                tm: 4,
                move: 347
            },
            pokemonxy: {
                tm: 4,
                move: 347
            },
            omegarubyalphasapphire: {
                tm: 4,
                move: 347
            }
        },
        names: {
            japanese: "わざマシン０４",
            korean: "기술머신04",
            french: "CT04",
            german: "TM04",
            spanish: "MT04",
            italian: "MT04",
            english: "TM04"
        }
    },
    tm05: {
        num: 510,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 5,
                move: 25
            },
            yellow: {
                tm: 5,
                move: 25
            },
            goldsilver: {
                tm: 5,
                move: 46
            },
            crystal: {
                tm: 5,
                move: 46
            },
            rubysapphire: {
                tm: 5,
                move: 46
            },
            emerald: {
                tm: 5,
                move: 46
            },
            fireredleafgreen: {
                tm: 5,
                move: 46
            },
            diamondpearl: {
                tm: 5,
                move: 46
            },
            platinum: {
                tm: 5,
                move: 46
            },
            heartgoldsoulsilver: {
                tm: 5,
                move: 46
            },
            blackwhite: {
                tm: 5,
                move: 46
            },
            colosseum: {
                tm: 5,
                move: 46
            },
            xd: {
                tm: 5,
                move: 46
            },
            black2white2: {
                tm: 5,
                move: 46
            },
            pokemonxy: {
                tm: 5,
                move: 46
            },
            omegarubyalphasapphire: {
                tm: 5,
                move: 46
            }
        },
        names: {
            japanese: "わざマシン０５",
            korean: "기술머신05",
            french: "CT05",
            german: "TM05",
            spanish: "MT05",
            italian: "MT05",
            english: "TM05"
        }
    },
    tm06: {
        num: 511,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 6,
                move: 92
            },
            yellow: {
                tm: 6,
                move: 92
            },
            goldsilver: {
                tm: 6,
                move: 92
            },
            crystal: {
                tm: 6,
                move: 92
            },
            rubysapphire: {
                tm: 6,
                move: 92
            },
            emerald: {
                tm: 6,
                move: 92
            },
            fireredleafgreen: {
                tm: 6,
                move: 92
            },
            diamondpearl: {
                tm: 6,
                move: 92
            },
            platinum: {
                tm: 6,
                move: 92
            },
            heartgoldsoulsilver: {
                tm: 6,
                move: 92
            },
            blackwhite: {
                tm: 6,
                move: 92
            },
            colosseum: {
                tm: 6,
                move: 92
            },
            xd: {
                tm: 6,
                move: 92
            },
            black2white2: {
                tm: 6,
                move: 92
            },
            pokemonxy: {
                tm: 6,
                move: 92
            },
            omegarubyalphasapphire: {
                tm: 6,
                move: 92
            }
        },
        names: {
            japanese: "わざマシン０６",
            korean: "기술머신06",
            french: "CT06",
            german: "TM06",
            spanish: "MT06",
            italian: "MT06",
            english: "TM06"
        }
    },
    tm07: {
        num: 512,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 7,
                move: 32
            },
            yellow: {
                tm: 7,
                move: 32
            },
            goldsilver: {
                tm: 7,
                move: 192
            },
            crystal: {
                tm: 7,
                move: 192
            },
            rubysapphire: {
                tm: 7,
                move: 258
            },
            emerald: {
                tm: 7,
                move: 258
            },
            fireredleafgreen: {
                tm: 7,
                move: 258
            },
            diamondpearl: {
                tm: 7,
                move: 258
            },
            platinum: {
                tm: 7,
                move: 258
            },
            heartgoldsoulsilver: {
                tm: 7,
                move: 258
            },
            blackwhite: {
                tm: 7,
                move: 258
            },
            colosseum: {
                tm: 7,
                move: 258
            },
            xd: {
                tm: 7,
                move: 258
            },
            black2white2: {
                tm: 7,
                move: 258
            },
            pokemonxy: {
                tm: 7,
                move: 258
            },
            omegarubyalphasapphire: {
                tm: 7,
                move: 258
            }
        },
        names: {
            japanese: "わざマシン０７",
            korean: "기술머신07",
            french: "CT07",
            german: "TM07",
            spanish: "MT07",
            italian: "MT07",
            english: "TM07"
        }
    },
    tm08: {
        num: 513,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 8,
                move: 34
            },
            yellow: {
                tm: 8,
                move: 34
            },
            goldsilver: {
                tm: 8,
                move: 249
            },
            crystal: {
                tm: 8,
                move: 249
            },
            rubysapphire: {
                tm: 8,
                move: 339
            },
            emerald: {
                tm: 8,
                move: 339
            },
            fireredleafgreen: {
                tm: 8,
                move: 339
            },
            diamondpearl: {
                tm: 8,
                move: 339
            },
            platinum: {
                tm: 8,
                move: 339
            },
            heartgoldsoulsilver: {
                tm: 8,
                move: 339
            },
            blackwhite: {
                tm: 8,
                move: 339
            },
            colosseum: {
                tm: 8,
                move: 339
            },
            xd: {
                tm: 8,
                move: 339
            },
            black2white2: {
                tm: 8,
                move: 339
            },
            pokemonxy: {
                tm: 8,
                move: 339
            },
            omegarubyalphasapphire: {
                tm: 8,
                move: 339
            }
        },
        names: {
            japanese: "わざマシン０８",
            korean: "기술머신08",
            french: "CT08",
            german: "TM08",
            spanish: "MT08",
            italian: "MT08",
            english: "TM08"
        }
    },
    tm09: {
        num: 514,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 9,
                move: 36
            },
            yellow: {
                tm: 9,
                move: 36
            },
            goldsilver: {
                tm: 9,
                move: 244
            },
            crystal: {
                tm: 9,
                move: 244
            },
            rubysapphire: {
                tm: 9,
                move: 331
            },
            emerald: {
                tm: 9,
                move: 331
            },
            fireredleafgreen: {
                tm: 9,
                move: 331
            },
            diamondpearl: {
                tm: 9,
                move: 331
            },
            platinum: {
                tm: 9,
                move: 331
            },
            heartgoldsoulsilver: {
                tm: 9,
                move: 331
            },
            blackwhite: {
                tm: 9,
                move: 474
            },
            colosseum: {
                tm: 9,
                move: 331
            },
            xd: {
                tm: 9,
                move: 331
            },
            black2white2: {
                tm: 9,
                move: 474
            },
            pokemonxy: {
                tm: 9,
                move: 474
            },
            omegarubyalphasapphire: {
                tm: 9,
                move: 474
            }
        },
        names: {
            japanese: "わざマシン０９",
            korean: "기술머신09",
            french: "CT09",
            german: "TM09",
            spanish: "MT09",
            italian: "MT09",
            english: "TM09"
        }
    },
    tm10: {
        num: 515,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 10,
                move: 38
            },
            yellow: {
                tm: 10,
                move: 38
            },
            goldsilver: {
                tm: 10,
                move: 237
            },
            crystal: {
                tm: 10,
                move: 237
            },
            rubysapphire: {
                tm: 10,
                move: 237
            },
            emerald: {
                tm: 10,
                move: 237
            },
            fireredleafgreen: {
                tm: 10,
                move: 237
            },
            diamondpearl: {
                tm: 10,
                move: 237
            },
            platinum: {
                tm: 10,
                move: 237
            },
            heartgoldsoulsilver: {
                tm: 10,
                move: 237
            },
            blackwhite: {
                tm: 10,
                move: 237
            },
            colosseum: {
                tm: 10,
                move: 237
            },
            xd: {
                tm: 10,
                move: 237
            },
            black2white2: {
                tm: 10,
                move: 237
            },
            pokemonxy: {
                tm: 10,
                move: 237
            },
            omegarubyalphasapphire: {
                tm: 10,
                move: 237
            }
        },
        names: {
            japanese: "わざマシン１０",
            korean: "기술머신10",
            french: "CT10",
            german: "TM10",
            spanish: "MT10",
            italian: "MT10",
            english: "TM10"
        }
    },
    tm11: {
        num: 516,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 11,
                move: 61
            },
            yellow: {
                tm: 11,
                move: 61
            },
            goldsilver: {
                tm: 11,
                move: 241
            },
            crystal: {
                tm: 11,
                move: 241
            },
            rubysapphire: {
                tm: 11,
                move: 241
            },
            emerald: {
                tm: 11,
                move: 241
            },
            fireredleafgreen: {
                tm: 11,
                move: 241
            },
            diamondpearl: {
                tm: 11,
                move: 241
            },
            platinum: {
                tm: 11,
                move: 241
            },
            heartgoldsoulsilver: {
                tm: 11,
                move: 241
            },
            blackwhite: {
                tm: 11,
                move: 241
            },
            colosseum: {
                tm: 11,
                move: 241
            },
            xd: {
                tm: 11,
                move: 241
            },
            black2white2: {
                tm: 11,
                move: 241
            },
            pokemonxy: {
                tm: 11,
                move: 241
            },
            omegarubyalphasapphire: {
                tm: 11,
                move: 241
            }
        },
        names: {
            japanese: "わざマシン１１",
            korean: "기술머신11",
            french: "CT11",
            german: "TM11",
            spanish: "MT11",
            italian: "MT11",
            english: "TM11"
        }
    },
    tm12: {
        num: 517,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 12,
                move: 55
            },
            yellow: {
                tm: 12,
                move: 55
            },
            goldsilver: {
                tm: 12,
                move: 230
            },
            crystal: {
                tm: 12,
                move: 230
            },
            rubysapphire: {
                tm: 12,
                move: 269
            },
            emerald: {
                tm: 12,
                move: 269
            },
            fireredleafgreen: {
                tm: 12,
                move: 269
            },
            diamondpearl: {
                tm: 12,
                move: 269
            },
            platinum: {
                tm: 12,
                move: 269
            },
            heartgoldsoulsilver: {
                tm: 12,
                move: 269
            },
            blackwhite: {
                tm: 12,
                move: 269
            },
            colosseum: {
                tm: 12,
                move: 269
            },
            xd: {
                tm: 12,
                move: 269
            },
            black2white2: {
                tm: 12,
                move: 269
            },
            pokemonxy: {
                tm: 12,
                move: 269
            },
            omegarubyalphasapphire: {
                tm: 12,
                move: 269
            }
        },
        names: {
            japanese: "わざマシン１２",
            korean: "기술머신12",
            french: "CT12",
            german: "TM12",
            spanish: "MT12",
            italian: "MT12",
            english: "TM12"
        }
    },
    tm13: {
        num: 518,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 13,
                move: 58
            },
            yellow: {
                tm: 13,
                move: 58
            },
            goldsilver: {
                tm: 13,
                move: 173
            },
            crystal: {
                tm: 13,
                move: 173
            },
            rubysapphire: {
                tm: 13,
                move: 58
            },
            emerald: {
                tm: 13,
                move: 58
            },
            fireredleafgreen: {
                tm: 13,
                move: 58
            },
            diamondpearl: {
                tm: 13,
                move: 58
            },
            platinum: {
                tm: 13,
                move: 58
            },
            heartgoldsoulsilver: {
                tm: 13,
                move: 58
            },
            blackwhite: {
                tm: 13,
                move: 58
            },
            colosseum: {
                tm: 13,
                move: 58
            },
            xd: {
                tm: 13,
                move: 58
            },
            black2white2: {
                tm: 13,
                move: 58
            },
            pokemonxy: {
                tm: 13,
                move: 58
            },
            omegarubyalphasapphire: {
                tm: 13,
                move: 58
            }
        },
        names: {
            japanese: "わざマシン１３",
            korean: "기술머신13",
            french: "CT13",
            german: "TM13",
            spanish: "MT13",
            italian: "MT13",
            english: "TM13"
        }
    },
    tm14: {
        num: 519,
        cost: 5500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 14,
                move: 59
            },
            yellow: {
                tm: 14,
                move: 59
            },
            goldsilver: {
                tm: 14,
                move: 59
            },
            crystal: {
                tm: 14,
                move: 59
            },
            rubysapphire: {
                tm: 14,
                move: 59
            },
            emerald: {
                tm: 14,
                move: 59
            },
            fireredleafgreen: {
                tm: 14,
                move: 59
            },
            diamondpearl: {
                tm: 14,
                move: 59
            },
            platinum: {
                tm: 14,
                move: 59
            },
            heartgoldsoulsilver: {
                tm: 14,
                move: 59
            },
            blackwhite: {
                tm: 14,
                move: 59
            },
            colosseum: {
                tm: 14,
                move: 59
            },
            xd: {
                tm: 14,
                move: 59
            },
            black2white2: {
                tm: 14,
                move: 59
            },
            pokemonxy: {
                tm: 14,
                move: 59
            },
            omegarubyalphasapphire: {
                tm: 14,
                move: 59
            }
        },
        names: {
            japanese: "わざマシン１４",
            korean: "기술머신14",
            french: "CT14",
            german: "TM14",
            spanish: "MT14",
            italian: "MT14",
            english: "TM14"
        }
    },
    tm15: {
        num: 520,
        cost: 7500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 15,
                move: 63
            },
            yellow: {
                tm: 15,
                move: 63
            },
            goldsilver: {
                tm: 15,
                move: 63
            },
            crystal: {
                tm: 15,
                move: 63
            },
            rubysapphire: {
                tm: 15,
                move: 63
            },
            emerald: {
                tm: 15,
                move: 63
            },
            fireredleafgreen: {
                tm: 15,
                move: 63
            },
            diamondpearl: {
                tm: 15,
                move: 63
            },
            platinum: {
                tm: 15,
                move: 63
            },
            heartgoldsoulsilver: {
                tm: 15,
                move: 63
            },
            blackwhite: {
                tm: 15,
                move: 63
            },
            colosseum: {
                tm: 15,
                move: 63
            },
            xd: {
                tm: 15,
                move: 63
            },
            black2white2: {
                tm: 15,
                move: 63
            },
            pokemonxy: {
                tm: 15,
                move: 63
            },
            omegarubyalphasapphire: {
                tm: 15,
                move: 63
            }
        },
        names: {
            japanese: "わざマシン１５",
            korean: "기술머신15",
            french: "CT15",
            german: "TM15",
            spanish: "MT15",
            italian: "MT15",
            english: "TM15"
        }
    },
    tm16: {
        num: 521,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 16,
                move: 6
            },
            yellow: {
                tm: 16,
                move: 6
            },
            goldsilver: {
                tm: 16,
                move: 196
            },
            crystal: {
                tm: 16,
                move: 196
            },
            rubysapphire: {
                tm: 16,
                move: 113
            },
            emerald: {
                tm: 16,
                move: 113
            },
            fireredleafgreen: {
                tm: 16,
                move: 113
            },
            diamondpearl: {
                tm: 16,
                move: 113
            },
            platinum: {
                tm: 16,
                move: 113
            },
            heartgoldsoulsilver: {
                tm: 16,
                move: 113
            },
            blackwhite: {
                tm: 16,
                move: 113
            },
            colosseum: {
                tm: 16,
                move: 113
            },
            xd: {
                tm: 16,
                move: 113
            },
            black2white2: {
                tm: 16,
                move: 113
            },
            pokemonxy: {
                tm: 16,
                move: 113
            },
            omegarubyalphasapphire: {
                tm: 16,
                move: 113
            }
        },
        names: {
            japanese: "わざマシン１６",
            korean: "기술머신16",
            french: "CT16",
            german: "TM16",
            spanish: "MT16",
            italian: "MT16",
            english: "TM16"
        }
    },
    tm17: {
        num: 522,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 17,
                move: 66
            },
            yellow: {
                tm: 17,
                move: 66
            },
            goldsilver: {
                tm: 17,
                move: 182
            },
            crystal: {
                tm: 17,
                move: 182
            },
            rubysapphire: {
                tm: 17,
                move: 182
            },
            emerald: {
                tm: 17,
                move: 182
            },
            fireredleafgreen: {
                tm: 17,
                move: 182
            },
            diamondpearl: {
                tm: 17,
                move: 182
            },
            platinum: {
                tm: 17,
                move: 182
            },
            heartgoldsoulsilver: {
                tm: 17,
                move: 182
            },
            blackwhite: {
                tm: 17,
                move: 182
            },
            colosseum: {
                tm: 17,
                move: 182
            },
            xd: {
                tm: 17,
                move: 182
            },
            black2white2: {
                tm: 17,
                move: 182
            },
            pokemonxy: {
                tm: 17,
                move: 182
            },
            omegarubyalphasapphire: {
                tm: 17,
                move: 182
            }
        },
        names: {
            japanese: "わざマシン１７",
            korean: "기술머신17",
            french: "CT17",
            german: "TM17",
            spanish: "MT17",
            italian: "MT17",
            english: "TM17"
        }
    },
    tm18: {
        num: 523,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 18,
                move: 68
            },
            yellow: {
                tm: 18,
                move: 68
            },
            goldsilver: {
                tm: 18,
                move: 240
            },
            crystal: {
                tm: 18,
                move: 240
            },
            rubysapphire: {
                tm: 18,
                move: 240
            },
            emerald: {
                tm: 18,
                move: 240
            },
            fireredleafgreen: {
                tm: 18,
                move: 240
            },
            diamondpearl: {
                tm: 18,
                move: 240
            },
            platinum: {
                tm: 18,
                move: 240
            },
            heartgoldsoulsilver: {
                tm: 18,
                move: 240
            },
            blackwhite: {
                tm: 18,
                move: 240
            },
            colosseum: {
                tm: 18,
                move: 240
            },
            xd: {
                tm: 18,
                move: 240
            },
            black2white2: {
                tm: 18,
                move: 240
            },
            pokemonxy: {
                tm: 18,
                move: 240
            },
            omegarubyalphasapphire: {
                tm: 18,
                move: 240
            }
        },
        names: {
            japanese: "わざマシン１８",
            korean: "기술머신18",
            french: "CT18",
            german: "TM18",
            spanish: "MT18",
            italian: "MT18",
            english: "TM18"
        }
    },
    tm19: {
        num: 524,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 19,
                move: 69
            },
            yellow: {
                tm: 19,
                move: 69
            },
            goldsilver: {
                tm: 19,
                move: 202
            },
            crystal: {
                tm: 19,
                move: 202
            },
            rubysapphire: {
                tm: 19,
                move: 202
            },
            emerald: {
                tm: 19,
                move: 202
            },
            fireredleafgreen: {
                tm: 19,
                move: 202
            },
            diamondpearl: {
                tm: 19,
                move: 202
            },
            platinum: {
                tm: 19,
                move: 202
            },
            heartgoldsoulsilver: {
                tm: 19,
                move: 202
            },
            blackwhite: {
                tm: 19,
                move: 477
            },
            colosseum: {
                tm: 19,
                move: 202
            },
            xd: {
                tm: 19,
                move: 202
            },
            black2white2: {
                tm: 19,
                move: 477
            },
            pokemonxy: {
                tm: 19,
                move: 355
            },
            omegarubyalphasapphire: {
                tm: 19,
                move: 355
            }
        },
        names: {
            japanese: "わざマシン１９",
            korean: "기술머신19",
            french: "CT19",
            german: "TM19",
            spanish: "MT19",
            italian: "MT19",
            english: "TM19"
        }
    },
    tm20: {
        num: 525,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 20,
                move: 99
            },
            yellow: {
                tm: 20,
                move: 99
            },
            goldsilver: {
                tm: 20,
                move: 203
            },
            crystal: {
                tm: 20,
                move: 203
            },
            rubysapphire: {
                tm: 20,
                move: 219
            },
            emerald: {
                tm: 20,
                move: 219
            },
            fireredleafgreen: {
                tm: 20,
                move: 219
            },
            diamondpearl: {
                tm: 20,
                move: 219
            },
            platinum: {
                tm: 20,
                move: 219
            },
            heartgoldsoulsilver: {
                tm: 20,
                move: 219
            },
            blackwhite: {
                tm: 20,
                move: 219
            },
            colosseum: {
                tm: 20,
                move: 219
            },
            xd: {
                tm: 20,
                move: 219
            },
            black2white2: {
                tm: 20,
                move: 219
            },
            pokemonxy: {
                tm: 20,
                move: 219
            },
            omegarubyalphasapphire: {
                tm: 20,
                move: 219
            }
        },
        names: {
            japanese: "わざマシン２０",
            korean: "기술머신20",
            french: "CT20",
            german: "TM20",
            spanish: "MT20",
            italian: "MT20",
            english: "TM20"
        }
    },
    tm21: {
        num: 526,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 21,
                move: 72
            },
            yellow: {
                tm: 21,
                move: 72
            },
            goldsilver: {
                tm: 21,
                move: 218
            },
            crystal: {
                tm: 21,
                move: 218
            },
            rubysapphire: {
                tm: 21,
                move: 218
            },
            emerald: {
                tm: 21,
                move: 218
            },
            fireredleafgreen: {
                tm: 21,
                move: 218
            },
            diamondpearl: {
                tm: 21,
                move: 218
            },
            platinum: {
                tm: 21,
                move: 218
            },
            heartgoldsoulsilver: {
                tm: 21,
                move: 218
            },
            blackwhite: {
                tm: 21,
                move: 218
            },
            colosseum: {
                tm: 21,
                move: 218
            },
            xd: {
                tm: 21,
                move: 218
            },
            black2white2: {
                tm: 21,
                move: 218
            },
            pokemonxy: {
                tm: 21,
                move: 218
            },
            omegarubyalphasapphire: {
                tm: 21,
                move: 218
            }
        },
        names: {
            japanese: "わざマシン２１",
            korean: "기술머신21",
            french: "CT21",
            german: "TM21",
            spanish: "MT21",
            italian: "MT21",
            english: "TM21"
        }
    },
    tm22: {
        num: 527,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 22,
                move: 76
            },
            yellow: {
                tm: 22,
                move: 76
            },
            goldsilver: {
                tm: 22,
                move: 76
            },
            crystal: {
                tm: 22,
                move: 76
            },
            rubysapphire: {
                tm: 22,
                move: 76
            },
            emerald: {
                tm: 22,
                move: 76
            },
            fireredleafgreen: {
                tm: 22,
                move: 76
            },
            diamondpearl: {
                tm: 22,
                move: 76
            },
            platinum: {
                tm: 22,
                move: 76
            },
            heartgoldsoulsilver: {
                tm: 22,
                move: 76
            },
            blackwhite: {
                tm: 22,
                move: 76
            },
            colosseum: {
                tm: 22,
                move: 76
            },
            xd: {
                tm: 22,
                move: 76
            },
            black2white2: {
                tm: 22,
                move: 76
            },
            pokemonxy: {
                tm: 22,
                move: 76
            },
            omegarubyalphasapphire: {
                tm: 22,
                move: 76
            }
        },
        names: {
            japanese: "わざマシン２２",
            korean: "기술머신22",
            french: "CT22",
            german: "TM22",
            spanish: "MT22",
            italian: "MT22",
            english: "TM22"
        }
    },
    tm23: {
        num: 528,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 23,
                move: 82
            },
            yellow: {
                tm: 23,
                move: 82
            },
            goldsilver: {
                tm: 23,
                move: 231
            },
            crystal: {
                tm: 23,
                move: 231
            },
            rubysapphire: {
                tm: 23,
                move: 231
            },
            emerald: {
                tm: 23,
                move: 231
            },
            fireredleafgreen: {
                tm: 23,
                move: 231
            },
            diamondpearl: {
                tm: 23,
                move: 231
            },
            platinum: {
                tm: 23,
                move: 231
            },
            heartgoldsoulsilver: {
                tm: 23,
                move: 231
            },
            blackwhite: {
                tm: 23,
                move: 479
            },
            colosseum: {
                tm: 23,
                move: 231
            },
            xd: {
                tm: 23,
                move: 231
            },
            black2white2: {
                tm: 23,
                move: 479
            },
            pokemonxy: {
                tm: 23,
                move: 479
            },
            omegarubyalphasapphire: {
                tm: 23,
                move: 479
            }
        },
        names: {
            japanese: "わざマシン２３",
            korean: "기술머신23",
            french: "CT23",
            german: "TM23",
            spanish: "MT23",
            italian: "MT23",
            english: "TM23"
        }
    },
    tm24: {
        num: 529,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 24,
                move: 85
            },
            yellow: {
                tm: 24,
                move: 85
            },
            goldsilver: {
                tm: 24,
                move: 225
            },
            crystal: {
                tm: 24,
                move: 225
            },
            rubysapphire: {
                tm: 24,
                move: 85
            },
            emerald: {
                tm: 24,
                move: 85
            },
            fireredleafgreen: {
                tm: 24,
                move: 85
            },
            diamondpearl: {
                tm: 24,
                move: 85
            },
            platinum: {
                tm: 24,
                move: 85
            },
            heartgoldsoulsilver: {
                tm: 24,
                move: 85
            },
            blackwhite: {
                tm: 24,
                move: 85
            },
            colosseum: {
                tm: 24,
                move: 85
            },
            xd: {
                tm: 24,
                move: 85
            },
            black2white2: {
                tm: 24,
                move: 85
            },
            pokemonxy: {
                tm: 24,
                move: 85
            },
            omegarubyalphasapphire: {
                tm: 24,
                move: 85
            }
        },
        names: {
            japanese: "わざマシン２４",
            korean: "기술머신24",
            french: "CT24",
            german: "TM24",
            spanish: "MT24",
            italian: "MT24",
            english: "TM24"
        }
    },
    tm25: {
        num: 530,
        cost: 5500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 25,
                move: 87
            },
            yellow: {
                tm: 25,
                move: 87
            },
            goldsilver: {
                tm: 25,
                move: 87
            },
            crystal: {
                tm: 25,
                move: 87
            },
            rubysapphire: {
                tm: 25,
                move: 87
            },
            emerald: {
                tm: 25,
                move: 87
            },
            fireredleafgreen: {
                tm: 25,
                move: 87
            },
            diamondpearl: {
                tm: 25,
                move: 87
            },
            platinum: {
                tm: 25,
                move: 87
            },
            heartgoldsoulsilver: {
                tm: 25,
                move: 87
            },
            blackwhite: {
                tm: 25,
                move: 87
            },
            colosseum: {
                tm: 25,
                move: 87
            },
            xd: {
                tm: 25,
                move: 87
            },
            black2white2: {
                tm: 25,
                move: 87
            },
            pokemonxy: {
                tm: 25,
                move: 87
            },
            omegarubyalphasapphire: {
                tm: 25,
                move: 87
            }
        },
        names: {
            japanese: "わざマシン２５",
            korean: "기술머신25",
            french: "CT25",
            german: "TM25",
            spanish: "MT25",
            italian: "MT25",
            english: "TM25"
        }
    },
    tm26: {
        num: 531,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 26,
                move: 89
            },
            yellow: {
                tm: 26,
                move: 89
            },
            goldsilver: {
                tm: 26,
                move: 89
            },
            crystal: {
                tm: 26,
                move: 89
            },
            rubysapphire: {
                tm: 26,
                move: 89
            },
            emerald: {
                tm: 26,
                move: 89
            },
            fireredleafgreen: {
                tm: 26,
                move: 89
            },
            diamondpearl: {
                tm: 26,
                move: 89
            },
            platinum: {
                tm: 26,
                move: 89
            },
            heartgoldsoulsilver: {
                tm: 26,
                move: 89
            },
            blackwhite: {
                tm: 26,
                move: 89
            },
            colosseum: {
                tm: 26,
                move: 89
            },
            xd: {
                tm: 26,
                move: 89
            },
            black2white2: {
                tm: 26,
                move: 89
            },
            pokemonxy: {
                tm: 26,
                move: 89
            },
            omegarubyalphasapphire: {
                tm: 26,
                move: 89
            }
        },
        names: {
            japanese: "わざマシン２６",
            korean: "기술머신26",
            french: "CT26",
            german: "TM26",
            spanish: "MT26",
            italian: "MT26",
            english: "TM26"
        }
    },
    tm27: {
        num: 532,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 27,
                move: 90
            },
            yellow: {
                tm: 27,
                move: 90
            },
            goldsilver: {
                tm: 27,
                move: 216
            },
            crystal: {
                tm: 27,
                move: 216
            },
            rubysapphire: {
                tm: 27,
                move: 216
            },
            emerald: {
                tm: 27,
                move: 216
            },
            fireredleafgreen: {
                tm: 27,
                move: 216
            },
            diamondpearl: {
                tm: 27,
                move: 216
            },
            platinum: {
                tm: 27,
                move: 216
            },
            heartgoldsoulsilver: {
                tm: 27,
                move: 216
            },
            blackwhite: {
                tm: 27,
                move: 216
            },
            colosseum: {
                tm: 27,
                move: 216
            },
            xd: {
                tm: 27,
                move: 216
            },
            black2white2: {
                tm: 27,
                move: 216
            },
            pokemonxy: {
                tm: 27,
                move: 216
            },
            omegarubyalphasapphire: {
                tm: 27,
                move: 216
            }
        },
        names: {
            japanese: "わざマシン２７",
            korean: "기술머신27",
            french: "CT27",
            german: "TM27",
            spanish: "MT27",
            italian: "MT27",
            english: "TM27"
        }
    },
    tm28: {
        num: 533,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 28,
                move: 91
            },
            yellow: {
                tm: 28,
                move: 91
            },
            goldsilver: {
                tm: 28,
                move: 91
            },
            crystal: {
                tm: 28,
                move: 91
            },
            rubysapphire: {
                tm: 28,
                move: 91
            },
            emerald: {
                tm: 28,
                move: 91
            },
            fireredleafgreen: {
                tm: 28,
                move: 91
            },
            diamondpearl: {
                tm: 28,
                move: 91
            },
            platinum: {
                tm: 28,
                move: 91
            },
            heartgoldsoulsilver: {
                tm: 28,
                move: 91
            },
            blackwhite: {
                tm: 28,
                move: 91
            },
            colosseum: {
                tm: 28,
                move: 91
            },
            xd: {
                tm: 28,
                move: 91
            },
            black2white2: {
                tm: 28,
                move: 91
            },
            pokemonxy: {
                tm: 28,
                move: 91
            },
            omegarubyalphasapphire: {
                tm: 28,
                move: 91
            }
        },
        names: {
            japanese: "わざマシン２８",
            korean: "기술머신28",
            french: "CT28",
            german: "TM28",
            spanish: "MT28",
            italian: "MT28",
            english: "TM28"
        }
    },
    tm29: {
        num: 536,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 29,
                move: 94
            },
            yellow: {
                tm: 29,
                move: 94
            },
            goldsilver: {
                tm: 29,
                move: 94
            },
            crystal: {
                tm: 29,
                move: 94
            },
            rubysapphire: {
                tm: 29,
                move: 94
            },
            emerald: {
                tm: 29,
                move: 94
            },
            fireredleafgreen: {
                tm: 29,
                move: 94
            },
            diamondpearl: {
                tm: 29,
                move: 94
            },
            platinum: {
                tm: 29,
                move: 94
            },
            heartgoldsoulsilver: {
                tm: 29,
                move: 94
            },
            blackwhite: {
                tm: 29,
                move: 94
            },
            colosseum: {
                tm: 29,
                move: 94
            },
            xd: {
                tm: 29,
                move: 94
            },
            black2white2: {
                tm: 29,
                move: 94
            },
            pokemonxy: {
                tm: 29,
                move: 94
            },
            omegarubyalphasapphire: {
                tm: 29,
                move: 94
            }
        },
        names: {
            japanese: "わざマシン２９",
            korean: "기술머신29",
            french: "CT29",
            german: "TM29",
            spanish: "MT29",
            italian: "MT29",
            english: "TM29"
        }
    },
    tm30: {
        num: 537,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 30,
                move: 100
            },
            yellow: {
                tm: 30,
                move: 100
            },
            goldsilver: {
                tm: 30,
                move: 247
            },
            crystal: {
                tm: 30,
                move: 247
            },
            rubysapphire: {
                tm: 30,
                move: 247
            },
            emerald: {
                tm: 30,
                move: 247
            },
            fireredleafgreen: {
                tm: 30,
                move: 247
            },
            diamondpearl: {
                tm: 30,
                move: 247
            },
            platinum: {
                tm: 30,
                move: 247
            },
            heartgoldsoulsilver: {
                tm: 30,
                move: 247
            },
            blackwhite: {
                tm: 30,
                move: 247
            },
            colosseum: {
                tm: 30,
                move: 247
            },
            xd: {
                tm: 30,
                move: 247
            },
            black2white2: {
                tm: 30,
                move: 247
            },
            pokemonxy: {
                tm: 30,
                move: 247
            },
            omegarubyalphasapphire: {
                tm: 30,
                move: 247
            }
        },
        names: {
            japanese: "わざマシン３０",
            korean: "기술머신30",
            french: "CT30",
            german: "TM30",
            spanish: "MT30",
            italian: "MT30",
            english: "TM30"
        }
    },
    tm31: {
        num: 565,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 31,
                move: 102
            },
            yellow: {
                tm: 31,
                move: 102
            },
            goldsilver: {
                tm: 31,
                move: 189
            },
            crystal: {
                tm: 31,
                move: 189
            },
            rubysapphire: {
                tm: 31,
                move: 280
            },
            emerald: {
                tm: 31,
                move: 280
            },
            fireredleafgreen: {
                tm: 31,
                move: 280
            },
            diamondpearl: {
                tm: 31,
                move: 280
            },
            platinum: {
                tm: 31,
                move: 280
            },
            heartgoldsoulsilver: {
                tm: 31,
                move: 280
            },
            blackwhite: {
                tm: 31,
                move: 280
            },
            colosseum: {
                tm: 31,
                move: 280
            },
            xd: {
                tm: 31,
                move: 280
            },
            black2white2: {
                tm: 31,
                move: 280
            },
            pokemonxy: {
                tm: 31,
                move: 280
            },
            omegarubyalphasapphire: {
                tm: 31,
                move: 280
            }
        },
        names: {
            japanese: "わざマシン３１",
            korean: "기술머신31",
            french: "CT31",
            german: "TM31",
            spanish: "MT31",
            italian: "MT31",
            english: "TM31"
        }
    },
    tm32: {
        num: 566,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 32,
                move: 104
            },
            yellow: {
                tm: 32,
                move: 104
            },
            goldsilver: {
                tm: 32,
                move: 104
            },
            crystal: {
                tm: 32,
                move: 104
            },
            rubysapphire: {
                tm: 32,
                move: 104
            },
            emerald: {
                tm: 32,
                move: 104
            },
            fireredleafgreen: {
                tm: 32,
                move: 104
            },
            diamondpearl: {
                tm: 32,
                move: 104
            },
            platinum: {
                tm: 32,
                move: 104
            },
            heartgoldsoulsilver: {
                tm: 32,
                move: 104
            },
            blackwhite: {
                tm: 32,
                move: 104
            },
            colosseum: {
                tm: 32,
                move: 104
            },
            xd: {
                tm: 32,
                move: 104
            },
            black2white2: {
                tm: 32,
                move: 104
            },
            pokemonxy: {
                tm: 32,
                move: 104
            },
            omegarubyalphasapphire: {
                tm: 32,
                move: 104
            }
        },
        names: {
            japanese: "わざマシン３２",
            korean: "기술머신32",
            french: "CT32",
            german: "TM32",
            spanish: "MT32",
            italian: "MT32",
            english: "TM32"
        }
    },
    tm33: {
        num: 567,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 33,
                move: 115
            },
            yellow: {
                tm: 33,
                move: 115
            },
            goldsilver: {
                tm: 33,
                move: 8
            },
            crystal: {
                tm: 33,
                move: 8
            },
            rubysapphire: {
                tm: 33,
                move: 115
            },
            emerald: {
                tm: 33,
                move: 115
            },
            fireredleafgreen: {
                tm: 33,
                move: 115
            },
            diamondpearl: {
                tm: 33,
                move: 115
            },
            platinum: {
                tm: 33,
                move: 115
            },
            heartgoldsoulsilver: {
                tm: 33,
                move: 115
            },
            blackwhite: {
                tm: 33,
                move: 115
            },
            colosseum: {
                tm: 33,
                move: 115
            },
            xd: {
                tm: 33,
                move: 115
            },
            black2white2: {
                tm: 33,
                move: 115
            },
            pokemonxy: {
                tm: 33,
                move: 115
            },
            omegarubyalphasapphire: {
                tm: 33,
                move: 115
            }
        },
        names: {
            japanese: "わざマシン３３",
            korean: "기술머신33",
            french: "CT33",
            german: "TM33",
            spanish: "MT33",
            italian: "MT33",
            english: "TM33"
        }
    },
    tm34: {
        num: 568,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 34,
                move: 117
            },
            yellow: {
                tm: 34,
                move: 117
            },
            goldsilver: {
                tm: 34,
                move: 207
            },
            crystal: {
                tm: 34,
                move: 207
            },
            rubysapphire: {
                tm: 34,
                move: 351
            },
            emerald: {
                tm: 34,
                move: 351
            },
            fireredleafgreen: {
                tm: 34,
                move: 351
            },
            diamondpearl: {
                tm: 34,
                move: 351
            },
            platinum: {
                tm: 34,
                move: 351
            },
            heartgoldsoulsilver: {
                tm: 34,
                move: 351
            },
            blackwhite: {
                tm: 34,
                move: 482
            },
            colosseum: {
                tm: 34,
                move: 351
            },
            xd: {
                tm: 34,
                move: 351
            },
            black2white2: {
                tm: 34,
                move: 482
            },
            pokemonxy: {
                tm: 34,
                move: 482
            },
            omegarubyalphasapphire: {
                tm: 34,
                move: 482
            }
        },
        names: {
            japanese: "わざマシン３４",
            korean: "기술머신34",
            french: "CT34",
            german: "TM34",
            spanish: "MT34",
            italian: "MT34",
            english: "TM34"
        }
    },
    tm35: {
        num: 569,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 35,
                move: 118
            },
            yellow: {
                tm: 35,
                move: 118
            },
            goldsilver: {
                tm: 35,
                move: 214
            },
            crystal: {
                tm: 35,
                move: 214
            },
            rubysapphire: {
                tm: 35,
                move: 53
            },
            emerald: {
                tm: 35,
                move: 53
            },
            fireredleafgreen: {
                tm: 35,
                move: 53
            },
            diamondpearl: {
                tm: 35,
                move: 53
            },
            platinum: {
                tm: 35,
                move: 53
            },
            heartgoldsoulsilver: {
                tm: 35,
                move: 53
            },
            blackwhite: {
                tm: 35,
                move: 53
            },
            colosseum: {
                tm: 35,
                move: 53
            },
            xd: {
                tm: 35,
                move: 53
            },
            black2white2: {
                tm: 35,
                move: 53
            },
            pokemonxy: {
                tm: 35,
                move: 53
            },
            omegarubyalphasapphire: {
                tm: 35,
                move: 53
            }
        },
        names: {
            japanese: "わざマシン３５",
            korean: "기술머신35",
            french: "CT35",
            german: "TM35",
            spanish: "MT35",
            italian: "MT35",
            english: "TM35"
        }
    },
    tm36: {
        num: 570,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 36,
                move: 120
            },
            yellow: {
                tm: 36,
                move: 120
            },
            goldsilver: {
                tm: 36,
                move: 188
            },
            crystal: {
                tm: 36,
                move: 188
            },
            rubysapphire: {
                tm: 36,
                move: 188
            },
            emerald: {
                tm: 36,
                move: 188
            },
            fireredleafgreen: {
                tm: 36,
                move: 188
            },
            diamondpearl: {
                tm: 36,
                move: 188
            },
            platinum: {
                tm: 36,
                move: 188
            },
            heartgoldsoulsilver: {
                tm: 36,
                move: 188
            },
            blackwhite: {
                tm: 36,
                move: 188
            },
            colosseum: {
                tm: 36,
                move: 188
            },
            xd: {
                tm: 36,
                move: 188
            },
            black2white2: {
                tm: 36,
                move: 188
            },
            pokemonxy: {
                tm: 36,
                move: 188
            },
            omegarubyalphasapphire: {
                tm: 36,
                move: 188
            }
        },
        names: {
            japanese: "わざマシン３６",
            korean: "기술머신36",
            french: "CT36",
            german: "TM36",
            spanish: "MT36",
            italian: "MT36",
            english: "TM36"
        }
    },
    tm37: {
        num: 571,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 37,
                move: 121
            },
            yellow: {
                tm: 37,
                move: 121
            },
            goldsilver: {
                tm: 37,
                move: 201
            },
            crystal: {
                tm: 37,
                move: 201
            },
            rubysapphire: {
                tm: 37,
                move: 201
            },
            emerald: {
                tm: 37,
                move: 201
            },
            fireredleafgreen: {
                tm: 37,
                move: 201
            },
            diamondpearl: {
                tm: 37,
                move: 201
            },
            platinum: {
                tm: 37,
                move: 201
            },
            heartgoldsoulsilver: {
                tm: 37,
                move: 201
            },
            blackwhite: {
                tm: 37,
                move: 201
            },
            colosseum: {
                tm: 37,
                move: 201
            },
            xd: {
                tm: 37,
                move: 201
            },
            black2white2: {
                tm: 37,
                move: 201
            },
            pokemonxy: {
                tm: 37,
                move: 201
            },
            omegarubyalphasapphire: {
                tm: 37,
                move: 201
            }
        },
        names: {
            japanese: "わざマシン３７",
            korean: "기술머신37",
            french: "CT37",
            german: "TM37",
            spanish: "MT37",
            italian: "MT37",
            english: "TM37"
        }
    },
    tm38: {
        num: 574,
        cost: 5500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 38,
                move: 126
            },
            yellow: {
                tm: 38,
                move: 126
            },
            goldsilver: {
                tm: 38,
                move: 126
            },
            crystal: {
                tm: 38,
                move: 126
            },
            rubysapphire: {
                tm: 38,
                move: 126
            },
            emerald: {
                tm: 38,
                move: 126
            },
            fireredleafgreen: {
                tm: 38,
                move: 126
            },
            diamondpearl: {
                tm: 38,
                move: 126
            },
            platinum: {
                tm: 38,
                move: 126
            },
            heartgoldsoulsilver: {
                tm: 38,
                move: 126
            },
            blackwhite: {
                tm: 38,
                move: 126
            },
            colosseum: {
                tm: 38,
                move: 126
            },
            xd: {
                tm: 38,
                move: 126
            },
            black2white2: {
                tm: 38,
                move: 126
            },
            pokemonxy: {
                tm: 38,
                move: 126
            },
            omegarubyalphasapphire: {
                tm: 38,
                move: 126
            }
        },
        names: {
            japanese: "わざマシン３８",
            korean: "기술머신38",
            french: "CT38",
            german: "TM38",
            spanish: "MT38",
            italian: "MT38",
            english: "TM38"
        }
    },
    tm39: {
        num: 575,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 39,
                move: 129
            },
            yellow: {
                tm: 39,
                move: 129
            },
            goldsilver: {
                tm: 39,
                move: 129
            },
            crystal: {
                tm: 39,
                move: 129
            },
            rubysapphire: {
                tm: 39,
                move: 317
            },
            emerald: {
                tm: 39,
                move: 317
            },
            fireredleafgreen: {
                tm: 39,
                move: 317
            },
            diamondpearl: {
                tm: 39,
                move: 317
            },
            platinum: {
                tm: 39,
                move: 317
            },
            heartgoldsoulsilver: {
                tm: 39,
                move: 317
            },
            blackwhite: {
                tm: 39,
                move: 317
            },
            colosseum: {
                tm: 39,
                move: 317
            },
            xd: {
                tm: 39,
                move: 317
            },
            black2white2: {
                tm: 39,
                move: 317
            },
            pokemonxy: {
                tm: 39,
                move: 317
            },
            omegarubyalphasapphire: {
                tm: 39,
                move: 317
            }
        },
        names: {
            japanese: "わざマシン３９",
            korean: "기술머신39",
            french: "CT39",
            german: "TM39",
            spanish: "MT39",
            italian: "MT39",
            english: "TM39"
        }
    },
    tm40: {
        num: 577,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 40,
                move: 130
            },
            yellow: {
                tm: 40,
                move: 130
            },
            goldsilver: {
                tm: 40,
                move: 111
            },
            crystal: {
                tm: 40,
                move: 111
            },
            rubysapphire: {
                tm: 40,
                move: 332
            },
            emerald: {
                tm: 40,
                move: 332
            },
            fireredleafgreen: {
                tm: 40,
                move: 332
            },
            diamondpearl: {
                tm: 40,
                move: 332
            },
            platinum: {
                tm: 40,
                move: 332
            },
            heartgoldsoulsilver: {
                tm: 40,
                move: 332
            },
            blackwhite: {
                tm: 40,
                move: 332
            },
            colosseum: {
                tm: 40,
                move: 332
            },
            xd: {
                tm: 40,
                move: 332
            },
            black2white2: {
                tm: 40,
                move: 332
            },
            pokemonxy: {
                tm: 40,
                move: 332
            },
            omegarubyalphasapphire: {
                tm: 40,
                move: 332
            }
        },
        names: {
            japanese: "わざマシン４０",
            korean: "기술머신40",
            french: "CT40",
            german: "TM40",
            spanish: "MT40",
            italian: "MT40",
            english: "TM40"
        }
    },
    tm41: {
        num: 578,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 41,
                move: 135
            },
            yellow: {
                tm: 41,
                move: 135
            },
            goldsilver: {
                tm: 41,
                move: 9
            },
            crystal: {
                tm: 41,
                move: 9
            },
            rubysapphire: {
                tm: 41,
                move: 259
            },
            emerald: {
                tm: 41,
                move: 259
            },
            fireredleafgreen: {
                tm: 41,
                move: 259
            },
            diamondpearl: {
                tm: 41,
                move: 259
            },
            platinum: {
                tm: 41,
                move: 259
            },
            heartgoldsoulsilver: {
                tm: 41,
                move: 259
            },
            blackwhite: {
                tm: 41,
                move: 259
            },
            colosseum: {
                tm: 41,
                move: 259
            },
            xd: {
                tm: 41,
                move: 259
            },
            black2white2: {
                tm: 41,
                move: 259
            },
            pokemonxy: {
                tm: 41,
                move: 259
            },
            omegarubyalphasapphire: {
                tm: 41,
                move: 259
            }
        },
        names: {
            japanese: "わざマシン４１",
            korean: "기술머신41",
            french: "CT41",
            german: "TM41",
            spanish: "MT41",
            italian: "MT41",
            english: "TM41"
        }
    },
    tm42: {
        num: 579,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 42,
                move: 138
            },
            yellow: {
                tm: 42,
                move: 138
            },
            goldsilver: {
                tm: 42,
                move: 138
            },
            crystal: {
                tm: 42,
                move: 138
            },
            rubysapphire: {
                tm: 42,
                move: 263
            },
            emerald: {
                tm: 42,
                move: 263
            },
            fireredleafgreen: {
                tm: 42,
                move: 263
            },
            diamondpearl: {
                tm: 42,
                move: 263
            },
            platinum: {
                tm: 42,
                move: 263
            },
            heartgoldsoulsilver: {
                tm: 42,
                move: 263
            },
            blackwhite: {
                tm: 42,
                move: 263
            },
            colosseum: {
                tm: 42,
                move: 263
            },
            xd: {
                tm: 42,
                move: 263
            },
            black2white2: {
                tm: 42,
                move: 263
            },
            pokemonxy: {
                tm: 42,
                move: 263
            },
            omegarubyalphasapphire: {
                tm: 42,
                move: 263
            }
        },
        names: {
            japanese: "わざマシン４２",
            korean: "기술머신42",
            french: "CT42",
            german: "TM42",
            spanish: "MT42",
            italian: "MT42",
            english: "TM42"
        }
    },
    tm43: {
        num: 580,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 43,
                move: 143
            },
            yellow: {
                tm: 43,
                move: 143
            },
            goldsilver: {
                tm: 43,
                move: 197
            },
            crystal: {
                tm: 43,
                move: 197
            },
            rubysapphire: {
                tm: 43,
                move: 290
            },
            emerald: {
                tm: 43,
                move: 290
            },
            fireredleafgreen: {
                tm: 43,
                move: 290
            },
            diamondpearl: {
                tm: 43,
                move: 290
            },
            platinum: {
                tm: 43,
                move: 290
            },
            heartgoldsoulsilver: {
                tm: 43,
                move: 290
            },
            blackwhite: {
                tm: 43,
                move: 488
            },
            colosseum: {
                tm: 43,
                move: 290
            },
            xd: {
                tm: 43,
                move: 290
            },
            black2white2: {
                tm: 43,
                move: 488
            },
            pokemonxy: {
                tm: 43,
                move: 488
            },
            omegarubyalphasapphire: {
                tm: 43,
                move: 488
            }
        },
        names: {
            japanese: "わざマシン４３",
            korean: "기술머신43",
            french: "CT43",
            german: "TM43",
            spanish: "MT43",
            italian: "MT43",
            english: "TM43"
        }
    },
    tm44: {
        num: 581,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 44,
                move: 156
            },
            yellow: {
                tm: 44,
                move: 156
            },
            goldsilver: {
                tm: 44,
                move: 156
            },
            crystal: {
                tm: 44,
                move: 156
            },
            rubysapphire: {
                tm: 44,
                move: 156
            },
            emerald: {
                tm: 44,
                move: 156
            },
            fireredleafgreen: {
                tm: 44,
                move: 156
            },
            diamondpearl: {
                tm: 44,
                move: 156
            },
            platinum: {
                tm: 44,
                move: 156
            },
            heartgoldsoulsilver: {
                tm: 44,
                move: 156
            },
            blackwhite: {
                tm: 44,
                move: 156
            },
            colosseum: {
                tm: 44,
                move: 156
            },
            xd: {
                tm: 44,
                move: 156
            },
            black2white2: {
                tm: 44,
                move: 156
            },
            pokemonxy: {
                tm: 44,
                move: 156
            },
            omegarubyalphasapphire: {
                tm: 44,
                move: 156
            }
        },
        names: {
            japanese: "わざマシン４４",
            korean: "기술머신44",
            french: "CT44",
            german: "TM44",
            spanish: "MT44",
            italian: "MT44",
            english: "TM44"
        }
    },
    tm45: {
        num: 582,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 45,
                move: 86
            },
            yellow: {
                tm: 45,
                move: 86
            },
            goldsilver: {
                tm: 45,
                move: 213
            },
            crystal: {
                tm: 45,
                move: 213
            },
            rubysapphire: {
                tm: 45,
                move: 213
            },
            emerald: {
                tm: 45,
                move: 213
            },
            fireredleafgreen: {
                tm: 45,
                move: 213
            },
            diamondpearl: {
                tm: 45,
                move: 213
            },
            platinum: {
                tm: 45,
                move: 213
            },
            heartgoldsoulsilver: {
                tm: 45,
                move: 213
            },
            blackwhite: {
                tm: 45,
                move: 213
            },
            colosseum: {
                tm: 45,
                move: 213
            },
            xd: {
                tm: 45,
                move: 213
            },
            black2white2: {
                tm: 45,
                move: 213
            },
            pokemonxy: {
                tm: 45,
                move: 213
            },
            omegarubyalphasapphire: {
                tm: 45,
                move: 213
            }
        },
        names: {
            japanese: "わざマシン４５",
            korean: "기술머신45",
            french: "CT45",
            german: "TM45",
            spanish: "MT45",
            italian: "MT45",
            english: "TM45"
        }
    },
    tm46: {
        num: 583,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 46,
                move: 149
            },
            yellow: {
                tm: 46,
                move: 149
            },
            goldsilver: {
                tm: 46,
                move: 168
            },
            crystal: {
                tm: 46,
                move: 168
            },
            rubysapphire: {
                tm: 46,
                move: 168
            },
            emerald: {
                tm: 46,
                move: 168
            },
            fireredleafgreen: {
                tm: 46,
                move: 168
            },
            diamondpearl: {
                tm: 46,
                move: 168
            },
            platinum: {
                tm: 46,
                move: 168
            },
            heartgoldsoulsilver: {
                tm: 46,
                move: 168
            },
            blackwhite: {
                tm: 46,
                move: 168
            },
            colosseum: {
                tm: 46,
                move: 168
            },
            xd: {
                tm: 46,
                move: 168
            },
            black2white2: {
                tm: 46,
                move: 168
            },
            pokemonxy: {
                tm: 46,
                move: 168
            },
            omegarubyalphasapphire: {
                tm: 46,
                move: 168
            }
        },
        names: {
            japanese: "わざマシン４６",
            korean: "기술머신46",
            french: "CT46",
            german: "TM46",
            spanish: "MT46",
            italian: "MT46",
            english: "TM46"
        }
    },
    tm47: {
        num: 584,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 47,
                move: 153
            },
            yellow: {
                tm: 47,
                move: 153
            },
            goldsilver: {
                tm: 47,
                move: 211
            },
            crystal: {
                tm: 47,
                move: 211
            },
            rubysapphire: {
                tm: 47,
                move: 211
            },
            emerald: {
                tm: 47,
                move: 211
            },
            fireredleafgreen: {
                tm: 47,
                move: 211
            },
            diamondpearl: {
                tm: 47,
                move: 211
            },
            platinum: {
                tm: 47,
                move: 211
            },
            heartgoldsoulsilver: {
                tm: 47,
                move: 211
            },
            blackwhite: {
                tm: 47,
                move: 490
            },
            colosseum: {
                tm: 47,
                move: 211
            },
            xd: {
                tm: 47,
                move: 211
            },
            black2white2: {
                tm: 47,
                move: 490
            },
            pokemonxy: {
                tm: 47,
                move: 490
            },
            omegarubyalphasapphire: {
                tm: 47,
                move: 490
            }
        },
        names: {
            japanese: "わざマシン４７",
            korean: "기술머신47",
            french: "CT47",
            german: "TM47",
            spanish: "MT47",
            italian: "MT47",
            english: "TM47"
        }
    },
    tm48: {
        num: 585,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 48,
                move: 157
            },
            yellow: {
                tm: 48,
                move: 157
            },
            goldsilver: {
                tm: 48,
                move: 7
            },
            crystal: {
                tm: 48,
                move: 7
            },
            rubysapphire: {
                tm: 48,
                move: 285
            },
            emerald: {
                tm: 48,
                move: 285
            },
            fireredleafgreen: {
                tm: 48,
                move: 285
            },
            diamondpearl: {
                tm: 48,
                move: 285
            },
            platinum: {
                tm: 48,
                move: 285
            },
            heartgoldsoulsilver: {
                tm: 48,
                move: 285
            },
            blackwhite: {
                tm: 48,
                move: 496
            },
            colosseum: {
                tm: 48,
                move: 285
            },
            xd: {
                tm: 48,
                move: 285
            },
            black2white2: {
                tm: 48,
                move: 496
            },
            pokemonxy: {
                tm: 48,
                move: 496
            },
            omegarubyalphasapphire: {
                tm: 48,
                move: 496
            }
        },
        names: {
            japanese: "わざマシン４８",
            korean: "기술머신48",
            french: "CT48",
            german: "TM48",
            spanish: "MT48",
            italian: "MT48",
            english: "TM48"
        }
    },
    tm49: {
        num: 586,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 49,
                move: 161
            },
            yellow: {
                tm: 49,
                move: 161
            },
            goldsilver: {
                tm: 49,
                move: 210
            },
            crystal: {
                tm: 49,
                move: 210
            },
            rubysapphire: {
                tm: 49,
                move: 289
            },
            emerald: {
                tm: 49,
                move: 289
            },
            fireredleafgreen: {
                tm: 49,
                move: 289
            },
            diamondpearl: {
                tm: 49,
                move: 289
            },
            platinum: {
                tm: 49,
                move: 289
            },
            heartgoldsoulsilver: {
                tm: 49,
                move: 289
            },
            blackwhite: {
                tm: 49,
                move: 497
            },
            colosseum: {
                tm: 49,
                move: 289
            },
            xd: {
                tm: 49,
                move: 289
            },
            black2white2: {
                tm: 49,
                move: 497
            },
            pokemonxy: {
                tm: 49,
                move: 497
            },
            omegarubyalphasapphire: {
                tm: 49,
                move: 497
            }
        },
        names: {
            japanese: "わざマシン４９",
            korean: "기술머신49",
            french: "CT49",
            german: "TM49",
            spanish: "MT49",
            italian: "MT49",
            english: "TM49"
        }
    },
    tm50: {
        num: 587,
        cost: 5500,
        itemcategory: "tm",
        tm: {
            redblue: {
                tm: 50,
                move: 164
            },
            yellow: {
                tm: 50,
                move: 164
            },
            goldsilver: {
                tm: 50,
                move: 171
            },
            crystal: {
                tm: 50,
                move: 171
            },
            rubysapphire: {
                tm: 50,
                move: 315
            },
            emerald: {
                tm: 50,
                move: 315
            },
            fireredleafgreen: {
                tm: 50,
                move: 315
            },
            diamondpearl: {
                tm: 50,
                move: 315
            },
            platinum: {
                tm: 50,
                move: 315
            },
            heartgoldsoulsilver: {
                tm: 50,
                move: 315
            },
            blackwhite: {
                tm: 50,
                move: 315
            },
            colosseum: {
                tm: 50,
                move: 315
            },
            xd: {
                tm: 50,
                move: 315
            },
            black2white2: {
                tm: 50,
                move: 315
            },
            pokemonxy: {
                tm: 50,
                move: 315
            },
            omegarubyalphasapphire: {
                tm: 50,
                move: 315
            }
        },
        names: {
            japanese: "わざマシン５０",
            korean: "기술머신50",
            french: "CT50",
            german: "TM50",
            spanish: "MT50",
            italian: "MT50",
            english: "TM50"
        }
    },
    tm51: {
        num: 588,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 51,
                move: 355
            },
            platinum: {
                tm: 51,
                move: 355
            },
            heartgoldsoulsilver: {
                tm: 51,
                move: 355
            },
            blackwhite: {
                tm: 51,
                move: 502
            },
            black2white2: {
                tm: 51,
                move: 502
            },
            pokemonxy: {
                tm: 51,
                move: 211
            },
            omegarubyalphasapphire: {
                tm: 51,
                move: 211
            }
        },
        names: {
            japanese: "わざマシン５１",
            korean: "기술머신51",
            french: "CT51",
            german: "TM51",
            spanish: "MT51",
            italian: "MT51",
            english: "TM51"
        }
    },
    tm52: {
        num: 589,
        cost: 5500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 52,
                move: 411
            },
            platinum: {
                tm: 52,
                move: 411
            },
            heartgoldsoulsilver: {
                tm: 52,
                move: 411
            },
            blackwhite: {
                tm: 52,
                move: 411
            },
            black2white2: {
                tm: 52,
                move: 411
            },
            pokemonxy: {
                tm: 52,
                move: 411
            },
            omegarubyalphasapphire: {
                tm: 52,
                move: 411
            }
        },
        names: {
            japanese: "わざマシン５２",
            korean: "기술머신52",
            french: "CT52",
            german: "TM52",
            spanish: "MT52",
            italian: "MT52",
            english: "TM52"
        }
    },
    tm53: {
        num: 590,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 53,
                move: 412
            },
            platinum: {
                tm: 53,
                move: 412
            },
            heartgoldsoulsilver: {
                tm: 53,
                move: 412
            },
            blackwhite: {
                tm: 53,
                move: 412
            },
            black2white2: {
                tm: 53,
                move: 412
            },
            pokemonxy: {
                tm: 53,
                move: 412
            },
            omegarubyalphasapphire: {
                tm: 53,
                move: 412
            }
        },
        names: {
            japanese: "わざマシン５３",
            korean: "기술머신53",
            french: "CT53",
            german: "TM53",
            spanish: "MT53",
            italian: "MT53",
            english: "TM53"
        }
    },
    tm54: {
        num: 591,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 54,
                move: 206
            },
            platinum: {
                tm: 54,
                move: 206
            },
            heartgoldsoulsilver: {
                tm: 54,
                move: 206
            },
            blackwhite: {
                tm: 54,
                move: 206
            },
            black2white2: {
                tm: 54,
                move: 206
            },
            pokemonxy: {
                tm: 54,
                move: 206
            },
            omegarubyalphasapphire: {
                tm: 54,
                move: 206
            }
        },
        names: {
            japanese: "わざマシン５４",
            korean: "기술머신54",
            french: "CT54",
            german: "TM54",
            spanish: "MT54",
            italian: "MT54",
            english: "TM54"
        }
    },
    tm55: {
        num: 592,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 55,
                move: 362
            },
            platinum: {
                tm: 55,
                move: 362
            },
            heartgoldsoulsilver: {
                tm: 55,
                move: 362
            },
            blackwhite: {
                tm: 55,
                move: 503
            },
            black2white2: {
                tm: 55,
                move: 503
            },
            pokemonxy: {
                tm: 55,
                move: 503
            },
            omegarubyalphasapphire: {
                tm: 55,
                move: 503
            }
        },
        names: {
            japanese: "わざマシン５５",
            korean: "기술머신55",
            french: "CT55",
            german: "TM55",
            spanish: "MT55",
            italian: "MT55",
            english: "TM55"
        }
    },
    tm56: {
        num: 593,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 56,
                move: 374
            },
            platinum: {
                tm: 56,
                move: 374
            },
            heartgoldsoulsilver: {
                tm: 56,
                move: 374
            },
            blackwhite: {
                tm: 56,
                move: 374
            },
            black2white2: {
                tm: 56,
                move: 374
            },
            pokemonxy: {
                tm: 56,
                move: 374
            },
            omegarubyalphasapphire: {
                tm: 56,
                move: 374
            }
        },
        names: {
            japanese: "わざマシン５６",
            korean: "기술머신56",
            french: "CT56",
            german: "TM56",
            spanish: "MT56",
            italian: "MT56",
            english: "TM56"
        }
    },
    tm57: {
        num: 594,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 57,
                move: 451
            },
            platinum: {
                tm: 57,
                move: 451
            },
            heartgoldsoulsilver: {
                tm: 57,
                move: 451
            },
            blackwhite: {
                tm: 57,
                move: 451
            },
            black2white2: {
                tm: 57,
                move: 451
            },
            pokemonxy: {
                tm: 57,
                move: 451
            },
            omegarubyalphasapphire: {
                tm: 57,
                move: 451
            }
        },
        names: {
            japanese: "わざマシン５７",
            korean: "기술머신57",
            french: "CT57",
            german: "TM57",
            spanish: "MT57",
            italian: "MT57",
            english: "TM57"
        }
    },
    tm58: {
        num: 595,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 58,
                move: 203
            },
            platinum: {
                tm: 58,
                move: 203
            },
            heartgoldsoulsilver: {
                tm: 58,
                move: 203
            },
            blackwhite: {
                tm: 58,
                move: 507
            },
            black2white2: {
                tm: 58,
                move: 507
            },
            pokemonxy: {
                tm: 58,
                move: 507
            },
            omegarubyalphasapphire: {
                tm: 58,
                move: 507
            }
        },
        names: {
            japanese: "わざマシン５８",
            korean: "기술머신58",
            french: "CT58",
            german: "TM58",
            spanish: "MT58",
            italian: "MT58",
            english: "TM58"
        }
    },
    tm59: {
        num: 596,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 59,
                move: 406
            },
            platinum: {
                tm: 59,
                move: 406
            },
            heartgoldsoulsilver: {
                tm: 59,
                move: 406
            },
            blackwhite: {
                tm: 59,
                move: 510
            },
            black2white2: {
                tm: 59,
                move: 510
            },
            pokemonxy: {
                tm: 59,
                move: 510
            },
            omegarubyalphasapphire: {
                tm: 59,
                move: 510
            }
        },
        names: {
            japanese: "わざマシン５９",
            korean: "기술머신59",
            french: "CT59",
            german: "TM59",
            spanish: "MT59",
            italian: "MT59",
            english: "TM59"
        }
    },
    tm60: {
        num: 597,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 60,
                move: 409
            },
            platinum: {
                tm: 60,
                move: 409
            },
            heartgoldsoulsilver: {
                tm: 60,
                move: 409
            },
            blackwhite: {
                tm: 60,
                move: 511
            },
            black2white2: {
                tm: 60,
                move: 511
            },
            pokemonxy: {
                tm: 60,
                move: 511
            },
            omegarubyalphasapphire: {
                tm: 60,
                move: 511
            }
        },
        names: {
            japanese: "わざマシン６０",
            korean: "기술머신60",
            french: "CT60",
            german: "TM60",
            spanish: "MT60",
            italian: "MT60",
            english: "TM60"
        }
    },
    tm61: {
        num: 598,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 61,
                move: 261
            },
            platinum: {
                tm: 61,
                move: 261
            },
            heartgoldsoulsilver: {
                tm: 61,
                move: 261
            },
            blackwhite: {
                tm: 61,
                move: 261
            },
            black2white2: {
                tm: 61,
                move: 261
            },
            pokemonxy: {
                tm: 61,
                move: 261
            },
            omegarubyalphasapphire: {
                tm: 61,
                move: 261
            }
        },
        names: {
            japanese: "わざマシン６１",
            korean: "기술머신61",
            french: "CT61",
            german: "TM61",
            spanish: "MT61",
            italian: "MT61",
            english: "TM61"
        }
    },
    tm62: {
        num: 599,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 62,
                move: 318
            },
            platinum: {
                tm: 62,
                move: 318
            },
            heartgoldsoulsilver: {
                tm: 62,
                move: 318
            },
            blackwhite: {
                tm: 62,
                move: 512
            },
            black2white2: {
                tm: 62,
                move: 512
            },
            pokemonxy: {
                tm: 62,
                move: 512
            },
            omegarubyalphasapphire: {
                tm: 62,
                move: 512
            }
        },
        names: {
            japanese: "わざマシン６２",
            korean: "기술머신62",
            french: "CT62",
            german: "TM62",
            spanish: "MT62",
            italian: "MT62",
            english: "TM62"
        }
    },
    tm63: {
        num: 600,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 63,
                move: 373
            },
            platinum: {
                tm: 63,
                move: 373
            },
            heartgoldsoulsilver: {
                tm: 63,
                move: 373
            },
            blackwhite: {
                tm: 63,
                move: 373
            },
            black2white2: {
                tm: 63,
                move: 373
            },
            pokemonxy: {
                tm: 63,
                move: 373
            },
            omegarubyalphasapphire: {
                tm: 63,
                move: 373
            }
        },
        names: {
            japanese: "わざマシン６３",
            korean: "기술머신63",
            french: "CT63",
            german: "TM63",
            spanish: "MT63",
            italian: "MT63",
            english: "TM63"
        }
    },
    tm64: {
        num: 601,
        cost: 7500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 64,
                move: 153
            },
            platinum: {
                tm: 64,
                move: 153
            },
            heartgoldsoulsilver: {
                tm: 64,
                move: 153
            },
            blackwhite: {
                tm: 64,
                move: 153
            },
            black2white2: {
                tm: 64,
                move: 153
            },
            pokemonxy: {
                tm: 64,
                move: 153
            },
            omegarubyalphasapphire: {
                tm: 64,
                move: 153
            }
        },
        names: {
            japanese: "わざマシン６４",
            korean: "기술머신64",
            french: "CT64",
            german: "TM64",
            spanish: "MT64",
            italian: "MT64",
            english: "TM64"
        }
    },
    tm65: {
        num: 602,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 65,
                move: 421
            },
            platinum: {
                tm: 65,
                move: 421
            },
            heartgoldsoulsilver: {
                tm: 65,
                move: 421
            },
            blackwhite: {
                tm: 65,
                move: 421
            },
            black2white2: {
                tm: 65,
                move: 421
            },
            pokemonxy: {
                tm: 65,
                move: 421
            },
            omegarubyalphasapphire: {
                tm: 65,
                move: 421
            }
        },
        names: {
            japanese: "わざマシン６５",
            korean: "기술머신65",
            french: "CT65",
            german: "TM65",
            spanish: "MT65",
            italian: "MT65",
            english: "TM65"
        }
    },
    tm66: {
        num: 603,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 66,
                move: 371
            },
            platinum: {
                tm: 66,
                move: 371
            },
            heartgoldsoulsilver: {
                tm: 66,
                move: 371
            },
            blackwhite: {
                tm: 66,
                move: 371
            },
            black2white2: {
                tm: 66,
                move: 371
            },
            pokemonxy: {
                tm: 66,
                move: 371
            },
            omegarubyalphasapphire: {
                tm: 66,
                move: 371
            }
        },
        names: {
            japanese: "わざマシン６６",
            korean: "기술머신66",
            french: "CT66",
            german: "TM66",
            spanish: "MT66",
            italian: "MT66",
            english: "TM66"
        }
    },
    tm67: {
        num: 604,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 67,
                move: 278
            },
            platinum: {
                tm: 67,
                move: 278
            },
            heartgoldsoulsilver: {
                tm: 67,
                move: 278
            },
            blackwhite: {
                tm: 67,
                move: 514
            },
            black2white2: {
                tm: 67,
                move: 514
            },
            pokemonxy: {
                tm: 67,
                move: 514
            },
            omegarubyalphasapphire: {
                tm: 67,
                move: 514
            }
        },
        names: {
            japanese: "わざマシン６７",
            korean: "기술머신67",
            french: "CT67",
            german: "TM67",
            spanish: "MT67",
            italian: "MT67",
            english: "TM67"
        }
    },
    tm68: {
        num: 605,
        cost: 7500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 68,
                move: 416
            },
            platinum: {
                tm: 68,
                move: 416
            },
            heartgoldsoulsilver: {
                tm: 68,
                move: 416
            },
            blackwhite: {
                tm: 68,
                move: 416
            },
            black2white2: {
                tm: 68,
                move: 416
            },
            pokemonxy: {
                tm: 68,
                move: 416
            },
            omegarubyalphasapphire: {
                tm: 68,
                move: 416
            }
        },
        names: {
            japanese: "わざマシン６８",
            korean: "기술머신68",
            french: "CT68",
            german: "TM68",
            spanish: "MT68",
            italian: "MT68",
            english: "TM68"
        }
    },
    tm69: {
        num: 606,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 69,
                move: 397
            },
            platinum: {
                tm: 69,
                move: 397
            },
            heartgoldsoulsilver: {
                tm: 69,
                move: 397
            },
            blackwhite: {
                tm: 69,
                move: 397
            },
            black2white2: {
                tm: 69,
                move: 397
            },
            pokemonxy: {
                tm: 69,
                move: 397
            },
            omegarubyalphasapphire: {
                tm: 69,
                move: 397
            }
        },
        names: {
            japanese: "わざマシン６９",
            korean: "기술머신69",
            french: "CT69",
            german: "TM69",
            spanish: "MT69",
            italian: "MT69",
            english: "TM69"
        }
    },
    tm70: {
        num: 607,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 70,
                move: 148
            },
            platinum: {
                tm: 70,
                move: 148
            },
            heartgoldsoulsilver: {
                tm: 70,
                move: 148
            },
            blackwhite: {
                tm: 70,
                move: 148
            },
            black2white2: {
                tm: 70,
                move: 148
            },
            pokemonxy: {
                tm: 70,
                move: 148
            },
            omegarubyalphasapphire: {
                tm: 70,
                move: 148
            }
        },
        names: {
            japanese: "わざマシン７０",
            korean: "기술머신70",
            french: "CT70",
            german: "TM70",
            spanish: "MT70",
            italian: "MT70",
            english: "TM70"
        }
    },
    tm71: {
        num: 608,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 71,
                move: 444
            },
            platinum: {
                tm: 71,
                move: 444
            },
            heartgoldsoulsilver: {
                tm: 71,
                move: 444
            },
            blackwhite: {
                tm: 71,
                move: 444
            },
            black2white2: {
                tm: 71,
                move: 444
            },
            pokemonxy: {
                tm: 71,
                move: 444
            },
            omegarubyalphasapphire: {
                tm: 71,
                move: 444
            }
        },
        names: {
            japanese: "わざマシン７１",
            korean: "기술머신71",
            french: "CT71",
            german: "TM71",
            spanish: "MT71",
            italian: "MT71",
            english: "TM71"
        }
    },
    tm72: {
        num: 609,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 72,
                move: 419
            },
            platinum: {
                tm: 72,
                move: 419
            },
            heartgoldsoulsilver: {
                tm: 72,
                move: 419
            },
            blackwhite: {
                tm: 72,
                move: 521
            },
            black2white2: {
                tm: 72,
                move: 521
            },
            pokemonxy: {
                tm: 72,
                move: 521
            },
            omegarubyalphasapphire: {
                tm: 72,
                move: 521
            }
        },
        names: {
            japanese: "わざマシン７２",
            korean: "기술머신72",
            french: "CT72",
            german: "TM72",
            spanish: "MT72",
            italian: "MT72",
            english: "TM72"
        }
    },
    tm73: {
        num: 610,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 73,
                move: 86
            },
            platinum: {
                tm: 73,
                move: 86
            },
            heartgoldsoulsilver: {
                tm: 73,
                move: 86
            },
            blackwhite: {
                tm: 73,
                move: 86
            },
            black2white2: {
                tm: 73,
                move: 86
            },
            pokemonxy: {
                tm: 73,
                move: 86
            },
            omegarubyalphasapphire: {
                tm: 73,
                move: 86
            }
        },
        names: {
            japanese: "わざマシン７３",
            korean: "기술머신73",
            french: "CT73",
            german: "TM73",
            spanish: "MT73",
            italian: "MT73",
            english: "TM73"
        }
    },
    tm74: {
        num: 611,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 74,
                move: 360
            },
            platinum: {
                tm: 74,
                move: 360
            },
            heartgoldsoulsilver: {
                tm: 74,
                move: 360
            },
            blackwhite: {
                tm: 74,
                move: 360
            },
            black2white2: {
                tm: 74,
                move: 360
            },
            pokemonxy: {
                tm: 74,
                move: 360
            },
            omegarubyalphasapphire: {
                tm: 74,
                move: 360
            }
        },
        names: {
            japanese: "わざマシン７４",
            korean: "기술머신74",
            french: "CT74",
            german: "TM74",
            spanish: "MT74",
            italian: "MT74",
            english: "TM74"
        }
    },
    tm75: {
        num: 612,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 75,
                move: 14
            },
            platinum: {
                tm: 75,
                move: 14
            },
            heartgoldsoulsilver: {
                tm: 75,
                move: 14
            },
            blackwhite: {
                tm: 75,
                move: 14
            },
            black2white2: {
                tm: 75,
                move: 14
            },
            pokemonxy: {
                tm: 75,
                move: 14
            },
            omegarubyalphasapphire: {
                tm: 75,
                move: 14
            }
        },
        names: {
            japanese: "わざマシン７５",
            korean: "기술머신75",
            french: "CT75",
            german: "TM75",
            spanish: "MT75",
            italian: "MT75",
            english: "TM75"
        }
    },
    tm76: {
        num: 613,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 76,
                move: 446
            },
            platinum: {
                tm: 76,
                move: 446
            },
            heartgoldsoulsilver: {
                tm: 76,
                move: 446
            },
            blackwhite: {
                tm: 76,
                move: 522
            },
            black2white2: {
                tm: 76,
                move: 522
            },
            pokemonxy: {
                tm: 76,
                move: 522
            },
            omegarubyalphasapphire: {
                tm: 76,
                move: 522
            }
        },
        names: {
            japanese: "わざマシン７６",
            korean: "기술머신76",
            french: "CT76",
            german: "TM76",
            spanish: "MT76",
            italian: "MT76",
            english: "TM76"
        }
    },
    tm77: {
        num: 614,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 77,
                move: 244
            },
            platinum: {
                tm: 77,
                move: 244
            },
            heartgoldsoulsilver: {
                tm: 77,
                move: 244
            },
            blackwhite: {
                tm: 77,
                move: 244
            },
            black2white2: {
                tm: 77,
                move: 244
            },
            pokemonxy: {
                tm: 77,
                move: 244
            },
            omegarubyalphasapphire: {
                tm: 77,
                move: 244
            }
        },
        names: {
            japanese: "わざマシン７７",
            korean: "기술머신77",
            french: "CT77",
            german: "TM77",
            spanish: "MT77",
            italian: "MT77",
            english: "TM77"
        }
    },
    tm78: {
        num: 615,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 78,
                move: 445
            },
            platinum: {
                tm: 78,
                move: 445
            },
            heartgoldsoulsilver: {
                tm: 78,
                move: 445
            },
            blackwhite: {
                tm: 78,
                move: 523
            },
            black2white2: {
                tm: 78,
                move: 523
            },
            pokemonxy: {
                tm: 78,
                move: 523
            },
            omegarubyalphasapphire: {
                tm: 78,
                move: 523
            }
        },
        names: {
            japanese: "わざマシン７８",
            korean: "기술머신78",
            french: "CT78",
            german: "TM78",
            spanish: "MT78",
            italian: "MT78",
            english: "TM78"
        }
    },
    tm79: {
        num: 616,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 79,
                move: 399
            },
            platinum: {
                tm: 79,
                move: 399
            },
            heartgoldsoulsilver: {
                tm: 79,
                move: 399
            },
            blackwhite: {
                tm: 79,
                move: 524
            },
            black2white2: {
                tm: 79,
                move: 524
            },
            pokemonxy: {
                tm: 79,
                move: 524
            },
            omegarubyalphasapphire: {
                tm: 79,
                move: 524
            }
        },
        names: {
            japanese: "わざマシン７９",
            korean: "기술머신79",
            french: "CT79",
            german: "TM79",
            spanish: "MT79",
            italian: "MT79",
            english: "TM79"
        }
    },
    tm80: {
        num: 617,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 80,
                move: 157
            },
            platinum: {
                tm: 80,
                move: 157
            },
            heartgoldsoulsilver: {
                tm: 80,
                move: 157
            },
            blackwhite: {
                tm: 80,
                move: 157
            },
            black2white2: {
                tm: 80,
                move: 157
            },
            pokemonxy: {
                tm: 80,
                move: 157
            },
            omegarubyalphasapphire: {
                tm: 80,
                move: 157
            }
        },
        names: {
            japanese: "わざマシン８０",
            korean: "기술머신80",
            french: "CT80",
            german: "TM80",
            spanish: "MT80",
            italian: "MT80",
            english: "TM80"
        }
    },
    tm81: {
        num: 618,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 81,
                move: 404
            },
            platinum: {
                tm: 81,
                move: 404
            },
            heartgoldsoulsilver: {
                tm: 81,
                move: 404
            },
            blackwhite: {
                tm: 81,
                move: 404
            },
            black2white2: {
                tm: 81,
                move: 404
            },
            pokemonxy: {
                tm: 81,
                move: 404
            },
            omegarubyalphasapphire: {
                tm: 81,
                move: 404
            }
        },
        names: {
            japanese: "わざマシン８１",
            korean: "기술머신81",
            french: "CT81",
            german: "TM81",
            spanish: "MT81",
            italian: "MT81",
            english: "TM81"
        }
    },
    tm82: {
        num: 619,
        cost: 1000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 82,
                move: 214
            },
            platinum: {
                tm: 82,
                move: 214
            },
            heartgoldsoulsilver: {
                tm: 82,
                move: 214
            },
            blackwhite: {
                tm: 82,
                move: 525
            },
            black2white2: {
                tm: 82,
                move: 525
            },
            pokemonxy: {
                tm: 82,
                move: 525
            },
            omegarubyalphasapphire: {
                tm: 82,
                move: 525
            }
        },
        names: {
            japanese: "わざマシン８２",
            korean: "기술머신82",
            french: "CT82",
            german: "TM82",
            spanish: "MT82",
            italian: "MT82",
            english: "TM82"
        }
    },
    tm83: {
        num: 620,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 83,
                move: 363
            },
            platinum: {
                tm: 83,
                move: 363
            },
            heartgoldsoulsilver: {
                tm: 83,
                move: 363
            },
            blackwhite: {
                tm: 83,
                move: 526
            },
            black2white2: {
                tm: 83,
                move: 526
            },
            pokemonxy: {
                tm: 83,
                move: 611
            },
            omegarubyalphasapphire: {
                tm: 83,
                move: 611
            }
        },
        names: {
            japanese: "わざマシン８３",
            korean: "기술머신83",
            french: "CT83",
            german: "TM83",
            spanish: "MT83",
            italian: "MT83",
            english: "TM83"
        }
    },
    tm84: {
        num: 621,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 84,
                move: 398
            },
            platinum: {
                tm: 84,
                move: 398
            },
            heartgoldsoulsilver: {
                tm: 84,
                move: 398
            },
            blackwhite: {
                tm: 84,
                move: 398
            },
            black2white2: {
                tm: 84,
                move: 398
            },
            pokemonxy: {
                tm: 84,
                move: 398
            },
            omegarubyalphasapphire: {
                tm: 84,
                move: 398
            }
        },
        names: {
            japanese: "わざマシン８４",
            korean: "기술머신84",
            french: "CT84",
            german: "TM84",
            spanish: "MT84",
            italian: "MT84",
            english: "TM84"
        }
    },
    tm85: {
        num: 622,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 85,
                move: 138
            },
            platinum: {
                tm: 85,
                move: 138
            },
            heartgoldsoulsilver: {
                tm: 85,
                move: 138
            },
            blackwhite: {
                tm: 85,
                move: 138
            },
            black2white2: {
                tm: 85,
                move: 138
            },
            pokemonxy: {
                tm: 85,
                move: 138
            },
            omegarubyalphasapphire: {
                tm: 85,
                move: 138
            }
        },
        names: {
            japanese: "わざマシン８５",
            korean: "기술머신85",
            french: "CT85",
            german: "TM85",
            spanish: "MT85",
            italian: "MT85",
            english: "TM85"
        }
    },
    tm86: {
        num: 623,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 86,
                move: 447
            },
            platinum: {
                tm: 86,
                move: 447
            },
            heartgoldsoulsilver: {
                tm: 86,
                move: 447
            },
            blackwhite: {
                tm: 86,
                move: 447
            },
            black2white2: {
                tm: 86,
                move: 447
            },
            pokemonxy: {
                tm: 86,
                move: 447
            },
            omegarubyalphasapphire: {
                tm: 86,
                move: 447
            }
        },
        names: {
            japanese: "わざマシン８６",
            korean: "기술머신86",
            french: "CT86",
            german: "TM86",
            spanish: "MT86",
            italian: "MT86",
            english: "TM86"
        }
    },
    tm87: {
        num: 624,
        cost: 1500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 87,
                move: 207
            },
            platinum: {
                tm: 87,
                move: 207
            },
            heartgoldsoulsilver: {
                tm: 87,
                move: 207
            },
            blackwhite: {
                tm: 87,
                move: 207
            },
            black2white2: {
                tm: 87,
                move: 207
            },
            pokemonxy: {
                tm: 87,
                move: 207
            },
            omegarubyalphasapphire: {
                tm: 87,
                move: 207
            }
        },
        names: {
            japanese: "わざマシン８７",
            korean: "기술머신87",
            french: "CT87",
            german: "TM87",
            spanish: "MT87",
            italian: "MT87",
            english: "TM87"
        }
    },
    tm88: {
        num: 625,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 88,
                move: 365
            },
            platinum: {
                tm: 88,
                move: 365
            },
            heartgoldsoulsilver: {
                tm: 88,
                move: 365
            },
            blackwhite: {
                tm: 88,
                move: 365
            },
            black2white2: {
                tm: 88,
                move: 365
            },
            pokemonxy: {
                tm: 88,
                move: 214
            },
            omegarubyalphasapphire: {
                tm: 88,
                move: 214
            }
        },
        names: {
            japanese: "わざマシン８８",
            korean: "기술머신88",
            french: "CT88",
            german: "TM88",
            spanish: "MT88",
            italian: "MT88",
            english: "TM88"
        }
    },
    tm89: {
        num: 626,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 89,
                move: 369
            },
            platinum: {
                tm: 89,
                move: 369
            },
            heartgoldsoulsilver: {
                tm: 89,
                move: 369
            },
            blackwhite: {
                tm: 89,
                move: 369
            },
            black2white2: {
                tm: 89,
                move: 369
            },
            pokemonxy: {
                tm: 89,
                move: 369
            },
            omegarubyalphasapphire: {
                tm: 89,
                move: 369
            }
        },
        names: {
            japanese: "わざマシン８９",
            korean: "기술머신89",
            french: "CT89",
            german: "TM89",
            spanish: "MT89",
            italian: "MT89",
            english: "TM89"
        }
    },
    tm90: {
        num: 627,
        cost: 2000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 90,
                move: 164
            },
            platinum: {
                tm: 90,
                move: 164
            },
            heartgoldsoulsilver: {
                tm: 90,
                move: 164
            },
            blackwhite: {
                tm: 90,
                move: 164
            },
            black2white2: {
                tm: 90,
                move: 164
            },
            pokemonxy: {
                tm: 90,
                move: 164
            },
            omegarubyalphasapphire: {
                tm: 90,
                move: 164
            }
        },
        names: {
            japanese: "わざマシン９０",
            korean: "기술머신90",
            french: "CT90",
            german: "TM90",
            spanish: "MT90",
            italian: "MT90",
            english: "TM90"
        }
    },
    tm91: {
        num: 628,
        cost: 3000,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 91,
                move: 430
            },
            platinum: {
                tm: 91,
                move: 430
            },
            heartgoldsoulsilver: {
                tm: 91,
                move: 430
            },
            blackwhite: {
                tm: 91,
                move: 430
            },
            black2white2: {
                tm: 91,
                move: 430
            },
            pokemonxy: {
                tm: 91,
                move: 430
            },
            omegarubyalphasapphire: {
                tm: 91,
                move: 430
            }
        },
        names: {
            japanese: "わざマシン９１",
            korean: "기술머신91",
            french: "CT91",
            german: "TM91",
            spanish: "MT91",
            italian: "MT91",
            english: "TM91"
        }
    },
    tm92: {
        num: 629,
        cost: 5500,
        itemcategory: "tm",
        tm: {
            diamondpearl: {
                tm: 92,
                move: 433
            },
            platinum: {
                tm: 92,
                move: 433
            },
            heartgoldsoulsilver: {
                tm: 92,
                move: 433
            },
            blackwhite: {
                tm: 92,
                move: 433
            },
            black2white2: {
                tm: 92,
                move: 433
            },
            pokemonxy: {
                tm: 92,
                move: 433
            },
            omegarubyalphasapphire: {
                tm: 92,
                move: 433
            }
        },
        names: {
            japanese: "わざマシン９２",
            korean: "기술머신92",
            french: "CT92",
            german: "TM92",
            spanish: "MT92",
            italian: "MT92",
            english: "TM92"
        }
    },
    hm01: {
        num: 630,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 93,
                move: 528
            },
            redblue: {
                tm: 101,
                move: 15
            },
            yellow: {
                tm: 101,
                move: 15
            },
            goldsilver: {
                tm: 101,
                move: 15
            },
            crystal: {
                tm: 101,
                move: 15
            },
            rubysapphire: {
                tm: 101,
                move: 15
            },
            emerald: {
                tm: 101,
                move: 15
            },
            fireredleafgreen: {
                tm: 101,
                move: 15
            },
            diamondpearl: {
                tm: 101,
                move: 15
            },
            platinum: {
                tm: 101,
                move: 15
            },
            heartgoldsoulsilver: {
                tm: 101,
                move: 15
            },
            blackwhite: {
                tm: 101,
                move: 15
            },
            colosseum: {
                tm: 101,
                move: 15
            },
            xd: {
                tm: 101,
                move: 15
            },
            black2white2: {
                tm: 101,
                move: 15
            },
            pokemonxy: {
                tm: 101,
                move: 15
            }
        },
        names: {
            japanese: "ひでんマシン０１",
            korean: "비전머신01",
            french: "CS01",
            german: "VM01",
            spanish: "MO01",
            italian: "MN01",
            english: "HM01"
        }
    },
    hm02: {
        num: 631,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 94,
                move: 290
            },
            redblue: {
                tm: 102,
                move: 19
            },
            yellow: {
                tm: 102,
                move: 19
            },
            goldsilver: {
                tm: 102,
                move: 19
            },
            crystal: {
                tm: 102,
                move: 19
            },
            rubysapphire: {
                tm: 102,
                move: 19
            },
            emerald: {
                tm: 102,
                move: 19
            },
            fireredleafgreen: {
                tm: 102,
                move: 19
            },
            diamondpearl: {
                tm: 102,
                move: 19
            },
            platinum: {
                tm: 102,
                move: 19
            },
            heartgoldsoulsilver: {
                tm: 102,
                move: 19
            },
            blackwhite: {
                tm: 102,
                move: 19
            },
            colosseum: {
                tm: 102,
                move: 19
            },
            xd: {
                tm: 102,
                move: 19
            },
            black2white2: {
                tm: 102,
                move: 19
            },
            pokemonxy: {
                tm: 102,
                move: 19
            }
        },
        names: {
            japanese: "ひでんマシン０２",
            korean: "비전머신02",
            french: "CS02",
            german: "VM02",
            spanish: "MO02",
            italian: "MN02",
            english: "HM02"
        }
    },
    hm03: {
        num: 632,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 95,
                move: 555
            },
            redblue: {
                tm: 103,
                move: 57
            },
            yellow: {
                tm: 103,
                move: 57
            },
            goldsilver: {
                tm: 103,
                move: 57
            },
            crystal: {
                tm: 103,
                move: 57
            },
            rubysapphire: {
                tm: 103,
                move: 57
            },
            emerald: {
                tm: 103,
                move: 57
            },
            fireredleafgreen: {
                tm: 103,
                move: 57
            },
            diamondpearl: {
                tm: 103,
                move: 57
            },
            platinum: {
                tm: 103,
                move: 57
            },
            heartgoldsoulsilver: {
                tm: 103,
                move: 57
            },
            blackwhite: {
                tm: 103,
                move: 57
            },
            colosseum: {
                tm: 103,
                move: 57
            },
            xd: {
                tm: 103,
                move: 57
            },
            black2white2: {
                tm: 103,
                move: 57
            },
            pokemonxy: {
                tm: 103,
                move: 57
            }
        },
        names: {
            japanese: "ひでんマシン０３",
            korean: "비전머신03",
            french: "CS03",
            german: "VM03",
            spanish: "MO03",
            italian: "MN03",
            english: "HM03"
        }
    },
    hm04: {
        num: 633,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 96,
                move: 267
            },
            redblue: {
                tm: 104,
                move: 70
            },
            yellow: {
                tm: 104,
                move: 70
            },
            goldsilver: {
                tm: 104,
                move: 70
            },
            crystal: {
                tm: 104,
                move: 70
            },
            rubysapphire: {
                tm: 104,
                move: 70
            },
            emerald: {
                tm: 104,
                move: 70
            },
            fireredleafgreen: {
                tm: 104,
                move: 70
            },
            diamondpearl: {
                tm: 104,
                move: 70
            },
            platinum: {
                tm: 104,
                move: 70
            },
            heartgoldsoulsilver: {
                tm: 104,
                move: 70
            },
            blackwhite: {
                tm: 104,
                move: 70
            },
            colosseum: {
                tm: 104,
                move: 70
            },
            xd: {
                tm: 104,
                move: 70
            },
            black2white2: {
                tm: 104,
                move: 70
            },
            pokemonxy: {
                tm: 104,
                move: 70
            }
        },
        names: {
            japanese: "ひでんマシン０４",
            korean: "비전머신04",
            french: "CS04",
            german: "VM04",
            spanish: "MO04",
            italian: "MN04",
            english: "HM04"
        }
    },
    hm05: {
        num: 634,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 97,
                move: 399
            },
            redblue: {
                tm: 105,
                move: 148
            },
            yellow: {
                tm: 105,
                move: 148
            },
            goldsilver: {
                tm: 105,
                move: 148
            },
            crystal: {
                tm: 105,
                move: 148
            },
            rubysapphire: {
                tm: 105,
                move: 148
            },
            emerald: {
                tm: 105,
                move: 148
            },
            fireredleafgreen: {
                tm: 105,
                move: 148
            },
            diamondpearl: {
                tm: 105,
                move: 432
            },
            platinum: {
                tm: 105,
                move: 432
            },
            heartgoldsoulsilver: {
                tm: 105,
                move: 250
            },
            blackwhite: {
                tm: 105,
                move: 127
            },
            colosseum: {
                tm: 105,
                move: 148
            },
            xd: {
                tm: 105,
                move: 148
            },
            black2white2: {
                tm: 105,
                move: 127
            },
            pokemonxy: {
                tm: 105,
                move: 127
            }
        },
        names: {
            japanese: "ひでんマシン０５",
            korean: "비전머신05",
            french: "CS05",
            german: "VM05",
            spanish: "MO05",
            italian: "MN05",
            english: "HM05"
        }
    },
    hm06: {
        num: 635,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 98,
                move: 612
            },
            goldsilver: {
                tm: 106,
                move: 250
            },
            crystal: {
                tm: 106,
                move: 250
            },
            rubysapphire: {
                tm: 106,
                move: 249
            },
            emerald: {
                tm: 106,
                move: 249
            },
            fireredleafgreen: {
                tm: 106,
                move: 249
            },
            diamondpearl: {
                tm: 106,
                move: 249
            },
            platinum: {
                tm: 106,
                move: 249
            },
            heartgoldsoulsilver: {
                tm: 106,
                move: 249
            },
            blackwhite: {
                tm: 106,
                move: 291
            },
            colosseum: {
                tm: 106,
                move: 249
            },
            xd: {
                tm: 106,
                move: 249
            },
            black2white2: {
                tm: 106,
                move: 291
            }
        },
        names: {
            japanese: "ひでんマシン０６",
            korean: "비전머신06",
            french: "CS06",
            german: "VM06",
            spanish: "MO06",
            italian: "MN06",
            english: "HM06"
        }
    },
    hm07: {
        num: 636,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 99,
                move: 605
            },
            goldsilver: {
                tm: 107,
                move: 127
            },
            crystal: {
                tm: 107,
                move: 127
            },
            rubysapphire: {
                tm: 107,
                move: 127
            },
            emerald: {
                tm: 107,
                move: 127
            },
            fireredleafgreen: {
                tm: 107,
                move: 127
            },
            diamondpearl: {
                tm: 107,
                move: 127
            },
            platinum: {
                tm: 107,
                move: 127
            },
            heartgoldsoulsilver: {
                tm: 107,
                move: 127
            },
            colosseum: {
                tm: 107,
                move: 127
            },
            xd: {
                tm: 107,
                move: 127
            }
        },
        names: {
            japanese: "ひでんマシン０７",
            french: "CS07",
            german: "VM07",
            spanish: "MO07",
            italian: "MN07",
            english: "HM07"
        }
    },
    hm08: {
        num: 637,
        cost: 0,
        itemcategory: "tm",
        tm: {
            omegarubyalphasapphire: {
                tm: 100,
                move: 590
            },
            rubysapphire: {
                tm: 108,
                move: 291
            },
            emerald: {
                tm: 108,
                move: 291
            },
            fireredleafgreen: {
                tm: 108,
                move: 291
            },
            diamondpearl: {
                tm: 108,
                move: 431
            },
            platinum: {
                tm: 108,
                move: 431
            },
            heartgoldsoulsilver: {
                tm: 108,
                move: 431
            },
            colosseum: {
                tm: 108,
                move: 291
            },
            xd: {
                tm: 108,
                move: 291
            }
        },
        names: {
            japanese: "ひでんマシン０８",
            french: "CS08",
            german: "VM08",
            spanish: "MO08",
            italian: "MN08",
            english: "HM08"
        }
    },
    explorerkit: {
        num: 638,
        cost: 0,
        itemcategory: "gameplay",
        tm: {
            omegarubyalphasapphire: {
                tm: 101,
                move: 15
            }
        },
        names: {
            japanese: "たんけんセット",
            korean: "탐험세트",
            french: "Explorakit",
            german: "Forschersack",
            spanish: "Kit Explorador",
            italian: "Esplorokit",
            english: "Explorer Kit"
        }
    },
    lootsack: {
        num: 641,
        cost: 0,
        itemcategory: "unuseditems",
        tm: {
            omegarubyalphasapphire: {
                tm: 102,
                move: 19
            }
        },
        names: {
            japanese: "たからぶくろ",
            korean: "보물주머니",
            french: "Sac Butin",
            german: "Beutesack",
            spanish: "Saca Botín",
            italian: "Bottinosacca",
            english: "Loot Sack"
        }
    },
    rulebook: {
        num: 642,
        cost: 0,
        itemcategory: "unuseditems",
        tm: {
            omegarubyalphasapphire: {
                tm: 103,
                move: 57
            }
        },
        names: {
            japanese: "ルールブック",
            korean: "룰북",
            french: "Livre Règles",
            german: "Regelbuch",
            spanish: "Reglamento",
            italian: "Libro Regole",
            english: "Rule Book"
        }
    },
    pokeradar: {
        num: 643,
        cost: 0,
        itemcategory: "gameplay",
        tm: {
            omegarubyalphasapphire: {
                tm: 104,
                move: 70
            }
        },
        names: {
            japanese: "ポケトレ",
            korean: "포켓트레",
            french: "Poké Radar",
            german: "Pokéradar",
            spanish: "Pokéradar",
            italian: "Poké Radar",
            english: "Poké Radar"
        }
    },
    pointcard: {
        num: 645,
        cost: 0,
        itemcategory: "gameplay",
        tm: {
            omegarubyalphasapphire: {
                tm: 105,
                move: 127
            }
        },
        names: {
            japanese: "ポイントカード",
            korean: "포인트카드",
            french: "Carte Points",
            german: "Punktekarte",
            spanish: "Tarjeta Puntos",
            italian: "Scheda Punti",
            english: "Point Card"
        }
    },
    journal: {
        num: 646,
        cost: 0,
        itemcategory: "gameplay",
        tm: {
            omegarubyalphasapphire: {
                tm: 106,
                move: 249
            }
        },
        names: {
            japanese: "ぼうけんノート",
            korean: "모험노트",
            french: "Journal",
            german: "Tagebuch",
            spanish: "Diario",
            italian: "Agenda",
            english: "Journal"
        }
    },
    sealcase: {
        num: 647,
        cost: 0,
        itemcategory: "gameplay",
        tm: {
            omegarubyalphasapphire: {
                tm: 107,
                move: 291
            }
        },
        names: {
            japanese: "シールいれ",
            korean: "실상자",
            french: "Boîte Sceaux",
            german: "Stick.Koffer",
            spanish: "Caja Sellos",
            italian: "Portabolli",
            english: "Seal Case"
        }
    },
    fashioncase: {
        num: 651,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "アクセサリーいれ",
            korean: "액세서리상자",
            french: "Coffret Mode",
            german: "Modekoffer",
            spanish: "Caja Corazón",
            italian: "Scatola Chic",
            english: "Fashion Case"
        }
    },
    sealbag: {
        num: 652,
        cost: 0,
        itemcategory: "unuseditems",
        names: {
            japanese: "シールぶくろ",
            korean: "실주머니",
            french: "Sac Sceaux",
            german: "Stickertüte",
            spanish: "Bolsa Sellos",
            italian: "Bollosacca",
            english: "Seal Bag"
        }
    },
    palpad: {
        num: 653,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ともだちてちょう",
            korean: "친구수첩",
            french: "Registre Ami",
            german: "Adressbuch",
            spanish: "Bloc amigos",
            italian: "Blocco Amici",
            english: "Pal Pad"
        }
    },
    workskey: {
        num: 654,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "はつでんしょキー",
            korean: "발전소키",
            french: "Clé Centrale",
            german: "K-Schlüssel",
            spanish: "Llave Central",
            italian: "Turbinchiave",
            english: "Works Key"
        }
    },
    oldcharm: {
        num: 655,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "こだいのおまもり",
            korean: "고대의부적",
            french: "Vieux Grigri",
            german: "Talisman",
            spanish: "Talismán",
            italian: "Arcamuleto",
            english: "Old Charm"
        }
    },
    galactickey: {
        num: 689,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ギンガだんのカギ",
            korean: "갤럭시단의열쇠",
            french: "Clé Galaxie",
            german: "G-Schlüssel",
            spanish: "Llave Galaxia",
            italian: "Galachiave",
            english: "Galactic Key"
        }
    },
    redchain: {
        num: 690,
        cost: 0,
        itemcategory: "unuseditems",
        names: {
            japanese: "あかいくさり",
            korean: "빨강쇠사슬",
            french: "Chaîne Rouge",
            german: "Rote Kette",
            spanish: "Cadena Roja",
            italian: "Rossocatena",
            english: "Red Chain"
        }
    },
    townmap: {
        num: 691,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "タウンマップ",
            korean: "타운맵",
            french: "Carte",
            german: "Karte",
            spanish: "Mapa",
            italian: "Mappa Città",
            english: "Town Map"
        }
    },
    vsseeker: {
        num: 692,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "バトルサーチャー",
            korean: "배틀서처",
            french: "Cherche VS",
            german: "Kampffahnder",
            spanish: "Buscapelea",
            italian: "Cercasfide",
            english: "Vs. Seeker"
        }
    },
    coincase: {
        num: 693,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "コインケース",
            korean: "동전케이스",
            french: "Boîte Jetons",
            german: "Münzkorb",
            spanish: "Monedero",
            italian: "Salvadanaio",
            english: "Coin Case"
        }
    },
    oldrod: {
        num: 694,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ボロのつりざお",
            korean: "낡은낚싯대",
            french: "Canne",
            german: "Angel",
            spanish: "Caña Vieja",
            italian: "Amo Vecchio",
            english: "Old Rod"
        }
    },
    goodrod: {
        num: 695,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "いいつりざお",
            korean: "좋은낚싯대",
            french: "Super Canne",
            german: "Profiangel",
            spanish: "Caña Buena",
            italian: "Amo Buono",
            english: "Good Rod"
        }
    },
    superrod: {
        num: 696,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "すごいつりざお",
            korean: "대단한낚싯대",
            french: "Méga Canne",
            german: "Superangel",
            spanish: "Supercaña",
            italian: "Super Amo",
            english: "Super Rod"
        }
    },
    sprayduck: {
        num: 697,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "コダックじょうろ",
            korean: "고라파덕물뿌리개",
            french: "Kwakarrosoir",
            german: "Entonkanne",
            spanish: "Psydugadera",
            italian: "Sprayduck",
            english: "Sprayduck"
        }
    },
    poffincase: {
        num: 698,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ポフィンケース",
            korean: "포핀케이스",
            french: "Boîte Poffin",
            german: "Knurspbox",
            spanish: "Pokochera",
            italian: "Portapoffin",
            english: "Poffin Case"
        }
    },
    bicycle: {
        num: 699,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "じてんしゃ",
            korean: "자전거",
            french: "Bicyclette",
            german: "Fahrrad",
            spanish: "Bici",
            italian: "Bicicletta",
            english: "Bicycle"
        }
    },
    suitekey: {
        num: 700,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "ルームキー",
            korean: "룸키",
            french: "Clé Chambre",
            german: "B-Schlüssel",
            spanish: "Llave Suite",
            italian: "Chiave Suite",
            english: "Suite Key"
        }
    },
    oaksletter: {
        num: 701,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "オーキドのてがみ",
            korean: "오박사의편지",
            french: "Lettre Chen",
            german: "Eichs Brief",
            spanish: "Carta Prof. Oak",
            italian: "Lettera Oak",
            english: "Oak's Letter"
        }
    },
    lunarwing: {
        num: 702,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "みかづきのはね",
            korean: "초승달날개",
            french: "Lun'Aile",
            german: "Lunarfeder",
            spanish: "Pluma Lunar",
            italian: "Alalunare",
            english: "Lunar Wing"
        }
    },
    membercard: {
        num: 703,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "メンバーズカード",
            korean: "멤버스카드",
            french: "Carte Membre",
            german: "Mitgl.Karte",
            spanish: "Carné Socio",
            italian: "Scheda Soci",
            english: "Member Card"
        }
    },
    azureflute: {
        num: 704,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "てんかいのふえ",
            korean: "천계의피리",
            french: "Flûte Azur",
            german: "Azurflöte",
            spanish: "Flauta Azur",
            italian: "Flauto Cielo",
            english: "Azure Flute"
        }
    },
    ssticket: {
        num: 705,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ふねのチケット",
            korean: "승선티켓",
            french: "Passe Bateau",
            german: "Bootsticket",
            spanish: "Ticket Barco",
            italian: "Biglietto Nave",
            english: "S.S. Ticket"
        }
    },
    contestpass: {
        num: 706,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "コンテストパス",
            korean: "콘테스트패스",
            french: "Passe Concours",
            german: "Wettb.-Karte",
            spanish: "Pase Concurso",
            italian: "Tessera Gare",
            english: "Contest Pass"
        }
    },
    magmastone: {
        num: 707,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "かざんのおきいし",
            korean: "화산의돌",
            french: "Pierre Magma",
            german: "Magmastein",
            spanish: "Piedra Magma",
            italian: "Magmapietra",
            english: "Magma Stone"
        }
    },
    parcel: {
        num: 708,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "おとどけもの",
            korean: "전해줄물건",
            french: "Colis",
            german: "Paket",
            spanish: "Paquete",
            italian: "Pacco",
            english: "Parcel"
        }
    },
    coupon1: {
        num: 709,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ひきかえけん１",
            korean: "교환권1",
            french: "Bon 1",
            german: "Kupon 1",
            spanish: "Cupón 1",
            italian: "Coupon 1",
            english: "Coupon 1"
        }
    },
    coupon2: {
        num: 710,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ひきかえけん２",
            korean: "교환권2",
            french: "Bon 2",
            german: "Kupon 2",
            spanish: "Cupón 2",
            italian: "Coupon 2",
            english: "Coupon 2"
        }
    },
    coupon3: {
        num: 711,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ひきかえけん３",
            korean: "교환권3",
            french: "Bon 3",
            german: "Kupon 3",
            spanish: "Cupón 3",
            italian: "Coupon 3",
            english: "Coupon 3"
        }
    },
    storagekey: {
        num: 712,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "そうこのカギ",
            korean: "창고열쇠",
            french: "Clé Stockage",
            german: "L-Schlüssel",
            spanish: "Llave Almacén",
            italian: "Depochiave",
            english: "Storage Key"
        }
    },
    secretpotion: {
        num: 713,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ひでんのくすり",
            korean: "비전신약",
            french: "Potion Secrète",
            german: "Geheimtrank",
            spanish: "Poción Secreta",
            italian: "Pozione Segreta",
            english: "Secret Potion"
        }
    },
    vsrecorder: {
        num: 716,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "バトルレコーダー",
            korean: "배틀레코더",
            french: "Magnéto VS",
            german: "Kampfkamera",
            spanish: "Cámara Lucha",
            italian: "Registradati",
            english: "Vs. Recorder"
        }
    },
    gracidea: {
        num: 717,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "グラシデアのはな",
            korean: "그라시데아꽃",
            french: "Gracidée",
            german: "Gracidea",
            spanish: "Gracídea",
            italian: "Gracidea",
            english: "Gracidea"
        }
    },
    secretkey: {
        num: 718,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "ひみつのカギ",
            korean: "비밀의열쇠",
            french: "Clé Secrète",
            german: "?-Öffner",
            spanish: "Llave Secreta",
            italian: "Chiave Segreta",
            english: "Secret Key"
        }
    },
    apricornbox: {
        num: 719,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ぼんぐりケース",
            korean: "규토리케이스",
            french: "Boîte Noigrume",
            german: "Aprikokobox",
            spanish: "Caja Bonguri",
            italian: "Ghicobox",
            english: "Apricorn Box"
        }
    },
    berrypots: {
        num: 720,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "きのみプランター",
            korean: "나무열매플랜터",
            french: "Plante-Baies",
            german: "Pflanzset",
            spanish: "Plantabayas",
            italian: "Piantabacche",
            english: "Berry Pots"
        }
    },
    squirtbottle: {
        num: 721,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ゼニガメじょうろ",
            korean: "꼬부기물뿌리개",
            french: "Carapuce à O",
            german: "Schiggykanne",
            spanish: "Regadera",
            italian: "Annaffiatoio",
            english: "Squirt Bottle"
        }
    },
    redapricorn: {
        num: 731,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "あかぼんぐり",
            korean: "빨간규토리",
            french: "Noigrume Rge",
            german: "Aprikoko Rot",
            spanish: "Bonguri Rojo",
            italian: "Ghicocca Rossa",
            english: "Red Apricorn"
        }
    },
    blueapricorn: {
        num: 732,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "あおぼんぐり",
            korean: "파란규토리",
            french: "Noigrume Blu",
            german: "Aprikoko Blu",
            spanish: "Bonguri Azul",
            italian: "Ghicocca Blu",
            english: "Blue Apricorn"
        }
    },
    yellowapricorn: {
        num: 733,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "きぼんぐり",
            korean: "노랑규토리",
            french: "Noigrume Jne",
            german: "Aprikoko Glb",
            spanish: "Bonguri Amarillo",
            italian: "Ghicocca Gialla",
            english: "Yellow Apricorn"
        }
    },
    greenapricorn: {
        num: 734,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "みどぼんぐり",
            korean: "초록규토리",
            french: "Noigrume Ver",
            german: "Aprikoko Grn",
            spanish: "Bonguri Verde",
            italian: "Ghicocca Verde",
            english: "Green Apricorn"
        }
    },
    pinkapricorn: {
        num: 735,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "ももぼんぐり",
            korean: "담홍규토리",
            french: "Noigrume Ros",
            german: "Aprikoko Pnk",
            spanish: "Bonguri Rosa",
            italian: "Ghicocca Rosa",
            english: "Pink Apricorn"
        }
    },
    whiteapricorn: {
        num: 736,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "しろぼんぐり",
            korean: "하얀규토리",
            french: "Noigrume Blc",
            german: "Aprikoko Wss",
            spanish: "Bonguri Blanco",
            italian: "Ghicocca Bianca",
            english: "White Apricorn"
        }
    },
    blackapricorn: {
        num: 737,
        cost: 0,
        itemcategory: "apricorn",
        names: {
            japanese: "くろぼんぐり",
            korean: "검은규토리",
            french: "Noigrume Noi",
            german: "Aprikoko Swz",
            spanish: "Bonguri Negro",
            italian: "Ghicocca Nera",
            english: "Black Apricorn"
        }
    },
    dowsingmachine: {
        num: 738,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ダウジングマシン",
            korean: "다우징머신",
            french: "Cherch'Objet",
            german: "Itemradar",
            spanish: "Zahorí",
            italian: "Ricerca Strum.",
            english: "Dowsing Machine"
        }
    },
    ragecandybar: {
        num: 739,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "いかりまんじゅう",
            korean: "분노의호두과자",
            french: "Bonbon Rage",
            german: "Wutkeks",
            spanish: "Caramelo Furia",
            italian: "Iramella",
            english: "Rage Candy Bar"
        }
    },
    jadeorb: {
        num: 742,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "もえぎいろのたま",
            korean: "연둣빛구슬",
            french: "Orbe Vert",
            german: "Grüne Kugel",
            spanish: "Esfera Verde",
            italian: "Sfera Verde",
            english: "Jade Orb"
        }
    },
    enigmastone: {
        num: 743,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "なぞのすいしょう",
            korean: "수수께끼의수정",
            french: "Mystécristal",
            german: "Mytokristall",
            spanish: "Misticristal",
            italian: "Misticristal",
            english: "Enigma Stone"
        }
    },
    unownreport: {
        num: 744,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "アンノーンノート",
            korean: "안농노트",
            french: "Carnet Zarbi",
            german: "Icognitoheft",
            spanish: "Bloc Unown",
            italian: "UnownBloc",
            english: "Unown Report"
        }
    },
    bluecard: {
        num: 745,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ブルーカード",
            korean: "블루카드",
            french: "Carte Bleue",
            german: "Blaue Karte",
            spanish: "Tarjeta Azul",
            italian: "Carta Blu",
            english: "Blue Card"
        }
    },
    slowpoketail: {
        num: 746,
        cost: 0,
        itemcategory: "unuseditems",
        names: {
            japanese: "おいしいシッポ",
            korean: "맛있는꼬리",
            french: "Queueramolos",
            german: "Flegmon-Rute",
            spanish: "Colaslowpoke",
            italian: "Codaslowpoke",
            english: "Slowpoke Tail"
        }
    },
    clearbell: {
        num: 747,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "とうめいなスズ",
            korean: "크리스탈방울",
            french: "Glas Transparent",
            german: "Klarglocke",
            spanish: "Campana Clara",
            italian: "Campana Chiara",
            english: "Clear Bell"
        }
    },
    cardkey: {
        num: 748,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "カードキー",
            korean: "카드키",
            french: "Carte Magnétique",
            german: "Türöffner",
            spanish: "Llave Magnética",
            italian: "Apriporta",
            english: "Card Key"
        }
    },
    basementkey: {
        num: 749,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ちかのかぎ",
            korean: "지하의열쇠",
            french: "Clé Sous-Sol",
            german: "Kelleröffner",
            spanish: "Llave Sótano",
            italian: "Chiave Sotterr.",
            english: "Basement Key"
        }
    },
    redscale: {
        num: 750,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "あかいウロコ",
            korean: "빨간비늘",
            french: "Écaillerouge",
            german: "Rote Haut",
            spanish: "Escama Roja",
            italian: "Squama Rossa",
            english: "Red Scale"
        }
    },
    lostitem: {
        num: 751,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "おとしもの",
            korean: "분실물",
            french: "Objet perdu",
            german: "Fundsache",
            spanish: "Objeto Perdido",
            italian: "Strumento perso",
            english: "Lost Item"
        }
    },
    pass: {
        num: 765,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "リニアパス",
            korean: "리니어패스",
            french: "Passe Train",
            german: "Fahrschein",
            spanish: "Magnetopase",
            italian: "Superpass",
            english: "Pass"
        }
    },
    machinepart: {
        num: 766,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "きかいのぶひん",
            korean: "기계부품",
            french: "Partie Machine",
            german: "Spule",
            spanish: "Maquinaria",
            italian: "Pezzo macch.",
            english: "Machine Part"
        }
    },
    silverwing: {
        num: 771,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ぎんいろのはね",
            korean: "은빛날개",
            french: "Argent'Aile",
            german: "Silberflügel",
            spanish: "Ala Plateada",
            italian: "Aladargento",
            english: "Silver Wing"
        }
    },
    rainbowwing: {
        num: 772,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "にじいろのはね",
            korean: "무지갯빛날개",
            french: "Arcenci'Aile",
            german: "Buntschwinge",
            spanish: "Ala Arcoíris",
            italian: "Ala d'Iride",
            english: "Rainbow Wing"
        }
    },
    mysteryegg: {
        num: 773,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ふしぎなタマゴ",
            korean: "이상한알",
            french: "Œuf Mystère",
            german: "Rätsel-Ei",
            spanish: "Huevo Misterioso",
            italian: "Uovo Mistero",
            english: "Mystery Egg"
        }
    },
    gbsounds: {
        num: 774,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ＧＢプレイヤー",
            korean: "GB플레이어",
            french: "Lecteur GB",
            german: "GB-Player",
            spanish: "Lector GB",
            italian: "Lettore GB",
            english: "GB Sounds"
        }
    },
    tidalbell: {
        num: 775,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "うみなりのスズ",
            korean: "해명의방울",
            french: "Glas Tempête",
            german: "Gischtglocke",
            spanish: "Campana Oleaje",
            italian: "Campana Onda",
            english: "Tidal Bell"
        }
    },
    datacard01: {
        num: 795,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０１",
            korean: "데이터카드01",
            french: "Carte Mémo01",
            german: "Datenkarte01",
            spanish: "Tarjeta Datos 01",
            italian: "Scheda Dati 01",
            english: "Data Card 01"
        }
    },
    datacard02: {
        num: 796,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０２",
            korean: "데이터카드02",
            french: "Carte Mémo02",
            german: "Datenkarte02",
            spanish: "Tarjeta Datos 02",
            italian: "Scheda Dati 02",
            english: "Data Card 02"
        }
    },
    datacard03: {
        num: 797,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０３",
            korean: "데이터카드03",
            french: "Carte Mémo03",
            german: "Datenkarte03",
            spanish: "Tarjeta Datos 03",
            italian: "Scheda Dati 03",
            english: "Data Card 03"
        }
    },
    datacard04: {
        num: 807,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０４",
            korean: "데이터카드04",
            french: "Carte Mémo04",
            german: "Datenkarte04",
            spanish: "Tarjeta Datos 04",
            italian: "Scheda Dati 04",
            english: "Data Card 04"
        }
    },
    datacard05: {
        num: 808,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０５",
            korean: "데이터카드05",
            french: "Carte Mémo05",
            german: "Datenkarte05",
            spanish: "Tarjeta Datos 05",
            italian: "Scheda Dati 05",
            english: "Data Card 05"
        }
    },
    datacard06: {
        num: 809,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０６",
            korean: "데이터카드06",
            french: "Carte Mémo06",
            german: "Datenkarte06",
            spanish: "Tarjeta Datos 06",
            italian: "Scheda Dati 06",
            english: "Data Card 06"
        }
    },
    datacard07: {
        num: 810,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０７",
            korean: "데이터카드07",
            french: "Carte Mémo07",
            german: "Datenkarte07",
            spanish: "Tarjeta Datos 07",
            italian: "Scheda Dati 07",
            english: "Data Card 07"
        }
    },
    datacard08: {
        num: 811,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０８",
            korean: "데이터카드08",
            french: "Carte Mémo08",
            german: "Datenkarte08",
            spanish: "Tarjeta Datos 08",
            italian: "Scheda Dati 08",
            english: "Data Card 08"
        }
    },
    datacard09: {
        num: 812,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード０９",
            korean: "데이터카드09",
            french: "Carte Mémo09",
            german: "Datenkarte09",
            spanish: "Tarjeta Datos 09",
            italian: "Scheda Dati 09",
            english: "Data Card 09"
        }
    },
    datacard10: {
        num: 813,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１０",
            korean: "데이터카드10",
            french: "Carte Mémo10",
            german: "Datenkarte10",
            spanish: "Tarjeta Datos 10",
            italian: "Scheda Dati 10",
            english: "Data Card 10"
        }
    },
    datacard11: {
        num: 814,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１１",
            korean: "데이터카드11",
            french: "Carte Mémo11",
            german: "Datenkarte11",
            spanish: "Tarjeta Datos 11",
            italian: "Scheda Dati 11",
            english: "Data Card 11"
        }
    },
    datacard12: {
        num: 815,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１２",
            korean: "데이터카드12",
            french: "Carte Mémo12",
            german: "Datenkarte12",
            spanish: "Tarjeta Datos 12",
            italian: "Scheda Dati 12",
            english: "Data Card 12"
        }
    },
    datacard13: {
        num: 816,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１３",
            korean: "데이터카드13",
            french: "Carte Mémo13",
            german: "Datenkarte13",
            spanish: "Tarjeta Datos 13",
            italian: "Scheda Dati 13",
            english: "Data Card 13"
        }
    },
    datacard14: {
        num: 817,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１４",
            korean: "데이터카드14",
            french: "Carte Mémo14",
            german: "Datenkarte14",
            spanish: "Tarjeta Datos 14",
            italian: "Scheda Dati 14",
            english: "Data Card 14"
        }
    },
    datacard15: {
        num: 818,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１５",
            korean: "데이터카드15",
            french: "Carte Mémo15",
            german: "Datenkarte15",
            spanish: "Tarjeta Datos 15",
            italian: "Scheda Dati 15",
            english: "Data Card 15"
        }
    },
    datacard16: {
        num: 819,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１６",
            korean: "데이터카드16",
            french: "Carte Mémo16",
            german: "Datenkarte16",
            spanish: "Tarjeta Datos 16",
            italian: "Scheda Dati 16",
            english: "Data Card 16"
        }
    },
    datacard17: {
        num: 820,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１７",
            korean: "데이터카드17",
            french: "Carte Mémo17",
            german: "Datenkarte17",
            spanish: "Tarjeta Datos 17",
            italian: "Scheda Dati 17",
            english: "Data Card 17"
        }
    },
    datacard18: {
        num: 821,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１８",
            korean: "데이터카드18",
            french: "Carte Mémo18",
            german: "Datenkarte18",
            spanish: "Tarjeta Datos 18",
            italian: "Scheda Dati 18",
            english: "Data Card 18"
        }
    },
    datacard19: {
        num: 822,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード１９",
            korean: "데이터카드19",
            french: "Carte Mémo19",
            german: "Datenkarte19",
            spanish: "Tarjeta Datos 19",
            italian: "Scheda Dati 19",
            english: "Data Card 19"
        }
    },
    datacard20: {
        num: 823,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２０",
            korean: "데이터카드20",
            french: "Carte Mémo20",
            german: "Datenkarte20",
            spanish: "Tarjeta Datos 20",
            italian: "Scheda Dati 20",
            english: "Data Card 20"
        }
    },
    datacard21: {
        num: 824,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２１",
            korean: "데이터카드21",
            french: "Carte Mémo21",
            german: "Datenkarte21",
            spanish: "Tarjeta Datos 21",
            italian: "Scheda Dati 21",
            english: "Data Card 21"
        }
    },
    datacard22: {
        num: 825,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２２",
            korean: "데이터카드22",
            french: "Carte Mémo22",
            german: "Datenkarte22",
            spanish: "Tarjeta Datos 22",
            italian: "Scheda Dati 22",
            english: "Data Card 22"
        }
    },
    datacard23: {
        num: 826,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２３",
            korean: "데이터카드23",
            french: "Carte Mémo23",
            german: "Datenkarte23",
            spanish: "Tarjeta Datos 23",
            italian: "Scheda Dati 23",
            english: "Data Card 23"
        }
    },
    datacard24: {
        num: 827,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２４",
            korean: "데이터카드24",
            french: "Carte Mémo24",
            german: "Datenkarte24",
            spanish: "Tarjeta Datos 24",
            italian: "Scheda Dati 24",
            english: "Data Card 24"
        }
    },
    datacard25: {
        num: 828,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２５",
            korean: "데이터카드25",
            french: "Carte Mémo25",
            german: "Datenkarte25",
            spanish: "Tarjeta Datos 25",
            italian: "Scheda Dati 25",
            english: "Data Card 25"
        }
    },
    datacard26: {
        num: 829,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２６",
            korean: "데이터카드26",
            french: "Carte Mémo26",
            german: "Datenkarte26",
            spanish: "Tarjeta Datos 26",
            italian: "Scheda Dati 26",
            english: "Data Card 26"
        }
    },
    datacard27: {
        num: 830,
        cost: 0,
        itemcategory: "datacard",
        names: {
            japanese: "データカード２７",
            korean: "데이터카드27",
            french: "Carte Mémo27",
            german: "Datenkarte27",
            spanish: "Tarjeta Datos 27",
            italian: "Scheda Dati 27",
            english: "Data Card 27"
        }
    },
    lockcapsule: {
        num: 831,
        cost: 0,
        itemcategory: "unuseditems",
        names: {
            japanese: "ロックカプセル",
            korean: "록캡슐",
            french: "Poké Écrin",
            german: "Tresorkapsel",
            spanish: "Cápsula Candado",
            italian: "Capsula Scrigno",
            english: "Lock Capsule"
        }
    },
    photoalbum: {
        num: 832,
        cost: 0,
        itemcategory: "unuseditems",
        names: {
            japanese: "フォトアルバム",
            korean: "포토앨범",
            french: "Album Photo",
            german: "Fotoalbum",
            spanish: "Álbum",
            italian: "Album",
            english: "Photo Album"
        }
    },
    orangemail: {
        num: 833,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "オレンジメール",
            french: "Lettre Oranj",
            german: "Zigzagbrief",
            spanish: "Car. Naranja",
            italian: "Mess. Agrume",
            english: "Orange Mail"
        }
    },
    harbormail: {
        num: 834,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ハーバーメール",
            french: "Lettre Port",
            german: "Hafenbrief",
            spanish: "Carta Puerto",
            italian: "Mess. Porto",
            english: "Harbor Mail"
        }
    },
    glittermail: {
        num: 835,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "キラキラメール",
            french: "Lettre Brill",
            german: "Glitzerbrief",
            spanish: "Carta Brillo",
            italian: "Mess. Luci",
            english: "Glitter Mail"
        }
    },
    mechmail: {
        num: 837,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "メカニカルメール",
            french: "Lettre Méca",
            german: "Eilbrief",
            spanish: "Carta Imán",
            italian: "Mess. Tecno",
            english: "Mech Mail"
        }
    },
    woodmail: {
        num: 838,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ウッディメール",
            french: "Lettre Bois",
            german: "Waldbrief",
            spanish: "Carta Madera",
            italian: "Mess. Bosco",
            english: "Wood Mail"
        }
    },
    wavemail: {
        num: 839,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "クロスメール",
            french: "Lettre Vague",
            german: "Wellenbrief",
            spanish: "Carta Ola",
            italian: "Mess. Onda",
            english: "Wave Mail"
        }
    },
    beadmail: {
        num: 840,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "トレジャーメール",
            french: "Lettre Bulle",
            german: "Perlenbrief",
            spanish: "Carta Imagen",
            italian: "Mess. Perle",
            english: "Bead Mail"
        }
    },
    shadowmail: {
        num: 841,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "シャドーメール",
            french: "Lettre Ombre",
            german: "Dunkelbrief",
            spanish: "Carta Sombra",
            italian: "Mess. Ombra",
            english: "Shadow Mail"
        }
    },
    tropicmail: {
        num: 842,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "トロピカルメール",
            french: "Lettre Tropi",
            german: "Tropenbrief",
            spanish: "Car. Tropic.",
            italian: "Mess. Tropic",
            english: "Tropic Mail"
        }
    },
    dreammail: {
        num: 843,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ドリームメール",
            french: "Lettre Songe",
            german: "Traumbrief",
            spanish: "Carta Sueño",
            italian: "Mess. Sogno",
            english: "Dream Mail"
        }
    },
    fabmail: {
        num: 844,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ミラクルメール",
            french: "Lettre Cool",
            german: "Edelbrief",
            spanish: "Carta Fab.",
            italian: "Mess. Lusso",
            english: "Fab Mail"
        }
    },
    retromail: {
        num: 845,
        cost: 0,
        itemcategory: "mail",
        names: {
            japanese: "レトロメール",
            french: "Lettre Retro",
            german: "Retrobrief",
            spanish: "Carta Retro.",
            italian: "Mess. Rétro",
            english: "Retro Mail"
        }
    },
    machbike: {
        num: 847,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "マッハじてんしゃ",
            french: "Vélo Course",
            german: "Eilrad",
            spanish: "Bici Carrera",
            italian: "Bici Corsa",
            english: "Mach Bike"
        }
    },
    acrobike: {
        num: 848,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ダートじてんしゃ",
            french: "Vélo Cross",
            german: "Kunstrad",
            spanish: "Bici Acrob.",
            italian: "Bici Cross",
            english: "Acro Bike"
        }
    },
    wailmerpail: {
        num: 849,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ホエルコじょうろ",
            french: "Seau Wailmer",
            german: "Wailmerkanne",
            spanish: "Cubo Wailmer",
            italian: "Vaso Wailmer",
            english: "Wailmer Pail"
        }
    },
    devongoods: {
        num: 850,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "デボンのにもつ",
            french: "Pack Devon",
            german: "Devon-Waren",
            spanish: "Piezas Devon",
            italian: "Merce Devon",
            english: "Devon Goods"
        }
    },
    sootsack: {
        num: 852,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "はいぶくろ",
            french: "Sac à Suie",
            german: "Aschetasche",
            spanish: "Saco Hollín",
            italian: "Sacco Cenere",
            english: "Soot Sack"
        }
    },
    pokeblockcase: {
        num: 853,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ポロックケース",
            french: "Boîte Pokéblocs",
            german: "Pokériegelbox",
            spanish: "Tubo Pokécubos",
            italian: "PortaPokémelle",
            english: "Pokéblock Case"
        }
    },
    letter: {
        num: 854,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ダイゴへのてがみ",
            french: "Lettre",
            german: "Brief",
            spanish: "Carta",
            italian: "Lettera",
            english: "Letter"
        }
    },
    eonticket: {
        num: 855,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "むげんのチケット",
            french: "Passe Éon",
            german: "Äon-Ticket",
            spanish: "Ticket Eón",
            italian: "Bigl. Eone",
            english: "Eon Ticket"
        }
    },
    scanner: {
        num: 856,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "たんちき",
            french: "Scanner",
            german: "Scanner",
            spanish: "Escáner",
            italian: "Scanner",
            english: "Scanner"
        }
    },
    gogoggles: {
        num: 857,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ゴーゴーゴーグル",
            french: "Lunet. Sable",
            german: "Wüstenglas",
            spanish: "Gaf. Aislan.",
            italian: "Occhialoni",
            english: "Go-Goggles"
        }
    },
    meteorite: {
        num: 858,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "いんせき",
            french: "Météorite",
            german: "Meteorit",
            spanish: "Meteorito",
            italian: "Meteorite",
            english: "Meteorite"
        }
    },
    rm1key: {
        num: 859,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "１ごうしつのカギ",
            french: "Clé Salle 1",
            german: "K1-Schlüssel",
            spanish: "Ll. Cabina 1",
            italian: "Chiave Cab.1",
            english: "Rm. 1 Key"
        }
    },
    rm2key: {
        num: 860,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "２ごうしつのカギ",
            french: "Clé Salle 2",
            german: "K2-Schlüssel",
            spanish: "Ll. Cabina 2",
            italian: "Chiave Cab.2",
            english: "Rm. 2 Key"
        }
    },
    rm4key: {
        num: 861,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "４ごうしつのカギ",
            french: "Clé Salle 4",
            german: "K4-Schlüssel",
            spanish: "Ll. Cabina 4",
            italian: "Chiave Cab.4",
            english: "Rm. 4 Key"
        }
    },
    rm6key: {
        num: 862,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "６ごうしつのカギ",
            french: "Clé Salle 6",
            german: "K6-Schlüssel",
            spanish: "Ll. Cabina 6",
            italian: "Chiave Cab.6",
            english: "Rm. 6 Key"
        }
    },
    devonscope: {
        num: 863,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "デボンスコープ",
            french: "Devon Scope",
            german: "Devon-Scope",
            spanish: "Detec. Devon",
            italian: "Devonscopio",
            english: "Devon Scope"
        }
    },
    oaksparcel: {
        num: 864,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "おとどけもの",
            french: "Colis Chen",
            german: "Eichs Paket",
            spanish: "Correo-Oak",
            italian: "Pacco di Oak",
            english: "Oak's Parcel"
        }
    },
    pokeflute: {
        num: 865,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ポケモンのふえ",
            korean: "포켓몬피리",
            french: "Pokéflûte",
            german: "Pokéflöte",
            spanish: "Poké Flauta",
            italian: "Poké Flauto",
            english: "Poké Flute"
        }
    },
    bikevoucher: {
        num: 866,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ひきかえけん",
            french: "Bon Commande",
            german: "Rad-Coupon",
            spanish: "Bono Bici",
            italian: "Buono Bici",
            english: "Bike Voucher"
        }
    },
    goldteeth: {
        num: 867,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "きんのいれば",
            french: "Dent d'Or",
            german: "Goldzähne",
            spanish: "Dientes Oro",
            italian: "Denti d'Oro",
            english: "Gold Teeth"
        }
    },
    liftkey: {
        num: 868,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "エレベータのカギ",
            french: "Clé Asc.",
            german: "Liftöffner",
            spanish: "Ll. Ascensor",
            italian: "Chiave Asc.",
            english: "Lift Key"
        }
    },
    silphscope: {
        num: 869,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "シルフスコープ",
            french: "Scope Sylphe",
            german: "Silph Scope",
            spanish: "Scope Silph",
            italian: "Spettrosonda",
            english: "Silph Scope"
        }
    },
    famechecker: {
        num: 870,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ボイスチェッカー",
            french: "Memorydex",
            german: "Ruhmesdatei",
            spanish: "Memorín",
            italian: "PokéVIP",
            english: "Fame Checker"
        }
    },
    tmcase: {
        num: 871,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "わざマシンケース",
            french: "Boîte CT",
            german: "VM/TM-Box",
            spanish: "Tubo MT-MO",
            italian: "Porta-MT",
            english: "TM Case"
        }
    },
    berrypouch: {
        num: 872,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "きのみぶくろ",
            french: "Sac à Baies",
            german: "Beerentüte",
            spanish: "Saco Bayas",
            italian: "Portabacche",
            english: "Berry Pouch"
        }
    },
    teachytv: {
        num: 873,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "おしえテレビ",
            french: "TV ABC",
            german: "Lehrkanal",
            spanish: "Poké Tele",
            italian: "Pokétivù",
            english: "Teachy TV"
        }
    },
    tripass: {
        num: 874,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "トライパス",
            french: "Tri-Passe",
            german: "Tri-Pass",
            spanish: "Tri-Ticket",
            italian: "Tris Pass",
            english: "Tri-Pass"
        }
    },
    rainbowpass: {
        num: 875,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "レインボーパス",
            french: "Passe Prisme",
            german: "Bunt-Pass",
            spanish: "Iris-Ticket",
            italian: "Sette Pass",
            english: "Rainbow Pass"
        }
    },
    tea: {
        num: 876,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "おちゃ",
            french: "Thé",
            german: "Tee",
            spanish: "Té",
            italian: "Tè",
            english: "Tea"
        }
    },
    mysticticket: {
        num: 877,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "しんぴのチケット",
            french: "Ticketmystik",
            german: "Geheimticket",
            spanish: "Misti-Ticket",
            italian: "Bigl. Magico",
            english: "MysticTicket"
        }
    },
    auroraticket: {
        num: 878,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "オーロラチケット",
            french: "Ticketaurora",
            german: "Auroraticket",
            spanish: "Ori-Ticket",
            italian: "Bigl. Aurora",
            english: "AuroraTicket"
        }
    },
    powderjar: {
        num: 885,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "こないれ",
            french: "Pot Poudre",
            german: "Puderdöschen",
            spanish: "Bote Polvos",
            italian: "Portafarina",
            english: "Powder Jar"
        }
    },
    ruby: {
        num: 886,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ルビー",
            french: "Rubis",
            german: "Rubin",
            spanish: "Rubí",
            italian: "Rubino",
            english: "Ruby"
        }
    },
    sapphire: {
        num: 887,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "サファイア",
            french: "Saphir",
            german: "Saphir",
            spanish: "Zafiro",
            italian: "Zaffiro",
            english: "Sapphire"
        }
    },
    magmaemblem: {
        num: 888,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "マグマのしるし",
            french: "Sceau Magma",
            german: "Magmaemblem",
            spanish: "Signo Magma",
            italian: "Stemma Magma",
            english: "Magma Emblem"
        }
    },
    oldseamap: {
        num: 889,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "ふるびたかいず",
            french: "Vieillecarte",
            german: "Alte Karte",
            spanish: "Mapa Viejo",
            italian: "Mappa Stinta",
            english: "Old Sea Map"
        }
    },
    sweetheart: {
        num: 894,
        cost: 100,
        itemcategory: "healing",
        heal: 20,
        names: {
            japanese: "ハートスイーツ",
            korean: "하트스위트",
            french: "Chococœur",
            german: "Herzkonfekt",
            spanish: "Corazón Dulce",
            italian: "Dolcecuore",
            english: "Sweet Heart"
        }
    },
    greetmail: {
        num: 895,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "はじめてメール",
            korean: "첫메일",
            french: "Lettre Salut",
            german: "Grußbrief",
            spanish: "Carta Inicial",
            italian: "Messaggio Inizio",
            english: "Greet Mail"
        }
    },
    favoredmail: {
        num: 896,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "だいすきメール",
            korean: "애호메일",
            french: "Lettre Fan",
            german: "Faiblebrief",
            spanish: "Carta Favoritos",
            italian: "Messaggio TVB",
            english: "Favored Mail"
        }
    },
    rsvpmail: {
        num: 897,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "おさそいメール",
            korean: "권유메일",
            french: "Lettre Invit",
            german: "Einladebrief",
            spanish: "Carta Invitar",
            italian: "Messaggio Invito",
            english: "RSVP Mail"
        }
    },
    thanksmail: {
        num: 898,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "かんしゃメール",
            korean: "감사메일",
            french: "Lettre Merci",
            german: "Dankesbrief",
            spanish: "Carta Gracias",
            italian: "Messaggio Grazie",
            english: "Thanks Mail"
        }
    },
    inquirymail: {
        num: 899,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "しつもんメール",
            korean: "질문메일",
            french: "Lettre Demande",
            german: "Fragebrief",
            spanish: "Carta Pregunta",
            italian: "Messaggio Chiedi",
            english: "Inquiry Mail"
        }
    },
    likemail: {
        num: 900,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "おすすめメール",
            korean: "추천메일",
            french: "Lettre Avis",
            german: "Insiderbrief",
            spanish: "Carta Gustos",
            italian: "Mess. Sugg.",
            english: "Like Mail"
        }
    },
    replymail: {
        num: 901,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "おかえしメール",
            korean: "답장메일",
            french: "Lettre Réponse",
            german: "Rückbrief",
            spanish: "Carta Respuesta",
            italian: "Mess. Risp.",
            english: "Reply Mail"
        }
    },
    bridgemails: {
        num: 902,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリッジメールＳ",
            korean: "브리지메일S",
            french: "Lettre PontS",
            german: "Brückbrief H",
            spanish: "Carta Puente S",
            italian: "Mess. Frec.",
            english: "Bridge Mail S"
        }
    },
    bridgemaild: {
        num: 903,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリッジメールＨ",
            korean: "브리지메일M",
            french: "Lettre PontY",
            german: "Brückbrief M",
            spanish: "Carta Puente F",
            italian: "Mess. Libec.",
            english: "Bridge Mail D"
        }
    },
    bridgemailt: {
        num: 921,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリッジメールＣ",
            korean: "브리지메일C",
            french: "Lettre PontF",
            german: "Brückbrief Z",
            spanish: "Carta Puente A",
            italian: "Mess. Prop.",
            english: "Bridge Mail T"
        }
    },
    bridgemailv: {
        num: 922,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリッジメールＶ",
            korean: "브리지메일V",
            french: "Lettre PontH",
            german: "Brückbrief D",
            spanish: "Carta Puente V",
            italian: "Mess. Vill.",
            english: "Bridge Mail V"
        }
    },
    bridgemailm: {
        num: 923,
        cost: 50,
        itemcategory: "mail",
        names: {
            japanese: "ブリッジメールＷ",
            korean: "브리지메일W",
            french: "Lettre PontI",
            german: "Brückbrief W",
            spanish: "Carta Puente P",
            italian: "Mess. Merav.",
            english: "Bridge Mail M"
        }
    },
    prismscale: {
        num: 924,
        cost: 500,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "きれいなウロコ",
            korean: "고운비늘",
            french: "Bel'Écaille",
            german: "Schönschuppe",
            spanish: "Escama Bella",
            italian: "Squama Bella",
            english: "Prism Scale"
        }
    },
    healthwing: {
        num: 950,
        cost: 3000,
        itemcategory: "vitamins",
        wing: true,
        stat:"hp",
        names: {
            japanese: "たいりょくのハネ",
            korean: "체력날개",
            french: "Aile Santé",
            german: "Heilfeder",
            spanish: "Pluma Vigor",
            italian: "Piumsalute",
            english: "Health Wing"
        }
    },
    musclewing: {
        num: 951,
        cost: 3000,
        itemcategory: "vitamins",
        wing: true,
        stat:"atk",
        names: {
            japanese: "きんりょくのハネ",
            korean: "근력날개",
            french: "Aile Force",
            german: "Kraftfeder",
            spanish: "Pluma Músculo",
            italian: "Piumpotenza",
            english: "Muscle Wing"
        }
    },
    resistwing: {
        num: 952,
        cost: 3000,
        itemcategory: "vitamins",
        wing: true,
        stat:'def',
        names: {
            japanese: "ていこうのハネ",
            korean: "저항력날개",
            french: "Aile Armure",
            german: "Abwehrfeder",
            spanish: "Pluma Aguante",
            italian: "Piumtutela",
            english: "Resist Wing"
        }
    },
    geniuswing: {
        num: 953,
        cost: 3000,
        itemcategory: "vitamins",
        wing: true,
        stat:'spa',
        names: {
            japanese: "ちりょくのハネ",
            korean: "지력날개",
            french: "Aile Esprit",
            german: "Geniefeder",
            spanish: "Pluma Intelecto",
            italian: "Piumingegno",
            english: "Genius Wing"
        }
    },
    cleverwing: {
        num: 954,
        cost: 3000,
        itemcategory: "vitamins",
        wing: true,
        stat: 'spd',
        names: {
            japanese: "せいしんのハネ",
            korean: "정신력날개",
            french: "Aile Mental",
            german: "Espritfeder",
            spanish: "Pluma Mente",
            italian: "Piumintuito",
            english: "Clever Wing"
        }
    },
    swiftwing: {
        num: 955,
        cost: 3000,
        itemcategory: "vitamins",
        wing: true,
        stat: 'spe',
        names: {
            japanese: "しゅんぱつのハネ",
            korean: "순발력날개",
            french: "Aile Sprint",
            german: "Flinkfeder",
            spanish: "Pluma Ímpetu",
            italian: "Piumreazione",
            english: "Swift Wing"
        }
    },
    prettywing: {
        num: 956,
        cost: 200,
        itemcategory: "loot",
        names: {
            japanese: "きれいなハネ",
            korean: "고운날개",
            french: "Jolie Aile",
            german: "Prachtfeder",
            spanish: "Pluma Bella",
            italian: "Piumabella",
            english: "Pretty Wing"
        }
    },
    libertypass: {
        num: 959,
        cost: 0,
        itemcategory: "eventitems",
        names: {
            japanese: "リバティチケット",
            korean: "리버티티켓",
            french: "Pass Liberté",
            german: "Gartenpass",
            spanish: "Tarjeta Libertad",
            italian: "Liberticket",
            english: "Liberty Pass"
        }
    },
    passorb: {
        num: 960,
        cost: 200,
        itemcategory: "helditems",
        names: {
            japanese: "デルダマ",
            korean: "딜구슬",
            french: "Offrisphère",
            german: "Transferorb",
            spanish: "Regalosfera",
            italian: "Passabilia",
            english: "Pass Orb"
        }
    },
    poketoy: {
        num: 962,
        cost: 1000,
        itemcategory: "escapeitem",
        names: {
            japanese: "ポケじゃらし",
            korean: "포켓풀",
            french: "Poképlumet",
            german: "Pokéwedel",
            spanish: "Pokéseñuelo",
            italian: "Pokégingillo",
            english: "Poké Toy"
        }
    },
    propcase: {
        num: 963,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "グッズケース",
            korean: "굿즈케이스",
            french: "Boîte Parure",
            german: "Deko-Box",
            spanish: "Neceser",
            italian: "Portagadget",
            english: "Prop Case"
        }
    },
    dragonskull: {
        num: 964,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ドラゴンのホネ",
            korean: "드래곤의뼈",
            french: "Crâne Dragon",
            german: "Drakoschädel",
            spanish: "Cráneo Dragón",
            italian: "Teschio",
            english: "Dragon Skull"
        }
    },
    balmmushroom: {
        num: 965,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "かおるキノコ",
            korean: "향기버섯",
            french: "Champi Suave",
            german: "Duftpilz",
            spanish: "Seta Aroma",
            italian: "Profumfungo",
            english: "Balm Mushroom"
        }
    },
    bignugget: {
        num: 966,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "でかいきんのたま",
            korean: "큰금구슬",
            french: "Maxi Pépite",
            german: "Riesennugget",
            spanish: "Maxipepita",
            italian: "Granpepita",
            english: "Big Nugget"
        }
    },
    pearlstring: {
        num: 967,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "おだんごしんじゅ",
            korean: "경단진주",
            french: "Perle Triple",
            german: "Triperle",
            spanish: "Sarta Perlas",
            italian: "Trittiperla",
            english: "Pearl String"
        }
    },
    cometshard: {
        num: 968,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "すいせいのかけら",
            korean: "혜성조각",
            french: "Morceau Comète",
            german: "Kometstück",
            spanish: "Fragmento Cometa",
            italian: "Pezzo Cometa",
            english: "Comet Shard"
        }
    },
    reliccopper: {
        num: 969,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのどうか",
            korean: "고대의동화",
            french: "Vieux Sou",
            german: "Alter Heller",
            spanish: "Real Cobre",
            italian: "Soldantico",
            english: "Relic Copper"
        }
    },
    relicsilver: {
        num: 970,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのぎんか",
            korean: "고대의은화",
            french: "Vieil Écu",
            german: "Alter Taler",
            spanish: "Real Plata",
            italian: "Ducatantico",
            english: "Relic Silver"
        }
    },
    relicgold: {
        num: 971,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのきんか",
            korean: "고대의금화",
            french: "Vieux Ducat",
            german: "Alter Dukat",
            spanish: "Real Oro",
            italian: "Doblonantico",
            english: "Relic Gold"
        }
    },
    relicvase: {
        num: 972,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのツボ",
            korean: "고대의항아리",
            french: "Vieux Vase",
            german: "Alte Vase",
            spanish: "Ánfora",
            italian: "Vasantico",
            english: "Relic Vase"
        }
    },
    relicband: {
        num: 973,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのうでわ",
            korean: "고대의팔찌",
            french: "Vieux Bijou",
            german: "Alter Reif",
            spanish: "Brazal",
            italian: "Bracciantico",
            english: "Relic Band"
        }
    },
    relicstatue: {
        num: 974,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのせきぞう",
            korean: "고대의석상",
            french: "Vieux Santon",
            german: "Alte Statue",
            spanish: "Efigie Antigua",
            italian: "Statuantica",
            english: "Relic Statue"
        }
    },
    reliccrown: {
        num: 975,
        cost: 0,
        itemcategory: "loot",
        names: {
            japanese: "こだいのおうかん",
            korean: "고대의왕관",
            french: "Vieux Tortil",
            german: "Alte Krone",
            spanish: "Corona Antigua",
            italian: "Coronantica",
            english: "Relic Crown"
        }
    },
    casteliacone: {
        num: 976,
        cost: 100,
        itemcategory: "healing",
        status: "all",
        names: {
            japanese: "ヒウンアイス",
            korean: "구름아이스",
            french: "Glace Volute",
            german: "Stratos-Eis",
            spanish: "Porcehelado",
            italian: "Conostropoli",
            english: "Casteliacone"
        }
    },
    direhit2: {
        num: 977,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'crit',
        levels: 2,
        names: {
            japanese: "クリティカット２",
            korean: "크리티컬커터2",
            french: "Muscle 2",
            german: "Angriffplus2",
            spanish: "Directo 2",
            italian: "Supercolpo 2",
            english: "Dire Hit 2"
        }
    },
    xspeed2: {
        num: 978,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'spe',
        amount: 2,
        names: {
            japanese: "スピーダー２",
            korean: "스피드업2",
            french: "Vitesse 2",
            german: "X-Tempo2",
            spanish: "Velocidad X 2",
            italian: "Velocità X-2",
            english: "X Speed 2"
        }
    },
    xspatk2: {
        num: 979,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'atk',
        amount: 2,
        names: {
            japanese: "ＳＰアップ２",
            korean: "스페셜업2",
            french: "Atq. Spé. 2",
            german: "X-Spezial2",
            spanish: "Especial X 2",
            italian: "Special X-2",
            english: "X Sp. Atk 2"
        }
    },
    xspdef2: {
        num: 980,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'def',
        amount: 2,
        names: {
            japanese: "ＳＰガード２",
            korean: "스페셜가드2",
            french: "Déf. Spé. 2",
            german: "X-Sp. Ver.2",
            spanish: "Def. Esp. X 2",
            italian: "Dif. Spec. X-2",
            english: "X Sp. Def 2"
        }
    },
    xdefense2: {
        num: 981,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'def',
        amount: 2,
        names: {
            japanese: "ディフェンダー２",
            korean: "디펜드업2",
            french: "Défense 2",
            german: "X-Abwehr2",
            spanish: "Defensa X 2",
            italian: "Difesa X-2",
            english: "X Defense 2"
        }
    },
    xattack2: {
        num: 982,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'atk',
        amount: 2,
        names: {
            japanese: "プラスパワー２",
            korean: "플러스파워2",
            french: "Attaque 2",
            german: "X-Angriff2",
            spanish: "Ataque X 2",
            italian: "Attacco X-2",
            english: "X Attack 2"
        }
    },
    xaccuracy2: {
        num: 983,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'acc',
        amount: 2,
        names: {
            japanese: "ヨクアタール２",
            korean: "잘-맞히기2",
            french: "Précision 2",
            german: "X-Treffer2",
            spanish: "Precisión X 2",
            italian: "Precisione X-2",
            english: "X Accuracy 2"
        }
    },
    xspeed3: {
        num: 984,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'spe',
        amount: 3,
        names: {
            japanese: "スピーダー３",
            korean: "스피드업3",
            french: "Vitesse 3",
            german: "X-Tempo3",
            spanish: "Velocidad X 3",
            italian: "Velocità X-3",
            english: "X Speed 3"
        }
    },
    xspatk3: {
        num: 985,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'atk',
        amount: 3,
        names: {
            japanese: "ＳＰアップ３",
            korean: "스페셜업3",
            french: "Atq. Spé. 3",
            german: "X-Spezial3",
            spanish: "Especial X 3",
            italian: "Special X-3",
            english: "X Sp. Atk 3"
        }
    },
    xspdef3: {
        num: 986,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'spd',
        amount: 3,
        names: {
            japanese: "ＳＰガード３",
            korean: "스페셜가드3",
            french: "Déf. Spé. 3",
            german: "X-Sp. Ver.3",
            spanish: "Def. Esp. X 3",
            italian: "Dif. Spec. X-3",
            english: "X Sp. Def 3"
        }
    },
    xdefense3: {
        num: 987,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'def',
        amount: 3,
        names: {
            japanese: "ディフェンダー３",
            korean: "디펜드업3",
            french: "Défense 3",
            german: "X-Abwehr3",
            spanish: "Defensa X 3",
            italian: "Difesa X-3",
            english: "X Defense 3"
        }
    },
    xattack3: {
        num: 988,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'atk',
        amount: 3,
        names: {
            japanese: "プラスパワー３",
            korean: "플러스파워3",
            french: "Attaque 3",
            german: "X-Angriff3",
            spanish: "Ataque X 3",
            italian: "Attacco X-3",
            english: "X Attack 3"
        }
    },
    xaccuracy3: {
        num: 989,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'acc',
        amount: 3,
        names: {
            japanese: "ヨクアタール３",
            korean: "잘-맞히기3",
            french: "Précision 3",
            german: "X-Treffer3",
            spanish: "Precisión X 3",
            italian: "Precisione X-3",
            english: "X Accuracy 3"
        }
    },
    xspeed6: {
        num: 990,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'spe',
        amount: 6,
        names: {
            japanese: "スピーダー６",
            korean: "스피드업6",
            french: "Vitesse 6",
            german: "X-Tempo6",
            spanish: "Velocidad X 6",
            italian: "Velocità X-6",
            english: "X Speed 6"
        }
    },
    xspatk6: {
        num: 991,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'atk',
        amount: 6,
        names: {
            japanese: "ＳＰアップ６",
            korean: "스페셜업6",
            french: "Atq. Spé. 6",
            german: "X-Spezial6",
            spanish: "Especial X 6",
            italian: "Special X-6",
            english: "X Sp. Atk 6"
        }
    },
    xspdef6: {
        num: 992,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'spd',
        amount: 6,
        names: {
            japanese: "ＳＰガード６",
            korean: "스페셜가드6",
            french: "Déf. Spé. 6",
            german: "X-Sp. Ver.6",
            spanish: "Def. Esp. X 6",
            italian: "Dif. Spec. X-6",
            english: "X Sp. Def 6"
        }
    },
    xdefense6: {
        num: 993,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'def',
        amount: 6,
        names: {
            japanese: "ディフェンダー６",
            korean: "디펜드업6",
            french: "Défense 6",
            german: "X-Abwehr6",
            spanish: "Defensa X 6",
            italian: "Difesa X-6",
            english: "X Defense 6"
        }
    },
    xattack6: {
        num: 994,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'atk',
        amount: 6,
        names: {
            japanese: "プラスパワー６",
            korean: "플러스파워6",
            french: "Attaque 6",
            german: "X-Angriff6",
            spanish: "Ataque X 6",
            italian: "Attacco X-6",
            english: "X Attack 6"
        }
    },
    xaccuracy6: {
        num: 995,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'acc',
        amount: 6,
        names: {
            japanese: "ヨクアタール６",
            korean: "잘-맞히기6",
            french: "Précision 6",
            german: "X-Treffer6",
            spanish: "Precisión X 6",
            italian: "Precisione X-6",
            english: "X Accuracy 6"
        }
    },
    abilityurge: {
        num: 996,
        cost: 0,
        itemcategory: "miracleshooter",
        useBattle: function(user){
            //@fixme
        },

        names: {
            japanese: "スキルコール",
            korean: "스킬콜",
            french: "Appel Talent",
            german: "Fäh.-Appell",
            spanish: "Habilitador",
            italian: "Chiamabilità",
            english: "Ability Urge"
        }
    },
    itemdrop: {
        num: 997,
        cost: 0,
        itemcategory: "miracleshooter",

        useBattle: function(user){
            //@fixme
        },

        names: {
            japanese: "アイテムドロップ",
            korean: "아이템드롭",
            french: "Jette Objet",
            german: "Itemabwurf",
            spanish: "Tiraobjeto",
            italian: "Lascioggetto",
            english: "Item Drop"
        }
    },
    itemurge: {
        num: 998,
        cost: 0,
        itemcategory: "miracleshooter",
        useBattle: function(user){
            //@fixme
        },
        names: {
            japanese: "アイテムコール",
            korean: "아이템콜",
            french: "Appel Objet",
            german: "Itemappell",
            spanish: "Activaobjeto",
            italian: "Chiamoggetto",
            english: "Item Urge"
        }
    },
    reseturge: {
        num: 999,
        cost: 0,
        itemcategory: "miracleshooter",
        useBattle: function(user){
            //@fixme
        },
        names: {
            japanese: "フラットコール",
            korean: "플랫콜",
            french: "Réamorçage",
            german: "Umkehrappell",
            spanish: "Quitaestado",
            italian: "Ripristino",
            english: "Reset Urge"
        }
    },
    direhit3: {
        num: 1000,
        cost: 0,
        itemcategory: "miracleshooter",
        stat: 'crit',
        amount: 3,
        names: {
            japanese: "クリティカット３",
            korean: "크리티컬커터3",
            french: "Muscle 3",
            german: "Angriffplus3",
            spanish: "Directo 3",
            italian: "Supercolpo 3",
            english: "Dire Hit 3"
        }
    },
    lightstone: {
        num: 1001,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ライトストーン",
            korean: "라이트스톤",
            french: "Galet Blanc",
            german: "Lichtstein",
            spanish: "Orbe Claro",
            italian: "Chiarolite",
            english: "Light Stone"
        }
    },
    darkstone: {
        num: 1002,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ダークストーン",
            korean: "다크스톤",
            french: "Galet Noir",
            german: "Dunkelstein",
            spanish: "Orbe Oscuro",
            italian: "Scurolite",
            english: "Dark Stone"
        }
    },
    tm93: {
        num: 1003,
        cost: 10000,
        itemcategory: "tm",
        tm: {
            blackwhite: {
                tm: 93,
                move: 528
            },
            black2white2: {
                tm: 93,
                move: 528
            },
            pokemonxy: {
                tm: 93,
                move: 528
            }
        },
        names: {
            japanese: "わざマシン９３",
            korean: "기술머신93",
            french: "CT93",
            german: "TM93",
            spanish: "MT93",
            italian: "MT93",
            english: "TM93"
        }
    },
    tm94: {
        num: 1004,
        cost: 10000,
        itemcategory: "tm",
        tm: {
            blackwhite: {
                tm: 94,
                move: 249
            },
            black2white2: {
                tm: 94,
                move: 249
            },
            pokemonxy: {
                tm: 94,
                move: 249
            }
        },
        names: {
            japanese: "わざマシン９４",
            korean: "기술머신94",
            french: "CT94",
            german: "TM94",
            spanish: "MT94",
            italian: "MT94",
            english: "TM94"
        }
    },
    tm95: {
        num: 1005,
        cost: 10000,
        itemcategory: "tm",
        tm: {
            blackwhite: {
                tm: 95,
                move: 555
            },
            black2white2: {
                tm: 95,
                move: 555
            },
            pokemonxy: {
                tm: 95,
                move: 555
            }
        },
        names: {
            japanese: "わざマシン９５",
            korean: "기술머신95",
            french: "CT95",
            german: "TM95",
            spanish: "MT95",
            italian: "MT95",
            english: "TM95"
        }
    },
    xtransceiver: {
        num: 1006,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ライブキャスター",
            korean: "라이브캐스터",
            french: "Vokit",
            german: "Viso-Caster",
            spanish: "Videomisor",
            italian: "Interpoké",
            english: "Xtransceiver"
        }
    },
    godstone: {
        num: 1007,
        cost: 0,
        itemcategory: "unuseditems",
        names: {
            japanese: "ゴッドストーン",
            english: "god stone"
        }
    },
    gram1: {
        num: 1008,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "はいたつぶつ１",
            korean: "배달물1",
            french: "Courrier 1",
            german: "Briefpost 1",
            spanish: "Envío 1",
            italian: "Missiva 1",
            english: "Gram 1"
        }
    },
    gram2: {
        num: 1009,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "はいたつぶつ２",
            korean: "배달물2",
            french: "Courrier 2",
            german: "Briefpost 2",
            spanish: "Envío 2",
            italian: "Missiva 2",
            english: "Gram 2"
        }
    },
    gram3: {
        num: 1010,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "はいたつぶつ３",
            korean: "배달물3",
            french: "Courrier 3",
            german: "Briefpost 3",
            spanish: "Envío 3",
            italian: "Missiva 3",
            english: "Gram 3"
        }
    },
    gram4: {
        num: 1011,
        cost: 0,
        itemcategory: 22
    },
    medalbox: {
        num: 1014,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "メダルボックス",
            korean: "메달박스",
            french: "Boîte Médailles",
            german: "Medaillenbox",
            spanish: "Caja Insignias",
            italian: "Box Premi",
            english: "Medal Box"
        }
    },
    dnasplicers: {
        num: 1015,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "いでんしのくさび",
            korean: "유전자쐐기",
            french: "Pointeau ADN",
            german: "DNS-Keil",
            spanish: "Punta ADN",
            italian: "Cuneo DNA",
            english: "DNA Splicers"
        }
    },
    permit: {
        num: 1016,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "きょかしょう",
            korean: "허가증",
            french: "Permis",
            german: "Genehmigung",
            spanish: "Pase",
            italian: "Permesso",
            english: "Permit"
        }
    },
    ovalcharm: {
        num: 1017,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "まるいおまもり",
            korean: "둥근부적",
            french: "Charme Ovale",
            german: "Ovalpin",
            spanish: "Amuleto Oval",
            italian: "Ovamuleto",
            english: "Oval Charm"
        }
    },
    shinycharm: {
        num: 1018,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ひかるおまもり",
            korean: "빛나는부적",
            french: "Charme Chroma",
            german: "Schillerpin",
            spanish: "Amuleto Iris",
            italian: "Cromamuleto",
            english: "Shiny Charm"
        }
    },
    plasmacard: {
        num: 1019,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "プラズマカード",
            korean: "플라스마카드",
            french: "Carte Plasma",
            german: "Plasmakarte",
            spanish: "Tarjeta Plasma",
            italian: "Carta Plasma",
            english: "Plasma Card"
        }
    },
    grubbyhanky: {
        num: 1020,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "よごれたハンカチ",
            korean: "더러운손수건",
            french: "Mouchoir Sale",
            german: "Schnäuztuch",
            spanish: "Pañuelo Sucio",
            italian: "Pezza Sporca",
            english: "Grubby Hanky"
        }
    },
    colressmachine: {
        num: 1021,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "アクロママシーン",
            korean: "아크로마머신",
            french: "Nikodule",
            german: "Achromat",
            spanish: "Acromáquina",
            italian: "Acrocongegno",
            english: "Colress Machine"
        }
    },
    droppeditem: {
        num: 1022,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "わすれもの",
            korean: "잊은물건",
            french: "Objet Trouvé",
            german: "Fundsache",
            spanish: "Objeto Perdido",
            italian: "Oggetto perso",
            english: "Dropped Item"
        }
    },
    revealglass: {
        num: 1023,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "うつしかがみ",
            korean: "비추는거울",
            french: "Miroir Sacré",
            german: "Wahrspiegel",
            spanish: "Espejo Veraz",
            italian: "Verispecchio",
            english: "Reveal Glass"
        }
    },
    abilitycapsule: {
        num: 1027,
        cost: 0,
        itemcategory: "vitamins",
        useOw: function(trainer,pokemon){
            //@fixme
        },
        stat: "ability",
        names: {
            japanese: "とくせいカプセル",
            korean: "특성캡슐",
            french: "Pilule Talent",
            german: "Fähigk.-Kapsel",
            spanish: "Cáps. Habilidad",
            italian: "Capsula abilità",
            english: "Ability Capsule"
        }
    },
    whippeddream: {
        num: 1028,
        cost: 0,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "ホイップポップ",
            korean: "휘핑팝",
            french: "Chantibonbon",
            german: "Sahnehäubchen",
            spanish: "Dulce de Nata",
            italian: "Dolcespuma",
            english: "Whipped Dream"
        }
    },
    sachet: {
        num: 1029,
        cost: 0,
        itemcategory: "evolution",
        usableoverworld:true,
        names: {
            japanese: "においぶくろ",
            korean: "향기주머니",
            french: "Sachet Senteur",
            german: "Duftbeutel",
            spanish: "Saquito Fragante",
            italian: "Bustina aromi",
            english: "Sachet"
        }
    },
    richmulch: {
        num: 1033,
        cost: 0,
        itemcategory: "mulch",
        names: {
            japanese: "たわわこやし",
            korean: "주렁주렁비료",
            french: "Fertibondance",
            german: "Sprießmulch",
            spanish: "Abono Fértil",
            italian: "Fertilflorido",
            english: "Rich Mulch"
        }
    },
    surprisemulch: {
        num: 1034,
        cost: 0,
        itemcategory: "mulch",
        names: {
            japanese: "びっくりこやし",
            korean: "깜놀비료",
            french: "Fertistantané",
            german: "Wundermulch",
            spanish: "Abono Sorpresa",
            italian: "Fertilsorpresa",
            english: "Surprise Mulch"
        }
    },
    boostmulch: {
        num: 1035,
        cost: 0,
        itemcategory: "mulch",
        names: {
            japanese: "ぐんぐんこやし",
            korean: "부쩍부쩍비료",
            french: "Fertibérance",
            german: "Wuchermulch",
            spanish: "Abono Fructífero",
            italian: "Fertilcopioso",
            english: "Boost Mulch"
        }
    },
    amazemulch: {
        num: 1036,
        cost: 0,
        itemcategory: "mulch",
        names: {
            japanese: "とんでもこやし",
            korean: "기절초풍비료",
            french: "Fertiprodige",
            german: "Ultramulch",
            spanish: "Abono Insólito",
            italian: "Fertilprodigio",
            english: "Amaze Mulch"
        }
    },
    discountcoupon: {
        num: 1068,
        cost: 0,
        itemcategory: 10001,
        names: {
            japanese: "バーゲンチケット",
            korean: "바겐세일티켓",
            french: "Bon Réduction",
            german: "Rabattmarke",
            spanish: "Vale Descuento",
            italian: "Buono sconto",
            english: "Discount Coupon"
        }
    },
    strangesouvenir: {
        num: 1069,
        cost: 0,
        itemcategory: 10001,
        names: {
            japanese: "ふしぎなおきもの",
            korean: "이상한장식품",
            french: "Bibelot Bizarre",
            german: "Skurriloskulptur",
            spanish: "Estatuilla Rara",
            italian: "Strano ninnolo",
            english: "Strange Souvenir"
        }
    },
    lumiosegalette: {
        num: 1070,
        cost: 0,
        itemcategory: "healing",
        status: "all",
        names: {
            japanese: "ミアレガレット",
            korean: "미르갈레트",
            french: "Galette Illumis",
            german: "Illumina-Galette",
            spanish: "Crêpe Luminalia",
            italian: "Pan di Lumi",
            english: "Lumiose Galette"
        }
    },
    jawfossil: {
        num: 1071,
        cost: 0,
        itemcategory: "dexcompletion",
        names: {
            japanese: "アゴのカセキ",
            korean: "턱화석",
            french: "Fossile Mâchoire",
            german: "Kieferfossil",
            spanish: "Fósil Mandíbula",
            italian: "Fossilmascella",
            english: "Jaw Fossil"
        }
    },
    sailfossil: {
        num: 1072,
        cost: 0,
        itemcategory: "dexcompletion",
        names: {
            japanese: "ヒレのカセキ",
            korean: "지느러미화석",
            french: "Fossile Nageoire",
            german: "Flossenfossil",
            spanish: "Fósil Aleta",
            italian: "Fossilpinna",
            english: "Sail Fossil"
        }
    },
    adventurerules: {
        num: 1074,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "たんけんこころえ",
            korean: "탐험수칙",
            french: "ABC Aventure",
            german: "Abenteuerfibel",
            spanish: "Guía de Máximas",
            italian: "Guida Avventura",
            english: "Adventure Rules"
        }
    },
    elevatorkey: {
        num: 1075,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "エレベータのキー",
            korean: "엘리베이터키",
            french: "Clé Ascenseur",
            german: "Liftschlüssel",
            spanish: "Llave Ascensor",
            italian: "Chiave ascensore",
            english: "Elevator Key"
        }
    },
    holocaster: {
        num: 1076,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ホロキャスター",
            korean: "홀로캐스터",
            french: "Holokit",
            german: "Holo-Log",
            spanish: "Holomisor",
            italian: "Holovox",
            english: "Holo Caster"
        }
    },
    honorofkalos: {
        num: 1077,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "カロスエンブレム",
            korean: "칼로스엠블럼",
            french: "Insigne de Kalos",
            german: "Kalos-Emblem",
            spanish: "Emblema Kalos",
            italian: "Emblema di Kalos",
            english: "Honor of Kalos"
        }
    },
    intriguingstone: {
        num: 1078,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "すごそうないし",
            korean: "대단할듯한돌",
            french: "Pierre Insolite",
            german: "Kurioser Stein",
            spanish: "Piedra Insólita",
            italian: "Sasso suggestivo",
            english: "Intriguing Stone"
        }
    },
    lenscase: {
        num: 1079,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "レンズケース",
            korean: "렌즈케이스",
            french: "Boîte Lentilles",
            german: "Linsenetui",
            spanish: "Portalentillas",
            italian: "Portalenti",
            english: "Lens Case"
        }
    },
    lookerticket: {
        num: 1080,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "ハンサムチケット",
            korean: "핸섬티켓",
            french: "Ticket Beladonis",
            german: "LeBelle-Ticket",
            spanish: "Boleto Handsome",
            italian: "Carta Bellocchio",
            english: "Looker Ticket"
        }
    },
    megaring: {
        num: 1081,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "メガリング",
            korean: "메가링",
            french: "Méga-Anneau",
            german: "Mega-Ring",
            spanish: "Mega-Aro",
            italian: "Megacerchio",
            english: "Mega Ring"
        }
    },
    powerplantpass: {
        num: 1082,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "はつでんしょパス",
            korean: "발전소패스",
            french: "Passe Centrale",
            german: "Kraftwerks-Pass",
            spanish: "Pase Central",
            italian: "Pass Centrale",
            english: "Power Plant Pass"
        }
    },
    profsletter: {
        num: 1083,
        cost: 0,
        itemcategory: "plotadvancement",
        names: {
            japanese: "はかせのてがみ",
            korean: "박사의편지",
            french: "Lettre du Prof",
            german: "Brief vom Prof",
            spanish: "Carta Profesor",
            italian: "Lettera del Prof",
            english: "Prof's Letter"
        }
    },
    rollerskates: {
        num: 1084,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ローラースケート",
            korean: "롤러스케이트",
            french: "Rollers",
            german: "Rollerskates",
            spanish: "Patines",
            italian: "Pattini",
            english: "Roller Skates"
        }
    },
    sprinklotad: {
        num: 1085,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ハスボーじょうろ",
            korean: "연꽃몬물뿌리개",
            french: "Nénurrosoir",
            german: "Loturzelkanne",
            spanish: "Lotadgadera",
            italian: "Irrigalotad",
            english: "Sprinklotad"
        }
    },
    tmvpass: {
        num: 1086,
        cost: 0,
        itemcategory: "gameplay",
        names: {
            japanese: "ＴＭＶパス",
            korean: "TMV패스",
            french: "Passe TMV",
            german: "TMV-Pass",
            spanish: "Abono del TMV",
            italian: "Pass TMV",
            english: "TMV Pass"
        }
    },
    tm96: {
        num: 1087,
        cost: 0,
        itemcategory: "tm",
        tm: {
            pokemonxy: {
                tm: 96,
                move: 267
            }
        },
        names: {
            japanese: "わざマシン９６",
            korean: "기술머신96",
            french: "CT96",
            german: "TM96",
            spanish: "MT96",
            italian: "MT96",
            english: "TM96"
        }
    },
    tm97: {
        num: 1088,
        cost: 0,
        itemcategory: "tm",
        tm: {
            pokemonxy: {
                tm: 97,
                move: 399
            }
        },
        names: {
            japanese: "わざマシン９７",
            korean: "기술머신97",
            french: "CT97",
            german: "TM97",
            spanish: "MT97",
            italian: "MT97",
            english: "TM97"
        }
    },
    tm98: {
        num: 1089,
        cost: 0,
        itemcategory: "tm",
        tm: {
            pokemonxy: {
                tm: 98,
                move: 612
            }
        },
        names: {
            japanese: "わざマシン９８",
            korean: "기술머신98",
            french: "CT98",
            german: "TM98",
            spanish: "MT98",
            italian: "MT98",
            english: "TM98"
        }
    },
    tm99: {
        num: 1090,
        cost: 0,
        itemcategory: "tm",
        tm: {
            pokemonxy: {
                tm: 99,
                move: 605
            }
        },
        names: {
            japanese: "わざマシン９９",
            korean: "기술머신99",
            french: "CT99",
            german: "TM99",
            spanish: "MT99",
            italian: "MT99",
            english: "TM99"
        }
    },
    tm100: {
        num: 1091,
        cost: 0,
        itemcategory: "tm",
        tm: {
            pokemonxy: {
                tm: 100,
                move: 590
            }
        },
        names: {
            japanese: "わざマシン１００",
            korean: "기술머신100",
            french: "CT100",
            german: "TM100",
            spanish: "MT100",
            italian: "MT100",
            english: "TM100"
        }
    }
}