

exports.moves = {
    "10000000voltthunderbolt": {
        num: 719
    },
    absorb: {
        num: 71,
        metacategory: 8,
        names: {
            japanese: "すいとる",
            korean: "흡수",
            french: "Vol-Vie",
            german: "Absorber",
            spanish: "Absorber",
            italian: "Assorbimento",
            english: "Absorb"
        }
    },
    accelerock: {
        num: 709
    },
    acid: {
        num: 51,
        metacategory: 6,
        names: {
            japanese: "ようかいえき",
            korean: "용해액",
            french: "Acide",
            german: "Säure",
            spanish: "Ácido",
            italian: "Acido",
            english: "Acid"
        }
    },
    acidarmor: {
        num: 151,
        metacategory: 2,
        names: {
            japanese: "とける",
            korean: "녹기",
            french: "Acidarmure",
            german: "Säurepanzer",
            spanish: "Armadura Ácida",
            italian: "Scudo Acido",
            english: "Acid Armor"
        }
    },
    aciddownpour: {
        num: 628
    },
    acidspray: {
        num: 491,
        metacategory: 6,
        names: {
            japanese: "アシッドボム",
            korean: "애시드봄",
            french: "Bombe Acide",
            german: "Säurespeier",
            spanish: "Bomba Ácida",
            italian: "Acidobomba",
            english: "Acid Spray"
        }
    },
    acrobatics: {
        num: 512,
        metacategory: 0,
        names: {
            japanese: "アクロバット",
            korean: "애크러뱃",
            french: "Acrobatie",
            german: "Akrobatik",
            spanish: "Acróbata",
            italian: "Acrobazia",
            english: "Acrobatics"
        }
    },
    acupressure: {
        num: 367,
        metacategory: 13,
        names: {
            japanese: "つぼをつく",
            korean: "경혈찌르기",
            french: "Acupression",
            german: "Akupressur",
            spanish: "Acupresión",
            italian: "Acupressione",
            english: "Acupressure"
        }
    },
    aerialace: {
        num: 332,
        metacategory: 0,
        names: {
            japanese: "つばめがえし",
            korean: "제비반환",
            french: "Aéropique",
            german: "Aero-Ass",
            spanish: "Golpe Aéreo",
            italian: "Aeroassalto",
            english: "Aerial Ace"
        }
    },
    aeroblast: {
        num: 177,
        metacategory: 0,
        names: {
            japanese: "エアロブラスト",
            korean: "에어로블라스트",
            french: "Aéroblast",
            german: "Luftstoß",
            spanish: "Aerochorro",
            italian: "Aerocolpo",
            english: "Aeroblast"
        }
    },
    afteryou: {
        num: 495,
        metacategory: 13,
        names: {
            japanese: "おさきにどうぞ",
            korean: "당신먼저",
            french: "Après Vous",
            german: "Galanterie",
            spanish: "Cede Paso",
            italian: "Cortesia",
            english: "After You"
        }
    },
    agility: {
        num: 97,
        metacategory: 2,
        names: {
            japanese: "こうそくいどう",
            korean: "고속이동",
            french: "Hâte",
            german: "Agilität",
            spanish: "Agilidad",
            italian: "Agilità",
            english: "Agility"
        }
    },
    aircutter: {
        num: 314,
        metacategory: 0,
        names: {
            japanese: "エアカッター",
            korean: "에어컷터",
            french: "Tranch'Air",
            german: "Windschnitt",
            spanish: "Aire Afilado",
            italian: "Aerasoio",
            english: "Air Cutter"
        }
    },
    airslash: {
        num: 403,
        metacategory: 0,
        names: {
            japanese: "エアスラッシュ",
            korean: "에어슬래시",
            french: "Lame d'Air",
            german: "Luftschnitt",
            spanish: "Tajo Aéreo",
            italian: "Eterelama",
            english: "Air Slash"
        }
    },
    alloutpummeling: {
        num: 624
    },
    allyswitch: {
        num: 502,
        metacategory: 13,
        names: {
            japanese: "サイドチェンジ",
            korean: "사이드체인지",
            french: "Interversion",
            german: "Seitentausch",
            spanish: "Cambio Banda",
            italian: "Cambiaposto",
            english: "Ally Switch"
        }
    },
    amnesia: {
        num: 133,
        metacategory: 2,
        names: {
            japanese: "ドわすれ",
            korean: "망각술",
            french: "Amnésie",
            german: "Amnesie",
            spanish: "Amnesia",
            italian: "Amnesia",
            english: "Amnesia"
        }
    },
    anchorshot: {
        num: 677
    },
    ancientpower: {
        num: 246,
        metacategory: 7,
        names: {
            japanese: "げんしのちから",
            korean: "원시의힘",
            french: "Pouvoir Antique",
            german: "Antik-Kraft",
            spanish: "Poder Pasado",
            italian: "Forzantica",
            english: "Ancient Power"
        }
    },
    aquajet: {
        num: 453,
        metacategory: 0,
        names: {
            japanese: "アクアジェット",
            korean: "아쿠아제트",
            french: "Aqua-Jet",
            german: "Wasserdüse",
            spanish: "Acua Jet",
            italian: "Acquagetto",
            english: "Aqua Jet"
        }
    },
    aquaring: {
        num: 392,
        metacategory: 13,
        names: {
            japanese: "アクアリング",
            korean: "아쿠아링",
            french: "Anneau Hydro",
            german: "Wasserring",
            spanish: "Acua Aro",
            italian: "Acquanello",
            english: "Aqua Ring"
        }
    },
    aquatail: {
        num: 401,
        metacategory: 0,
        names: {
            japanese: "アクアテール",
            korean: "아쿠아테일",
            french: "Hydroqueue",
            german: "Nassschweif",
            spanish: "Acua Cola",
            italian: "Idrondata",
            english: "Aqua Tail"
        }
    },
    armthrust: {
        num: 292,
        metacategory: 0,
        names: {
            japanese: "つっぱり",
            korean: "손바닥치기",
            french: "Cogne",
            german: "Armstoß",
            spanish: "Empujón",
            italian: "Sberletese",
            english: "Arm Thrust"
        }
    },
    aromatherapy: {
        num: 312,
        metacategory: 13,
        names: {
            japanese: "アロマセラピー",
            korean: "아로마테라피",
            french: "Aromathérapie",
            german: "Aromakur",
            spanish: "Aromaterapia",
            italian: "Aromaterapia",
            english: "Aromatherapy"
        }
    },
    aromaticmist: {
        num: 597,
        metacategory: 2,
        names: {
            japanese: "アロマミスト",
            korean: "아로마미스트",
            french: "Brume Capiteuse",
            german: "Duftwolke",
            spanish: "Niebla Aromática",
            italian: "Nebularoma",
            english: "Aromatic Mist"
        }
    },
    assist: {
        num: 274,
        metacategory: 13,
        names: {
            japanese: "ねこのて",
            korean: "조수",
            french: "Assistance",
            german: "Zuschuss",
            spanish: "Ayuda",
            italian: "Assistente",
            english: "Assist"
        }
    },
    assurance: {
        num: 372,
        metacategory: 0,
        names: {
            japanese: "ダメおし",
            korean: "승부굳히기",
            french: "Assurance",
            german: "Gewissheit",
            spanish: "Buena Baza",
            italian: "Garanzia",
            english: "Assurance"
        }
    },
    astonish: {
        num: 310,
        metacategory: 0,
        names: {
            japanese: "おどろかす",
            korean: "놀래키기",
            french: "Étonnement",
            german: "Erstauner",
            spanish: "Impresionar",
            italian: "Sgomento",
            english: "Astonish"
        }
    },
    attackorder: {
        num: 454,
        metacategory: 0,
        names: {
            japanese: "こうげきしれい",
            korean: "공격지령",
            french: "Appel Attak",
            german: "Schlagbefehl",
            spanish: "Al Ataque",
            italian: "Comandourto",
            english: "Attack Order"
        }
    },
    attract: {
        num: 213,
        metacategory: 1,
        names: {
            japanese: "メロメロ",
            korean: "헤롱헤롱",
            french: "Attraction",
            german: "Anziehung",
            spanish: "Atracción",
            italian: "Attrazione",
            english: "Attract"
        }
    },
    aurasphere: {
        num: 396,
        metacategory: 0,
        names: {
            japanese: "はどうだん",
            korean: "파동탄",
            french: "Aurasphère",
            german: "Aurasphäre",
            spanish: "Esfera Aural",
            italian: "Forzasfera",
            english: "Aura Sphere"
        }
    },
    aurorabeam: {
        num: 62,
        metacategory: 6,
        names: {
            japanese: "オーロラビーム",
            korean: "오로라빔",
            french: "Onde Boréale",
            german: "Aurorastrahl",
            spanish: "Rayo Aurora",
            italian: "Raggiaurora",
            english: "Aurora Beam"
        }
    },
    auroraveil: {
        num: 694
    },
    autotomize: {
        num: 475,
        metacategory: 2,
        names: {
            japanese: "ボディパージ",
            korean: "바디퍼지",
            french: "Allègement",
            german: "Autotomie",
            spanish: "Aligerar",
            italian: "Sganciapesi",
            english: "Autotomize"
        }
    },
    avalanche: {
        num: 419,
        metacategory: 0,
        names: {
            japanese: "ゆきなだれ",
            korean: "눈사태",
            french: "Avalanche",
            german: "Lawine",
            spanish: "Alud",
            italian: "Slavina",
            english: "Avalanche"
        }
    },
    babydolleyes: {
        num: 608,
        metacategory: 2,
        names: {
            japanese: "つぶらなひとみ",
            korean: "초롱초롱눈동자",
            french: "Regard Touchant",
            german: "Kulleraugen",
            spanish: "Ojitos Tiernos",
            italian: "Occhioni Teneri",
            english: "Baby-Doll Eyes"
        }
    },
    banefulbunker: {
        num: 661
    },
    barrage: {
        num: 140,
        metacategory: 0,
        names: {
            japanese: "たまなげ",
            korean: "구슬던지기",
            french: "Pilonnage",
            german: "Stakkato",
            spanish: "Presa",
            italian: "Attacco Pioggia",
            english: "Barrage"
        }
    },
    barrier: {
        num: 112,
        metacategory: 2,
        names: {
            japanese: "バリアー",
            korean: "배리어",
            french: "Bouclier",
            german: "Barriere",
            spanish: "Barrera",
            italian: "Barriera",
            english: "Barrier"
        }
    },
    batonpass: {
        num: 226,
        metacategory: 13,
        names: {
            japanese: "バトンタッチ",
            korean: "바톤터치",
            french: "Relais",
            german: "Staffette",
            spanish: "Relevo",
            italian: "Staffetta",
            english: "Baton Pass"
        }
    },
    beakblast: {
        num: 690
    },
    beatup: {
        num: 251,
        metacategory: 0,
        names: {
            japanese: "ふくろだたき",
            korean: "집단구타",
            french: "Baston",
            german: "Prügler",
            spanish: "Paliza",
            italian: "Picchiaduro",
            english: "Beat Up"
        }
    },
    belch: {
        num: 562,
        metacategory: 0,
        names: {
            japanese: "ゲップ",
            korean: "트림",
            french: "Éructation",
            german: "Rülpser",
            spanish: "Eructo",
            italian: "Rutto",
            english: "Belch"
        }
    },
    bellydrum: {
        num: 187,
        metacategory: 13,
        names: {
            japanese: "はらだいこ",
            korean: "배북",
            french: "Cognobidon",
            german: "Bauchtrommel",
            spanish: "Tambor",
            italian: "Panciamburo",
            english: "Belly Drum"
        }
    },
    bestow: {
        num: 516,
        metacategory: 13,
        names: {
            japanese: "ギフトパス",
            korean: "기프트패스",
            french: "Passe-Cadeau",
            german: "Offerte",
            spanish: "Ofrenda",
            italian: "Cediregalo",
            english: "Bestow"
        }
    },
    bide: {
        num: 117,
        metacategory: 0,
        names: {
            japanese: "がまん",
            korean: "참기",
            french: "Patience",
            german: "Geduld",
            spanish: "Venganza",
            italian: "Pazienza",
            english: "Bide"
        }
    },
    bind: {
        num: 20,
        metacategory: 4,
        names: {
            japanese: "しめつける",
            korean: "조이기",
            french: "Étreinte",
            german: "Klammergriff",
            spanish: "Atadura",
            italian: "Legatutto",
            english: "Bind"
        }
    },
    bite: {
        num: 44,
        metacategory: 0,
        names: {
            japanese: "かみつく",
            korean: "물기",
            french: "Morsure",
            german: "Biss",
            spanish: "Mordisco",
            italian: "Morso",
            english: "Bite"
        }
    },
    blackholeeclipse: {
        num: 654
    },
    blastburn: {
        num: 307,
        metacategory: 0,
        names: {
            japanese: "ブラストバーン",
            korean: "블러스트번",
            french: "Rafale Feu",
            german: "Lohekanonade",
            spanish: "Anillo Ígneo",
            italian: "Incendio",
            english: "Blast Burn"
        }
    },
    blazekick: {
        num: 299,
        metacategory: 4,
        names: {
            japanese: "ブレイズキック",
            korean: "브레이즈킥",
            french: "Pied Brûleur",
            german: "Feuerfeger",
            spanish: "Patada Ígnea",
            italian: "Calciardente",
            english: "Blaze Kick"
        }
    },
    blizzard: {
        num: 59,
        metacategory: 4,
        names: {
            japanese: "ふぶき",
            korean: "눈보라",
            french: "Blizzard",
            german: "Blizzard",
            spanish: "Ventisca",
            italian: "Bora",
            english: "Blizzard"
        }
    },
    block: {
        num: 335,
        metacategory: 13,
        names: {
            japanese: "とおせんぼう",
            korean: "블록",
            french: "Barrage",
            german: "Rückentzug",
            spanish: "Bloqueo",
            italian: "Blocco",
            english: "Block"
        }
    },
    bloomdoom: {
        num: 644
    },
    blueflare: {
        num: 551,
        metacategory: 4,
        names: {
            japanese: "あおいほのお",
            korean: "푸른불꽃",
            french: "Flamme Bleue",
            german: "Blauflammen",
            spanish: "Llama Azul",
            italian: "Fuocoblu",
            english: "Blue Flare"
        }
    },
    bodyslam: {
        num: 34,
        metacategory: 4,
        names: {
            japanese: "のしかかり",
            korean: "누르기",
            french: "Plaquage",
            german: "Bodyslam",
            spanish: "Golpe Cuerpo",
            italian: "Corposcontro",
            english: "Body Slam"
        }
    },
    boltstrike: {
        num: 550,
        metacategory: 4,
        names: {
            japanese: "らいげき",
            korean: "뇌격",
            french: "Charge Foudre",
            german: "Blitzschlag",
            spanish: "Ataque Fulgor",
            italian: "Lucesiluro",
            english: "Bolt Strike"
        }
    },
    boneclub: {
        num: 125,
        metacategory: 0,
        names: {
            japanese: "ホネこんぼう",
            korean: "뼈다귀치기",
            french: "Massd'Os",
            german: "Knochenkeule",
            spanish: "Hueso Palo",
            italian: "Ossoclava",
            english: "Bone Club"
        }
    },
    bonerush: {
        num: 198,
        metacategory: 0,
        names: {
            japanese: "ボーンラッシュ",
            korean: "본러쉬",
            french: "Charge-Os",
            german: "Knochenhatz",
            spanish: "Ataque Óseo",
            italian: "Ossoraffica",
            english: "Bone Rush"
        }
    },
    bonemerang: {
        num: 155,
        metacategory: 0,
        names: {
            japanese: "ホネブーメラン",
            korean: "뼈다귀부메랑",
            french: "Osmerang",
            german: "Knochmerang",
            spanish: "Huesomerang",
            italian: "Ossomerang",
            english: "Bonemerang"
        }
    },
    boomburst: {
        num: 586,
        metacategory: 0,
        names: {
            japanese: "ばくおんぱ",
            korean: "폭음파",
            french: "Bang Sonique",
            german: "Überschallknall",
            spanish: "Estruendo",
            italian: "Ondaboato",
            english: "Boomburst"
        }
    },
    bounce: {
        num: 340,
        metacategory: 4,
        names: {
            japanese: "とびはねる",
            korean: "뛰어오르다",
            french: "Rebond",
            german: "Sprungfeder",
            spanish: "Bote",
            italian: "Rimbalzo",
            english: "Bounce"
        }
    },
    bravebird: {
        num: 413,
        metacategory: 0,
        names: {
            japanese: "ブレイブバード",
            korean: "브레이브버드",
            french: "Rapace",
            german: "Sturzflug",
            spanish: "Pájaro Osado",
            italian: "Baldeali",
            english: "Brave Bird"
        }
    },
    breakneckblitz: {
        num: 622
    },
    brickbreak: {
        num: 280,
        metacategory: 0,
        names: {
            japanese: "かわらわり",
            korean: "깨트리다",
            french: "Casse-Brique",
            german: "Durchbruch",
            spanish: "Demolición",
            italian: "Breccia",
            english: "Brick Break"
        }
    },
    brine: {
        num: 362,
        metacategory: 0,
        names: {
            japanese: "しおみず",
            korean: "소금물",
            french: "Saumure",
            german: "Lake",
            spanish: "Salmuera",
            italian: "Acquadisale",
            english: "Brine"
        }
    },
    brutalswing: {
        num: 693
    },
    bubble: {
        num: 145,
        metacategory: 6,
        names: {
            japanese: "あわ",
            korean: "거품",
            french: "Écume",
            german: "Blubber",
            spanish: "Burbuja",
            italian: "Bolla",
            english: "Bubble"
        }
    },
    bubblebeam: {
        num: 61,
        metacategory: 6,
        names: {
            japanese: "バブルこうせん",
            korean: "거품광선",
            french: "Bulles d'O",
            german: "Blubbstrahl",
            spanish: "Rayo Burbuja",
            italian: "Bollaraggio",
            english: "Bubble Beam"
        }
    },
    bugbite: {
        num: 450,
        metacategory: 0,
        names: {
            japanese: "むしくい",
            korean: "벌레먹음",
            french: "Piqûre",
            german: "Käferbiss",
            spanish: "Picadura",
            italian: "Coleomorso",
            english: "Bug Bite"
        }
    },
    bugbuzz: {
        num: 405,
        metacategory: 6,
        names: {
            japanese: "むしのさざめき",
            korean: "벌레의야단법석",
            french: "Bourdon",
            german: "Käfergebrumm",
            spanish: "Zumbido",
            italian: "Ronzio",
            english: "Bug Buzz"
        }
    },
    bulkup: {
        num: 339,
        metacategory: 2,
        names: {
            japanese: "ビルドアップ",
            korean: "벌크업",
            french: "Gonflette",
            german: "Protzer",
            spanish: "Corpulencia",
            italian: "Granfisico",
            english: "Bulk Up"
        }
    },
    bulldoze: {
        num: 523,
        metacategory: 6,
        names: {
            japanese: "じならし",
            korean: "땅고르기",
            french: "Piétisol",
            german: "Dampfwalze",
            spanish: "Terratemblor",
            italian: "Battiterra",
            english: "Bulldoze"
        }
    },
    bulletpunch: {
        num: 418,
        metacategory: 0,
        names: {
            japanese: "バレットパンチ",
            korean: "불릿펀치",
            french: "Pisto-Poing",
            german: "Patronenhieb",
            spanish: "Puño Bala",
            italian: "Pugnoscarica",
            english: "Bullet Punch"
        }
    },
    bulletseed: {
        num: 331,
        metacategory: 0,
        names: {
            japanese: "タネマシンガン",
            korean: "기관총",
            french: "Balle Graine",
            german: "Kugelsaat",
            spanish: "Recurrente",
            italian: "Semitraglia",
            english: "Bullet Seed"
        }
    },
    burnup: {
        num: 682
    },
    calmmind: {
        num: 347,
        metacategory: 2,
        names: {
            japanese: "めいそう",
            korean: "명상",
            french: "Plénitude",
            german: "Gedankengut",
            spanish: "Paz Mental",
            italian: "Calmamente",
            english: "Calm Mind"
        }
    },
    camouflage: {
        num: 293,
        metacategory: 13,
        names: {
            japanese: "ほごしょく",
            korean: "보호색",
            french: "Camouflage",
            german: "Tarnung",
            spanish: "Camuflaje",
            italian: "Camuffamento",
            english: "Camouflage"
        }
    },
    captivate: {
        num: 445,
        metacategory: 2,
        names: {
            japanese: "ゆうわく",
            korean: "유혹",
            french: "Séduction",
            german: "Liebreiz",
            spanish: "Seducción",
            italian: "Incanto",
            english: "Captivate"
        }
    },
    catastropika: {
        num: 658
    },
    celebrate: {
        num: 606,
        metacategory: 13,
        names: {
            japanese: "おいわい",
            korean: "축하",
            french: "Célébration",
            german: "Ehrentag",
            spanish: "Celebración",
            italian: "Auguri",
            english: "Celebrate"
        }
    },
    charge: {
        num: 268,
        metacategory: 2,
        names: {
            japanese: "じゅうでん",
            korean: "충전",
            french: "Chargeur",
            german: "Ladevorgang",
            spanish: "Carga",
            italian: "Sottocarica",
            english: "Charge"
        }
    },
    chargebeam: {
        num: 451,
        metacategory: 7,
        names: {
            japanese: "チャージビーム",
            korean: "차지빔",
            french: "Rayon Chargé",
            german: "Ladestrahl",
            spanish: "Rayo Carga",
            italian: "Raggioscossa",
            english: "Charge Beam"
        }
    },
    charm: {
        num: 204,
        metacategory: 2,
        names: {
            japanese: "あまえる",
            korean: "애교부리기",
            french: "Charme",
            german: "Charme",
            spanish: "Encanto",
            italian: "Fascino",
            english: "Charm"
        }
    },
    chatter: {
        num: 448,
        metacategory: 4,
        names: {
            japanese: "おしゃべり",
            korean: "수다",
            french: "Babil",
            german: "Geschwätz",
            spanish: "Cháchara",
            italian: "Schiamazzo",
            english: "Chatter"
        }
    },
    chipaway: {
        num: 498,
        metacategory: 0,
        names: {
            japanese: "なしくずし",
            korean: "야금야금",
            french: "Attrition",
            german: "Zermürben",
            spanish: "Guardia Baja",
            italian: "Insidia",
            english: "Chip Away"
        }
    },
    circlethrow: {
        num: 509,
        metacategory: 0,
        names: {
            japanese: "ともえなげ",
            korean: "배대뒤치기",
            french: "Projection",
            german: "Überkopfwurf",
            spanish: "Llave Giro",
            italian: "Ribaltiro",
            english: "Circle Throw"
        }
    },
    clamp: {
        num: 128,
        metacategory: 4,
        names: {
            japanese: "からではさむ",
            korean: "껍질끼우기",
            french: "Claquoir",
            german: "Schnapper",
            spanish: "Tenaza",
            italian: "Tenaglia",
            english: "Clamp"
        }
    },
    clangingscales: {
        num: 691
    },
    clearsmog: {
        num: 499,
        metacategory: 0,
        names: {
            japanese: "クリアスモッグ",
            korean: "클리어스모그",
            french: "Bain de Smog",
            german: "Klärsmog",
            spanish: "Niebla Clara",
            italian: "Pulifumo",
            english: "Clear Smog"
        }
    },
    closecombat: {
        num: 370,
        metacategory: 7,
        names: {
            japanese: "インファイト",
            korean: "인파이트",
            french: "Close Combat",
            german: "Nahkampf",
            spanish: "A Bocajarro",
            italian: "Zuffa",
            english: "Close Combat"
        }
    },
    coil: {
        num: 489,
        metacategory: 2,
        names: {
            japanese: "とぐろをまく",
            korean: "똬리틀기",
            french: "Enroulement",
            german: "Einrollen",
            spanish: "Enrosque",
            italian: "Arrotola",
            english: "Coil"
        }
    },
    cometpunch: {
        num: 4,
        metacategory: 0,
        names: {
            japanese: "れんぞくパンチ",
            korean: "연속펀치",
            french: "Poing Comète",
            german: "Kometenhieb",
            spanish: "Puño Cometa",
            italian: "Cometapugno",
            english: "Comet Punch"
        }
    },
    confide: {
        num: 590,
        metacategory: 2,
        names: {
            japanese: "ないしょばなし",
            korean: "비밀이야기",
            french: "Confidence",
            german: "Vertrauenssache",
            spanish: "Confidencia",
            italian: "Confidenza",
            english: "Confide"
        }
    },
    confuseray: {
        num: 109,
        metacategory: 1,
        names: {
            japanese: "あやしいひかり",
            korean: "이상한빛",
            french: "Onde Folie",
            german: "Konfustrahl",
            spanish: "Rayo Confuso",
            italian: "Stordiraggio",
            english: "Confuse Ray"
        }
    },
    confusion: {
        num: 93,
        metacategory: 4,
        names: {
            japanese: "ねんりき",
            korean: "염동력",
            french: "Choc Mental",
            german: "Konfusion",
            spanish: "Confusión",
            italian: "Confusione",
            english: "Confusion"
        }
    },
    constrict: {
        num: 132,
        metacategory: 6,
        names: {
            japanese: "からみつく",
            korean: "휘감기",
            french: "Constriction",
            german: "Umklammerung",
            spanish: "Restricción",
            italian: "Limitazione",
            english: "Constrict"
        }
    },
    continentalcrush: {
        num: 632
    },
    conversion: {
        num: 160,
        metacategory: 13,
        names: {
            japanese: "テクスチャー",
            korean: "텍스처",
            french: "Conversion",
            german: "Umwandlung",
            spanish: "Conversión",
            italian: "Conversione",
            english: "Conversion"
        }
    },
    conversion2: {
        num: 176,
        metacategory: 13,
        names: {
            japanese: "テクスチャー２",
            korean: "텍스처2",
            french: "Conversion 2",
            german: "Umwandlung2",
            spanish: "Conversión2",
            italian: "Conversione2",
            english: "Conversion 2"
        }
    },
    copycat: {
        num: 383,
        metacategory: 13,
        names: {
            japanese: "まねっこ",
            korean: "흉내쟁이",
            french: "Photocopie",
            german: "Imitator",
            spanish: "Copión",
            italian: "Copione",
            english: "Copycat"
        }
    },
    coreenforcer: {
        num: 687
    },
    corkscrewcrash: {
        num: 638
    },
    cosmicpower: {
        num: 322,
        metacategory: 2,
        names: {
            japanese: "コスモパワー",
            korean: "코스믹파워",
            french: "Force Cosmik",
            german: "Kosmik-Kraft",
            spanish: "Masa Cósmica",
            italian: "Cosmoforza",
            english: "Cosmic Power"
        }
    },
    cottonguard: {
        num: 538,
        metacategory: 2,
        names: {
            japanese: "コットンガード",
            korean: "코튼가드",
            french: "Cotogarde",
            german: "Watteschild",
            spanish: "Rizo Algodón",
            italian: "Cotonscudo",
            english: "Cotton Guard"
        }
    },
    cottonspore: {
        num: 178,
        metacategory: 2,
        names: {
            japanese: "わたほうし",
            korean: "목화포자",
            french: "Spore Coton",
            german: "Baumwollsaat",
            spanish: "Esporagodón",
            italian: "Cottonspora",
            english: "Cotton Spore"
        }
    },
    counter: {
        num: 68,
        metacategory: 0,
        names: {
            japanese: "カウンター",
            korean: "카운터",
            french: "Riposte",
            german: "Konter",
            spanish: "Contraataque",
            italian: "Contrattacco",
            english: "Counter"
        }
    },
    covet: {
        num: 343,
        metacategory: 0,
        names: {
            japanese: "ほしがる",
            korean: "탐내다",
            french: "Implore",
            german: "Bezirzer",
            spanish: "Antojo",
            italian: "Supplica",
            english: "Covet"
        }
    },
    crabhammer: {
        num: 152,
        metacategory: 0,
        names: {
            japanese: "クラブハンマー",
            korean: "찝게햄머",
            french: "Pince-Masse",
            german: "Krabbhammer",
            spanish: "Martillazo",
            italian: "Martellata",
            english: "Crabhammer"
        }
    },
    craftyshield: {
        num: 578,
        metacategory: 11,
        names: {
            japanese: "トリックガード",
            korean: "트릭가드",
            french: "Vigilance",
            german: "Trickschutz",
            spanish: "Truco Defensa",
            italian: "Truccodifesa",
            english: "Crafty Shield"
        }
    },
    crosschop: {
        num: 238,
        metacategory: 0,
        names: {
            japanese: "クロスチョップ",
            korean: "크로스촙",
            french: "Coup-Croix",
            german: "Kreuzhieb",
            spanish: "Tajo Cruzado",
            italian: "Incrocolpo",
            english: "Cross Chop"
        }
    },
    crosspoison: {
        num: 440,
        metacategory: 4,
        names: {
            japanese: "クロスポイズン",
            korean: "크로스포이즌",
            french: "Poison-Croix",
            german: "Giftstreich",
            spanish: "Veneno X",
            italian: "Velenocroce",
            english: "Cross Poison"
        }
    },
    crunch: {
        num: 242,
        metacategory: 6,
        names: {
            japanese: "かみくだく",
            korean: "깨물어부수기",
            french: "Mâchouille",
            german: "Knirscher",
            spanish: "Triturar",
            italian: "Sgranocchio",
            english: "Crunch"
        }
    },
    crushclaw: {
        num: 306,
        metacategory: 6,
        names: {
            japanese: "ブレイククロー",
            korean: "브레이크크루",
            french: "Éclate Griffe",
            german: "Zermalmklaue",
            spanish: "Garra Brutal",
            italian: "Tritartigli",
            english: "Crush Claw"
        }
    },
    crushgrip: {
        num: 462,
        metacategory: 0,
        names: {
            japanese: "にぎりつぶす",
            korean: "묵사발",
            french: "Presse",
            german: "Quetschgriff",
            spanish: "Agarrón",
            italian: "Sbriciolmano",
            english: "Crush Grip"
        }
    },
    curse: {
        num: 174,
        metacategory: 13,
        names: {
            japanese: "のろい",
            korean: "저주",
            french: "Malédiction",
            german: "Fluch",
            spanish: "Maldición",
            italian: "Maledizione",
            english: "Curse"
        }
    },
    cut: {
        num: 15,
        metacategory: 0,
        names: {
            japanese: "いあいぎり",
            korean: "풀베기",
            french: "Coupe",
            german: "Zerschneider",
            spanish: "Corte",
            italian: "Taglio",
            english: "Cut"
        }
    },
    darkpulse: {
        num: 399,
        metacategory: 0,
        names: {
            japanese: "あくのはどう",
            korean: "악의파동",
            french: "Vibrobscur",
            german: "Finsteraura",
            spanish: "Pulso Umbrío",
            italian: "Neropulsar",
            english: "Dark Pulse"
        }
    },
    darkvoid: {
        num: 464,
        metacategory: 1,
        names: {
            japanese: "ダークホール",
            korean: "다크홀",
            french: "Trou Noir",
            german: "Schlummerort",
            spanish: "Brecha Negra",
            italian: "Vuototetro",
            english: "Dark Void"
        }
    },
    darkestlariat: {
        num: 663
    },
    dazzlinggleam: {
        num: 605,
        metacategory: 0,
        names: {
            japanese: "マジカルシャイン",
            korean: "매지컬샤인",
            french: "Éclat Magique",
            german: "Zauberschein",
            spanish: "Brillo Mágico",
            italian: "Magibrillio",
            english: "Dazzling Gleam"
        }
    },
    defendorder: {
        num: 455,
        metacategory: 2,
        names: {
            japanese: "ぼうぎょしれい",
            korean: "방어지령",
            french: "Appel Défense",
            german: "Blockbefehl",
            spanish: "A Defender",
            italian: "Comandoscudo",
            english: "Defend Order"
        }
    },
    defensecurl: {
        num: 111,
        metacategory: 2,
        names: {
            japanese: "まるくなる",
            korean: "웅크리기",
            french: "Boul'Armure",
            german: "Einigler",
            spanish: "Rizo Defensa",
            italian: "Ricciolscudo",
            english: "Defense Curl"
        }
    },
    defog: {
        num: 432,
        metacategory: 13,
        names: {
            japanese: "きりばらい",
            korean: "안개제거",
            french: "Anti-Brume",
            german: "Auflockern",
            spanish: "Despejar",
            italian: "Scacciabruma",
            english: "Defog"
        }
    },
    destinybond: {
        num: 194,
        metacategory: 13,
        names: {
            japanese: "みちづれ",
            korean: "길동무",
            french: "Prélèvem. Destin",
            german: "Abgangsbund",
            spanish: "Mismo Destino",
            italian: "Destinobbligato",
            english: "Destiny Bond"
        }
    },
    detect: {
        num: 197,
        metacategory: 13,
        names: {
            japanese: "みきり",
            korean: "판별",
            french: "Détection",
            german: "Scanner",
            spanish: "Detección",
            italian: "Individua",
            english: "Detect"
        }
    },
    devastatingdrake: {
        num: 652
    },
    diamondstorm: {
        num: 591,
        metacategory: 7,
        names: {
            japanese: "ダイヤストーム",
            korean: "다이아스톰",
            french: "Orage Adamantin",
            german: "Diamantsturm",
            spanish: "Torm. Diamantes",
            italian: "Diamantempesta",
            english: "Diamond Storm"
        }
    },
    dig: {
        num: 91,
        metacategory: 0,
        names: {
            japanese: "あなをほる",
            korean: "구멍파기",
            french: "Tunnel",
            german: "Schaufler",
            spanish: "Excavar",
            italian: "Fossa",
            english: "Dig"
        }
    },
    disable: {
        num: 50,
        metacategory: 13,
        names: {
            japanese: "かなしばり",
            korean: "사슬묶기",
            french: "Entrave",
            german: "Aussetzer",
            spanish: "Anulación",
            italian: "Inibitore",
            english: "Disable"
        }
    },
    disarmingvoice: {
        num: 574,
        metacategory: 0,
        names: {
            japanese: "チャームボイス",
            korean: "차밍보이스",
            french: "Voix Enjôleuse",
            german: "Säuselstimme",
            spanish: "Voz Cautivadora",
            italian: "Incantavoce",
            english: "Disarming Voice"
        }
    },
    discharge: {
        num: 435,
        metacategory: 4,
        names: {
            japanese: "ほうでん",
            korean: "방전",
            french: "Coup d'Jus",
            german: "Ladungsstoß",
            spanish: "Chispazo",
            italian: "Scarica",
            english: "Discharge"
        }
    },
    dive: {
        num: 291,
        metacategory: 0,
        names: {
            japanese: "ダイビング",
            korean: "다이빙",
            french: "Plongée",
            german: "Taucher",
            spanish: "Buceo",
            italian: "Sub",
            english: "Dive"
        }
    },
    dizzypunch: {
        num: 146,
        metacategory: 4,
        names: {
            japanese: "ピヨピヨパンチ",
            korean: "잼잼펀치",
            french: "Uppercut",
            german: "Irrschlag",
            spanish: "Puño Mareo",
            italian: "Stordipugno",
            english: "Dizzy Punch"
        }
    },
    doomdesire: {
        num: 353,
        metacategory: 13,
        names: {
            japanese: "はめつのねがい",
            korean: "파멸의소원",
            french: "Carnareket",
            german: "Kismetwunsch",
            spanish: "Deseo Oculto",
            italian: "Obbliderio",
            english: "Doom Desire"
        }
    },
    doubleedge: {
        num: 38,
        metacategory: 0,
        names: {
            japanese: "すてみタックル",
            korean: "이판사판태클",
            french: "Damoclès",
            german: "Risikotackle",
            spanish: "Doble Filo",
            italian: "Sdoppiatore",
            english: "Double-Edge"
        }
    },
    doublehit: {
        num: 458,
        metacategory: 0,
        names: {
            japanese: "ダブルアタック",
            korean: "더블어택",
            french: "Coup Double",
            german: "Doppelschlag",
            spanish: "Doble Golpe",
            italian: "Doppiosmash",
            english: "Double Hit"
        }
    },
    doublekick: {
        num: 24,
        metacategory: 0,
        names: {
            japanese: "にどげり",
            korean: "두번치기",
            french: "Double Pied",
            german: "Doppelkick",
            spanish: "Doble Patada",
            italian: "Doppiocalcio",
            english: "Double Kick"
        }
    },
    doubleslap: {
        num: 3,
        metacategory: 0,
        names: {
            japanese: "おうふくビンタ",
            korean: "연속뺨치기",
            french: "Torgnoles",
            german: "Duplexhieb",
            spanish: "Doble Bofetón",
            italian: "Doppiasberla",
            english: "Double Slap"
        }
    },
    doubleteam: {
        num: 104,
        metacategory: 2,
        names: {
            japanese: "かげぶんしん",
            korean: "그림자분신",
            french: "Reflet",
            german: "Doppelteam",
            spanish: "Doble Equipo",
            italian: "Doppioteam",
            english: "Double Team"
        }
    },
    dracometeor: {
        num: 434,
        metacategory: 7,
        names: {
            japanese: "りゅうせいぐん",
            korean: "용성군",
            french: "Draco Météor",
            german: "Draco Meteor",
            spanish: "Cometa Draco",
            italian: "Dragobolide",
            english: "Draco Meteor"
        }
    },
    dragonascent: {
        num: 620,
        names: {
            japanese: "ガリョウテンセイ",
            french: "Draco Ascension",
            german: "Zenitstürmer",
            spanish: "Ascenso Draco",
            italian: "Ascesa del Drago",
            english: "Dragon Ascent"
        }
    },
    dragonbreath: {
        num: 225,
        metacategory: 4,
        names: {
            japanese: "りゅうのいぶき",
            korean: "용의숨결",
            french: "Dracosouffle",
            german: "Feuerodem",
            spanish: "Dragoaliento",
            italian: "Dragospiro",
            english: "Dragon Breath"
        }
    },
    dragonclaw: {
        num: 337,
        metacategory: 0,
        names: {
            japanese: "ドラゴンクロー",
            korean: "드래곤크루",
            french: "Dracogriffe",
            german: "Drachenklaue",
            spanish: "Garra Dragón",
            italian: "Dragartigli",
            english: "Dragon Claw"
        }
    },
    dragondance: {
        num: 349,
        metacategory: 2,
        names: {
            japanese: "りゅうのまい",
            korean: "용의춤",
            french: "Danse Draco",
            german: "Drachentanz",
            spanish: "Danza Dragón",
            italian: "Dragodanza",
            english: "Dragon Dance"
        }
    },
    dragonhammer: {
        num: 692
    },
    dragonpulse: {
        num: 406,
        metacategory: 0,
        names: {
            japanese: "りゅうのはどう",
            korean: "용의파동",
            french: "Dracochoc",
            german: "Drachenpuls",
            spanish: "Pulso Dragón",
            italian: "Dragopulsar",
            english: "Dragon Pulse"
        }
    },
    dragonrage: {
        num: 82,
        metacategory: 0,
        names: {
            japanese: "りゅうのいかり",
            korean: "용의분노",
            french: "Draco-Rage",
            german: "Drachenwut",
            spanish: "Furia Dragón",
            italian: "Ira di Drago",
            english: "Dragon Rage"
        }
    },
    dragonrush: {
        num: 407,
        metacategory: 0,
        names: {
            japanese: "ドラゴンダイブ",
            korean: "드래곤다이브",
            french: "Dracocharge",
            german: "Drachenstoß",
            spanish: "Carga Dragón",
            italian: "Dragofuria",
            english: "Dragon Rush"
        }
    },
    dragontail: {
        num: 525,
        metacategory: 0,
        names: {
            japanese: "ドラゴンテール",
            korean: "드래곤테일",
            french: "Draco-Queue",
            german: "Drachenrute",
            spanish: "Cola Dragón",
            italian: "Codadrago",
            english: "Dragon Tail"
        }
    },
    drainingkiss: {
        num: 577,
        metacategory: 8,
        names: {
            japanese: "ドレインキッス",
            korean: "드레인키스",
            french: "Vampibaiser",
            german: "Diebeskuss",
            spanish: "Beso Drenaje",
            italian: "Assorbibacio",
            english: "Draining Kiss"
        }
    },
    drainpunch: {
        num: 409,
        metacategory: 8,
        names: {
            japanese: "ドレインパンチ",
            korean: "드레인펀치",
            french: "Vampipoing",
            german: "Ableithieb",
            spanish: "Puño Drenaje",
            italian: "Assorbipugno",
            english: "Drain Punch"
        }
    },
    dreameater: {
        num: 138,
        metacategory: 8,
        names: {
            japanese: "ゆめくい",
            korean: "꿈먹기",
            french: "Dévorêve",
            german: "Traumfresser",
            spanish: "Come Sueños",
            italian: "Mangiasogni",
            english: "Dream Eater"
        }
    },
    drillpeck: {
        num: 65,
        metacategory: 0,
        names: {
            japanese: "ドリルくちばし",
            korean: "회전부리",
            french: "Bec Vrille",
            german: "Bohrschnabel",
            spanish: "Pico Taladro",
            italian: "Perforbecco",
            english: "Drill Peck"
        }
    },
    drillrun: {
        num: 529,
        metacategory: 0,
        names: {
            japanese: "ドリルライナー",
            korean: "드릴라이너",
            french: "Tunnelier",
            german: "Schlagbohrer",
            spanish: "Taladradora",
            italian: "Giravvita",
            english: "Drill Run"
        }
    },
    dualchop: {
        num: 530,
        metacategory: 0,
        names: {
            japanese: "ダブルチョップ",
            korean: "더블촙",
            french: "Double Baffe",
            german: "Doppelhieb",
            spanish: "Golpe Bis",
            italian: "Doppiocolpo",
            english: "Dual Chop"
        }
    },
    dynamicpunch: {
        num: 223,
        metacategory: 4,
        names: {
            japanese: "ばくれつパンチ",
            korean: "폭발펀치",
            french: "Dynamopoing",
            german: "Wuchtschlag",
            spanish: "Puño Dinámico",
            italian: "Dinamipugno",
            english: "Dynamic Punch"
        }
    },
    earthpower: {
        num: 414,
        metacategory: 6,
        names: {
            japanese: "だいちのちから",
            korean: "대지의힘",
            french: "Telluriforce",
            german: "Erdkräfte",
            spanish: "Tierra Viva",
            italian: "Geoforza",
            english: "Earth Power"
        }
    },
    earthquake: {
        num: 89,
        metacategory: 0,
        names: {
            japanese: "じしん",
            korean: "지진",
            french: "Séisme",
            german: "Erdbeben",
            spanish: "Terremoto",
            italian: "Terremoto",
            english: "Earthquake"
        }
    },
    echoedvoice: {
        num: 497,
        metacategory: 0,
        names: {
            japanese: "エコーボイス",
            korean: "에코보이스",
            french: "Écho",
            german: "Widerhall",
            spanish: "Eco Voz",
            italian: "Echeggiavoce",
            english: "Echoed Voice"
        }
    },
    eerieimpulse: {
        num: 598,
        metacategory: 2,
        names: {
            japanese: "かいでんぱ",
            korean: "괴전파",
            french: "Ondes Étranges",
            german: "Mystowellen",
            spanish: "Onda Anómala",
            italian: "Elettromistero",
            english: "Eerie Impulse"
        }
    },
    eggbomb: {
        num: 121,
        metacategory: 0,
        names: {
            japanese: "タマゴばくだん",
            korean: "알폭탄",
            french: "Bomb'Œuf",
            german: "Eierbombe",
            spanish: "Bomba Huevo",
            italian: "Uovobomba",
            english: "Egg Bomb"
        }
    },
    electricterrain: {
        num: 604,
        metacategory: 10,
        names: {
            japanese: "エレキフィールド",
            korean: "일렉트릭필드",
            french: "Champ Électrifié",
            german: "Elektrofeld",
            spanish: "Campo Eléctrico",
            italian: "Campo Elettrico",
            english: "Electric Terrain"
        }
    },
    electrify: {
        num: 582,
        metacategory: 13,
        names: {
            japanese: "そうでん",
            korean: "송전",
            french: "Électrisation",
            german: "Elektrifizierung",
            spanish: "Electrificación",
            italian: "Elettrocontagio",
            english: "Electrify"
        }
    },
    electroball: {
        num: 486,
        metacategory: 0,
        names: {
            japanese: "エレキボール",
            korean: "일렉트릭볼",
            french: "Boule Élek",
            german: "Elektroball",
            spanish: "Bola Voltio",
            italian: "Energisfera",
            english: "Electro Ball"
        }
    },
    electroweb: {
        num: 527,
        metacategory: 6,
        names: {
            japanese: "エレキネット",
            korean: "일렉트릭네트",
            french: "Toile Élek",
            german: "Elektronetz",
            spanish: "Electrotela",
            italian: "Elettrotela",
            english: "Electroweb"
        }
    },
    embargo: {
        num: 373,
        metacategory: 1,
        names: {
            japanese: "さしおさえ",
            korean: "금제",
            french: "Embargo",
            german: "Itemsperre",
            spanish: "Embargo",
            italian: "Divieto",
            english: "Embargo"
        }
    },
    ember: {
        num: 52,
        metacategory: 4,
        names: {
            japanese: "ひのこ",
            korean: "불꽃세례",
            french: "Flammèche",
            german: "Glut",
            spanish: "Ascuas",
            italian: "Braciere",
            english: "Ember"
        }
    },
    encore: {
        num: 227,
        metacategory: 13,
        names: {
            japanese: "アンコール",
            korean: "앵콜",
            french: "Encore",
            german: "Zugabe",
            spanish: "Otra Vez",
            italian: "Ripeti",
            english: "Encore"
        }
    },
    endeavor: {
        num: 283,
        metacategory: 0,
        names: {
            japanese: "がむしゃら",
            korean: "죽기살기",
            french: "Effort",
            german: "Notsituation",
            spanish: "Esfuerzo",
            italian: "Rimonta",
            english: "Endeavor"
        }
    },
    endure: {
        num: 203,
        metacategory: 13,
        names: {
            japanese: "こらえる",
            korean: "버티기",
            french: "Ténacité",
            german: "Ausdauer",
            spanish: "Aguante",
            italian: "Resistenza",
            english: "Endure"
        }
    },
    energyball: {
        num: 412,
        metacategory: 6,
        names: {
            japanese: "エナジーボール",
            korean: "에너지볼",
            french: "Éco-Sphère",
            german: "Energieball",
            spanish: "Energibola",
            italian: "Energipalla",
            english: "Energy Ball"
        }
    },
    entrainment: {
        num: 494,
        metacategory: 13,
        names: {
            japanese: "なかまづくり",
            korean: "동료만들기",
            french: "Ten-danse",
            german: "Zwango",
            spanish: "Danza Amiga",
            italian: "Saltamicizia",
            english: "Entrainment"
        }
    },
    eruption: {
        num: 284,
        metacategory: 0,
        names: {
            japanese: "ふんか",
            korean: "분화",
            french: "Éruption",
            german: "Eruption",
            spanish: "Estallido",
            italian: "Eruzione",
            english: "Eruption"
        }
    },
    explosion: {
        num: 153,
        metacategory: 0,
        names: {
            japanese: "だいばくはつ",
            korean: "대폭발",
            french: "Explosion",
            german: "Explosion",
            spanish: "Explosión",
            italian: "Esplosione",
            english: "Explosion"
        }
    },
    extrasensory: {
        num: 326,
        metacategory: 0,
        names: {
            japanese: "じんつうりき",
            korean: "신통력",
            french: "Extrasenseur",
            german: "Sondersensor",
            spanish: "Paranormal",
            italian: "Extrasenso",
            english: "Extrasensory"
        }
    },
    extremeevoboost: {
        num: 702
    },
    extremespeed: {
        num: 245,
        metacategory: 0,
        names: {
            japanese: "しんそく",
            korean: "신속",
            french: "Vitesse Extrême",
            german: "Turbotempo",
            spanish: "Veloc. Extrema",
            italian: "Extrarapido",
            english: "Extreme Speed"
        }
    },
    facade: {
        num: 263,
        metacategory: 0,
        names: {
            japanese: "からげんき",
            korean: "객기",
            french: "Façade",
            german: "Fassade",
            spanish: "Imagen",
            italian: "Facciata",
            english: "Facade"
        }
    },
    feintattack: {
        num: 185,
        metacategory: 0,
        names: {
            japanese: "だましうち",
            korean: "속여때리기",
            french: "Feinte",
            german: "Finte",
            spanish: "Finta",
            italian: "Finta",
            english: "Feint Attack"
        }
    },
    fairylock: {
        num: 587,
        metacategory: 10,
        names: {
            japanese: "フェアリーロック",
            korean: "페어리록",
            french: "Verrou Enchanté",
            german: "Feenschloss",
            spanish: "Cerrojo Feérico",
            italian: "Blocco Fatato",
            english: "Fairy Lock"
        }
    },
    fairywind: {
        num: 584,
        metacategory: 0,
        names: {
            japanese: "ようせいのかぜ",
            korean: "요정의바람",
            french: "Vent Féérique",
            german: "Feenbrise",
            spanish: "Viento Feérico",
            italian: "Vento di Fata",
            english: "Fairy Wind"
        }
    },
    fakeout: {
        num: 252,
        metacategory: 0,
        names: {
            japanese: "ねこだまし",
            korean: "속이다",
            french: "Bluff",
            german: "Mogelhieb",
            spanish: "Sorpresa",
            italian: "Bruciapelo",
            english: "Fake Out"
        }
    },
    faketears: {
        num: 313,
        metacategory: 2,
        names: {
            japanese: "うそなき",
            korean: "거짓울음",
            french: "Croco Larme",
            german: "Trugträne",
            spanish: "Llanto Falso",
            italian: "Falselacrime",
            english: "Fake Tears"
        }
    },
    falseswipe: {
        num: 206,
        metacategory: 0,
        names: {
            japanese: "みねうち",
            korean: "칼등치기",
            french: "Faux-Chage",
            german: "Trugschlag",
            spanish: "Falsotortazo",
            italian: "Falsofinale",
            english: "False Swipe"
        }
    },
    featherdance: {
        num: 297,
        metacategory: 2,
        names: {
            japanese: "フェザーダンス",
            korean: "깃털댄스",
            french: "Danse-Plume",
            german: "Daunenreigen",
            spanish: "Danza Pluma",
            italian: "Danzadipiume",
            english: "Feather Dance"
        }
    },
    feint: {
        num: 364,
        metacategory: 0,
        names: {
            japanese: "フェイント",
            korean: "페인트",
            french: "Ruse",
            german: "Offenlegung",
            spanish: "Amago",
            italian: "Fintoattacco",
            english: "Feint"
        }
    },
    fellstinger: {
        num: 565,
        metacategory: 0,
        names: {
            japanese: "とどめばり",
            korean: "마지막일침",
            french: "Dard Mortel",
            german: "Stachelfinale",
            spanish: "Aguijón Letal",
            italian: "Pungiglione",
            english: "Fell Stinger"
        }
    },
    fierydance: {
        num: 552,
        metacategory: 7,
        names: {
            japanese: "ほのおのまい",
            korean: "불꽃춤",
            french: "Danse du Feu",
            german: "Feuerreigen",
            spanish: "Danza Llama",
            italian: "Voldifuoco",
            english: "Fiery Dance"
        }
    },
    finalgambit: {
        num: 515,
        metacategory: 0,
        names: {
            japanese: "いのちがけ",
            korean: "목숨걸기",
            french: "Tout ou Rien",
            german: "Wagemut",
            spanish: "Sacrificio",
            italian: "Azzardo",
            english: "Final Gambit"
        }
    },
    fireblast: {
        num: 126,
        metacategory: 4,
        names: {
            japanese: "だいもんじ",
            korean: "불대문자",
            french: "Déflagration",
            german: "Feuersturm",
            spanish: "Llamarada",
            italian: "Fuocobomba",
            english: "Fire Blast"
        }
    },
    firefang: {
        num: 424,
        metacategory: 4,
        names: {
            japanese: "ほのおのキバ",
            korean: "불꽃엄니",
            french: "Crocs Feu",
            german: "Feuerzahn",
            spanish: "Colmillo Ígneo",
            italian: "Rogodenti",
            english: "Fire Fang"
        }
    },
    firelash: {
        num: 680
    },
    firepledge: {
        num: 519,
        metacategory: 0,
        names: {
            japanese: "ほのおのちかい",
            korean: "불꽃의맹세",
            french: "Aire de Feu",
            german: "Feuersäulen",
            spanish: "Voto Fuego",
            italian: "Fiammapatto",
            english: "Fire Pledge"
        }
    },
    firepunch: {
        num: 7,
        metacategory: 4,
        names: {
            japanese: "ほのおのパンチ",
            korean: "불꽃펀치",
            french: "Poing de Feu",
            german: "Feuerschlag",
            spanish: "Puño Fuego",
            italian: "Fuocopugno",
            english: "Fire Punch"
        }
    },
    firespin: {
        num: 83,
        metacategory: 4,
        names: {
            japanese: "ほのおのうず",
            korean: "회오리불꽃",
            french: "Danse Flamme",
            german: "Feuerwirbel",
            spanish: "Giro Fuego",
            italian: "Turbofuoco",
            english: "Fire Spin"
        }
    },
    firstimpression: {
        num: 660
    },
    fissure: {
        num: 90,
        metacategory: 9,
        names: {
            japanese: "じわれ",
            korean: "땅가르기",
            french: "Abîme",
            german: "Geofissur",
            spanish: "Fisura",
            italian: "Abisso",
            english: "Fissure"
        }
    },
    flail: {
        num: 175,
        metacategory: 0,
        names: {
            japanese: "じたばた",
            korean: "바둥바둥",
            french: "Fléau",
            german: "Dreschflegel",
            spanish: "Azote",
            italian: "Flagello",
            english: "Flail"
        }
    },
    flameburst: {
        num: 481,
        metacategory: 0,
        names: {
            japanese: "はじけるほのお",
            korean: "불꽃튀기기",
            french: "Rebondifeu",
            german: "Funkenflug",
            spanish: "Pirotecnia",
            italian: "Pirolancio",
            english: "Flame Burst"
        }
    },
    flamecharge: {
        num: 488,
        metacategory: 7,
        names: {
            japanese: "ニトロチャージ",
            korean: "니트로차지",
            french: "Nitrocharge",
            german: "Nitroladung",
            spanish: "Nitrocarga",
            italian: "Nitrocarica",
            english: "Flame Charge"
        }
    },
    flamewheel: {
        num: 172,
        metacategory: 4,
        names: {
            japanese: "かえんぐるま",
            korean: "화염자동차",
            french: "Roue de Feu",
            german: "Flammenrad",
            spanish: "Rueda Fuego",
            italian: "Ruotafuoco",
            english: "Flame Wheel"
        }
    },
    flamethrower: {
        num: 53,
        metacategory: 4,
        names: {
            japanese: "かえんほうしゃ",
            korean: "화염방사",
            french: "Lance-Flammes",
            german: "Flammenwurf",
            spanish: "Lanzallamas",
            italian: "Lanciafiamme",
            english: "Flamethrower"
        }
    },
    flareblitz: {
        num: 394,
        metacategory: 4,
        names: {
            japanese: "フレアドライブ",
            korean: "플레어드라이브",
            french: "Boutefeu",
            german: "Flammenblitz",
            spanish: "Envite Ígneo",
            italian: "Fuococarica",
            english: "Flare Blitz"
        }
    },
    flash: {
        num: 148,
        metacategory: 2,
        names: {
            japanese: "フラッシュ",
            korean: "플래시",
            french: "Flash",
            german: "Blitz",
            spanish: "Destello",
            italian: "Flash",
            english: "Flash"
        }
    },
    flashcannon: {
        num: 430,
        metacategory: 6,
        names: {
            japanese: "ラスターカノン",
            korean: "러스터캐논",
            french: "Luminocanon",
            german: "Lichtkanone",
            spanish: "Foco Resplandor",
            italian: "Cannonflash",
            english: "Flash Cannon"
        }
    },
    flatter: {
        num: 260,
        metacategory: 5,
        names: {
            japanese: "おだてる",
            korean: "부추기기",
            french: "Flatterie",
            german: "Schmeichler",
            spanish: "Camelo",
            italian: "Adulazione",
            english: "Flatter"
        }
    },
    fleurcannon: {
        num: 705
    },
    fling: {
        num: 374,
        metacategory: 0,
        names: {
            japanese: "なげつける",
            korean: "내던지기",
            french: "Dégommage",
            german: "Schleuder",
            spanish: "Lanzamiento",
            italian: "Lancio",
            english: "Fling"
        }
    },
    floralhealing: {
        num: 666
    },
    flowershield: {
        num: 579,
        metacategory: 13,
        names: {
            japanese: "フラワーガード",
            korean: "플라워가드",
            french: "Garde Florale",
            german: "Floraschutz",
            spanish: "Defensa Floral",
            italian: "Fiordifesa",
            english: "Flower Shield"
        }
    },
    fly: {
        num: 19,
        metacategory: 0,
        names: {
            japanese: "そらをとぶ",
            korean: "공중날기",
            french: "Vol",
            german: "Fliegen",
            spanish: "Vuelo",
            italian: "Volo",
            english: "Fly"
        }
    },
    flyingpress: {
        num: 560,
        metacategory: 0,
        names: {
            japanese: "フライングプレス",
            korean: "플라잉프레스",
            french: "Flying Press",
            german: "Flying Press",
            spanish: "Plancha Voladora",
            italian: "Schiacciatuffo",
            english: "Flying Press"
        }
    },
    focusblast: {
        num: 411,
        metacategory: 6,
        names: {
            japanese: "きあいだま",
            korean: "기합구슬",
            french: "Exploforce",
            german: "Fokusstoß",
            spanish: "Onda Certera",
            italian: "Focalcolpo",
            english: "Focus Blast"
        }
    },
    focusenergy: {
        num: 116,
        metacategory: 13,
        names: {
            japanese: "きあいだめ",
            korean: "기충전",
            french: "Puissance",
            german: "Energiefokus",
            spanish: "Foco Energía",
            italian: "Focalenergia",
            english: "Focus Energy"
        }
    },
    focuspunch: {
        num: 264,
        metacategory: 0,
        names: {
            japanese: "きあいパンチ",
            korean: "힘껏펀치",
            french: "Mitra-Poing",
            german: "Power-Punch",
            spanish: "Puño Certero",
            italian: "Centripugno",
            english: "Focus Punch"
        }
    },
    followme: {
        num: 266,
        metacategory: 13,
        names: {
            japanese: "このゆびとまれ",
            korean: "날따름",
            french: "Par Ici",
            german: "Spotlight",
            spanish: "Señuelo",
            italian: "Sonoqui",
            english: "Follow Me"
        }
    },
    forcepalm: {
        num: 395,
        metacategory: 4,
        names: {
            japanese: "はっけい",
            korean: "발경",
            french: "Forte-Paume",
            german: "Kraftwelle",
            spanish: "Palmeo",
            italian: "Palmoforza",
            english: "Force Palm"
        }
    },
    foresight: {
        num: 193,
        metacategory: 1,
        names: {
            japanese: "みやぶる",
            korean: "꿰뚫어보기",
            french: "Clairvoyance",
            german: "Gesichte",
            spanish: "Profecía",
            italian: "Preveggenza",
            english: "Foresight"
        }
    },
    forestscurse: {
        num: 571,
        metacategory: 13,
        names: {
            japanese: "もりののろい",
            korean: "숲의저주",
            french: "Maléfice Sylvain",
            german: "Waldesfluch",
            spanish: "Condena Silvana",
            italian: "Boscomalocchio",
            english: "Forest's Curse"
        }
    },
    foulplay: {
        num: 492,
        metacategory: 0,
        names: {
            japanese: "イカサマ",
            korean: "속임수",
            french: "Tricherie",
            german: "Schmarotzer",
            spanish: "Juego Sucio",
            italian: "Ripicca",
            english: "Foul Play"
        }
    },
    freezedry: {
        num: 573,
        metacategory: 4,
        names: {
            japanese: "フリーズドライ",
            korean: "프리즈드라이",
            french: "Lyophilisation",
            german: "Gefriertrockner",
            spanish: "Liofilización",
            italian: "Liofilizzazione",
            english: "Freeze-Dry"
        }
    },
    freezeshock: {
        num: 553,
        metacategory: 4,
        names: {
            japanese: "フリーズボルト",
            korean: "프리즈볼트",
            french: "Éclair Gelé",
            german: "Frostvolt",
            spanish: "Rayo Gélido",
            italian: "Elettrogelo",
            english: "Freeze Shock"
        }
    },
    frenzyplant: {
        num: 338,
        metacategory: 0,
        names: {
            japanese: "ハードプラント",
            korean: "하드플랜트",
            french: "Végé-Attak",
            german: "Fauna-Statue",
            spanish: "Planta Feroz",
            italian: "Radicalbero",
            english: "Frenzy Plant"
        }
    },
    frostbreath: {
        num: 524,
        metacategory: 0,
        names: {
            japanese: "こおりのいぶき",
            korean: "얼음숨결",
            french: "Souffle Glacé",
            german: "Eisesodem",
            spanish: "Vaho Gélido",
            italian: "Alitogelido",
            english: "Frost Breath"
        }
    },
    frustration: {
        num: 218,
        metacategory: 0,
        names: {
            japanese: "やつあたり",
            korean: "화풀이",
            french: "Frustration",
            german: "Frustration",
            spanish: "Frustración",
            italian: "Frustrazione",
            english: "Frustration"
        }
    },
    furyattack: {
        num: 31,
        metacategory: 0,
        names: {
            japanese: "みだれづき",
            korean: "마구찌르기",
            french: "Furie",
            german: "Furienschlag",
            spanish: "Ataque Furia",
            italian: "Furia",
            english: "Fury Attack"
        }
    },
    furycutter: {
        num: 210,
        metacategory: 0,
        names: {
            japanese: "れんぞくぎり",
            korean: "연속자르기",
            french: "Taillade",
            german: "Zornklinge",
            spanish: "Corte Furia",
            italian: "Tagliofuria",
            english: "Fury Cutter"
        }
    },
    furyswipes: {
        num: 154,
        metacategory: 0,
        names: {
            japanese: "みだれひっかき",
            korean: "마구할퀴기",
            french: "Combo-Griffe",
            german: "Kratzfurie",
            spanish: "Golpes Furia",
            italian: "Sfuriate",
            english: "Fury Swipes"
        }
    },
    fusionbolt: {
        num: 559,
        metacategory: 0,
        names: {
            japanese: "クロスサンダー",
            korean: "크로스썬더",
            french: "Éclair Croix",
            german: "Kreuzdonner",
            spanish: "Rayo Fusión",
            italian: "Incrotuono",
            english: "Fusion Bolt"
        }
    },
    fusionflare: {
        num: 558,
        metacategory: 0,
        names: {
            japanese: "クロスフレイム",
            korean: "크로스플레임",
            french: "Flamme Croix",
            german: "Kreuzflamme",
            spanish: "Llama Fusión",
            italian: "Incrofiamma",
            english: "Fusion Flare"
        }
    },
    futuresight: {
        num: 248,
        metacategory: 13,
        names: {
            japanese: "みらいよち",
            korean: "미래예지",
            french: "Prescience",
            german: "Seher",
            spanish: "Premonición",
            italian: "Divinazione",
            english: "Future Sight"
        }
    },
    gastroacid: {
        num: 380,
        metacategory: 13,
        names: {
            japanese: "いえき",
            korean: "위액",
            french: "Suc Digestif",
            german: "Magensäfte",
            spanish: "Bilis",
            italian: "Gastroacido",
            english: "Gastro Acid"
        }
    },
    geargrind: {
        num: 544,
        metacategory: 0,
        names: {
            japanese: "ギアソーサー",
            korean: "기어소서",
            french: "Lancécrou",
            german: "Klikkdiskus",
            spanish: "Rueda Doble",
            italian: "Ingracolpo",
            english: "Gear Grind"
        }
    },
    gearup: {
        num: 674
    },
    genesissupernova: {
        num: 703
    },
    geomancy: {
        num: 601,
        metacategory: 2,
        names: {
            japanese: "ジオコントロール",
            korean: "지오컨트롤",
            french: "Géo-Contrôle",
            german: "Geokontrolle",
            spanish: "Geocontrol",
            italian: "Geocontrollo",
            english: "Geomancy"
        }
    },
    gigadrain: {
        num: 202,
        metacategory: 8,
        names: {
            japanese: "ギガドレイン",
            korean: "기가드레인",
            french: "Giga-Sangsue",
            german: "Gigasauger",
            spanish: "Gigadrenado",
            italian: "Gigassorbimento",
            english: "Giga Drain"
        }
    },
    gigaimpact: {
        num: 416,
        metacategory: 0,
        names: {
            japanese: "ギガインパクト",
            korean: "기가임팩트",
            french: "Giga Impact",
            german: "Gigastoß",
            spanish: "Giga Impacto",
            italian: "Gigaimpatto",
            english: "Giga Impact"
        }
    },
    gigavolthavoc: {
        num: 646
    },
    glaciate: {
        num: 549,
        metacategory: 6,
        names: {
            japanese: "こごえるせかい",
            korean: "얼다세계",
            french: "Ère Glaciaire",
            german: "Eiszeit",
            spanish: "Mundo Gélido",
            italian: "Gelamondo",
            english: "Glaciate"
        }
    },
    glare: {
        num: 137,
        metacategory: 1,
        names: {
            japanese: "へびにらみ",
            korean: "뱀눈초리",
            french: "Regard Médusant",
            german: "Giftblick",
            spanish: "Deslumbrar",
            italian: "Sguardo Feroce",
            english: "Glare"
        }
    },
    grassknot: {
        num: 447,
        metacategory: 0,
        names: {
            japanese: "くさむすび",
            korean: "풀묶기",
            french: "Nœud Herbe",
            german: "Strauchler",
            spanish: "Hierba Lazo",
            italian: "Laccioerboso",
            english: "Grass Knot"
        }
    },
    grasspledge: {
        num: 520,
        metacategory: 0,
        names: {
            japanese: "くさのちかい",
            korean: "풀의맹세",
            french: "Aire d'Herbe",
            german: "Pflanzsäulen",
            spanish: "Voto Planta",
            italian: "Erbapatto",
            english: "Grass Pledge"
        }
    },
    grasswhistle: {
        num: 320,
        metacategory: 1,
        names: {
            japanese: "くさぶえ",
            korean: "풀피리",
            french: "Siffl'Herbe",
            german: "Grasflöte",
            spanish: "Silbato",
            italian: "Meloderba",
            english: "Grass Whistle"
        }
    },
    grassyterrain: {
        num: 580,
        metacategory: 10,
        names: {
            japanese: "グラスフィールド",
            korean: "그래스필드",
            french: "Champ Herbu",
            german: "Grasfeld",
            spanish: "Campo de Hierba",
            italian: "Campo Erboso",
            english: "Grassy Terrain"
        }
    },
    gravity: {
        num: 356,
        metacategory: 10,
        names: {
            japanese: "じゅうりょく",
            korean: "중력",
            french: "Gravité",
            german: "Erdanziehung",
            spanish: "Gravedad",
            italian: "Gravità",
            english: "Gravity"
        }
    },
    growl: {
        num: 45,
        metacategory: 2,
        names: {
            japanese: "なきごえ",
            korean: "울음소리",
            french: "Rugissement",
            german: "Heuler",
            spanish: "Gruñido",
            italian: "Ruggito",
            english: "Growl"
        }
    },
    growth: {
        num: 74,
        metacategory: 2,
        names: {
            japanese: "せいちょう",
            korean: "성장",
            french: "Croissance",
            german: "Wachstum",
            spanish: "Desarrollo",
            italian: "Crescita",
            english: "Growth"
        }
    },
    grudge: {
        num: 288,
        metacategory: 13,
        names: {
            japanese: "おんねん",
            korean: "원념",
            french: "Rancune",
            german: "Nachspiel",
            spanish: "Rabia",
            italian: "Rancore",
            english: "Grudge"
        }
    },
    guardsplit: {
        num: 470,
        metacategory: 13,
        names: {
            japanese: "ガードシェア",
            korean: "가드셰어",
            french: "Partage Garde",
            german: "Schutzteiler",
            spanish: "Isoguardia",
            italian: "Paridifesa",
            english: "Guard Split"
        }
    },
    guardswap: {
        num: 385,
        metacategory: 13,
        names: {
            japanese: "ガードスワップ",
            korean: "가드스웹",
            french: "Permugarde",
            german: "Schutztausch",
            spanish: "Cambia Defensa",
            italian: "Barattoscudo",
            english: "Guard Swap"
        }
    },
    guardianofalola: {
        num: 698
    },
    guillotine: {
        num: 12,
        metacategory: 9,
        names: {
            japanese: "ハサミギロチン",
            korean: "가위자르기",
            french: "Guillotine",
            german: "Guillotine",
            spanish: "Guillotina",
            italian: "Ghigliottina",
            english: "Guillotine"
        }
    },
    gunkshot: {
        num: 441,
        metacategory: 4,
        names: {
            japanese: "ダストシュート",
            korean: "더스트슈트",
            french: "Détricanon",
            german: "Mülltreffer",
            spanish: "Lanza Mugre",
            italian: "Sporcolancio",
            english: "Gunk Shot"
        }
    },
    gust: {
        num: 16,
        metacategory: 0,
        names: {
            japanese: "かぜおこし",
            korean: "바람일으키기",
            french: "Tornade",
            german: "Windstoß",
            spanish: "Tornado",
            italian: "Raffica",
            english: "Gust"
        }
    },
    gyroball: {
        num: 360,
        metacategory: 0,
        names: {
            japanese: "ジャイロボール",
            korean: "자이로볼",
            french: "Gyroballe",
            german: "Gyroball",
            spanish: "Giro Bola",
            italian: "Vortexpalla",
            english: "Gyro Ball"
        }
    },
    hail: {
        num: 258,
        metacategory: 10,
        names: {
            japanese: "あられ",
            korean: "싸라기눈",
            french: "Grêle",
            german: "Hagelsturm",
            spanish: "Granizo",
            italian: "Grandine",
            english: "Hail"
        }
    },
    hammerarm: {
        num: 359,
        metacategory: 7,
        names: {
            japanese: "アームハンマー",
            korean: "암해머",
            french: "Marto-Poing",
            german: "Hammerarm",
            spanish: "Machada",
            italian: "Martelpugno",
            english: "Hammer Arm"
        }
    },
    happyhour: {
        num: 603,
        metacategory: 13,
        names: {
            japanese: "ハッピータイム",
            korean: "해피타임",
            french: "Étrennes",
            german: "Goldene Zeiten",
            spanish: "Paga Extra",
            italian: "Cuccagna",
            english: "Happy Hour"
        }
    },
    harden: {
        num: 106,
        metacategory: 2,
        names: {
            japanese: "かたくなる",
            korean: "단단해지기",
            french: "Armure",
            german: "Härtner",
            spanish: "Fortaleza",
            italian: "Rafforzatore",
            english: "Harden"
        }
    },
    haze: {
        num: 114,
        metacategory: 10,
        names: {
            japanese: "くろいきり",
            korean: "흑안개",
            french: "Buée Noire",
            german: "Dunkelnebel",
            spanish: "Niebla",
            italian: "Nube",
            english: "Haze"
        }
    },
    headcharge: {
        num: 543,
        metacategory: 0,
        names: {
            japanese: "アフロブレイク",
            korean: "아프로브레이크",
            french: "Peignée",
            german: "Steinschädel",
            spanish: "Ariete",
            italian: "Ricciolata",
            english: "Head Charge"
        }
    },
    headsmash: {
        num: 457,
        metacategory: 0,
        names: {
            japanese: "もろはのずつき",
            korean: "양날박치기",
            french: "Fracass'Tête",
            german: "Kopfstoß",
            spanish: "Testarazo",
            italian: "Zuccata",
            english: "Head Smash"
        }
    },
    headbutt: {
        num: 29,
        metacategory: 0,
        names: {
            japanese: "ずつき",
            korean: "박치기",
            french: "Coup d'Boule",
            german: "Kopfnuss",
            spanish: "Golpe Cabeza",
            italian: "Bottintesta",
            english: "Headbutt"
        }
    },
    healbell: {
        num: 215,
        metacategory: 13,
        names: {
            japanese: "いやしのすず",
            korean: "치료방울",
            french: "Glas de Soin",
            german: "Vitalglocke",
            spanish: "Campana Cura",
            italian: "Rintoccasana",
            english: "Heal Bell"
        }
    },
    healblock: {
        num: 377,
        metacategory: 1,
        names: {
            japanese: "かいふくふうじ",
            korean: "회복봉인",
            french: "Anti-Soin",
            german: "Heilblockade",
            spanish: "Anticura",
            italian: "Anticura",
            english: "Heal Block"
        }
    },
    healorder: {
        num: 456,
        metacategory: 3,
        names: {
            japanese: "かいふくしれい",
            korean: "회복지령",
            french: "Appel Soins",
            german: "Heilbefehl",
            spanish: "Auxilio",
            italian: "Comandocura",
            english: "Heal Order"
        }
    },
    healpulse: {
        num: 505,
        metacategory: 3,
        names: {
            japanese: "いやしのはどう",
            korean: "치유파동",
            french: "Vibra Soin",
            german: "Heilwoge",
            spanish: "Pulso Cura",
            italian: "Ondasana",
            english: "Heal Pulse"
        }
    },
    healingwish: {
        num: 361,
        metacategory: 13,
        names: {
            japanese: "いやしのねがい",
            korean: "치유소원",
            french: "Vœu Soin",
            german: "Heilopfer",
            spanish: "Deseo Cura",
            italian: "Curardore",
            english: "Healing Wish"
        }
    },
    heartstamp: {
        num: 531,
        metacategory: 0,
        names: {
            japanese: "ハートスタンプ",
            korean: "하트스탬프",
            french: "Crève-Cœur",
            german: "Herzstempel",
            spanish: "Arrumaco",
            italian: "Cuorestampo",
            english: "Heart Stamp"
        }
    },
    heartswap: {
        num: 391,
        metacategory: 13,
        names: {
            japanese: "ハートスワップ",
            korean: "하트스웹",
            french: "Permucœur",
            german: "Statustausch",
            spanish: "Cambia Almas",
            italian: "Cuorbaratto",
            english: "Heart Swap"
        }
    },
    heatcrash: {
        num: 535,
        metacategory: 0,
        names: {
            japanese: "ヒートスタンプ",
            korean: "히트스탬프",
            french: "Tacle Feu",
            german: "Brandstempel",
            spanish: "Golpe Calor",
            italian: "Marchiafuoco",
            english: "Heat Crash"
        }
    },
    heatwave: {
        num: 257,
        metacategory: 4,
        names: {
            japanese: "ねっぷう",
            korean: "열풍",
            french: "Canicule",
            german: "Hitzewelle",
            spanish: "Onda Ígnea",
            italian: "Ondacalda",
            english: "Heat Wave"
        }
    },
    heavyslam: {
        num: 484,
        metacategory: 0,
        names: {
            japanese: "ヘビーボンバー",
            korean: "헤비봄버",
            french: "Tacle Lourd",
            german: "Rammboss",
            spanish: "Cuerpo Pesado",
            italian: "Pesobomba",
            english: "Heavy Slam"
        }
    },
    helpinghand: {
        num: 270,
        metacategory: 13,
        names: {
            japanese: "てだすけ",
            korean: "도우미",
            french: "Coup d'Main",
            german: "Rechte Hand",
            spanish: "Refuerzo",
            italian: "Altruismo",
            english: "Helping Hand"
        }
    },
    hex: {
        num: 506,
        metacategory: 0,
        names: {
            japanese: "たたりめ",
            korean: "병상첨병",
            french: "Châtiment",
            german: "Bürde",
            spanish: "Infortunio",
            italian: "Sciagura",
            english: "Hex"
        }
    },
    hiddenpower: {
        num: 237,
        metacategory: 0,
        names: {
            japanese: "めざめるパワー",
            korean: "잠재파워",
            french: "Puissance Cachée",
            german: "Kraftreserve",
            spanish: "Poder Oculto",
            italian: "Introforza",
            english: "Hidden Power"
        }
    },
    hiddenpowerbug: {},
    hiddenpowerdark: {},
    hiddenpowerdragon: {},
    hiddenpowerelectric: {},
    hiddenpowerfighting: {},
    hiddenpowerfire: {},
    hiddenpowerflying: {},
    hiddenpowerghost: {},
    hiddenpowergrass: {},
    hiddenpowerground: {},
    hiddenpowerice: {},
    hiddenpowerpoison: {},
    hiddenpowerpsychic: {},
    hiddenpowerrock: {},
    hiddenpowersteel: {},
    hiddenpowerwater: {},
    highhorsepower: {
        num: 667
    },
    highjumpkick: {
        num: 136,
        metacategory: 0,
        names: {
            japanese: "とびひざげり",
            korean: "무릎차기",
            french: "Pied Voltige",
            german: "Turmkick",
            spanish: "Pat. Salto Alta",
            italian: "Calcinvolo",
            english: "High Jump Kick"
        }
    },
    holdback: {
        num: 610,
        metacategory: 0,
        names: {
            japanese: "てかげん",
            korean: "적당히손봐주기",
            french: "Retenue",
            german: "Zurückhaltung",
            spanish: "Clemencia",
            italian: "Riguardo",
            english: "Hold Back"
        }
    },
    holdhands: {
        num: 615,
        metacategory: 0,
        names: {
            japanese: "サウザンウェーブ",
            korean: "사우전드웨이브",
            french: "Myria-Vagues",
            german: "Tausend Wellen",
            spanish: "Mil Temblores",
            italian: "Mille Onde",
            english: "Thousand Waves"
        }
    },
    honeclaws: {
        num: 468,
        metacategory: 2,
        names: {
            japanese: "つめとぎ",
            korean: "손톱갈기",
            french: "Aiguisage",
            german: "Klauenwetzer",
            spanish: "Afilagarras",
            italian: "Unghiaguzze",
            english: "Hone Claws"
        }
    },
    hornattack: {
        num: 30,
        metacategory: 0,
        names: {
            japanese: "つのでつく",
            korean: "뿔찌르기",
            french: "Koud'Korne",
            german: "Hornattacke",
            spanish: "Cornada",
            italian: "Incornata",
            english: "Horn Attack"
        }
    },
    horndrill: {
        num: 32,
        metacategory: 9,
        names: {
            japanese: "つのドリル",
            korean: "뿔드릴",
            french: "Empal'Korne",
            german: "Hornbohrer",
            spanish: "Perforador",
            italian: "Perforcorno",
            english: "Horn Drill"
        }
    },
    hornleech: {
        num: 532,
        metacategory: 8,
        names: {
            japanese: "ウッドホーン",
            korean: "우드호른",
            french: "Encornebois",
            german: "Holzgeweih",
            spanish: "Asta Drenaje",
            italian: "Legnicorno",
            english: "Horn Leech"
        }
    },
    howl: {
        num: 336,
        metacategory: 2,
        names: {
            japanese: "とおぼえ",
            korean: "멀리짖음",
            french: "Grondement",
            german: "Jauler",
            spanish: "Aullido",
            italian: "Gridodilotta",
            english: "Howl"
        }
    },
    hurricane: {
        num: 542,
        metacategory: 4,
        names: {
            japanese: "ぼうふう",
            korean: "폭풍",
            french: "Vent Violent",
            german: "Orkan",
            spanish: "Vendaval",
            italian: "Tifone",
            english: "Hurricane"
        }
    },
    hydrocannon: {
        num: 308,
        metacategory: 0,
        names: {
            japanese: "ハイドロカノン",
            korean: "하이드로캐논",
            french: "Hydroblast",
            german: "Aquahaubitze",
            spanish: "Hidrocañón",
            italian: "Idrocannone",
            english: "Hydro Cannon"
        }
    },
    hydropump: {
        num: 56,
        metacategory: 0,
        names: {
            japanese: "ハイドロポンプ",
            korean: "하이드로펌프",
            french: "Hydrocanon",
            german: "Hydropumpe",
            spanish: "Hidrobomba",
            italian: "Idropompa",
            english: "Hydro Pump"
        }
    },
    hydrovortex: {
        num: 642
    },
    hyperbeam: {
        num: 63,
        metacategory: 0,
        names: {
            japanese: "はかいこうせん",
            korean: "파괴광선",
            french: "Ultralaser",
            german: "Hyperstrahl",
            spanish: "Hiperrayo",
            italian: "Iper Raggio",
            english: "Hyper Beam"
        }
    },
    hyperfang: {
        num: 158,
        metacategory: 0,
        names: {
            japanese: "ひっさつまえば",
            korean: "필살앞니",
            french: "Croc de Mort",
            german: "Hyperzahn",
            spanish: "Hipercolmillo",
            italian: "Iperzanna",
            english: "Hyper Fang"
        }
    },
    hyperspacefury: {
        num: 621,
        names: {
            japanese: "いじげんラッシュ",
            french: "Furie Dimension",
            german: "Dimensionswahn",
            spanish: "Cerco Dimensión",
            italian: "Urtodimensionale",
            english: "Hyperspace Fury"
        }
    },
    hyperspacehole: {
        num: 593,
        metacategory: 0,
        names: {
            japanese: "いじげんホール",
            korean: "다른차원홀",
            french: "TrouDimensionnel",
            german: "Dimensionsloch",
            spanish: "Paso Dimensional",
            italian: "Forodimensionale",
            english: "Hyperspace Hole"
        }
    },
    hypervoice: {
        num: 304,
        metacategory: 0,
        names: {
            japanese: "ハイパーボイス",
            korean: "하이퍼보이스",
            french: "Mégaphone",
            german: "Schallwelle",
            spanish: "Vozarrón",
            italian: "Granvoce",
            english: "Hyper Voice"
        }
    },
    hypnosis: {
        num: 95,
        metacategory: 1,
        names: {
            japanese: "さいみんじゅつ",
            korean: "최면술",
            french: "Hypnose",
            german: "Hypnose",
            spanish: "Hipnosis",
            italian: "Ipnosi",
            english: "Hypnosis"
        }
    },
    iceball: {
        num: 301,
        metacategory: 0,
        names: {
            japanese: "アイスボール",
            korean: "아이스볼",
            french: "Ball'Glace",
            german: "Frostbeule",
            spanish: "Bola Hielo",
            italian: "Palla Gelo",
            english: "Ice Ball"
        }
    },
    icebeam: {
        num: 58,
        metacategory: 4,
        names: {
            japanese: "れいとうビーム",
            korean: "냉동빔",
            french: "Laser Glace",
            german: "Eisstrahl",
            spanish: "Rayo Hielo",
            italian: "Geloraggio",
            english: "Ice Beam"
        }
    },
    iceburn: {
        num: 554,
        metacategory: 4,
        names: {
            japanese: "コールドフレア",
            korean: "콜드플레어",
            french: "Feu Glacé",
            german: "Frosthauch",
            spanish: "Llama Gélida",
            italian: "Vampagelida",
            english: "Ice Burn"
        }
    },
    icefang: {
        num: 423,
        metacategory: 4,
        names: {
            japanese: "こおりのキバ",
            korean: "얼음엄니",
            french: "Crocs Givre",
            german: "Eiszahn",
            spanish: "Colmillo Hielo",
            italian: "Gelodenti",
            english: "Ice Fang"
        }
    },
    icehammer: {
        num: 665
    },
    icepunch: {
        num: 8,
        metacategory: 4,
        names: {
            japanese: "れいとうパンチ",
            korean: "냉동펀치",
            french: "Poing-Glace",
            german: "Eishieb",
            spanish: "Puño Hielo",
            italian: "Gelopugno",
            english: "Ice Punch"
        }
    },
    iceshard: {
        num: 420,
        metacategory: 0,
        names: {
            japanese: "こおりのつぶて",
            korean: "얼음뭉치",
            french: "Éclats Glace",
            german: "Eissplitter",
            spanish: "Canto Helado",
            italian: "Geloscheggia",
            english: "Ice Shard"
        }
    },
    iciclecrash: {
        num: 556,
        metacategory: 0,
        names: {
            japanese: "つららおとし",
            korean: "고드름떨구기",
            french: "Chute Glace",
            german: "Eiszapfhagel",
            spanish: "Chuzos",
            italian: "Scagliagelo",
            english: "Icicle Crash"
        }
    },
    iciclespear: {
        num: 333,
        metacategory: 0,
        names: {
            japanese: "つららばり",
            korean: "고드름침",
            french: "Stalagtite",
            german: "Eisspeer",
            spanish: "Carámbano",
            italian: "Gelolancia",
            english: "Icicle Spear"
        }
    },
    icywind: {
        num: 196,
        metacategory: 6,
        names: {
            japanese: "こごえるかぜ",
            korean: "얼다바람",
            french: "Vent Glace",
            german: "Eissturm",
            spanish: "Viento Hielo",
            italian: "Ventogelato",
            english: "Icy Wind"
        }
    },
    imprison: {
        num: 286,
        metacategory: 13,
        names: {
            japanese: "ふういん",
            korean: "봉인",
            french: "Possessif",
            german: "Begrenzer",
            spanish: "Cerca",
            italian: "Esclusiva",
            english: "Imprison"
        }
    },
    incinerate: {
        num: 510,
        metacategory: 0,
        names: {
            japanese: "やきつくす",
            korean: "불태우기",
            french: "Calcination",
            german: "Einäschern",
            spanish: "Calcinación",
            italian: "Bruciatutto",
            english: "Incinerate"
        }
    },
    inferno: {
        num: 517,
        metacategory: 4,
        names: {
            japanese: "れんごく",
            korean: "연옥",
            french: "Feu d'Enfer",
            german: "Inferno",
            spanish: "Infierno",
            italian: "Marchiatura",
            english: "Inferno"
        }
    },
    infernooverdrive: {
        num: 640
    },
    infestation: {
        num: 611,
        metacategory: 4,
        names: {
            japanese: "まとわりつく",
            korean: "엉겨붙기",
            french: "Harcèlement",
            german: "Plage",
            spanish: "Acoso",
            italian: "Assillo",
            english: "Infestation"
        }
    },
    ingrain: {
        num: 275,
        metacategory: 1,
        names: {
            japanese: "ねをはる",
            korean: "뿌리박기",
            french: "Racines",
            german: "Verwurzler",
            spanish: "Arraigo",
            italian: "Radicamento",
            english: "Ingrain"
        }
    },
    instruct: {
        num: 689
    },
    iondeluge: {
        num: 569,
        metacategory: 10,
        names: {
            japanese: "プラズマシャワー",
            korean: "플라스마샤워",
            french: "DélugePlasmique",
            german: "Plasmaschauer",
            spanish: "Cortina Plasma",
            italian: "Pioggiaplasma",
            english: "Ion Deluge"
        }
    },
    irondefense: {
        num: 334,
        metacategory: 2,
        names: {
            japanese: "てっぺき",
            korean: "철벽",
            french: "Mur de Fer",
            german: "Eisenabwehr",
            spanish: "Defensa Férrea",
            italian: "Ferroscudo",
            english: "Iron Defense"
        }
    },
    ironhead: {
        num: 442,
        metacategory: 0,
        names: {
            japanese: "アイアンヘッド",
            korean: "아이언헤드",
            french: "Tête de Fer",
            german: "Eisenschädel",
            spanish: "Cabeza de Hierro",
            italian: "Metaltestata",
            english: "Iron Head"
        }
    },
    irontail: {
        num: 231,
        metacategory: 6,
        names: {
            japanese: "アイアンテール",
            korean: "아이언테일",
            french: "Queue de Fer",
            german: "Eisenschweif",
            spanish: "Cola Férrea",
            italian: "Codacciaio",
            english: "Iron Tail"
        }
    },
    judgment: {
        num: 449,
        metacategory: 0,
        names: {
            japanese: "さばきのつぶて",
            korean: "심판의뭉치",
            french: "Jugement",
            german: "Urteilskraft",
            spanish: "Sentencia",
            italian: "Giudizio",
            english: "Judgment"
        }
    },
    jumpkick: {
        num: 26,
        metacategory: 0,
        names: {
            japanese: "とびげり",
            korean: "점프킥",
            french: "Pied Sauté",
            german: "Sprungkick",
            spanish: "Patada Salto",
            italian: "Calciosalto",
            english: "Jump Kick"
        }
    },
    karatechop: {
        num: 2,
        metacategory: 0,
        names: {
            japanese: "からてチョップ",
            korean: "태권당수",
            french: "Poing-Karaté",
            german: "Karateschlag",
            spanish: "Golpe Kárate",
            italian: "Colpokarate",
            english: "Karate Chop"
        }
    },
    kinesis: {
        num: 134,
        metacategory: 2,
        names: {
            japanese: "スプーンまげ",
            korean: "숟가락휘기",
            french: "Télékinésie",
            german: "Psykraft",
            spanish: "Kinético",
            italian: "Cinèsi",
            english: "Kinesis"
        }
    },
    kingsshield: {
        num: 588,
        metacategory: 13,
        names: {
            japanese: "キングシールド",
            korean: "킹실드",
            french: "Bouclier Royal",
            german: "Königsschild",
            spanish: "Escudo Real",
            italian: "Scudo Reale",
            english: "King's Shield"
        }
    },
    knockoff: {
        num: 282,
        metacategory: 0,
        names: {
            japanese: "はたきおとす",
            korean: "탁쳐서떨구기",
            french: "Sabotage",
            german: "Abschlag",
            spanish: "Desarme",
            italian: "Privazione",
            english: "Knock Off"
        }
    },
    landswrath: {
        num: 616,
        metacategory: 0,
        names: {
            japanese: "グランドフォース",
            korean: "그라운드포스",
            french: "Force Chtonienne",
            german: "Bodengewalt",
            spanish: "Fuerza Telúrica",
            italian: "Forza Tellurica",
            english: "Land's Wrath"
        }
    },
    laserfocus: {
        num: 673
    },
    lastresort: {
        num: 387,
        metacategory: 0,
        names: {
            japanese: "とっておき",
            korean: "뒀다쓰기",
            french: "Dernier Recours",
            german: "Zuflucht",
            spanish: "Última Baza",
            italian: "Ultimascelta",
            english: "Last Resort"
        }
    },
    lavaplume: {
        num: 436,
        metacategory: 4,
        names: {
            japanese: "ふんえん",
            korean: "분연",
            french: "Ébullilave",
            german: "Flammensturm",
            spanish: "Humareda",
            italian: "Lavasbuffo",
            english: "Lava Plume"
        }
    },
    leafblade: {
        num: 348,
        metacategory: 0,
        names: {
            japanese: "リーフブレード",
            korean: "리프블레이드",
            french: "Lame-Feuille",
            german: "Laubklinge",
            spanish: "Hoja Aguda",
            italian: "Fendifoglia",
            english: "Leaf Blade"
        }
    },
    leafstorm: {
        num: 437,
        metacategory: 7,
        names: {
            japanese: "リーフストーム",
            korean: "리프스톰",
            french: "Tempête Verte",
            german: "Blättersturm",
            spanish: "Lluevehojas",
            italian: "Verdebufera",
            english: "Leaf Storm"
        }
    },
    leaftornado: {
        num: 536,
        metacategory: 6,
        names: {
            japanese: "グラスミキサー",
            korean: "그래스믹서",
            french: "Phytomixeur",
            german: "Grasmixer",
            spanish: "Ciclón de Hojas",
            italian: "Vorticerba",
            english: "Leaf Tornado"
        }
    },
    leafage: {
        num: 670
    },
    leechlife: {
        num: 141,
        metacategory: 8,
        names: {
            japanese: "きゅうけつ",
            korean: "흡혈",
            french: "Vampirisme",
            german: "Blutsauger",
            spanish: "Chupavidas",
            italian: "Sanguisuga",
            english: "Leech Life"
        }
    },
    leechseed: {
        num: 73,
        metacategory: 1,
        names: {
            japanese: "やどりぎのタネ",
            korean: "씨뿌리기",
            french: "Vampigraine",
            german: "Egelsamen",
            spanish: "Drenadoras",
            italian: "Parassiseme",
            english: "Leech Seed"
        }
    },
    leer: {
        num: 43,
        metacategory: 2,
        names: {
            japanese: "にらみつける",
            korean: "째려보기",
            french: "Groz'Yeux",
            german: "Silberblick",
            spanish: "Malicioso",
            italian: "Fulmisguardo",
            english: "Leer"
        }
    },
    lick: {
        num: 122,
        metacategory: 4,
        names: {
            japanese: "したでなめる",
            korean: "핥기",
            french: "Léchouille",
            german: "Schlecker",
            spanish: "Lengüetazo",
            italian: "Leccata",
            english: "Lick"
        }
    },
    lightofruin: {
        num: 617,
        metacategory: 0,
        names: {
            japanese: "はめつのひかり",
            korean: "파멸의빛",
            french: "Lumière du Néant",
            german: "Lux Calamitatis",
            spanish: "Luz Aniquiladora",
            italian: "Luce Nefasta",
            english: "Light of Ruin"
        }
    },
    lightscreen: {
        num: 113,
        metacategory: 11,
        names: {
            japanese: "ひかりのかべ",
            korean: "빛의장막",
            french: "Mur Lumière",
            german: "Lichtschild",
            spanish: "Pantalla de Luz",
            italian: "Schermoluce",
            english: "Light Screen"
        }
    },
    liquidation: {
        num: 710
    },
    lockon: {
        num: 199,
        metacategory: 13,
        names: {
            japanese: "ロックオン",
            korean: "록온",
            french: "Verrouillage",
            german: "Zielschuss",
            spanish: "Fijar Blanco",
            italian: "Localizza",
            english: "Lock-On"
        }
    },
    lovelykiss: {
        num: 142,
        metacategory: 1,
        names: {
            japanese: "あくまのキッス",
            korean: "악마의키스",
            french: "Grobisou",
            german: "Todeskuss",
            spanish: "Beso Amoroso",
            italian: "Demonbacio",
            english: "Lovely Kiss"
        }
    },
    lowkick: {
        num: 67,
        metacategory: 0,
        names: {
            japanese: "けたぐり",
            korean: "안다리걸기",
            french: "Balayage",
            german: "Fußkick",
            spanish: "Patada Baja",
            italian: "Colpo Basso",
            english: "Low Kick"
        }
    },
    lowsweep: {
        num: 490,
        metacategory: 6,
        names: {
            japanese: "ローキック",
            korean: "로킥",
            french: "Balayette",
            german: "Fußtritt",
            spanish: "Puntapié",
            italian: "Calciobasso",
            english: "Low Sweep"
        }
    },
    luckychant: {
        num: 381,
        metacategory: 11,
        names: {
            japanese: "おまじない",
            korean: "주술",
            french: "Air Veinard",
            german: "Beschwörung",
            spanish: "Conjuro",
            italian: "Fortuncanto",
            english: "Lucky Chant"
        }
    },
    lunardance: {
        num: 461,
        metacategory: 13,
        names: {
            japanese: "みかづきのまい",
            korean: "초승달춤",
            french: "Danse-Lune",
            german: "Lunartanz",
            spanish: "Danza Lunar",
            italian: "Lunardanza",
            english: "Lunar Dance"
        }
    },
    lunge: {
        num: 679
    },
    lusterpurge: {
        num: 295,
        metacategory: 6,
        names: {
            japanese: "ラスターパージ",
            korean: "라스트버지",
            french: "Lumi-Éclat",
            german: "Scheinwerfer",
            spanish: "Resplandor",
            italian: "Abbagliante",
            english: "Luster Purge"
        }
    },
    machpunch: {
        num: 183,
        metacategory: 0,
        names: {
            japanese: "マッハパンチ",
            korean: "마하펀치",
            french: "Mach Punch",
            german: "Tempohieb",
            spanish: "Ultrapuño",
            italian: "Pugnorapido",
            english: "Mach Punch"
        }
    },
    magiccoat: {
        num: 277,
        metacategory: 13,
        names: {
            japanese: "マジックコート",
            korean: "매직코트",
            french: "Reflet Magik",
            german: "Magiemantel",
            spanish: "Capa Mágica",
            italian: "Magivelo",
            english: "Magic Coat"
        }
    },
    magicroom: {
        num: 478,
        metacategory: 10,
        names: {
            japanese: "マジックルーム",
            korean: "매직룸",
            french: "Zone Magique",
            german: "Magieraum",
            spanish: "Zona Mágica",
            italian: "Magicozona",
            english: "Magic Room"
        }
    },
    magicalleaf: {
        num: 345,
        metacategory: 0,
        names: {
            japanese: "マジカルリーフ",
            korean: "메지컬리프",
            french: "Feuille Magik",
            german: "Zauberblatt",
            spanish: "Hoja Mágica",
            italian: "Fogliamagica",
            english: "Magical Leaf"
        }
    },
    magmastorm: {
        num: 463,
        metacategory: 4,
        names: {
            japanese: "マグマストーム",
            korean: "마그마스톰",
            french: "Vortex Magma",
            german: "Lavasturm",
            spanish: "Lluvia Ígnea",
            italian: "Magmaclisma",
            english: "Magma Storm"
        }
    },
    magnetbomb: {
        num: 443,
        metacategory: 0,
        names: {
            japanese: "マグネットボム",
            korean: "마그넷봄",
            french: "Bombaimant",
            german: "Magnetbombe",
            spanish: "Bomba Imán",
            italian: "Bombagnete",
            english: "Magnet Bomb"
        }
    },
    magneticflux: {
        num: 602,
        metacategory: 2,
        names: {
            japanese: "じばそうさ",
            korean: "자기장조작",
            french: "Magné-Contrôle",
            german: "Magnetregler",
            spanish: "Aura Magnética",
            italian: "Controllo Polare",
            english: "Magnetic Flux"
        }
    },
    magnetrise: {
        num: 393,
        metacategory: 13,
        names: {
            japanese: "でんじふゆう",
            korean: "전자부유",
            french: "Vol Magnétik",
            german: "Magnetflug",
            spanish: "Levitón",
            italian: "Magnetascesa",
            english: "Magnet Rise"
        }
    },
    magnitude: {
        num: 222,
        metacategory: 0,
        names: {
            japanese: "マグニチュード",
            korean: "매그니튜드",
            french: "Ampleur",
            german: "Intensität",
            spanish: "Magnitud",
            italian: "Magnitudo",
            english: "Magnitude"
        }
    },
    maliciousmoonsault: {
        num: 696
    },
    matblock: {
        num: 561,
        metacategory: 11,
        names: {
            japanese: "たたみがえし",
            korean: "마룻바닥세워막기",
            french: "Tatamigaeshi",
            german: "Tatami-Schild",
            spanish: "Escudo Tatami",
            italian: "Ribaltappeto",
            english: "Mat Block"
        }
    },
    mefirst: {
        num: 382,
        metacategory: 0,
        names: {
            japanese: "さきどり",
            korean: "선취",
            french: "Moi d'Abord",
            german: "Egotrip",
            spanish: "Yo Primero",
            italian: "Precedenza",
            english: "Me First"
        }
    },
    meanlook: {
        num: 212,
        metacategory: 13,
        names: {
            japanese: "くろいまなざし",
            korean: "검은눈빛",
            french: "Regard Noir",
            german: "Horrorblick",
            spanish: "Mal de Ojo",
            italian: "Malosguardo",
            english: "Mean Look"
        }
    },
    meditate: {
        num: 96,
        metacategory: 2,
        names: {
            japanese: "ヨガのポーズ",
            korean: "요가포즈",
            french: "Yoga",
            german: "Meditation",
            spanish: "Meditación",
            italian: "Meditazione",
            english: "Meditate"
        }
    },
    megadrain: {
        num: 72,
        metacategory: 8,
        names: {
            japanese: "メガドレイン",
            korean: "메가드레인",
            french: "Méga-Sangsue",
            german: "Megasauger",
            spanish: "Megaagotar",
            italian: "Megassorbimento",
            english: "Mega Drain"
        }
    },
    megakick: {
        num: 25,
        metacategory: 0,
        names: {
            japanese: "メガトンキック",
            korean: "메가톤킥",
            french: "Ultimawashi",
            german: "Megakick",
            spanish: "Megapatada",
            italian: "Megacalcio",
            english: "Mega Kick"
        }
    },
    megapunch: {
        num: 5,
        metacategory: 0,
        names: {
            japanese: "メガトンパンチ",
            korean: "메가톤펀치",
            french: "Ultimapoing",
            german: "Megahieb",
            spanish: "Megapuño",
            italian: "Megapugno",
            english: "Mega Punch"
        }
    },
    megahorn: {
        num: 224,
        metacategory: 0,
        names: {
            japanese: "メガホーン",
            korean: "메가폰",
            french: "Mégacorne",
            german: "Vielender",
            spanish: "Megacuerno",
            italian: "Megacorno",
            english: "Megahorn"
        }
    },
    memento: {
        num: 262,
        metacategory: 13,
        names: {
            japanese: "おきみやげ",
            korean: "추억의선물",
            french: "Souvenir",
            german: "Memento-Mori",
            spanish: "Legado",
            italian: "Memento",
            english: "Memento"
        }
    },
    metalburst: {
        num: 368,
        metacategory: 0,
        names: {
            japanese: "メタルバースト",
            korean: "메탈버스트",
            french: "Fulmifer",
            german: "Metallstoß",
            spanish: "Represión Metal",
            italian: "Metalscoppio",
            english: "Metal Burst"
        }
    },
    metalclaw: {
        num: 232,
        metacategory: 7,
        names: {
            japanese: "メタルクロー",
            korean: "메탈크로우",
            french: "Griffe Acier",
            german: "Metallklaue",
            spanish: "Garra Metal",
            italian: "Ferrartigli",
            english: "Metal Claw"
        }
    },
    metalsound: {
        num: 319,
        metacategory: 2,
        names: {
            japanese: "きんぞくおん",
            korean: "금속음",
            french: "Strido-Son",
            german: "Metallsound",
            spanish: "Eco Metálico",
            italian: "Ferrostrido",
            english: "Metal Sound"
        }
    },
    meteormash: {
        num: 309,
        metacategory: 7,
        names: {
            japanese: "コメットパンチ",
            korean: "코멧펀치",
            french: "Poing Météor",
            german: "Sternenhieb",
            spanish: "Puño Meteoro",
            italian: "Meteorpugno",
            english: "Meteor Mash"
        }
    },
    metronome: {
        num: 118,
        metacategory: 13,
        names: {
            japanese: "ゆびをふる",
            korean: "손가락흔들기",
            french: "Métronome",
            german: "Metronom",
            spanish: "Metrónomo",
            italian: "Metronomo",
            english: "Metronome"
        }
    },
    milkdrink: {
        num: 208,
        metacategory: 3,
        names: {
            japanese: "ミルクのみ",
            korean: "우유마시기",
            french: "Lait à Boire",
            german: "Milchgetränk",
            spanish: "Batido",
            italian: "Buonlatte",
            english: "Milk Drink"
        }
    },
    mimic: {
        num: 102,
        metacategory: 13,
        names: {
            japanese: "ものまね",
            korean: "흉내내기",
            french: "Copie",
            german: "Mimikry",
            spanish: "Mimético",
            italian: "Mimica",
            english: "Mimic"
        }
    },
    mindreader: {
        num: 170,
        metacategory: 13,
        names: {
            japanese: "こころのめ",
            korean: "마음의눈",
            french: "Lire-Esprit",
            german: "Willensleser",
            spanish: "Telépata",
            italian: "Leggimente",
            english: "Mind Reader"
        }
    },
    minimize: {
        num: 107,
        metacategory: 2,
        names: {
            japanese: "ちいさくなる",
            korean: "작아지기",
            french: "Lilliput",
            german: "Komprimator",
            spanish: "Reducción",
            italian: "Minimizzato",
            english: "Minimize"
        }
    },
    miracleeye: {
        num: 357,
        metacategory: 1,
        names: {
            japanese: "ミラクルアイ",
            korean: "미라클아이",
            french: "Œil Miracle",
            german: "Wunderauge",
            spanish: "Gran Ojo",
            italian: "Miracolvista",
            english: "Miracle Eye"
        }
    },
    mirrorcoat: {
        num: 243,
        metacategory: 0,
        names: {
            japanese: "ミラーコート",
            korean: "미러코트",
            french: "Voile Miroir",
            german: "Spiegelcape",
            spanish: "Manto Espejo",
            italian: "Specchiovelo",
            english: "Mirror Coat"
        }
    },
    mirrormove: {
        num: 119,
        metacategory: 13,
        names: {
            japanese: "オウムがえし",
            korean: "따라하기",
            french: "Mimique",
            german: "Spiegeltrick",
            spanish: "Mov. Espejo",
            italian: "Speculmossa",
            english: "Mirror Move"
        }
    },
    mirrorshot: {
        num: 429,
        metacategory: 6,
        names: {
            japanese: "ミラーショット",
            korean: "미러숏",
            french: "Miroi-Tir",
            german: "Spiegelsalve",
            spanish: "Disparo Espejo",
            italian: "Cristalcolpo",
            english: "Mirror Shot"
        }
    },
    mist: {
        num: 54,
        metacategory: 11,
        names: {
            japanese: "しろいきり",
            korean: "흰안개",
            french: "Brume",
            german: "Weißnebel",
            spanish: "Neblina",
            italian: "Nebbia",
            english: "Mist"
        }
    },
    mistball: {
        num: 296,
        metacategory: 6,
        names: {
            japanese: "ミストボール",
            korean: "미스트볼",
            french: "Ball'Brume",
            german: "Nebelball",
            spanish: "Bola Neblina",
            italian: "Foschisfera",
            english: "Mist Ball"
        }
    },
    mistyterrain: {
        num: 581,
        metacategory: 10,
        names: {
            japanese: "ミストフィールド",
            korean: "미스트필드",
            french: "Champ Brumeux",
            german: "Nebelfeld",
            spanish: "Campo de Niebla",
            italian: "Campo Nebbioso",
            english: "Misty Terrain"
        }
    },
    moonblast: {
        num: 585,
        metacategory: 6,
        names: {
            japanese: "ムーンフォース",
            korean: "문포스",
            french: "Pouvoir Lunaire",
            german: "Mondgewalt",
            spanish: "Fuerza Lunar",
            italian: "Forza Lunare",
            english: "Moonblast"
        }
    },
    moongeistbeam: {
        num: 714
    },
    moonlight: {
        num: 236,
        metacategory: 3,
        names: {
            japanese: "つきのひかり",
            korean: "달의불빛",
            french: "Rayon Lune",
            german: "Mondschein",
            spanish: "Luz Lunar",
            italian: "Lucelunare",
            english: "Moonlight"
        }
    },
    morningsun: {
        num: 234,
        metacategory: 3,
        names: {
            japanese: "あさのひざし",
            korean: "아침햇살",
            french: "Aurore",
            german: "Morgengrauen",
            spanish: "Sol Matinal",
            italian: "Mattindoro",
            english: "Morning Sun"
        }
    },
    mudslap: {
        num: 189,
        metacategory: 6,
        names: {
            japanese: "どろかけ",
            korean: "진흙뿌리기",
            french: "Coud'Boue",
            german: "Lehmschelle",
            spanish: "Bofetón Lodo",
            italian: "Fangosberla",
            english: "Mud-Slap"
        }
    },
    mudbomb: {
        num: 426,
        metacategory: 6,
        names: {
            japanese: "どろばくだん",
            korean: "진흙폭탄",
            french: "Boue-Bombe",
            german: "Schlammbombe",
            spanish: "Bomba Fango",
            italian: "Pantanobomba",
            english: "Mud Bomb"
        }
    },
    mudshot: {
        num: 341,
        metacategory: 6,
        names: {
            japanese: "マッドショット",
            korean: "머드숏",
            french: "Tir de Boue",
            german: "Lehmschuss",
            spanish: "Disparo Lodo",
            italian: "Colpodifango",
            english: "Mud Shot"
        }
    },
    mudsport: {
        num: 300,
        metacategory: 10,
        names: {
            japanese: "どろあそび",
            korean: "흙놀이",
            french: "Lance-Boue",
            german: "Lehmsuhler",
            spanish: "Chapoteo Lodo",
            italian: "Fangata",
            english: "Mud Sport"
        }
    },
    muddywater: {
        num: 330,
        metacategory: 6,
        names: {
            japanese: "だくりゅう",
            korean: "탁류",
            french: "Ocroupi",
            german: "Lehmbrühe",
            spanish: "Agua Lodosa",
            italian: "Fanghiglia",
            english: "Muddy Water"
        }
    },
    multiattack: {
        num: 718
    },
    mysticalfire: {
        num: 595,
        metacategory: 6,
        names: {
            japanese: "マジカルフレイム",
            korean: "매지컬플레임",
            french: "Feu Ensorcelé",
            german: "Magieflamme",
            spanish: "Llama Embrujada",
            italian: "Magifiamma",
            english: "Mystical Fire"
        }
    },
    nastyplot: {
        num: 417,
        metacategory: 2,
        names: {
            japanese: "わるだくみ",
            korean: "나쁜음모",
            french: "Machination",
            german: "Ränkeschmied",
            spanish: "Maquinación",
            italian: "Congiura",
            english: "Nasty Plot"
        }
    },
    naturalgift: {
        num: 363,
        metacategory: 0,
        names: {
            japanese: "しぜんのめぐみ",
            korean: "자연의은혜",
            french: "Don Naturel",
            german: "Beerenkräfte",
            spanish: "Don Natural",
            italian: "Dononaturale",
            english: "Natural Gift"
        }
    },
    naturepower: {
        num: 267,
        metacategory: 13,
        names: {
            japanese: "しぜんのちから",
            korean: "자연의힘",
            french: "Force-Nature",
            german: "Natur-Kraft",
            spanish: "Adaptación",
            italian: "Naturforza",
            english: "Nature Power"
        }
    },
    naturesmadness: {
        num: 717
    },
    needlearm: {
        num: 302,
        metacategory: 0,
        names: {
            japanese: "ニードルアーム",
            korean: "바늘팔",
            french: "Poing Dard",
            german: "Nietenranke",
            spanish: "Brazo Pincho",
            italian: "Pugnospine",
            english: "Needle Arm"
        }
    },
    neverendingnightmare: {
        num: 636
    },
    nightdaze: {
        num: 539,
        metacategory: 6,
        names: {
            japanese: "ナイトバースト",
            korean: "나이트버스트",
            french: "Explonuit",
            german: "Nachtflut",
            spanish: "Pulso Noche",
            italian: "Urtoscuro",
            english: "Night Daze"
        }
    },
    nightshade: {
        num: 101,
        metacategory: 0,
        names: {
            japanese: "ナイトヘッド",
            korean: "나이트헤드",
            french: "Ombre Nocturne",
            german: "Nachtnebel",
            spanish: "Tinieblas",
            italian: "Ombra Notturna",
            english: "Night Shade"
        }
    },
    nightslash: {
        num: 400,
        metacategory: 0,
        names: {
            japanese: "つじぎり",
            korean: "깜짝베기",
            french: "Tranche-Nuit",
            german: "Nachthieb",
            spanish: "Tajo Umbrío",
            italian: "Nottesferza",
            english: "Night Slash"
        }
    },
    nightmare: {
        num: 171,
        metacategory: 1,
        names: {
            japanese: "あくむ",
            korean: "악몽",
            french: "Cauchemar",
            german: "Nachtmahr",
            spanish: "Pesadilla",
            italian: "Incubo",
            english: "Nightmare"
        }
    },
    nobleroar: {
        num: 568,
        metacategory: 2,
        names: {
            japanese: "おたけび",
            korean: "부르짖기",
            french: "Râle Mâle",
            german: "Kampfgebrüll",
            spanish: "Rugido de Guerra",
            italian: "Urlo",
            english: "Noble Roar"
        }
    },
    nuzzle: {
        num: 609,
        metacategory: 4,
        names: {
            japanese: "ほっぺすりすり",
            korean: "볼부비부비",
            french: "Frotte-Frimousse",
            german: "Wangenrubbler",
            spanish: "Moflete Estático",
            italian: "Elettrococcola",
            english: "Nuzzle"
        }
    },
    oblivionwing: {
        num: 613,
        metacategory: 8,
        names: {
            japanese: "デスウイング",
            korean: "데스윙",
            french: "Mort-Ailes",
            german: "Unheilsschwingen",
            spanish: "Ala Mortífera",
            italian: "Ali del Fato",
            english: "Oblivion Wing"
        }
    },
    oceanicoperetta: {
        num: 697
    },
    octazooka: {
        num: 190,
        metacategory: 6,
        names: {
            japanese: "オクタンほう",
            korean: "대포무노포",
            french: "Octazooka",
            german: "Octazooka",
            spanish: "Pulpocañón",
            italian: "Octazooka",
            english: "Octazooka"
        }
    },
    odorsleuth: {
        num: 316,
        metacategory: 1,
        names: {
            japanese: "かぎわける",
            korean: "냄새구별",
            french: "Flair",
            german: "Schnüffler",
            spanish: "Rastreo",
            italian: "Segugio",
            english: "Odor Sleuth"
        }
    },
    ominouswind: {
        num: 466,
        metacategory: 7,
        names: {
            japanese: "あやしいかぜ",
            korean: "괴상한바람",
            french: "Vent Mauvais",
            german: "Unheilböen",
            spanish: "Viento Aciago",
            italian: "Funestovento",
            english: "Ominous Wind"
        }
    },
    originpulse: {
        num: 618,
        names: {
            japanese: "こんげんのはどう",
            french: "Onde Originelle",
            german: "Ursprungswoge",
            spanish: "Pulso Primigenio",
            italian: "Primopulsar",
            english: "Origin Pulse"
        }
    },
    outrage: {
        num: 200,
        metacategory: 0,
        names: {
            japanese: "げきりん",
            korean: "역린",
            french: "Colère",
            german: "Wutanfall",
            spanish: "Enfado",
            italian: "Oltraggio",
            english: "Outrage"
        }
    },
    overheat: {
        num: 315,
        metacategory: 7,
        names: {
            japanese: "オーバーヒート",
            korean: "오버히트",
            french: "Surchauffe",
            german: "Hitzekoller",
            spanish: "Sofoco",
            italian: "Vampata",
            english: "Overheat"
        }
    },
    painsplit: {
        num: 220,
        metacategory: 13,
        names: {
            japanese: "いたみわけ",
            korean: "아픔나누기",
            french: "Balance",
            german: "Leidteiler",
            spanish: "Divide Dolor",
            italian: "Malcomune",
            english: "Pain Split"
        }
    },
    paraboliccharge: {
        num: 570,
        metacategory: 8,
        names: {
            japanese: "パラボラチャージ",
            korean: "파라볼라차지",
            french: "Parabocharge",
            german: "Parabolladung",
            spanish: "Carga Parábola",
            italian: "Caricaparabola",
            english: "Parabolic Charge"
        }
    },
    partingshot: {
        num: 575,
        metacategory: 2,
        names: {
            japanese: "すてゼリフ",
            korean: "막말내뱉기",
            french: "Dernier Mot",
            german: "Abgangstirade",
            spanish: "Última Palabra",
            italian: "Monito",
            english: "Parting Shot"
        }
    },
    payday: {
        num: 6,
        metacategory: 0,
        names: {
            japanese: "ネコにこばん",
            korean: "고양이돈받기",
            french: "Jackpot",
            german: "Zahltag",
            spanish: "Día de Pago",
            italian: "Giornopaga",
            english: "Pay Day"
        }
    },
    payback: {
        num: 371,
        metacategory: 0,
        names: {
            japanese: "しっぺがえし",
            korean: "보복",
            french: "Représailles",
            german: "Gegenstoß",
            spanish: "Vendetta",
            italian: "Rivincita",
            english: "Payback"
        }
    },
    peck: {
        num: 64,
        metacategory: 0,
        names: {
            japanese: "つつく",
            korean: "쪼기",
            french: "Picpic",
            german: "Schnabel",
            spanish: "Picotazo",
            italian: "Beccata",
            english: "Peck"
        }
    },
    perishsong: {
        num: 195,
        metacategory: 1,
        names: {
            japanese: "ほろびのうた",
            korean: "멸망의노래",
            french: "Requiem",
            german: "Abgesang",
            spanish: "Canto Mortal",
            italian: "Ultimocanto",
            english: "Perish Song"
        }
    },
    petalblizzard: {
        num: 572,
        metacategory: 0,
        names: {
            japanese: "はなふぶき",
            korean: "꽃보라",
            french: "Tempête Florale",
            german: "Blütenwirbel",
            spanish: "Tormenta Floral",
            italian: "Fiortempesta",
            english: "Petal Blizzard"
        }
    },
    petaldance: {
        num: 80,
        metacategory: 0,
        names: {
            japanese: "はなびらのまい",
            korean: "꽃잎댄스",
            french: "Danse-Fleur",
            german: "Blättertanz",
            spanish: "Danza Pétalo",
            italian: "Petalodanza",
            english: "Petal Dance"
        }
    },
    phantomforce: {
        num: 566,
        metacategory: 0,
        names: {
            japanese: "ゴーストダイブ",
            korean: "고스트다이브",
            french: "Hantise",
            german: "Phantomkraft",
            spanish: "Golpe Fantasma",
            italian: "Spettrotuffo",
            english: "Phantom Force"
        }
    },
    pinmissile: {
        num: 42,
        metacategory: 0,
        names: {
            japanese: "ミサイルばり",
            korean: "바늘미사일",
            french: "Dard-Nuée",
            german: "Nadelrakete",
            spanish: "Pin Misil",
            italian: "Missilspillo",
            english: "Pin Missile"
        }
    },
    playnice: {
        num: 589,
        metacategory: 2,
        names: {
            japanese: "なかよくする",
            korean: "친해지기",
            french: "Camaraderie",
            german: "Kameradschaft",
            spanish: "Camaradería",
            italian: "Simpatia",
            english: "Play Nice"
        }
    },
    playrough: {
        num: 583,
        metacategory: 6,
        names: {
            japanese: "じゃれつく",
            korean: "치근거리기",
            french: "Câlinerie",
            german: "Knuddler",
            spanish: "Carantoña",
            italian: "Carineria",
            english: "Play Rough"
        }
    },
    pluck: {
        num: 365,
        metacategory: 0,
        names: {
            japanese: "ついばむ",
            korean: "쪼아대기",
            french: "Picore",
            german: "Pflücker",
            spanish: "Picoteo",
            italian: "Spennata",
            english: "Pluck"
        }
    },
    poisonfang: {
        num: 305,
        metacategory: 4,
        names: {
            japanese: "どくどくのキバ",
            korean: "독엄니",
            french: "Crochet Venin",
            german: "Giftzahn",
            spanish: "Colmillo Ven",
            italian: "Velenodenti",
            english: "Poison Fang"
        }
    },
    poisongas: {
        num: 139,
        metacategory: 1,
        names: {
            japanese: "どくガス",
            korean: "독가스",
            french: "Gaz Toxik",
            german: "Giftwolke",
            spanish: "Gas Venenoso",
            italian: "Velenogas",
            english: "Poison Gas"
        }
    },
    poisonjab: {
        num: 398,
        metacategory: 4,
        names: {
            japanese: "どくづき",
            korean: "독찌르기",
            french: "Direct Toxik",
            german: "Gifthieb",
            spanish: "Puya Nociva",
            italian: "Velenpuntura",
            english: "Poison Jab"
        }
    },
    poisonpowder: {
        num: 77,
        metacategory: 1,
        names: {
            japanese: "どくのこな",
            korean: "독가루",
            french: "Poudre Toxik",
            german: "Giftpuder",
            spanish: "Polvo Veneno",
            italian: "Velenpolvere",
            english: "Poison Powder"
        }
    },
    poisonsting: {
        num: 40,
        metacategory: 4,
        names: {
            japanese: "どくばり",
            korean: "독침",
            french: "Dard-Venin",
            german: "Giftstachel",
            spanish: "Picotazo Ven",
            italian: "Velenospina",
            english: "Poison Sting"
        }
    },
    poisontail: {
        num: 342,
        metacategory: 4,
        names: {
            japanese: "ポイズンテール",
            korean: "포이즌테일",
            french: "Queue-Poison",
            german: "Giftschweif",
            spanish: "Cola Veneno",
            italian: "Velenocoda",
            english: "Poison Tail"
        }
    },
    pollenpuff: {
        num: 676
    },
    pound: {
        num: 1,
        metacategory: 0,
        names: {
            japanese: "はたく",
            korean: "막치기",
            french: "Écras'Face",
            german: "Pfund",
            spanish: "Destructor",
            italian: "Botta",
            english: "Pound"
        }
    },
    powder: {
        num: 600,
        metacategory: 13,
        names: {
            japanese: "ふんじん",
            korean: "분진",
            french: "Nuée de Poudre",
            german: "Pulverschleuder",
            spanish: "Polvo Explosivo",
            italian: "Pulviscoppio",
            english: "Powder"
        }
    },
    powdersnow: {
        num: 181,
        metacategory: 4,
        names: {
            japanese: "こなゆき",
            korean: "눈싸라기",
            french: "Poudreuse",
            german: "Pulverschnee",
            spanish: "Nieve Polvo",
            italian: "Polneve",
            english: "Powder Snow"
        }
    },
    powergem: {
        num: 408,
        metacategory: 0,
        names: {
            japanese: "パワージェム",
            korean: "파워젬",
            french: "Rayon Gemme",
            german: "Juwelenkraft",
            spanish: "Joya de Luz",
            italian: "Gemmoforza",
            english: "Power Gem"
        }
    },
    powersplit: {
        num: 471,
        metacategory: 13,
        names: {
            japanese: "パワーシェア",
            korean: "파워셰어",
            french: "Partage Force",
            german: "Kraftteiler",
            spanish: "Isofuerza",
            italian: "Pariattacco",
            english: "Power Split"
        }
    },
    powerswap: {
        num: 384,
        metacategory: 13,
        names: {
            japanese: "パワースワップ",
            korean: "파워스웹",
            french: "Permuforce",
            german: "Krafttausch",
            spanish: "Cambia Fuerza",
            italian: "Barattoforza",
            english: "Power Swap"
        }
    },
    powertrick: {
        num: 379,
        metacategory: 13,
        names: {
            japanese: "パワートリック",
            korean: "파워트릭",
            french: "Astuce Force",
            german: "Krafttrick",
            spanish: "Truco Fuerza",
            italian: "Ingannoforza",
            english: "Power Trick"
        }
    },
    powertrip: {
        num: 681
    },
    poweruppunch: {
        num: 612,
        metacategory: 7,
        names: {
            japanese: "グロウパンチ",
            korean: "그로우펀치",
            french: "Poing Boost",
            german: "Steigerungshieb",
            spanish: "Puño Incremento",
            italian: "Crescipugno",
            english: "Power-Up Punch"
        }
    },
    powerwhip: {
        num: 438,
        metacategory: 0,
        names: {
            japanese: "パワーウィップ",
            korean: "파워휩",
            french: "Mégafouet",
            german: "Blattgeißel",
            spanish: "Latigazo",
            italian: "Vigorcolpo",
            english: "Power Whip"
        }
    },
    precipiceblades: {
        num: 619,
        names: {
            japanese: "だんがいのつるぎ",
            french: "Lame Pangéenne",
            german: "Abgrundsklinge",
            spanish: "Filo del Abismo",
            italian: "Spade Telluriche",
            english: "Precipice Blades"
        }
    },
    present: {
        num: 217,
        metacategory: 0,
        names: {
            japanese: "プレゼント",
            korean: "프레젠트",
            french: "Cadeau",
            german: "Geschenk",
            spanish: "Presente",
            italian: "Regalino",
            english: "Present"
        }
    },
    prismaticlaser: {
        num: 711
    },
    protect: {
        num: 182,
        metacategory: 13,
        names: {
            japanese: "まもる",
            korean: "방어",
            french: "Abri",
            german: "Schutzschild",
            spanish: "Protección",
            italian: "Protezione",
            english: "Protect"
        }
    },
    psybeam: {
        num: 60,
        metacategory: 4,
        names: {
            japanese: "サイケこうせん",
            korean: "환상빔",
            french: "Rafale Psy",
            german: "Psystrahl",
            spanish: "Psicorrayo",
            italian: "Psicoraggio",
            english: "Psybeam"
        }
    },
    psychup: {
        num: 244,
        metacategory: 13,
        names: {
            japanese: "じこあんじ",
            korean: "자기암시",
            french: "Boost",
            german: "Psycho-Plus",
            spanish: "Más Psique",
            italian: "Psicamisù",
            english: "Psych Up"
        }
    },
    psychic: {
        num: 94,
        metacategory: 6,
        names: {
            japanese: "サイコキネシス",
            korean: "사이코키네시스",
            french: "Psyko",
            german: "Psychokinese",
            spanish: "Psíquico",
            italian: "Psichico",
            english: "Psychic"
        }
    },
    psychicfangs: {
        num: 706
    },
    psychicterrain: {
        num: 678
    },
    psychoboost: {
        num: 354,
        metacategory: 7,
        names: {
            japanese: "サイコブースト",
            korean: "사이코부스트",
            french: "Psycho Boost",
            german: "Psyschub",
            spanish: "Psicoataque",
            italian: "Psicoslancio",
            english: "Psycho Boost"
        }
    },
    psychocut: {
        num: 427,
        metacategory: 0,
        names: {
            japanese: "サイコカッター",
            korean: "사이코커터",
            french: "Coupe Psycho",
            german: "Psychoklinge",
            spanish: "Psicocorte",
            italian: "Psicotaglio",
            english: "Psycho Cut"
        }
    },
    psychoshift: {
        num: 375,
        metacategory: 13,
        names: {
            japanese: "サイコシフト",
            korean: "사이코시프트",
            french: "Échange Psy",
            german: "Psybann",
            spanish: "Psicocambio",
            italian: "Psicotrasfer",
            english: "Psycho Shift"
        }
    },
    psyshock: {
        num: 473,
        metacategory: 0,
        names: {
            japanese: "サイコショック",
            korean: "사이코쇼크",
            french: "Choc Psy",
            german: "Psychoschock",
            spanish: "Psicocarga",
            italian: "Psicoshock",
            english: "Psyshock"
        }
    },
    psystrike: {
        num: 540,
        metacategory: 0,
        names: {
            japanese: "サイコブレイク",
            korean: "사이코브레이크",
            french: "Frappe Psy",
            german: "Psychostoß",
            spanish: "Onda Mental",
            italian: "Psicobotta",
            english: "Psystrike"
        }
    },
    psywave: {
        num: 149,
        metacategory: 0,
        names: {
            japanese: "サイコウェーブ",
            korean: "사이코웨이브",
            french: "Vague Psy",
            german: "Psywelle",
            spanish: "Psicoonda",
            italian: "Psiconda",
            english: "Psywave"
        }
    },
    pulverizingpancake: {
        num: 701
    },
    punishment: {
        num: 386,
        metacategory: 0,
        names: {
            japanese: "おしおき",
            korean: "혼내기",
            french: "Punition",
            german: "Strafattacke",
            spanish: "Castigo",
            italian: "Punizione",
            english: "Punishment"
        }
    },
    purify: {
        num: 685
    },
    pursuit: {
        num: 228,
        metacategory: 0,
        names: {
            japanese: "おいうち",
            korean: "따라가때리기",
            french: "Poursuite",
            german: "Verfolgung",
            spanish: "Persecución",
            italian: "Inseguimento",
            english: "Pursuit"
        }
    },
    quash: {
        num: 511,
        metacategory: 13,
        names: {
            japanese: "さきおくり",
            korean: "순서미루기",
            french: "À la Queue",
            german: "Verzögerung",
            spanish: "Último Lugar",
            italian: "Spintone",
            english: "Quash"
        }
    },
    quickattack: {
        num: 98,
        metacategory: 0,
        names: {
            japanese: "でんこうせっか",
            korean: "전광석화",
            french: "Vive-Attaque",
            german: "Ruckzuckhieb",
            spanish: "Ataque Rápido",
            italian: "Attacco Rapido",
            english: "Quick Attack"
        }
    },
    quickguard: {
        num: 501,
        metacategory: 11,
        names: {
            japanese: "ファストガード",
            korean: "퍼스트가드",
            french: "Prévention",
            german: "Rapidschutz",
            spanish: "Anticipo",
            italian: "Anticipo",
            english: "Quick Guard"
        }
    },
    quiverdance: {
        num: 483,
        metacategory: 2,
        names: {
            japanese: "ちょうのまい",
            korean: "나비춤",
            french: "Papillodanse",
            german: "Falterreigen",
            spanish: "Danza Aleteo",
            italian: "Eledanza",
            english: "Quiver Dance"
        }
    },
    rage: {
        num: 99,
        metacategory: 0,
        names: {
            japanese: "いかり",
            korean: "분노",
            french: "Frénésie",
            german: "Raserei",
            spanish: "Furia",
            italian: "Ira",
            english: "Rage"
        }
    },
    ragepowder: {
        num: 476,
        metacategory: 13,
        names: {
            japanese: "いかりのこな",
            korean: "분노가루",
            french: "Poudre Fureur",
            german: "Wutpulver",
            spanish: "Polvo Ira",
            italian: "Polverabbia",
            english: "Rage Powder"
        }
    },
    raindance: {
        num: 240,
        metacategory: 10,
        names: {
            japanese: "あまごい",
            korean: "비바라기",
            french: "Danse Pluie",
            german: "Regentanz",
            spanish: "Danza Lluvia",
            italian: "Pioggiadanza",
            english: "Rain Dance"
        }
    },
    rapidspin: {
        num: 229,
        metacategory: 0,
        names: {
            japanese: "こうそくスピン",
            korean: "고속스핀",
            french: "Tour Rapide",
            german: "Turbodreher",
            spanish: "Giro Rápido",
            italian: "Rapigiro",
            english: "Rapid Spin"
        }
    },
    razorleaf: {
        num: 75,
        metacategory: 0,
        names: {
            japanese: "はっぱカッター",
            korean: "잎날가르기",
            french: "Tranch'Herbe",
            german: "Rasierblatt",
            spanish: "Hoja Afilada",
            italian: "Foglielama",
            english: "Razor Leaf"
        }
    },
    razorshell: {
        num: 534,
        metacategory: 6,
        names: {
            japanese: "シェルブレード",
            korean: "셸블레이드",
            french: "Coquilame",
            german: "Kalkklinge",
            spanish: "Concha Filo",
            italian: "Conchilama",
            english: "Razor Shell"
        }
    },
    razorwind: {
        num: 13,
        metacategory: 0,
        names: {
            japanese: "かまいたち",
            korean: "칼바람",
            french: "Coupe-Vent",
            german: "Klingensturm",
            spanish: "Viento Cortante",
            italian: "Ventagliente",
            english: "Razor Wind"
        }
    },
    recover: {
        num: 105,
        metacategory: 3,
        names: {
            japanese: "じこさいせい",
            korean: "HP회복",
            french: "Soin",
            german: "Genesung",
            spanish: "Recuperación",
            italian: "Ripresa",
            english: "Recover"
        }
    },
    recycle: {
        num: 278,
        metacategory: 13,
        names: {
            japanese: "リサイクル",
            korean: "리사이클",
            french: "Recyclage",
            german: "Aufbereitung",
            spanish: "Reciclaje",
            italian: "Riciclo",
            english: "Recycle"
        }
    },
    reflect: {
        num: 115,
        metacategory: 11,
        names: {
            japanese: "リフレクター",
            korean: "리플렉터",
            french: "Protection",
            german: "Reflektor",
            spanish: "Reflejo",
            italian: "Riflesso",
            english: "Reflect"
        }
    },
    reflecttype: {
        num: 513,
        metacategory: 13,
        names: {
            japanese: "ミラータイプ",
            korean: "미러타입",
            french: "Copie Type",
            german: "Typenspiegel",
            spanish: "Clonatipo",
            italian: "Riflettipo",
            english: "Reflect Type"
        }
    },
    refresh: {
        num: 287,
        metacategory: 13,
        names: {
            japanese: "リフレッシュ",
            korean: "리프레쉬",
            french: "Régénération",
            german: "Heilung",
            spanish: "Alivio",
            italian: "Rinfrescata",
            english: "Refresh"
        }
    },
    relicsong: {
        num: 547,
        metacategory: 4,
        names: {
            japanese: "いにしえのうた",
            korean: "옛노래",
            french: "Chant Antique",
            german: "Urgesang",
            spanish: "Canto Arcaico",
            italian: "Cantoantico",
            english: "Relic Song"
        }
    },
    rest: {
        num: 156,
        metacategory: 13,
        names: {
            japanese: "ねむる",
            korean: "잠자기",
            french: "Repos",
            german: "Erholung",
            spanish: "Descanso",
            italian: "Riposo",
            english: "Rest"
        }
    },
    retaliate: {
        num: 514,
        metacategory: 0,
        names: {
            japanese: "かたきうち",
            korean: "원수갚기",
            french: "Vengeance",
            german: "Heimzahlung",
            spanish: "Represalia",
            italian: "Nemesi",
            english: "Retaliate"
        }
    },
    return: {
        num: 216,
        metacategory: 0,
        names: {
            japanese: "おんがえし",
            korean: "은혜갚기",
            french: "Retour",
            german: "Rückkehr",
            spanish: "Retribución",
            italian: "Ritorno",
            english: "Return"
        }
    },
    revelationdance: {
        num: 686
    },
    revenge: {
        num: 279,
        metacategory: 0,
        names: {
            japanese: "リベンジ",
            korean: "리벤지",
            french: "Vendetta",
            german: "Vergeltung",
            spanish: "Desquite",
            italian: "Vendetta",
            english: "Revenge"
        }
    },
    reversal: {
        num: 179,
        metacategory: 0,
        names: {
            japanese: "きしかいせい",
            korean: "기사회생",
            french: "Contre",
            german: "Gegenschlag",
            spanish: "Inversión",
            italian: "Contropiede",
            english: "Reversal"
        }
    },
    roar: {
        num: 46,
        metacategory: 12,
        names: {
            japanese: "ほえる",
            korean: "울부짖기",
            french: "Hurlement",
            german: "Brüller",
            spanish: "Rugido",
            italian: "Boato",
            english: "Roar"
        }
    },
    roaroftime: {
        num: 459,
        metacategory: 0,
        names: {
            japanese: "ときのほうこう",
            korean: "시간의포효",
            french: "Hurle-Temps",
            german: "Zeitenlärm",
            spanish: "Distorsión",
            italian: "Fragortempo",
            english: "Roar of Time"
        }
    },
    rockblast: {
        num: 350,
        metacategory: 0,
        names: {
            japanese: "ロックブラスト",
            korean: "락블레스트",
            french: "Boule Roc",
            german: "Felswurf",
            spanish: "Pedrada",
            italian: "Cadutamassi",
            english: "Rock Blast"
        }
    },
    rockclimb: {
        num: 431,
        metacategory: 4,
        names: {
            japanese: "ロッククライム",
            korean: "락클라임",
            french: "Escalade",
            german: "Kraxler",
            spanish: "Treparrocas",
            italian: "Scalaroccia",
            english: "Rock Climb"
        }
    },
    rockpolish: {
        num: 397,
        metacategory: 2,
        names: {
            japanese: "ロックカット",
            korean: "록커트",
            french: "Poliroche",
            german: "Steinpolitur",
            spanish: "Pulimento",
            italian: "Lucidatura",
            english: "Rock Polish"
        }
    },
    rockslide: {
        num: 157,
        metacategory: 0,
        names: {
            japanese: "いわなだれ",
            korean: "스톤샤워",
            french: "Éboulement",
            german: "Steinhagel",
            spanish: "Avalancha",
            italian: "Frana",
            english: "Rock Slide"
        }
    },
    rocksmash: {
        num: 249,
        metacategory: 6,
        names: {
            japanese: "いわくだき",
            korean: "바위깨기",
            french: "Éclate-Roc",
            german: "Zertrümmerer",
            spanish: "Golpe Roca",
            italian: "Spaccaroccia",
            english: "Rock Smash"
        }
    },
    rockthrow: {
        num: 88,
        metacategory: 0,
        names: {
            japanese: "いわおとし",
            korean: "돌떨구기",
            french: "Jet-Pierres",
            german: "Steinwurf",
            spanish: "Lanzarrocas",
            italian: "Sassata",
            english: "Rock Throw"
        }
    },
    rocktomb: {
        num: 317,
        metacategory: 6,
        names: {
            japanese: "がんせきふうじ",
            korean: "암석봉인",
            french: "Tomberoche",
            german: "Felsgrab",
            spanish: "Tumba Rocas",
            italian: "Rocciotomba",
            english: "Rock Tomb"
        }
    },
    rockwrecker: {
        num: 439,
        metacategory: 0,
        names: {
            japanese: "がんせきほう",
            korean: "암석포",
            french: "Roc-Boulet",
            german: "Felswerfer",
            spanish: "Romperrocas",
            italian: "Devastomasso",
            english: "Rock Wrecker"
        }
    },
    roleplay: {
        num: 272,
        metacategory: 13,
        names: {
            japanese: "なりきり",
            korean: "역할",
            french: "Imitation",
            german: "Rollentausch",
            spanish: "Imitación",
            italian: "Giocodiruolo",
            english: "Role Play"
        }
    },
    rollingkick: {
        num: 27,
        metacategory: 0,
        names: {
            japanese: "まわしげり",
            korean: "돌려차기",
            french: "Mawashi Geri",
            german: "Fegekick",
            spanish: "Patada Giro",
            italian: "Calciorullo",
            english: "Rolling Kick"
        }
    },
    rollout: {
        num: 205,
        metacategory: 0,
        names: {
            japanese: "ころがる",
            korean: "구르기",
            french: "Roulade",
            german: "Walzer",
            spanish: "Desenrollar",
            italian: "Rotolamento",
            english: "Rollout"
        }
    },
    roost: {
        num: 355,
        metacategory: 3,
        names: {
            japanese: "はねやすめ",
            korean: "날개쉬기",
            french: "Atterrissage",
            german: "Ruheort",
            spanish: "Respiro",
            italian: "Trespolo",
            english: "Roost"
        }
    },
    rototiller: {
        num: 563,
        metacategory: 2,
        names: {
            japanese: "たがやす",
            korean: "일구기",
            french: "Fertilisation",
            german: "Pflüger",
            spanish: "Fertilizante",
            italian: "Aracampo",
            english: "Rototiller"
        }
    },
    round: {
        num: 496,
        metacategory: 0,
        names: {
            japanese: "りんしょう",
            korean: "돌림노래",
            french: "Chant Canon",
            german: "Kanon",
            spanish: "Canon",
            italian: "Coro",
            english: "Round"
        }
    },
    sacredfire: {
        num: 221,
        metacategory: 4,
        names: {
            japanese: "せいなるほのお",
            korean: "성스러운불꽃",
            french: "Feu Sacré",
            german: "Läuterfeuer",
            spanish: "Fuego Sagrado",
            italian: "Magifuoco",
            english: "Sacred Fire"
        }
    },
    sacredsword: {
        num: 533,
        metacategory: 0,
        names: {
            japanese: "せいなるつるぎ",
            korean: "성스러운칼",
            french: "Lame Sainte",
            german: "Sanctoklinge",
            spanish: "Espada Santa",
            italian: "Spadasolenne",
            english: "Sacred Sword"
        }
    },
    safeguard: {
        num: 219,
        metacategory: 11,
        names: {
            japanese: "しんぴのまもり",
            korean: "신비의부적",
            french: "Rune Protect",
            german: "Bodyguard",
            spanish: "Velo Sagrado",
            italian: "Salvaguardia",
            english: "Safeguard"
        }
    },
    sandattack: {
        num: 28,
        metacategory: 2,
        names: {
            japanese: "すなかけ",
            korean: "모래뿌리기",
            french: "Jet de Sable",
            german: "Sandwirbel",
            spanish: "Ataque Arena",
            italian: "Turbosabbia",
            english: "Sand Attack"
        }
    },
    sandtomb: {
        num: 328,
        metacategory: 4,
        names: {
            japanese: "すなじごく",
            korean: "모래지옥",
            french: "Tourbi-Sable",
            german: "Sandgrab",
            spanish: "Bucle Arena",
            italian: "Sabbiotomba",
            english: "Sand Tomb"
        }
    },
    sandstorm: {
        num: 201,
        metacategory: 10,
        names: {
            japanese: "すなあらし",
            korean: "모래바람",
            french: "Tempête de Sable",
            german: "Sandsturm",
            spanish: "Tormenta Arena",
            italian: "Terrempesta",
            english: "Sandstorm"
        }
    },
    savagespinout: {
        num: 634
    },
    scald: {
        num: 503,
        metacategory: 4,
        names: {
            japanese: "ねっとう",
            korean: "열탕",
            french: "Ébullition",
            german: "Siedewasser",
            spanish: "Escaldar",
            italian: "Idrovampata",
            english: "Scald"
        }
    },
    scaryface: {
        num: 184,
        metacategory: 2,
        names: {
            japanese: "こわいかお",
            korean: "겁나는얼굴",
            french: "Grimace",
            german: "Grimasse",
            spanish: "Cara Susto",
            italian: "Visotruce",
            english: "Scary Face"
        }
    },
    scratch: {
        num: 10,
        metacategory: 0,
        names: {
            japanese: "ひっかく",
            korean: "할퀴기",
            french: "Griffe",
            german: "Kratzer",
            spanish: "Arañazo",
            italian: "Graffio",
            english: "Scratch"
        }
    },
    screech: {
        num: 103,
        metacategory: 2,
        names: {
            japanese: "いやなおと",
            korean: "싫은소리",
            french: "Grincement",
            german: "Kreideschrei",
            spanish: "Chirrido",
            italian: "Stridio",
            english: "Screech"
        }
    },
    searingshot: {
        num: 545,
        metacategory: 4,
        names: {
            japanese: "かえんだん",
            korean: "화염탄",
            french: "Incendie",
            german: "Flammenball",
            spanish: "Bomba Ígnea",
            italian: "Sparafuoco",
            english: "Searing Shot"
        }
    },
    secretpower: {
        num: 290,
        metacategory: 0,
        names: {
            japanese: "ひみつのちから",
            korean: "비밀의힘",
            french: "Force Cachée",
            german: "Geheimpower",
            spanish: "Daño Secreto",
            italian: "Forzasegreta",
            english: "Secret Power"
        }
    },
    secretsword: {
        num: 548,
        metacategory: 0,
        names: {
            japanese: "しんぴのつるぎ",
            korean: "신비의칼",
            french: "Lame Ointe",
            german: "Mystoschwert",
            spanish: "Sable Místico",
            italian: "Spadamistica",
            english: "Secret Sword"
        }
    },
    seedbomb: {
        num: 402,
        metacategory: 0,
        names: {
            japanese: "タネばくだん",
            korean: "씨폭탄",
            french: "Canon Graine",
            german: "Samenbomben",
            spanish: "Bomba Germen",
            italian: "Semebomba",
            english: "Seed Bomb"
        }
    },
    seedflare: {
        num: 465,
        metacategory: 6,
        names: {
            japanese: "シードフレア",
            korean: "시드플레어",
            french: "Fulmigraine",
            german: "Schocksamen",
            spanish: "Fogonazo",
            italian: "Infuriaseme",
            english: "Seed Flare"
        }
    },
    seismictoss: {
        num: 69,
        metacategory: 0,
        names: {
            japanese: "ちきゅうなげ",
            korean: "지구던지기",
            french: "Frappe Atlas",
            german: "Geowurf",
            spanish: "Mov. Sísmico",
            italian: "Movim. Sismico",
            english: "Seismic Toss"
        }
    },
    selfdestruct: {
        num: 120,
        metacategory: 0,
        names: {
            japanese: "じばく",
            korean: "자폭",
            french: "Destruction",
            german: "Finale",
            spanish: "Autodestrucción",
            italian: "Autodistruzione",
            english: "Self-Destruct"
        }
    },
    shadowball: {
        num: 247,
        metacategory: 6,
        names: {
            japanese: "シャドーボール",
            korean: "섀도볼",
            french: "Ball'Ombre",
            german: "Spukball",
            spanish: "Bola Sombra",
            italian: "Palla Ombra",
            english: "Shadow Ball"
        }
    },
    shadowbone: {
        num: 708
    },
    shadowclaw: {
        num: 421,
        metacategory: 0,
        names: {
            japanese: "シャドークロー",
            korean: "섀도크루",
            french: "Griffe Ombre",
            german: "Dunkelklaue",
            spanish: "Garra Umbría",
            italian: "Ombrartigli",
            english: "Shadow Claw"
        }
    },
    shadowforce: {
        num: 467,
        metacategory: 0,
        names: {
            japanese: "シャドーダイブ",
            korean: "섀도다이브",
            french: "Revenant",
            german: "Schemenkraft",
            spanish: "Golpe Umbrío",
            italian: "Oscurotuffo",
            english: "Shadow Force"
        }
    },
    shadowpunch: {
        num: 325,
        metacategory: 0,
        names: {
            japanese: "シャドーパンチ",
            korean: "섀도펀치",
            french: "Poing Ombre",
            german: "Finsterfaust",
            spanish: "Puño Sombra",
            italian: "Pugnodombra",
            english: "Shadow Punch"
        }
    },
    shadowsneak: {
        num: 425,
        metacategory: 0,
        names: {
            japanese: "かげうち",
            korean: "야습",
            french: "Ombre Portée",
            german: "Schattenstoß",
            spanish: "Sombra Vil",
            italian: "Furtivombra",
            english: "Shadow Sneak"
        }
    },
    sharpen: {
        num: 159,
        metacategory: 2,
        names: {
            japanese: "かくばる",
            korean: "각지기",
            french: "Affûtage",
            german: "Schärfer",
            spanish: "Afilar",
            italian: "Affilatore",
            english: "Sharpen"
        }
    },
    shatteredpsyche: {
        num: 648
    },
    sheercold: {
        num: 329,
        metacategory: 9,
        names: {
            japanese: "ぜったいれいど",
            korean: "절대영도",
            french: "Glaciation",
            german: "Eiseskälte",
            spanish: "Frío Polar",
            italian: "Purogelo",
            english: "Sheer Cold"
        }
    },
    shellsmash: {
        num: 504,
        metacategory: 13,
        names: {
            japanese: "からをやぶる",
            korean: "껍질깨기",
            french: "Exuviation",
            german: "Hausbruch",
            spanish: "Rompecoraza",
            italian: "Gettaguscio",
            english: "Shell Smash"
        }
    },
    shelltrap: {
        num: 704
    },
    shiftgear: {
        num: 508,
        metacategory: 2,
        names: {
            japanese: "ギアチェンジ",
            korean: "기어체인지",
            french: "Chgt Vitesse",
            german: "Gangwechsel",
            spanish: "Cambio de Marcha",
            italian: "Cambiomarcia",
            english: "Shift Gear"
        }
    },
    shockwave: {
        num: 351,
        metacategory: 0,
        names: {
            japanese: "でんげきは",
            korean: "전격파",
            french: "Onde de Choc",
            german: "Schockwelle",
            spanish: "Onda Voltio",
            italian: "Ondashock",
            english: "Shock Wave"
        }
    },
    shoreup: {
        num: 659
    },
    signalbeam: {
        num: 324,
        metacategory: 4,
        names: {
            japanese: "シグナルビーム",
            korean: "시그널빔",
            french: "Rayon Signal",
            german: "Ampelleuchte",
            spanish: "Doble Rayo",
            italian: "Segnoraggio",
            english: "Signal Beam"
        }
    },
    silverwind: {
        num: 318,
        metacategory: 7,
        names: {
            japanese: "ぎんいろのかぜ",
            korean: "은빛바람",
            french: "Vent Argenté",
            german: "Silberhauch",
            spanish: "Viento Plata",
            italian: "Ventargenteo",
            english: "Silver Wind"
        }
    },
    simplebeam: {
        num: 493,
        metacategory: 13,
        names: {
            japanese: "シンプルビーム",
            korean: "심플빔",
            french: "Rayon Simple",
            german: "Wankelstrahl",
            spanish: "Onda Simple",
            italian: "Ondisinvolta",
            english: "Simple Beam"
        }
    },
    sing: {
        num: 47,
        metacategory: 1,
        names: {
            japanese: "うたう",
            korean: "노래하기",
            french: "Berceuse",
            german: "Gesang",
            spanish: "Canto",
            italian: "Canto",
            english: "Sing"
        }
    },
    sinisterarrowraid: {
        num: 695
    },
    sketch: {
        num: 166,
        metacategory: 13,
        names: {
            japanese: "スケッチ",
            korean: "스케치",
            french: "Gribouille",
            german: "Nachahmer",
            spanish: "Esquema",
            italian: "Schizzo",
            english: "Sketch"
        }
    },
    skillswap: {
        num: 285,
        metacategory: 13,
        names: {
            japanese: "スキルスワップ",
            korean: "스킬스웹",
            french: "Échange",
            german: "Wertewechsel",
            spanish: "Intercambio",
            italian: "Baratto",
            english: "Skill Swap"
        }
    },
    skullbash: {
        num: 130,
        metacategory: 0,
        names: {
            japanese: "ロケットずつき",
            korean: "로케트박치기",
            french: "Coud'Krâne",
            german: "Schädelwumme",
            spanish: "Cabezazo",
            italian: "Capocciata",
            english: "Skull Bash"
        }
    },
    skyattack: {
        num: 143,
        metacategory: 0,
        names: {
            japanese: "ゴッドバード",
            korean: "불새",
            french: "Piqué",
            german: "Himmelsfeger",
            spanish: "Ataque Aéreo",
            italian: "Aeroattacco",
            english: "Sky Attack"
        }
    },
    skydrop: {
        num: 507,
        metacategory: 0,
        names: {
            japanese: "フリーフォール",
            korean: "프리폴",
            french: "Chute Libre",
            german: "Freier Fall",
            spanish: "Caída Libre",
            italian: "Cadutalibera",
            english: "Sky Drop"
        }
    },
    skyuppercut: {
        num: 327,
        metacategory: 0,
        names: {
            japanese: "スカイアッパー",
            korean: "스카이업퍼",
            french: "Stratopercut",
            german: "Himmelhieb",
            spanish: "Gancho Alto",
            italian: "Stramontante",
            english: "Sky Uppercut"
        }
    },
    slackoff: {
        num: 303,
        metacategory: 3,
        names: {
            japanese: "なまける",
            korean: "태만함",
            french: "Paresse",
            german: "Tagedieb",
            spanish: "Relajo",
            italian: "Pigro",
            english: "Slack Off"
        }
    },
    slam: {
        num: 21,
        metacategory: 0,
        names: {
            japanese: "たたきつける",
            korean: "힘껏치기",
            french: "Souplesse",
            german: "Slam",
            spanish: "Atizar",
            italian: "Schianto",
            english: "Slam"
        }
    },
    slash: {
        num: 163,
        metacategory: 0,
        names: {
            japanese: "きりさく",
            korean: "베어가르기",
            french: "Tranche",
            german: "Schlitzer",
            spanish: "Cuchillada",
            italian: "Lacerazione",
            english: "Slash"
        }
    },
    sleeppowder: {
        num: 79,
        metacategory: 1,
        names: {
            japanese: "ねむりごな",
            korean: "수면가루",
            french: "Poudre Dodo",
            german: "Schlafpuder",
            spanish: "Somnífero",
            italian: "Sonnifero",
            english: "Sleep Powder"
        }
    },
    sleeptalk: {
        num: 214,
        metacategory: 13,
        names: {
            japanese: "ねごと",
            korean: "잠꼬대",
            french: "Blabla Dodo",
            german: "Schlafrede",
            spanish: "Sonámbulo",
            italian: "Sonnolalia",
            english: "Sleep Talk"
        }
    },
    sludge: {
        num: 124,
        metacategory: 4,
        names: {
            japanese: "ヘドロこうげき",
            korean: "오물공격",
            french: "Détritus",
            german: "Schlammbad",
            spanish: "Residuos",
            italian: "Fango",
            english: "Sludge"
        }
    },
    sludgebomb: {
        num: 188,
        metacategory: 4,
        names: {
            japanese: "ヘドロばくだん",
            korean: "오물폭탄",
            french: "Bomb-Beurk",
            german: "Matschbombe",
            spanish: "Bomba Lodo",
            italian: "Fangobomba",
            english: "Sludge Bomb"
        }
    },
    sludgewave: {
        num: 482,
        metacategory: 4,
        names: {
            japanese: "ヘドロウェーブ",
            korean: "오물웨이브",
            french: "Cradovague",
            german: "Schlammwoge",
            spanish: "Onda Tóxica",
            italian: "Fangonda",
            english: "Sludge Wave"
        }
    },
    smackdown: {
        num: 479,
        metacategory: 0,
        names: {
            japanese: "うちおとす",
            korean: "떨어뜨리기",
            french: "Anti-Air",
            german: "Katapult",
            spanish: "Antiaéreo",
            italian: "Abbattimento",
            english: "Smack Down"
        }
    },
    smartstrike: {
        num: 684
    },
    smellingsalts: {
        num: 265,
        metacategory: 0,
        names: {
            japanese: "きつけ",
            korean: "정신차리기",
            french: "Stimulant",
            german: "Riechsalz",
            spanish: "Estímulo",
            italian: "Maniereforti",
            english: "Smelling Salts"
        }
    },
    smog: {
        num: 123,
        metacategory: 4,
        names: {
            japanese: "スモッグ",
            korean: "스모그",
            french: "Purédpois",
            german: "Smog",
            spanish: "Polución",
            italian: "Smog",
            english: "Smog"
        }
    },
    smokescreen: {
        num: 108,
        metacategory: 2,
        names: {
            japanese: "えんまく",
            korean: "연막",
            french: "Brouillard",
            german: "Rauchwolke",
            spanish: "Pantalla de Humo",
            italian: "Muro di Fumo",
            english: "Smokescreen"
        }
    },
    snarl: {
        num: 555,
        metacategory: 6,
        names: {
            japanese: "バークアウト",
            korean: "바크아웃",
            french: "Aboiement",
            german: "Standpauke",
            spanish: "Alarido",
            italian: "Urlorabbia",
            english: "Snarl"
        }
    },
    snatch: {
        num: 289,
        metacategory: 13,
        names: {
            japanese: "よこどり",
            korean: "가로챔",
            french: "Saisie",
            german: "Übernahme",
            spanish: "Robo",
            italian: "Scippo",
            english: "Snatch"
        }
    },
    snore: {
        num: 173,
        metacategory: 0,
        names: {
            japanese: "いびき",
            korean: "코골기",
            french: "Ronflement",
            german: "Schnarcher",
            spanish: "Ronquido",
            italian: "Russare",
            english: "Snore"
        }
    },
    spectralthief: {
        num: 712
    },
    speedswap: {
        num: 683
    },
    spikyshield: {
        num: 596,
        metacategory: 13,
        names: {
            japanese: "ニードルガード",
            korean: "니들가드",
            french: "Pico-Défense",
            german: "Schutzstacheln",
            spanish: "Barrera Espinosa",
            italian: "Agodifesa",
            english: "Spiky Shield"
        }
    },
    spiritshackle: {
        num: 662
    },
    soak: {
        num: 487,
        metacategory: 13,
        names: {
            japanese: "みずびたし",
            korean: "물붓기",
            french: "Détrempage",
            german: "Überflutung",
            spanish: "Anegar",
            italian: "Inondazione",
            english: "Soak"
        }
    },
    softboiled: {
        num: 135,
        metacategory: 3,
        names: {
            japanese: "タマゴうみ",
            korean: "알낳기",
            french: "E-Coque",
            german: "Weichei",
            spanish: "Amortiguador",
            italian: "Covauova",
            english: "Soft-Boiled"
        }
    },
    solarbeam: {
        num: 76,
        metacategory: 0,
        names: {
            japanese: "ソーラービーム",
            korean: "솔라빔",
            french: "Lance-Soleil",
            german: "Solarstrahl",
            spanish: "Rayo Solar",
            italian: "Solarraggio",
            english: "Solar Beam"
        }
    },
    solarblade: {
        num: 669
    },
    sonicboom: {
        num: 49,
        metacategory: 0,
        names: {
            japanese: "ソニックブーム",
            korean: "소닉붐",
            french: "Sonicboom",
            german: "Ultraschall",
            spanish: "Bomba Sónica",
            italian: "Sonicboom",
            english: "Sonic Boom"
        }
    },
    soulstealing7starstrike: {
        num: 699
    },
    spacialrend: {
        num: 460,
        metacategory: 0,
        names: {
            japanese: "あくうせつだん",
            korean: "공간절단",
            french: "Spatio-Rift",
            german: "Raumschlag",
            spanish: "Corte Vacío",
            italian: "Fendispazio",
            english: "Spacial Rend"
        }
    },
    spark: {
        num: 209,
        metacategory: 4,
        names: {
            japanese: "スパーク",
            korean: "스파크",
            french: "Étincelle",
            german: "Funkensprung",
            spanish: "Chispa",
            italian: "Scintilla",
            english: "Spark"
        }
    },
    sparklingaria: {
        num: 664
    },
    spiderweb: {
        num: 169,
        metacategory: 13,
        names: {
            japanese: "クモのす",
            korean: "거미집",
            french: "Toile",
            german: "Spinnennetz",
            spanish: "Telaraña",
            italian: "Ragnatela",
            english: "Spider Web"
        }
    },
    spikecannon: {
        num: 131,
        metacategory: 0,
        names: {
            japanese: "とげキャノン",
            korean: "가시대포",
            french: "Picanon",
            german: "Dornkanone",
            spanish: "Clavo Cañón",
            italian: "Sparalance",
            english: "Spike Cannon"
        }
    },
    spikes: {
        num: 191,
        metacategory: 11,
        names: {
            japanese: "まきびし",
            korean: "압정뿌리기",
            french: "Picots",
            german: "Stachler",
            spanish: "Púas",
            italian: "Punte",
            english: "Spikes"
        }
    },
    spitup: {
        num: 255,
        metacategory: 0,
        names: {
            japanese: "はきだす",
            korean: "토해내기",
            french: "Relâche",
            german: "Entfessler",
            spanish: "Escupir",
            italian: "Sfoghenergia",
            english: "Spit Up"
        }
    },
    spite: {
        num: 180,
        metacategory: 13,
        names: {
            japanese: "うらみ",
            korean: "원한",
            french: "Dépit",
            german: "Groll",
            spanish: "Rencor",
            italian: "Dispetto",
            english: "Spite"
        }
    },
    splash: {
        num: 150,
        metacategory: 13,
        names: {
            japanese: "はねる",
            korean: "튀어오르기",
            french: "Trempette",
            german: "Platscher",
            spanish: "Salpicadura",
            italian: "Splash",
            english: "Splash"
        }
    },
    spore: {
        num: 147,
        metacategory: 1,
        names: {
            japanese: "キノコのほうし",
            korean: "버섯포자",
            french: "Spore",
            german: "Pilzspore",
            spanish: "Espora",
            italian: "Spora",
            english: "Spore"
        }
    },
    spotlight: {
        num: 671
    },
    stealthrock: {
        num: 446,
        metacategory: 11,
        names: {
            japanese: "ステルスロック",
            korean: "스텔스록",
            french: "Piège de Roc",
            german: "Tarnsteine",
            spanish: "Trampa Rocas",
            italian: "Levitoroccia",
            english: "Stealth Rock"
        }
    },
    steameruption: {
        num: 592,
        metacategory: 4,
        names: {
            japanese: "スチームバースト",
            korean: "스팀버스트",
            french: "Jet de Vapeur",
            german: "Dampfschwall",
            spanish: "Chorro de Vapor",
            italian: "Vaporscoppio",
            english: "Steam Eruption"
        }
    },
    steelwing: {
        num: 211,
        metacategory: 7,
        names: {
            japanese: "はがねのつばさ",
            korean: "강철날개",
            french: "Aile d'Acier",
            german: "Stahlflügel",
            spanish: "Ala de Acero",
            italian: "Alacciaio",
            english: "Steel Wing"
        }
    },
    stickyweb: {
        num: 564,
        metacategory: 11,
        names: {
            japanese: "ねばねばネット",
            korean: "끈적끈적네트",
            french: "Toile Gluante",
            german: "Klebenetz",
            spanish: "Red Viscosa",
            italian: "Rete Vischiosa",
            english: "Sticky Web"
        }
    },
    stockpile: {
        num: 254,
        metacategory: 13,
        names: {
            japanese: "たくわえる",
            korean: "비축하기",
            french: "Stockage",
            german: "Horter",
            spanish: "Reserva",
            italian: "Accumulo",
            english: "Stockpile"
        }
    },
    stokedsparksurfer: {
        num: 700
    },
    stomp: {
        num: 23,
        metacategory: 0,
        names: {
            japanese: "ふみつけ",
            korean: "짓밟기",
            french: "Écrasement",
            german: "Stampfer",
            spanish: "Pisotón",
            italian: "Pestone",
            english: "Stomp"
        }
    },
    stompingtantrum: {
        num: 707
    },
    stoneedge: {
        num: 444,
        metacategory: 0,
        names: {
            japanese: "ストーンエッジ",
            korean: "스톤에지",
            french: "Lame de Roc",
            german: "Steinkante",
            spanish: "Roca Afilada",
            italian: "Pietrataglio",
            english: "Stone Edge"
        }
    },
    storedpower: {
        num: 500,
        metacategory: 0,
        names: {
            japanese: "アシストパワー",
            korean: "어시스트파워",
            french: "Force Ajoutée",
            german: "Kraftvorrat",
            spanish: "Poder Reserva",
            italian: "Veicolaforza",
            english: "Stored Power"
        }
    },
    stormthrow: {
        num: 480,
        metacategory: 0,
        names: {
            japanese: "やまあらし",
            korean: "업어후리기",
            french: "Yama Arashi",
            german: "Bergsturm",
            spanish: "Llave Corsé",
            italian: "Tempestretta",
            english: "Storm Throw"
        }
    },
    steamroller: {
        num: 537,
        metacategory: 0,
        names: {
            japanese: "ハードローラー",
            korean: "하드롤러",
            french: "Bulldoboule",
            german: "Quetschwalze",
            spanish: "Rodillo de Púas",
            italian: "Rulloduro",
            english: "Steamroller"
        }
    },
    strength: {
        num: 70,
        metacategory: 0,
        names: {
            japanese: "かいりき",
            korean: "괴력",
            french: "Force",
            german: "Stärke",
            spanish: "Fuerza",
            italian: "Forza",
            english: "Strength"
        }
    },
    strengthsap: {
        num: 668
    },
    stringshot: {
        num: 81,
        metacategory: 2,
        names: {
            japanese: "いとをはく",
            korean: "실뿜기",
            french: "Sécrétion",
            german: "Fadenschuss",
            spanish: "Disparo Demora",
            italian: "Millebave",
            english: "String Shot"
        }
    },
    struggle: {
        num: 165,
        metacategory: 0,
        names: {
            japanese: "わるあがき",
            korean: "발버둥",
            french: "Lutte",
            german: "Verzweifler",
            spanish: "Combate",
            italian: "Scontro",
            english: "Struggle"
        }
    },
    strugglebug: {
        num: 522,
        metacategory: 6,
        names: {
            japanese: "むしのていこう",
            korean: "벌레의저항",
            french: "Survinsecte",
            german: "Käfertrutz",
            spanish: "Estoicismo",
            italian: "Entomoblocco",
            english: "Struggle Bug"
        }
    },
    stunspore: {
        num: 78,
        metacategory: 1,
        names: {
            japanese: "しびれごな",
            korean: "저리가루",
            french: "Para-Spore",
            german: "Stachelspore",
            spanish: "Paralizador",
            italian: "Paralizzante",
            english: "Stun Spore"
        }
    },
    submission: {
        num: 66,
        metacategory: 0,
        names: {
            japanese: "じごくぐるま",
            korean: "지옥의바퀴",
            french: "Sacrifice",
            german: "Überroller",
            spanish: "Sumisión",
            italian: "Sottomissione",
            english: "Submission"
        }
    },
    substitute: {
        num: 164,
        metacategory: 13,
        names: {
            japanese: "みがわり",
            korean: "대타출동",
            french: "Clonage",
            german: "Delegator",
            spanish: "Sustituto",
            italian: "Sostituto",
            english: "Substitute"
        }
    },
    subzeroslammer: {
        num: 650
    },
    suckerpunch: {
        num: 389,
        metacategory: 0,
        names: {
            japanese: "ふいうち",
            korean: "기습",
            french: "Coup Bas",
            german: "Tiefschlag",
            spanish: "Golpe Bajo",
            italian: "Sbigoattacco",
            english: "Sucker Punch"
        }
    },
    sunnyday: {
        num: 241,
        metacategory: 10,
        names: {
            japanese: "にほんばれ",
            korean: "쾌청",
            french: "Zénith",
            german: "Sonnentag",
            spanish: "Día Soleado",
            italian: "Giornodisole",
            english: "Sunny Day"
        }
    },
    sunsteelstrike: {
        num: 713
    },
    superfang: {
        num: 162,
        metacategory: 0,
        names: {
            japanese: "いかりのまえば",
            korean: "분노의앞니",
            french: "Croc Fatal",
            german: "Superzahn",
            spanish: "Superdiente",
            italian: "Superzanna",
            english: "Super Fang"
        }
    },
    superpower: {
        num: 276,
        metacategory: 7,
        names: {
            japanese: "ばかぢから",
            korean: "엄청난힘",
            french: "Surpuissance",
            german: "Kraftkoloss",
            spanish: "Fuerza Bruta",
            italian: "Troppoforte",
            english: "Superpower"
        }
    },
    supersonic: {
        num: 48,
        metacategory: 1,
        names: {
            japanese: "ちょうおんぱ",
            korean: "초음파",
            french: "Ultrason",
            german: "Superschall",
            spanish: "Supersónico",
            italian: "Supersuono",
            english: "Supersonic"
        }
    },
    supersonicskystrike: {
        num: 626
    },
    surf: {
        num: 57,
        metacategory: 0,
        names: {
            japanese: "なみのり",
            korean: "파도타기",
            french: "Surf",
            german: "Surfer",
            spanish: "Surf",
            italian: "Surf",
            english: "Surf"
        }
    },
    swagger: {
        num: 207,
        metacategory: 5,
        names: {
            japanese: "いばる",
            korean: "뽐내기",
            french: "Vantardise",
            german: "Angeberei",
            spanish: "Contoneo",
            italian: "Bullo",
            english: "Swagger"
        }
    },
    swallow: {
        num: 256,
        metacategory: 3,
        names: {
            japanese: "のみこむ",
            korean: "꿀꺽",
            french: "Avale",
            german: "Verzehrer",
            spanish: "Tragar",
            italian: "Introenergia",
            english: "Swallow"
        }
    },
    sweetkiss: {
        num: 186,
        metacategory: 1,
        names: {
            japanese: "てんしのキッス",
            korean: "천사의키스",
            french: "Doux Baiser",
            german: "Bitterkuss",
            spanish: "Beso Dulce",
            italian: "Dolcebacio",
            english: "Sweet Kiss"
        }
    },
    sweetscent: {
        num: 230,
        metacategory: 2,
        names: {
            japanese: "あまいかおり",
            korean: "달콤한향기",
            french: "Doux Parfum",
            german: "Lockduft",
            spanish: "Dulce Aroma",
            italian: "Profumino",
            english: "Sweet Scent"
        }
    },
    swift: {
        num: 129,
        metacategory: 0,
        names: {
            japanese: "スピードスター",
            korean: "스피드스타",
            french: "Météores",
            german: "Sternschauer",
            spanish: "Rapidez",
            italian: "Comete",
            english: "Swift"
        }
    },
    switcheroo: {
        num: 415,
        metacategory: 13,
        names: {
            japanese: "すりかえ",
            korean: "바꿔치기",
            french: "Passe-Passe",
            german: "Wechseldich",
            spanish: "Trapicheo",
            italian: "Rapidscambio",
            english: "Switcheroo"
        }
    },
    swordsdance: {
        num: 14,
        metacategory: 2,
        names: {
            japanese: "つるぎのまい",
            korean: "칼춤",
            french: "Danse-Lames",
            german: "Schwerttanz",
            spanish: "Danza Espada",
            italian: "Danzaspada",
            english: "Swords Dance"
        }
    },
    synchronoise: {
        num: 485,
        metacategory: 0,
        names: {
            japanese: "シンクロノイズ",
            korean: "싱크로노이즈",
            french: "Synchropeine",
            german: "Synchrolärm",
            spanish: "Sincrorruido",
            italian: "Sincrumore",
            english: "Synchronoise"
        }
    },
    synthesis: {
        num: 235,
        metacategory: 3,
        names: {
            japanese: "こうごうせい",
            korean: "광합성",
            french: "Synthèse",
            german: "Synthese",
            spanish: "Síntesis",
            italian: "Sintesi",
            english: "Synthesis"
        }
    },
    tackle: {
        num: 33,
        metacategory: 0,
        names: {
            japanese: "たいあたり",
            korean: "몸통박치기",
            french: "Charge",
            german: "Tackle",
            spanish: "Placaje",
            italian: "Azione",
            english: "Tackle"
        }
    },
    tailglow: {
        num: 294,
        metacategory: 2,
        names: {
            japanese: "ほたるび",
            korean: "반딧불",
            french: "Lumiqueue",
            german: "Schweifglanz",
            spanish: "Ráfaga",
            italian: "Codadiluce",
            english: "Tail Glow"
        }
    },
    tailslap: {
        num: 541,
        metacategory: 0,
        names: {
            japanese: "スイープビンタ",
            korean: "스위프뺨치기",
            french: "Plumo-Queue",
            german: "Kehrschelle",
            spanish: "Plumerazo",
            italian: "Spazzasberla",
            english: "Tail Slap"
        }
    },
    tailwhip: {
        num: 39,
        metacategory: 2,
        names: {
            japanese: "しっぽをふる",
            korean: "꼬리흔들기",
            french: "Mimi-Queue",
            german: "Rutenschlag",
            spanish: "Látigo",
            italian: "Colpocoda",
            english: "Tail Whip"
        }
    },
    tailwind: {
        num: 366,
        metacategory: 11,
        names: {
            japanese: "おいかぜ",
            korean: "순풍",
            french: "Vent Arrière",
            german: "Rückenwind",
            spanish: "Viento Afín",
            italian: "Ventoincoda",
            english: "Tailwind"
        }
    },
    takedown: {
        num: 36,
        metacategory: 0,
        names: {
            japanese: "とっしん",
            korean: "돌진",
            french: "Bélier",
            german: "Bodycheck",
            spanish: "Derribo",
            italian: "Riduttore",
            english: "Take Down"
        }
    },
    taunt: {
        num: 269,
        metacategory: 13,
        names: {
            japanese: "ちょうはつ",
            korean: "도발",
            french: "Provoc",
            german: "Verhöhner",
            spanish: "Mofa",
            italian: "Provocazione",
            english: "Taunt"
        }
    },
    tearfullook: {
        num: 715
    },
    technoblast: {
        num: 546,
        metacategory: 0,
        names: {
            japanese: "テクノバスター",
            korean: "테크노버스터",
            french: "TechnoBuster",
            german: "Techblaster",
            spanish: "Tecno Shock",
            italian: "Tecnobotto",
            english: "Techno Blast"
        }
    },
    tectonicrage: {
        num: 630
    },
    teeterdance: {
        num: 298,
        metacategory: 1,
        names: {
            japanese: "フラフラダンス",
            korean: "흔들흔들댄스",
            french: "Danse-Folle",
            german: "Taumeltanz",
            spanish: "Danza Caos",
            italian: "Strampadanza",
            english: "Teeter Dance"
        }
    },
    telekinesis: {
        num: 477,
        metacategory: 1,
        names: {
            japanese: "テレキネシス",
            korean: "텔레키네시스",
            french: "Lévikinésie",
            german: "Telekinese",
            spanish: "Telequinesis",
            italian: "Telecinesi",
            english: "Telekinesis"
        }
    },
    teleport: {
        num: 100,
        metacategory: 13,
        names: {
            japanese: "テレポート",
            korean: "순간이동",
            french: "Téléport",
            german: "Teleport",
            spanish: "Teletransporte",
            italian: "Teletrasporto",
            english: "Teleport"
        }
    },
    thief: {
        num: 168,
        metacategory: 0,
        names: {
            japanese: "どろぼう",
            korean: "도둑질",
            french: "Larcin",
            german: "Raub",
            spanish: "Ladrón",
            italian: "Furto",
            english: "Thief"
        }
    },
    thousandarrows: {
        num: 614,
        metacategory: 0,
        names: {
            japanese: "サウザンアロー",
            korean: "사우전드애로",
            french: "Myria-Flèches",
            german: "Tausend Pfeile",
            spanish: "Mil Flechas",
            italian: "Mille Frecce",
            english: "Thousand Arrows"
        }
    },
    thousandwaves: {
        num: 615
    },
    thrash: {
        num: 37,
        metacategory: 0,
        names: {
            japanese: "あばれる",
            korean: "난동부리기",
            french: "Mania",
            german: "Fuchtler",
            spanish: "Golpe",
            italian: "Colpo",
            english: "Thrash"
        }
    },
    throatchop: {
        num: 675
    },
    thunder: {
        num: 87,
        metacategory: 4,
        names: {
            japanese: "かみなり",
            korean: "번개",
            french: "Fatal-Foudre",
            german: "Donner",
            spanish: "Trueno",
            italian: "Tuono",
            english: "Thunder"
        }
    },
    thunderfang: {
        num: 422,
        metacategory: 4,
        names: {
            japanese: "かみなりのキバ",
            korean: "번개엄니",
            french: "Crocs Éclair",
            german: "Donnerzahn",
            spanish: "Colmillo Rayo",
            italian: "Fulmindenti",
            english: "Thunder Fang"
        }
    },
    thunderpunch: {
        num: 9,
        metacategory: 4,
        names: {
            japanese: "かみなりパンチ",
            korean: "번개펀치",
            french: "Poing-Éclair",
            german: "Donnerschlag",
            spanish: "Puño Trueno",
            italian: "Tuonopugno",
            english: "Thunder Punch"
        }
    },
    thundershock: {
        num: 84,
        metacategory: 4,
        names: {
            japanese: "でんきショック",
            korean: "전기쇼크",
            french: "Éclair",
            german: "Donnerschock",
            spanish: "Impactrueno",
            italian: "Tuonoshock",
            english: "Thunder Shock"
        }
    },
    thunderwave: {
        num: 86,
        metacategory: 1,
        names: {
            japanese: "でんじは",
            korean: "전기자석파",
            french: "Cage-Éclair",
            german: "Donnerwelle",
            spanish: "Onda Trueno",
            italian: "Tuononda",
            english: "Thunder Wave"
        }
    },
    thunderbolt: {
        num: 85,
        metacategory: 4,
        names: {
            japanese: "１０まんボルト",
            korean: "10만볼트",
            french: "Tonnerre",
            german: "Donnerblitz",
            spanish: "Rayo",
            italian: "Fulmine",
            english: "Thunderbolt"
        }
    },
    tickle: {
        num: 321,
        metacategory: 2,
        names: {
            japanese: "くすぐる",
            korean: "간지르기",
            french: "Chatouille",
            german: "Spaßkanone",
            spanish: "Cosquillas",
            italian: "Solletico",
            english: "Tickle"
        }
    },
    topsyturvy: {
        num: 576,
        metacategory: 13,
        names: {
            japanese: "ひっくりかえす",
            korean: "뒤집어엎기",
            french: "Renversement",
            german: "Invertigo",
            spanish: "Reversión",
            italian: "Sottosopra",
            english: "Topsy-Turvy"
        }
    },
    torment: {
        num: 259,
        metacategory: 1,
        names: {
            japanese: "いちゃもん",
            korean: "트집",
            french: "Tourmente",
            german: "Folterknecht",
            spanish: "Tormento",
            italian: "Attaccalite",
            english: "Torment"
        }
    },
    toxic: {
        num: 92,
        metacategory: 1,
        names: {
            japanese: "どくどく",
            korean: "맹독",
            french: "Toxik",
            german: "Toxin",
            spanish: "Tóxico",
            italian: "Tossina",
            english: "Toxic"
        }
    },
    toxicspikes: {
        num: 390,
        metacategory: 11,
        names: {
            japanese: "どくびし",
            korean: "독압정",
            french: "Pics Toxik",
            german: "Giftspitzen",
            spanish: "Púas Tóxicas",
            italian: "Fielepunte",
            english: "Toxic Spikes"
        }
    },
    toxicthread: {
        num: 672
    },
    transform: {
        num: 144,
        metacategory: 13,
        names: {
            japanese: "へんしん",
            korean: "변신",
            french: "Morphing",
            german: "Wandler",
            spanish: "Transformación",
            italian: "Trasformazione",
            english: "Transform"
        }
    },
    triattack: {
        num: 161,
        metacategory: 4,
        names: {
            japanese: "トライアタック",
            korean: "트라이어택",
            french: "Triplattaque",
            german: "Triplette",
            spanish: "Triataque",
            italian: "Tripletta",
            english: "Tri Attack"
        }
    },
    trick: {
        num: 271,
        metacategory: 13,
        names: {
            japanese: "トリック",
            korean: "트릭",
            french: "Tourmagik",
            german: "Trickbetrug",
            spanish: "Truco",
            italian: "Raggiro",
            english: "Trick"
        }
    },
    trickortreat: {
        num: 567,
        metacategory: 13,
        names: {
            japanese: "ハロウィン",
            korean: "핼러윈",
            french: "Halloween",
            german: "Halloween",
            spanish: "Halloween",
            italian: "Halloween",
            english: "Trick-or-Treat"
        }
    },
    trickroom: {
        num: 433,
        metacategory: 10,
        names: {
            japanese: "トリックルーム",
            korean: "트릭룸",
            french: "Distorsion",
            german: "Bizarroraum",
            spanish: "Espacio Raro",
            italian: "Distortozona",
            english: "Trick Room"
        }
    },
    triplekick: {
        num: 167,
        metacategory: 0,
        names: {
            japanese: "トリプルキック",
            korean: "트리플킥",
            french: "Triple Pied",
            german: "Dreifachkick",
            spanish: "Triple Patada",
            italian: "Triplocalcio",
            english: "Triple Kick"
        }
    },
    tropkick: {
        num: 688
    },
    trumpcard: {
        num: 376,
        metacategory: 0,
        names: {
            japanese: "きりふだ",
            korean: "마지막수단",
            french: "Atout",
            german: "Trumpfkarte",
            spanish: "As Oculto",
            italian: "Asso",
            english: "Trump Card"
        }
    },
    twineedle: {
        num: 41,
        metacategory: 4,
        names: {
            japanese: "ダブルニードル",
            korean: "더블니들",
            french: "Double-Dard",
            german: "Duonadel",
            spanish: "Doble Ataque",
            italian: "Doppio Ago",
            english: "Twineedle"
        }
    },
    twinkletackle: {
        num: 656
    },
    twister: {
        num: 239,
        metacategory: 0,
        names: {
            japanese: "たつまき",
            korean: "회오리",
            french: "Ouragan",
            german: "Windhose",
            spanish: "Ciclón",
            italian: "Tornado",
            english: "Twister"
        }
    },
    uturn: {
        num: 369,
        metacategory: 0,
        names: {
            japanese: "とんぼがえり",
            korean: "유턴",
            french: "Demi-Tour",
            german: "Kehrtwende",
            spanish: "Ida y Vuelta",
            italian: "Retromarcia",
            english: "U-turn"
        }
    },
    uproar: {
        num: 253,
        metacategory: 0,
        names: {
            japanese: "さわぐ",
            korean: "소란피기",
            french: "Brouhaha",
            german: "Aufruhr",
            spanish: "Alboroto",
            italian: "Baraonda",
            english: "Uproar"
        }
    },
    vcreate: {
        num: 557,
        metacategory: 7,
        names: {
            japanese: "Ｖジェネレート",
            korean: "V제너레이트",
            french: "Coup Victoire",
            german: "V-Generator",
            spanish: "V de Fuego",
            italian: "Generatore V",
            english: "V-create"
        }
    },
    vacuumwave: {
        num: 410,
        metacategory: 0,
        names: {
            japanese: "しんくうは",
            korean: "진공파",
            french: "Onde Vide",
            german: "Vakuumwelle",
            spanish: "Onda Vacío",
            italian: "Vuotonda",
            english: "Vacuum Wave"
        }
    },
    venomdrench: {
        num: 599,
        metacategory: 2,
        names: {
            japanese: "ベノムトラップ",
            korean: "베놈트랩",
            french: "Piège de Venin",
            german: "Giftfalle",
            spanish: "Trampa Venenosa",
            italian: "Velenotrappola",
            english: "Venom Drench"
        }
    },
    venoshock: {
        num: 474,
        metacategory: 0,
        names: {
            japanese: "ベノムショック",
            korean: "베놈쇼크",
            french: "Choc Venin",
            german: "Giftschock",
            spanish: "Carga Tóxica",
            italian: "Velenoshock",
            english: "Venoshock"
        }
    },
    vicegrip: {
        num: 11,
        metacategory: 0,
        names: {
            japanese: "はさむ",
            korean: "찝기",
            french: "Force Poigne",
            german: "Klammer",
            spanish: "Agarre",
            italian: "Presa",
            english: "Vice Grip"
        }
    },
    vinewhip: {
        num: 22,
        metacategory: 0,
        names: {
            japanese: "つるのムチ",
            korean: "덩굴채찍",
            french: "Fouet Lianes",
            german: "Rankenhieb",
            spanish: "Látigo Cepa",
            italian: "Frustata",
            english: "Vine Whip"
        }
    },
    vitalthrow: {
        num: 233,
        metacategory: 0,
        names: {
            japanese: "あてみなげ",
            korean: "받아던지기",
            french: "Corps Perdu",
            german: "Überwurf",
            spanish: "Tiro Vital",
            italian: "Vitaltiro",
            english: "Vital Throw"
        }
    },
    voltswitch: {
        num: 521,
        metacategory: 0,
        names: {
            japanese: "ボルトチェンジ",
            korean: "볼트체인지",
            french: "Change Éclair",
            german: "Voltwechsel",
            spanish: "Voltiocambio",
            italian: "Invertivolt",
            english: "Volt Switch"
        }
    },
    volttackle: {
        num: 344,
        metacategory: 4,
        names: {
            japanese: "ボルテッカー",
            korean: "볼트태클",
            french: "Électacle",
            german: "Volttackle",
            spanish: "Placaje Eléc",
            italian: "Locomovolt",
            english: "Volt Tackle"
        }
    },
    wakeupslap: {
        num: 358,
        metacategory: 0,
        names: {
            japanese: "めざましビンタ",
            korean: "잠깨움뺨치기",
            french: "Réveil Forcé",
            german: "Weckruf",
            spanish: "Espabila",
            italian: "Svegliopacca",
            english: "Wake-Up Slap"
        }
    },
    watergun: {
        num: 55,
        metacategory: 0,
        names: {
            japanese: "みずでっぽう",
            korean: "물대포",
            french: "Pistolet à O",
            german: "Aquaknarre",
            spanish: "Pistola Agua",
            italian: "Pistolacqua",
            english: "Water Gun"
        }
    },
    waterpledge: {
        num: 518,
        metacategory: 0,
        names: {
            japanese: "みずのちかい",
            korean: "물의맹세",
            french: "Aire d'Eau",
            german: "Wassersäulen",
            spanish: "Voto Agua",
            italian: "Acquapatto",
            english: "Water Pledge"
        }
    },
    waterpulse: {
        num: 352,
        metacategory: 4,
        names: {
            japanese: "みずのはどう",
            korean: "물의파동",
            french: "Vibraqua",
            german: "Aquawelle",
            spanish: "Hidropulso",
            italian: "Idropulsar",
            english: "Water Pulse"
        }
    },
    watersport: {
        num: 346,
        metacategory: 10,
        names: {
            japanese: "みずあそび",
            korean: "물놀이",
            french: "Tourniquet",
            german: "Nassmacher",
            spanish: "Hidrochorro",
            italian: "Docciascudo",
            english: "Water Sport"
        }
    },
    waterspout: {
        num: 323,
        metacategory: 0,
        names: {
            japanese: "しおふき",
            korean: "해수스파우팅",
            french: "Giclédo",
            german: "Fontränen",
            spanish: "Salpicar",
            italian: "Zampillo",
            english: "Water Spout"
        }
    },
    waterfall: {
        num: 127,
        metacategory: 0,
        names: {
            japanese: "たきのぼり",
            korean: "폭포오르기",
            french: "Cascade",
            german: "Kaskade",
            spanish: "Cascada",
            italian: "Cascata",
            english: "Waterfall"
        }
    },
    watershuriken: {
        num: 594,
        metacategory: 0,
        names: {
            japanese: "みずしゅりけん",
            korean: "물수리검",
            french: "Sheauriken",
            german: "Wasser-Shuriken",
            spanish: "Shuriken de Agua",
            italian: "Acqualame",
            english: "Water Shuriken"
        }
    },
    weatherball: {
        num: 311,
        metacategory: 0,
        names: {
            japanese: "ウェザーボール",
            korean: "웨더볼",
            french: "Ball'Météo",
            german: "Meteorologe",
            spanish: "Meteorobola",
            italian: "Palla Clima",
            english: "Weather Ball"
        }
    },
    whirlpool: {
        num: 250,
        metacategory: 4,
        names: {
            japanese: "うずしお",
            korean: "바다회오리",
            french: "Siphon",
            german: "Whirlpool",
            spanish: "Torbellino",
            italian: "Mulinello",
            english: "Whirlpool"
        }
    },
    whirlwind: {
        num: 18,
        metacategory: 12,
        names: {
            japanese: "ふきとばし",
            korean: "날려버리기",
            french: "Cyclone",
            german: "Wirbelwind",
            spanish: "Remolino",
            italian: "Turbine",
            english: "Whirlwind"
        }
    },
    wideguard: {
        num: 469,
        metacategory: 11,
        names: {
            japanese: "ワイドガード",
            korean: "와이드가드",
            french: "Garde Large",
            german: "Rundumschutz",
            spanish: "Vastaguardia",
            italian: "Bodyguard",
            english: "Wide Guard"
        }
    },
    wildcharge: {
        num: 528,
        metacategory: 0,
        names: {
            japanese: "ワイルドボルト",
            korean: "와일드볼트",
            french: "Éclair Fou",
            german: "Stromstoß",
            spanish: "Voltio Cruel",
            italian: "Sprizzalampo",
            english: "Wild Charge"
        }
    },
    willowisp: {
        num: 261,
        metacategory: 1,
        names: {
            japanese: "おにび",
            korean: "도깨비불",
            french: "Feu Follet",
            german: "Irrlicht",
            spanish: "Fuego Fatuo",
            italian: "Fuocofatuo",
            english: "Will-O-Wisp"
        }
    },
    wingattack: {
        num: 17,
        metacategory: 0,
        names: {
            japanese: "つばさでうつ",
            korean: "날개치기",
            french: "Cru-Aile",
            german: "Flügelschlag",
            spanish: "Ataque Ala",
            italian: "Attacco d'Ala",
            english: "Wing Attack"
        }
    },
    wish: {
        num: 273,
        metacategory: 13,
        names: {
            japanese: "ねがいごと",
            korean: "희망사항",
            french: "Vœu",
            german: "Wunschtraum",
            spanish: "Deseo",
            italian: "Desiderio",
            english: "Wish"
        }
    },
    withdraw: {
        num: 110,
        metacategory: 2,
        names: {
            japanese: "からにこもる",
            korean: "껍질에숨기",
            french: "Repli",
            german: "Panzerschutz",
            spanish: "Refugio",
            italian: "Ritirata",
            english: "Withdraw"
        }
    },
    wonderroom: {
        num: 472,
        metacategory: 10,
        names: {
            japanese: "ワンダールーム",
            korean: "원더룸",
            french: "Zone Étrange",
            german: "Wunderraum",
            spanish: "Zona Extraña",
            italian: "Mirabilzona",
            english: "Wonder Room"
        }
    },
    woodhammer: {
        num: 452,
        metacategory: 0,
        names: {
            japanese: "ウッドハンマー",
            korean: "우드해머",
            french: "Martobois",
            german: "Holzhammer",
            spanish: "Mazazo",
            italian: "Mazzuolegno",
            english: "Wood Hammer"
        }
    },
    workup: {
        num: 526,
        metacategory: 2,
        names: {
            japanese: "ふるいたてる",
            korean: "분발",
            french: "Rengorgement",
            german: "Kraftschub",
            spanish: "Avivar",
            italian: "Cuordileone",
            english: "Work Up"
        }
    },
    worryseed: {
        num: 388,
        metacategory: 13,
        names: {
            japanese: "なやみのタネ",
            korean: "고민씨",
            french: "Soucigraine",
            german: "Sorgensamen",
            spanish: "Abatidoras",
            italian: "Affannoseme",
            english: "Worry Seed"
        }
    },
    wrap: {
        num: 35,
        metacategory: 4,
        names: {
            japanese: "まきつく",
            korean: "김밥말이",
            french: "Ligotage",
            german: "Wickel",
            spanish: "Constricción",
            italian: "Avvolgibotta",
            english: "Wrap"
        }
    },
    wringout: {
        num: 378,
        metacategory: 0,
        names: {
            japanese: "しぼりとる",
            korean: "쥐어짜기",
            french: "Essorage",
            german: "Auswringen",
            spanish: "Estrujón",
            italian: "Strizzata",
            english: "Wring Out"
        }
    },
    xscissor: {
        num: 404,
        metacategory: 0,
        names: {
            japanese: "シザークロス",
            korean: "시저크로스",
            french: "Plaie-Croix",
            german: "Kreuzschere",
            spanish: "Tijera X",
            italian: "Forbice X",
            english: "X-Scissor"
        }
    },
    yawn: {
        num: 281,
        metacategory: 1,
        names: {
            japanese: "あくび",
            korean: "하품",
            french: "Bâillement",
            german: "Gähner",
            spanish: "Bostezo",
            italian: "Sbadiglio",
            english: "Yawn"
        }
    },
    zapcannon: {
        num: 192,
        metacategory: 4,
        names: {
            japanese: "でんじほう",
            korean: "전자포",
            french: "Élecanon",
            german: "Blitzkanone",
            spanish: "Electrocañón",
            italian: "Falcecannone",
            english: "Zap Cannon"
        }
    },
    zenheadbutt: {
        num: 428,
        metacategory: 0,
        names: {
            japanese: "しねんのずつき",
            korean: "사념의박치기",
            french: "Psykoud'Boul",
            german: "Zen-Kopfstoß",
            spanish: "Cabezazo Zen",
            italian: "Cozzata Zen",
            english: "Zen Headbutt"
        }
    },
    zingzap: {
        num: 716
    },
    paleowave: {},
    shadowstrike: {},
    magikarpsrevenge: {}
}