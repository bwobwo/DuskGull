exports.natures = {
    hardy: {
        num: 1,
        decstat: 2,
        incstat: 2,
        hatesflavor: 1,
        likesflavor: 1,
        gamenum: 0
    },
    bold: {
        num: 2,
        decstat: 2,
        incstat: 3,
        hatesflavor: 1,
        likesflavor: 5,
        gamenum: 5
    },
    modest: {
        num: 3,
        decstat: 2,
        incstat: 4,
        hatesflavor: 1,
        likesflavor: 2,
        gamenum: 15
    },
    calm: {
        num: 4,
        decstat: 2,
        incstat: 5,
        hatesflavor: 1,
        likesflavor: 4,
        gamenum: 20
    },
    timid: {
        num: 5,
        decstat: 2,
        incstat: 6,
        hatesflavor: 1,
        likesflavor: 3,
        gamenum: 10
    },
    lonely: {
        num: 6,
        decstat: 3,
        incstat: 2,
        hatesflavor: 5,
        likesflavor: 1,
        gamenum: 1
    },
    docile: {
        num: 7,
        decstat: 3,
        incstat: 3,
        hatesflavor: 5,
        likesflavor: 5,
        gamenum: 6
    },
    mild: {
        num: 8,
        decstat: 3,
        incstat: 4,
        hatesflavor: 5,
        likesflavor: 2,
        gamenum: 16
    },
    gentle: {
        num: 9,
        decstat: 3,
        incstat: 5,
        hatesflavor: 5,
        likesflavor: 4,
        gamenum: 21
    },
    hasty: {
        num: 10,
        decstat: 3,
        incstat: 6,
        hatesflavor: 5,
        likesflavor: 3,
        gamenum: 11
    },
    adamant: {
        num: 11,
        decstat: 4,
        incstat: 2,
        hatesflavor: 2,
        likesflavor: 1,
        gamenum: 3
    },
    impish: {
        num: 12,
        decstat: 4,
        incstat: 3,
        hatesflavor: 2,
        likesflavor: 5,
        gamenum: 8
    },
    bashful: {
        num: 13,
        decstat: 4,
        incstat: 4,
        hatesflavor: 2,
        likesflavor: 2,
        gamenum: 18
    },
    careful: {
        num: 14,
        decstat: 4,
        incstat: 5,
        hatesflavor: 2,
        likesflavor: 4,
        gamenum: 23
    },
    rash: {
        num: 15,
        decstat: 5,
        incstat: 4,
        hatesflavor: 4,
        likesflavor: 2,
        gamenum: 19
    },
    jolly: {
        num: 16,
        decstat: 4,
        incstat: 6,
        hatesflavor: 2,
        likesflavor: 3,
        gamenum: 13
    },
    naughty: {
        num: 17,
        decstat: 5,
        incstat: 2,
        hatesflavor: 4,
        likesflavor: 1,
        gamenum: 4
    },
    lax: {
        num: 18,
        decstat: 5,
        incstat: 3,
        hatesflavor: 4,
        likesflavor: 5,
        gamenum: 9
    },
    quirky: {
        num: 19,
        decstat: 5,
        incstat: 5,
        hatesflavor: 4,
        likesflavor: 4,
        gamenum: 24
    },
    naive: {
        num: 20,
        decstat: 5,
        incstat: 6,
        hatesflavor: 4,
        likesflavor: 3,
        gamenum: 14
    },
    brave: {
        num: 21,
        decstat: 6,
        incstat: 2,
        hatesflavor: 3,
        likesflavor: 1,
        gamenum: 2
    },
    relaxed: {
        num: 22,
        decstat: 6,
        incstat: 3,
        hatesflavor: 3,
        likesflavor: 5,
        gamenum: 7
    },
    quiet: {
        num: 23,
        decstat: 6,
        incstat: 4,
        hatesflavor: 3,
        likesflavor: 2,
        gamenum: 17
    },
    sassy: {
        num: 24,
        decstat: 6,
        incstat: 5,
        hatesflavor: 3,
        likesflavor: 4,
        gamenum: 22
    },
    serious: {
        num: 25,
        decstat: 6,
        incstat: 6,
        hatesflavor: 3,
        likesflavor: 3,
        gamenum: 12
    }
}