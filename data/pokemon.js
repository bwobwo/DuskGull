
exports.pokemon = {
    bulbasaur: {
        num: 1,
        isdefault: true,
        baseExperience: 64,
        order: 1,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 2,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フシギダネ",
            romaji: "Fushigidane",
            korean: "이상해씨",
            chinese: "妙蛙種子",
            french: "Bulbizarre",
            german: "Bisasam",
            spanish: "Bulbasaur",
            italian: "Bulbasaur",
            english: "Bulbasaur"
        }
    },
    ivysaur: {
        num: 2,
        isdefault: true,
        baseExperience: 142,
        order: 2,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 3,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フシギソウ",
            romaji: "Fushigisou",
            korean: "이상해풀",
            chinese: "妙蛙草",
            french: "Herbizarre",
            german: "Bisaknosp",
            spanish: "Ivysaur",
            italian: "Ivysaur",
            english: "Ivysaur"
        }
    },
    venusaur: {
        num: 3,
        isdefault: true,
        baseExperience: 236,
        order: 3,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 5,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "フシギバナ",
            romaji: "Fushigibana",
            korean: "이상해꽃",
            chinese: "妙蛙花",
            french: "Florizarre",
            german: "Bisaflor",
            spanish: "Venusaur",
            italian: "Venusaur",
            english: "Venusaur"
        }
    },
    venusaurmega: {
        num: 3,
        isdefault: false,
        baseExperience: 281,
        order: 4
    },
    charmander: {
        num: 4,
        isdefault: true,
        baseExperience: 62,
        order: 5,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 5,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒトカゲ",
            romaji: "Hitokage",
            korean: "파이리",
            chinese: "小火龍",
            french: "Salamèche",
            german: "Glumanda",
            spanish: "Charmander",
            italian: "Charmander",
            english: "Charmander"
        }
    },
    charmeleon: {
        num: 5,
        isdefault: true,
        baseExperience: 142,
        order: 6,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 6,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "リザード",
            romaji: "Lizardo",
            korean: "리자드",
            chinese: "火恐龍",
            french: "Reptincel",
            german: "Glutexo",
            spanish: "Charmeleon",
            italian: "Charmeleon",
            english: "Charmeleon"
        }
    },
    charizard: {
        num: 6,
        isdefault: true,
        baseExperience: 240,
        order: 7,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "リザードン",
            romaji: "Lizardon",
            korean: "리자몽",
            chinese: "噴火龍",
            french: "Dracaufeu",
            german: "Glurak",
            spanish: "Charizard",
            italian: "Charizard",
            english: "Charizard"
        }
    },
    charizardmegax: {
        num: 6,
        isdefault: false,
        baseExperience: 285,
        order: 8
    },
    charizardmegay: {
        num: 6,
        isdefault: false,
        baseExperience: 285,
        order: 9
    },
    squirtle: {
        num: 7,
        isdefault: true,
        baseExperience: 63,
        order: 10,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 8,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゼニガメ",
            romaji: "Zenigame",
            korean: "꼬부기",
            chinese: "傑尼龜",
            french: "Carapuce",
            german: "Schiggy",
            spanish: "Squirtle",
            italian: "Squirtle",
            english: "Squirtle"
        }
    },
    wartortle: {
        num: 8,
        isdefault: true,
        baseExperience: 142,
        order: 11,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 9,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カメール",
            romaji: "Kameil",
            korean: "어니부기",
            chinese: "卡咪龜",
            french: "Carabaffe",
            german: "Schillok",
            spanish: "Wartortle",
            italian: "Wartortle",
            english: "Wartortle"
        }
    },
    blastoise: {
        num: 9,
        isdefault: true,
        baseExperience: 239,
        order: 12,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "カメックス",
            romaji: "Kamex",
            korean: "거북왕",
            chinese: "水箭龜",
            french: "Tortank",
            german: "Turtok",
            spanish: "Blastoise",
            italian: "Blastoise",
            english: "Blastoise"
        }
    },
    blastoisemega: {
        num: 9,
        isdefault: false,
        baseExperience: 284,
        order: 13
    },
    caterpie: {
        num: 10,
        isdefault: true,
        baseExperience: 39,
        order: 14,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 1,
        color: 5,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 11,
                trigger: "levelup",
                level: 7,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キャタピー",
            romaji: "Caterpie",
            korean: "캐터피",
            chinese: "綠毛蟲",
            french: "Chenipan",
            german: "Raupy",
            spanish: "Caterpie",
            italian: "Caterpie",
            english: "Caterpie"
        }
    },
    metapod: {
        num: 11,
        isdefault: true,
        baseExperience: 72,
        order: 15,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 1,
        color: 5,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 12,
                trigger: "levelup",
                level: 10,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "トランセル",
            romaji: "Trancell",
            korean: "단데기",
            chinese: "鐵甲蛹",
            french: "Chrysacier",
            german: "Safcon",
            spanish: "Metapod",
            italian: "Metapod",
            english: "Metapod"
        }
    },
    butterfree: {
        num: 12,
        isdefault: true,
        baseExperience: 178,
        order: 16,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 1,
        color: 9,
        shape: 13,
        genderDiff: 1,
        names: {
            japanese: "バタフリー",
            romaji: "Butterfree",
            korean: "버터플",
            chinese: "巴大蝴",
            french: "Papilusion",
            german: "Smettbo",
            spanish: "Butterfree",
            italian: "Butterfree",
            english: "Butterfree"
        }
    },
    weedle: {
        num: 13,
        isdefault: true,
        baseExperience: 39,
        order: 17,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 1,
        color: 3,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 14,
                trigger: "levelup",
                level: 7,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ビードル",
            romaji: "Beedle",
            korean: "뿔충이",
            chinese: "獨角蟲",
            french: "Aspicot",
            german: "Hornliu",
            spanish: "Weedle",
            italian: "Weedle",
            english: "Weedle"
        }
    },
    kakuna: {
        num: 14,
        isdefault: true,
        baseExperience: 72,
        order: 18,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 1,
        color: 10,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 15,
                trigger: "levelup",
                level: 10,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コクーン",
            romaji: "Cocoon",
            korean: "딱충이",
            chinese: "鐵殼昆",
            french: "Coconfort",
            german: "Kokuna",
            spanish: "Kakuna",
            italian: "Kakuna",
            english: "Kakuna"
        }
    },
    beedrill: {
        num: 15,
        isdefault: true,
        baseExperience: 178,
        order: 19,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 1,
        color: 10,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "スピアー",
            romaji: "Spear",
            korean: "독침붕",
            chinese: "大針蜂",
            french: "Dardargnan",
            german: "Bibor",
            spanish: "Beedrill",
            italian: "Beedrill",
            english: "Beedrill"
        }
    },
    beedrillmega: {
        num: 15,
        isdefault: false,
        baseExperience: 223,
        order: 20
    },
    pidgey: {
        num: 16,
        isdefault: true,
        baseExperience: 50,
        order: 21,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 1,
        color: 3,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 17,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポッポ",
            romaji: "Poppo",
            korean: "구구",
            chinese: "波波",
            french: "Roucool",
            german: "Taubsi",
            spanish: "Pidgey",
            italian: "Pidgey",
            english: "Pidgey"
        }
    },
    pidgeotto: {
        num: 17,
        isdefault: true,
        baseExperience: 122,
        order: 22,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 1,
        color: 3,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 18,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ピジョン",
            romaji: "Pigeon",
            korean: "피죤",
            chinese: "比比鳥",
            french: "Roucoups",
            german: "Tauboga",
            spanish: "Pidgeotto",
            italian: "Pidgeotto",
            english: "Pidgeotto"
        }
    },
    pidgeot: {
        num: 18,
        isdefault: true,
        baseExperience: 216,
        order: 23,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 1,
        color: 3,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ピジョット",
            romaji: "Pigeot",
            korean: "피죤투",
            chinese: "比鵰",
            french: "Roucarnage",
            german: "Tauboss",
            spanish: "Pidgeot",
            italian: "Pidgeot",
            english: "Pidgeot"
        }
    },
    pidgeotmega: {
        num: 18,
        isdefault: false,
        baseExperience: 261,
        order: 24
    },
    rattata: {
        num: 19,
        isdefault: true,
        baseExperience: 51,
        order: 25,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 1,
        color: 7,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 20,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コラッタ",
            romaji: "Koratta",
            korean: "꼬렛",
            chinese: "小拉達",
            french: "Rattata",
            german: "Rattfratz",
            spanish: "Rattata",
            italian: "Rattata",
            english: "Rattata"
        }
    },
    rattataalola: {
        num: 19,
        isdefault: false
    },
    raticate: {
        num: 20,
        isdefault: true,
        baseExperience: 145,
        order: 26,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 127,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "ラッタ",
            romaji: "Ratta",
            korean: "레트라",
            chinese: "拉達",
            french: "Rattatac",
            german: "Rattikarl",
            spanish: "Raticate",
            italian: "Raticate",
            english: "Raticate"
        }
    },
    raticatealola: {
        num: 20,
        isdefault: false
    },
    spearow: {
        num: 21,
        isdefault: true,
        baseExperience: 52,
        order: 27,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 6,
        generation: 1,
        color: 3,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 22,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "オニスズメ",
            romaji: "Onisuzume",
            korean: "깨비참",
            chinese: "烈雀",
            french: "Piafabec",
            german: "Habitak",
            spanish: "Spearow",
            italian: "Spearow",
            english: "Spearow"
        }
    },
    fearow: {
        num: 22,
        isdefault: true,
        baseExperience: 155,
        order: 28,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 6,
        generation: 1,
        color: 3,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "オニドリル",
            romaji: "Onidrill",
            korean: "깨비드릴조",
            chinese: "大嘴雀",
            french: "Rapasdepic",
            german: "Ibitak",
            spanish: "Fearow",
            italian: "Fearow",
            english: "Fearow"
        }
    },
    ekans: {
        num: 23,
        isdefault: true,
        baseExperience: 58,
        order: 29,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 1,
        color: 7,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 24,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アーボ",
            romaji: "Arbo",
            korean: "아보",
            chinese: "阿柏蛇",
            french: "Abo",
            german: "Rettan",
            spanish: "Ekans",
            italian: "Ekans",
            english: "Ekans"
        }
    },
    arbok: {
        num: 24,
        isdefault: true,
        baseExperience: 153,
        order: 30,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 3,
        generation: 1,
        color: 7,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "アーボック",
            romaji: "Arbok",
            korean: "아보크",
            chinese: "阿柏怪",
            french: "Arbok",
            german: "Arbok",
            spanish: "Arbok",
            italian: "Arbok",
            english: "Arbok"
        }
    },
    pikachu: {
        num: 25,
        isdefault: true,
        baseExperience: 112,
        order: 32,
        growthRate: 2,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 26,
                trigger: "useitem",
                itemtrigger: "thunderstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ピカチュウ",
            romaji: "Pikachu",
            korean: "피카츄",
            chinese: "皮卡丘",
            french: "Pikachu",
            german: "Pikachu",
            spanish: "Pikachu",
            italian: "Pikachu",
            english: "Pikachu"
        }
    },
    pikachucosplay: {
        num: 25,
        isdefault: false,
        baseExperience: 112,
        order: 33
    },
    pikachurockstar: {
        num: 25,
        isdefault: false,
        baseExperience: 112,
        order: 34
    },
    pikachubelle: {
        num: 25,
        isdefault: false,
        baseExperience: 112,
        order: 35
    },
    pikachupopstar: {
        num: 25,
        isdefault: false,
        baseExperience: 112,
        order: 36
    },
    pikachuphd: {
        num: 25,
        isdefault: false,
        baseExperience: 112,
        order: 37
    },
    pikachulibre: {
        num: 25,
        isdefault: false,
        baseExperience: 112,
        order: 38
    },
    pikachuoriginal: {
        num: 25,
        isdefault: false
    },
    pikachuhoenn: {
        num: 25,
        isdefault: false
    },
    pikachusinnoh: {
        num: 25,
        isdefault: false
    },
    pikachuunova: {
        num: 25,
        isdefault: false
    },
    pikachukalos: {
        num: 25,
        isdefault: false
    },
    pikachualola: {
        num: 25,
        isdefault: false
    },
    raichu: {
        num: 26,
        isdefault: true,
        baseExperience: 218,
        order: 39,
        growthRate: 2,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 1,
        color: 10,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ライチュウ",
            romaji: "Raichu",
            korean: "라이츄",
            chinese: "雷丘",
            french: "Raichu",
            german: "Raichu",
            spanish: "Raichu",
            italian: "Raichu",
            english: "Raichu"
        }
    },
    raichualola: {
        num: 26,
        isdefault: false
    },
    sandshrew: {
        num: 27,
        isdefault: true,
        baseExperience: 60,
        order: 40,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 6,
        generation: 1,
        color: 10,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 28,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "サンド",
            romaji: "Sand",
            korean: "모래두지",
            chinese: "穿山鼠",
            french: "Sabelette",
            german: "Sandan",
            spanish: "Sandshrew",
            italian: "Sandshrew",
            english: "Sandshrew"
        }
    },
    sandshrewalola: {
        num: 27,
        isdefault: false
    },
    sandslash: {
        num: 28,
        isdefault: true,
        baseExperience: 158,
        order: 41,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 6,
        generation: 1,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "サンドパン",
            romaji: "Sandpan",
            korean: "고지",
            chinese: "穿山王",
            french: "Sablaireau",
            german: "Sandamer",
            spanish: "Sandslash",
            italian: "Sandslash",
            english: "Sandslash"
        }
    },
    sandslashalola: {
        num: 28,
        isdefault: false
    },
    nidoranf: {
        num: 29,
        isdefault: true,
        baseExperience: 55,
        order: 42,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 235,
        habitat: 3,
        generation: 1,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 30,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニドラン♀",
            romaji: "Nidoran♀",
            korean: "니드런♀",
            chinese: "尼多蘭",
            french: "Nidoran♀",
            german: "Nidoran♀",
            spanish: "Nidoran♀",
            italian: "Nidoran♀",
            english: "Nidoran♀"
        }
    },
    nidorina: {
        num: 30,
        isdefault: true,
        baseExperience: 128,
        order: 43,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 1,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 31,
                trigger: "useitem",
                itemtrigger: "moonstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニドリーナ",
            romaji: "Nidorina",
            korean: "니드리나",
            chinese: "尼多娜",
            french: "Nidorina",
            german: "Nidorina",
            spanish: "Nidorina",
            italian: "Nidorina",
            english: "Nidorina"
        }
    },
    nidoqueen: {
        num: 31,
        isdefault: true,
        baseExperience: 227,
        order: 44,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ニドクイン",
            romaji: "Nidoqueen",
            korean: "니드퀸",
            chinese: "尼多后",
            french: "Nidoqueen",
            german: "Nidoqueen",
            spanish: "Nidoqueen",
            italian: "Nidoqueen",
            english: "Nidoqueen"
        }
    },
    nidoranm: {
        num: 32,
        isdefault: true,
        baseExperience: 55,
        order: 45,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 235,
        habitat: 3,
        generation: 1,
        color: 7,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 33,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニドラン♂",
            romaji: "Nidoran♂",
            korean: "니드런♂",
            chinese: "尼多朗",
            french: "Nidoran♂",
            german: "Nidoran♂",
            spanish: "Nidoran♂",
            italian: "Nidoran♂",
            english: "Nidoran♂"
        }
    },
    nidorino: {
        num: 33,
        isdefault: true,
        baseExperience: 128,
        order: 46,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 1,
        color: 7,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 34,
                trigger: "useitem",
                itemtrigger: "moonstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニドリーノ",
            romaji: "Nidorino",
            korean: "니드리노",
            chinese: "尼多力諾",
            french: "Nidorino",
            german: "Nidorino",
            spanish: "Nidorino",
            italian: "Nidorino",
            english: "Nidorino"
        }
    },
    nidoking: {
        num: 34,
        isdefault: true,
        baseExperience: 227,
        order: 47,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ニドキング",
            romaji: "Nidoking",
            korean: "니드킹",
            chinese: "尼多王",
            french: "Nidoking",
            german: "Nidoking",
            spanish: "Nidoking",
            italian: "Nidoking",
            english: "Nidoking"
        }
    },
    clefairy: {
        num: 35,
        isdefault: true,
        baseExperience: 113,
        order: 49,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 150,
        habitat: 4,
        generation: 1,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 36,
                trigger: "useitem",
                itemtrigger: "moonstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ピッピ",
            romaji: "Pippi",
            korean: "삐삐",
            chinese: "皮皮",
            french: "Mélofée",
            german: "Piepi",
            spanish: "Clefairy",
            italian: "Clefairy",
            english: "Clefairy"
        }
    },
    clefable: {
        num: 36,
        isdefault: true,
        baseExperience: 217,
        order: 50,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 25,
        habitat: 4,
        generation: 1,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ピクシー",
            romaji: "Pixy",
            korean: "픽시",
            chinese: "皮可西",
            french: "Mélodelfe",
            german: "Pixi",
            spanish: "Clefable",
            italian: "Clefable",
            english: "Clefable"
        }
    },
    vulpix: {
        num: 37,
        isdefault: true,
        baseExperience: 60,
        order: 51,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 38,
                trigger: "useitem",
                itemtrigger: "firestone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ロコン",
            romaji: "Rokon",
            korean: "식스테일",
            chinese: "六尾",
            french: "Goupix",
            german: "Vulpix",
            spanish: "Vulpix",
            italian: "Vulpix",
            english: "Vulpix"
        }
    },
    vulpixalola: {
        num: 37,
        isdefault: false
    },
    ninetales: {
        num: 38,
        isdefault: true,
        baseExperience: 177,
        order: 52,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 3,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "キュウコン",
            romaji: "Kyukon",
            korean: "나인테일",
            chinese: "九尾",
            french: "Feunard",
            german: "Vulnona",
            spanish: "Ninetales",
            italian: "Ninetales",
            english: "Ninetales"
        }
    },
    ninetalesalola: {
        num: 38,
        isdefault: false
    },
    jigglypuff: {
        num: 39,
        isdefault: true,
        baseExperience: 95,
        order: 54,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 170,
        habitat: 3,
        generation: 1,
        color: 6,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 40,
                trigger: "useitem",
                itemtrigger: "moonstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "プリン",
            romaji: "Purin",
            korean: "푸린",
            chinese: "胖丁",
            french: "Rondoudou",
            german: "Pummeluff",
            spanish: "Jigglypuff",
            italian: "Jigglypuff",
            english: "Jigglypuff"
        }
    },
    wigglytuff: {
        num: 40,
        isdefault: true,
        baseExperience: 196,
        order: 55,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 50,
        habitat: 3,
        generation: 1,
        color: 6,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "プクリン",
            romaji: "Pukurin",
            korean: "푸크린",
            chinese: "胖可丁",
            french: "Grodoudou",
            german: "Knuddeluff",
            spanish: "Wigglytuff",
            italian: "Wigglytuff",
            english: "Wigglytuff"
        }
    },
    zubat: {
        num: 41,
        isdefault: true,
        baseExperience: 49,
        order: 56,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 1,
        generation: 1,
        color: 7,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 42,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ズバット",
            romaji: "Zubat",
            korean: "주뱃",
            chinese: "超音蝠",
            french: "Nosferapti",
            german: "Zubat",
            spanish: "Zubat",
            italian: "Zubat",
            english: "Zubat"
        }
    },
    golbat: {
        num: 42,
        isdefault: true,
        baseExperience: 159,
        order: 57,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 1,
        generation: 1,
        color: 7,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 169,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴルバット",
            romaji: "Golbat",
            korean: "골뱃",
            chinese: "大嘴蝠",
            french: "Nosferalto",
            german: "Golbat",
            spanish: "Golbat",
            italian: "Golbat",
            english: "Golbat"
        }
    },
    oddish: {
        num: 43,
        isdefault: true,
        baseExperience: 64,
        order: 59,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 1,
        color: 2,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 44,
                trigger: "levelup",
                level: 21,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ナゾノクサ",
            romaji: "Nazonokusa",
            korean: "뚜벅쵸",
            chinese: "走路草",
            french: "Mystherbe",
            german: "Myrapla",
            spanish: "Oddish",
            italian: "Oddish",
            english: "Oddish"
        }
    },
    gloom: {
        num: 44,
        isdefault: true,
        baseExperience: 138,
        order: 60,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 1,
        color: 2,
        shape: 12,
        genderDiff: 1,
        evomethods: [
            {
                num: 45,
                trigger: "useitem",
                itemtrigger: "leafstone",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 182,
                trigger: "useitem",
                itemtrigger: "sunstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クサイハナ",
            romaji: "Kusaihana",
            korean: "냄새꼬",
            chinese: "臭臭花",
            french: "Ortide",
            german: "Duflor",
            spanish: "Gloom",
            italian: "Gloom",
            english: "Gloom"
        }
    },
    vileplume: {
        num: 45,
        isdefault: true,
        baseExperience: 221,
        order: 61,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 8,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ラフレシア",
            romaji: "Ruffresia",
            korean: "라플레시아",
            chinese: "霸王花",
            french: "Rafflesia",
            german: "Giflor",
            spanish: "Vileplume",
            italian: "Vileplume",
            english: "Vileplume"
        }
    },
    paras: {
        num: 46,
        isdefault: true,
        baseExperience: 57,
        order: 63,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 1,
        color: 8,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 47,
                trigger: "levelup",
                level: 24,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "パラス",
            romaji: "Paras",
            korean: "파라스",
            chinese: "派拉斯",
            french: "Paras",
            german: "Paras",
            spanish: "Paras",
            italian: "Paras",
            english: "Paras"
        }
    },
    parasect: {
        num: 47,
        isdefault: true,
        baseExperience: 142,
        order: 64,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 1,
        color: 8,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "パラセクト",
            romaji: "Parasect",
            korean: "파라섹트",
            chinese: "派拉斯特",
            french: "Parasect",
            german: "Parasek",
            spanish: "Parasect",
            italian: "Parasect",
            english: "Parasect"
        }
    },
    venonat: {
        num: 48,
        isdefault: true,
        baseExperience: 61,
        order: 65,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 1,
        color: 7,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 49,
                trigger: "levelup",
                level: 31,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コンパン",
            romaji: "Kongpang",
            korean: "콘팡",
            chinese: "毛球",
            french: "Mimitoss",
            german: "Bluzuk",
            spanish: "Venonat",
            italian: "Venonat",
            english: "Venonat"
        }
    },
    venomoth: {
        num: 49,
        isdefault: true,
        baseExperience: 158,
        order: 66,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 1,
        color: 7,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "モルフォン",
            romaji: "Morphon",
            korean: "도나리",
            chinese: "末入蛾",
            french: "Aéromite",
            german: "Omot",
            spanish: "Venomoth",
            italian: "Venomoth",
            english: "Venomoth"
        }
    },
    diglett: {
        num: 50,
        isdefault: true,
        baseExperience: 53,
        order: 67,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 1,
        generation: 1,
        color: 3,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 51,
                trigger: "levelup",
                level: 26,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ディグダ",
            romaji: "Digda",
            korean: "디그다",
            chinese: "地鼠",
            french: "Taupiqueur",
            german: "Digda",
            spanish: "Diglett",
            italian: "Diglett",
            english: "Diglett"
        }
    },
    diglettalola: {
        num: 50,
        isdefault: false
    },
    dugtrio: {
        num: 51,
        isdefault: true,
        baseExperience: 142,
        order: 68,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 50,
        habitat: 1,
        generation: 1,
        color: 3,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "ダグトリオ",
            romaji: "Dugtrio",
            korean: "닥트리오",
            chinese: "三地鼠",
            french: "Triopikeur",
            german: "Digdri",
            spanish: "Dugtrio",
            italian: "Dugtrio",
            english: "Dugtrio"
        }
    },
    dugtrioalola: {
        num: 51,
        isdefault: false
    },
    meowth: {
        num: 52,
        isdefault: true,
        baseExperience: 58,
        order: 69,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 8,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 53,
                trigger: "levelup",
                level: 28,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニャース",
            romaji: "Nyarth",
            korean: "나옹",
            chinese: "喵喵",
            french: "Miaouss",
            german: "Mauzi",
            spanish: "Meowth",
            italian: "Meowth",
            english: "Meowth"
        }
    },
    meowthalola: {
        num: 52,
        isdefault: false
    },
    persian: {
        num: 53,
        isdefault: true,
        baseExperience: 154,
        order: 70,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 8,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ペルシアン",
            romaji: "Persian",
            korean: "페르시온",
            chinese: "貓老大",
            french: "Persian",
            german: "Snobilikat",
            spanish: "Persian",
            italian: "Persian",
            english: "Persian"
        }
    },
    persianalola: {
        num: 53,
        isdefault: false
    },
    psyduck: {
        num: 54,
        isdefault: true,
        baseExperience: 64,
        order: 71,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 9,
        generation: 1,
        color: 10,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 55,
                trigger: "levelup",
                level: 33,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コダック",
            romaji: "Koduck",
            korean: "고라파덕",
            chinese: "可達鴨",
            french: "Psykokwak",
            german: "Enton",
            spanish: "Psyduck",
            italian: "Psyduck",
            english: "Psyduck"
        }
    },
    golduck: {
        num: 55,
        isdefault: true,
        baseExperience: 175,
        order: 72,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ゴルダック",
            romaji: "Golduck",
            korean: "골덕",
            chinese: "哥達鴨",
            french: "Akwakwak",
            german: "Entoron",
            spanish: "Golduck",
            italian: "Golduck",
            english: "Golduck"
        }
    },
    mankey: {
        num: 56,
        isdefault: true,
        baseExperience: 61,
        order: 73,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 57,
                trigger: "levelup",
                level: 28,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マンキー",
            romaji: "Mankey",
            korean: "망키",
            chinese: "猴怪",
            french: "Férosinge",
            german: "Menki",
            spanish: "Mankey",
            italian: "Mankey",
            english: "Mankey"
        }
    },
    primeape: {
        num: 57,
        isdefault: true,
        baseExperience: 159,
        order: 74,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "オコリザル",
            romaji: "Okorizaru",
            korean: "성원숭",
            chinese: "火爆猴",
            french: "Colossinge",
            german: "Rasaff",
            spanish: "Primeape",
            italian: "Primeape",
            english: "Primeape"
        }
    },
    growlithe: {
        num: 58,
        isdefault: true,
        baseExperience: 70,
        order: 75,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 59,
                trigger: "useitem",
                itemtrigger: "firestone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ガーディ",
            romaji: "Gardie",
            korean: "가디",
            chinese: "卡蒂狗",
            french: "Caninos",
            german: "Fukano",
            spanish: "Growlithe",
            italian: "Growlithe",
            english: "Growlithe"
        }
    },
    arcanine: {
        num: 59,
        isdefault: true,
        baseExperience: 194,
        order: 76,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ウインディ",
            romaji: "Windie",
            korean: "윈디",
            chinese: "風速狗",
            french: "Arcanin",
            german: "Arkani",
            spanish: "Arcanine",
            italian: "Arcanine",
            english: "Arcanine"
        }
    },
    poliwag: {
        num: 60,
        isdefault: true,
        baseExperience: 60,
        order: 77,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 61,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニョロモ",
            romaji: "Nyoromo",
            korean: "발챙이",
            chinese: "蚊香蝌蚪",
            french: "Ptitard",
            german: "Quapsel",
            spanish: "Poliwag",
            italian: "Poliwag",
            english: "Poliwag"
        }
    },
    poliwhirl: {
        num: 61,
        isdefault: true,
        baseExperience: 135,
        order: 78,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 62,
                trigger: "useitem",
                itemtrigger: "waterstone",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 186,
                trigger: "trade",
                helditem: 198,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニョロゾ",
            romaji: "Nyorozo",
            korean: "슈륙챙이",
            chinese: "蚊香蛙",
            french: "Têtarte",
            german: "Quaputzi",
            spanish: "Poliwhirl",
            italian: "Poliwhirl",
            english: "Poliwhirl"
        }
    },
    poliwrath: {
        num: 62,
        isdefault: true,
        baseExperience: 230,
        order: 79,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ニョロボン",
            romaji: "Nyorobon",
            korean: "강챙이",
            chinese: "快泳蛙",
            french: "Tartard",
            german: "Quappo",
            spanish: "Poliwrath",
            italian: "Poliwrath",
            english: "Poliwrath"
        }
    },
    abra: {
        num: 63,
        isdefault: true,
        baseExperience: 62,
        order: 81,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 8,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 64,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ケーシィ",
            romaji: "Casey",
            korean: "캐이시",
            chinese: "凱西",
            french: "Abra",
            german: "Abra",
            spanish: "Abra",
            italian: "Abra",
            english: "Abra"
        }
    },
    kadabra: {
        num: 64,
        isdefault: true,
        baseExperience: 140,
        order: 82,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 100,
        habitat: 8,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 65,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ユンゲラー",
            romaji: "Yungerer",
            korean: "윤겔라",
            chinese: "勇吉拉",
            french: "Kadabra",
            german: "Kadabra",
            spanish: "Kadabra",
            italian: "Kadabra",
            english: "Kadabra"
        }
    },
    alakazam: {
        num: 65,
        isdefault: true,
        baseExperience: 225,
        order: 83,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 50,
        habitat: 8,
        generation: 1,
        color: 3,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "フーディン",
            romaji: "Foodin",
            korean: "후딘",
            chinese: "胡地",
            french: "Alakazam",
            german: "Simsala",
            spanish: "Alakazam",
            italian: "Alakazam",
            english: "Alakazam"
        }
    },
    alakazammega: {
        num: 65,
        isdefault: false,
        baseExperience: 266,
        order: 84
    },
    machop: {
        num: 66,
        isdefault: true,
        baseExperience: 61,
        order: 85,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: 4,
        generation: 1,
        color: 4,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 67,
                trigger: "levelup",
                level: 28,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ワンリキー",
            romaji: "Wanriky",
            korean: "알통몬",
            chinese: "腕力",
            french: "Machoc",
            german: "Machollo",
            spanish: "Machop",
            italian: "Machop",
            english: "Machop"
        }
    },
    machoke: {
        num: 67,
        isdefault: true,
        baseExperience: 142,
        order: 86,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 4,
        generation: 1,
        color: 4,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 68,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴーリキー",
            romaji: "Goriky",
            korean: "근육몬",
            chinese: "豪力",
            french: "Machopeur",
            german: "Maschock",
            spanish: "Machoke",
            italian: "Machoke",
            english: "Machoke"
        }
    },
    machamp: {
        num: 68,
        isdefault: true,
        baseExperience: 227,
        order: 87,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 4,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "カイリキー",
            romaji: "Kairiky",
            korean: "괴력몬",
            chinese: "怪力",
            french: "Mackogneur",
            german: "Machomei",
            spanish: "Machamp",
            italian: "Machamp",
            english: "Machamp"
        }
    },
    bellsprout: {
        num: 69,
        isdefault: true,
        baseExperience: 60,
        order: 88,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 1,
        color: 5,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 70,
                trigger: "levelup",
                level: 21,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マダツボミ",
            romaji: "Madatsubomi",
            korean: "모다피",
            chinese: "喇叭芽",
            french: "Chétiflor",
            german: "Knofensa",
            spanish: "Bellsprout",
            italian: "Bellsprout",
            english: "Bellsprout"
        }
    },
    weepinbell: {
        num: 70,
        isdefault: true,
        baseExperience: 137,
        order: 89,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 1,
        color: 5,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 71,
                trigger: "useitem",
                itemtrigger: "leafstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ウツドン",
            romaji: "Utsudon",
            korean: "우츠동",
            chinese: "口呆花",
            french: "Boustiflor",
            german: "Ultrigaria",
            spanish: "Weepinbell",
            italian: "Weepinbell",
            english: "Weepinbell"
        }
    },
    victreebel: {
        num: 71,
        isdefault: true,
        baseExperience: 221,
        order: 90,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 1,
        color: 5,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ウツボット",
            romaji: "Utsubot",
            korean: "우츠보트",
            chinese: "大食花",
            french: "Empiflor",
            german: "Sarzenia",
            spanish: "Victreebel",
            italian: "Victreebel",
            english: "Victreebel"
        }
    },
    tentacool: {
        num: 72,
        isdefault: true,
        baseExperience: 67,
        order: 91,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 10,
        genderDiff: 0,
        evomethods: [
            {
                num: 73,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "メノクラゲ",
            romaji: "Menokurage",
            korean: "왕눈해",
            chinese: "瑪瑙水母",
            french: "Tentacool",
            german: "Tentacha",
            spanish: "Tentacool",
            italian: "Tentacool",
            english: "Tentacool"
        }
    },
    tentacruel: {
        num: 73,
        isdefault: true,
        baseExperience: 180,
        order: 92,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "ドククラゲ",
            romaji: "Dokukurage",
            korean: "독파리",
            chinese: "毒刺水母",
            french: "Tentacruel",
            german: "Tentoxa",
            spanish: "Tentacruel",
            italian: "Tentacruel",
            english: "Tentacruel"
        }
    },
    geodude: {
        num: 74,
        isdefault: true,
        baseExperience: 60,
        order: 93,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 75,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "イシツブテ",
            romaji: "Isitsubute",
            korean: "꼬마돌",
            chinese: "小拳石",
            french: "Racaillou",
            german: "Kleinstein",
            spanish: "Geodude",
            italian: "Geodude",
            english: "Geodude"
        }
    },
    geodudealola: {
        num: 74,
        isdefault: false
    },
    graveler: {
        num: 75,
        isdefault: true,
        baseExperience: 137,
        order: 94,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 76,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴローン",
            romaji: "Golone",
            korean: "데구리",
            chinese: "隆隆石",
            french: "Gravalanch",
            german: "Georok",
            spanish: "Graveler",
            italian: "Graveler",
            english: "Graveler"
        }
    },
    graveleralola: {
        num: 75,
        isdefault: false
    },
    golem: {
        num: 76,
        isdefault: true,
        baseExperience: 223,
        order: 95,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ゴローニャ",
            romaji: "Golonya",
            korean: "딱구리",
            chinese: "隆隆岩",
            french: "Grolem",
            german: "Geowaz",
            spanish: "Golem",
            italian: "Golem",
            english: "Golem"
        }
    },
    golemalola: {
        num: 76,
        isdefault: false
    },
    ponyta: {
        num: 77,
        isdefault: true,
        baseExperience: 82,
        order: 96,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 3,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 78,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポニータ",
            romaji: "Ponyta",
            korean: "포니타",
            chinese: "小火馬",
            french: "Ponyta",
            german: "Ponita",
            spanish: "Ponyta",
            italian: "Ponyta",
            english: "Ponyta"
        }
    },
    rapidash: {
        num: 78,
        isdefault: true,
        baseExperience: 175,
        order: 97,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 3,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ギャロップ",
            romaji: "Gallop",
            korean: "날쌩마",
            chinese: "烈焰馬",
            french: "Galopa",
            german: "Gallopa",
            spanish: "Rapidash",
            italian: "Rapidash",
            english: "Rapidash"
        }
    },
    slowpoke: {
        num: 79,
        isdefault: true,
        baseExperience: 63,
        order: 98,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 9,
        generation: 1,
        color: 6,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 80,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 199,
                trigger: "trade",
                helditem: 198,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤドン",
            romaji: "Yadon",
            korean: "야돈",
            chinese: "呆呆獸",
            french: "Ramoloss",
            german: "Flegmon",
            spanish: "Slowpoke",
            italian: "Slowpoke",
            english: "Slowpoke"
        }
    },
    slowbro: {
        num: 80,
        isdefault: true,
        baseExperience: 172,
        order: 99,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 9,
        generation: 1,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ヤドラン",
            romaji: "Yadoran",
            korean: "야도란",
            chinese: "呆河馬",
            french: "Flagadoss",
            german: "Lahmus",
            spanish: "Slowbro",
            italian: "Slowbro",
            english: "Slowbro"
        }
    },
    slowbromega: {
        num: 80,
        isdefault: false,
        baseExperience: 207,
        order: 100
    },
    magnemite: {
        num: 81,
        isdefault: true,
        baseExperience: 65,
        order: 102,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 6,
        generation: 1,
        color: 4,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 82,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コイル",
            romaji: "Coil",
            korean: "코일",
            chinese: "小磁怪",
            french: "Magnéti",
            german: "Magnetilo",
            spanish: "Magnemite",
            italian: "Magnemite",
            english: "Magnemite"
        }
    },
    magneton: {
        num: 82,
        isdefault: true,
        baseExperience: 163,
        order: 103,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 6,
        generation: 1,
        color: 4,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 462,
                trigger: "levelup",
                location: 10,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 462,
                trigger: "levelup",
                location: 379,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 462,
                trigger: "levelup",
                location: 629,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "レアコイル",
            romaji: "Rarecoil",
            korean: "레어코일",
            chinese: "三合一磁怪",
            french: "Magnéton",
            german: "Magneton",
            spanish: "Magneton",
            italian: "Magneton",
            english: "Magneton"
        }
    },
    farfetchd: {
        num: 83,
        isdefault: true,
        baseExperience: 123,
        order: 105,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "カモネギ",
            romaji: "Kamonegi",
            korean: "파오리",
            chinese: "大蔥鴨",
            french: "Canarticho",
            german: "Porenta",
            spanish: "Farfetch'd",
            italian: "Farfetch'd",
            english: "Farfetch'd"
        }
    },
    doduo: {
        num: 84,
        isdefault: true,
        baseExperience: 62,
        order: 106,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 7,
        genderDiff: 1,
        evomethods: [
            {
                num: 85,
                trigger: "levelup",
                level: 31,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドードー",
            romaji: "Dodo",
            korean: "두두",
            chinese: "嘟嘟",
            french: "Doduo",
            german: "Dodu",
            spanish: "Doduo",
            italian: "Doduo",
            english: "Doduo"
        }
    },
    dodrio: {
        num: 85,
        isdefault: true,
        baseExperience: 161,
        order: 107,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 7,
        genderDiff: 1,
        names: {
            japanese: "ドードリオ",
            romaji: "Dodorio",
            korean: "두트리오",
            chinese: "嘟嘟利",
            french: "Dodrio",
            german: "Dodri",
            spanish: "Dodrio",
            italian: "Dodrio",
            english: "Dodrio"
        }
    },
    seel: {
        num: 86,
        isdefault: true,
        baseExperience: 65,
        order: 108,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 7,
        generation: 1,
        color: 9,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 87,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "パウワウ",
            romaji: "Pawou",
            korean: "쥬쥬",
            chinese: "小海獅",
            french: "Otaria",
            german: "Jurob",
            spanish: "Seel",
            italian: "Seel",
            english: "Seel"
        }
    },
    dewgong: {
        num: 87,
        isdefault: true,
        baseExperience: 166,
        order: 109,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 7,
        generation: 1,
        color: 9,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ジュゴン",
            romaji: "Jugon",
            korean: "쥬레곤",
            chinese: "白海獅",
            french: "Lamantine",
            german: "Jugong",
            spanish: "Dewgong",
            italian: "Dewgong",
            english: "Dewgong"
        }
    },
    grimer: {
        num: 88,
        isdefault: true,
        baseExperience: 65,
        order: 110,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 8,
        generation: 1,
        color: 7,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 89,
                trigger: "levelup",
                level: 38,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ベトベター",
            romaji: "Betbeter",
            korean: "질퍽이",
            chinese: "臭泥",
            french: "Tadmorv",
            german: "Sleima",
            spanish: "Grimer",
            italian: "Grimer",
            english: "Grimer"
        }
    },
    grimeralola: {
        num: 88,
        isdefault: false
    },
    muk: {
        num: 89,
        isdefault: true,
        baseExperience: 175,
        order: 111,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 8,
        generation: 1,
        color: 7,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ベトベトン",
            romaji: "Betbeton",
            korean: "질뻐기",
            chinese: "臭臭泥",
            french: "Grotadmorv",
            german: "Sleimok",
            spanish: "Muk",
            italian: "Muk",
            english: "Muk"
        }
    },
    mukalola: {
        num: 89,
        isdefault: false
    },
    shellder: {
        num: 90,
        isdefault: true,
        baseExperience: 61,
        order: 112,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 7,
        generation: 1,
        color: 7,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 91,
                trigger: "useitem",
                itemtrigger: "waterstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シェルダー",
            romaji: "Shellder",
            korean: "셀러",
            chinese: "大舌貝",
            french: "Kokiyas",
            german: "Muschas",
            spanish: "Shellder",
            italian: "Shellder",
            english: "Shellder"
        }
    },
    cloyster: {
        num: 91,
        isdefault: true,
        baseExperience: 184,
        order: 113,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 1,
        color: 7,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "パルシェン",
            romaji: "Parshen",
            korean: "파르셀",
            chinese: "鐵甲貝",
            french: "Crustabri",
            german: "Austos",
            spanish: "Cloyster",
            italian: "Cloyster",
            english: "Cloyster"
        }
    },
    gastly: {
        num: 92,
        isdefault: true,
        baseExperience: 62,
        order: 114,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 1,
        generation: 1,
        color: 7,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 93,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴース",
            romaji: "Ghos",
            korean: "고오스",
            chinese: "鬼斯",
            french: "Fantominus",
            german: "Nebulak",
            spanish: "Gastly",
            italian: "Gastly",
            english: "Gastly"
        }
    },
    haunter: {
        num: 93,
        isdefault: true,
        baseExperience: 142,
        order: 115,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 1,
        generation: 1,
        color: 7,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 94,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴースト",
            romaji: "Ghost",
            korean: "고우스트",
            chinese: "鬼斯通",
            french: "Spectrum",
            german: "Alpollo",
            spanish: "Haunter",
            italian: "Haunter",
            english: "Haunter"
        }
    },
    gengar: {
        num: 94,
        isdefault: true,
        baseExperience: 225,
        order: 116,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 1,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ゲンガー",
            romaji: "Gangar",
            korean: "팬텀",
            chinese: "耿鬼",
            french: "Ectoplasma",
            german: "Gengar",
            spanish: "Gengar",
            italian: "Gengar",
            english: "Gengar"
        }
    },
    gengarmega: {
        num: 94,
        isdefault: false,
        baseExperience: 270,
        order: 117
    },
    onix: {
        num: 95,
        isdefault: true,
        baseExperience: 77,
        order: 118,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 1,
        color: 4,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 208,
                trigger: "trade",
                helditem: 210,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "イワーク",
            romaji: "Iwark",
            korean: "롱스톤",
            chinese: "大岩蛇",
            french: "Onix",
            german: "Onix",
            spanish: "Onix",
            italian: "Onix",
            english: "Onix"
        }
    },
    drowzee: {
        num: 96,
        isdefault: true,
        baseExperience: 66,
        order: 121,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 3,
        generation: 1,
        color: 10,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 97,
                trigger: "levelup",
                level: 26,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "スリープ",
            romaji: "Sleep",
            korean: "슬리프",
            chinese: "素利普",
            french: "Soporifik",
            german: "Traumato",
            spanish: "Drowzee",
            italian: "Drowzee",
            english: "Drowzee"
        }
    },
    hypno: {
        num: 97,
        isdefault: true,
        baseExperience: 169,
        order: 122,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 3,
        generation: 1,
        color: 10,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "スリーパー",
            romaji: "Sleeper",
            korean: "슬리퍼",
            chinese: "素利拍",
            french: "Hypnomade",
            german: "Hypno",
            spanish: "Hypno",
            italian: "Hypno",
            english: "Hypno"
        }
    },
    krabby: {
        num: 98,
        isdefault: true,
        baseExperience: 65,
        order: 123,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 9,
        generation: 1,
        color: 8,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 99,
                trigger: "levelup",
                level: 28,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クラブ",
            romaji: "Crab",
            korean: "크랩",
            chinese: "大鉗蟹",
            french: "Krabby",
            german: "Krabby",
            spanish: "Krabby",
            italian: "Krabby",
            english: "Krabby"
        }
    },
    kingler: {
        num: 99,
        isdefault: true,
        baseExperience: 166,
        order: 124,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 9,
        generation: 1,
        color: 8,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "キングラー",
            romaji: "Kingler",
            korean: "킹크랩",
            chinese: "巨鉗蟹",
            french: "Krabboss",
            german: "Kingler",
            spanish: "Kingler",
            italian: "Kingler",
            english: "Kingler"
        }
    },
    voltorb: {
        num: 100,
        isdefault: true,
        baseExperience: 66,
        order: 125,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 8,
        generation: 1,
        color: 8,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 101,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ビリリダマ",
            romaji: "Biriridama",
            korean: "찌리리공",
            chinese: "雷電球",
            french: "Voltorbe",
            german: "Voltobal",
            spanish: "Voltorb",
            italian: "Voltorb",
            english: "Voltorb"
        }
    },
    electrode: {
        num: 101,
        isdefault: true,
        baseExperience: 168,
        order: 126,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 8,
        generation: 1,
        color: 8,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "マルマイン",
            romaji: "Marumine",
            korean: "붐볼",
            chinese: "頑皮彈",
            french: "Électrode",
            german: "Lektrobal",
            spanish: "Electrode",
            italian: "Electrode",
            english: "Electrode"
        }
    },
    exeggcute: {
        num: 102,
        isdefault: true,
        baseExperience: 65,
        order: 127,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 2,
        generation: 1,
        color: 6,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 103,
                trigger: "useitem",
                itemtrigger: "leafstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タマタマ",
            romaji: "Tamatama",
            korean: "아라리",
            chinese: "蛋蛋",
            french: "Noeunoeuf",
            german: "Owei",
            spanish: "Exeggcute",
            italian: "Exeggcute",
            english: "Exeggcute"
        }
    },
    exeggutor: {
        num: 103,
        isdefault: true,
        baseExperience: 182,
        order: 128,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 1,
        color: 10,
        shape: 7,
        genderDiff: 0,
        names: {
            japanese: "ナッシー",
            romaji: "Nassy",
            korean: "나시",
            chinese: "椰蛋樹",
            french: "Noadkoko",
            german: "Kokowei",
            spanish: "Exeggutor",
            italian: "Exeggutor",
            english: "Exeggutor"
        }
    },
    exeggutoralola: {
        num: 103,
        isdefault: false
    },
    cubone: {
        num: 104,
        isdefault: true,
        baseExperience: 64,
        order: 129,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 105,
                trigger: "levelup",
                level: 28,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カラカラ",
            romaji: "Karakara",
            korean: "탕구리",
            chinese: "可拉可拉",
            french: "Osselait",
            german: "Tragosso",
            spanish: "Cubone",
            italian: "Cubone",
            english: "Cubone"
        }
    },
    marowak: {
        num: 105,
        isdefault: true,
        baseExperience: 149,
        order: 130,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 4,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ガラガラ",
            romaji: "Garagara",
            korean: "텅구리",
            chinese: "嘎啦嘎啦",
            french: "Ossatueur",
            german: "Knogga",
            spanish: "Marowak",
            italian: "Marowak",
            english: "Marowak"
        }
    },
    marowakalola: {
        num: 105,
        isdefault: false
    },
    hitmonlee: {
        num: 106,
        isdefault: true,
        baseExperience: 159,
        order: 132,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "サワムラー",
            romaji: "Sawamular",
            korean: "시라소몬",
            chinese: "沙瓦郎",
            french: "Kicklee",
            german: "Kicklee",
            spanish: "Hitmonlee",
            italian: "Hitmonlee",
            english: "Hitmonlee"
        }
    },
    hitmonchan: {
        num: 107,
        isdefault: true,
        baseExperience: 159,
        order: 133,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "エビワラー",
            romaji: "Ebiwalar",
            korean: "홍수몬",
            chinese: "艾比郎",
            french: "Tygnon",
            german: "Nockchan",
            spanish: "Hitmonchan",
            italian: "Hitmonchan",
            english: "Hitmonchan"
        }
    },
    lickitung: {
        num: 108,
        isdefault: true,
        baseExperience: 77,
        order: 135,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 463,
                trigger: "levelup",
                move: 205,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ベロリンガ",
            romaji: "Beroringa",
            korean: "내루미",
            chinese: "大舌頭",
            french: "Excelangue",
            german: "Schlurp",
            spanish: "Lickitung",
            italian: "Lickitung",
            english: "Lickitung"
        }
    },
    koffing: {
        num: 109,
        isdefault: true,
        baseExperience: 68,
        order: 137,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 8,
        generation: 1,
        color: 7,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 110,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドガース",
            romaji: "Dogars",
            korean: "또가스",
            chinese: "瓦斯彈",
            french: "Smogo",
            german: "Smogon",
            spanish: "Koffing",
            italian: "Koffing",
            english: "Koffing"
        }
    },
    weezing: {
        num: 110,
        isdefault: true,
        baseExperience: 172,
        order: 138,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 8,
        generation: 1,
        color: 7,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "マタドガス",
            romaji: "Matadogas",
            korean: "또도가스",
            chinese: "雙彈瓦斯",
            french: "Smogogo",
            german: "Smogmog",
            spanish: "Weezing",
            italian: "Weezing",
            english: "Weezing"
        }
    },
    rhyhorn: {
        num: 111,
        isdefault: true,
        baseExperience: 69,
        order: 139,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 6,
        generation: 1,
        color: 4,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 112,
                trigger: "levelup",
                level: 42,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "サイホーン",
            romaji: "Sihorn",
            korean: "뿔카노",
            chinese: "鐵甲犀牛",
            french: "Rhinocorne",
            german: "Rihorn",
            spanish: "Rhyhorn",
            italian: "Rhyhorn",
            english: "Rhyhorn"
        }
    },
    rhydon: {
        num: 112,
        isdefault: true,
        baseExperience: 170,
        order: 140,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 6,
        generation: 1,
        color: 4,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 464,
                trigger: "trade",
                helditem: 298,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "サイドン",
            romaji: "Sidon",
            korean: "코뿌리",
            chinese: "鐵甲暴龍",
            french: "Rhinoféros",
            german: "Rizeros",
            spanish: "Rhydon",
            italian: "Rhydon",
            english: "Rhydon"
        }
    },
    chansey: {
        num: 113,
        isdefault: true,
        baseExperience: 395,
        order: 143,
        growthRate: 3,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 30,
        habitat: 8,
        generation: 1,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 242,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ラッキー",
            romaji: "Lucky",
            korean: "럭키",
            chinese: "吉利蛋",
            french: "Leveinard",
            german: "Chaneira",
            spanish: "Chansey",
            italian: "Chansey",
            english: "Chansey"
        }
    },
    tangela: {
        num: 114,
        isdefault: true,
        baseExperience: 87,
        order: 145,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 2,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 465,
                trigger: "levelup",
                move: 246,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "モンジャラ",
            romaji: "Monjara",
            korean: "덩쿠리",
            chinese: "蔓藤怪",
            french: "Saquedeneu",
            german: "Tangela",
            spanish: "Tangela",
            italian: "Tangela",
            english: "Tangela"
        }
    },
    kangaskhan: {
        num: 115,
        isdefault: true,
        baseExperience: 172,
        order: 147,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ガルーラ",
            romaji: "Garura",
            korean: "캥카",
            chinese: "袋龍",
            french: "Kangourex",
            german: "Kangama",
            spanish: "Kangaskhan",
            italian: "Kangaskhan",
            english: "Kangaskhan"
        }
    },
    kangaskhanmega: {
        num: 115,
        isdefault: false,
        baseExperience: 207,
        order: 148
    },
    horsea: {
        num: 116,
        isdefault: true,
        baseExperience: 59,
        order: 149,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 117,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タッツー",
            romaji: "Tattu",
            korean: "쏘드라",
            chinese: "墨海馬",
            french: "Hypotrempe",
            german: "Seeper",
            spanish: "Horsea",
            italian: "Horsea",
            english: "Horsea"
        }
    },
    seadra: {
        num: 117,
        isdefault: true,
        baseExperience: 154,
        order: 150,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 230,
                trigger: "trade",
                helditem: 212,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シードラ",
            romaji: "Seadra",
            korean: "시드라",
            chinese: "海刺龍",
            french: "Hypocéan",
            german: "Seemon",
            spanish: "Seadra",
            italian: "Seadra",
            english: "Seadra"
        }
    },
    goldeen: {
        num: 118,
        isdefault: true,
        baseExperience: 64,
        order: 152,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 9,
        generation: 1,
        color: 8,
        shape: 3,
        genderDiff: 1,
        evomethods: [
            {
                num: 119,
                trigger: "levelup",
                level: 33,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "トサキント",
            romaji: "Tosakinto",
            korean: "콘치",
            chinese: "角金魚",
            french: "Poissirène",
            german: "Goldini",
            spanish: "Goldeen",
            italian: "Goldeen",
            english: "Goldeen"
        }
    },
    seaking: {
        num: 119,
        isdefault: true,
        baseExperience: 158,
        order: 153,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 9,
        generation: 1,
        color: 8,
        shape: 3,
        genderDiff: 1,
        names: {
            japanese: "アズマオウ",
            romaji: "Azumao",
            korean: "왕콘치",
            chinese: "金魚王",
            french: "Poissoroy",
            german: "Golking",
            spanish: "Seaking",
            italian: "Seaking",
            english: "Seaking"
        }
    },
    staryu: {
        num: 120,
        isdefault: true,
        baseExperience: 68,
        order: 154,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 7,
        generation: 1,
        color: 3,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 121,
                trigger: "useitem",
                itemtrigger: "waterstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒトデマン",
            romaji: "Hitodeman",
            korean: "별가사리",
            chinese: "海星星",
            french: "Stari",
            german: "Sterndu",
            spanish: "Staryu",
            italian: "Staryu",
            english: "Staryu"
        }
    },
    starmie: {
        num: 121,
        isdefault: true,
        baseExperience: 182,
        order: 155,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 1,
        color: 7,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "スターミー",
            romaji: "Starmie",
            korean: "아쿠스타",
            chinese: "寶石海星",
            french: "Staross",
            german: "Starmie",
            spanish: "Starmie",
            italian: "Starmie",
            english: "Starmie"
        }
    },
    mrmime: {
        num: 122,
        isdefault: true,
        baseExperience: 161,
        order: 157,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 6,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "バリヤード",
            romaji: "Barrierd",
            korean: "마임맨",
            chinese: "吸盤魔偶",
            french: "M. Mime",
            german: "Pantimos",
            spanish: "Mr. Mime",
            italian: "Mr. Mime",
            english: "Mr. Mime"
        }
    },
    scyther: {
        num: 123,
        isdefault: true,
        baseExperience: 100,
        order: 158,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 5,
        shape: 13,
        genderDiff: 1,
        evomethods: [
            {
                num: 212,
                trigger: "trade",
                helditem: 210,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ストライク",
            romaji: "Strike",
            korean: "스라크",
            chinese: "飛天螳螂",
            french: "Insécateur",
            german: "Sichlor",
            spanish: "Scyther",
            italian: "Scyther",
            english: "Scyther"
        }
    },
    jynx: {
        num: 124,
        isdefault: true,
        baseExperience: 159,
        order: 162,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 8,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ルージュラ",
            romaji: "Rougela",
            korean: "루주라",
            chinese: "迷唇姐",
            french: "Lippoutou",
            german: "Rossana",
            spanish: "Jynx",
            italian: "Jynx",
            english: "Jynx"
        }
    },
    electabuzz: {
        num: 125,
        isdefault: true,
        baseExperience: 172,
        order: 164,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 10,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 466,
                trigger: "trade",
                helditem: 299,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "エレブー",
            romaji: "Eleboo",
            korean: "에레브",
            chinese: "電擊獸",
            french: "Élektek",
            german: "Elektek",
            spanish: "Electabuzz",
            italian: "Electabuzz",
            english: "Electabuzz"
        }
    },
    magmar: {
        num: 126,
        isdefault: true,
        baseExperience: 173,
        order: 167,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 467,
                trigger: "trade",
                helditem: 300,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ブーバー",
            romaji: "Boober",
            korean: "마그마",
            chinese: "鴨嘴火龍",
            french: "Magmar",
            german: "Magmar",
            spanish: "Magmar",
            italian: "Magmar",
            english: "Magmar"
        }
    },
    pinsir: {
        num: 127,
        isdefault: true,
        baseExperience: 175,
        order: 169,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 1,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "カイロス",
            romaji: "Kailios",
            korean: "쁘사이저",
            chinese: "大甲",
            french: "Scarabrute",
            german: "Pinsir",
            spanish: "Pinsir",
            italian: "Pinsir",
            english: "Pinsir"
        }
    },
    pinsirmega: {
        num: 127,
        isdefault: false,
        baseExperience: 210,
        order: 170
    },
    tauros: {
        num: 128,
        isdefault: true,
        baseExperience: 172,
        order: 171,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 1,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ケンタロス",
            romaji: "Kentauros",
            korean: "켄타로스",
            chinese: "肯泰羅",
            french: "Tauros",
            german: "Tauros",
            spanish: "Tauros",
            italian: "Tauros",
            english: "Tauros"
        }
    },
    magikarp: {
        num: 129,
        isdefault: true,
        baseExperience: 40,
        order: 172,
        growthRate: 1,
        hatchCounter: 5,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 9,
        generation: 1,
        color: 8,
        shape: 3,
        genderDiff: 1,
        evomethods: [
            {
                num: 130,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コイキング",
            romaji: "Koiking",
            korean: "잉어킹",
            chinese: "鯉魚王",
            french: "Magicarpe",
            german: "Karpador",
            spanish: "Magikarp",
            italian: "Magikarp",
            english: "Magikarp"
        }
    },
    gyarados: {
        num: 130,
        isdefault: true,
        baseExperience: 189,
        order: 173,
        growthRate: 1,
        hatchCounter: 5,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 2,
        genderDiff: 1,
        names: {
            japanese: "ギャラドス",
            romaji: "Gyarados",
            korean: "갸라도스",
            chinese: "暴鯉龍",
            french: "Léviator",
            german: "Garados",
            spanish: "Gyarados",
            italian: "Gyarados",
            english: "Gyarados"
        }
    },
    gyaradosmega: {
        num: 130,
        isdefault: false,
        baseExperience: 224,
        order: 174
    },
    lapras: {
        num: 131,
        isdefault: true,
        baseExperience: 187,
        order: 175,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ラプラス",
            romaji: "Laplace",
            korean: "라프라스",
            chinese: "乘龍",
            french: "Lokhlass",
            german: "Lapras",
            spanish: "Lapras",
            italian: "Lapras",
            english: "Lapras"
        }
    },
    ditto: {
        num: 132,
        isdefault: true,
        baseExperience: 101,
        order: 176,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 35,
        habitat: 8,
        generation: 1,
        color: 7,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "メタモン",
            romaji: "Metamon",
            korean: "메타몽",
            chinese: "百變怪",
            french: "Métamorph",
            german: "Ditto",
            spanish: "Ditto",
            italian: "Ditto",
            english: "Ditto"
        }
    },
    eevee: {
        num: 133,
        isdefault: true,
        baseExperience: 65,
        order: 177,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 134,
                trigger: "useitem",
                itemtrigger: "waterstone",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 135,
                trigger: "useitem",
                itemtrigger: "thunderstone",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 136,
                trigger: "useitem",
                itemtrigger: "firestone",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 196,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 197,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            },
            {
                num: 470,
                trigger: "levelup",
                location: 8,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 471,
                trigger: "levelup",
                location: 48,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 470,
                trigger: "levelup",
                location: 375,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 471,
                trigger: "levelup",
                location: 380,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 700,
                trigger: "levelup",
                moveType: 18,
                affection: 2,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 470,
                trigger: "levelup",
                location: 650,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 471,
                trigger: "levelup",
                location: 640,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "イーブイ",
            romaji: "Eievui",
            korean: "이브이",
            chinese: "伊布",
            french: "Évoli",
            german: "Evoli",
            spanish: "Eevee",
            italian: "Eevee",
            english: "Eevee"
        }
    },
    vaporeon: {
        num: 134,
        isdefault: true,
        baseExperience: 184,
        order: 178,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "シャワーズ",
            romaji: "Showers",
            korean: "샤미드",
            chinese: "水精靈",
            french: "Aquali",
            german: "Aquana",
            spanish: "Vaporeon",
            italian: "Vaporeon",
            english: "Vaporeon"
        }
    },
    jolteon: {
        num: 135,
        isdefault: true,
        baseExperience: 184,
        order: 179,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "サンダース",
            romaji: "Thunders",
            korean: "쥬피썬더",
            chinese: "雷精靈",
            french: "Voltali",
            german: "Blitza",
            spanish: "Jolteon",
            italian: "Jolteon",
            english: "Jolteon"
        }
    },
    flareon: {
        num: 136,
        isdefault: true,
        baseExperience: 184,
        order: 180,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 8,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ブースター",
            romaji: "Booster",
            korean: "부스터",
            chinese: "火精靈",
            french: "Pyroli",
            german: "Flamara",
            spanish: "Flareon",
            italian: "Flareon",
            english: "Flareon"
        }
    },
    porygon: {
        num: 137,
        isdefault: true,
        baseExperience: 79,
        order: 186,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 1,
        color: 6,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 233,
                trigger: "trade",
                helditem: 229,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポリゴン",
            romaji: "Porygon",
            korean: "폴리곤",
            chinese: "3D龍",
            french: "Porygon",
            german: "Porygon",
            spanish: "Porygon",
            italian: "Porygon",
            english: "Porygon"
        }
    },
    omanyte: {
        num: 138,
        isdefault: true,
        baseExperience: 71,
        order: 189,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 10,
        genderDiff: 0,
        evomethods: [
            {
                num: 139,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "オムナイト",
            romaji: "Omnite",
            korean: "암나이트",
            chinese: "菊石獸",
            french: "Amonita",
            german: "Amonitas",
            spanish: "Omanyte",
            italian: "Omanyte",
            english: "Omanyte"
        }
    },
    omastar: {
        num: 139,
        isdefault: true,
        baseExperience: 173,
        order: 190,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 1,
        color: 2,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "オムスター",
            romaji: "Omstar",
            korean: "암스타",
            chinese: "多刺菊石獸",
            french: "Amonistar",
            german: "Amoroso",
            spanish: "Omastar",
            italian: "Omastar",
            english: "Omastar"
        }
    },
    kabuto: {
        num: 140,
        isdefault: true,
        baseExperience: 71,
        order: 191,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 1,
        color: 3,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 141,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カブト",
            romaji: "Kabuto",
            korean: "투구",
            chinese: "化石盔",
            french: "Kabuto",
            german: "Kabuto",
            spanish: "Kabuto",
            italian: "Kabuto",
            english: "Kabuto"
        }
    },
    kabutops: {
        num: 141,
        isdefault: true,
        baseExperience: 173,
        order: 192,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "カブトプス",
            romaji: "Kabutops",
            korean: "투구푸스",
            chinese: "鐮刀盔",
            french: "Kabutops",
            german: "Kabutops",
            spanish: "Kabutops",
            italian: "Kabutops",
            english: "Kabutops"
        }
    },
    aerodactyl: {
        num: 142,
        isdefault: true,
        baseExperience: 180,
        order: 193,
        growthRate: 1,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 1,
        color: 7,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "プテラ",
            romaji: "Ptera",
            korean: "프테라",
            chinese: "化石翼龍",
            french: "Ptéra",
            german: "Aerodactyl",
            spanish: "Aerodactyl",
            italian: "Aerodactyl",
            english: "Aerodactyl"
        }
    },
    aerodactylmega: {
        num: 142,
        isdefault: false,
        baseExperience: 215,
        order: 194
    },
    snorlax: {
        num: 143,
        isdefault: true,
        baseExperience: 189,
        order: 196,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: 4,
        generation: 1,
        color: 1,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "カビゴン",
            romaji: "Kabigon",
            korean: "잠만보",
            chinese: "卡比獸",
            french: "Ronflex",
            german: "Relaxo",
            spanish: "Snorlax",
            italian: "Snorlax",
            english: "Snorlax"
        }
    },
    articuno: {
        num: 144,
        isdefault: true,
        baseExperience: 261,
        order: 197,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 5,
        generation: 1,
        color: 2,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "フリーザー",
            romaji: "Freezer",
            korean: "프리져",
            chinese: "急凍鳥",
            french: "Artikodin",
            german: "Arktos",
            spanish: "Articuno",
            italian: "Articuno",
            english: "Articuno"
        }
    },
    zapdos: {
        num: 145,
        isdefault: true,
        baseExperience: 261,
        order: 198,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 5,
        generation: 1,
        color: 10,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "サンダー",
            romaji: "Thunder",
            korean: "썬더",
            chinese: "閃電鳥",
            french: "Électhor",
            german: "Zapdos",
            spanish: "Zapdos",
            italian: "Zapdos",
            english: "Zapdos"
        }
    },
    moltres: {
        num: 146,
        isdefault: true,
        baseExperience: 261,
        order: 199,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 5,
        generation: 1,
        color: 10,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ファイヤー",
            romaji: "Fire",
            korean: "파이어",
            chinese: "火焰鳥",
            french: "Sulfura",
            german: "Lavados",
            spanish: "Moltres",
            italian: "Moltres",
            english: "Moltres"
        }
    },
    dratini: {
        num: 147,
        isdefault: true,
        baseExperience: 60,
        order: 200,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 148,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミニリュウ",
            romaji: "Miniryu",
            korean: "미뇽",
            chinese: "迷你龍",
            french: "Minidraco",
            german: "Dratini",
            spanish: "Dratini",
            italian: "Dratini",
            english: "Dratini"
        }
    },
    dragonair: {
        num: 148,
        isdefault: true,
        baseExperience: 147,
        order: 201,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 2,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 149,
                trigger: "levelup",
                level: 55,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハクリュー",
            romaji: "Hakuryu",
            korean: "신뇽",
            chinese: "哈克龍",
            french: "Draco",
            german: "Dragonir",
            spanish: "Dragonair",
            italian: "Dragonair",
            english: "Dragonair"
        }
    },
    dragonite: {
        num: 149,
        isdefault: true,
        baseExperience: 270,
        order: 202,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 9,
        generation: 1,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "カイリュー",
            romaji: "Kairyu",
            korean: "망나뇽",
            chinese: "快龍",
            french: "Dracolosse",
            german: "Dragoran",
            spanish: "Dragonite",
            italian: "Dragonite",
            english: "Dragonite"
        }
    },
    mewtwo: {
        num: 150,
        isdefault: true,
        baseExperience: 306,
        order: 203,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: 5,
        generation: 1,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ミュウツー",
            romaji: "Mewtwo",
            korean: "뮤츠",
            chinese: "超夢",
            french: "Mewtwo",
            german: "Mewtu",
            spanish: "Mewtwo",
            italian: "Mewtwo",
            english: "Mewtwo"
        }
    },
    mewtwomegax: {
        num: 150,
        isdefault: false,
        baseExperience: 351,
        order: 204
    },
    mewtwomegay: {
        num: 150,
        isdefault: false,
        baseExperience: 351,
        order: 205
    },
    mew: {
        num: 151,
        isdefault: true,
        baseExperience: 270,
        order: 206,
        growthRate: 4,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 45,
        habitat: 5,
        generation: 1,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ミュウ",
            romaji: "Mew",
            korean: "뮤",
            chinese: "夢幻",
            french: "Mew",
            german: "Mew",
            spanish: "Mew",
            italian: "Mew",
            english: "Mew"
        }
    },
    chikorita: {
        num: 152,
        isdefault: true,
        baseExperience: 64,
        order: 207,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 153,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チコリータ",
            romaji: "Chicorita",
            korean: "치코리타",
            chinese: "菊草葉",
            french: "Germignon",
            german: "Endivie",
            spanish: "Chikorita",
            italian: "Chikorita",
            english: "Chikorita"
        }
    },
    bayleef: {
        num: 153,
        isdefault: true,
        baseExperience: 142,
        order: 208,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 154,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ベイリーフ",
            romaji: "Bayleaf",
            korean: "베이리프",
            chinese: "月桂葉",
            french: "Macronium",
            german: "Lorblatt",
            spanish: "Bayleef",
            italian: "Bayleef",
            english: "Bayleef"
        }
    },
    meganium: {
        num: 154,
        isdefault: true,
        baseExperience: 236,
        order: 209,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 5,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "メガニウム",
            romaji: "Meganium",
            korean: "메가니움",
            chinese: "大菊花",
            french: "Méganium",
            german: "Meganie",
            spanish: "Meganium",
            italian: "Meganium",
            english: "Meganium"
        }
    },
    cyndaquil: {
        num: 155,
        isdefault: true,
        baseExperience: 62,
        order: 210,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 156,
                trigger: "levelup",
                level: 14,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒノアラシ",
            romaji: "Hinoarashi",
            korean: "브케인",
            chinese: "火球鼠",
            french: "Héricendre",
            german: "Feurigel",
            spanish: "Cyndaquil",
            italian: "Cyndaquil",
            english: "Cyndaquil"
        }
    },
    quilava: {
        num: 156,
        isdefault: true,
        baseExperience: 142,
        order: 211,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 157,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マグマラシ",
            romaji: "Magmarashi",
            korean: "마그케인",
            chinese: "火岩鼠",
            french: "Feurisson",
            german: "Igelavar",
            spanish: "Quilava",
            italian: "Quilava",
            english: "Quilava"
        }
    },
    typhlosion: {
        num: 157,
        isdefault: true,
        baseExperience: 240,
        order: 212,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "バクフーン",
            romaji: "Bakphoon",
            korean: "블레이범",
            chinese: "火暴獸",
            french: "Typhlosion",
            german: "Tornupto",
            spanish: "Typhlosion",
            italian: "Typhlosion",
            english: "Typhlosion"
        }
    },
    totodile: {
        num: 158,
        isdefault: true,
        baseExperience: 63,
        order: 213,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 159,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ワニノコ",
            romaji: "Waninoko",
            korean: "리아코",
            chinese: "小鋸鱷",
            french: "Kaiminus",
            german: "Karnimani",
            spanish: "Totodile",
            italian: "Totodile",
            english: "Totodile"
        }
    },
    croconaw: {
        num: 159,
        isdefault: true,
        baseExperience: 142,
        order: 214,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 160,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アリゲイツ",
            romaji: "Alligates",
            korean: "엘리게이",
            chinese: "藍鱷",
            french: "Crocrodil",
            german: "Tyracroc",
            spanish: "Croconaw",
            italian: "Croconaw",
            english: "Croconaw"
        }
    },
    feraligatr: {
        num: 160,
        isdefault: true,
        baseExperience: 239,
        order: 215,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "オーダイル",
            romaji: "Ordile",
            korean: "장크로다일",
            chinese: "大力鱷",
            french: "Aligatueur",
            german: "Impergator",
            spanish: "Feraligatr",
            italian: "Feraligatr",
            english: "Feraligatr"
        }
    },
    sentret: {
        num: 161,
        isdefault: true,
        baseExperience: 43,
        order: 216,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 2,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 162,
                trigger: "levelup",
                level: 15,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "オタチ",
            romaji: "Otachi",
            korean: "꼬리선",
            chinese: "尾立",
            french: "Fouinette",
            german: "Wiesor",
            spanish: "Sentret",
            italian: "Sentret",
            english: "Sentret"
        }
    },
    furret: {
        num: 162,
        isdefault: true,
        baseExperience: 145,
        order: 217,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 3,
        generation: 2,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "オオタチ",
            romaji: "Ootachi",
            korean: "다꼬리",
            chinese: "大尾立",
            french: "Fouinar",
            german: "Wiesenior",
            spanish: "Furret",
            italian: "Furret",
            english: "Furret"
        }
    },
    hoothoot: {
        num: 163,
        isdefault: true,
        baseExperience: 52,
        order: 218,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 2,
        color: 3,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 164,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ホーホー",
            romaji: "Hoho",
            korean: "부우부",
            chinese: "咕咕",
            french: "Hoothoot",
            german: "Hoothoot",
            spanish: "Hoothoot",
            italian: "Hoothoot",
            english: "Hoothoot"
        }
    },
    noctowl: {
        num: 164,
        isdefault: true,
        baseExperience: 155,
        order: 219,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 2,
        generation: 2,
        color: 3,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ヨルノズク",
            romaji: "Yorunozuku",
            korean: "야부엉",
            chinese: "貓頭夜鷹",
            french: "Noarfang",
            german: "Noctuh",
            spanish: "Noctowl",
            italian: "Noctowl",
            english: "Noctowl"
        }
    },
    ledyba: {
        num: 165,
        isdefault: true,
        baseExperience: 53,
        order: 220,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 2,
        color: 8,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 166,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "レディバ",
            romaji: "Rediba",
            korean: "레디바",
            chinese: "芭瓢蟲",
            french: "Coxy",
            german: "Ledyba",
            spanish: "Ledyba",
            italian: "Ledyba",
            english: "Ledyba"
        }
    },
    ledian: {
        num: 166,
        isdefault: true,
        baseExperience: 137,
        order: 221,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 2,
        generation: 2,
        color: 8,
        shape: 9,
        genderDiff: 1,
        names: {
            japanese: "レディアン",
            romaji: "Redian",
            korean: "레디안",
            chinese: "安瓢蟲",
            french: "Coxyclaque",
            german: "Ledian",
            spanish: "Ledian",
            italian: "Ledian",
            english: "Ledian"
        }
    },
    spinarak: {
        num: 167,
        isdefault: true,
        baseExperience: 50,
        order: 222,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 2,
        color: 5,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 168,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "イトマル",
            romaji: "Itomaru",
            korean: "페이검",
            chinese: "線球",
            french: "Mimigal",
            german: "Webarak",
            spanish: "Spinarak",
            italian: "Spinarak",
            english: "Spinarak"
        }
    },
    ariados: {
        num: 168,
        isdefault: true,
        baseExperience: 137,
        order: 223,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 2,
        generation: 2,
        color: 8,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "アリアドス",
            romaji: "Ariados",
            korean: "아리아도스",
            chinese: "阿利多斯",
            french: "Migalos",
            german: "Ariados",
            spanish: "Ariados",
            italian: "Ariados",
            english: "Ariados"
        }
    },
    crobat: {
        num: 169,
        isdefault: true,
        baseExperience: 241,
        order: 58,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 1,
        generation: 2,
        color: 7,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "クロバット",
            romaji: "Crobat",
            korean: "크로뱃",
            chinese: "叉字蝠",
            french: "Nostenfer",
            german: "Iksbat",
            spanish: "Crobat",
            italian: "Crobat",
            english: "Crobat"
        }
    },
    chinchou: {
        num: 170,
        isdefault: true,
        baseExperience: 66,
        order: 224,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 7,
        generation: 2,
        color: 2,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 171,
                trigger: "levelup",
                level: 27,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チョンチー",
            romaji: "Chonchie",
            korean: "초라기",
            chinese: "燈籠魚",
            french: "Loupio",
            german: "Lampi",
            spanish: "Chinchou",
            italian: "Chinchou",
            english: "Chinchou"
        }
    },
    lanturn: {
        num: 171,
        isdefault: true,
        baseExperience: 161,
        order: 225,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 7,
        generation: 2,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ランターン",
            romaji: "Lantern",
            korean: "랜턴",
            chinese: "電燈怪",
            french: "Lanturn",
            german: "Lanturn",
            spanish: "Lanturn",
            italian: "Lanturn",
            english: "Lanturn"
        }
    },
    pichu: {
        num: 172,
        isdefault: true,
        baseExperience: 41,
        order: 31,
        growthRate: 2,
        hatchCounter: 10,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 2,
        color: 10,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 25,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ピチュー",
            romaji: "Pichu",
            korean: "피츄",
            chinese: "皮丘",
            french: "Pichu",
            german: "Pichu",
            spanish: "Pichu",
            italian: "Pichu",
            english: "Pichu"
        }
    },
    pichuspikyeared: {
        num: 172,
        isdefault: false
    },
    cleffa: {
        num: 173,
        isdefault: true,
        baseExperience: 44,
        order: 48,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 1,
        baseHappiness: 140,
        captureRate: 150,
        habitat: 4,
        generation: 2,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 35,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ピィ",
            romaji: "Py",
            korean: "삐",
            chinese: "皮寶寶",
            french: "Mélo",
            german: "Pii",
            spanish: "Cleffa",
            italian: "Cleffa",
            english: "Cleffa"
        }
    },
    igglybuff: {
        num: 174,
        isdefault: true,
        baseExperience: 42,
        order: 53,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 170,
        habitat: 3,
        generation: 2,
        color: 6,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 39,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ププリン",
            romaji: "Pupurin",
            korean: "푸푸린",
            chinese: "寶寶丁",
            french: "Toudoudou",
            german: "Fluffeluff",
            spanish: "Igglybuff",
            italian: "Igglybuff",
            english: "Igglybuff"
        }
    },
    togepi: {
        num: 175,
        isdefault: true,
        baseExperience: 49,
        order: 226,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 2,
        color: 9,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 176,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "トゲピー",
            romaji: "Togepy",
            korean: "토게피",
            chinese: "波克比",
            french: "Togepi",
            german: "Togepi",
            spanish: "Togepi",
            italian: "Togepi",
            english: "Togepi"
        }
    },
    togetic: {
        num: 176,
        isdefault: true,
        baseExperience: 142,
        order: 227,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 2,
        color: 9,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 468,
                trigger: "useitem",
                itemtrigger: "shinystone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "トゲチック",
            romaji: "Togechick",
            korean: "토게틱",
            chinese: "波克基古",
            french: "Togetic",
            german: "Togetic",
            spanish: "Togetic",
            italian: "Togetic",
            english: "Togetic"
        }
    },
    natu: {
        num: 177,
        isdefault: true,
        baseExperience: 64,
        order: 229,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 2,
        color: 5,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 178,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ネイティ",
            romaji: "Naty",
            korean: "네이티",
            chinese: "天然雀",
            french: "Natu",
            german: "Natu",
            spanish: "Natu",
            italian: "Natu",
            english: "Natu"
        }
    },
    xatu: {
        num: 178,
        isdefault: true,
        baseExperience: 165,
        order: 230,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 2,
        color: 5,
        shape: 9,
        genderDiff: 1,
        names: {
            japanese: "ネイティオ",
            romaji: "Natio",
            korean: "네이티오",
            chinese: "天然鳥",
            french: "Xatu",
            german: "Xatu",
            spanish: "Xatu",
            italian: "Xatu",
            english: "Xatu"
        }
    },
    mareep: {
        num: 179,
        isdefault: true,
        baseExperience: 56,
        order: 231,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 235,
        habitat: 3,
        generation: 2,
        color: 9,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 180,
                trigger: "levelup",
                level: 15,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "メリープ",
            romaji: "Merriep",
            korean: "메리프",
            chinese: "咩利羊",
            french: "Wattouat",
            german: "Voltilamm",
            spanish: "Mareep",
            italian: "Mareep",
            english: "Mareep"
        }
    },
    flaaffy: {
        num: 180,
        isdefault: true,
        baseExperience: 128,
        order: 232,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 2,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 181,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "モココ",
            romaji: "Mokoko",
            korean: "보송송",
            chinese: "綿綿",
            french: "Lainergie",
            german: "Waaty",
            spanish: "Flaaffy",
            italian: "Flaaffy",
            english: "Flaaffy"
        }
    },
    ampharos: {
        num: 181,
        isdefault: true,
        baseExperience: 230,
        order: 233,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "デンリュウ",
            romaji: "Denryu",
            korean: "전룡",
            chinese: "電龍",
            french: "Pharamp",
            german: "Ampharos",
            spanish: "Ampharos",
            italian: "Ampharos",
            english: "Ampharos"
        }
    },
    ampharosmega: {
        num: 181,
        isdefault: false,
        baseExperience: 275,
        order: 234
    },
    bellossom: {
        num: 182,
        isdefault: true,
        baseExperience: 221,
        order: 62,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 5,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "キレイハナ",
            romaji: "Kireihana",
            korean: "아르코",
            chinese: "美麗花",
            french: "Joliflor",
            german: "Blubella",
            spanish: "Bellossom",
            italian: "Bellossom",
            english: "Bellossom"
        }
    },
    marill: {
        num: 183,
        isdefault: true,
        baseExperience: 88,
        order: 236,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 184,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マリル",
            romaji: "Maril",
            korean: "마릴",
            chinese: "瑪力露",
            french: "Marill",
            german: "Marill",
            spanish: "Marill",
            italian: "Marill",
            english: "Marill"
        }
    },
    azumarill: {
        num: 184,
        isdefault: true,
        baseExperience: 189,
        order: 237,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "マリルリ",
            romaji: "Marilli",
            korean: "마릴리",
            chinese: "瑪力露麗",
            french: "Azumarill",
            german: "Azumarill",
            spanish: "Azumarill",
            italian: "Azumarill",
            english: "Azumarill"
        }
    },
    sudowoodo: {
        num: 185,
        isdefault: true,
        baseExperience: 144,
        order: 239,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 65,
        habitat: 2,
        generation: 2,
        color: 3,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ウソッキー",
            romaji: "Usokkie",
            korean: "꼬지모",
            chinese: "胡說樹",
            french: "Simularbre",
            german: "Mogelbaum",
            spanish: "Sudowoodo",
            italian: "Sudowoodo",
            english: "Sudowoodo"
        }
    },
    politoed: {
        num: 186,
        isdefault: true,
        baseExperience: 225,
        order: 80,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 2,
        color: 5,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ニョロトノ",
            romaji: "Nyorotono",
            korean: "왕구리",
            chinese: "牛蛙君",
            french: "Tarpaud",
            german: "Quaxo",
            spanish: "Politoed",
            italian: "Politoed",
            english: "Politoed"
        }
    },
    hoppip: {
        num: 187,
        isdefault: true,
        baseExperience: 50,
        order: 240,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 2,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 188,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハネッコ",
            romaji: "Hanecco",
            korean: "통통코",
            chinese: "毽子草",
            french: "Granivol",
            german: "Hoppspross",
            spanish: "Hoppip",
            italian: "Hoppip",
            english: "Hoppip"
        }
    },
    skiploom: {
        num: 188,
        isdefault: true,
        baseExperience: 119,
        order: 241,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 2,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 189,
                trigger: "levelup",
                level: 27,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポポッコ",
            romaji: "Popocco",
            korean: "두코",
            chinese: "毽子花",
            french: "Floravol",
            german: "Hubelupf",
            spanish: "Skiploom",
            italian: "Skiploom",
            english: "Skiploom"
        }
    },
    jumpluff: {
        num: 189,
        isdefault: true,
        baseExperience: 207,
        order: 242,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ワタッコ",
            romaji: "Watacco",
            korean: "솜솜코",
            chinese: "毽子綿",
            french: "Cotovol",
            german: "Papungha",
            spanish: "Jumpluff",
            italian: "Jumpluff",
            english: "Jumpluff"
        }
    },
    aipom: {
        num: 190,
        isdefault: true,
        baseExperience: 72,
        order: 243,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 2,
        color: 7,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 424,
                trigger: "levelup",
                move: 458,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "エイパム",
            romaji: "Eipam",
            korean: "에이팜",
            chinese: "長尾怪手",
            french: "Capumain",
            german: "Griffel",
            spanish: "Aipom",
            italian: "Aipom",
            english: "Aipom"
        }
    },
    sunkern: {
        num: 191,
        isdefault: true,
        baseExperience: 36,
        order: 245,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 235,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 192,
                trigger: "useitem",
                itemtrigger: "sunstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒマナッツ",
            romaji: "Himanuts",
            korean: "해너츠",
            chinese: "向日種子",
            french: "Tournegrin",
            german: "Sonnkern",
            spanish: "Sunkern",
            italian: "Sunkern",
            english: "Sunkern"
        }
    },
    sunflora: {
        num: 192,
        isdefault: true,
        baseExperience: 149,
        order: 246,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "キマワリ",
            romaji: "Kimawari",
            korean: "해루미",
            chinese: "向日花怪",
            french: "Héliatronc",
            german: "Sonnflora",
            spanish: "Sunflora",
            italian: "Sunflora",
            english: "Sunflora"
        }
    },
    yanma: {
        num: 193,
        isdefault: true,
        baseExperience: 78,
        order: 247,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 2,
        color: 8,
        shape: 13,
        genderDiff: 0,
        evomethods: [
            {
                num: 469,
                trigger: "levelup",
                move: 246,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤンヤンマ",
            romaji: "Yanyanma",
            korean: "왕자리",
            chinese: "陽陽瑪",
            french: "Yanma",
            german: "Yanma",
            spanish: "Yanma",
            italian: "Yanma",
            english: "Yanma"
        }
    },
    wooper: {
        num: 194,
        isdefault: true,
        baseExperience: 42,
        order: 249,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 7,
        genderDiff: 1,
        evomethods: [
            {
                num: 195,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ウパー",
            romaji: "Upah",
            korean: "우파",
            chinese: "烏波",
            french: "Axoloto",
            german: "Felino",
            spanish: "Wooper",
            italian: "Wooper",
            english: "Wooper"
        }
    },
    quagsire: {
        num: 195,
        isdefault: true,
        baseExperience: 151,
        order: 250,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 9,
        generation: 2,
        color: 2,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ヌオー",
            romaji: "Nuoh",
            korean: "누오",
            chinese: "沼王",
            french: "Maraiste",
            german: "Morlord",
            spanish: "Quagsire",
            italian: "Quagsire",
            english: "Quagsire"
        }
    },
    espeon: {
        num: 196,
        isdefault: true,
        baseExperience: 184,
        order: 181,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 2,
        color: 7,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "エーフィ",
            romaji: "Eifie",
            korean: "에브이",
            chinese: "太陽精靈",
            french: "Mentali",
            german: "Psiana",
            spanish: "Espeon",
            italian: "Espeon",
            english: "Espeon"
        }
    },
    umbreon: {
        num: 197,
        isdefault: true,
        baseExperience: 184,
        order: 182,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 8,
        generation: 2,
        color: 1,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ブラッキー",
            romaji: "Blacky",
            korean: "블래키",
            chinese: "月精靈",
            french: "Noctali",
            german: "Nachtara",
            spanish: "Umbreon",
            italian: "Umbreon",
            english: "Umbreon"
        }
    },
    murkrow: {
        num: 198,
        isdefault: true,
        baseExperience: 81,
        order: 251,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 30,
        habitat: 2,
        generation: 2,
        color: 1,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 430,
                trigger: "useitem",
                itemtrigger: "duskstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤミカラス",
            romaji: "Yamikarasu",
            korean: "니로우",
            chinese: "黑暗鴉",
            french: "Cornèbre",
            german: "Kramurx",
            spanish: "Murkrow",
            italian: "Murkrow",
            english: "Murkrow"
        }
    },
    slowking: {
        num: 199,
        isdefault: true,
        baseExperience: 172,
        order: 101,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 70,
        habitat: 9,
        generation: 2,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ヤドキング",
            romaji: "Yadoking",
            korean: "야도킹",
            chinese: "河馬王",
            french: "Roigada",
            german: "Laschoking",
            spanish: "Slowking",
            italian: "Slowking",
            english: "Slowking"
        }
    },
    misdreavus: {
        num: 200,
        isdefault: true,
        baseExperience: 87,
        order: 253,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 1,
        generation: 2,
        color: 4,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 429,
                trigger: "useitem",
                itemtrigger: "duskstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ムウマ",
            romaji: "Muma",
            korean: "무우마",
            chinese: "夢妖",
            french: "Feuforêve",
            german: "Traunfugil",
            spanish: "Misdreavus",
            italian: "Misdreavus",
            english: "Misdreavus"
        }
    },
    unown: {
        num: 201,
        isdefault: true,
        baseExperience: 118,
        order: 255,
        growthRate: 2,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 5,
        generation: 2,
        color: 1,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "アンノーン",
            romaji: "Unknown",
            korean: "안농",
            chinese: "未知圖騰",
            french: "Zarbi",
            german: "Icognito",
            spanish: "Unown",
            italian: "Unown",
            english: "Unown"
        }
    },
    wobbuffet: {
        num: 202,
        isdefault: true,
        baseExperience: 142,
        order: 257,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 2,
        color: 2,
        shape: 5,
        genderDiff: 1,
        names: {
            japanese: "ソーナンス",
            romaji: "Sonans",
            korean: "마자용",
            chinese: "果然翁",
            french: "Qulbutoké",
            german: "Woingenau",
            spanish: "Wobbuffet",
            italian: "Wobbuffet",
            english: "Wobbuffet"
        }
    },
    girafarig: {
        num: 203,
        isdefault: true,
        baseExperience: 159,
        order: 258,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "キリンリキ",
            romaji: "Kirinriki",
            korean: "키링키",
            chinese: "麒麟奇",
            french: "Girafarig",
            german: "Girafarig",
            spanish: "Girafarig",
            italian: "Girafarig",
            english: "Girafarig"
        }
    },
    pineco: {
        num: 204,
        isdefault: true,
        baseExperience: 58,
        order: 259,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 2,
        generation: 2,
        color: 4,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 205,
                trigger: "levelup",
                level: 31,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クヌギダマ",
            romaji: "Kunugidama",
            korean: "피콘",
            chinese: "榛果球",
            french: "Pomdepik",
            german: "Tannza",
            spanish: "Pineco",
            italian: "Pineco",
            english: "Pineco"
        }
    },
    forretress: {
        num: 205,
        isdefault: true,
        baseExperience: 163,
        order: 260,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 2,
        generation: 2,
        color: 7,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "フォレトス",
            romaji: "Foretos",
            korean: "쏘콘",
            chinese: "佛烈托斯",
            french: "Foretress",
            german: "Forstellka",
            spanish: "Forretress",
            italian: "Forretress",
            english: "Forretress"
        }
    },
    dunsparce: {
        num: 206,
        isdefault: true,
        baseExperience: 145,
        order: 261,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 1,
        generation: 2,
        color: 10,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ノコッチ",
            romaji: "Nokocchi",
            korean: "노고치",
            chinese: "土龍弟弟",
            french: "Insolourdo",
            german: "Dummisel",
            spanish: "Dunsparce",
            italian: "Dunsparce",
            english: "Dunsparce"
        }
    },
    gligar: {
        num: 207,
        isdefault: true,
        baseExperience: 86,
        order: 262,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 4,
        generation: 2,
        color: 7,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 472,
                trigger: "levelup",
                helditem: 304,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "グライガー",
            romaji: "Gliger",
            korean: "글라이거",
            chinese: "天蠍",
            french: "Scorplane",
            german: "Skorgla",
            spanish: "Gligar",
            italian: "Gligar",
            english: "Gligar"
        }
    },
    steelix: {
        num: 208,
        isdefault: true,
        baseExperience: 179,
        order: 119,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: 1,
        generation: 2,
        color: 4,
        shape: 2,
        genderDiff: 1,
        names: {
            japanese: "ハガネール",
            romaji: "Haganeil",
            korean: "강철톤",
            chinese: "大鋼蛇",
            french: "Steelix",
            german: "Stahlos",
            spanish: "Steelix",
            italian: "Steelix",
            english: "Steelix"
        }
    },
    steelixmega: {
        num: 208,
        isdefault: false,
        baseExperience: 214,
        order: 120
    },
    snubbull: {
        num: 209,
        isdefault: true,
        baseExperience: 60,
        order: 264,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 8,
        generation: 2,
        color: 6,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 210,
                trigger: "levelup",
                level: 23,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ブルー",
            romaji: "Bulu",
            korean: "블루",
            chinese: "布盧",
            french: "Snubbull",
            german: "Snubbull",
            spanish: "Snubbull",
            italian: "Snubbull",
            english: "Snubbull"
        }
    },
    granbull: {
        num: 210,
        isdefault: true,
        baseExperience: 158,
        order: 265,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 8,
        generation: 2,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "グランブル",
            romaji: "Granbulu",
            korean: "그랑블루",
            chinese: "布盧皇",
            french: "Granbull",
            german: "Granbull",
            spanish: "Granbull",
            italian: "Granbull",
            english: "Granbull"
        }
    },
    qwilfish: {
        num: 211,
        isdefault: true,
        baseExperience: 86,
        order: 266,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 2,
        color: 4,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ハリーセン",
            romaji: "Harysen",
            korean: "침바루",
            chinese: "千針魚",
            french: "Qwilfish",
            german: "Baldorfish",
            spanish: "Qwilfish",
            italian: "Qwilfish",
            english: "Qwilfish"
        }
    },
    scizor: {
        num: 212,
        isdefault: true,
        baseExperience: 175,
        order: 159,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: 3,
        generation: 2,
        color: 8,
        shape: 13,
        genderDiff: 1,
        names: {
            japanese: "ハッサム",
            romaji: "Hassam",
            korean: "핫삼",
            chinese: "巨鉗螳螂",
            french: "Cizayox",
            german: "Scherox",
            spanish: "Scizor",
            italian: "Scizor",
            english: "Scizor"
        }
    },
    scizormega: {
        num: 212,
        isdefault: false,
        baseExperience: 210,
        order: 160
    },
    shuckle: {
        num: 213,
        isdefault: true,
        baseExperience: 177,
        order: 267,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 4,
        generation: 2,
        color: 10,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "ツボツボ",
            romaji: "Tsubotsubo",
            korean: "단단지",
            chinese: "壺壺",
            french: "Caratroc",
            german: "Pottrott",
            spanish: "Shuckle",
            italian: "Shuckle",
            english: "Shuckle"
        }
    },
    heracross: {
        num: 214,
        isdefault: true,
        baseExperience: 175,
        order: 268,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 2,
        color: 2,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ヘラクロス",
            romaji: "Heracros",
            korean: "헤라크로스",
            chinese: "赫拉剋羅斯",
            french: "Scarhino",
            german: "Skaraborn",
            spanish: "Heracross",
            italian: "Heracross",
            english: "Heracross"
        }
    },
    heracrossmega: {
        num: 214,
        isdefault: false,
        baseExperience: 210,
        order: 269
    },
    sneasel: {
        num: 215,
        isdefault: true,
        baseExperience: 86,
        order: 270,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 60,
        habitat: 2,
        generation: 2,
        color: 1,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 461,
                trigger: "levelup",
                helditem: 303,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニューラ",
            romaji: "Nyula",
            korean: "포푸니",
            chinese: "狃拉",
            french: "Farfuret",
            german: "Sniebel",
            spanish: "Sneasel",
            italian: "Sneasel",
            english: "Sneasel"
        }
    },
    teddiursa: {
        num: 216,
        isdefault: true,
        baseExperience: 66,
        order: 272,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 4,
        generation: 2,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 217,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒメグマ",
            romaji: "Himeguma",
            korean: "깜지곰",
            chinese: "熊寶寶",
            french: "Teddiursa",
            german: "Teddiursa",
            spanish: "Teddiursa",
            italian: "Teddiursa",
            english: "Teddiursa"
        }
    },
    ursaring: {
        num: 217,
        isdefault: true,
        baseExperience: 175,
        order: 273,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 4,
        generation: 2,
        color: 3,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "リングマ",
            romaji: "Ringuma",
            korean: "링곰",
            chinese: "圈圈熊",
            french: "Ursaring",
            german: "Ursaring",
            spanish: "Ursaring",
            italian: "Ursaring",
            english: "Ursaring"
        }
    },
    slugma: {
        num: 218,
        isdefault: true,
        baseExperience: 50,
        order: 274,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 4,
        generation: 2,
        color: 8,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 219,
                trigger: "levelup",
                level: 38,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マグマッグ",
            romaji: "Magmag",
            korean: "마그마그",
            chinese: "熔岩蟲",
            french: "Limagma",
            german: "Schneckmag",
            spanish: "Slugma",
            italian: "Slugma",
            english: "Slugma"
        }
    },
    magcargo: {
        num: 219,
        isdefault: true,
        baseExperience: 144,
        order: 275,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 4,
        generation: 2,
        color: 8,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "マグカルゴ",
            romaji: "Magcargot",
            korean: "마그카르고",
            chinese: "熔岩蝸牛",
            french: "Volcaropod",
            german: "Magcargo",
            spanish: "Magcargo",
            italian: "Magcargo",
            english: "Magcargo"
        }
    },
    swinub: {
        num: 220,
        isdefault: true,
        baseExperience: 50,
        order: 276,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 1,
        generation: 2,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 221,
                trigger: "levelup",
                level: 33,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ウリムー",
            romaji: "Urimoo",
            korean: "꾸꾸리",
            chinese: "小山豬",
            french: "Marcacrin",
            german: "Quiekel",
            spanish: "Swinub",
            italian: "Swinub",
            english: "Swinub"
        }
    },
    piloswine: {
        num: 221,
        isdefault: true,
        baseExperience: 158,
        order: 277,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 1,
        generation: 2,
        color: 3,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 473,
                trigger: "levelup",
                move: 246,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "イノムー",
            romaji: "Inomoo",
            korean: "메꾸리",
            chinese: "長毛豬",
            french: "Cochignon",
            german: "Keifel",
            spanish: "Piloswine",
            italian: "Piloswine",
            english: "Piloswine"
        }
    },
    corsola: {
        num: 222,
        isdefault: true,
        baseExperience: 133,
        order: 279,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 2,
        color: 6,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "サニーゴ",
            romaji: "Sunnygo",
            korean: "코산호",
            chinese: "太陽珊瑚",
            french: "Corayon",
            german: "Corasonn",
            spanish: "Corsola",
            italian: "Corsola",
            english: "Corsola"
        }
    },
    remoraid: {
        num: 223,
        isdefault: true,
        baseExperience: 60,
        order: 280,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 7,
        generation: 2,
        color: 4,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 224,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "テッポウオ",
            romaji: "Teppouo",
            korean: "총어",
            chinese: "鐵砲魚",
            french: "Rémoraid",
            german: "Remoraid",
            spanish: "Remoraid",
            italian: "Remoraid",
            english: "Remoraid"
        }
    },
    octillery: {
        num: 224,
        isdefault: true,
        baseExperience: 168,
        order: 281,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 7,
        generation: 2,
        color: 8,
        shape: 10,
        genderDiff: 1,
        names: {
            japanese: "オクタン",
            romaji: "Okutank",
            korean: "대포무노",
            chinese: "章魚桶",
            french: "Octillery",
            german: "Octillery",
            spanish: "Octillery",
            italian: "Octillery",
            english: "Octillery"
        }
    },
    delibird: {
        num: 225,
        isdefault: true,
        baseExperience: 116,
        order: 282,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 2,
        color: 8,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "デリバード",
            romaji: "Delibird",
            korean: "딜리버드",
            chinese: "信使鳥",
            french: "Cadoizo",
            german: "Botogel",
            spanish: "Delibird",
            italian: "Delibird",
            english: "Delibird"
        }
    },
    mantine: {
        num: 226,
        isdefault: true,
        baseExperience: 163,
        order: 284,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: 7,
        generation: 2,
        color: 7,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "マンタイン",
            romaji: "Mantain",
            korean: "만타인",
            chinese: "巨翅飛魚",
            french: "Démanta",
            german: "Mantax",
            spanish: "Mantine",
            italian: "Mantine",
            english: "Mantine"
        }
    },
    skarmory: {
        num: 227,
        isdefault: true,
        baseExperience: 163,
        order: 285,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: 6,
        generation: 2,
        color: 4,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "エアームド",
            romaji: "Airmd",
            korean: "무장조",
            chinese: "盔甲鳥",
            french: "Airmure",
            german: "Panzaeron",
            spanish: "Skarmory",
            italian: "Skarmory",
            english: "Skarmory"
        }
    },
    houndour: {
        num: 228,
        isdefault: true,
        baseExperience: 66,
        order: 286,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 120,
        habitat: 6,
        generation: 2,
        color: 1,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 229,
                trigger: "levelup",
                level: 24,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "デルビル",
            romaji: "Delvil",
            korean: "델빌",
            chinese: "戴魯比",
            french: "Malosse",
            german: "Hunduster",
            spanish: "Houndour",
            italian: "Houndour",
            english: "Houndour"
        }
    },
    houndoom: {
        num: 229,
        isdefault: true,
        baseExperience: 175,
        order: 287,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 6,
        generation: 2,
        color: 1,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "ヘルガー",
            romaji: "Hellgar",
            korean: "헬가",
            chinese: "黑魯加",
            french: "Démolosse",
            german: "Hundemon",
            spanish: "Houndoom",
            italian: "Houndoom",
            english: "Houndoom"
        }
    },
    houndoommega: {
        num: 229,
        isdefault: false,
        baseExperience: 210,
        order: 288
    },
    kingdra: {
        num: 230,
        isdefault: true,
        baseExperience: 243,
        order: 151,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 2,
        color: 2,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "キングドラ",
            romaji: "Kingdra",
            korean: "킹드라",
            chinese: "刺龍王",
            french: "Hyporoi",
            german: "Seedraking",
            spanish: "Kingdra",
            italian: "Kingdra",
            english: "Kingdra"
        }
    },
    phanpy: {
        num: 231,
        isdefault: true,
        baseExperience: 66,
        order: 289,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 6,
        generation: 2,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 232,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴマゾウ",
            romaji: "Gomazou",
            korean: "코코리",
            chinese: "小小象",
            french: "Phanpy",
            german: "Phanpy",
            spanish: "Phanpy",
            italian: "Phanpy",
            english: "Phanpy"
        }
    },
    donphan: {
        num: 232,
        isdefault: true,
        baseExperience: 175,
        order: 290,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 6,
        generation: 2,
        color: 4,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "ドンファン",
            romaji: "Donfan",
            korean: "코리갑",
            chinese: "頓甲",
            french: "Donphan",
            german: "Donphan",
            spanish: "Donphan",
            italian: "Donphan",
            english: "Donphan"
        }
    },
    porygon2: {
        num: 233,
        isdefault: true,
        baseExperience: 180,
        order: 187,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 2,
        color: 8,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 474,
                trigger: "trade",
                helditem: 301,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポリゴン２",
            romaji: "Porygon2",
            korean: "폴리곤2",
            chinese: "3D龍2",
            french: "Porygon2",
            german: "Porygon2",
            spanish: "Porygon2",
            italian: "Porygon2",
            english: "Porygon2"
        }
    },
    stantler: {
        num: 234,
        isdefault: true,
        baseExperience: 163,
        order: 291,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 2,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "オドシシ",
            romaji: "Odoshishi",
            korean: "노라키",
            chinese: "驚角鹿",
            french: "Cerfrousse",
            german: "Damhirplex",
            spanish: "Stantler",
            italian: "Stantler",
            english: "Stantler"
        }
    },
    smeargle: {
        num: 235,
        isdefault: true,
        baseExperience: 88,
        order: 292,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 2,
        color: 9,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ドーブル",
            romaji: "Doble",
            korean: "루브도",
            chinese: "圖圖犬",
            french: "Queulorior",
            german: "Farbeagle",
            spanish: "Smeargle",
            italian: "Smeargle",
            english: "Smeargle"
        }
    },
    pokestarsmeargle: {
        num: 235,
        isdefault: true
    },
    tyrogue: {
        num: 236,
        isdefault: true,
        baseExperience: 42,
        order: 131,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 8,
        generation: 2,
        color: 7,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 106,
                trigger: "levelup",
                level: 20,
                evs: 1,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 107,
                trigger: "levelup",
                level: 20,
                evs: -1,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 237,
                trigger: "levelup",
                level: 20,
                evs: 0,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バルキー",
            romaji: "Balkie",
            korean: "배루키",
            chinese: "巴爾郎",
            french: "Debugant",
            german: "Rabauz",
            spanish: "Tyrogue",
            italian: "Tyrogue",
            english: "Tyrogue"
        }
    },
    hitmontop: {
        num: 237,
        isdefault: true,
        baseExperience: 159,
        order: 134,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 2,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "カポエラー",
            romaji: "Kapoerer",
            korean: "카포에라",
            chinese: "柯波朗",
            french: "Kapoera",
            german: "Kapoera",
            spanish: "Hitmontop",
            italian: "Hitmontop",
            english: "Hitmontop"
        }
    },
    smoochum: {
        num: 238,
        isdefault: true,
        baseExperience: 61,
        order: 161,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 8,
        generation: 2,
        color: 6,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 124,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ムチュール",
            romaji: "Muchul",
            korean: "뽀뽀라",
            chinese: "迷唇娃",
            french: "Lippouti",
            german: "Kussilla",
            spanish: "Smoochum",
            italian: "Smoochum",
            english: "Smoochum"
        }
    },
    elekid: {
        num: 239,
        isdefault: true,
        baseExperience: 72,
        order: 163,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 125,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "エレキッド",
            romaji: "Elekid",
            korean: "에레키드",
            chinese: "電擊怪",
            french: "Élekid",
            german: "Elekid",
            spanish: "Elekid",
            italian: "Elekid",
            english: "Elekid"
        }
    },
    magby: {
        num: 240,
        isdefault: true,
        baseExperience: 73,
        order: 166,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 4,
        generation: 2,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 126,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ブビィ",
            romaji: "Buby",
            korean: "마그비",
            chinese: "小鴨嘴龍",
            french: "Magby",
            german: "Magby",
            spanish: "Magby",
            italian: "Magby",
            english: "Magby"
        }
    },
    miltank: {
        num: 241,
        isdefault: true,
        baseExperience: 172,
        order: 293,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 2,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ミルタンク",
            romaji: "Miltank",
            korean: "밀탱크",
            chinese: "大奶罐",
            french: "Écrémeuh",
            german: "Miltank",
            spanish: "Miltank",
            italian: "Miltank",
            english: "Miltank"
        }
    },
    blissey: {
        num: 242,
        isdefault: true,
        baseExperience: 608,
        order: 144,
        growthRate: 3,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 30,
        habitat: 8,
        generation: 2,
        color: 6,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ハピナス",
            romaji: "Happinas",
            korean: "해피너스",
            chinese: "幸福蛋",
            french: "Leuphorie",
            german: "Heiteira",
            spanish: "Blissey",
            italian: "Blissey",
            english: "Blissey"
        }
    },
    raikou: {
        num: 243,
        isdefault: true,
        baseExperience: 261,
        order: 294,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 3,
        generation: 2,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ライコウ",
            romaji: "Raikou",
            korean: "라이코",
            chinese: "雷公",
            french: "Raikou",
            german: "Raikou",
            spanish: "Raikou",
            italian: "Raikou",
            english: "Raikou"
        }
    },
    entei: {
        num: 244,
        isdefault: true,
        baseExperience: 261,
        order: 295,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 3,
        generation: 2,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "エンテイ",
            romaji: "Entei",
            korean: "앤테이",
            chinese: "炎帝",
            french: "Entei",
            german: "Entei",
            spanish: "Entei",
            italian: "Entei",
            english: "Entei"
        }
    },
    suicune: {
        num: 245,
        isdefault: true,
        baseExperience: 261,
        order: 296,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 3,
        generation: 2,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "スイクン",
            romaji: "Suikun",
            korean: "스이쿤",
            chinese: "水君",
            french: "Suicune",
            german: "Suicune",
            spanish: "Suicune",
            italian: "Suicune",
            english: "Suicune"
        }
    },
    larvitar: {
        num: 246,
        isdefault: true,
        baseExperience: 60,
        order: 297,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 4,
        generation: 2,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 247,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヨーギラス",
            romaji: "Yogiras",
            korean: "애버라스",
            chinese: "由基拉",
            french: "Embrylex",
            german: "Larvitar",
            spanish: "Larvitar",
            italian: "Larvitar",
            english: "Larvitar"
        }
    },
    pupitar: {
        num: 247,
        isdefault: true,
        baseExperience: 144,
        order: 298,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 4,
        generation: 2,
        color: 4,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 248,
                trigger: "levelup",
                level: 55,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "サナギラス",
            romaji: "Sanagiras",
            korean: "데기라스",
            chinese: "沙基拉",
            french: "Ymphect",
            german: "Pupitar",
            spanish: "Pupitar",
            italian: "Pupitar",
            english: "Pupitar"
        }
    },
    tyranitar: {
        num: 248,
        isdefault: true,
        baseExperience: 270,
        order: 299,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 4,
        generation: 2,
        color: 5,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "バンギラス",
            romaji: "Bangiras",
            korean: "마기라스",
            chinese: "班吉拉",
            french: "Tyranocif",
            german: "Despotar",
            spanish: "Tyranitar",
            italian: "Tyranitar",
            english: "Tyranitar"
        }
    },
    tyranitarmega: {
        num: 248,
        isdefault: false,
        baseExperience: 315,
        order: 300
    },
    lugia: {
        num: 249,
        isdefault: true,
        baseExperience: 306,
        order: 301,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: 5,
        generation: 2,
        color: 9,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ルギア",
            romaji: "Lugia",
            korean: "루기아",
            chinese: "洛奇亞",
            french: "Lugia",
            german: "Lugia",
            spanish: "Lugia",
            italian: "Lugia",
            english: "Lugia"
        }
    },
    hooh: {
        num: 250,
        isdefault: true,
        baseExperience: 306,
        order: 302,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: 5,
        generation: 2,
        color: 8,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ホウオウ",
            romaji: "Houou",
            korean: "칠색조",
            chinese: "鳳王",
            french: "Ho-Oh",
            german: "Ho-Oh",
            spanish: "Ho-Oh",
            italian: "Ho-Oh",
            english: "Ho-Oh"
        }
    },
    celebi: {
        num: 251,
        isdefault: true,
        baseExperience: 270,
        order: 303,
        growthRate: 4,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 45,
        habitat: 2,
        generation: 2,
        color: 5,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "セレビィ",
            romaji: "Celebi",
            korean: "세레비",
            chinese: "雪拉比",
            french: "Celebi",
            german: "Celebi",
            spanish: "Celebi",
            italian: "Celebi",
            english: "Celebi"
        }
    },
    treecko: {
        num: 252,
        isdefault: true,
        baseExperience: 62,
        order: 304,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 253,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キモリ",
            romaji: "Kimori",
            korean: "나무지기",
            chinese: "木守宮",
            french: "Arcko",
            german: "Geckarbor",
            spanish: "Treecko",
            italian: "Treecko",
            english: "Treecko"
        }
    },
    grovyle: {
        num: 253,
        isdefault: true,
        baseExperience: 142,
        order: 305,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 254,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ジュプトル",
            romaji: "Juptile",
            korean: "나무돌이",
            chinese: "森林蜥蜴",
            french: "Massko",
            german: "Reptain",
            spanish: "Grovyle",
            italian: "Grovyle",
            english: "Grovyle"
        }
    },
    sceptile: {
        num: 254,
        isdefault: true,
        baseExperience: 239,
        order: 306,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ジュカイン",
            romaji: "Jukain",
            korean: "나무킹",
            chinese: "蜥蜴王",
            french: "Jungko",
            german: "Gewaldro",
            spanish: "Sceptile",
            italian: "Sceptile",
            english: "Sceptile"
        }
    },
    sceptilemega: {
        num: 254,
        isdefault: false,
        baseExperience: 284,
        order: 307
    },
    torchic: {
        num: 255,
        isdefault: true,
        baseExperience: 62,
        order: 308,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 8,
        shape: 7,
        genderDiff: 1,
        evomethods: [
            {
                num: 256,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アチャモ",
            romaji: "Achamo",
            korean: "아차모",
            chinese: "火稚雞",
            french: "Poussifeu",
            german: "Flemmli",
            spanish: "Torchic",
            italian: "Torchic",
            english: "Torchic"
        }
    },
    combusken: {
        num: 256,
        isdefault: true,
        baseExperience: 142,
        order: 309,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 8,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 257,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ワカシャモ",
            romaji: "Wakasyamo",
            korean: "영치코",
            chinese: "力壯雞",
            french: "Galifeu",
            german: "Jungglut",
            spanish: "Combusken",
            italian: "Combusken",
            english: "Combusken"
        }
    },
    blaziken: {
        num: 257,
        isdefault: true,
        baseExperience: 239,
        order: 310,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 8,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "バシャーモ",
            romaji: "Bursyamo",
            korean: "번치코",
            chinese: "火焰雞",
            french: "Braségali",
            german: "Lohgock",
            spanish: "Blaziken",
            italian: "Blaziken",
            english: "Blaziken"
        }
    },
    blazikenmega: {
        num: 257,
        isdefault: false,
        baseExperience: 284,
        order: 311
    },
    mudkip: {
        num: 258,
        isdefault: true,
        baseExperience: 62,
        order: 312,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 259,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミズゴロウ",
            romaji: "Mizugorou",
            korean: "물짱이",
            chinese: "水躍魚",
            french: "Gobou",
            german: "Hydropi",
            spanish: "Mudkip",
            italian: "Mudkip",
            english: "Mudkip"
        }
    },
    marshtomp: {
        num: 259,
        isdefault: true,
        baseExperience: 142,
        order: 313,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 260,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヌマクロー",
            romaji: "Numacraw",
            korean: "늪짱이",
            chinese: "沼躍魚",
            french: "Flobio",
            german: "Moorabbel",
            spanish: "Marshtomp",
            italian: "Marshtomp",
            english: "Marshtomp"
        }
    },
    swampert: {
        num: 260,
        isdefault: true,
        baseExperience: 241,
        order: 314,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ラグラージ",
            romaji: "Laglarge",
            korean: "대짱이",
            chinese: "巨沼怪",
            french: "Laggron",
            german: "Sumpex",
            spanish: "Swampert",
            italian: "Swampert",
            english: "Swampert"
        }
    },
    swampertmega: {
        num: 260,
        isdefault: false,
        baseExperience: 286,
        order: 315
    },
    poochyena: {
        num: 261,
        isdefault: true,
        baseExperience: 44,
        order: 316,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 3,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 262,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポチエナ",
            romaji: "Pochiena",
            korean: "포챠나",
            chinese: "土狼犬",
            french: "Medhyèna",
            german: "Fiffyen",
            spanish: "Poochyena",
            italian: "Poochyena",
            english: "Poochyena"
        }
    },
    mightyena: {
        num: 262,
        isdefault: true,
        baseExperience: 147,
        order: 317,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 127,
        habitat: 3,
        generation: 3,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "グラエナ",
            romaji: "Guraena",
            korean: "그라에나",
            chinese: "大狼犬",
            french: "Grahyèna",
            german: "Magnayen",
            spanish: "Mightyena",
            italian: "Mightyena",
            english: "Mightyena"
        }
    },
    zigzagoon: {
        num: 263,
        isdefault: true,
        baseExperience: 48,
        order: 318,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 3,
        generation: 3,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 264,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ジグザグマ",
            romaji: "Ziguzaguma",
            korean: "지그제구리",
            chinese: "蛇紋熊",
            french: "Zigzaton",
            german: "Zigzachs",
            spanish: "Zigzagoon",
            italian: "Zigzagoon",
            english: "Zigzagoon"
        }
    },
    linoone: {
        num: 264,
        isdefault: true,
        baseExperience: 147,
        order: 319,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 3,
        generation: 3,
        color: 9,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "マッスグマ",
            romaji: "Massuguma",
            korean: "직구리",
            chinese: "直衝熊",
            french: "Linéon",
            german: "Geradaks",
            spanish: "Linoone",
            italian: "Linoone",
            english: "Linoone"
        }
    },
    wurmple: {
        num: 265,
        isdefault: true,
        baseExperience: 39,
        order: 320,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 8,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 266,
                trigger: "levelup",
                level: 7,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 268,
                trigger: "levelup",
                level: 7,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ケムッソ",
            romaji: "Kemusso",
            korean: "개무소",
            chinese: "刺尾蟲",
            french: "Chenipotte",
            german: "Waumpel",
            spanish: "Wurmple",
            italian: "Wurmple",
            english: "Wurmple"
        }
    },
    silcoon: {
        num: 266,
        isdefault: true,
        baseExperience: 72,
        order: 321,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 3,
        color: 9,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 267,
                trigger: "levelup",
                level: 10,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カラサリス",
            romaji: "Karasalis",
            korean: "실쿤",
            chinese: "甲殼蛹",
            french: "Armulys",
            german: "Schaloko",
            spanish: "Silcoon",
            italian: "Silcoon",
            english: "Silcoon"
        }
    },
    beautifly: {
        num: 267,
        isdefault: true,
        baseExperience: 178,
        order: 322,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 10,
        shape: 13,
        genderDiff: 1,
        names: {
            japanese: "アゲハント",
            romaji: "Agehunt",
            korean: "뷰티플라이",
            chinese: "狩獵鳳蝶",
            french: "Charmillon",
            german: "Papinella",
            spanish: "Beautifly",
            italian: "Beautifly",
            english: "Beautifly"
        }
    },
    cascoon: {
        num: 268,
        isdefault: true,
        baseExperience: 41,
        order: 323,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 3,
        color: 7,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 269,
                trigger: "levelup",
                level: 10,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マユルド",
            romaji: "Mayuld",
            korean: "카스쿤",
            chinese: "盾甲繭",
            french: "Blindalys",
            german: "Panekon",
            spanish: "Cascoon",
            italian: "Cascoon",
            english: "Cascoon"
        }
    },
    dustox: {
        num: 269,
        isdefault: true,
        baseExperience: 135,
        order: 324,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 13,
        genderDiff: 1,
        names: {
            japanese: "ドクケイル",
            romaji: "Dokucale",
            korean: "독케일",
            chinese: "毒粉蝶",
            french: "Papinox",
            german: "Pudox",
            spanish: "Dustox",
            italian: "Dustox",
            english: "Dustox"
        }
    },
    lotad: {
        num: 270,
        isdefault: true,
        baseExperience: 44,
        order: 325,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 9,
        generation: 3,
        color: 5,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 271,
                trigger: "levelup",
                level: 14,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハスボー",
            romaji: "Hassboh",
            korean: "연꽃몬",
            chinese: "蓮葉童子",
            french: "Nénupiot",
            german: "Loturzel",
            spanish: "Lotad",
            italian: "Lotad",
            english: "Lotad"
        }
    },
    lombre: {
        num: 271,
        isdefault: true,
        baseExperience: 119,
        order: 326,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 9,
        generation: 3,
        color: 5,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 272,
                trigger: "useitem",
                itemtrigger: "waterstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハスブレロ",
            romaji: "Hasubrero",
            korean: "로토스",
            chinese: "蓮帽小童",
            french: "Lombre",
            german: "Lombrero",
            spanish: "Lombre",
            italian: "Lombre",
            english: "Lombre"
        }
    },
    ludicolo: {
        num: 272,
        isdefault: true,
        baseExperience: 216,
        order: 327,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 3,
        color: 5,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ルンパッパ",
            romaji: "Runpappa",
            korean: "로파파",
            chinese: "樂天河童",
            french: "Ludicolo",
            german: "Kappalores",
            spanish: "Ludicolo",
            italian: "Ludicolo",
            english: "Ludicolo"
        }
    },
    seedot: {
        num: 273,
        isdefault: true,
        baseExperience: 44,
        order: 328,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 274,
                trigger: "levelup",
                level: 14,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タネボー",
            romaji: "Taneboh",
            korean: "도토링",
            chinese: "橡實果",
            french: "Grainipiot",
            german: "Samurzel",
            spanish: "Seedot",
            italian: "Seedot",
            english: "Seedot"
        }
    },
    nuzleaf: {
        num: 274,
        isdefault: true,
        baseExperience: 119,
        order: 329,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 12,
        genderDiff: 1,
        evomethods: [
            {
                num: 275,
                trigger: "useitem",
                itemtrigger: "leafstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コノハナ",
            romaji: "Konohana",
            korean: "잎새코",
            chinese: "長鼻葉",
            french: "Pifeuil",
            german: "Blanas",
            spanish: "Nuzleaf",
            italian: "Nuzleaf",
            english: "Nuzleaf"
        }
    },
    shiftry: {
        num: 275,
        isdefault: true,
        baseExperience: 216,
        order: 330,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ダーテング",
            romaji: "Dirteng",
            korean: "다탱구",
            chinese: "狡猾天狗",
            french: "Tengalice",
            german: "Tengulist",
            spanish: "Shiftry",
            italian: "Shiftry",
            english: "Shiftry"
        }
    },
    taillow: {
        num: 276,
        isdefault: true,
        baseExperience: 54,
        order: 331,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 3,
        generation: 3,
        color: 2,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 277,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "スバメ",
            romaji: "Subame",
            korean: "테일로",
            chinese: "傲骨燕",
            french: "Nirondelle",
            german: "Schwalbini",
            spanish: "Taillow",
            italian: "Taillow",
            english: "Taillow"
        }
    },
    swellow: {
        num: 277,
        isdefault: true,
        baseExperience: 151,
        order: 332,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 2,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "オオスバメ",
            romaji: "Ohsubame",
            korean: "스왈로",
            chinese: "大王燕",
            french: "Hélédelle",
            german: "Schwalboss",
            spanish: "Swellow",
            italian: "Swellow",
            english: "Swellow"
        }
    },
    wingull: {
        num: 278,
        isdefault: true,
        baseExperience: 54,
        order: 333,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 7,
        generation: 3,
        color: 9,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 279,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キャモメ",
            romaji: "Camome",
            korean: "갈모매",
            chinese: "長翅鷗",
            french: "Goélise",
            german: "Wingull",
            spanish: "Wingull",
            italian: "Wingull",
            english: "Wingull"
        }
    },
    pelipper: {
        num: 279,
        isdefault: true,
        baseExperience: 151,
        order: 334,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 3,
        color: 10,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ペリッパー",
            romaji: "Pelipper",
            korean: "패리퍼",
            chinese: "大嘴鷗",
            french: "Bekipan",
            german: "Pelipper",
            spanish: "Pelipper",
            italian: "Pelipper",
            english: "Pelipper"
        }
    },
    ralts: {
        num: 280,
        isdefault: true,
        baseExperience: 40,
        order: 335,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 235,
        habitat: 8,
        generation: 3,
        color: 9,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 281,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ラルトス",
            romaji: "Ralts",
            korean: "랄토스",
            chinese: "拉魯拉絲",
            french: "Tarsal",
            german: "Trasla",
            spanish: "Ralts",
            italian: "Ralts",
            english: "Ralts"
        }
    },
    kirlia: {
        num: 281,
        isdefault: true,
        baseExperience: 97,
        order: 336,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 120,
        habitat: 8,
        generation: 3,
        color: 9,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 282,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 475,
                trigger: "useitem",
                itemtrigger: "dawnstone",
                gender: 2,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キルリア",
            romaji: "Kirlia",
            korean: "킬리아",
            chinese: "奇魯莉安",
            french: "Kirlia",
            german: "Kirlia",
            spanish: "Kirlia",
            italian: "Kirlia",
            english: "Kirlia"
        }
    },
    gardevoir: {
        num: 282,
        isdefault: true,
        baseExperience: 233,
        order: 337,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 8,
        generation: 3,
        color: 9,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "サーナイト",
            romaji: "Sirnight",
            korean: "가디안",
            chinese: "沙奈朵",
            french: "Gardevoir",
            german: "Guardevoir",
            spanish: "Gardevoir",
            italian: "Gardevoir",
            english: "Gardevoir"
        }
    },
    gardevoirmega: {
        num: 282,
        isdefault: false,
        baseExperience: 278,
        order: 338
    },
    surskit: {
        num: 283,
        isdefault: true,
        baseExperience: 54,
        order: 341,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 284,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アメタマ",
            romaji: "Ametama",
            korean: "비구술",
            chinese: "溜溜糖球",
            french: "Arakdo",
            german: "Gehweiher",
            spanish: "Surskit",
            italian: "Surskit",
            english: "Surskit"
        }
    },
    masquerain: {
        num: 284,
        isdefault: true,
        baseExperience: 145,
        order: 342,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "アメモース",
            romaji: "Amemoth",
            korean: "비나방",
            chinese: "雨翅蛾",
            french: "Maskadra",
            german: "Maskeregen",
            spanish: "Masquerain",
            italian: "Masquerain",
            english: "Masquerain"
        }
    },
    shroomish: {
        num: 285,
        isdefault: true,
        baseExperience: 59,
        order: 343,
        growthRate: 6,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 286,
                trigger: "levelup",
                level: 23,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キノココ",
            romaji: "Kinococo",
            korean: "버섯꼬",
            chinese: "蘑蘑菇",
            french: "Balignon",
            german: "Knilz",
            spanish: "Shroomish",
            italian: "Shroomish",
            english: "Shroomish"
        }
    },
    breloom: {
        num: 286,
        isdefault: true,
        baseExperience: 161,
        order: 344,
        growthRate: 6,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "キノガッサ",
            romaji: "Kinogassa",
            korean: "버섯모",
            chinese: "斗笠菇",
            french: "Chapignon",
            german: "Kapilz",
            spanish: "Breloom",
            italian: "Breloom",
            english: "Breloom"
        }
    },
    slakoth: {
        num: 287,
        isdefault: true,
        baseExperience: 56,
        order: 345,
        growthRate: 1,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 288,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ナマケロ",
            romaji: "Namakero",
            korean: "게을로",
            chinese: "懶人翁",
            french: "Parecool",
            german: "Bummelz",
            spanish: "Slakoth",
            italian: "Slakoth",
            english: "Slakoth"
        }
    },
    vigoroth: {
        num: 288,
        isdefault: true,
        baseExperience: 154,
        order: 346,
        growthRate: 1,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 3,
        color: 9,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 289,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤルキモノ",
            romaji: "Yarukimono",
            korean: "발바로",
            chinese: "過動猿",
            french: "Vigoroth",
            german: "Muntier",
            spanish: "Vigoroth",
            italian: "Vigoroth",
            english: "Vigoroth"
        }
    },
    slaking: {
        num: 289,
        isdefault: true,
        baseExperience: 252,
        order: 347,
        growthRate: 1,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ケッキング",
            romaji: "Kekking",
            korean: "게을킹",
            chinese: "請假王",
            french: "Monaflèmit",
            german: "Letarking",
            spanish: "Slaking",
            italian: "Slaking",
            english: "Slaking"
        }
    },
    nincada: {
        num: 290,
        isdefault: true,
        baseExperience: 53,
        order: 348,
        growthRate: 5,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 4,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 291,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 292,
                trigger: "shed",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ツチニン",
            romaji: "Tutinin",
            korean: "토중몬",
            chinese: "土居忍士",
            french: "Ningale",
            german: "Nincada",
            spanish: "Nincada",
            italian: "Nincada",
            english: "Nincada"
        }
    },
    ninjask: {
        num: 291,
        isdefault: true,
        baseExperience: 160,
        order: 349,
        growthRate: 5,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 2,
        generation: 3,
        color: 10,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "テッカニン",
            romaji: "Tekkanin",
            korean: "아이스크",
            chinese: "鐵面忍者",
            french: "Ninjask",
            german: "Ninjask",
            spanish: "Ninjask",
            italian: "Ninjask",
            english: "Ninjask"
        }
    },
    shedinja: {
        num: 292,
        isdefault: true,
        baseExperience: 83,
        order: 350,
        growthRate: 5,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 3,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ヌケニン",
            romaji: "Nukenin",
            korean: "껍질몬",
            chinese: "脫殼忍者",
            french: "Munja",
            german: "Ninjatom",
            spanish: "Shedinja",
            italian: "Shedinja",
            english: "Shedinja"
        }
    },
    whismur: {
        num: 293,
        isdefault: true,
        baseExperience: 48,
        order: 351,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 1,
        generation: 3,
        color: 6,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 294,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴニョニョ",
            romaji: "Gonyonyo",
            korean: "소곤룡",
            chinese: "咕妞妞",
            french: "Chuchmur",
            german: "Flurmel",
            spanish: "Whismur",
            italian: "Whismur",
            english: "Whismur"
        }
    },
    loudred: {
        num: 294,
        isdefault: true,
        baseExperience: 126,
        order: 352,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 1,
        generation: 3,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 295,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドゴーム",
            romaji: "Dogohmb",
            korean: "노공룡",
            chinese: "吼爆彈",
            french: "Ramboum",
            german: "Krakeelo",
            spanish: "Loudred",
            italian: "Loudred",
            english: "Loudred"
        }
    },
    exploud: {
        num: 295,
        isdefault: true,
        baseExperience: 221,
        order: 353,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 3,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "バクオング",
            romaji: "Bakuong",
            korean: "폭음룡",
            chinese: "爆音怪",
            french: "Brouhabam",
            german: "Krawumms",
            spanish: "Exploud",
            italian: "Exploud",
            english: "Exploud"
        }
    },
    makuhita: {
        num: 296,
        isdefault: true,
        baseExperience: 47,
        order: 354,
        growthRate: 6,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: 4,
        generation: 3,
        color: 10,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 297,
                trigger: "levelup",
                level: 24,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マクノシタ",
            romaji: "Makunoshita",
            korean: "마크탕",
            chinese: "幕下力士",
            french: "Makuhita",
            german: "Makuhita",
            spanish: "Makuhita",
            italian: "Makuhita",
            english: "Makuhita"
        }
    },
    hariyama: {
        num: 297,
        isdefault: true,
        baseExperience: 166,
        order: 355,
        growthRate: 6,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 4,
        generation: 3,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ハリテヤマ",
            romaji: "Hariteyama",
            korean: "하리뭉",
            chinese: "超力王",
            french: "Hariyama",
            german: "Hariyama",
            spanish: "Hariyama",
            italian: "Hariyama",
            english: "Hariyama"
        }
    },
    azurill: {
        num: 298,
        isdefault: true,
        baseExperience: 38,
        order: 235,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 150,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 183,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ルリリ",
            romaji: "Ruriri",
            korean: "루리리",
            chinese: "露力麗",
            french: "Azurill",
            german: "Azurill",
            spanish: "Azurill",
            italian: "Azurill",
            english: "Azurill"
        }
    },
    nosepass: {
        num: 299,
        isdefault: true,
        baseExperience: 75,
        order: 356,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 1,
        generation: 3,
        color: 4,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 476,
                trigger: "levelup",
                location: 10,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 476,
                trigger: "levelup",
                location: 379,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 476,
                trigger: "levelup",
                location: 629,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ノズパス",
            romaji: "Nosepass",
            korean: "코코파스",
            chinese: "朝北鼻",
            french: "Tarinor",
            german: "Nasgnet",
            spanish: "Nosepass",
            italian: "Nosepass",
            english: "Nosepass"
        }
    },
    skitty: {
        num: 300,
        isdefault: true,
        baseExperience: 52,
        order: 358,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 6,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 301,
                trigger: "useitem",
                itemtrigger: "moonstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "エネコ",
            romaji: "Eneco",
            korean: "에나비",
            chinese: "向尾喵",
            french: "Skitty",
            german: "Eneco",
            spanish: "Skitty",
            italian: "Skitty",
            english: "Skitty"
        }
    },
    delcatty: {
        num: 301,
        isdefault: true,
        baseExperience: 133,
        order: 359,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 2,
        generation: 3,
        color: 7,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "エネコロロ",
            romaji: "Enekororo",
            korean: "델케티",
            chinese: "優雅貓",
            french: "Delcatty",
            german: "Enekoro",
            spanish: "Delcatty",
            italian: "Delcatty",
            english: "Delcatty"
        }
    },
    sableye: {
        num: 302,
        isdefault: true,
        baseExperience: 133,
        order: 360,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 1,
        generation: 3,
        color: 7,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ヤミラミ",
            romaji: "Yamirami",
            korean: "깜까미",
            chinese: "勾魂眼",
            french: "Ténéfix",
            german: "Zobiris",
            spanish: "Sableye",
            italian: "Sableye",
            english: "Sableye"
        }
    },
    sableyemega: {
        num: 302,
        isdefault: false,
        baseExperience: 168,
        order: 361
    },
    mawile: {
        num: 303,
        isdefault: true,
        baseExperience: 133,
        order: 362,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 3,
        color: 1,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "クチート",
            romaji: "Kucheat",
            korean: "입치트",
            chinese: "大嘴娃",
            french: "Mysdibule",
            german: "Flunkifer",
            spanish: "Mawile",
            italian: "Mawile",
            english: "Mawile"
        }
    },
    mawilemega: {
        num: 303,
        isdefault: false,
        baseExperience: 168,
        order: 363
    },
    aron: {
        num: 304,
        isdefault: true,
        baseExperience: 66,
        order: 364,
        growthRate: 1,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 180,
        habitat: 4,
        generation: 3,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 305,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ココドラ",
            romaji: "Cokodora",
            korean: "가보리",
            chinese: "可可多拉",
            french: "Galekid",
            german: "Stollunior",
            spanish: "Aron",
            italian: "Aron",
            english: "Aron"
        }
    },
    lairon: {
        num: 305,
        isdefault: true,
        baseExperience: 151,
        order: 365,
        growthRate: 1,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 90,
        habitat: 4,
        generation: 3,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 306,
                trigger: "levelup",
                level: 42,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コドラ",
            romaji: "Kodora",
            korean: "갱도라",
            chinese: "可多拉",
            french: "Galegon",
            german: "Stollrak",
            spanish: "Lairon",
            italian: "Lairon",
            english: "Lairon"
        }
    },
    aggron: {
        num: 306,
        isdefault: true,
        baseExperience: 239,
        order: 366,
        growthRate: 1,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 4,
        generation: 3,
        color: 4,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ボスゴドラ",
            romaji: "Bossgodora",
            korean: "보스로라",
            chinese: "波士可多拉",
            french: "Galeking",
            german: "Stolloss",
            spanish: "Aggron",
            italian: "Aggron",
            english: "Aggron"
        }
    },
    aggronmega: {
        num: 306,
        isdefault: false,
        baseExperience: 284,
        order: 367
    },
    meditite: {
        num: 307,
        isdefault: true,
        baseExperience: 56,
        order: 368,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: 4,
        generation: 3,
        color: 2,
        shape: 12,
        genderDiff: 1,
        evomethods: [
            {
                num: 308,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アサナン",
            romaji: "Asanan",
            korean: "요가랑",
            chinese: "瑪沙那",
            french: "Méditikka",
            german: "Meditie",
            spanish: "Meditite",
            italian: "Meditite",
            english: "Meditite"
        }
    },
    medicham: {
        num: 308,
        isdefault: true,
        baseExperience: 144,
        order: 369,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 4,
        generation: 3,
        color: 8,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "チャーレム",
            romaji: "Charem",
            korean: "요가램",
            chinese: "恰雷姆",
            french: "Charmina",
            german: "Meditalis",
            spanish: "Medicham",
            italian: "Medicham",
            english: "Medicham"
        }
    },
    medichammega: {
        num: 308,
        isdefault: false,
        baseExperience: 179,
        order: 370
    },
    electrike: {
        num: 309,
        isdefault: true,
        baseExperience: 59,
        order: 371,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 3,
        generation: 3,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 310,
                trigger: "levelup",
                level: 26,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ラクライ",
            romaji: "Rakurai",
            korean: "썬더라이",
            chinese: "落雷獸",
            french: "Dynavolt",
            german: "Frizelbliz",
            spanish: "Electrike",
            italian: "Electrike",
            english: "Electrike"
        }
    },
    manectric: {
        num: 310,
        isdefault: true,
        baseExperience: 166,
        order: 372,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ライボルト",
            romaji: "Livolt",
            korean: "썬더볼트",
            chinese: "雷電獸",
            french: "Élecsprint",
            german: "Voltenso",
            spanish: "Manectric",
            italian: "Manectric",
            english: "Manectric"
        }
    },
    manectricmega: {
        num: 310,
        isdefault: false,
        baseExperience: 201,
        order: 373
    },
    plusle: {
        num: 311,
        isdefault: true,
        baseExperience: 142,
        order: 374,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 3,
        generation: 3,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "プラスル",
            romaji: "Prasle",
            korean: "플러시",
            chinese: "正電拍拍",
            french: "Posipi",
            german: "Plusle",
            spanish: "Plusle",
            italian: "Plusle",
            english: "Plusle"
        }
    },
    minun: {
        num: 312,
        isdefault: true,
        baseExperience: 142,
        order: 375,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 3,
        generation: 3,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "マイナン",
            romaji: "Minun",
            korean: "마이농",
            chinese: "負電拍拍",
            french: "Négapi",
            german: "Minun",
            spanish: "Minun",
            italian: "Minun",
            english: "Minun"
        }
    },
    volbeat: {
        num: 313,
        isdefault: true,
        baseExperience: 140,
        order: 376,
        growthRate: 5,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 150,
        habitat: 2,
        generation: 3,
        color: 4,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "バルビート",
            romaji: "Barubeat",
            korean: "볼비트",
            chinese: "電螢蟲",
            french: "Muciole",
            german: "Volbeat",
            spanish: "Volbeat",
            italian: "Volbeat",
            english: "Volbeat"
        }
    },
    illumise: {
        num: 314,
        isdefault: true,
        baseExperience: 140,
        order: 377,
        growthRate: 6,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 150,
        habitat: 2,
        generation: 3,
        color: 7,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "イルミーゼ",
            romaji: "Illumise",
            korean: "네오비트",
            chinese: "甜甜螢",
            french: "Lumivole",
            german: "Illumise",
            spanish: "Illumise",
            italian: "Illumise",
            english: "Illumise"
        }
    },
    roselia: {
        num: 315,
        isdefault: true,
        baseExperience: 140,
        order: 379,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 150,
        habitat: 3,
        generation: 3,
        color: 5,
        shape: 12,
        genderDiff: 1,
        evomethods: [
            {
                num: 407,
                trigger: "useitem",
                itemtrigger: "shinystone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ロゼリア",
            romaji: "Roselia",
            korean: "로젤리아",
            chinese: "毒薔薇",
            french: "Rosélia",
            german: "Roselia",
            spanish: "Roselia",
            italian: "Roselia",
            english: "Roselia"
        }
    },
    gulpin: {
        num: 316,
        isdefault: true,
        baseExperience: 60,
        order: 381,
        growthRate: 6,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 3,
        generation: 3,
        color: 5,
        shape: 4,
        genderDiff: 1,
        evomethods: [
            {
                num: 317,
                trigger: "levelup",
                level: 26,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴクリン",
            romaji: "Gokulin",
            korean: "꼴깍몬",
            chinese: "溶食獸",
            french: "Gloupti",
            german: "Schluppuck",
            spanish: "Gulpin",
            italian: "Gulpin",
            english: "Gulpin"
        }
    },
    swalot: {
        num: 317,
        isdefault: true,
        baseExperience: 163,
        order: 382,
        growthRate: 6,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 3,
        generation: 3,
        color: 7,
        shape: 4,
        genderDiff: 1,
        names: {
            japanese: "マルノーム",
            romaji: "Marunoom",
            korean: "꿀꺽몬",
            chinese: "吞食獸",
            french: "Avaltout",
            german: "Schlukwech",
            spanish: "Swalot",
            italian: "Swalot",
            english: "Swalot"
        }
    },
    carvanha: {
        num: 318,
        isdefault: true,
        baseExperience: 61,
        order: 383,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 225,
        habitat: 7,
        generation: 3,
        color: 8,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 319,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キバニア",
            romaji: "Kibanha",
            korean: "샤프니아",
            chinese: "利牙魚",
            french: "Carvanha",
            german: "Kanivanha",
            spanish: "Carvanha",
            italian: "Carvanha",
            english: "Carvanha"
        }
    },
    sharpedo: {
        num: 319,
        isdefault: true,
        baseExperience: 161,
        order: 384,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 60,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "サメハダー",
            romaji: "Samehader",
            korean: "샤크니아",
            chinese: "巨牙鯊",
            french: "Sharpedo",
            german: "Tohaido",
            spanish: "Sharpedo",
            italian: "Sharpedo",
            english: "Sharpedo"
        }
    },
    sharpedomega: {
        num: 319,
        isdefault: false,
        baseExperience: 196,
        order: 385
    },
    wailmer: {
        num: 320,
        isdefault: true,
        baseExperience: 80,
        order: 386,
        growthRate: 6,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 125,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 321,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ホエルコ",
            romaji: "Whalko",
            korean: "고래왕자",
            chinese: "吼吼鯨",
            french: "Wailmer",
            german: "Wailmer",
            spanish: "Wailmer",
            italian: "Wailmer",
            english: "Wailmer"
        }
    },
    wailord: {
        num: 321,
        isdefault: true,
        baseExperience: 175,
        order: 387,
        growthRate: 6,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ホエルオー",
            romaji: "Whaloh",
            korean: "고래왕",
            chinese: "吼鯨王",
            french: "Wailord",
            german: "Wailord",
            spanish: "Wailord",
            italian: "Wailord",
            english: "Wailord"
        }
    },
    numel: {
        num: 322,
        isdefault: true,
        baseExperience: 61,
        order: 388,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 4,
        generation: 3,
        color: 10,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 323,
                trigger: "levelup",
                level: 33,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドンメル",
            romaji: "Donmel",
            korean: "둔타",
            chinese: "呆火駝",
            french: "Chamallot",
            german: "Camaub",
            spanish: "Numel",
            italian: "Numel",
            english: "Numel"
        }
    },
    camerupt: {
        num: 323,
        isdefault: true,
        baseExperience: 161,
        order: 389,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 150,
        habitat: 4,
        generation: 3,
        color: 8,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "バクーダ",
            romaji: "Bakuuda",
            korean: "폭타",
            chinese: "噴火駝",
            french: "Camérupt",
            german: "Camerupt",
            spanish: "Camerupt",
            italian: "Camerupt",
            english: "Camerupt"
        }
    },
    cameruptmega: {
        num: 323,
        isdefault: false,
        baseExperience: 196,
        order: 390
    },
    torkoal: {
        num: 324,
        isdefault: true,
        baseExperience: 165,
        order: 391,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 4,
        generation: 3,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "コータス",
            romaji: "Cotoise",
            korean: "코터스",
            chinese: "煤炭龜",
            french: "Chartor",
            german: "Qurtel",
            spanish: "Torkoal",
            italian: "Torkoal",
            english: "Torkoal"
        }
    },
    spoink: {
        num: 325,
        isdefault: true,
        baseExperience: 66,
        order: 392,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 4,
        generation: 3,
        color: 1,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 326,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バネブー",
            romaji: "Baneboo",
            korean: "피그점프",
            chinese: "跳跳豬",
            french: "Spoink",
            german: "Spoink",
            spanish: "Spoink",
            italian: "Spoink",
            english: "Spoink"
        }
    },
    grumpig: {
        num: 326,
        isdefault: true,
        baseExperience: 165,
        order: 393,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 4,
        generation: 3,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ブーピッグ",
            romaji: "Boopig",
            korean: "피그킹",
            chinese: "噗噗豬",
            french: "Groret",
            german: "Groink",
            spanish: "Grumpig",
            italian: "Grumpig",
            english: "Grumpig"
        }
    },
    spinda: {
        num: 327,
        isdefault: true,
        baseExperience: 126,
        order: 394,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 4,
        generation: 3,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "パッチール",
            romaji: "Patcheel",
            korean: "얼루기",
            chinese: "晃晃斑",
            french: "Spinda",
            german: "Pandir",
            spanish: "Spinda",
            italian: "Spinda",
            english: "Spinda"
        }
    },
    trapinch: {
        num: 328,
        isdefault: true,
        baseExperience: 58,
        order: 395,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 6,
        generation: 3,
        color: 3,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 329,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ナックラー",
            romaji: "Nuckrar",
            korean: "톱치",
            chinese: "大顎蟻",
            french: "Kraknoix",
            german: "Knacklion",
            spanish: "Trapinch",
            italian: "Trapinch",
            english: "Trapinch"
        }
    },
    vibrava: {
        num: 329,
        isdefault: true,
        baseExperience: 119,
        order: 396,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 6,
        generation: 3,
        color: 5,
        shape: 13,
        genderDiff: 0,
        evomethods: [
            {
                num: 330,
                trigger: "levelup",
                level: 45,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ビブラーバ",
            romaji: "Vibrava",
            korean: "비브라바",
            chinese: "超音波幼蟲",
            french: "Vibraninf",
            german: "Vibrava",
            spanish: "Vibrava",
            italian: "Vibrava",
            english: "Vibrava"
        }
    },
    flygon: {
        num: 330,
        isdefault: true,
        baseExperience: 234,
        order: 397,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 6,
        generation: 3,
        color: 5,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "フライゴン",
            romaji: "Frygon",
            korean: "플라이곤",
            chinese: "沙漠蜻蜓",
            french: "Libégon",
            german: "Libelldra",
            spanish: "Flygon",
            italian: "Flygon",
            english: "Flygon"
        }
    },
    cacnea: {
        num: 331,
        isdefault: true,
        baseExperience: 67,
        order: 398,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 190,
        habitat: 6,
        generation: 3,
        color: 5,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 332,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "サボネア",
            romaji: "Sabonea",
            korean: "선인왕",
            chinese: "沙漠奈亞",
            french: "Cacnea",
            german: "Tuska",
            spanish: "Cacnea",
            italian: "Cacnea",
            english: "Cacnea"
        }
    },
    cacturne: {
        num: 332,
        isdefault: true,
        baseExperience: 166,
        order: 399,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 60,
        habitat: 6,
        generation: 3,
        color: 5,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ノクタス",
            romaji: "Noctus",
            korean: "밤선인",
            chinese: "夢歌奈亞",
            french: "Cacturne",
            german: "Noktuska",
            spanish: "Cacturne",
            italian: "Cacturne",
            english: "Cacturne"
        }
    },
    swablu: {
        num: 333,
        isdefault: true,
        baseExperience: 62,
        order: 400,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 2,
        generation: 3,
        color: 2,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 334,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チルット",
            romaji: "Tyltto",
            korean: "파비코",
            chinese: "青綿鳥",
            french: "Tylton",
            german: "Wablu",
            spanish: "Swablu",
            italian: "Swablu",
            english: "Swablu"
        }
    },
    altaria: {
        num: 334,
        isdefault: true,
        baseExperience: 172,
        order: 401,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 2,
        generation: 3,
        color: 2,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "チルタリス",
            romaji: "Tyltalis",
            korean: "파비코리",
            chinese: "七夕青鳥",
            french: "Altaria",
            german: "Altaria",
            spanish: "Altaria",
            italian: "Altaria",
            english: "Altaria"
        }
    },
    altariamega: {
        num: 334,
        isdefault: false,
        baseExperience: 207,
        order: 402
    },
    zangoose: {
        num: 335,
        isdefault: true,
        baseExperience: 160,
        order: 403,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 3,
        generation: 3,
        color: 9,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ザングース",
            romaji: "Zangoose",
            korean: "쟝고",
            chinese: "貓鼬斬",
            french: "Mangriff",
            german: "Sengo",
            spanish: "Zangoose",
            italian: "Zangoose",
            english: "Zangoose"
        }
    },
    seviper: {
        num: 336,
        isdefault: true,
        baseExperience: 160,
        order: 404,
        growthRate: 6,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 3,
        generation: 3,
        color: 1,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ハブネーク",
            romaji: "Habunake",
            korean: "세비퍼",
            chinese: "飯匙蛇",
            french: "Séviper",
            german: "Vipitis",
            spanish: "Seviper",
            italian: "Seviper",
            english: "Seviper"
        }
    },
    lunatone: {
        num: 337,
        isdefault: true,
        baseExperience: 154,
        order: 405,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 3,
        color: 10,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "ルナトーン",
            romaji: "Lunatone",
            korean: "루나톤",
            chinese: "月石",
            french: "Séléroc",
            german: "Lunastein",
            spanish: "Lunatone",
            italian: "Lunatone",
            english: "Lunatone"
        }
    },
    solrock: {
        num: 338,
        isdefault: true,
        baseExperience: 154,
        order: 406,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 1,
        generation: 3,
        color: 8,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "ソルロック",
            romaji: "Solrock",
            korean: "솔록",
            chinese: "太陽岩",
            french: "Solaroc",
            german: "Sonnfel",
            spanish: "Solrock",
            italian: "Solrock",
            english: "Solrock"
        }
    },
    barboach: {
        num: 339,
        isdefault: true,
        baseExperience: 58,
        order: 407,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 9,
        generation: 3,
        color: 4,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 340,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドジョッチ",
            romaji: "Dojoach",
            korean: "미꾸리",
            chinese: "泥泥鰍",
            french: "Barloche",
            german: "Schmerbe",
            spanish: "Barboach",
            italian: "Barboach",
            english: "Barboach"
        }
    },
    whiscash: {
        num: 340,
        isdefault: true,
        baseExperience: 164,
        order: 408,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ナマズン",
            romaji: "Namazun",
            korean: "메깅",
            chinese: "鯰魚王",
            french: "Barbicha",
            german: "Welsar",
            spanish: "Whiscash",
            italian: "Whiscash",
            english: "Whiscash"
        }
    },
    corphish: {
        num: 341,
        isdefault: true,
        baseExperience: 62,
        order: 409,
        growthRate: 6,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 205,
        habitat: 9,
        generation: 3,
        color: 8,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 342,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヘイガニ",
            romaji: "Heigani",
            korean: "가재군",
            chinese: "龍蝦小兵",
            french: "Écrapince",
            german: "Krebscorps",
            spanish: "Corphish",
            italian: "Corphish",
            english: "Corphish"
        }
    },
    crawdaunt: {
        num: 342,
        isdefault: true,
        baseExperience: 164,
        order: 410,
        growthRate: 6,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 155,
        habitat: 9,
        generation: 3,
        color: 8,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "シザリガー",
            romaji: "Shizariger",
            korean: "가재장군",
            chinese: "鐵螯龍蝦",
            french: "Colhomard",
            german: "Krebutack",
            spanish: "Crawdaunt",
            italian: "Crawdaunt",
            english: "Crawdaunt"
        }
    },
    baltoy: {
        num: 343,
        isdefault: true,
        baseExperience: 60,
        order: 411,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 6,
        generation: 3,
        color: 3,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 344,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤジロン",
            romaji: "Yajilon",
            korean: "오뚝군",
            chinese: "天秤偶",
            french: "Balbuto",
            german: "Puppance",
            spanish: "Baltoy",
            italian: "Baltoy",
            english: "Baltoy"
        }
    },
    claydol: {
        num: 344,
        isdefault: true,
        baseExperience: 175,
        order: 412,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: 6,
        generation: 3,
        color: 1,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ネンドール",
            romaji: "Nendoll",
            korean: "점토도리",
            chinese: "念力土偶",
            french: "Kaorine",
            german: "Lepumentas",
            spanish: "Claydol",
            italian: "Claydol",
            english: "Claydol"
        }
    },
    lileep: {
        num: 345,
        isdefault: true,
        baseExperience: 71,
        order: 413,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 3,
        color: 7,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 346,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "リリーラ",
            romaji: "Lilyla",
            korean: "릴링",
            chinese: "觸手百合",
            french: "Lilia",
            german: "Liliep",
            spanish: "Lileep",
            italian: "Lileep",
            english: "Lileep"
        }
    },
    cradily: {
        num: 346,
        isdefault: true,
        baseExperience: 173,
        order: 414,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 3,
        color: 5,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ユレイドル",
            romaji: "Yuradle",
            korean: "릴리요",
            chinese: "搖籃百合",
            french: "Vacilys",
            german: "Wielie",
            spanish: "Cradily",
            italian: "Cradily",
            english: "Cradily"
        }
    },
    anorith: {
        num: 347,
        isdefault: true,
        baseExperience: 71,
        order: 415,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 3,
        color: 4,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 348,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アノプス",
            romaji: "Anopth",
            korean: "아노딥스",
            chinese: "太古羽蟲",
            french: "Anorith",
            german: "Anorith",
            spanish: "Anorith",
            italian: "Anorith",
            english: "Anorith"
        }
    },
    armaldo: {
        num: 348,
        isdefault: true,
        baseExperience: 173,
        order: 416,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 9,
        generation: 3,
        color: 4,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "アーマルド",
            romaji: "Armaldo",
            korean: "아말도",
            chinese: "太古盔甲",
            french: "Armaldo",
            german: "Armaldo",
            spanish: "Armaldo",
            italian: "Armaldo",
            english: "Armaldo"
        }
    },
    feebas: {
        num: 349,
        isdefault: true,
        baseExperience: 40,
        order: 417,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 9,
        generation: 3,
        color: 3,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 350,
                trigger: "levelup",
                beauty: 171,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 350,
                trigger: "trade",
                helditem: 580,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒンバス",
            romaji: "Hinbass",
            korean: "빈티나",
            chinese: "笨笨魚",
            french: "Barpau",
            german: "Barschwa",
            spanish: "Feebas",
            italian: "Feebas",
            english: "Feebas"
        }
    },
    milotic: {
        num: 350,
        isdefault: true,
        baseExperience: 189,
        order: 418,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 9,
        generation: 3,
        color: 6,
        shape: 2,
        genderDiff: 1,
        names: {
            japanese: "ミロカロス",
            romaji: "Milokaross",
            korean: "밀로틱",
            chinese: "美納斯",
            french: "Milobellus",
            german: "Milotic",
            spanish: "Milotic",
            italian: "Milotic",
            english: "Milotic"
        }
    },
    castform: {
        num: 351,
        isdefault: true,
        baseExperience: 147,
        order: 419,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 9,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "ポワルン",
            romaji: "Powalen",
            korean: "캐스퐁",
            chinese: "漂浮泡泡",
            french: "Morphéo",
            german: "Formeo",
            spanish: "Castform",
            italian: "Castform",
            english: "Castform"
        }
    },
    castformsunny: {
        num: 351,
        isdefault: false,
        baseExperience: 147,
        order: 420
    },
    castformrainy: {
        num: 351,
        isdefault: false,
        baseExperience: 147,
        order: 421
    },
    castformsnowy: {
        num: 351,
        isdefault: false,
        baseExperience: 147,
        order: 422
    },
    kecleon: {
        num: 352,
        isdefault: true,
        baseExperience: 154,
        order: 423,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "カクレオン",
            romaji: "Kakureon",
            korean: "켈리몬",
            chinese: "變隱龍",
            french: "Kecleon",
            german: "Kecleon",
            spanish: "Kecleon",
            italian: "Kecleon",
            english: "Kecleon"
        }
    },
    shuppet: {
        num: 353,
        isdefault: true,
        baseExperience: 59,
        order: 424,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 225,
        habitat: 8,
        generation: 3,
        color: 1,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 354,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カゲボウズ",
            romaji: "Kagebouzu",
            korean: "어둠대신",
            chinese: "怨影娃娃",
            french: "Polichombr",
            german: "Shuppet",
            spanish: "Shuppet",
            italian: "Shuppet",
            english: "Shuppet"
        }
    },
    banette: {
        num: 354,
        isdefault: true,
        baseExperience: 159,
        order: 425,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 8,
        generation: 3,
        color: 1,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ジュペッタ",
            romaji: "Juppeta",
            korean: "다크펫",
            chinese: "詛咒娃娃",
            french: "Branette",
            german: "Banette",
            spanish: "Banette",
            italian: "Banette",
            english: "Banette"
        }
    },
    banettemega: {
        num: 354,
        isdefault: false,
        baseExperience: 194,
        order: 426
    },
    duskull: {
        num: 355,
        isdefault: true,
        baseExperience: 59,
        order: 427,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 190,
        habitat: 2,
        generation: 3,
        color: 1,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 356,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヨマワル",
            romaji: "Yomawaru",
            korean: "해골몽",
            chinese: "夜骷髏",
            french: "Skelénox",
            german: "Zwirrlicht",
            spanish: "Duskull",
            italian: "Duskull",
            english: "Duskull"
        }
    },
    dusclops: {
        num: 356,
        isdefault: true,
        baseExperience: 159,
        order: 428,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 90,
        habitat: 2,
        generation: 3,
        color: 1,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 477,
                trigger: "trade",
                helditem: 302,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "サマヨール",
            romaji: "Samayouru",
            korean: "미라몽",
            chinese: "夜巨人",
            french: "Téraclope",
            german: "Zwirrklop",
            spanish: "Dusclops",
            italian: "Dusclops",
            english: "Dusclops"
        }
    },
    tropius: {
        num: 357,
        isdefault: true,
        baseExperience: 161,
        order: 430,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: 2,
        generation: 3,
        color: 5,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "トロピウス",
            romaji: "Tropius",
            korean: "트로피우스",
            chinese: "熱帶龍",
            french: "Tropius",
            german: "Tropius",
            spanish: "Tropius",
            italian: "Tropius",
            english: "Tropius"
        }
    },
    chimecho: {
        num: 358,
        isdefault: true,
        baseExperience: 149,
        order: 432,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 3,
        generation: 3,
        color: 2,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "チリーン",
            romaji: "Chirean",
            korean: "치렁",
            chinese: "風鈴鈴",
            french: "Éoko",
            german: "Palimpalim",
            spanish: "Chimecho",
            italian: "Chimecho",
            english: "Chimecho"
        }
    },
    absol: {
        num: 359,
        isdefault: true,
        baseExperience: 163,
        order: 433,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 30,
        habitat: 4,
        generation: 3,
        color: 9,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "アブソル",
            romaji: "Absol",
            korean: "앱솔",
            chinese: "阿勃梭魯",
            french: "Absol",
            german: "Absol",
            spanish: "Absol",
            italian: "Absol",
            english: "Absol"
        }
    },
    absolmega: {
        num: 359,
        isdefault: false,
        baseExperience: 198,
        order: 434
    },
    wynaut: {
        num: 360,
        isdefault: true,
        baseExperience: 52,
        order: 256,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 125,
        habitat: 1,
        generation: 3,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 202,
                trigger: "levelup",
                level: 15,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ソーナノ",
            romaji: "Sohnano",
            korean: "마자",
            chinese: "小果然",
            french: "Okéoké",
            german: "Isso",
            spanish: "Wynaut",
            italian: "Wynaut",
            english: "Wynaut"
        }
    },
    snorunt: {
        num: 361,
        isdefault: true,
        baseExperience: 60,
        order: 435,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: 1,
        generation: 3,
        color: 4,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 362,
                trigger: "levelup",
                level: 42,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 478,
                trigger: "useitem",
                itemtrigger: "dawnstone",
                gender: 1,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ユキワラシ",
            romaji: "Yukiwarashi",
            korean: "눈꼬마",
            chinese: "雪童子",
            french: "Stalgamin",
            german: "Schneppke",
            spanish: "Snorunt",
            italian: "Snorunt",
            english: "Snorunt"
        }
    },
    glalie: {
        num: 362,
        isdefault: true,
        baseExperience: 168,
        order: 436,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: 1,
        generation: 3,
        color: 4,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "オニゴーリ",
            romaji: "Onigohri",
            korean: "얼음귀신",
            chinese: "冰鬼護",
            french: "Oniglali",
            german: "Firnontor",
            spanish: "Glalie",
            italian: "Glalie",
            english: "Glalie"
        }
    },
    glaliemega: {
        num: 362,
        isdefault: false,
        baseExperience: 203,
        order: 437
    },
    spheal: {
        num: 363,
        isdefault: true,
        baseExperience: 58,
        order: 439,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 364,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タマザラシ",
            romaji: "Tamazarashi",
            korean: "대굴레오",
            chinese: "海豹球",
            french: "Obalie",
            german: "Seemops",
            spanish: "Spheal",
            italian: "Spheal",
            english: "Spheal"
        }
    },
    sealeo: {
        num: 364,
        isdefault: true,
        baseExperience: 144,
        order: 440,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 365,
                trigger: "levelup",
                level: 44,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "トドグラー",
            romaji: "Todoggler",
            korean: "씨레오",
            chinese: "海魔獅",
            french: "Phogleur",
            german: "Seejong",
            spanish: "Sealeo",
            italian: "Sealeo",
            english: "Sealeo"
        }
    },
    walrein: {
        num: 365,
        isdefault: true,
        baseExperience: 239,
        order: 441,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "トドゼルガ",
            romaji: "Todoseruga",
            korean: "씨카이저",
            chinese: "帝牙海獅",
            french: "Kaimorse",
            german: "Walraisa",
            spanish: "Walrein",
            italian: "Walrein",
            english: "Walrein"
        }
    },
    clamperl: {
        num: 366,
        isdefault: true,
        baseExperience: 69,
        order: 442,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 367,
                trigger: "trade",
                helditem: 203,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 368,
                trigger: "trade",
                helditem: 204,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "パールル",
            romaji: "Pearlulu",
            korean: "진주몽",
            chinese: "珍珠貝",
            french: "Coquiperl",
            german: "Perlu",
            spanish: "Clamperl",
            italian: "Clamperl",
            english: "Clamperl"
        }
    },
    huntail: {
        num: 367,
        isdefault: true,
        baseExperience: 170,
        order: 443,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ハンテール",
            romaji: "Huntail",
            korean: "헌테일",
            chinese: "獵斑魚",
            french: "Serpang",
            german: "Aalabyss",
            spanish: "Huntail",
            italian: "Huntail",
            english: "Huntail"
        }
    },
    gorebyss: {
        num: 368,
        isdefault: true,
        baseExperience: 170,
        order: 444,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: 7,
        generation: 3,
        color: 6,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "サクラビス",
            romaji: "Sakurabyss",
            korean: "분홍장이",
            chinese: "櫻花魚",
            french: "Rosabyss",
            german: "Saganabyss",
            spanish: "Gorebyss",
            italian: "Gorebyss",
            english: "Gorebyss"
        }
    },
    relicanth: {
        num: 369,
        isdefault: true,
        baseExperience: 170,
        order: 445,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: 7,
        generation: 3,
        color: 4,
        shape: 3,
        genderDiff: 1,
        names: {
            japanese: "ジーランス",
            romaji: "Glanth",
            korean: "시라칸",
            chinese: "古空棘魚",
            french: "Relicanth",
            german: "Relicanth",
            spanish: "Relicanth",
            italian: "Relicanth",
            english: "Relicanth"
        }
    },
    luvdisc: {
        num: 370,
        isdefault: true,
        baseExperience: 116,
        order: 446,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: 7,
        generation: 3,
        color: 6,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ラブカス",
            romaji: "Lovecus",
            korean: "사랑동이",
            chinese: "愛心魚",
            french: "Lovdisc",
            german: "Liebiskus",
            spanish: "Luvdisc",
            italian: "Luvdisc",
            english: "Luvdisc"
        }
    },
    bagon: {
        num: 371,
        isdefault: true,
        baseExperience: 60,
        order: 447,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 6,
        generation: 3,
        color: 2,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 372,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タツベイ",
            romaji: "Tatsubay",
            korean: "아공이",
            chinese: "寶貝龍",
            french: "Draby",
            german: "Kindwurm",
            spanish: "Bagon",
            italian: "Bagon",
            english: "Bagon"
        }
    },
    shelgon: {
        num: 372,
        isdefault: true,
        baseExperience: 147,
        order: 448,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 6,
        generation: 3,
        color: 9,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 373,
                trigger: "levelup",
                level: 50,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コモルー",
            romaji: "Komoruu",
            korean: "쉘곤",
            chinese: "甲殼龍",
            french: "Drackhaus",
            german: "Draschel",
            spanish: "Shelgon",
            italian: "Shelgon",
            english: "Shelgon"
        }
    },
    salamence: {
        num: 373,
        isdefault: true,
        baseExperience: 270,
        order: 449,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: 6,
        generation: 3,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ボーマンダ",
            romaji: "Bohmander",
            korean: "보만다",
            chinese: "暴蠑螈",
            french: "Drattak",
            german: "Brutalanda",
            spanish: "Salamence",
            italian: "Salamence",
            english: "Salamence"
        }
    },
    salamencemega: {
        num: 373,
        isdefault: false,
        baseExperience: 315,
        order: 450
    },
    beldum: {
        num: 374,
        isdefault: true,
        baseExperience: 60,
        order: 451,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 6,
        generation: 3,
        color: 2,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 375,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ダンバル",
            romaji: "Dumbber",
            korean: "메탕",
            chinese: "鐵啞鈴",
            french: "Terhal",
            german: "Tanhel",
            spanish: "Beldum",
            italian: "Beldum",
            english: "Beldum"
        }
    },
    metang: {
        num: 375,
        isdefault: true,
        baseExperience: 147,
        order: 452,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 6,
        generation: 3,
        color: 2,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 376,
                trigger: "levelup",
                level: 45,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "メタング",
            romaji: "Metang",
            korean: "메탕구",
            chinese: "金屬怪",
            french: "Métang",
            german: "Metang",
            spanish: "Metang",
            italian: "Metang",
            english: "Metang"
        }
    },
    metagross: {
        num: 376,
        isdefault: true,
        baseExperience: 270,
        order: 453,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 6,
        generation: 3,
        color: 2,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "メタグロス",
            romaji: "Metagross",
            korean: "메타그로스",
            chinese: "巨金怪",
            french: "Métalosse",
            german: "Metagross",
            spanish: "Metagross",
            italian: "Metagross",
            english: "Metagross"
        }
    },
    metagrossmega: {
        num: 376,
        isdefault: false,
        baseExperience: 315,
        order: 454
    },
    regirock: {
        num: 377,
        isdefault: true,
        baseExperience: 261,
        order: 455,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 1,
        generation: 3,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "レジロック",
            romaji: "Regirock",
            korean: "레지락",
            chinese: "雷吉洛克",
            french: "Regirock",
            german: "Regirock",
            spanish: "Regirock",
            italian: "Regirock",
            english: "Regirock"
        }
    },
    regice: {
        num: 378,
        isdefault: true,
        baseExperience: 261,
        order: 456,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 1,
        generation: 3,
        color: 2,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "レジアイス",
            romaji: "Regice",
            korean: "레지아이스",
            chinese: "雷吉艾斯",
            french: "Regice",
            german: "Regice",
            spanish: "Regice",
            italian: "Regice",
            english: "Regice"
        }
    },
    registeel: {
        num: 379,
        isdefault: true,
        baseExperience: 261,
        order: 457,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: 1,
        generation: 3,
        color: 4,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "レジスチル",
            romaji: "Registeel",
            korean: "레지스틸",
            chinese: "雷吉斯奇魯",
            french: "Registeel",
            german: "Registeel",
            spanish: "Registeel",
            italian: "Registeel",
            english: "Registeel"
        }
    },
    latias: {
        num: 380,
        isdefault: true,
        baseExperience: 270,
        order: 458,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 90,
        captureRate: 3,
        habitat: 9,
        generation: 3,
        color: 8,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ラティアス",
            romaji: "Latias",
            korean: "라티아스",
            chinese: "拉帝亞斯",
            french: "Latias",
            german: "Latias",
            spanish: "Latias",
            italian: "Latias",
            english: "Latias"
        }
    },
    latiasmega: {
        num: 380,
        isdefault: false,
        baseExperience: 315,
        order: 459
    },
    latios: {
        num: 381,
        isdefault: true,
        baseExperience: 270,
        order: 460,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 90,
        captureRate: 3,
        habitat: 9,
        generation: 3,
        color: 2,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ラティオス",
            romaji: "Latios",
            korean: "라티오스",
            chinese: "拉帝歐斯",
            french: "Latios",
            german: "Latios",
            spanish: "Latios",
            italian: "Latios",
            english: "Latios"
        }
    },
    latiosmega: {
        num: 381,
        isdefault: false,
        baseExperience: 315,
        order: 461
    },
    kyogre: {
        num: 382,
        isdefault: true,
        baseExperience: 302,
        order: 462,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 5,
        habitat: 7,
        generation: 3,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "カイオーガ",
            romaji: "Kyogre",
            korean: "가이오가",
            chinese: "蓋歐卡",
            french: "Kyogre",
            german: "Kyogre",
            spanish: "Kyogre",
            italian: "Kyogre",
            english: "Kyogre"
        }
    },
    kyogreprimal: {
        num: 382,
        isdefault: false,
        baseExperience: 347,
        order: 463
    },
    groudon: {
        num: 383,
        isdefault: true,
        baseExperience: 302,
        order: 464,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 5,
        habitat: 6,
        generation: 3,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "グラードン",
            romaji: "Groudon",
            korean: "그란돈",
            chinese: "固拉多",
            french: "Groudon",
            german: "Groudon",
            spanish: "Groudon",
            italian: "Groudon",
            english: "Groudon"
        }
    },
    groudonprimal: {
        num: 383,
        isdefault: false,
        baseExperience: 347,
        order: 465
    },
    rayquaza: {
        num: 384,
        isdefault: true,
        baseExperience: 306,
        order: 466,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: 5,
        generation: 3,
        color: 5,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "レックウザ",
            romaji: "Rayquaza",
            korean: "레쿠쟈",
            chinese: "烈空坐",
            french: "Rayquaza",
            german: "Rayquaza",
            spanish: "Rayquaza",
            italian: "Rayquaza",
            english: "Rayquaza"
        }
    },
    rayquazamega: {
        num: 384,
        isdefault: false,
        baseExperience: 351,
        order: 467
    },
    jirachi: {
        num: 385,
        isdefault: true,
        baseExperience: 270,
        order: 468,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: 4,
        generation: 3,
        color: 10,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ジラーチ",
            romaji: "Jirachi",
            korean: "지라치",
            chinese: "基拉祈",
            french: "Jirachi",
            german: "Jirachi",
            spanish: "Jirachi",
            italian: "Jirachi",
            english: "Jirachi"
        }
    },
    deoxys: {
        num: 386,
        isdefault: true,
        baseExperience: 270,
        order: 469,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: 5,
        generation: 3,
        color: 8,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "デオキシス",
            romaji: "Deoxys",
            korean: "테오키스",
            chinese: "代歐奇希斯",
            french: "Deoxys",
            german: "Deoxys",
            spanish: "Deoxys",
            italian: "Deoxys",
            english: "Deoxys"
        }
    },
    deoxysattack: {
        num: 386,
        isdefault: false,
        baseExperience: 270,
        order: 470
    },
    deoxysdefense: {
        num: 386,
        isdefault: false,
        baseExperience: 270,
        order: 471
    },
    deoxysspeed: {
        num: 386,
        isdefault: false,
        baseExperience: 270,
        order: 472
    },
    turtwig: {
        num: 387,
        isdefault: true,
        baseExperience: 64,
        order: 473,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 388,
                trigger: "levelup",
                level: 18,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ナエトル",
            romaji: "Naetle",
            korean: "모부기",
            chinese: "草苗龜",
            french: "Tortipouss",
            german: "Chelast",
            spanish: "Turtwig",
            italian: "Turtwig",
            english: "Turtwig"
        }
    },
    grotle: {
        num: 388,
        isdefault: true,
        baseExperience: 142,
        order: 474,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 389,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハヤシガメ",
            romaji: "Hayashigame",
            korean: "수풀부기",
            chinese: "樹林龜",
            french: "Boskara",
            german: "Chelcarain",
            spanish: "Grotle",
            italian: "Grotle",
            english: "Grotle"
        }
    },
    torterra: {
        num: 389,
        isdefault: true,
        baseExperience: 236,
        order: 475,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ドダイトス",
            romaji: "Dodaitose",
            korean: "토대부기",
            chinese: "土台龜",
            french: "Torterra",
            german: "Chelterrar",
            spanish: "Torterra",
            italian: "Torterra",
            english: "Torterra"
        }
    },
    chimchar: {
        num: 390,
        isdefault: true,
        baseExperience: 62,
        order: 476,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 391,
                trigger: "levelup",
                level: 14,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒコザル",
            romaji: "Hikozaru",
            korean: "불꽃숭이",
            chinese: "小火焰猴",
            french: "Ouisticram",
            german: "Panflam",
            spanish: "Chimchar",
            italian: "Chimchar",
            english: "Chimchar"
        }
    },
    monferno: {
        num: 391,
        isdefault: true,
        baseExperience: 142,
        order: 477,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 392,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "モウカザル",
            romaji: "Mōkazaru",
            korean: "파이숭이",
            chinese: "猛火猴",
            french: "Chimpenfeu",
            german: "Panpyro",
            spanish: "Monferno",
            italian: "Monferno",
            english: "Monferno"
        }
    },
    infernape: {
        num: 392,
        isdefault: true,
        baseExperience: 240,
        order: 478,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ゴウカザル",
            romaji: "Gōkazaru",
            korean: "초염몽",
            chinese: "烈焰猴",
            french: "Simiabraz",
            german: "Panferno",
            spanish: "Infernape",
            italian: "Infernape",
            english: "Infernape"
        }
    },
    piplup: {
        num: 393,
        isdefault: true,
        baseExperience: 63,
        order: 479,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 394,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポッチャマ",
            romaji: "Pochama",
            korean: "팽도리",
            chinese: "波加曼",
            french: "Tiplouf",
            german: "Plinfa",
            spanish: "Piplup",
            italian: "Piplup",
            english: "Piplup"
        }
    },
    prinplup: {
        num: 394,
        isdefault: true,
        baseExperience: 142,
        order: 480,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 395,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポッタイシ",
            romaji: "Pottaishi",
            korean: "팽태자",
            chinese: "波皇子",
            french: "Prinplouf",
            german: "Pliprin",
            spanish: "Prinplup",
            italian: "Prinplup",
            english: "Prinplup"
        }
    },
    empoleon: {
        num: 395,
        isdefault: true,
        baseExperience: 239,
        order: 481,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "エンペルト",
            romaji: "Emperte",
            korean: "엠페르트",
            chinese: "帝王拿波",
            french: "Pingoléon",
            german: "Impoleon",
            spanish: "Empoleon",
            italian: "Empoleon",
            english: "Empoleon"
        }
    },
    starly: {
        num: 396,
        isdefault: true,
        baseExperience: 49,
        order: 482,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 397,
                trigger: "levelup",
                level: 14,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ムックル",
            romaji: "Mukkuru",
            korean: "찌르꼬",
            chinese: "姆克兒",
            french: "Étourmi",
            german: "Staralili",
            spanish: "Starly",
            italian: "Starly",
            english: "Starly"
        }
    },
    staravia: {
        num: 397,
        isdefault: true,
        baseExperience: 119,
        order: 483,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 9,
        genderDiff: 1,
        evomethods: [
            {
                num: 398,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ムクバード",
            romaji: "Mukubird",
            korean: "찌르버드",
            chinese: "姆克鳥",
            french: "Étourvol",
            german: "Staravia",
            spanish: "Staravia",
            italian: "Staravia",
            english: "Staravia"
        }
    },
    staraptor: {
        num: 398,
        isdefault: true,
        baseExperience: 218,
        order: 484,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 9,
        genderDiff: 1,
        names: {
            japanese: "ムクホーク",
            romaji: "Mukuhawk",
            korean: "찌르호크",
            chinese: "姆克鷹",
            french: "Étouraptor",
            german: "Staraptor",
            spanish: "Staraptor",
            italian: "Staraptor",
            english: "Staraptor"
        }
    },
    bidoof: {
        num: 399,
        isdefault: true,
        baseExperience: 50,
        order: 485,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 400,
                trigger: "levelup",
                level: 15,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ビッパ",
            romaji: "Bipper",
            korean: "비버니",
            chinese: "大牙狸",
            french: "Keunotor",
            german: "Bidiza",
            spanish: "Bidoof",
            italian: "Bidoof",
            english: "Bidoof"
        }
    },
    bibarel: {
        num: 400,
        isdefault: true,
        baseExperience: 144,
        order: 486,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 127,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ビーダル",
            romaji: "Beadull",
            korean: "비버통",
            chinese: "大尾狸",
            french: "Castorno",
            german: "Bidifas",
            spanish: "Bibarel",
            italian: "Bibarel",
            english: "Bibarel"
        }
    },
    kricketot: {
        num: 401,
        isdefault: true,
        baseExperience: 39,
        order: 487,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 4,
        color: 8,
        shape: 12,
        genderDiff: 1,
        evomethods: [
            {
                num: 402,
                trigger: "levelup",
                level: 10,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コロボーシ",
            romaji: "Korobohshi",
            korean: "귀뚤뚜기",
            chinese: "圓法師",
            french: "Crikzik",
            german: "Zirpurze",
            spanish: "Kricketot",
            italian: "Kricketot",
            english: "Kricketot"
        }
    },
    kricketune: {
        num: 402,
        isdefault: true,
        baseExperience: 134,
        order: 488,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 8,
        shape: 13,
        genderDiff: 1,
        names: {
            japanese: "コロトック",
            romaji: "Korotok",
            korean: "귀뚤톡크",
            chinese: "音箱蟀",
            french: "Mélokrik",
            german: "Zirpeise",
            spanish: "Kricketune",
            italian: "Kricketune",
            english: "Kricketune"
        }
    },
    shinx: {
        num: 403,
        isdefault: true,
        baseExperience: 53,
        order: 489,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 235,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 404,
                trigger: "levelup",
                level: 15,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コリンク",
            romaji: "Kolink",
            korean: "꼬링크",
            chinese: "小貓怪",
            french: "Lixy",
            german: "Sheinux",
            spanish: "Shinx",
            italian: "Shinx",
            english: "Shinx"
        }
    },
    luxio: {
        num: 404,
        isdefault: true,
        baseExperience: 127,
        order: 490,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 405,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ルクシオ",
            romaji: "Luxio",
            korean: "럭시오",
            chinese: "勒克貓",
            french: "Luxio",
            german: "Luxio",
            spanish: "Luxio",
            italian: "Luxio",
            english: "Luxio"
        }
    },
    luxray: {
        num: 405,
        isdefault: true,
        baseExperience: 235,
        order: 491,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "レントラー",
            romaji: "Rentorar",
            korean: "렌트라",
            chinese: "倫琴貓",
            french: "Luxray",
            german: "Luxtra",
            spanish: "Luxray",
            italian: "Luxray",
            english: "Luxray"
        }
    },
    budew: {
        num: 406,
        isdefault: true,
        baseExperience: 56,
        order: 378,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 315,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "スボミー",
            romaji: "Subomie",
            korean: "꼬몽울",
            chinese: "含羞苞",
            french: "Rozbouton",
            german: "Knospi",
            spanish: "Budew",
            italian: "Budew",
            english: "Budew"
        }
    },
    roserade: {
        num: 407,
        isdefault: true,
        baseExperience: 232,
        order: 380,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ロズレイド",
            romaji: "Roserade",
            korean: "로즈레이드",
            chinese: "羅絲雷朵",
            french: "Roserade",
            german: "Roserade",
            spanish: "Roserade",
            italian: "Roserade",
            english: "Roserade"
        }
    },
    cranidos: {
        num: 408,
        isdefault: true,
        baseExperience: 70,
        order: 492,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 409,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ズガイドス",
            romaji: "Zugaidos",
            korean: "두개도스",
            chinese: "頭蓋龍",
            french: "Kranidos",
            german: "Koknodon",
            spanish: "Cranidos",
            italian: "Cranidos",
            english: "Cranidos"
        }
    },
    rampardos: {
        num: 409,
        isdefault: true,
        baseExperience: 173,
        order: 493,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ラムパルド",
            romaji: "Rampard",
            korean: "램펄드",
            chinese: "戰槌龍",
            french: "Charkos",
            german: "Rameidon",
            spanish: "Rampardos",
            italian: "Rampardos",
            english: "Rampardos"
        }
    },
    shieldon: {
        num: 410,
        isdefault: true,
        baseExperience: 70,
        order: 494,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 411,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タテトプス",
            romaji: "Tatetops",
            korean: "방패톱스",
            chinese: "盾甲龍",
            french: "Dinoclier",
            german: "Schilterus",
            spanish: "Shieldon",
            italian: "Shieldon",
            english: "Shieldon"
        }
    },
    bastiodon: {
        num: 411,
        isdefault: true,
        baseExperience: 173,
        order: 495,
        growthRate: 5,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "トリデプス",
            romaji: "Trideps",
            korean: "바리톱스",
            chinese: "護城龍",
            french: "Bastiodon",
            german: "Bollterus",
            spanish: "Bastiodon",
            italian: "Bastiodon",
            english: "Bastiodon"
        }
    },
    burmy: {
        num: 412,
        isdefault: true,
        baseExperience: 45,
        order: 496,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 413,
                trigger: "levelup",
                level: 20,
                gender: 1,
                rain: 0,
                upsidedown: 0
            },
            {
                num: 414,
                trigger: "levelup",
                level: 20,
                gender: 2,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミノムッチ",
            romaji: "Minomucchi",
            korean: "도롱충이",
            chinese: "結草兒",
            french: "Cheniti",
            german: "Burmy",
            spanish: "Burmy",
            italian: "Burmy",
            english: "Burmy"
        }
    },
    wormadam: {
        num: 413,
        isdefault: true,
        baseExperience: 148,
        order: 497,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ミノマダム",
            romaji: "Minomadam",
            korean: "도롱마담",
            chinese: "結草貴婦",
            french: "Cheniselle",
            german: "Burmadame",
            spanish: "Wormadam",
            italian: "Wormadam",
            english: "Wormadam"
        }
    },
    wormadamsandy: {
        num: 413,
        isdefault: false,
        baseExperience: 148,
        order: 498
    },
    wormadamtrash: {
        num: 413,
        isdefault: false,
        baseExperience: 148,
        order: 499
    },
    mothim: {
        num: 414,
        isdefault: true,
        baseExperience: 148,
        order: 500,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "ガーメイル",
            romaji: "Garmeil",
            korean: "나메일",
            chinese: "紳士蛾",
            french: "Papilord",
            german: "Moterpel",
            spanish: "Mothim",
            italian: "Mothim",
            english: "Mothim"
        }
    },
    combee: {
        num: 415,
        isdefault: true,
        baseExperience: 49,
        order: 501,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 11,
        genderDiff: 1,
        evomethods: [
            {
                num: 416,
                trigger: "levelup",
                level: 21,
                gender: 1,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミツハニー",
            romaji: "Mitsuhoney",
            korean: "세꿀버리",
            chinese: "三蜜蜂",
            french: "Apitrini",
            german: "Wadribie",
            spanish: "Combee",
            italian: "Combee",
            english: "Combee"
        }
    },
    vespiquen: {
        num: 416,
        isdefault: true,
        baseExperience: 166,
        order: 502,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ビークイン",
            romaji: "Beequeen",
            korean: "비퀸",
            chinese: "蜂后",
            french: "Apireine",
            german: "Honweisel",
            spanish: "Vespiquen",
            italian: "Vespiquen",
            english: "Vespiquen"
        }
    },
    pachirisu: {
        num: 417,
        isdefault: true,
        baseExperience: 142,
        order: 503,
        growthRate: 2,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 200,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "パチリス",
            romaji: "Pachirisu",
            korean: "파치리스",
            chinese: "帕奇利茲",
            french: "Pachirisu",
            german: "Pachirisu",
            spanish: "Pachirisu",
            italian: "Pachirisu",
            english: "Pachirisu"
        }
    },
    buizel: {
        num: 418,
        isdefault: true,
        baseExperience: 66,
        order: 504,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 419,
                trigger: "levelup",
                level: 26,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ブイゼル",
            romaji: "Buoysel",
            korean: "브이젤",
            chinese: "泳氣鼬",
            french: "Mustébouée",
            german: "Bamelin",
            spanish: "Buizel",
            italian: "Buizel",
            english: "Buizel"
        }
    },
    floatzel: {
        num: 419,
        isdefault: true,
        baseExperience: 173,
        order: 505,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "フローゼル",
            romaji: "Flowsel",
            korean: "플로젤",
            chinese: "浮潛鼬",
            french: "Mustéflott",
            german: "Bojelin",
            spanish: "Floatzel",
            italian: "Floatzel",
            english: "Floatzel"
        }
    },
    cherubi: {
        num: 420,
        isdefault: true,
        baseExperience: 55,
        order: 506,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 4,
        color: 6,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 421,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チェリンボ",
            romaji: "Cherinbo",
            korean: "체리버",
            chinese: "櫻花寶",
            french: "Ceribou",
            german: "Kikugi",
            spanish: "Cherubi",
            italian: "Cherubi",
            english: "Cherubi"
        }
    },
    cherrim: {
        num: 421,
        isdefault: true,
        baseExperience: 158,
        order: 507,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 6,
        shape: 7,
        genderDiff: 0,
        names: {
            japanese: "チェリム",
            romaji: "Cherrim",
            korean: "체리꼬",
            chinese: "櫻花兒",
            french: "Ceriflor",
            german: "Kinoso",
            spanish: "Cherrim",
            italian: "Cherrim",
            english: "Cherrim"
        }
    },
    cherrimsunshine: {
        num: 421,
        isdefault: false
    },
    shellos: {
        num: 422,
        isdefault: true,
        baseExperience: 65,
        order: 508,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 423,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カラナクシ",
            romaji: "Karanakushi",
            korean: "깝질무",
            chinese: "無殼海牛",
            french: "Sancoki",
            german: "Schalellos",
            spanish: "Shellos",
            italian: "Shellos",
            english: "Shellos"
        }
    },
    gastrodon: {
        num: 423,
        isdefault: true,
        baseExperience: 166,
        order: 509,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "トリトドン",
            romaji: "Toritodon",
            korean: "트리토돈",
            chinese: "海牛獸",
            french: "Tritosor",
            german: "Gastrodon",
            spanish: "Gastrodon",
            italian: "Gastrodon",
            english: "Gastrodon"
        }
    },
    ambipom: {
        num: 424,
        isdefault: true,
        baseExperience: 169,
        order: 244,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "エテボース",
            romaji: "Eteboth",
            korean: "겟핸보숭",
            chinese: "雙尾怪手",
            french: "Capidextre",
            german: "Ambidiffel",
            spanish: "Ambipom",
            italian: "Ambipom",
            english: "Ambipom"
        }
    },
    drifloon: {
        num: 425,
        isdefault: true,
        baseExperience: 70,
        order: 510,
        growthRate: 6,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 125,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 426,
                trigger: "levelup",
                level: 28,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フワンテ",
            romaji: "Fuwante",
            korean: "흔들풍손",
            chinese: "飄飄球",
            french: "Baudrive",
            german: "Driftlon",
            spanish: "Drifloon",
            italian: "Drifloon",
            english: "Drifloon"
        }
    },
    drifblim: {
        num: 426,
        isdefault: true,
        baseExperience: 174,
        order: 511,
        growthRate: 6,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "フワライド",
            romaji: "Fuwaride",
            korean: "둥실라이드",
            chinese: "附和氣球",
            french: "Grodrive",
            german: "Drifzepeli",
            spanish: "Drifblim",
            italian: "Drifblim",
            english: "Drifblim"
        }
    },
    buneary: {
        num: 427,
        isdefault: true,
        baseExperience: 70,
        order: 512,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 190,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 428,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミミロル",
            romaji: "Mimirol",
            korean: "이어롤",
            chinese: "捲捲耳",
            french: "Laporeille",
            german: "Haspiror",
            spanish: "Buneary",
            italian: "Buneary",
            english: "Buneary"
        }
    },
    lopunny: {
        num: 428,
        isdefault: true,
        baseExperience: 168,
        order: 513,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 60,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ミミロップ",
            romaji: "Mimilop",
            korean: "이어롭",
            chinese: "長耳兔",
            french: "Lockpin",
            german: "Schlapor",
            spanish: "Lopunny",
            italian: "Lopunny",
            english: "Lopunny"
        }
    },
    lopunnymega: {
        num: 428,
        isdefault: false,
        baseExperience: 203,
        order: 514
    },
    mismagius: {
        num: 429,
        isdefault: true,
        baseExperience: 173,
        order: 254,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "ムウマージ",
            romaji: "Mumage",
            korean: "무우마직",
            chinese: "夢妖魔",
            french: "Magirêve",
            german: "Traunmagil",
            spanish: "Mismagius",
            italian: "Mismagius",
            english: "Mismagius"
        }
    },
    honchkrow: {
        num: 430,
        isdefault: true,
        baseExperience: 177,
        order: 252,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ドンカラス",
            romaji: "Donkarasu",
            korean: "돈크로우",
            chinese: "烏鴉頭頭",
            french: "Corboss",
            german: "Kramshef",
            spanish: "Honchkrow",
            italian: "Honchkrow",
            english: "Honchkrow"
        }
    },
    glameow: {
        num: 431,
        isdefault: true,
        baseExperience: 62,
        order: 515,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 432,
                trigger: "levelup",
                level: 38,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニャルマー",
            romaji: "Nyarmar",
            korean: "나옹마",
            chinese: "魅力喵",
            french: "Chaglam",
            german: "Charmian",
            spanish: "Glameow",
            italian: "Glameow",
            english: "Glameow"
        }
    },
    purugly: {
        num: 432,
        isdefault: true,
        baseExperience: 158,
        order: 516,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ブニャット",
            romaji: "Bunyat",
            korean: "몬냥이",
            chinese: "東施喵",
            french: "Chaffreux",
            german: "Shnurgarst",
            spanish: "Purugly",
            italian: "Purugly",
            english: "Purugly"
        }
    },
    chingling: {
        num: 433,
        isdefault: true,
        baseExperience: 57,
        order: 431,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 358,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "リーシャン",
            romaji: "Lisyan",
            korean: "랑딸랑",
            chinese: "鈴鐺響",
            french: "Korillon",
            german: "Klingplim",
            spanish: "Chingling",
            italian: "Chingling",
            english: "Chingling"
        }
    },
    stunky: {
        num: 434,
        isdefault: true,
        baseExperience: 66,
        order: 517,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 435,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "スカンプー",
            romaji: "Skunpoo",
            korean: "스컹뿡",
            chinese: "臭鼬噗",
            french: "Moufouette",
            german: "Skunkapuh",
            spanish: "Stunky",
            italian: "Stunky",
            english: "Stunky"
        }
    },
    skuntank: {
        num: 435,
        isdefault: true,
        baseExperience: 168,
        order: 518,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "スカタンク",
            romaji: "Skutank",
            korean: "스컹탱크",
            chinese: "坦克臭鼬",
            french: "Moufflair",
            german: "Skuntank",
            spanish: "Skuntank",
            italian: "Skuntank",
            english: "Skuntank"
        }
    },
    bronzor: {
        num: 436,
        isdefault: true,
        baseExperience: 60,
        order: 519,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 437,
                trigger: "levelup",
                level: 33,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドーミラー",
            romaji: "Domirror",
            korean: "동미러",
            chinese: "銅鏡怪",
            french: "Archéomire",
            german: "Bronzel",
            spanish: "Bronzor",
            italian: "Bronzor",
            english: "Bronzor"
        }
    },
    bronzong: {
        num: 437,
        isdefault: true,
        baseExperience: 175,
        order: 520,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ドータクン",
            romaji: "Dotakun",
            korean: "동탁군",
            chinese: "青銅鐘",
            french: "Archéodong",
            german: "Bronzong",
            spanish: "Bronzong",
            italian: "Bronzong",
            english: "Bronzong"
        }
    },
    bonsly: {
        num: 438,
        isdefault: true,
        baseExperience: 58,
        order: 238,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 185,
                trigger: "levelup",
                move: 102,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ウソハチ",
            romaji: "Usohachi",
            korean: "꼬지지",
            chinese: "愛哭樹",
            french: "Manzaï",
            german: "Mobai",
            spanish: "Bonsly",
            italian: "Bonsly",
            english: "Bonsly"
        }
    },
    mimejr: {
        num: 439,
        isdefault: true,
        baseExperience: 62,
        order: 156,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 145,
        habitat: null,
        generation: 4,
        color: 6,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 122,
                trigger: "levelup",
                move: 102,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マネネ",
            romaji: "Manene",
            korean: "흉내내",
            chinese: "魔尼尼",
            french: "Mime Jr.",
            german: "Pantimimi",
            spanish: "Mime Jr.",
            italian: "Mime Jr.",
            english: "Mime Jr."
        }
    },
    happiny: {
        num: 440,
        isdefault: true,
        baseExperience: 110,
        order: 142,
        growthRate: 3,
        hatchCounter: 40,
        isBaby: 1,
        baseHappiness: 140,
        captureRate: 130,
        habitat: null,
        generation: 4,
        color: 6,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 113,
                trigger: "levelup",
                helditem: 110,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ピンプク",
            romaji: "Pinpuku",
            korean: "핑복",
            chinese: "好運蛋",
            french: "Ptiravi",
            german: "Wonneira",
            spanish: "Happiny",
            italian: "Happiny",
            english: "Happiny"
        }
    },
    chatot: {
        num: 441,
        isdefault: true,
        baseExperience: 144,
        order: 521,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ペラップ",
            romaji: "Perap",
            korean: "페라페",
            chinese: "聒噪鳥",
            french: "Pijako",
            german: "Plaudagei",
            spanish: "Chatot",
            italian: "Chatot",
            english: "Chatot"
        }
    },
    spiritomb: {
        num: 442,
        isdefault: true,
        baseExperience: 170,
        order: 522,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 100,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ミカルゲ",
            romaji: "Mikaruge",
            korean: "화강돌",
            chinese: "花岩怪",
            french: "Spiritomb",
            german: "Kryppuk",
            spanish: "Spiritomb",
            italian: "Spiritomb",
            english: "Spiritomb"
        }
    },
    gible: {
        num: 443,
        isdefault: true,
        baseExperience: 60,
        order: 523,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 444,
                trigger: "levelup",
                level: 24,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フカマル",
            romaji: "Fukamaru",
            korean: "딥상어동",
            chinese: "圓陸鲨",
            french: "Griknot",
            german: "Kaumalat",
            spanish: "Gible",
            italian: "Gible",
            english: "Gible"
        }
    },
    gabite: {
        num: 444,
        isdefault: true,
        baseExperience: 144,
        order: 524,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 445,
                trigger: "levelup",
                level: 48,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ガバイト",
            romaji: "Gabite",
            korean: "한바이트",
            chinese: "尖牙陸鲨",
            french: "Carmache",
            german: "Knarksel",
            spanish: "Gabite",
            italian: "Gabite",
            english: "Gabite"
        }
    },
    garchomp: {
        num: 445,
        isdefault: true,
        baseExperience: 270,
        order: 525,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ガブリアス",
            romaji: "Gablias",
            korean: "한카리아스",
            chinese: "烈咬陸鲨",
            french: "Carchacrok",
            german: "Knakrack",
            spanish: "Garchomp",
            italian: "Garchomp",
            english: "Garchomp"
        }
    },
    garchompmega: {
        num: 445,
        isdefault: false,
        baseExperience: 315,
        order: 526
    },
    munchlax: {
        num: 446,
        isdefault: true,
        baseExperience: 78,
        order: 195,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 50,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 143,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴンベ",
            romaji: "Gonbe",
            korean: "먹고자",
            chinese: "小卡比獸",
            french: "Goinfrex",
            german: "Mampfaxo",
            spanish: "Munchlax",
            italian: "Munchlax",
            english: "Munchlax"
        }
    },
    riolu: {
        num: 447,
        isdefault: true,
        baseExperience: 57,
        order: 527,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 448,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "リオル",
            romaji: "Riolu",
            korean: "리오르",
            chinese: "利歐路",
            french: "Riolu",
            german: "Riolu",
            spanish: "Riolu",
            italian: "Riolu",
            english: "Riolu"
        }
    },
    lucario: {
        num: 448,
        isdefault: true,
        baseExperience: 184,
        order: 528,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ルカリオ",
            romaji: "Lucario",
            korean: "루카리오",
            chinese: "路卡利歐",
            french: "Lucario",
            german: "Lucario",
            spanish: "Lucario",
            italian: "Lucario",
            english: "Lucario"
        }
    },
    lucariomega: {
        num: 448,
        isdefault: false,
        baseExperience: 219,
        order: 529
    },
    hippopotas: {
        num: 449,
        isdefault: true,
        baseExperience: 66,
        order: 530,
        growthRate: 1,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 140,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 1,
        evomethods: [
            {
                num: 450,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒポポタス",
            romaji: "Hipopotas",
            korean: "히포포타스",
            chinese: "怪河馬",
            french: "Hippopotas",
            german: "Hippopotas",
            spanish: "Hippopotas",
            italian: "Hippopotas",
            english: "Hippopotas"
        }
    },
    hippowdon: {
        num: 450,
        isdefault: true,
        baseExperience: 184,
        order: 531,
        growthRate: 1,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "カバルドン",
            romaji: "Kabarudon",
            korean: "하마돈",
            chinese: "河馬獸",
            french: "Hippodocus",
            german: "Hippoterus",
            spanish: "Hippowdon",
            italian: "Hippowdon",
            english: "Hippowdon"
        }
    },
    skorupi: {
        num: 451,
        isdefault: true,
        baseExperience: 66,
        order: 532,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 452,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "スコルピ",
            romaji: "Scorpi",
            korean: "스콜피",
            chinese: "紫天蠍",
            french: "Rapion",
            german: "Pionskora",
            spanish: "Skorupi",
            italian: "Skorupi",
            english: "Skorupi"
        }
    },
    drapion: {
        num: 452,
        isdefault: true,
        baseExperience: 175,
        order: 533,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "ドラピオン",
            romaji: "Dorapion",
            korean: "드래피온",
            chinese: "龍王蠍",
            french: "Drascore",
            german: "Piondragi",
            spanish: "Drapion",
            italian: "Drapion",
            english: "Drapion"
        }
    },
    croagunk: {
        num: 453,
        isdefault: true,
        baseExperience: 60,
        order: 534,
        growthRate: 2,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 140,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 12,
        genderDiff: 1,
        evomethods: [
            {
                num: 454,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "グレッグル",
            romaji: "Gureggru",
            korean: "삐딱구리",
            chinese: "不良蛙",
            french: "Cradopaud",
            german: "Glibunkel",
            spanish: "Croagunk",
            italian: "Croagunk",
            english: "Croagunk"
        }
    },
    toxicroak: {
        num: 454,
        isdefault: true,
        baseExperience: 172,
        order: 535,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "ドクロッグ",
            romaji: "Dokurog",
            korean: "독개굴",
            chinese: "毒骷蛙",
            french: "Coatox",
            german: "Toxiquak",
            spanish: "Toxicroak",
            italian: "Toxicroak",
            english: "Toxicroak"
        }
    },
    carnivine: {
        num: 455,
        isdefault: true,
        baseExperience: 159,
        order: 536,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "マスキッパ",
            romaji: "Muskippa",
            korean: "무스틈니",
            chinese: "尖牙籠",
            french: "Vortente",
            german: "Venuflibis",
            spanish: "Carnivine",
            italian: "Carnivine",
            english: "Carnivine"
        }
    },
    finneon: {
        num: 456,
        isdefault: true,
        baseExperience: 66,
        order: 537,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 3,
        genderDiff: 1,
        evomethods: [
            {
                num: 457,
                trigger: "levelup",
                level: 31,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ケイコウオ",
            romaji: "Keikouo",
            korean: "형광어",
            chinese: "螢光魚",
            french: "Écayon",
            german: "Finneon",
            spanish: "Finneon",
            italian: "Finneon",
            english: "Finneon"
        }
    },
    lumineon: {
        num: 457,
        isdefault: true,
        baseExperience: 161,
        order: 538,
        growthRate: 5,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 3,
        genderDiff: 1,
        names: {
            japanese: "ネオラント",
            romaji: "Neolant",
            korean: "네오라이트",
            chinese: "霓虹魚",
            french: "Luminéon",
            german: "Lumineon",
            spanish: "Lumineon",
            italian: "Lumineon",
            english: "Lumineon"
        }
    },
    mantyke: {
        num: 458,
        isdefault: true,
        baseExperience: 69,
        order: 283,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 1,
        baseHappiness: 70,
        captureRate: 25,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 226,
                trigger: "levelup",
                party: 223,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タマンタ",
            romaji: "Tamanta",
            korean: "타만타",
            chinese: "小球飛魚",
            french: "Babimanta",
            german: "Mantirps",
            spanish: "Mantyke",
            italian: "Mantyke",
            english: "Mantyke"
        }
    },
    snover: {
        num: 459,
        isdefault: true,
        baseExperience: 67,
        order: 539,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 6,
        genderDiff: 1,
        evomethods: [
            {
                num: 460,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ユキカブリ",
            romaji: "Yukikaburi",
            korean: "눈쓰개",
            chinese: "雪笠怪",
            french: "Blizzi",
            german: "Shnebedeck",
            spanish: "Snover",
            italian: "Snover",
            english: "Snover"
        }
    },
    abomasnow: {
        num: 460,
        isdefault: true,
        baseExperience: 173,
        order: 540,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ユキノオー",
            romaji: "Yukinooh",
            korean: "눈설왕",
            chinese: "暴雪王",
            french: "Blizzaroi",
            german: "Rexblisar",
            spanish: "Abomasnow",
            italian: "Abomasnow",
            english: "Abomasnow"
        }
    },
    abomasnowmega: {
        num: 460,
        isdefault: false,
        baseExperience: 208,
        order: 541
    },
    weavile: {
        num: 461,
        isdefault: true,
        baseExperience: 179,
        order: 271,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "マニューラ",
            romaji: "Manyula",
            korean: "포푸니라",
            chinese: "瑪狃拉",
            french: "Dimoret",
            german: "Snibunna",
            spanish: "Weavile",
            italian: "Weavile",
            english: "Weavile"
        }
    },
    magnezone: {
        num: 462,
        isdefault: true,
        baseExperience: 241,
        order: 104,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ジバコイル",
            romaji: "Jibacoil",
            korean: "자포코일",
            chinese: "自爆磁怪",
            french: "Magnézone",
            german: "Magnezone",
            spanish: "Magnezone",
            italian: "Magnezone",
            english: "Magnezone"
        }
    },
    lickilicky: {
        num: 463,
        isdefault: true,
        baseExperience: 180,
        order: 136,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 6,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ベロベルト",
            romaji: "Beroberto",
            korean: "내룸벨트",
            chinese: "大舌舔",
            french: "Coudlangue",
            german: "Schlurplek",
            spanish: "Lickilicky",
            italian: "Lickilicky",
            english: "Lickilicky"
        }
    },
    rhyperior: {
        num: 464,
        isdefault: true,
        baseExperience: 241,
        order: 141,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ドサイドン",
            romaji: "Dosydon",
            korean: "거대코뿌리",
            chinese: "超鐵暴龍",
            french: "Rhinastoc",
            german: "Rihornior",
            spanish: "Rhyperior",
            italian: "Rhyperior",
            english: "Rhyperior"
        }
    },
    tangrowth: {
        num: 465,
        isdefault: true,
        baseExperience: 187,
        order: 146,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 12,
        genderDiff: 1,
        names: {
            japanese: "モジャンボ",
            romaji: "Mojumbo",
            korean: "덩쿠림보",
            chinese: "巨蔓藤",
            french: "Bouldeneu",
            german: "Tangoloss",
            spanish: "Tangrowth",
            italian: "Tangrowth",
            english: "Tangrowth"
        }
    },
    electivire: {
        num: 466,
        isdefault: true,
        baseExperience: 243,
        order: 165,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "エレキブル",
            romaji: "Elekible",
            korean: "에레키블",
            chinese: "電擊魔獸",
            french: "Élekable",
            german: "Elevoltek",
            spanish: "Electivire",
            italian: "Electivire",
            english: "Electivire"
        }
    },
    magmortar: {
        num: 467,
        isdefault: true,
        baseExperience: 243,
        order: 168,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ブーバーン",
            romaji: "Booburn",
            korean: "마그마번",
            chinese: "鴨嘴焰龍",
            french: "Maganon",
            german: "Magbrant",
            spanish: "Magmortar",
            italian: "Magmortar",
            english: "Magmortar"
        }
    },
    togekiss: {
        num: 468,
        isdefault: true,
        baseExperience: 245,
        order: 228,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "トゲキッス",
            romaji: "Togekiss",
            korean: "토게키스",
            chinese: "波克基斯",
            french: "Togekiss",
            german: "Togekiss",
            spanish: "Togekiss",
            italian: "Togekiss",
            english: "Togekiss"
        }
    },
    yanmega: {
        num: 469,
        isdefault: true,
        baseExperience: 180,
        order: 248,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "メガヤンマ",
            romaji: "Megayanma",
            korean: "메가자리",
            chinese: "梅卡陽瑪",
            french: "Yanmega",
            german: "Yanmega",
            spanish: "Yanmega",
            italian: "Yanmega",
            english: "Yanmega"
        }
    },
    leafeon: {
        num: 470,
        isdefault: true,
        baseExperience: 184,
        order: 183,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "リーフィア",
            romaji: "Leafia",
            korean: "리피아",
            chinese: "葉精靈",
            french: "Phyllali",
            german: "Folipurba",
            spanish: "Leafeon",
            italian: "Leafeon",
            english: "Leafeon"
        }
    },
    glaceon: {
        num: 471,
        isdefault: true,
        baseExperience: 184,
        order: 184,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "グレイシア",
            romaji: "Glacia",
            korean: "글레이시아",
            chinese: "冰精靈",
            french: "Givrali",
            german: "Glaziola",
            spanish: "Glaceon",
            italian: "Glaceon",
            english: "Glaceon"
        }
    },
    gliscor: {
        num: 472,
        isdefault: true,
        baseExperience: 179,
        order: 263,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "グライオン",
            romaji: "Glion",
            korean: "글라이온",
            chinese: "天蠍王",
            french: "Scorvol",
            german: "Skorgro",
            spanish: "Gliscor",
            italian: "Gliscor",
            english: "Gliscor"
        }
    },
    mamoswine: {
        num: 473,
        isdefault: true,
        baseExperience: 239,
        order: 278,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 50,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "マンムー",
            romaji: "Mammoo",
            korean: "맘모꾸리",
            chinese: "象牙豬",
            french: "Mammochon",
            german: "Mamutel",
            spanish: "Mamoswine",
            italian: "Mamoswine",
            english: "Mamoswine"
        }
    },
    porygonz: {
        num: 474,
        isdefault: true,
        baseExperience: 241,
        order: 188,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 8,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ポリゴンＺ",
            romaji: "PorygonZ",
            korean: "폴리곤Z",
            chinese: "3D龍Z",
            french: "Porygon-Z",
            german: "Porygon-Z",
            spanish: "Porygon-Z",
            italian: "Porygon-Z",
            english: "Porygon-Z"
        }
    },
    gallade: {
        num: 475,
        isdefault: true,
        baseExperience: 233,
        order: 339,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "エルレイド",
            romaji: "Erlade",
            korean: "엘레이드",
            chinese: "艾路雷朵",
            french: "Gallame",
            german: "Galagladi",
            spanish: "Gallade",
            italian: "Gallade",
            english: "Gallade"
        }
    },
    gallademega: {
        num: 475,
        isdefault: false,
        baseExperience: 278,
        order: 340
    },
    probopass: {
        num: 476,
        isdefault: true,
        baseExperience: 184,
        order: 357,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "ダイノーズ",
            romaji: "Dainose",
            korean: "대코파스",
            chinese: "大朝北鼻",
            french: "Tarinorme",
            german: "Voluminas",
            spanish: "Probopass",
            italian: "Probopass",
            english: "Probopass"
        }
    },
    dusknoir: {
        num: 477,
        isdefault: true,
        baseExperience: 236,
        order: 429,
        growthRate: 3,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ヨノワール",
            romaji: "Yonoir",
            korean: "야느와르몽",
            chinese: "夜黑魔人",
            french: "Noctunoir",
            german: "Zwirrfinst",
            spanish: "Dusknoir",
            italian: "Dusknoir",
            english: "Dusknoir"
        }
    },
    froslass: {
        num: 478,
        isdefault: true,
        baseExperience: 168,
        order: 438,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ユキメノコ",
            romaji: "Yukimenoko",
            korean: "눈여아",
            chinese: "雪妖女",
            french: "Momartik",
            german: "Frosdedje",
            spanish: "Froslass",
            italian: "Froslass",
            english: "Froslass"
        }
    },
    rotom: {
        num: 479,
        isdefault: true,
        baseExperience: 154,
        order: 542,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 8,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "ロトム",
            romaji: "Rotom",
            korean: "로토무",
            chinese: "洛托姆",
            french: "Motisma",
            german: "Rotom",
            spanish: "Rotom",
            italian: "Rotom",
            english: "Rotom"
        }
    },
    rotomheat: {
        num: 479,
        isdefault: false,
        baseExperience: 182,
        order: 543
    },
    rotomwash: {
        num: 479,
        isdefault: false,
        baseExperience: 182,
        order: 544
    },
    rotomfrost: {
        num: 479,
        isdefault: false,
        baseExperience: 182,
        order: 545
    },
    rotomfan: {
        num: 479,
        isdefault: false,
        baseExperience: 182,
        order: 546
    },
    rotommow: {
        num: 479,
        isdefault: false,
        baseExperience: 182,
        order: 547
    },
    uxie: {
        num: 480,
        isdefault: true,
        baseExperience: 261,
        order: 548,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ユクシー",
            romaji: "Yuxie",
            korean: "유크시",
            chinese: "由克希",
            french: "Créhelf",
            german: "Selfe",
            spanish: "Uxie",
            italian: "Uxie",
            english: "Uxie"
        }
    },
    mesprit: {
        num: 481,
        isdefault: true,
        baseExperience: 261,
        order: 549,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "エムリット",
            romaji: "Emrit",
            korean: "엠라이트",
            chinese: "艾姆利多",
            french: "Créfollet",
            german: "Vesprit",
            spanish: "Mesprit",
            italian: "Mesprit",
            english: "Mesprit"
        }
    },
    azelf: {
        num: 482,
        isdefault: true,
        baseExperience: 261,
        order: 550,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 140,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "アグノム",
            romaji: "Agnome",
            korean: "아그놈",
            chinese: "亞克諾姆",
            french: "Créfadet",
            german: "Tobutz",
            spanish: "Azelf",
            italian: "Azelf",
            english: "Azelf"
        }
    },
    dialga: {
        num: 483,
        isdefault: true,
        baseExperience: 306,
        order: 551,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ディアルガ",
            romaji: "Dialga",
            korean: "디아루가",
            chinese: "帝牙盧卡",
            french: "Dialga",
            german: "Dialga",
            spanish: "Dialga",
            italian: "Dialga",
            english: "Dialga"
        }
    },
    palkia: {
        num: 484,
        isdefault: true,
        baseExperience: 306,
        order: 552,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "パルキア",
            romaji: "Palkia",
            korean: "펄기아",
            chinese: "帕路奇犽",
            french: "Palkia",
            german: "Palkia",
            spanish: "Palkia",
            italian: "Palkia",
            english: "Palkia"
        }
    },
    heatran: {
        num: 485,
        isdefault: true,
        baseExperience: 270,
        order: 553,
        growthRate: 1,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ヒードラン",
            romaji: "Heatran",
            korean: "히드런",
            chinese: "席多藍恩",
            french: "Heatran",
            german: "Heatran",
            spanish: "Heatran",
            italian: "Heatran",
            english: "Heatran"
        }
    },
    regigigas: {
        num: 486,
        isdefault: true,
        baseExperience: 302,
        order: 554,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 9,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "レジギガス",
            romaji: "Regigigas",
            korean: "레지기가스",
            chinese: "雷吉奇卡斯",
            french: "Regigigas",
            german: "Regigigas",
            spanish: "Regigigas",
            italian: "Regigigas",
            english: "Regigigas"
        }
    },
    giratina: {
        num: 487,
        isdefault: true,
        baseExperience: 306,
        order: 555,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "ギラティナ",
            romaji: "Giratina",
            korean: "기라티나",
            chinese: "騎拉帝納",
            french: "Giratina",
            german: "Giratina",
            spanish: "Giratina",
            italian: "Giratina",
            english: "Giratina"
        }
    },
    giratinaorigin: {
        num: 487,
        isdefault: false,
        baseExperience: 306,
        order: 556
    },
    cresselia: {
        num: 488,
        isdefault: true,
        baseExperience: 270,
        order: 557,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 10,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "クレセリア",
            romaji: "Crecelia",
            korean: "크레세리아",
            chinese: "克雷色利亞",
            french: "Cresselia",
            german: "Cresselia",
            spanish: "Cresselia",
            italian: "Cresselia",
            english: "Cresselia"
        }
    },
    phione: {
        num: 489,
        isdefault: true,
        baseExperience: 216,
        order: 558,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "フィオネ",
            romaji: "Phione",
            korean: "피오네",
            chinese: "霏歐納",
            french: "Phione",
            german: "Phione",
            spanish: "Phione",
            italian: "Phione",
            english: "Phione"
        }
    },
    manaphy: {
        num: 490,
        isdefault: true,
        baseExperience: 270,
        order: 559,
        growthRate: 1,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 2,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "マナフィ",
            romaji: "Manaphy",
            korean: "마나피",
            chinese: "瑪納霏",
            french: "Manaphy",
            german: "Manaphy",
            spanish: "Manaphy",
            italian: "Manaphy",
            english: "Manaphy"
        }
    },
    darkrai: {
        num: 491,
        isdefault: true,
        baseExperience: 270,
        order: 560,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 1,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ダークライ",
            romaji: "Darkrai",
            korean: "다크라이",
            chinese: "達克萊伊",
            french: "Darkrai",
            german: "Darkrai",
            spanish: "Darkrai",
            italian: "Darkrai",
            english: "Darkrai"
        }
    },
    shaymin: {
        num: 492,
        isdefault: true,
        baseExperience: 270,
        order: 561,
        growthRate: 4,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 45,
        habitat: null,
        generation: 4,
        color: 5,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "シェイミ",
            romaji: "Shaymin",
            korean: "쉐이미",
            chinese: "潔咪",
            french: "Shaymin",
            german: "Shaymin",
            spanish: "Shaymin",
            italian: "Shaymin",
            english: "Shaymin"
        }
    },
    shayminsky: {
        num: 492,
        isdefault: false,
        baseExperience: 270,
        order: 562
    },
    arceus: {
        num: 493,
        isdefault: true,
        baseExperience: 324,
        order: 563,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 4,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "アルセウス",
            romaji: "Arceus",
            korean: "아르세우스",
            chinese: "雅魯塞宇斯",
            french: "Arceus",
            german: "Arceus",
            spanish: "Arceus",
            italian: "Arceus",
            english: "Arceus"
        }
    },
    arceusbug: {
        num: 493,
        isdefault: false
    },
    arceusdark: {
        num: 493,
        isdefault: false
    },
    arceusdragon: {
        num: 493,
        isdefault: false
    },
    arceuselectric: {
        num: 493,
        isdefault: false
    },
    arceusfairy: {
        num: 493,
        isdefault: false
    },
    arceusfighting: {
        num: 493,
        isdefault: false
    },
    arceusfire: {
        num: 493,
        isdefault: false
    },
    arceusflying: {
        num: 493,
        isdefault: false
    },
    arceusghost: {
        num: 493,
        isdefault: false
    },
    arceusgrass: {
        num: 493,
        isdefault: false
    },
    arceusground: {
        num: 493,
        isdefault: false
    },
    arceusice: {
        num: 493,
        isdefault: false
    },
    arceuspoison: {
        num: 493,
        isdefault: false
    },
    arceuspsychic: {
        num: 493,
        isdefault: false
    },
    arceusrock: {
        num: 493,
        isdefault: false
    },
    arceussteel: {
        num: 493,
        isdefault: false
    },
    arceuswater: {
        num: 493,
        isdefault: false
    },
    victini: {
        num: 494,
        isdefault: true,
        baseExperience: 270,
        order: 564,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ビクティニ",
            korean: "비크티니",
            french: "Victini",
            german: "Victini",
            spanish: "Victini",
            italian: "Victini",
            english: "Victini"
        }
    },
    snivy: {
        num: 495,
        isdefault: true,
        baseExperience: 62,
        order: 565,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 496,
                trigger: "levelup",
                level: 17,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ツタージャ",
            korean: "주리비얀",
            chinese: "藤藤蛇",
            french: "Vipélierre",
            german: "Serpifeu",
            spanish: "Snivy",
            italian: "Snivy",
            english: "Snivy"
        }
    },
    servine: {
        num: 496,
        isdefault: true,
        baseExperience: 145,
        order: 566,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 497,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ジャノビー",
            korean: "샤비",
            french: "Lianaja",
            german: "Efoserp",
            spanish: "Servine",
            italian: "Servine",
            english: "Servine"
        }
    },
    serperior: {
        num: 497,
        isdefault: true,
        baseExperience: 238,
        order: 567,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ジャローダ",
            korean: "샤로다",
            french: "Majaspic",
            german: "Serpiroyal",
            spanish: "Serperior",
            italian: "Serperior",
            english: "Serperior"
        }
    },
    tepig: {
        num: 498,
        isdefault: true,
        baseExperience: 62,
        order: 568,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 499,
                trigger: "levelup",
                level: 17,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ポカブ",
            korean: "뚜꾸리",
            chinese: "暖暖豬",
            french: "Gruikui",
            german: "Floink",
            spanish: "Tepig",
            italian: "Tepig",
            english: "Tepig"
        }
    },
    pignite: {
        num: 499,
        isdefault: true,
        baseExperience: 146,
        order: 569,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 500,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チャオブー",
            korean: "차오꿀",
            french: "Grotichon",
            german: "Ferkokel",
            spanish: "Pignite",
            italian: "Pignite",
            english: "Pignite"
        }
    },
    emboar: {
        num: 500,
        isdefault: true,
        baseExperience: 238,
        order: 570,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "エンブオー",
            korean: "염무왕",
            french: "Roitiflam",
            german: "Flambirex",
            spanish: "Emboar",
            italian: "Emboar",
            english: "Emboar"
        }
    },
    oshawott: {
        num: 501,
        isdefault: true,
        baseExperience: 62,
        order: 571,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 502,
                trigger: "levelup",
                level: 17,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミジュマル",
            korean: "수댕이",
            chinese: "水水獺",
            french: "Moustillon",
            german: "Ottaro",
            spanish: "Oshawott",
            italian: "Oshawott",
            english: "Oshawott"
        }
    },
    dewott: {
        num: 502,
        isdefault: true,
        baseExperience: 145,
        order: 572,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 503,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フタチマル",
            korean: "쌍검자비",
            french: "Mateloutre",
            german: "Zwottronin",
            spanish: "Dewott",
            italian: "Dewott",
            english: "Dewott"
        }
    },
    samurott: {
        num: 503,
        isdefault: true,
        baseExperience: 238,
        order: 573,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ダイケンキ",
            korean: "대검귀",
            french: "Clamiral",
            german: "Admurai",
            spanish: "Samurott",
            italian: "Samurott",
            english: "Samurott"
        }
    },
    patrat: {
        num: 504,
        isdefault: true,
        baseExperience: 51,
        order: 574,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 505,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ミネズミ",
            korean: "보르쥐",
            french: "Ratentif",
            german: "Nagelotz",
            spanish: "Patrat",
            italian: "Patrat",
            english: "Patrat"
        }
    },
    watchog: {
        num: 505,
        isdefault: true,
        baseExperience: 147,
        order: 575,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ミルホッグ",
            korean: "보르그",
            french: "Miradar",
            german: "Kukmarda",
            spanish: "Watchog",
            italian: "Watchog",
            english: "Watchog"
        }
    },
    lillipup: {
        num: 506,
        isdefault: true,
        baseExperience: 55,
        order: 576,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 507,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヨーテリー",
            korean: "요테리",
            french: "Ponchiot",
            german: "Yorkleff",
            spanish: "Lillipup",
            italian: "Lillipup",
            english: "Lillipup"
        }
    },
    herdier: {
        num: 507,
        isdefault: true,
        baseExperience: 130,
        order: 577,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 508,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハーデリア",
            korean: "하데리어",
            french: "Ponchien",
            german: "Terribark",
            spanish: "Herdier",
            italian: "Herdier",
            english: "Herdier"
        }
    },
    stoutland: {
        num: 508,
        isdefault: true,
        baseExperience: 225,
        order: 578,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ムーランド",
            korean: "바랜드",
            french: "Mastouffe",
            german: "Bissbark",
            spanish: "Stoutland",
            italian: "Stoutland",
            english: "Stoutland"
        }
    },
    purrloin: {
        num: 509,
        isdefault: true,
        baseExperience: 56,
        order: 579,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 510,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チョロネコ",
            korean: "쌔비냥",
            french: "Chacripan",
            german: "Felilou",
            spanish: "Purrloin",
            italian: "Purrloin",
            english: "Purrloin"
        }
    },
    liepard: {
        num: 510,
        isdefault: true,
        baseExperience: 156,
        order: 580,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "レパルダス",
            korean: "레파르다스",
            french: "Léopardus",
            german: "Kleoparda",
            spanish: "Liepard",
            italian: "Liepard",
            english: "Liepard"
        }
    },
    pansage: {
        num: 511,
        isdefault: true,
        baseExperience: 63,
        order: 581,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 512,
                trigger: "useitem",
                itemtrigger: "leafstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤナップ",
            korean: "야나프",
            chinese: "花椰猴",
            french: "Feuillajou",
            german: "Vegimak",
            spanish: "Pansage",
            italian: "Pansage",
            english: "Pansage"
        }
    },
    simisage: {
        num: 512,
        isdefault: true,
        baseExperience: 174,
        order: 582,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ヤナッキー",
            korean: "야나키",
            french: "Feuiloutan",
            german: "Vegichita",
            spanish: "Simisage",
            italian: "Simisage",
            english: "Simisage"
        }
    },
    pansear: {
        num: 513,
        isdefault: true,
        baseExperience: 63,
        order: 583,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 514,
                trigger: "useitem",
                itemtrigger: "firestone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バオップ",
            korean: "바오프",
            french: "Flamajou",
            german: "Grillmak",
            spanish: "Pansear",
            italian: "Pansear",
            english: "Pansear"
        }
    },
    simisear: {
        num: 514,
        isdefault: true,
        baseExperience: 174,
        order: 584,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "バオッキー",
            korean: "바오키",
            french: "Flamoutan",
            german: "Grillchita",
            spanish: "Simisear",
            italian: "Simisear",
            english: "Simisear"
        }
    },
    panpour: {
        num: 515,
        isdefault: true,
        baseExperience: 63,
        order: 585,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 516,
                trigger: "useitem",
                itemtrigger: "waterstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒヤップ",
            korean: "앗차프",
            french: "Flotajou",
            german: "Sodamak",
            spanish: "Panpour",
            italian: "Panpour",
            english: "Panpour"
        }
    },
    simipour: {
        num: 516,
        isdefault: true,
        baseExperience: 174,
        order: 586,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ヒヤッキー",
            korean: "앗차키",
            french: "Flotoutan",
            german: "Sodachita",
            spanish: "Simipour",
            italian: "Simipour",
            english: "Simipour"
        }
    },
    munna: {
        num: 517,
        isdefault: true,
        baseExperience: 58,
        order: 587,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 6,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 518,
                trigger: "useitem",
                itemtrigger: "moonstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ムンナ",
            korean: "몽나",
            french: "Munna",
            german: "Somniam",
            spanish: "Munna",
            italian: "Munna",
            english: "Munna"
        }
    },
    musharna: {
        num: 518,
        isdefault: true,
        baseExperience: 170,
        order: 588,
        growthRate: 3,
        hatchCounter: 10,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 6,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ムシャーナ",
            korean: "몽얌나",
            french: "Mushana",
            german: "Somnivora",
            spanish: "Musharna",
            italian: "Musharna",
            english: "Musharna"
        }
    },
    pidove: {
        num: 519,
        isdefault: true,
        baseExperience: 53,
        order: 589,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 520,
                trigger: "levelup",
                level: 21,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "マメパト",
            korean: "콩둘기",
            french: "Poichigeon",
            german: "Dusselgurr",
            spanish: "Pidove",
            italian: "Pidove",
            english: "Pidove"
        }
    },
    tranquill: {
        num: 520,
        isdefault: true,
        baseExperience: 125,
        order: 590,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 521,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハトーボー",
            korean: "유토브",
            french: "Colombeau",
            german: "Navitaub",
            spanish: "Tranquill",
            italian: "Tranquill",
            english: "Tranquill"
        }
    },
    unfezant: {
        num: 521,
        isdefault: true,
        baseExperience: 220,
        order: 591,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 9,
        genderDiff: 1,
        names: {
            japanese: "ケンホロウ",
            korean: "켄호로우",
            french: "Déflaisan",
            german: "Fasasnob",
            spanish: "Unfezant",
            italian: "Unfezant",
            english: "Unfezant"
        }
    },
    blitzle: {
        num: 522,
        isdefault: true,
        baseExperience: 59,
        order: 592,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 523,
                trigger: "levelup",
                level: 27,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シママ",
            korean: "줄뮤마",
            french: "Zébibron",
            german: "Elezeba",
            spanish: "Blitzle",
            italian: "Blitzle",
            english: "Blitzle"
        }
    },
    zebstrika: {
        num: 523,
        isdefault: true,
        baseExperience: 174,
        order: 593,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ゼブライカ",
            korean: "제브라이카",
            french: "Zéblitz",
            german: "Zebritz",
            spanish: "Zebstrika",
            italian: "Zebstrika",
            english: "Zebstrika"
        }
    },
    roggenrola: {
        num: 524,
        isdefault: true,
        baseExperience: 56,
        order: 594,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 525,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ダンゴロ",
            korean: "단굴",
            french: "Nodulithe",
            german: "Kiesling",
            spanish: "Roggenrola",
            italian: "Roggenrola",
            english: "Roggenrola"
        }
    },
    boldore: {
        num: 525,
        isdefault: true,
        baseExperience: 137,
        order: 595,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 10,
        genderDiff: 0,
        evomethods: [
            {
                num: 526,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ガントル",
            korean: "암트르",
            french: "Géolithe",
            german: "Sedimantur",
            spanish: "Boldore",
            italian: "Boldore",
            english: "Boldore"
        }
    },
    gigalith: {
        num: 526,
        isdefault: true,
        baseExperience: 232,
        order: 596,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "ギガイアス",
            korean: "기가이어스",
            french: "Gigalithe",
            german: "Brockoloss",
            spanish: "Gigalith",
            italian: "Gigalith",
            english: "Gigalith"
        }
    },
    woobat: {
        num: 527,
        isdefault: true,
        baseExperience: 63,
        order: 597,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 528,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コロモリ",
            korean: "또르박쥐",
            french: "Chovsourir",
            german: "Fleknoil",
            spanish: "Woobat",
            italian: "Woobat",
            english: "Woobat"
        }
    },
    swoobat: {
        num: 528,
        isdefault: true,
        baseExperience: 149,
        order: 598,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ココロモリ",
            korean: "맘박쥐",
            french: "Rhinolove",
            german: "Fletiamo",
            spanish: "Swoobat",
            italian: "Swoobat",
            english: "Swoobat"
        }
    },
    drilbur: {
        num: 529,
        isdefault: true,
        baseExperience: 66,
        order: 599,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 530,
                trigger: "levelup",
                level: 31,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "モグリュー",
            korean: "두더류",
            french: "Rototaupe",
            german: "Rotomurf",
            spanish: "Drilbur",
            italian: "Drilbur",
            english: "Drilbur"
        }
    },
    excadrill: {
        num: 530,
        isdefault: true,
        baseExperience: 178,
        order: 600,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ドリュウズ",
            korean: "몰드류",
            french: "Minotaupe",
            german: "Stalobor",
            spanish: "Excadrill",
            italian: "Excadrill",
            english: "Excadrill"
        }
    },
    audino: {
        num: 531,
        isdefault: true,
        baseExperience: 390,
        order: 601,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 6,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "タブンネ",
            korean: "다부니",
            french: "Nanméouïe",
            german: "Ohrdoch",
            spanish: "Audino",
            italian: "Audino",
            english: "Audino"
        }
    },
    audinomega: {
        num: 531,
        isdefault: false,
        baseExperience: 425,
        order: 602
    },
    timburr: {
        num: 532,
        isdefault: true,
        baseExperience: 61,
        order: 603,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 533,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドッコラー",
            korean: "으랏차",
            french: "Charpenti",
            german: "Praktibalk",
            spanish: "Timburr",
            italian: "Timburr",
            english: "Timburr"
        }
    },
    gurdurr: {
        num: 533,
        isdefault: true,
        baseExperience: 142,
        order: 604,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 534,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ドテッコツ",
            korean: "토쇠골",
            french: "Ouvrifier",
            german: "Strepoli",
            spanish: "Gurdurr",
            italian: "Gurdurr",
            english: "Gurdurr"
        }
    },
    conkeldurr: {
        num: 534,
        isdefault: true,
        baseExperience: 227,
        order: 605,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ローブシン",
            korean: "노보청",
            french: "Bétochef",
            german: "Meistagrif",
            spanish: "Conkeldurr",
            italian: "Conkeldurr",
            english: "Conkeldurr"
        }
    },
    tympole: {
        num: 535,
        isdefault: true,
        baseExperience: 59,
        order: 606,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 536,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "オタマロ",
            korean: "동챙이",
            french: "Tritonde",
            german: "Schallquap",
            spanish: "Tympole",
            italian: "Tympole",
            english: "Tympole"
        }
    },
    palpitoad: {
        num: 536,
        isdefault: true,
        baseExperience: 134,
        order: 607,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 537,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ガマガル",
            korean: "두까비",
            french: "Batracné",
            german: "Mebrana",
            spanish: "Palpitoad",
            italian: "Palpitoad",
            english: "Palpitoad"
        }
    },
    seismitoad: {
        num: 537,
        isdefault: true,
        baseExperience: 229,
        order: 608,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ガマゲロゲ",
            korean: "두빅굴",
            french: "Crapustule",
            german: "Branawarz",
            spanish: "Seismitoad",
            italian: "Seismitoad",
            english: "Seismitoad"
        }
    },
    throh: {
        num: 538,
        isdefault: true,
        baseExperience: 163,
        order: 609,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ナゲキ",
            korean: "던지미",
            french: "Judokrak",
            german: "Jiutesto",
            spanish: "Throh",
            italian: "Throh",
            english: "Throh"
        }
    },
    sawk: {
        num: 539,
        isdefault: true,
        baseExperience: 163,
        order: 610,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ダゲキ",
            korean: "타격귀",
            french: "Karaclée",
            german: "Karadonis",
            spanish: "Sawk",
            italian: "Sawk",
            english: "Sawk"
        }
    },
    sewaddle: {
        num: 540,
        isdefault: true,
        baseExperience: 62,
        order: 611,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 541,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クルミル",
            korean: "두르보",
            french: "Larveyette",
            german: "Strawickl",
            spanish: "Sewaddle",
            italian: "Sewaddle",
            english: "Sewaddle"
        }
    },
    swadloon: {
        num: 541,
        isdefault: true,
        baseExperience: 133,
        order: 612,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 542,
                trigger: "levelup",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クルマユ",
            korean: "두르쿤",
            french: "Couverdure",
            german: "Folikon",
            spanish: "Swadloon",
            italian: "Swadloon",
            english: "Swadloon"
        }
    },
    leavanny: {
        num: 542,
        isdefault: true,
        baseExperience: 225,
        order: 613,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ハハコモリ",
            korean: "모아머",
            french: "Manternel",
            german: "Matrifol",
            spanish: "Leavanny",
            italian: "Leavanny",
            english: "Leavanny"
        }
    },
    venipede: {
        num: 543,
        isdefault: true,
        baseExperience: 52,
        order: 614,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 544,
                trigger: "levelup",
                level: 22,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フシデ",
            korean: "마디네",
            french: "Venipatte",
            german: "Toxiped",
            spanish: "Venipede",
            italian: "Venipede",
            english: "Venipede"
        }
    },
    whirlipede: {
        num: 544,
        isdefault: true,
        baseExperience: 126,
        order: 615,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 545,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ホイーガ",
            korean: "휠구",
            french: "Scobolide",
            german: "Rollum",
            spanish: "Whirlipede",
            italian: "Whirlipede",
            english: "Whirlipede"
        }
    },
    scolipede: {
        num: 545,
        isdefault: true,
        baseExperience: 218,
        order: 616,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "ペンドラー",
            korean: "펜드라",
            french: "Brutapode",
            german: "Cerapendra",
            spanish: "Scolipede",
            italian: "Scolipede",
            english: "Scolipede"
        }
    },
    cottonee: {
        num: 546,
        isdefault: true,
        baseExperience: 56,
        order: 617,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 547,
                trigger: "useitem",
                itemtrigger: "sunstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "モンメン",
            korean: "소미안",
            french: "Doudouvet",
            german: "Waumboll",
            spanish: "Cottonee",
            italian: "Cottonee",
            english: "Cottonee"
        }
    },
    whimsicott: {
        num: 547,
        isdefault: true,
        baseExperience: 168,
        order: 618,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "エルフーン",
            korean: "엘풍",
            french: "Farfaduvet",
            german: "Elfun",
            spanish: "Whimsicott",
            italian: "Whimsicott",
            english: "Whimsicott"
        }
    },
    petilil: {
        num: 548,
        isdefault: true,
        baseExperience: 56,
        order: 619,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 549,
                trigger: "useitem",
                itemtrigger: "sunstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チュリネ",
            korean: "치릴리",
            french: "Chlorobule",
            german: "Lilminip",
            spanish: "Petilil",
            italian: "Petilil",
            english: "Petilil"
        }
    },
    lilligant: {
        num: 549,
        isdefault: true,
        baseExperience: 168,
        order: 620,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ドレディア",
            korean: "드레디어",
            french: "Fragilady",
            german: "Dressella",
            spanish: "Lilligant",
            italian: "Lilligant",
            english: "Lilligant"
        }
    },
    basculin: {
        num: 550,
        isdefault: true,
        baseExperience: 161,
        order: 621,
        growthRate: 2,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "バスラオ",
            korean: "배쓰나이",
            french: "Bargantua",
            german: "Barschuft",
            spanish: "Basculin",
            italian: "Basculin",
            english: "Basculin"
        }
    },
    basculinbluestriped: {
        num: 550,
        isdefault: false,
        baseExperience: 161,
        order: 622
    },
    sandile: {
        num: 551,
        isdefault: true,
        baseExperience: 58,
        order: 623,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 552,
                trigger: "levelup",
                level: 29,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "メグロコ",
            korean: "깜눈크",
            french: "Mascaïman",
            german: "Ganovil",
            spanish: "Sandile",
            italian: "Sandile",
            english: "Sandile"
        }
    },
    krokorok: {
        num: 552,
        isdefault: true,
        baseExperience: 123,
        order: 624,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 553,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ワルビル",
            korean: "악비르",
            french: "Escroco",
            german: "Rokkaiman",
            spanish: "Krokorok",
            italian: "Krokorok",
            english: "Krokorok"
        }
    },
    krookodile: {
        num: 553,
        isdefault: true,
        baseExperience: 234,
        order: 625,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ワルビアル",
            korean: "악비아르",
            french: "Crocorible",
            german: "Rabigator",
            spanish: "Krookodile",
            italian: "Krookodile",
            english: "Krookodile"
        }
    },
    darumaka: {
        num: 554,
        isdefault: true,
        baseExperience: 63,
        order: 626,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 555,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ダルマッカ",
            korean: "달막화",
            french: "Darumarond",
            german: "Flampion",
            spanish: "Darumaka",
            italian: "Darumaka",
            english: "Darumaka"
        }
    },
    darmanitan: {
        num: 555,
        isdefault: true,
        baseExperience: 168,
        order: 627,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ヒヒダルマ",
            korean: "불비달마",
            french: "Darumacho",
            german: "Flampivian",
            spanish: "Darmanitan",
            italian: "Darmanitan",
            english: "Darmanitan"
        }
    },
    darmanitanzen: {
        num: 555,
        isdefault: false,
        baseExperience: 189,
        order: 628
    },
    maractus: {
        num: 556,
        isdefault: true,
        baseExperience: 161,
        order: 629,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "マラカッチ",
            korean: "마라카치",
            french: "Maracachi",
            german: "Maracamba",
            spanish: "Maractus",
            italian: "Maractus",
            english: "Maractus"
        }
    },
    dwebble: {
        num: 557,
        isdefault: true,
        baseExperience: 65,
        order: 630,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 558,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "イシズマイ",
            korean: "돌살이",
            french: "Crabicoque",
            german: "Lithomith",
            spanish: "Dwebble",
            italian: "Dwebble",
            english: "Dwebble"
        }
    },
    crustle: {
        num: 558,
        isdefault: true,
        baseExperience: 166,
        order: 631,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "イワパレス",
            korean: "암팰리스",
            french: "Crabaraque",
            german: "Castellith",
            spanish: "Crustle",
            italian: "Crustle",
            english: "Crustle"
        }
    },
    scraggy: {
        num: 559,
        isdefault: true,
        baseExperience: 70,
        order: 632,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 180,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 560,
                trigger: "levelup",
                level: 39,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ズルッグ",
            korean: "곤율랭",
            french: "Baggiguane",
            german: "Zurrokex",
            spanish: "Scraggy",
            italian: "Scraggy",
            english: "Scraggy"
        }
    },
    scrafty: {
        num: 560,
        isdefault: true,
        baseExperience: 171,
        order: 633,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ズルズキン",
            korean: "곤율거니",
            french: "Baggaïd",
            german: "Irokex",
            spanish: "Scrafty",
            italian: "Scrafty",
            english: "Scrafty"
        }
    },
    sigilyph: {
        num: 561,
        isdefault: true,
        baseExperience: 172,
        order: 634,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "シンボラー",
            korean: "심보러",
            french: "Cryptéro",
            german: "Symvolara",
            spanish: "Sigilyph",
            italian: "Sigilyph",
            english: "Sigilyph"
        }
    },
    yamask: {
        num: 562,
        isdefault: true,
        baseExperience: 61,
        order: 635,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 563,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "デスマス",
            korean: "데스마스",
            french: "Tutafeh",
            german: "Makabaja",
            spanish: "Yamask",
            italian: "Yamask",
            english: "Yamask"
        }
    },
    cofagrigus: {
        num: 563,
        isdefault: true,
        baseExperience: 169,
        order: 636,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "デスカーン",
            korean: "데스니칸",
            french: "Tutankafer",
            german: "Echnatoll",
            spanish: "Cofagrigus",
            italian: "Cofagrigus",
            english: "Cofagrigus"
        }
    },
    tirtouga: {
        num: 564,
        isdefault: true,
        baseExperience: 71,
        order: 637,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 565,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "プロトーガ",
            korean: "프로토가",
            french: "Carapagos",
            german: "Galapaflos",
            spanish: "Tirtouga",
            italian: "Tirtouga",
            english: "Tirtouga"
        }
    },
    carracosta: {
        num: 565,
        isdefault: true,
        baseExperience: 173,
        order: 638,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "アバゴーラ",
            korean: "늑골라",
            french: "Mégapagos",
            german: "Karippas",
            spanish: "Carracosta",
            italian: "Carracosta",
            english: "Carracosta"
        }
    },
    archen: {
        num: 566,
        isdefault: true,
        baseExperience: 71,
        order: 639,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 567,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アーケン",
            korean: "아켄",
            french: "Arkéapti",
            german: "Flapteryx",
            spanish: "Archen",
            italian: "Archen",
            english: "Archen"
        }
    },
    archeops: {
        num: 567,
        isdefault: true,
        baseExperience: 177,
        order: 640,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "アーケオス",
            korean: "아케오스",
            french: "Aéroptéryx",
            german: "Aeropteryx",
            spanish: "Archeops",
            italian: "Archeops",
            english: "Archeops"
        }
    },
    trubbish: {
        num: 568,
        isdefault: true,
        baseExperience: 66,
        order: 641,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 569,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤブクロン",
            korean: "깨봉이",
            french: "Miamiasme",
            german: "Unratütox",
            spanish: "Trubbish",
            italian: "Trubbish",
            english: "Trubbish"
        }
    },
    garbodor: {
        num: 569,
        isdefault: true,
        baseExperience: 166,
        order: 642,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ダストダス",
            korean: "더스트나",
            french: "Miasmax",
            german: "Deponitox",
            spanish: "Garbodor",
            italian: "Garbodor",
            english: "Garbodor"
        }
    },
    zorua: {
        num: 570,
        isdefault: true,
        baseExperience: 66,
        order: 643,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 571,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゾロア",
            korean: "조로아",
            french: "Zorua",
            german: "Zorua",
            spanish: "Zorua",
            italian: "Zorua",
            english: "Zorua"
        }
    },
    zoroark: {
        num: 571,
        isdefault: true,
        baseExperience: 179,
        order: 644,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ゾロアーク",
            korean: "조로아크",
            french: "Zoroark",
            german: "Zoroark",
            spanish: "Zoroark",
            italian: "Zoroark",
            english: "Zoroark"
        }
    },
    minccino: {
        num: 572,
        isdefault: true,
        baseExperience: 60,
        order: 645,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 573,
                trigger: "useitem",
                itemtrigger: "shinystone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チラーミィ",
            korean: "치라미",
            french: "Chinchidou",
            german: "Picochilla",
            spanish: "Minccino",
            italian: "Minccino",
            english: "Minccino"
        }
    },
    cinccino: {
        num: 573,
        isdefault: true,
        baseExperience: 165,
        order: 646,
        growthRate: 3,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "チラチーノ",
            korean: "치라치노",
            french: "Pashmilla",
            german: "Chillabell",
            spanish: "Cinccino",
            italian: "Cinccino",
            english: "Cinccino"
        }
    },
    gothita: {
        num: 574,
        isdefault: true,
        baseExperience: 58,
        order: 647,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 575,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴチム",
            korean: "고디탱",
            french: "Scrutella",
            german: "Mollimorba",
            spanish: "Gothita",
            italian: "Gothita",
            english: "Gothita"
        }
    },
    gothorita: {
        num: 575,
        isdefault: true,
        baseExperience: 137,
        order: 648,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 100,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 576,
                trigger: "levelup",
                level: 41,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴチミル",
            korean: "고디보미",
            french: "Mesmérella",
            german: "Hypnomorba",
            spanish: "Gothorita",
            italian: "Gothorita",
            english: "Gothorita"
        }
    },
    gothitelle: {
        num: 576,
        isdefault: true,
        baseExperience: 221,
        order: 649,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 50,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ゴチルゼル",
            korean: "고디모아젤",
            french: "Sidérella",
            german: "Morbitesse",
            spanish: "Gothitelle",
            italian: "Gothitelle",
            english: "Gothitelle"
        }
    },
    solosis: {
        num: 577,
        isdefault: true,
        baseExperience: 58,
        order: 650,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 578,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ユニラン",
            korean: "유니란",
            french: "Nucléos",
            german: "Monozyto",
            spanish: "Solosis",
            italian: "Solosis",
            english: "Solosis"
        }
    },
    duosion: {
        num: 578,
        isdefault: true,
        baseExperience: 130,
        order: 651,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 100,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 579,
                trigger: "levelup",
                level: 41,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ダブラン",
            korean: "듀란",
            french: "Méios",
            german: "Mitodos",
            spanish: "Duosion",
            italian: "Duosion",
            english: "Duosion"
        }
    },
    reuniclus: {
        num: 579,
        isdefault: true,
        baseExperience: 221,
        order: 652,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 50,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ランクルス",
            korean: "란쿨루스",
            french: "Symbios",
            german: "Zytomega",
            spanish: "Reuniclus",
            italian: "Reuniclus",
            english: "Reuniclus"
        }
    },
    ducklett: {
        num: 580,
        isdefault: true,
        baseExperience: 61,
        order: 653,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 581,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コアルヒー",
            korean: "꼬지보리",
            french: "Couaneton",
            german: "Piccolente",
            spanish: "Ducklett",
            italian: "Ducklett",
            english: "Ducklett"
        }
    },
    swanna: {
        num: 581,
        isdefault: true,
        baseExperience: 166,
        order: 654,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "スワンナ",
            korean: "스완나",
            french: "Lakmécygne",
            german: "Swaroness",
            spanish: "Swanna",
            italian: "Swanna",
            english: "Swanna"
        }
    },
    vanillite: {
        num: 582,
        isdefault: true,
        baseExperience: 61,
        order: 655,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 583,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バニプッチ",
            korean: "바닐프티",
            french: "Sorbébé",
            german: "Gelatini",
            spanish: "Vanillite",
            italian: "Vanillite",
            english: "Vanillite"
        }
    },
    vanillish: {
        num: 583,
        isdefault: true,
        baseExperience: 138,
        order: 656,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 584,
                trigger: "levelup",
                level: 47,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バニリッチ",
            korean: "바닐리치",
            french: "Sorboul",
            german: "Gelatroppo",
            spanish: "Vanillish",
            italian: "Vanillish",
            english: "Vanillish"
        }
    },
    vanilluxe: {
        num: 584,
        isdefault: true,
        baseExperience: 241,
        order: 657,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "バイバニラ",
            korean: "배바닐라",
            french: "Sorbouboul",
            german: "Gelatwino",
            spanish: "Vanilluxe",
            italian: "Vanilluxe",
            english: "Vanilluxe"
        }
    },
    deerling: {
        num: 585,
        isdefault: true,
        baseExperience: 67,
        order: 658,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 586,
                trigger: "levelup",
                level: 34,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シキジカ",
            korean: "사철록",
            french: "Vivaldaim",
            german: "Sesokitz",
            spanish: "Deerling",
            italian: "Deerling",
            english: "Deerling"
        }
    },
    sawsbuck: {
        num: 586,
        isdefault: true,
        baseExperience: 166,
        order: 659,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "メブキジカ",
            korean: "바라철록",
            french: "Haydaim",
            german: "Kronjuwild",
            spanish: "Sawsbuck",
            italian: "Sawsbuck",
            english: "Sawsbuck"
        }
    },
    emolga: {
        num: 587,
        isdefault: true,
        baseExperience: 150,
        order: 660,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "エモンガ",
            korean: "에몽가",
            french: "Emolga",
            german: "Emolga",
            spanish: "Emolga",
            italian: "Emolga",
            english: "Emolga"
        }
    },
    karrablast: {
        num: 588,
        isdefault: true,
        baseExperience: 63,
        order: 661,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 589,
                trigger: "trade",
                trade: 616,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カブルモ",
            korean: "딱정곤",
            french: "Carabing",
            german: "Laukaps",
            spanish: "Karrablast",
            italian: "Karrablast",
            english: "Karrablast"
        }
    },
    escavalier: {
        num: 589,
        isdefault: true,
        baseExperience: 173,
        order: 662,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "シュバルゴ",
            korean: "슈바르고",
            french: "Lançargot",
            german: "Cavalanzas",
            spanish: "Escavalier",
            italian: "Escavalier",
            english: "Escavalier"
        }
    },
    foongus: {
        num: 590,
        isdefault: true,
        baseExperience: 59,
        order: 663,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 591,
                trigger: "levelup",
                level: 39,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "タマゲタケ",
            korean: "깜놀버슬",
            french: "Trompignon",
            german: "Tarnpignon",
            spanish: "Foongus",
            italian: "Foongus",
            english: "Foongus"
        }
    },
    amoonguss: {
        num: 591,
        isdefault: true,
        baseExperience: 162,
        order: 664,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "モロバレル",
            korean: "뽀록나",
            french: "Gaulet",
            german: "Hutsassa",
            spanish: "Amoonguss",
            italian: "Amoonguss",
            english: "Amoonguss"
        }
    },
    frillish: {
        num: 592,
        isdefault: true,
        baseExperience: 67,
        order: 665,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 10,
        genderDiff: 1,
        evomethods: [
            {
                num: 593,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "プルリル",
            korean: "탱그릴",
            french: "Viskuse",
            german: "Quabbel",
            spanish: "Frillish",
            italian: "Frillish",
            english: "Frillish"
        }
    },
    jellicent: {
        num: 593,
        isdefault: true,
        baseExperience: 168,
        order: 666,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 10,
        genderDiff: 1,
        names: {
            japanese: "ブルンゲル",
            korean: "탱탱겔",
            french: "Moyade",
            german: "Apoquallyp",
            spanish: "Jellicent",
            italian: "Jellicent",
            english: "Jellicent"
        }
    },
    alomomola: {
        num: 594,
        isdefault: true,
        baseExperience: 165,
        order: 667,
        growthRate: 3,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 6,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "ママンボウ",
            korean: "맘복치",
            french: "Mamanbo",
            german: "Mamolida",
            spanish: "Alomomola",
            italian: "Alomomola",
            english: "Alomomola"
        }
    },
    joltik: {
        num: 595,
        isdefault: true,
        baseExperience: 64,
        order: 668,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 596,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バチュル",
            korean: "파쪼옥",
            french: "Statitik",
            german: "Wattzapf",
            spanish: "Joltik",
            italian: "Joltik",
            english: "Joltik"
        }
    },
    galvantula: {
        num: 596,
        isdefault: true,
        baseExperience: 165,
        order: 669,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "デンチュラ",
            korean: "전툴라",
            french: "Mygavolt",
            german: "Voltula",
            spanish: "Galvantula",
            italian: "Galvantula",
            english: "Galvantula"
        }
    },
    ferroseed: {
        num: 597,
        isdefault: true,
        baseExperience: 61,
        order: 670,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 598,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "テッシード",
            korean: "철시드",
            french: "Grindur",
            german: "Kastadur",
            spanish: "Ferroseed",
            italian: "Ferroseed",
            english: "Ferroseed"
        }
    },
    ferrothorn: {
        num: 598,
        isdefault: true,
        baseExperience: 171,
        order: 671,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "ナットレイ",
            korean: "너트령",
            french: "Noacier",
            german: "Tentantel",
            spanish: "Ferrothorn",
            italian: "Ferrothorn",
            english: "Ferrothorn"
        }
    },
    klink: {
        num: 599,
        isdefault: true,
        baseExperience: 60,
        order: 672,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 130,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 600,
                trigger: "levelup",
                level: 38,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ギアル",
            korean: "기어르",
            french: "Tic",
            german: "Klikk",
            spanish: "Klink",
            italian: "Klink",
            english: "Klink"
        }
    },
    klang: {
        num: 600,
        isdefault: true,
        baseExperience: 154,
        order: 673,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 601,
                trigger: "levelup",
                level: 49,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ギギアル",
            korean: "기기어르",
            french: "Clic",
            german: "Kliklak",
            spanish: "Klang",
            italian: "Klang",
            english: "Klang"
        }
    },
    klinklang: {
        num: 601,
        isdefault: true,
        baseExperience: 234,
        order: 674,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "ギギギアル",
            korean: "기기기어르",
            french: "Cliticlic",
            german: "Klikdiklak",
            spanish: "Klinklang",
            italian: "Klinklang",
            english: "Klinklang"
        }
    },
    tynamo: {
        num: 602,
        isdefault: true,
        baseExperience: 55,
        order: 675,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 603,
                trigger: "levelup",
                level: 39,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シビシラス",
            korean: "저리어",
            french: "Anchwatt",
            german: "Zapplardin",
            spanish: "Tynamo",
            italian: "Tynamo",
            english: "Tynamo"
        }
    },
    eelektrik: {
        num: 603,
        isdefault: true,
        baseExperience: 142,
        order: 676,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 3,
        genderDiff: 0,
        evomethods: [
            {
                num: 604,
                trigger: "useitem",
                itemtrigger: "thunderstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シビビール",
            korean: "저리릴",
            french: "Lampéroie",
            german: "Zapplalek",
            spanish: "Eelektrik",
            italian: "Eelektrik",
            english: "Eelektrik"
        }
    },
    eelektross: {
        num: 604,
        isdefault: true,
        baseExperience: 232,
        order: 677,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 30,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "シビルドン",
            korean: "저리더프",
            french: "Ohmassacre",
            german: "Zapplarang",
            spanish: "Eelektross",
            italian: "Eelektross",
            english: "Eelektross"
        }
    },
    elgyem: {
        num: 605,
        isdefault: true,
        baseExperience: 67,
        order: 678,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 606,
                trigger: "levelup",
                level: 42,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "リグレー",
            korean: "리그레",
            french: "Lewsor",
            german: "Pygraulon",
            spanish: "Elgyem",
            italian: "Elgyem",
            english: "Elgyem"
        }
    },
    beheeyem: {
        num: 606,
        isdefault: true,
        baseExperience: 170,
        order: 679,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "オーベム",
            korean: "벰크",
            french: "Neitram",
            german: "Megalon",
            spanish: "Beheeyem",
            italian: "Beheeyem",
            english: "Beheeyem"
        }
    },
    litwick: {
        num: 607,
        isdefault: true,
        baseExperience: 55,
        order: 680,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 608,
                trigger: "levelup",
                level: 41,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒトモシ",
            korean: "불켜미",
            french: "Funécire",
            german: "Lichtel",
            spanish: "Litwick",
            italian: "Litwick",
            english: "Litwick"
        }
    },
    lampent: {
        num: 608,
        isdefault: true,
        baseExperience: 130,
        order: 681,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 609,
                trigger: "useitem",
                itemtrigger: "duskstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ランプラー",
            korean: "램프라",
            french: "Mélancolux",
            german: "Laternecto",
            spanish: "Lampent",
            italian: "Lampent",
            english: "Lampent"
        }
    },
    chandelure: {
        num: 609,
        isdefault: true,
        baseExperience: 234,
        order: 682,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "シャンデラ",
            korean: "샹델라",
            french: "Lugulabre",
            german: "Skelabra",
            spanish: "Chandelure",
            italian: "Chandelure",
            english: "Chandelure"
        }
    },
    axew: {
        num: 610,
        isdefault: true,
        baseExperience: 64,
        order: 683,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 611,
                trigger: "levelup",
                level: 38,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "キバゴ",
            korean: "터검니",
            chinese: "牙牙",
            french: "Coupenotte",
            german: "Milza",
            spanish: "Axew",
            italian: "Axew",
            english: "Axew"
        }
    },
    fraxure: {
        num: 611,
        isdefault: true,
        baseExperience: 144,
        order: 684,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 612,
                trigger: "levelup",
                level: 48,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "オノンド",
            korean: "액슨도",
            french: "Incisache",
            german: "Sharfax",
            spanish: "Fraxure",
            italian: "Fraxure",
            english: "Fraxure"
        }
    },
    haxorus: {
        num: 612,
        isdefault: true,
        baseExperience: 243,
        order: 685,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "オノノクス",
            korean: "액스라이즈",
            french: "Tranchodon",
            german: "Maxax",
            spanish: "Haxorus",
            italian: "Haxorus",
            english: "Haxorus"
        }
    },
    cubchoo: {
        num: 613,
        isdefault: true,
        baseExperience: 61,
        order: 686,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 614,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クマシュン",
            korean: "코고미",
            french: "Polarhume",
            german: "Petznief",
            spanish: "Cubchoo",
            italian: "Cubchoo",
            english: "Cubchoo"
        }
    },
    beartic: {
        num: 614,
        isdefault: true,
        baseExperience: 170,
        order: 687,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ツンベアー",
            korean: "툰베어",
            french: "Polagriffe",
            german: "Siberio",
            spanish: "Beartic",
            italian: "Beartic",
            english: "Beartic"
        }
    },
    cryogonal: {
        num: 615,
        isdefault: true,
        baseExperience: 170,
        order: 688,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 25,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "フリージオ",
            korean: "프리지오",
            french: "Hexagel",
            german: "Frigometri",
            spanish: "Cryogonal",
            italian: "Cryogonal",
            english: "Cryogonal"
        }
    },
    shelmet: {
        num: 616,
        isdefault: true,
        baseExperience: 61,
        order: 689,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 617,
                trigger: "trade",
                trade: 588,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チョボマキ",
            korean: "쪼마리",
            french: "Escargaume",
            german: "Schnuthelm",
            spanish: "Shelmet",
            italian: "Shelmet",
            english: "Shelmet"
        }
    },
    accelgor: {
        num: 617,
        isdefault: true,
        baseExperience: 173,
        order: 690,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "アギルダー",
            korean: "어지리더",
            french: "Limaspeed",
            german: "Hydragil",
            spanish: "Accelgor",
            italian: "Accelgor",
            english: "Accelgor"
        }
    },
    stunfisk: {
        num: 618,
        isdefault: true,
        baseExperience: 165,
        order: 691,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 3,
        genderDiff: 0,
        names: {
            japanese: "マッギョ",
            korean: "메더",
            french: "Limonde",
            german: "Flunschlik",
            spanish: "Stunfisk",
            italian: "Stunfisk",
            english: "Stunfisk"
        }
    },
    mienfoo: {
        num: 619,
        isdefault: true,
        baseExperience: 70,
        order: 692,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 620,
                trigger: "levelup",
                level: 50,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コジョフー",
            korean: "비조푸",
            french: "Kungfouine",
            german: "Lin-Fu",
            spanish: "Mienfoo",
            italian: "Mienfoo",
            english: "Mienfoo"
        }
    },
    mienshao: {
        num: 620,
        isdefault: true,
        baseExperience: 179,
        order: 693,
        growthRate: 4,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "コジョンド",
            korean: "비조도",
            french: "Shaofouine",
            german: "Wie-Shu",
            spanish: "Mienshao",
            italian: "Mienshao",
            english: "Mienshao"
        }
    },
    druddigon: {
        num: 621,
        isdefault: true,
        baseExperience: 170,
        order: 694,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "クリムガン",
            korean: "크리만",
            french: "Drakkarmin",
            german: "Shardrago",
            spanish: "Druddigon",
            italian: "Druddigon",
            english: "Druddigon"
        }
    },
    golett: {
        num: 622,
        isdefault: true,
        baseExperience: 61,
        order: 695,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 623,
                trigger: "levelup",
                level: 43,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゴビット",
            korean: "골비람",
            french: "Gringolem",
            german: "Golbit",
            spanish: "Golett",
            italian: "Golett",
            english: "Golett"
        }
    },
    golurk: {
        num: 623,
        isdefault: true,
        baseExperience: 169,
        order: 696,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ゴルーグ",
            korean: "골루그",
            french: "Golemastoc",
            german: "Golgantes",
            spanish: "Golurk",
            italian: "Golurk",
            english: "Golurk"
        }
    },
    pawniard: {
        num: 624,
        isdefault: true,
        baseExperience: 68,
        order: 697,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 120,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 625,
                trigger: "levelup",
                level: 52,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コマタナ",
            korean: "자망칼",
            french: "Scalpion",
            german: "Gladiantri",
            spanish: "Pawniard",
            italian: "Pawniard",
            english: "Pawniard"
        }
    },
    bisharp: {
        num: 625,
        isdefault: true,
        baseExperience: 172,
        order: 698,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "キリキザン",
            korean: "절각참",
            french: "Scalproie",
            german: "Caesurio",
            spanish: "Bisharp",
            italian: "Bisharp",
            english: "Bisharp"
        }
    },
    bouffalant: {
        num: 626,
        isdefault: true,
        baseExperience: 172,
        order: 699,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "バッフロン",
            korean: "버프론",
            french: "Frison",
            german: "Bisofank",
            spanish: "Bouffalant",
            italian: "Bouffalant",
            english: "Bouffalant"
        }
    },
    rufflet: {
        num: 627,
        isdefault: true,
        baseExperience: 70,
        order: 700,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 628,
                trigger: "levelup",
                level: 54,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ワシボン",
            korean: "수리둥보",
            french: "Furaiglon",
            german: "Geronimatz",
            spanish: "Rufflet",
            italian: "Rufflet",
            english: "Rufflet"
        }
    },
    braviary: {
        num: 628,
        isdefault: true,
        baseExperience: 179,
        order: 701,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ウォーグル",
            korean: "워글",
            french: "Gueriaigle",
            german: "Washakwil",
            spanish: "Braviary",
            italian: "Braviary",
            english: "Braviary"
        }
    },
    vullaby: {
        num: 629,
        isdefault: true,
        baseExperience: 74,
        order: 702,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 190,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 630,
                trigger: "levelup",
                level: 54,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バルチャイ",
            korean: "벌차이",
            french: "Vostourno",
            german: "Skallyk",
            spanish: "Vullaby",
            italian: "Vullaby",
            english: "Vullaby"
        }
    },
    mandibuzz: {
        num: 630,
        isdefault: true,
        baseExperience: 179,
        order: 703,
        growthRate: 1,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 60,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "バルジーナ",
            korean: "버랜지나",
            french: "Vaututrice",
            german: "Grypheldis",
            spanish: "Mandibuzz",
            italian: "Mandibuzz",
            english: "Mandibuzz"
        }
    },
    heatmor: {
        num: 631,
        isdefault: true,
        baseExperience: 169,
        order: 704,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "クイタラン",
            korean: "앤티골",
            french: "Aflamanoir",
            german: "Furnifraß",
            spanish: "Heatmor",
            italian: "Heatmor",
            english: "Heatmor"
        }
    },
    durant: {
        num: 632,
        isdefault: true,
        baseExperience: 169,
        order: 705,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 14,
        genderDiff: 0,
        names: {
            japanese: "アイアント",
            korean: "아이앤트",
            french: "Fermite",
            german: "Fermicula",
            spanish: "Durant",
            italian: "Durant",
            english: "Durant"
        }
    },
    deino: {
        num: 633,
        isdefault: true,
        baseExperience: 60,
        order: 706,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 634,
                trigger: "levelup",
                level: 50,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "モノズ",
            korean: "모노두",
            french: "Solochi",
            german: "Kapuno",
            spanish: "Deino",
            italian: "Deino",
            english: "Deino"
        }
    },
    zweilous: {
        num: 634,
        isdefault: true,
        baseExperience: 147,
        order: 707,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 635,
                trigger: "levelup",
                level: 64,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ジヘッド",
            korean: "디헤드",
            french: "Diamat",
            german: "Duodino",
            spanish: "Zweilous",
            italian: "Zweilous",
            english: "Zweilous"
        }
    },
    hydreigon: {
        num: 635,
        isdefault: true,
        baseExperience: 270,
        order: 708,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "サザンドラ",
            korean: "삼삼드래",
            french: "Trioxhydre",
            german: "Trikephalo",
            spanish: "Hydreigon",
            italian: "Hydreigon",
            english: "Hydreigon"
        }
    },
    larvesta: {
        num: 636,
        isdefault: true,
        baseExperience: 72,
        order: 709,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 637,
                trigger: "levelup",
                level: 59,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "メラルバ",
            korean: "활화르바",
            french: "Pyronille",
            german: "Ignivor",
            spanish: "Larvesta",
            italian: "Larvesta",
            english: "Larvesta"
        }
    },
    volcarona: {
        num: 637,
        isdefault: true,
        baseExperience: 248,
        order: 710,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 15,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "ウルガモス",
            korean: "불카모스",
            french: "Pyrax",
            german: "Ramoth",
            spanish: "Volcarona",
            italian: "Volcarona",
            english: "Volcarona"
        }
    },
    cobalion: {
        num: 638,
        isdefault: true,
        baseExperience: 261,
        order: 711,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "コバルオン",
            korean: "코바르온",
            french: "Cobaltium",
            german: "Kobalium",
            spanish: "Cobalion",
            italian: "Cobalion",
            english: "Cobalion"
        }
    },
    terrakion: {
        num: 639,
        isdefault: true,
        baseExperience: 261,
        order: 712,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "テラキオン",
            korean: "테라키온",
            french: "Terrakium",
            german: "Terrakium",
            spanish: "Terrakion",
            italian: "Terrakion",
            english: "Terrakion"
        }
    },
    virizion: {
        num: 640,
        isdefault: true,
        baseExperience: 261,
        order: 713,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ビリジオン",
            korean: "비리디온",
            french: "Viridium",
            german: "Viridium",
            spanish: "Virizion",
            italian: "Virizion",
            english: "Virizion"
        }
    },
    tornadus: {
        num: 641,
        isdefault: true,
        baseExperience: 261,
        order: 714,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 90,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 5,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "トルネロス",
            korean: "토네로스",
            french: "Boréas",
            german: "Boreos",
            spanish: "Tornadus",
            italian: "Tornadus",
            english: "Tornadus"
        }
    },
    tornadustherian: {
        num: 641,
        isdefault: false,
        baseExperience: 261,
        order: 715
    },
    thundurus: {
        num: 642,
        isdefault: true,
        baseExperience: 261,
        order: 716,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 90,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 2,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ボルトロス",
            korean: "볼트로스",
            french: "Fulguris",
            german: "Voltolos",
            spanish: "Thundurus",
            italian: "Thundurus",
            english: "Thundurus"
        }
    },
    thundurustherian: {
        num: 642,
        isdefault: false,
        baseExperience: 261,
        order: 717
    },
    reshiram: {
        num: 643,
        isdefault: true,
        baseExperience: 306,
        order: 718,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "レシラム",
            korean: "레시라무",
            chinese: "雷希拉姆",
            french: "Reshiram",
            german: "Reshiram",
            spanish: "Reshiram",
            italian: "Reshiram",
            english: "Reshiram"
        }
    },
    zekrom: {
        num: 644,
        isdefault: true,
        baseExperience: 306,
        order: 719,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 45,
        habitat: null,
        generation: 5,
        color: 1,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ゼクロム",
            korean: "제크로무",
            chinese: "捷克羅姆",
            french: "Zekrom",
            german: "Zekrom",
            spanish: "Zekrom",
            italian: "Zekrom",
            english: "Zekrom"
        }
    },
    landorus: {
        num: 645,
        isdefault: true,
        baseExperience: 270,
        order: 720,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 90,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 3,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ランドロス",
            korean: "랜드로스",
            french: "Démétéros",
            german: "Demeteros",
            spanish: "Landorus",
            italian: "Landorus",
            english: "Landorus"
        }
    },
    landorustherian: {
        num: 645,
        isdefault: false,
        baseExperience: 270,
        order: 721
    },
    kyurem: {
        num: 646,
        isdefault: true,
        baseExperience: 297,
        order: 722,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 4,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "キュレム",
            korean: "큐레무",
            french: "Kyurem",
            german: "Kyurem",
            spanish: "Kyurem",
            italian: "Kyurem",
            english: "Kyurem"
        }
    },
    kyuremblack: {
        num: 646,
        isdefault: false,
        baseExperience: 315,
        order: 723
    },
    kyuremwhite: {
        num: 646,
        isdefault: false,
        baseExperience: 315,
        order: 724
    },
    keldeo: {
        num: 647,
        isdefault: true,
        baseExperience: 261,
        order: 725,
        growthRate: 1,
        hatchCounter: 80,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 10,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ケルディオ",
            korean: "케르디오",
            french: "Keldeo",
            german: "Keldeo",
            spanish: "Keldeo",
            italian: "Keldeo",
            english: "Keldeo"
        }
    },
    keldeoresolute: {
        num: 647,
        isdefault: false,
        baseExperience: 261,
        order: 726
    },
    meloetta: {
        num: 648,
        isdefault: true,
        baseExperience: 270,
        order: 727,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 9,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "メロエッタ",
            korean: "메로엣타",
            french: "Meloetta",
            german: "Meloetta",
            spanish: "Meloetta",
            italian: "Meloetta",
            english: "Meloetta"
        }
    },
    meloettapirouette: {
        num: 648,
        isdefault: false,
        baseExperience: 270,
        order: 728
    },
    genesect: {
        num: 649,
        isdefault: true,
        baseExperience: 270,
        order: 729,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 5,
        color: 7,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ゲノセクト",
            korean: "게노세크트",
            french: "Genesect",
            german: "Genesect",
            spanish: "Genesect",
            italian: "Genesect",
            english: "Genesect"
        }
    },
    genesectdouse: {
        num: 649,
        isdefault: false
    },
    genesectshock: {
        num: 649,
        isdefault: false
    },
    genesectburn: {
        num: 649,
        isdefault: false
    },
    genesectchill: {
        num: 649,
        isdefault: false
    },
    chespin: {
        num: 650,
        isdefault: true,
        baseExperience: 63,
        order: 730,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 651,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハリマロン",
            korean: "도치마론",
            french: "Marisson",
            german: "Igamaro",
            spanish: "Chespin",
            italian: "Chespin",
            english: "Chespin"
        }
    },
    quilladin: {
        num: 651,
        isdefault: true,
        baseExperience: 142,
        order: 731,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 5,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 652,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ハリボーグ",
            korean: "도치보구",
            french: "Boguérisse",
            german: "Igastarnish",
            spanish: "Quilladin",
            italian: "Quilladin",
            english: "Quilladin"
        }
    },
    chesnaught: {
        num: 652,
        isdefault: true,
        baseExperience: 239,
        order: 732,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 5,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ブリガロン",
            korean: "브리가론",
            french: "Blindépique",
            german: "Brigaron",
            spanish: "Chesnaught",
            italian: "Chesnaught",
            english: "Chesnaught"
        }
    },
    fennekin: {
        num: 653,
        isdefault: true,
        baseExperience: 61,
        order: 733,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 654,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フォッコ",
            korean: "푸호꼬",
            french: "Feunnec",
            german: "Fynx",
            spanish: "Fennekin",
            italian: "Fennekin",
            english: "Fennekin"
        }
    },
    braixen: {
        num: 654,
        isdefault: true,
        baseExperience: 143,
        order: 734,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 655,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "テールナー",
            korean: "테르나",
            french: "Roussil",
            german: "Rutena",
            spanish: "Braixen",
            italian: "Braixen",
            english: "Braixen"
        }
    },
    delphox: {
        num: 655,
        isdefault: true,
        baseExperience: 240,
        order: 735,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "マフォクシー",
            korean: "마폭시",
            french: "Goupelin",
            german: "Fennexis",
            spanish: "Delphox",
            italian: "Delphox",
            english: "Delphox"
        }
    },
    froakie: {
        num: 656,
        isdefault: true,
        baseExperience: 63,
        order: 736,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 657,
                trigger: "levelup",
                level: 16,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ケロマツ",
            korean: "개구마르",
            french: "Grenousse",
            german: "Froxy",
            spanish: "Froakie",
            italian: "Froakie",
            english: "Froakie"
        }
    },
    frogadier: {
        num: 657,
        isdefault: true,
        baseExperience: 142,
        order: 737,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 12,
        genderDiff: 0,
        evomethods: [
            {
                num: 658,
                trigger: "levelup",
                level: 36,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ゲコガシラ",
            korean: "개굴반장",
            french: "Croâporal",
            german: "Amphizel",
            spanish: "Frogadier",
            italian: "Frogadier",
            english: "Frogadier"
        }
    },
    greninja: {
        num: 658,
        isdefault: true,
        baseExperience: 239,
        order: 738,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ゲッコウガ",
            korean: "개굴닌자",
            french: "Amphinobi",
            german: "Quajutsu",
            spanish: "Greninja",
            italian: "Greninja",
            english: "Greninja"
        }
    },
    greninjaash: {
        num: 658,
        isdefault: false
    },
    bunnelby: {
        num: 659,
        isdefault: true,
        baseExperience: 47,
        order: 739,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 660,
                trigger: "levelup",
                level: 20,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ホルビー",
            korean: "파르빗",
            french: "Sapereau",
            german: "Scoppel",
            spanish: "Bunnelby",
            italian: "Bunnelby",
            english: "Bunnelby"
        }
    },
    diggersby: {
        num: 660,
        isdefault: true,
        baseExperience: 148,
        order: 740,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 127,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ホルード",
            korean: "파르토",
            french: "Excavarenne",
            german: "Grebbit",
            spanish: "Diggersby",
            italian: "Diggersby",
            english: "Diggersby"
        }
    },
    fletchling: {
        num: 661,
        isdefault: true,
        baseExperience: 56,
        order: 741,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 662,
                trigger: "levelup",
                level: 17,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤヤコマ",
            korean: "화살꼬빈",
            french: "Passerouge",
            german: "Dartiri",
            spanish: "Fletchling",
            italian: "Fletchling",
            english: "Fletchling"
        }
    },
    fletchinder: {
        num: 662,
        isdefault: true,
        baseExperience: 134,
        order: 742,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 663,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒノヤコマ",
            korean: "불화살빈",
            french: "Braisillon",
            german: "Dartignis",
            spanish: "Fletchinder",
            italian: "Fletchinder",
            english: "Fletchinder"
        }
    },
    talonflame: {
        num: 663,
        isdefault: true,
        baseExperience: 175,
        order: 743,
        growthRate: 4,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "ファイアロー",
            korean: "파이어로",
            french: "Flambusard",
            german: "Fiaro",
            spanish: "Talonflame",
            italian: "Talonflame",
            english: "Talonflame"
        }
    },
    scatterbug: {
        num: 664,
        isdefault: true,
        baseExperience: 40,
        order: 744,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 255,
        habitat: null,
        generation: 6,
        color: 1,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 665,
                trigger: "levelup",
                level: 9,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コフキムシ",
            korean: "분이벌레",
            french: "Lépidonille",
            german: "Purmel",
            spanish: "Scatterbug",
            italian: "Scatterbug",
            english: "Scatterbug"
        }
    },
    spewpa: {
        num: 665,
        isdefault: true,
        baseExperience: 75,
        order: 745,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 6,
        color: 1,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 666,
                trigger: "levelup",
                level: 12,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "コフーライ",
            korean: "분떠도리",
            french: "Pérégrain",
            german: "Puponcho",
            spanish: "Spewpa",
            italian: "Spewpa",
            english: "Spewpa"
        }
    },
    vivillon: {
        num: 666,
        isdefault: true,
        baseExperience: 185,
        order: 746,
        growthRate: 2,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 1,
        shape: 13,
        genderDiff: 0,
        names: {
            japanese: "ビビヨン",
            korean: "비비용",
            french: "Prismillon",
            german: "Vivillon",
            spanish: "Vivillon",
            italian: "Vivillon",
            english: "Vivillon"
        }
    },
    vivillonfancy: {
        num: 666,
        isdefault: false
    },
    vivillonpokeball: {
        num: 666,
        isdefault: false
    },
    litleo: {
        num: 667,
        isdefault: true,
        baseExperience: 74,
        order: 747,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 220,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 668,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シシコ",
            korean: "레오꼬",
            french: "Hélionceau",
            german: "Leufeo",
            spanish: "Litleo",
            italian: "Litleo",
            english: "Litleo"
        }
    },
    pyroar: {
        num: 668,
        isdefault: true,
        baseExperience: 177,
        order: 748,
        growthRate: 4,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 65,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 8,
        genderDiff: 1,
        names: {
            japanese: "カエンジシ",
            korean: "화염레오",
            french: "Némélios",
            german: "Pyroleo",
            spanish: "Pyroar",
            italian: "Pyroar",
            english: "Pyroar"
        }
    },
    flabebe: {
        num: 669,
        isdefault: true,
        baseExperience: 61,
        order: 749,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 670,
                trigger: "levelup",
                level: 19,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フラベベ",
            korean: "플라베베",
            french: "Flabébé",
            german: "Flabébé",
            spanish: "Flabébé",
            italian: "Flabébé",
            english: "Flabébé"
        }
    },
    floette: {
        num: 670,
        isdefault: true,
        baseExperience: 130,
        order: 750,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 671,
                trigger: "useitem",
                itemtrigger: "shinystone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "フラエッテ",
            korean: "플라엣테",
            french: "Floette",
            german: "Floette",
            spanish: "Floette",
            italian: "Floette",
            english: "Floette"
        }
    },
    floetteeternal: {
        num: 670,
        isdefault: false,
        baseExperience: 243,
        order: 751
    },
    florges: {
        num: 671,
        isdefault: true,
        baseExperience: 248,
        order: 752,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "フラージェス",
            korean: "플라제스",
            french: "Florges",
            german: "Florges",
            spanish: "Florges",
            italian: "Florges",
            english: "Florges"
        }
    },
    skiddo: {
        num: 672,
        isdefault: true,
        baseExperience: 70,
        order: 753,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 673,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "メェークル",
            korean: "메이클",
            french: "Cabriolaine",
            german: "Mähikel",
            spanish: "Skiddo",
            italian: "Skiddo",
            english: "Skiddo"
        }
    },
    gogoat: {
        num: 673,
        isdefault: true,
        baseExperience: 186,
        order: 754,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ゴーゴート",
            korean: "고고트",
            french: "Chevroum",
            german: "Chevrumm",
            spanish: "Gogoat",
            italian: "Gogoat",
            english: "Gogoat"
        }
    },
    pancham: {
        num: 674,
        isdefault: true,
        baseExperience: 70,
        order: 755,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 220,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 675,
                trigger: "levelup",
                level: 32,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヤンチャム",
            korean: "판짱",
            french: "Pandespiègle",
            german: "Pam-Pam",
            spanish: "Pancham",
            italian: "Pancham",
            english: "Pancham"
        }
    },
    pangoro: {
        num: 675,
        isdefault: true,
        baseExperience: 173,
        order: 756,
        growthRate: 2,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 65,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ゴロンダ",
            korean: "부란다",
            french: "Pandarbare",
            german: "Pandagro",
            spanish: "Pangoro",
            italian: "Pangoro",
            english: "Pangoro"
        }
    },
    furfrou: {
        num: 676,
        isdefault: true,
        baseExperience: 165,
        order: 757,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 160,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "トリミアン",
            korean: "트리미앙",
            french: "Couafarel",
            german: "Coiffwaff",
            spanish: "Furfrou",
            italian: "Furfrou",
            english: "Furfrou"
        }
    },
    espurr: {
        num: 677,
        isdefault: true,
        baseExperience: 71,
        order: 758,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 6,
        color: 4,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 678,
                trigger: "levelup",
                level: 25,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニャスパー",
            korean: "냐스퍼",
            french: "Psystigri",
            german: "Psiau",
            spanish: "Espurr",
            italian: "Espurr",
            english: "Espurr"
        }
    },
    meowstic: {
        num: 678,
        isdefault: true,
        baseExperience: 163,
        order: 760,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 6,
        genderDiff: 1,
        names: {
            japanese: "ニャオニクス",
            korean: "냐오닉스",
            french: "Mistigrix",
            german: "Psiaugon",
            spanish: "Meowstic",
            italian: "Meowstic",
            english: "Meowstic"
        }
    },
    meowsticf: {
        num: 678,
        isdefault: false
    },
    honedge: {
        num: 679,
        isdefault: true,
        baseExperience: 65,
        order: 761,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 680,
                trigger: "levelup",
                level: 35,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヒトツキ",
            korean: "단칼빙",
            french: "Monorpale",
            german: "Gramokles",
            spanish: "Honedge",
            italian: "Honedge",
            english: "Honedge"
        }
    },
    doublade: {
        num: 680,
        isdefault: true,
        baseExperience: 157,
        order: 762,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 90,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 681,
                trigger: "useitem",
                itemtrigger: "duskstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ニダンギル",
            korean: "쌍검킬",
            french: "Dimoclès",
            german: "Duokles",
            spanish: "Doublade",
            italian: "Doublade",
            english: "Doublade"
        }
    },
    aegislash: {
        num: 681,
        isdefault: true,
        baseExperience: 234,
        order: 763,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ギルガルド",
            korean: "킬가르도",
            french: "Exagide",
            german: "Durengard",
            spanish: "Aegislash",
            italian: "Aegislash",
            english: "Aegislash"
        }
    },
    aegislashblade: {
        num: 681,
        isdefault: false,
        baseExperience: 234,
        order: 764
    },
    spritzee: {
        num: 682,
        isdefault: true,
        baseExperience: 68,
        order: 765,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 6,
        color: 6,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 683,
                trigger: "trade",
                helditem: 687,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "シュシュプ",
            korean: "슈쁘",
            french: "Fluvetin",
            german: "Parfi",
            spanish: "Spritzee",
            italian: "Spritzee",
            english: "Spritzee"
        }
    },
    aromatisse: {
        num: 683,
        isdefault: true,
        baseExperience: 162,
        order: 766,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 140,
        habitat: null,
        generation: 6,
        color: 6,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "フレフワン",
            korean: "프레프티르",
            french: "Cocotine",
            german: "Parfinesse",
            spanish: "Aromatisse",
            italian: "Aromatisse",
            english: "Aromatisse"
        }
    },
    swirlix: {
        num: 684,
        isdefault: true,
        baseExperience: 68,
        order: 767,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 200,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 7,
        genderDiff: 0,
        evomethods: [
            {
                num: 685,
                trigger: "trade",
                helditem: 686,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ペロッパフ",
            korean: "나룸퍼프",
            french: "Sucroquin",
            german: "Flauschling",
            spanish: "Swirlix",
            italian: "Swirlix",
            english: "Swirlix"
        }
    },
    slurpuff: {
        num: 685,
        isdefault: true,
        baseExperience: 168,
        order: 768,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 140,
        habitat: null,
        generation: 6,
        color: 9,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ペロリーム",
            korean: "나루림",
            french: "Cupcanaille",
            german: "Sabbaione",
            spanish: "Slurpuff",
            italian: "Slurpuff",
            english: "Slurpuff"
        }
    },
    inkay: {
        num: 686,
        isdefault: true,
        baseExperience: 58,
        order: 769,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 10,
        genderDiff: 0,
        evomethods: [
            {
                num: 687,
                trigger: "levelup",
                level: 30,
                rain: 0,
                upsidedown: 1
            }
        ],
        names: {
            japanese: "マーイーカ",
            korean: "오케이징",
            french: "Sepiatop",
            german: "Iscalar",
            spanish: "Inkay",
            italian: "Inkay",
            english: "Inkay"
        }
    },
    malamar: {
        num: 687,
        isdefault: true,
        baseExperience: 169,
        order: 770,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 80,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "カラマネロ",
            korean: "칼라마네로",
            french: "Sepiatroce",
            german: "Calamanero",
            spanish: "Malamar",
            italian: "Malamar",
            english: "Malamar"
        }
    },
    binacle: {
        num: 688,
        isdefault: true,
        baseExperience: 61,
        order: 771,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 11,
        genderDiff: 0,
        evomethods: [
            {
                num: 689,
                trigger: "levelup",
                level: 39,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カメテテ",
            korean: "거북손손",
            french: "Opermine",
            german: "Bithora",
            spanish: "Binacle",
            italian: "Binacle",
            english: "Binacle"
        }
    },
    barbaracle: {
        num: 689,
        isdefault: true,
        baseExperience: 175,
        order: 772,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 11,
        genderDiff: 0,
        names: {
            japanese: "ガメノデス",
            korean: "거북손데스",
            french: "Golgopathe",
            german: "Thanathora",
            spanish: "Barbaracle",
            italian: "Barbaracle",
            english: "Barbaracle"
        }
    },
    skrelp: {
        num: 690,
        isdefault: true,
        baseExperience: 64,
        order: 773,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 5,
        genderDiff: 0,
        evomethods: [
            {
                num: 691,
                trigger: "levelup",
                level: 48,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "クズモー",
            korean: "수레기",
            french: "Venalgue",
            german: "Algitt",
            spanish: "Skrelp",
            italian: "Skrelp",
            english: "Skrelp"
        }
    },
    dragalge: {
        num: 691,
        isdefault: true,
        baseExperience: 173,
        order: 774,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 55,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "ドラミドロ",
            korean: "드래캄",
            french: "Kravarech",
            german: "Tandrak",
            spanish: "Dragalge",
            italian: "Dragalge",
            english: "Dragalge"
        }
    },
    clauncher: {
        num: 692,
        isdefault: true,
        baseExperience: 66,
        order: 775,
        growthRate: 1,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 225,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 14,
        genderDiff: 0,
        evomethods: [
            {
                num: 693,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ウデッポウ",
            korean: "완철포",
            french: "Flingouste",
            german: "Scampisto",
            spanish: "Clauncher",
            italian: "Clauncher",
            english: "Clauncher"
        }
    },
    clawitzer: {
        num: 693,
        isdefault: true,
        baseExperience: 100,
        order: 776,
        growthRate: 1,
        hatchCounter: 15,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 55,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ブロスター",
            korean: "블로스터",
            french: "Gamblast",
            german: "Wummer",
            spanish: "Clawitzer",
            italian: "Clawitzer",
            english: "Clawitzer"
        }
    },
    helioptile: {
        num: 694,
        isdefault: true,
        baseExperience: 58,
        order: 777,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 6,
        color: 10,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 695,
                trigger: "useitem",
                itemtrigger: "sunstone",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "エリキテル",
            korean: "목도리키텔",
            french: "Galvaran",
            german: "Eguana",
            spanish: "Helioptile",
            italian: "Helioptile",
            english: "Helioptile"
        }
    },
    heliolisk: {
        num: 695,
        isdefault: true,
        baseExperience: 168,
        order: 778,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 6,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "エレザード",
            korean: "일레도리자드",
            french: "Iguolta",
            german: "Elezard",
            spanish: "Heliolisk",
            italian: "Heliolisk",
            english: "Heliolisk"
        }
    },
    tyrunt: {
        num: 696,
        isdefault: true,
        baseExperience: 72,
        order: 779,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 6,
        genderDiff: 0,
        evomethods: [
            {
                num: 697,
                trigger: "levelup",
                level: 39,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "チゴラス",
            korean: "티고라스",
            french: "Ptyranidur",
            german: "Balgoras",
            spanish: "Tyrunt",
            italian: "Tyrunt",
            english: "Tyrunt"
        }
    },
    tyrantrum: {
        num: 697,
        isdefault: true,
        baseExperience: 182,
        order: 780,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ガチゴラス",
            korean: "견고라스",
            french: "Rexillius",
            german: "Monargoras",
            spanish: "Tyrantrum",
            italian: "Tyrantrum",
            english: "Tyrantrum"
        }
    },
    amaura: {
        num: 698,
        isdefault: true,
        baseExperience: 72,
        order: 781,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 699,
                trigger: "levelup",
                level: 39,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "アマルス",
            korean: "아마루스",
            french: "Amagara",
            german: "Amarino",
            spanish: "Amaura",
            italian: "Amaura",
            english: "Amaura"
        }
    },
    aurorus: {
        num: 699,
        isdefault: true,
        baseExperience: 104,
        order: 782,
        growthRate: 2,
        hatchCounter: 30,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "アマルルガ",
            korean: "아마루르가",
            french: "Dragmara",
            german: "Amagarga",
            spanish: "Aurorus",
            italian: "Aurorus",
            english: "Aurorus"
        }
    },
    sylveon: {
        num: 700,
        isdefault: true,
        baseExperience: 184,
        order: 185,
        growthRate: 2,
        hatchCounter: 35,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 6,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ニンフィア",
            korean: "님피아",
            french: "Nymphali",
            german: "Feelinara",
            spanish: "Sylveon",
            italian: "Sylveon",
            english: "Sylveon"
        }
    },
    hawlucha: {
        num: 701,
        isdefault: true,
        baseExperience: 175,
        order: 783,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 100,
        habitat: null,
        generation: 6,
        color: 5,
        shape: 12,
        genderDiff: 0,
        names: {
            japanese: "ルチャブル",
            korean: "루차불",
            french: "Brutalibré",
            german: "Resladero",
            spanish: "Hawlucha",
            italian: "Hawlucha",
            english: "Hawlucha"
        }
    },
    dedenne: {
        num: 702,
        isdefault: true,
        baseExperience: 151,
        order: 784,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 180,
        habitat: null,
        generation: 6,
        color: 10,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "デデンネ",
            korean: "데덴네",
            french: "Dedenne",
            german: "Dedenne",
            spanish: "Dedenne",
            italian: "Dedenne",
            english: "Dedenne"
        }
    },
    carbink: {
        num: 703,
        isdefault: true,
        baseExperience: 100,
        order: 785,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 6,
        color: 4,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "メレシー",
            korean: "멜리시",
            french: "Strassie",
            german: "Rocara",
            spanish: "Carbink",
            italian: "Carbink",
            english: "Carbink"
        }
    },
    goomy: {
        num: 704,
        isdefault: true,
        baseExperience: 60,
        order: 786,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 7,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 705,
                trigger: "levelup",
                level: 40,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヌメラ",
            korean: "미끄메라",
            french: "Mucuscule",
            german: "Viscora",
            spanish: "Goomy",
            italian: "Goomy",
            english: "Goomy"
        }
    },
    sliggoo: {
        num: 705,
        isdefault: true,
        baseExperience: 158,
        order: 787,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 7,
        shape: 2,
        genderDiff: 0,
        evomethods: [
            {
                num: 706,
                trigger: "levelup",
                level: 50,
                rain: 1,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ヌメイル",
            korean: "미끄네일",
            french: "Colimucus",
            german: "Viscargot",
            spanish: "Sliggoo",
            italian: "Sliggoo",
            english: "Sliggoo"
        }
    },
    goodra: {
        num: 706,
        isdefault: true,
        baseExperience: 270,
        order: 788,
        growthRate: 1,
        hatchCounter: 40,
        isBaby: 0,
        baseHappiness: 35,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 7,
        shape: 6,
        genderDiff: 0,
        names: {
            japanese: "ヌメルゴン",
            korean: "미끄래곤",
            french: "Muplodocus",
            german: "Viscogon",
            spanish: "Goodra",
            italian: "Goodra",
            english: "Goodra"
        }
    },
    klefki: {
        num: 707,
        isdefault: true,
        baseExperience: 165,
        order: 789,
        growthRate: 3,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 75,
        habitat: null,
        generation: 6,
        color: 4,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "クレッフィ",
            korean: "클레피",
            french: "Trousselin",
            german: "Clavion",
            spanish: "Klefki",
            italian: "Klefki",
            english: "Klefki"
        }
    },
    phantump: {
        num: 708,
        isdefault: true,
        baseExperience: 62,
        order: 790,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 4,
        genderDiff: 0,
        evomethods: [
            {
                num: 709,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "ボクレー",
            korean: "나목령",
            french: "Brocélôme",
            german: "Paragoni",
            spanish: "Phantump",
            italian: "Phantump",
            english: "Phantump"
        }
    },
    trevenant: {
        num: 709,
        isdefault: true,
        baseExperience: 166,
        order: 791,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 10,
        genderDiff: 0,
        names: {
            japanese: "オーロット",
            korean: "대로트",
            french: "Desséliande",
            german: "Trombork",
            spanish: "Trevenant",
            italian: "Trevenant",
            english: "Trevenant"
        }
    },
    pumpkaboo: {
        num: 710,
        isdefault: true,
        baseExperience: 67,
        order: 793,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 120,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 1,
        genderDiff: 0,
        evomethods: [
            {
                num: 711,
                trigger: "trade",
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "バケッチャ",
            korean: "호바귀",
            french: "Pitrouille",
            german: "Irrbis",
            spanish: "Pumpkaboo",
            italian: "Pumpkaboo",
            english: "Pumpkaboo"
        }
    },
    pumpkaboosmall: {
        num: 710,
        isdefault: false,
        baseExperience: 67,
        order: 792
    },
    pumpkaboolarge: {
        num: 710,
        isdefault: false,
        baseExperience: 67,
        order: 794
    },
    pumpkaboosuper: {
        num: 710,
        isdefault: false,
        baseExperience: 67,
        order: 795
    },
    gourgeist: {
        num: 711,
        isdefault: true,
        baseExperience: 173,
        order: 797,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 60,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 5,
        genderDiff: 0,
        names: {
            japanese: "パンプジン",
            korean: "펌킨인",
            french: "Banshitrouye",
            german: "Pumpdjinn",
            spanish: "Gourgeist",
            italian: "Gourgeist",
            english: "Gourgeist"
        }
    },
    gourgeistsmall: {
        num: 711,
        isdefault: false,
        baseExperience: 173,
        order: 796
    },
    gourgeistlarge: {
        num: 711,
        isdefault: false,
        baseExperience: 173,
        order: 798
    },
    gourgeistsuper: {
        num: 711,
        isdefault: false,
        baseExperience: 173,
        order: 799
    },
    bergmite: {
        num: 712,
        isdefault: true,
        baseExperience: 61,
        order: 800,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 8,
        genderDiff: 0,
        evomethods: [
            {
                num: 713,
                trigger: "levelup",
                level: 37,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "カチコール",
            korean: "꽁어름",
            french: "Grelaçon",
            german: "Arktip",
            spanish: "Bergmite",
            italian: "Bergmite",
            english: "Bergmite"
        }
    },
    avalugg: {
        num: 713,
        isdefault: true,
        baseExperience: 180,
        order: 801,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 55,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "クレベース",
            korean: "크레베이스",
            french: "Séracrawl",
            german: "Arktilas",
            spanish: "Avalugg",
            italian: "Avalugg",
            english: "Avalugg"
        }
    },
    noibat: {
        num: 714,
        isdefault: true,
        baseExperience: 49,
        order: 802,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 190,
        habitat: null,
        generation: 6,
        color: 7,
        shape: 9,
        genderDiff: 0,
        evomethods: [
            {
                num: 715,
                trigger: "levelup",
                level: 48,
                rain: 0,
                upsidedown: 0
            }
        ],
        names: {
            japanese: "オンバット",
            korean: "음뱃",
            french: "Sonistrelle",
            german: "eF-eM",
            spanish: "Noibat",
            italian: "Noibat",
            english: "Noibat"
        }
    },
    noivern: {
        num: 715,
        isdefault: true,
        baseExperience: 187,
        order: 803,
        growthRate: 2,
        hatchCounter: 20,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 7,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "オンバーン",
            korean: "음번",
            french: "Bruyverne",
            german: "UHaFnir",
            spanish: "Noivern",
            italian: "Noivern",
            english: "Noivern"
        }
    },
    xerneas: {
        num: 716,
        isdefault: true,
        baseExperience: 306,
        order: 804,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 2,
        shape: 8,
        genderDiff: 0,
        names: {
            japanese: "ゼルネアス",
            korean: "제르네아스",
            french: "Xerneas",
            german: "Xerneas",
            spanish: "Xerneas",
            italian: "Xerneas",
            english: "Xerneas"
        }
    },
    yveltal: {
        num: 717,
        isdefault: true,
        baseExperience: 306,
        order: 805,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 45,
        habitat: null,
        generation: 6,
        color: 8,
        shape: 9,
        genderDiff: 0,
        names: {
            japanese: "イベルタル",
            korean: "이벨타르",
            french: "Yveltal",
            german: "Yveltal",
            spanish: "Yveltal",
            italian: "Yveltal",
            english: "Yveltal"
        }
    },
    zygarde: {
        num: 718,
        isdefault: true,
        baseExperience: 270,
        order: 806,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 0,
        captureRate: 3,
        habitat: null,
        generation: 6,
        color: 5,
        shape: 2,
        genderDiff: 0,
        names: {
            japanese: "ジガルデ",
            korean: "지가르데",
            french: "Zygarde",
            german: "Zygarde",
            spanish: "Zygarde",
            italian: "Zygarde",
            english: "Zygarde"
        }
    },
    zygarde10: {
        num: 718,
        isdefault: false
    },
    zygardecomplete: {
        num: 718,
        isdefault: false
    },
    diancie: {
        num: 719,
        isdefault: true,
        baseExperience: 270,
        order: 807,
        growthRate: 1,
        hatchCounter: 25,
        isBaby: 0,
        baseHappiness: 70,
        captureRate: 3,
        habitat: null,
        generation: 6,
        color: 6,
        shape: 4,
        genderDiff: 0,
        names: {
            japanese: "ディアンシー",
            korean: "디안시",
            french: "Diancie",
            german: "Diancie",
            spanish: "Diancie",
            italian: "Diancie",
            english: "Diancie"
        }
    },
    dianciemega: {
        num: 719,
        isdefault: false,
        baseExperience: 315,
        order: 808
    },
    hoopa: {
        num: 720,
        isdefault: true,
        baseExperience: 270,
        order: 809,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: null,
        generation: 6,
        color: 7,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "フーパ",
            korean: "후파",
            french: "Hoopa",
            german: "Hoopa",
            spanish: "Hoopa",
            italian: "Hoopa",
            english: "Hoopa"
        }
    },
    hoopaunbound: {
        num: 720,
        isdefault: false,
        baseExperience: 306,
        order: 810
    },
    volcanion: {
        num: 721,
        isdefault: true,
        baseExperience: 270,
        order: 811,
        growthRate: 1,
        hatchCounter: 120,
        isBaby: 0,
        baseHappiness: 100,
        captureRate: 3,
        habitat: null,
        generation: 6,
        color: 3,
        shape: 1,
        genderDiff: 0,
        names: {
            japanese: "ボルケニオン",
            korean: "볼케니온",
            french: "Volcanion",
            german: "Volcanion",
            spanish: "Volcanion",
            italian: "Volcanion",
            english: "Volcanion"
        }
    },
    rowlet: {
        num: 722,
        isdefault: true
    },
    dartrix: {
        num: 723,
        isdefault: true
    },
    decidueye: {
        num: 724,
        isdefault: true
    },
    litten: {
        num: 725,
        isdefault: true
    },
    torracat: {
        num: 726,
        isdefault: true
    },
    incineroar: {
        num: 727,
        isdefault: true
    },
    popplio: {
        num: 728,
        isdefault: true
    },
    brionne: {
        num: 729,
        isdefault: true
    },
    primarina: {
        num: 730,
        isdefault: true
    },
    pikipek: {
        num: 731,
        isdefault: true
    },
    trumbeak: {
        num: 732,
        isdefault: true
    },
    toucannon: {
        num: 733,
        isdefault: true
    },
    yungoos: {
        num: 734,
        isdefault: true
    },
    gumshoos: {
        num: 735,
        isdefault: true
    },
    grubbin: {
        num: 736,
        isdefault: true
    },
    charjabug: {
        num: 737,
        isdefault: true
    },
    vikavolt: {
        num: 738,
        isdefault: true
    },
    crabrawler: {
        num: 739,
        isdefault: true
    },
    crabominable: {
        num: 740,
        isdefault: true
    },
    oricorio: {
        num: 741,
        isdefault: true
    },
    oricoriopompom: {
        num: 741,
        isdefault: false
    },
    oricoriopau: {
        num: 741,
        isdefault: false
    },
    oricoriosensu: {
        num: 741,
        isdefault: false
    },
    cutiefly: {
        num: 742,
        isdefault: true
    },
    ribombee: {
        num: 743,
        isdefault: true
    },
    rockruff: {
        num: 744,
        isdefault: true
    },
    lycanroc: {
        num: 745,
        isdefault: true
    },
    lycanrocmidnight: {
        num: 745,
        isdefault: false
    },
    wishiwashi: {
        num: 746,
        isdefault: true
    },
    wishiwashischool: {
        num: 746,
        isdefault: false
    },
    mareanie: {
        num: 747,
        isdefault: true
    },
    toxapex: {
        num: 748,
        isdefault: true
    },
    mudbray: {
        num: 749,
        isdefault: true
    },
    mudsdale: {
        num: 750,
        isdefault: true
    },
    dewpider: {
        num: 751,
        isdefault: true
    },
    araquanid: {
        num: 752,
        isdefault: true
    },
    fomantis: {
        num: 753,
        isdefault: true
    },
    lurantis: {
        num: 754,
        isdefault: true
    },
    morelull: {
        num: 755,
        isdefault: true
    },
    shiinotic: {
        num: 756,
        isdefault: true
    },
    salandit: {
        num: 757,
        isdefault: true
    },
    salazzle: {
        num: 758,
        isdefault: true
    },
    stufful: {
        num: 759,
        isdefault: true
    },
    bewear: {
        num: 760,
        isdefault: true
    },
    bounsweet: {
        num: 761,
        isdefault: true
    },
    steenee: {
        num: 762,
        isdefault: true
    },
    tsareena: {
        num: 763,
        isdefault: true
    },
    comfey: {
        num: 764,
        isdefault: true
    },
    oranguru: {
        num: 765,
        isdefault: true
    },
    passimian: {
        num: 766,
        isdefault: true
    },
    wimpod: {
        num: 767,
        isdefault: true
    },
    golisopod: {
        num: 768,
        isdefault: true
    },
    sandygast: {
        num: 769,
        isdefault: true
    },
    palossand: {
        num: 770,
        isdefault: true
    },
    pyukumuku: {
        num: 771,
        isdefault: true
    },
    typenull: {
        num: 772,
        isdefault: true
    },
    silvally: {
        num: 773,
        isdefault: true
    },
    silvallybug: {
        num: 773,
        isdefault: false
    },
    silvallydark: {
        num: 773,
        isdefault: false
    },
    silvallydragon: {
        num: 773,
        isdefault: false
    },
    silvallyelectric: {
        num: 773,
        isdefault: false
    },
    silvallyfairy: {
        num: 773,
        isdefault: false
    },
    silvallyfighting: {
        num: 773,
        isdefault: false
    },
    silvallyfire: {
        num: 773,
        isdefault: false
    },
    silvallyflying: {
        num: 773,
        isdefault: false
    },
    silvallyghost: {
        num: 773,
        isdefault: false
    },
    silvallygrass: {
        num: 773,
        isdefault: false
    },
    silvallyground: {
        num: 773,
        isdefault: false
    },
    silvallyice: {
        num: 773,
        isdefault: false
    },
    silvallypoison: {
        num: 773,
        isdefault: false
    },
    silvallypsychic: {
        num: 773,
        isdefault: false
    },
    silvallyrock: {
        num: 773,
        isdefault: false
    },
    silvallysteel: {
        num: 773,
        isdefault: false
    },
    silvallywater: {
        num: 773,
        isdefault: false
    },
    minior: {
        num: 774,
        isdefault: true
    },
    miniormeteor: {
        num: 774,
        isdefault: false
    },
    komala: {
        num: 775,
        isdefault: true
    },
    turtonator: {
        num: 776,
        isdefault: true
    },
    togedemaru: {
        num: 777,
        isdefault: true
    },
    mimikyu: {
        num: 778,
        isdefault: true
    },
    mimikyubusted: {
        num: 778,
        isdefault: false
    },
    bruxish: {
        num: 779,
        isdefault: true
    },
    drampa: {
        num: 780,
        isdefault: true
    },
    dhelmise: {
        num: 781,
        isdefault: true
    },
    jangmoo: {
        num: 782,
        isdefault: true
    },
    hakamoo: {
        num: 783,
        isdefault: true
    },
    kommoo: {
        num: 784,
        isdefault: true
    },
    tapukoko: {
        num: 785,
        isdefault: true
    },
    tapulele: {
        num: 786,
        isdefault: true
    },
    tapubulu: {
        num: 787,
        isdefault: true
    },
    tapufini: {
        num: 788,
        isdefault: true
    },
    cosmog: {
        num: 789,
        isdefault: true
    },
    cosmoem: {
        num: 790,
        isdefault: true
    },
    solgaleo: {
        num: 791,
        isdefault: true
    },
    lunala: {
        num: 792,
        isdefault: true
    },
    nihilego: {
        num: 793,
        isdefault: true
    },
    buzzwole: {
        num: 794,
        isdefault: true
    },
    pheromosa: {
        num: 795,
        isdefault: true
    },
    xurkitree: {
        num: 796,
        isdefault: true
    },
    celesteela: {
        num: 797,
        isdefault: true
    },
    kartana: {
        num: 798,
        isdefault: true
    },
    guzzlord: {
        num: 799,
        isdefault: true
    },
    necrozma: {
        num: 800,
        isdefault: true
    },
    magearna: {
        num: 801,
        isdefault: true
    },
    magearnaoriginal: {
        num: 801,
        isdefault: false
    },
    marshadow: {
        num: 802,
        isdefault: true
    },
    missingno: {
        num: 0,
        isdefault: true
    },
    tomohawk: {
        num: -1,
        isdefault: true
    },
    necturna: {
        num: -2,
        isdefault: true
    },
    mollux: {
        num: -3,
        isdefault: true
    },
    aurumoth: {
        num: -4,
        isdefault: true
    },
    malaconda: {
        num: -5,
        isdefault: true
    },
    cawmodore: {
        num: -6,
        isdefault: true
    },
    volkraken: {
        num: -7,
        isdefault: true
    },
    plasmanta: {
        num: -8,
        isdefault: true
    },
    naviathan: {
        num: -9,
        isdefault: true
    },
    crucibelle: {
        num: -10,
        isdefault: true
    },
    crucibellemega: {
        num: -10,
        isdefault: false
    },
    kerfluffle: {
        num: -11,
        isdefault: true
    },
    syclant: {
        num: -51,
        isdefault: true
    },
    revenankh: {
        num: -52,
        isdefault: true
    },
    pyroak: {
        num: -53,
        isdefault: true
    },
    fidgit: {
        num: -54,
        isdefault: true
    },
    stratagem: {
        num: -55,
        isdefault: true
    },
    arghonaut: {
        num: -56,
        isdefault: true
    },
    kitsunoh: {
        num: -57,
        isdefault: true
    },
    cyclohm: {
        num: -58,
        isdefault: true
    },
    colossoil: {
        num: -59,
        isdefault: true
    },
    krilowatt: {
        num: -60,
        isdefault: true
    },
    voodoom: {
        num: -61,
        isdefault: true
    },
    syclar: {
        num: -101,
        isdefault: true
    },
    embirch: {
        num: -102,
        isdefault: true
    },
    flarelm: {
        num: -103,
        isdefault: true
    },
    breezi: {
        num: -104,
        isdefault: true
    },
    scratchet: {
        num: -105,
        isdefault: true
    },
    necturine: {
        num: -106,
        isdefault: true
    },
    cupra: {
        num: -107,
        isdefault: true
    },
    argalis: {
        num: -108,
        isdefault: true
    },
    brattler: {
        num: -109,
        isdefault: true
    },
    cawdet: {
        num: -110,
        isdefault: true
    },
    volkritter: {
        num: -111,
        isdefault: true
    },
    snugglow: {
        num: -112,
        isdefault: true
    },
    floatoy: {
        num: -113,
        isdefault: true
    },
    caimanoe: {
        num: -114,
        isdefault: true
    },
    pluffle: {
        num: -115,
        isdefault: true
    },
    pokestarufo: {
        num: -5001,
        isdefault: true
    },
    pokestarufo2: {
        num: -5001,
        isdefault: false
    },
    pokestarbrycenman: {
        num: -5002,
        isdefault: true
    },
    pokestarmt: {
        num: -5003,
        isdefault: true
    },
    pokestarmt2: {
        num: -5004,
        isdefault: true
    },
    pokestartransport: {
        num: -5005,
        isdefault: true
    },
    pokestargiant: {
        num: -5006,
        isdefault: true
    },
    pokestarhumanoid: {
        num: -5007,
        isdefault: true
    },
    pokestarmonster: {
        num: -5008,
        isdefault: true
    },
    pokestarf00: {
        num: -5009,
        isdefault: true
    },
    pokestarf002: {
        num: -5010,
        isdefault: true
    },
    pokestarspirit: {
        num: -5011,
        isdefault: true
    },
    pokestarblackdoor: {
        num: -5012,
        isdefault: true
    },
    pokestarwhitedoor: {
        num: -5013,
        isdefault: true
    },
    pokestarblackbelt: {
        num: -5014,
        isdefault: true
    },
    pokestarufopropu2: {
        num: -5001,
        isdefault: true
    }
}