var parse = require('csv-parse') 
var fs = require('fs'); 

function read(inputFile,linecb,finalcb){
	//Parse experience data.
	var parser = parse({delimiter: ','}, function (err, data) {
	    let fields = data[0];
	   	data.shift();

	    data.forEach(function(line){
	    	let get = {};
	    	get["string"] = function(field){
	    		if(fields.indexOf(field)==-1) return null;
				return line[fields.indexOf(field)]	    		
	    	}

	    	get["int"] = function(field){
	    		try{
	    		if(fields.indexOf(field)==-1) return null;
				return parseInt(line[fields.indexOf(field)])
				} catch(error){
					return null;
				}	    		
	    	}

	    	get["numbool"] = function(field){
	    		if(fields.indexOf(field)==-1) return null;
				return line[fields.indexOf(field)]=="1"	    		
	    	}

	    	get["float"] = function(field){
	    		if(fields.indexOf(field)==-1) return null;
				return parseFloat(line[fields.indexOf(field)])	    		
	    	}
	    	linecb(get)
	    });
	    finalcb();
	});
	fs.createReadStream(inputFile).pipe(parser);
}
exports.read = read;