let Util = require("./Util").Util;
let csv = require("./csv");
let Log = require("./logga");
let Tools = require("./showdown/tools");
let async = require("async")
let fs = require('fs');

let rates = {}

csv.read("dex/csv/experience.csv",function(get){
	let id = get.int("growth_rate_id");
	if(rates[id]==null){
		rates[id] = {}
	}
	let level = get.int("level");
	let experience = get.int("experience");
	rates[id][level] = experience;
},function(){
	write(rates,"experience","data/experience.js");	
});

function write(arr,name,file){
	let json = JSON.stringify(arr,null,4);
	json = "\n\nexports."+name+" = "+json;
	json = json.replace(/\"([^(\")"]+)\":/g,"$1:");
	fs.writeFile(file,json,function(){})
}
