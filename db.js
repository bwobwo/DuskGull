/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~db.js~
 * The main database connection file. 
 * The abstract class DB shows what methods needs implementing.
 */

const Settings = require("./settings");
const Util = require('./util')

class DB{
	init(cb){}
	put(obj,cb){}
	give(obj,cb){}
	giveFirst(obj,cb){}
	clear(cb){}
}

let mongo = require('mongodb')
class MongoDB{
	constructor(){
		this.initialized=false;
	}

	checkInit(){
		if(!this.initialized){
			throw InternalError("DBError","Database has not been initialized, you need to call DB.init(cb)")
		}
	}

	init(cb=Util.defcb()){
		this.client = mongo.MongoClient;		
		let thiz = this;
		this.client.connect(Settings.dbAddress, function(err, db) {
			if(err){
				console.log("Database Error (did you start mongodb?): "+err.message);
			}
			thiz.db = db;
			thiz.initialized=true;
			cb();
		});
	}

	clear(cb){
		cb();
	}

	remove(obj,col,cb){
		this.checkInit();
		col = this.db.collection(col);
		col.remove(obj,[], function(err,obj){
			if(err){
				throw err;
			}
			cb(obj);
		});
	}

	put(obj,col,cb){
		this.checkInit();
		col = this.db.collection(col);
		col.update({'_id':obj._id},obj,{upsert:true}, function(error){
			cb(error,obj);
		});
	}

	give(searchobj,collection,cb){
		this.checkInit();
		collection = this.db.collection(collection);
		let cursor = collection.find(searchobj);
		let docs = []
		cursor.each(
			function(err,doc){
				if(err){
					throw err;
				}

				if(doc==null){
					cb(null,docs[0]);
					return;
				}
				docs.push(doc);
			});
	}

	giveFirst(obj,collection,cb){
		this.checkInit();
		this.give(obj,collection,cb);
	}
}

module.exports = new MongoDB();