/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~dex.js~
 *
 * A global "Pokedex" for data related to the Pokemon games.
 * Its information is a merge of Showdowns internal BattleDex entries,
 * with some overworld data scrapped from PokeApi.
 *
 * The overworld data is now stored under duskgull/data.  
 *
 * Note: When both sources contain the same data tags, Showdown takes precedence.
 *
 * Data is queried globally through dex.load(...);
 *
 * The types of data supported currently are Pokemon, Moves and Items. 
 * 
 */

const owmon = require("./data/pokemon").pokemon
const owmoves = require("./data/moves").moves
const owitems = require("./data/items").items
const formes = require("./data/formes").formes
const itemdesc = require("./localization/itemdesc").desc
const pokelang = require("./localization/pokemondesc").desc

const Util = require("./util");
const Settings = require("./settings");
const experience = require('./data/experience');

/**
 * Tries to convert anything into a numeric Pokemon Generation
 */

/**
 * @class  A single object containing ALL available data from ALL Pokemon generations by defualt.
 */
class Dex{
	constructor(){
		this.isLoaded=false;
	}
	numericGen(ind){
		if(ind=="gennext") return 7;
		if(ind=="mixandmega") return 7;
		if(ind=="stadium") return 7;
		if(ind=="theorymon") return 7;
		if(ind=="base") return 7;
		if(typeof(ind)=='number'){
			return ind;
		}
		return parseInt(ind.substr(ind.length-1));
	}

	allGens(){
		let dexes = []
		for(let i in Tools.dexes){
			dexes.push(i);
		}
		return dexes;
	}

	//Empty generations means all generations. 
	addProperty(dextype,dexid,field,value,generation=[]){
		if(generation.length==0){
			generation = this.allGens();
			if(Tools.data[dextype][dexid]==null){
				Tools.data[dextype][dexid] = {}
			}

			Tools.data[dextype][dexid][field] = value;
			Tools.data[dextype][dexid]._id = dexid;
		}

		for(let i in generation){
			if(Tools.dexes[generation[i]].data[dextype][dexid]==null){
				Tools.dexes[generation[i]].data[dextype][dexid]={}
			}

			Tools.dexes[generation[i]].data[dextype][dexid][field] = value;
			Tools.dexes[generation[i]].data[dextype][dexid]._id = dexid;
		}
	}

	init(){
		console.log("Loading dexdata...");
		let t = Util.timestamp()

		Tools.includeData();
		Tools.includeModData();

		let js = 0;
		let gens = this.allGens();

		let Item = require('./item');

		for(let i in owitems){
			try{
				owitems[i].battleTarget = 
					Item.types[owitems[i].itemcategory].battleTarget;
			} catch(error){
			}
			try{
				owitems[i].owTarget = 
					Item.types[owitems[i].itemcategory].owTarget;
			} catch(error){
			}
		}

		for(let i in owmon){
			for(let j in owmon[i]){
				this.addProperty("Pokedex",i,j,owmon[i][j]);
			}

			for(let j in gens){
				let q = Tools.dexes[gens[j]].getTemplate(i);
				let genid = this.numericGen(gens[j]);

				let genlearnset = {}

				for(let move in q.learnset){
					for(let j in q.learnset[move]){
						let method = q.learnset[move][j];
						let methodgen = method.substring(0,1);
						if(methodgen==genid){
							if(genlearnset[move]==null){
								genlearnset[move] = []
							}

							genlearnset[move].push(method);
						}
					}
				}
				
				//Quick reference. Remove this. 
				q.name = q.species;

				//We add a few functions to Pokedex objects to make them easily accessable 
				//from Pokemon objects. 
				q.finalPrevo = function(){
					if(this.prevo!=null&&this.prevo!=""){
						return dex.load("pokemon",this.prevo).finalPrevo();
					}
					return this;
				}

				q.levelAndPercentAtExp = function(exp){
					let growth = experience[this.growthRate];
					for(let i=2;i<=100;i++){
						if(exp<growth[i]){
							let level = i-1;
							let lastExp = growth[i-1];
							let expOnLevel = exp-lastExp;
							let newLevelExp = growth[i]-lastExp;
							let percent = expOnLevel/newLevelExp;
							let lo = {'level':level,'percent':percent}
							return lo;
						}
					}
					return {'level':100,'percent':0};
				}

				//used by my sprite writer, probably nothing else. 
				q.formeName = function(no){
					try{
						let q = formes[this._id][no];
						if(q==null) return "";
						return q;
					} catch(error){
						return "";
					}
				}

				q.levelAtExp = function(exp){
					let q = experience[this.growthRate];
					if(exp<0) return 1;
					for(let i=2;i<=100;i++){
						if(q[i]>exp){
							return i-1;
						}
					}
					return 100;
				}

				q.expAtLevel = function(level){
					return experience[this.growthRate][level]
				}

				q.genlearnset = genlearnset;
				
				//Returns all moves learnable for this pokemon at a given level
				//(i.e. central for move reminders). 
				q.movesToLevel = function(level){
					let moves = []
					for(let move in this.genlearnset){
						for(let method in this.genlearnset[move]){
							

							let str = this.genlearnset[move][method];
							if(str.substring(1,2)=="L"){
								let movelevel = parseInt(str.substring(2));
								if(level>=movelevel){
									moves.push(move);
								}
							}
						}
					}
					return moves;
				}

				q.allMoves = function(level){
					let moves = []
					for(let move in this.genlearnset){
						for(let method in this.genlearnset[move]){
							let str = this.genlearnset[move][method];
							if(str.substring(1,2)=="L"){
								let movelevel = parseInt(str.substring(2));
								if(level>=movelevel){
									if(moves.indexOf(move)==-1){
										moves.push(move);										
									}
								}
							} else {
								if(moves.indexOf(move)==-1){
									moves.push(move);										
								}							
							}
						}
					}
					return moves
				}

				q.movesAt = function(level){
					let moves = []
					for(let move in this.genlearnset){
						for(let method in this.genlearnset[move]){
							let str = this.genlearnset[move][method];
							if(str.substring(1,2)=="L"){
								let movelevel = parseInt(str.substring(2));
								if(level==movelevel){
									moves.push(move);
								}
							}
						}
					}
					return moves
				}
				
				//@todo: this is inaccurate as hell.
				q.getStats = function(pokemon){
					return new BattleEngine.BattlePokemon(pokemon,{'battle':factorybattle}).baseStats;
				}

			}
		}

		for(let i in owmoves){
			for(let j in owmoves[i]){
				//Quick reference. Remove this. 
				this.addProperty("Movedex",i,j,owmoves[i][j])
			}
			for(let j in gens){
				Tools.dexes[gens[j]].getMove(i);
			}
		}

		for(let i in owitems){
			for(let j in owitems[i]){
				this.addProperty("Items",i,j,owitems[i][j])
			}
			for(let j in gens){
				Tools.dexes[gens[j]].getItem(i);
			}
		}	
		
		
		factorybattle = BattleEngine.construct(Tools.getFormat("gen"+Settings.GEN,false),false,new FakeMGR());
		console.log("Dexdata loading time ~"+Util.timestamp(t)+"ms")
		this.isLoaded=true;
	}	


	/**
	 * 
	 * @param  {[type]} dex     [description]
	 * @param  {[type]} query   [description]
	 * @param  {[type]} version [description]
	 * @example
	 * @return {[type]}         [description]
	 */
	findAll(dex,query,version){
		if(!this.isLoaded){
			throw InternalError("DexError","Dex has not been initialized, you need to call 'Dex.init()'")
		}

		dex = this.parseDex(dex);
		version = this.parseVersion(version);
		return Util.findAll(version.data[dex],query);
	}

	findFirst(dex,query,version){
		if(!this.isLoaded){
			throw InternalError("DexError","Dex has not been initialized, you need to call 'Dex.init()'")
		}
		
		dex = this.parseDex(dex);
		version = this.parseVersion(version);
		return Util.findFirst(version.data[dex],query);
	}

	/**
	 * Fetches a pokemon by its string or numeric id. 
	 * It's a separate function to mark that it's not really a 'search' as ids/nums are supposedly unique. 
	 * @param  {[type]} dex     [description]
	 * @param  {[type]} id      [description]
	 * @param  {[type]} version [description]
	 * @return {[type]}         [description]
	 */
	load(dex,id,version){
		if(!this.isLoaded){
			throw InternalError("DexError","Dex has not been initialized, you need to call 'Dex.init()'")
		}


		if(dex=="format"){
			return Tools.data.Formats[id];
		}

		if(Array.isArray(id)){
			return id.map(x=>(dex.load(dex,x,version)));
		}

		let num = parseInt(id)
		if(typeof(id)=='object'){
			return this.findFirst(dex,{_id:id._id},version);
		}

		if(isNaN(num)){
			return this.findFirst(dex,{_id:toId(id)},version);			
		} else {
			return this.findFirst(dex,{num:num},version);			
		}
	}

	//Lists mods available
	mods(){
		return Object.keys(Tools.dexes);
	}

	//Lists formats available
	formats(){
		let fmout = []
		for(let i in Tools.data.Formats){
			let format = Tools.data.Formats[i];
			if(format.effectType=="Format"&&!i.includes("random")&&!i.includes("custom")){

				format.id=i;
				fmout.push({id:i,name:format.name,section:format.section});
			}
		}
		return fmout;
	}

	parseDex(dex){
		dex = dex.replace("s",""); // lol
		switch(dex){
			case 'pokemon':
				return 'TemplateCache';
			case 'move':
				return 'MoveCache';
			case 'item':
				return 'ItemCache';
		}
	}

	parseVersion(version){
		if(version == null) return Tools.dexes["gen"+Settings.generation];
		return Tools.dexes[version];
	}

	itemDesc(term,version,language){
		return itemdesc[term][version][language]
	}

	//Gets the index of the forme of this pokemon 
	//(i.e. unown (201) is 1, castform is 2 etc)
	//Specifically used by my sprite manager. 
	gen5formemon(pokemon){
		let pmnoid = this.load("pokemon",pokemon).num;
		let g = []
		for(let i in formes){
			g.push(dex.load("pokemon",i).num);
		}
		g = g.sort((a,b)=>a-b);
		return g.indexOf(pmnoid);
	}
	formeOffset(pokemon){
		let pmonid = this.load("pokemon",pokemon).num
		let pmonname = this.load("pokemon",pokemon)._id

		let g = []
		for(let i in formes){
			g.push(this.load("pokemon",i).num);
		}
		g.sort((a,b)=>a-b);


		let p = []
		for(let i in g){
			p.push(this.load("pokemon",g[i])._id)
		}

		let cur = 0;
		for(let i in p){
			if(p[i]==pmonname) return cur;
			cur+=formes[p[i]].length-1;
		}
		return -1;
	}

	all(type){
		switch(type){
			case 'pokemon':
				return this.findAll('pokemon',{}).map(species=>(species.species))
			case 'item':
				return this.findALl('item',{}).map(item=>(item.id));
			case 'move':
				return this.findAll('move',{}).map(item=>(move.id));
			default:
				throw InternalError("");
		}
	}

	formeOffsetTable(){
		let pmonid = this.load("pokemon",pokemon).num
		let pmonname = this.load("pokemon",pokemon)._id
		let g = []
		for(let i in formes){
			g.push(this.load("pokemon",i).num);
		}
		g.sort((a,b)=>a-b);
		let p = []
		for(let i in g){
			p.push(this.load("pokemon",g[i])._id)
		}

		let cur = 0;
		
		let formes = {}
		for(let i in p){
			formes[g[i]] = cur;
			if(p[i]==pmonname) return cur;
			cur+=formes[p[i]].length-1;
		}
		return formes;
	}

	root(){
		return Tools.dexes;
	}

	//Gets the english Pokemon description for Pokemon Black version. 
	pokeDesc(pokemon){
		let pmonid = this.load("pokemon",pokemon).num;
		return pokelang[pmonid].flavor.find(x=>(
			x.version.name=="black"&&x.language.name=="en"
		)).flavor_text
	}
}


const dex = new Dex();

module.exports = dex;

//bugfix
const BattleEngine = require("./showdown/battle-engine");
class FakeMGR{engineMessage(){}}
let factorybattle;
