/*
 * DuskGull - Pokemon Game Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Initializes all subsystems required for the full world simulation.
 */
require("./errors");
require('colors')

//Note, I'm only making this global because showdown scripts require it and I'm not actually sure how to provide it otherwise. 
global.toId = function(text) {
	// this is a duplicate of Dex.getId, for performance reasons
	if (text && text.id) {
		text = text.id;
	} else if (text && text.userid) {
		text = text.userid;
	}
	if (typeof text !== 'string' && typeof text !== 'number') return '';
	return ('' + text).toLowerCase().replace(/[^a-z0-9]+/g, '');
}

//Also required :(
global.Tools = require('./showdown/Tools');

module.exports.Gen = require('./generation');
module.exports.Generator =  module.exports.Gen.Application();
module.exports.setGenerator = function(filepath,rng){
	module.exports.Generator = module.exports.Gen.Application(filepath,rng);
}

module.exports.DB = require('./DB');
module.exports.Util =  require('./util');
module.exports.Dex =  require('./dex');

module.exports.Server = require('./server');
module.exports.Settings =  require('./Settings');
module.exports.WorldData =  require('./save');
module.exports.Pokemon =  require('./pokemon');
module.exports.Trainer =  require('./trainer');
module.exports.Battle =  require('./battle');
module.exports.Errors = require('./errors');
module.exports.Tools = require('./showdown/tools');
module.exports.Functionize = require('./jsonizer').Functionize;

module.exports.init = function(cb=Util.defcb("Initializing DuskGull without callback...")){
	module.exports.Dex.init();
	module.exports.DB.init(function(){
		
		cb();
	})
}