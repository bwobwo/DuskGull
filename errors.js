/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


global.IsDuskGullError = function(error){
	if(error.errtype==null||error.errcat==null||error.errmsg==null) return false;
	return true;
}

//This describes broken internal functionality. 
//For now, this is only used knowingly in two places: 
//1. The API service wrapper when it catches non-described errors
//2. When a Battle objects queries Pokemon Showdown, because it's actually supposed to rule out other types of errors itself. 
global.InternalError = function(type,msg,error){
	let k = ({errcat:"internal",errtype:type,errmsg:msg});
	if(error==null){
		error = new Error(msg);
	}
	k.stack = error.stack.split("\n");
	return k;
}

//This describes protocol errors (i.e. giving a method something it's not at all supposed to get)
//These are things that would NEVER be considered valid. 
global.FormatError = function(type,msg){
	let k = ({errcat:"input",errtype:type,errmsg:msg});
	k.stack = new Error(msg).stack.split("\n");
	return k;
}

//This describes illegal actions that are still protocol-valid. 
//These are errors that a player should not be able to choose due to situation (i.e. choosing a non-existing target)
global.IllegalError = function(type,msg){
	let k = ({errcat:"illegal",errtype:type,errmsg:msg});
	k.stack = new Error(msg).stack.split("\n");
	return k;
}

//Missing functionality. 
global.ImplementationError = function(type,msg){
	let k = ({errcat:"implementation",errtype:type,errmsg:msg});
	k.stack = new Error(msg).stack.split("\n");
	return k;
}

//These are like illegal errors, but they are SUPPOSED to be choosable by the end player
//(For example, choosing a move that's out of PP or switching/running when trapped)
//Receiving this should display an error message to the player. 
//They don't have stack traces because well, they're supposed to be well-documented. 
//You don't pass a message to these because they must be localized (see localization/errors.js); 
global.PokemonError = function(type,info){
	return {errcat:"pokemon",errtype:type,data:info};
}

//This is used for missing savedata (or chunks/parts of savedata) 
//These should have very specific client-side handlers. 
global.MissingError = function(type,msg){
	let k = ({errcat:"missing",errtype:type,errmsg:msg});
	k.stack = new Error(msg).stack.split("\n");
	return k;
}