/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//This got its own file because its so large (and I don't even know where to put it)
function checkEvolutions(method,pokemon,data){
	let queue = [];
	if(!pokemon.species.evomethods||pokemon.species.evomethods.length==0)return [];
	
	for(let idx in pokemon.species.evomethods){
		let evomethod = pokemon.species.evomethods[idx];

		if(evomethod.trigger!=method) continue;
		
		//Basic one-liner requirement checks
		if(evomethod.level!=null&&evomethod.level>pokemon.getLevel()) continue;			
		if(evomethod.item!=null&&evomethod.item!=pokemon.item) continue;
		if(evomethod.gender&&evomethod.gender!=pokemon.gender) continue;
		if(evomethod.location!=null&&evomethod.location !=pokemon.location) continue;
		if(evomethod.gender!=null&&evomethod.gender!=pokemon.gender) continue;
		if(evomethod.happiness!=null&&evomethod.happiness>pokemon.happiness) continue;
		if(evomethod.beauty!=null&&evomethod.beauty>pokemon.beautiful) continue;
		if(evomethod.affection!=null&&evomethod.affection>pokemon.affection) continue;


		if(evomethod.trigger=="useitem"){
			if(data!=evomethod.itemtrigger) continue;
		}

		
		//This is a special case I believe only hitmon uses. 
		//Hitmonlee has 1 (i.e. higher atk),
		//Hitmonchan has -1 (i.e. higher def),
		//Hitmontop has 0 (i.e. atk=def,
		if(evomethod.stats!=null){
			let stat = evomethod.stats;
			if(stat==0){
				if(pokemon.evs.atk!=pokemon.evs.def){
					continue
				}
			}
			if(stat==-1){
				if(pokemon.evs.atk>pokemon.evs.def){
					continue;
				}
			}
			if(stat==1){
				if(pokemon.evs.atk<pokemon.evs.def){
					continue
				}
			}
		}

		
		//Species-in-party check (used by Mantyke>Mantine)
		if(evomethod.party!=null){
			//This is numeric
			let pokeid = evomethod.party;

			//We have to access the trainers party
			if(pokemon.trainer==null) continue;
			
			let found = false;
			for(let i in pokemon.trainer.party){
				if(pokemon.trainer.party[i].species.num==pokeid){
					found=true;
					break;
				}
			}
			if(!found) continue;
		}


		//Types-in-party check (used by sylveon)
		if(evomethod.partytype!=null){
			let typeid = evomethod.partytype;
			if(pokemon.trainer==null) continue;
			let found = false;
			for(let i in pokemon.trainer.party){
				for(let j in pokemon.trainer.party[i].types){
					if(types[j].num==typeid) found = true;
					break;
				}
				if(found) break;
			}
			if(!found) continue;
		}


		//Time-of-day check (used by eevee->umbreon/sylveon)
		if(evomethod.time!=null){
			let isDay = Clock.isDay();
			if(evomethod.time=="night"&&isDay){
				continue;
			}
			if(evomethod.time=="day"&&!isDay){
				continue;
			}
		}

		//Move check, used by Piloswine->Mamoswine
		if(evomethod.hasOwnProperty.move){
			let found = false;
			for(let i=0;i<pokemon.moves.length;i++){
				if(pokemon.moves[i].move.num == evomethod.move){
					found=true;
						break;
					}
				}
				if(!found) continue;
			}

		//Move-type check, used by sylveon
		if(evomethod.movetype){
			let found = false;
			for(let i in pokemon.moves){
				//@FIXME: I'm pretty sure internal uses strings and this is nums. 
				if(moves[i].move.type==evomethod.movetype){
					found=true;
					break;
				}
			}
			if(!found) continue;
		}

		//Trade-for-species check (used by shelmet>accelgor)
		if(evomethod.trade){
			if(!data) continue;
			if(data.species.num!=evomethod.trade) continue;
		}

		//Weather check, used by Sliggoo->Goodra
		if(evomethod.weather&&evomethod.weather!=0){
			if(pokemon.getWeather()!="raining") continue;
		}

		//The funniest method, used by inkay->malamar.
		if(evomethod.upsidedown!=null&&evomethod.upsidedown!=0){
			if(!pokemon.isUpsideDown()) continue;
		}

		//if we made it this far, it's a valid evolution.
		queue.push(evomethod);
	}
	return queue;
}

exports.checkEvolutions = checkEvolutions;