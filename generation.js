/**
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/************************************************
 *          Generator Factories                 *
 ************************************************/
function GenInt(v1,v2){
	let gen = {}
	gen.generator ="int";
	gen.low=Math.min(v1,v2);
	gen.high=Math.max(v1,v2);
	return gen;
}

function GenFloat(v1,v2){
	let gen={}
	gen.generator="float";
	gen.low=Math.min(v1,v2);
	gen.high=Math.max(v1,v2);
	return gen
}

function GenFile(fname){
	let gen = {}
	gen.generator ="file";
	gen.file = fname;
	return gen;
}

function GenList(args){
	let gen = {}
	gen.list = args;
	gen.generator="list";
	return gen;
}

function GenWeightList(args){
	let gen = {}
	gen.weightsum = 0;
	gen.generator="weightlist";
	gen.list=[]
	for(let i in args){
		gen.list.push(args[i]);
		gen.weightsum+=args[i].w
	}
	return gen;
}
function GenHalt(val){
	let gen = {}
	gen.val=val;
	gen.generator = function(rngstate){
		return gen.val;
	}
	return gen;
}
function GenMulti(count,unique,generator){
	let gen = {}
	gen.raw = generator;
	gen.count=count;
	gen.unique=unique;
	gen.generator="multi"
	return gen
}


/************************************************
 *                 RANDOMIZERS                  *
 ************************************************/

function RandomState(seed=[+new Date()]){
	/**
	 * Copyright (C) 2010 by Johannes Baagøe <baagoe@baagoe.org>

	 * Permission is hereby granted, free of charge, to any person obtaining a copy
	 * of this software and associated documentation files (the "Software"), to deal
	 * in the Software without restriction, including without limitation the rights
	 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	 * copies of the Software, and to permit persons to whom the Software is
	 * furnished to do so, subject to the following conditions:

	 * The above copyright notice and this permission notice shall be included in
	 * all copies or substantial portions of the Software.

	 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	 * THE SOFTWARE.
	 */
	let state = [0,0,0,1,0];

	let n = 0xefc8249d;
	let mash = function(data) {
	data = data.toString();
	for (let i = 0; i < data.length; i++) {
		n += data.charCodeAt(i);
		let h = 0.02519603282416938 * n;
		n = h >>> 0;
		h -= n;
		h *= n;
		n = h >>> 0;
		h -= n;
		n += h * 0x100000000; // 2^32
	}
	return (n >>> 0) * 2.3283064365386963e-10; // 2^-32
	};

	state[0] = mash(' ');
	state[1] = mash(' ');
	state[2] = mash(' ');

	for (let i = 0; i < seed.length; i++) {
		state[0] -= mash(seed[i]);
	   	if (state[0] < 0) {
			state[0] += 1;
		}
		state[1] -= mash(seed[i]);
		if (state[1] < 0) {
			state[1] += 1;
		}
		state[2] -= mash(seed[i]);
		if (state[2] < 0) {
			state[2] += 1;
	   }
	}
	mash = null;

	return state;
}

function RandomInt(min=Number.MIN_VALUE,max=Number.MAX_VALUE,state){
	return Math.floor(RandomScale(state) * (max - min + 1)) + min;
}

function RandomScale(state){
	if(state==null) return Math.random();
	if(typeof(state)=="string"){
		state = RandomState(state);
	}
	let t = 2091639 * state[0] + state[3] * 2.3283064365386963e-10; // 2^-32
	state[0] = state[1];
	state[1] = state[2];
	state[4]++;
	return state[2] = t - (state[3] = t | 0);
}

function RandomFloat(state,min=Number.MIN_VALUE,max=Number.MAX_VALUE){
	return (RandomScale(state)*(max-min)+min);
}

function RandomIndexWeight(list,weightsum,rngstate){
	let val = RandomScale(rngstate)*weightsum
	for(let i=0;i<list.length;i++){
		val-=list[i].w; //currently uses a 'w' property. Change this to fit your needs. 
		if(val<=0){
			return i;
		}
	}
}

function RandomItem(list,rngstate){
	return list[RandomIndex(list,rngstate)]
}

function RandomIndex(list,rngstate){
	return RandomInt(0,list.length-1,rngstate);
}

/************************************************
 *           Generator Application              *
 ************************************************/
function GenApplication(fileroot=null,defaultRng=RandomState()){
	let app = {}
	app.sync=true;
	app.defaultRng = defaultRng;

	app.generate = function(obj,rngstate=this.defaultRng){
		if(obj==null)return null;

		if(typeof(obj)!='object') return obj;
		
		if(obj.generator!=null){
			return this.generators(obj,rngstate);
		}
		for(let i in obj){
			obj[i] = this.generate(obj[i],rngstate);
		}
		return obj
	}

	app.generateFile = function(path,state){
		return this.generate(this.getGenerator(path),state);
	}

	/**
	 * This might be one of the scarier functions.
	 * Luckily you probably don't need to bother with it. 
	 * 
	 * It's where this are applied!
	 * 
	 * Because we want this to be cross-platform friendly,
	 * we need a central storage for their application. 
	 * 
	 * For portability reasons we use switches instead of objects. 
	 * If you want to add generator types at runtime, change this to a map/object instead.
	*/
	app.generators = function(gen,rngstate=this.defaultRng){
		switch(gen.generator){
			case "file":
				return this.generate(this.getGenerator(gen.file));
			case "halt":
				return gen.val;
			case "float":
				return this.generate(RandomFloat(gen.low,gen.high,rngstate));
			case "int":
				return this.generate(RandomInt(gen.low,gen.high,rngstate));
			case "list":
				return this.generate(gen.list[RandomIndex(gen.list,rngstate)]);			
			case "weightlist":
				return this.generate(
					gen.list[RandomIndexWeight(gen.list,gen.weightsum,rngstate)].v);			
			case "multi":
				let list = []
				//Non-generators
				multicount = this.generate(gen.count);
				if(typeof(gen.raw)!='object'||gen.raw.generator==null){
					for(let i=0;i<multicount;i++){
						list.push(this.generate(gen.raw));
					}
					return list;
				}
				
				//generators are just applied if we don't need unique checking.
				if(!gen.unique){
					for(let i=0;i<multicount;i++){
						list.push(this.generators(gen.raw,rngstate));
					}
					return list;
				}

				//Do ranges
				if(gen.raw.high!=null){
					let saved=[]
					for(let i=gen.raw.low;i<gen.raw.high;i++){
						saved.push(i);
					}

					for(let i=0;i<multicount;i++){
						if(saved.length<=0) throw new Error("uhhh what error is this??+!!")
						let index = RandomIndex(saved,rngstate);
						list.push(saved.splice(index,1)[0])
					}
					return list;
				}


				//Unique becomes really tricky
				let olist = gen.raw.list.slice();
				let oweight = gen.raw.weightsum;

				//Unique lists. 
				if(oweight==null){
					for(let i=0;i<multicount;i++){
						let index = RandomIndex(olist,rngstate);
						list.push(this.generate(olist.splice(index,1)[0]));
					}
					return list;
				}

				//Unique WEIGHTED lists...
				for(let i=0;i<multicount;i++){
					let index = RandomIndexWeight(olist,oweight,rngstate);
					let val = olist.splice(index,1)[0];
					list.push(val.v);
					oweight-=val.w;
				}
				return list;
		}
	}

	app.loadFiles = function(rot,root=""){
		let files = getFiles(rot).filter(function(file){
			return file.endsWith(".js");
		});


		for(let file in files){
			let path;
			try{
				let q = eval("x="+ReadFile(files[file]));
				path = files[file].replace(".js","");
				rot = replaceAll(rot)
				filepath = replaceAll(path,"/",".","\\")+".js";
				path = replaceAll(path,".","/","\\");
				rot = replaceAll(rot,".","/","\\");

				path = path.replace(rot,"")
				if(path.startsWith(".")) path = path.substring(1);

				path = path.split(".");
				gen = path.splice(-1,1)+".js";


				if(this.files==null) this.files={}
				let curobj = this.files;

				for(let i in path){
					let dir = path[i];
					if(curobj[dir]==null) curobj[dir] = {}
					curobj = curobj[dir];
				}

				curobj[gen] = q;
			} catch(error){
				console.log("Error in generator: ",path+"\\"+files[file]+" : "+error+" ");
			}
		}
	}
	
	app.getGenerator = function(path){
		path = replaceAll(path,".","/","\\");
		path = path.split(".");
		gen = path.splice(-1,1)+".js";

		let curobj = this.files;
		for(let i in path){
			let dir = path[i];
			if(curobj[dir] == null) return null;
			curobj = curobj[dir];
		}
		return curobj[gen];
	}
	if(fileroot!=null){
		app.files = {}
		app.fileroot=fileroot;
		app.loadFiles(app.fileroot);
	}
	return app;
}

/************************************************
 *                   MISC                       *
 ************************************************/
function W(val,weight){
	return {w:weight,v:val}
}
function replaceAll(str,replacement,...replace) {
	for(let i in replace){
		str = str.split(replace[i]).join(replacement);
	}
	return str;
};

/************************************************
 *             I/O (Configure this)             *
 ************************************************/
const walk = require("walk");
const fs = require("fs");

function ReadFile(file){
	return fs.readFileSync(file,"utf-8")
}
function getFiles(rot,sync=true,cb=function(){}){
	//Sync
	if(sync){
		let p=[]
		walk.walkSync(rot,{
			listeners:{
				file:function(dir,file){
					p.push(dir+"\\"+file.name);
				}
			}
		})
		return p;
	}

	//Async
	let walker = walk.walk(rot,{followLinks:false});
	let p=[]
	walker.on("file",function(dir,file,next){
		p.push(dir+file.name);
		next();
	})
	walker.on("end",function(){
		cb(p);
	});
}

/************************************************
 *        Exports (remove if on nashorn)        *
 ************************************************/
module.exports = {
	Random:{
		Int:RandomInt,
		Item:RandomItem,
		Index:RandomIndex,
		Float:RandomFloat
	},
	Int: GenInt,
	Float:GenFloat,
	File: GenFile,
	List: GenList,
	Multi: GenMulti,
	WeightList: GenWeightList,
	Halt: GenHalt,
	Application: GenApplication,
}