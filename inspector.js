require('colors')

let watchedPaths = {}

function getIndent(str){
	let indent=""
	for(let c of str){
		if(c==' '||c=='\t') indent+=c;
		else break;
	}
	return indent
}

function parseFunc(path){
	//1. Get text
	let fun = eval(path+".toString()")

	//2. Fix tabbing. 
	fun = fun.split("\n");	
	let indent = getIndent(fun[fun.length-1]);
	for(let row in fun){
		if(fun[row].startsWith(indent)){
			fun[row] = fun[row].replace(indent,"")
		}
	}
	fun = fun.join('\n');
	return fun
}

function formatCode(str){
	str = str.split('\n');
	for(let i in str){
		str[i] = (i+': ').yellow+(str[i].endsWith("//<--- YOUR INJECTION")?str[i].green:str[i]);
	}
	return str.join('\n');
}

let currentView=null;
function watch(req){
	let str=false;
	let arr = []
	cur="";
	for(let c of req){
		if(c==" "&&!str){
			arr.push(cur);
			cur="";
			continue;
		}
		if(c=='"') str = !str
		cur+=c;
	}
	arr.push(cur);

	let path = arr[1];
	path = path.split(".")
		.map(sub=>(sub==""?"prototype":sub)).join(".");

	let fun = parseFunc(path).split('\n');

	let row = parseInt(arr[3])-parseInt(arr[2]);
	let statement = getIndent(fun[row])+'console.log("[INJECTOR]['+path+']['+(row)+']:"+'+arr[4]+')\t//<--- YOUR INJECTION';
	
	fun.splice(row,0,statement);
	fun = fun.join('\n');

	let lasteval = path+"=eval(`x = function "+fun+"`)";
	
	console.log("Your function is now:\n"+formatCode(fun))
	eval(lasteval)
}

class Man{
	gringo(){
		let x = 25
	}
}


watch('insp Man..gringo 73 74 "hello world!"')

new Man().gringo()