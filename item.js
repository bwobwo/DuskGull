/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~item.js~
 * The main item file for shared item behavior. All item usage, battle and overworld, goes through here.
 */

/**
 * During battle, we have to check if a move is valid before we queue it. 
 * @param  {[type]} item   [description]
 * @param  {[type]} user   [description]
 * @param  {[type]} target [description]
 * @param  {[type]} battle [description]
 * @return {[type]}        [description]
 */

const Algorithms = require('./algorithms');
const Util = require('./util');

function canUseBattle(battle,item,user,target){
    item=getItem(item);

    //1. Check ruleset. 
    let allowed = battle.manager.settings.allowedItemTypes;
    if(!allowed
        ||(allowed.indexOf(item.itemcategory)==-1
            &&allowed.indexOf("all")==-1)) 
        throw PokemonError("ItemDisallowed");


    //2. Check specific
    if(item.canUseBattle!=null){
        item.canUseBattle(battle,user,target);
        return;
    }

    //3. Check item category if no specific was found. 
    let itemcat = exports.types[item.itemcategory];
    if(itemcat.canUseBattle==null){
        throw new PokemonError("NoItemBattleUse");
    }

    //4. Actually run the 
    itemcat.canUseBattle(battle,item,user,target);
}

/**
 * Uses an item in battle. 
 * This fails silently because error checks should've been done in canUseBattle. 
 * Check the log for errors from here. 
 */
function useBattle(battle,item,user,target){
    item=getItem(item);

    //1. Run specific handler first, if any. 
    if(item.useBattle!=null){
        user.battle.add("useitemtrainer",item,user,target,item.names.english,item.itemcategory);
        item.useBattle(battle,item,user,target);
        return;
    }

    //2. Run category handler, if any. 
    if(exports.types[item.itemcategory].useBattle!=null){
        user.battle.add("useitemtrainer",item,user,target,item.names.english,item.itemcategory);
        exports.types[item.itemcategory].useBattle(battle,item,user,target);
        return
    }
}

/**
 * Uses an item in overworld.
 * This does not fail silent, because error checking is done during execution.
 * (That's how the games does it) 
 */
function useOw(item,user,target){
    //Validate target
    target = user.pokeFromParty(target);
    if(target==null){
        throw IllegalError("NotInParty","Pokemon "+target+" is not in the party");
    }

    //It is not allowed to use items on Pokémon with residual requests
    target.checkRequests();

    item=getItem(item);
    if(item==null){
        throw FormatError("NoSuchItem","Item "+item+" has no definition.");
    }

    if(item.useOw!=null){
        var x = item.useOw(user,target);
        target.save();
        return {events:x,requests:target.requests};
    }

    let itemcat = exports.types[item.itemcategory];
    if(itemcat == null){
        //All items SHOULD have a valid itemcategory. 
        throw InternalError("MalformedItemCategory","Item has invalid item category: "+item.itemcategory);
    }

    if(itemcat.useOw!=null){
        var x = itemcat.useOw(item,user,target);
        target.save();
        return {events:x,requests:target.requests};
    }
    //Items with no use should hide its 'use' option. 
    throw IllegalError("NoItemUse","Item "+item.id+" has no overworld use: Hide its use option!");
}

function getItem(item){
    if(item==null){
        throw FormatError("NoSuchItem","Item "+item+" has no definition.");
    }
    if(!Util.isPointer(item)){
        return item;
    }
    return Dex.load("item",item);
}

exports.canUseBattle = canUseBattle;
exports.useBattle = useBattle;
exports.useOw = useOw;
exports.types = {
    ball:{
        battleTarget: "none",
        useBattle: function(battle,item,user){
            target = user.battle.manager.getSingleOpponent(user);
            if(target==null){
                console.log("no target");
                return;  
            }
            let rolls = Algorithms.catching(battle,user,item,target);
            user.battle.add("catch",user._id,target._id,rolls,rolls==-1,rolls==4);
            
            //4 rolls means successful catch.
            if(rolls==4){
                try{
                    user.battle.manager.catch = {user:user._id,target:target._id};                    
                } catch(error){
                    console.log("BALL ERROR: ",error);
                }
                user.battle.win(user.side);
            }
        },

        canUseBattle: function(battle,item,user,target){
            if(battle.manager.getSingleOpponent(user)==null){
                throw PokemonError("TooManyEnemies");
            }
        },
    },

    revive: {
        battleTarget: "partyfnt",
        owTarget: "partyfnt",

        canUseBattle: function(battle,item,user,target){
            return target.fainted;
        },

        useBattle: function(battle,item,user,target){
            throw ImplementationError("Revive isn't enabled yet, I'm working on it");
        },

        useOw: function(item,trainer,target){
            if(target.hp!=0){
                throw PokemonError("CantReviveAlive",target.name);
            }
            target.setHpRatio(item.revive);
        }
    },
    pprecovery: {
        battleTarget : "party",
        owTarget: "party",
        canUseBattle: function(battle,item,user,target){
            return !target.fainted;
        },
        useOw: function(item,user,target){
            if(user.hp==0) throw PokemonError("CantHealFainted",target.name);            
        },
        useBattle: function(battle,item,user,target){
            throw ImplementationError("PP recovery isn't implemented in battles yet");
        }
    },

    healing:{
        battleTarget : "party",
        owTarget : "party",
        canUseBattle: function(battle,item,user,target){
            return !target.fainted;
        },
        useOw: function(item,user,target){
            if(target.hp==0){
                throw PokemonError("CantHealFainted");
            }
            if(item.status!=null){
                let fail = target.healStatus(item.status);
                if(!item.heal){
                    return target.name + " was healed of "+item.status=='all'?
                    "all its status conditions":
                    "its "+item.status;
                }
            }
            if(item.heal){
                return target.heal(item.heal);
            }
        },
        useBattle: function(battle,item,user,target){
            
            if(item.revive){
                user.battle.healItem(item.revive*target.maxhp,target,user,true);
            }
            if(item.heal){
                user.battle.healItem(item.heal,target,user,false);
            }

            if(item.status){
                user.battle.cureStatus(item.status,target,user,false);
            }
        }
    },

    statboosts:{
        battleTarget: 'none', //it doesn't target, it's always applied on the user. 
        useBattle: function(battle,item,user){
            let q = {}
            q[item.stat] = 1;
            user.battle.boostItem(q)
        },
        canUseBattle: function(){
            //dead pokemon can't choose so
        }
    },

    //these are very similar to statboosts. 
    miracleshooter: {
        battleTarget: 'none',
        useBattle: function(battle,item,user){
            let q = {}
            q[item.stat] = item.amount;
            user.battle.boostItem(q)
        },
        canUseBattle: function(){
            //same as statboosts. 
        }
    },


    //berries that drop evs. 
    evdropberry:{
        owTarget: 'party',
        useOw: function(item,trainer,target){
            target.evs[item.ev] = Math.max(target.evs[item.ev]-10,0);
            if(target.happiness<100){
                target.happiness+=10;
            }
            else if(target.happiness<200){
                target.happiness+=5;
            }
            else{
                target.happines = Math.min(target.happiness+2,255);
            }
        }
    },

    berrypicky:{
        //@fixme: need to actually read in berry flavor data I think. That's not there right now. 
        //
    },


    //these are handled completely internally by showdown (not 'used' internally either way)
    megastones:{},

 

    flutes: {
        owTarget: "party",
        battleTarget: "party",
        
        //@researchme: no idea
        useBattle: function(battle,item,user,target){
        },
        canUseBattle: function(battle,item,user,target){
        },
        useOw: function(item,user,target){

        }

    },

    vitamins:{
        owTarget: 'party',
        useOw: function(item,trainer,target){
            //@fixme
            if(item.wing==true){
                target.gainEvs(item.stat,1);
            } else {
                if(target.evs[item.stat]<100){
                    target.gainEvs(item.stat,10);
                } else {
                    throw PokemonError("NoMoreVitamins");
                }
            }
        }
    },

    tm: {
        owTarget: 'party',
        useOw: function(item,trainer,target){
            let genstr = "pokemonxy";
            throw ImplementationError("Tms are not implemented");
        }
    },

    //@writeme: these probably need very specific coding per-item. They're used differently, too. 
    //(exp shares, cleanse tag, soothe bell all go here).
    trainingitem:{},

    evolution:{
        owTarget: 'party',
        useOw: function(item,trainer,target){
            let evoqueue = Evolution.checkEvolutions("useitem",target,item);
            Log.temp("Evoqueue: "+JSON.stringify(evoqueue));
            if(evoqueue.length!=0){
                target.setSpecies(evoqueue[0].num);
                return {events:[{event:"evolution",id:target._id,speciesid:target.species._id,speciesname:target.species.species}]}
            }
            return {events:[]}
        }
    },

    escapeitem:{
        battleTarget: 'party',
        useBattle: function(battle,item,user){
            throw ImplementationError("escape items are not implemented yet")            
        }
    },

    escaperope:{
        owTarget: 'none',
        //Implement this yourself. 
    },

    repels:{
        owTarget: 'none',
        
        useOw: function(item,trainer){
            if(trainer.hasRepel()){
                //@writeme language
                throw PokemonError("RepelAlreadyActive");
            }
            trainer.applyRepel(item.amount);
        }
    },

    repelflutes:{
        useOwn: function(item,trainer){
            //@writeme. A buff? 
        }
    },

    //no uses
    berryother:{},
    berryTypeProtection:{},
    baking:{},
    collectibles:{},
    helditems:{},
    choiceitems:{},
    effortitem:{},
    plates:{},
    speciesspecific:{},
    typeenhancement:{},

    eventitems:{},
    gameplays:{},
    plotadvancement:{},
    unuseditems:{},

    loot:{},
    mail:{},

    //not implemented
    mulch:{},
    dexcompletion:{},
    scarves:{},
    gems:{},
    datacards:{}
}

const Dex = require('./dex');