/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~jsonizer.js~
 *
 * Object mapper that supports turning objects with an '_id' into simplified 'pointers'. 
 * This is used extensively for save-objects and pokedex values. 
 * 
 * Pointers to objects are transformed to their '_id' fields before serialization
 * If '_id' field is missing (or pointing to another object/array), the object is ignored.  
 * 
 */
const cJSON = require('circular-json');

function JSONObject(objin,depth=0,hideDepth=1){

	if(typeof(objin)!='object'){
		return objin;
	}

	if(objin==null){
		return "null";
	}

	if(Array.isArray(objin)){
		return JSONArray(objin,depth); //Don't increase depth, we just mistook an array for obj.
 	}

	if(objin._id!=null&&depth>=hideDepth){
		let out = {'pointer':true,'_id':objin._id}
		if(objin.clazz!=null) out.clazz=objin.clazz;
		if(objin.num!=null) out.num=objin.num;
		if(objin.name!=null) out.name=objin.name;
		return out;
	}

	let outobj = {}
	for(let i in objin){
		let obj = objin[i];

		if(obj==null){
			outobj[i] = "null";
			continue;
		}
		outobj[i] = JSONObject(obj,depth+1);
	}
	return outobj;
}

function JSONArray(arrIn,depth=0){
	let outarr = [];
	for(let i in arrIn){
		//Note: Arrays don't increase depth for their children by design. 
		outarr.push(JSONObject(arrIn[i],depth))
	}
	return outarr;
}

function JSONString(objin,indents=4,hideDepth=0){
	let nobj;
	if(typeof(objin) =='object'){
		nobj = JSONObject(objin,0,hideDepth);
	}
	else {
		return ""+objin;
	}
	return cJSON.stringify(nobj,null,indents);
}


function tab(times){
	return "\t".repeat(times);
}

/**
 * Pretty much the contrary to all the other functions,
 * This one serializes everything, doesn't stop for _id fields,
 * and even keeps functions as strings. 
 */
function Functionize(obj,cur=0){
	if(typeof(obj)=='function') return obj;
	if(typeof(obj)=='string') return '"'+obj+'"';
	if(typeof(obj)!='object') return obj;
	let arr = Array.isArray(obj);
	let s = (arr?"[":"{")+"\n";
	for(let i in obj){
		s+=tab(cur+1)+(arr?"":i+": ")+Functionize(obj[i],cur+1)+",\n"
	}
	s = s.substring(0,s.length-2)+"\n";

	s+=tab(cur)+(arr?"]":"}");
	return s
}

exports.JSONObject = JSONObject;
exports.JSONArray = JSONArray;
exports.JSONString = JSONString;
exports.Functionize = Functionize;

//for old api 
global.JSONObject = JSONObject;
global.JSONArray = JSONArray;
global.JSONString = JSONString;
global.Functionize = Functionize;