exports.names = [
    {
        "id": "abomasite",
        "names": {
            "japanese": "ユキノオナイト",
            "korean": "눈설왕나이트",
            "french": "Blizzarite",
            "german": "Rexblisarnit",
            "spanish": "Abomasnowita",
            "italian": "Abomasnowite",
            "english": "Abomasite"
        }
    },
    {
        "id": "absolite",
        "names": {
            "japanese": "アブソルナイト",
            "korean": "앱솔나이트",
            "french": "Absolite",
            "german": "Absolnit",
            "spanish": "Absolita",
            "italian": "Absolite",
            "english": "Absolite"
        }
    },
    {
        "id": "absorbbulb",
        "names": {
            "japanese": "きゅうこん",
            "korean": "구근",
            "french": "Bulbe",
            "german": "Knolle",
            "spanish": "Tubérculo",
            "italian": "Bulbo",
            "english": "Absorb Bulb"
        }
    },
    {
        "id": "adamantorb",
        "names": {
            "japanese": "こんごうだま",
            "korean": "금강옥",
            "french": "Orbe Adamant",
            "german": "Adamant-Orb",
            "spanish": "Diamansfera",
            "italian": "Adamasfera",
            "english": "Adamant Orb"
        }
    },
    {
        "id": "adrenalineorb"
    },
    {
        "id": "aerodactylite",
        "names": {
            "japanese": "プテラナイト",
            "korean": "프테라나이트",
            "french": "Ptéraïte",
            "german": "Aerodactylonit",
            "spanish": "Aerodactylita",
            "italian": "Aerodactylite",
            "english": "Aerodactylite"
        }
    },
    {
        "id": "aggronite",
        "names": {
            "japanese": "ボスゴドラナイト",
            "korean": "보스로라나이트",
            "french": "Galekingite",
            "german": "Stollossnit",
            "spanish": "Aggronita",
            "italian": "Aggronite",
            "english": "Aggronite"
        }
    },
    {
        "id": "aguavberry",
        "names": {
            "japanese": "バンジのみ",
            "korean": "아바열매",
            "french": "Baie Gowav",
            "german": "Gauvebeere",
            "spanish": "Baya Guaya",
            "italian": "Baccaguava",
            "english": "Aguav Berry"
        }
    },
    {
        "id": "airballoon",
        "names": {
            "japanese": "ふうせん",
            "korean": "풍선",
            "french": "Ballon",
            "german": "Luftballon",
            "spanish": "Globo Helio",
            "italian": "Palloncino",
            "english": "Air Balloon"
        }
    },
    {
        "id": "alakazite",
        "names": {
            "japanese": "フーディナイト",
            "korean": "후디나이트",
            "french": "Alakazamite",
            "german": "Simsalanit",
            "spanish": "Alakazamita",
            "italian": "Alakazamite",
            "english": "Alakazite"
        }
    },
    {
        "id": "aloraichiumz"
    },
    {
        "id": "altarianite"
    },
    {
        "id": "ampharosite",
        "names": {
            "japanese": "デンリュウナイト",
            "korean": "전룡나이트",
            "french": "Pharampite",
            "german": "Ampharosnit",
            "spanish": "Ampharosita",
            "italian": "Ampharosite",
            "english": "Ampharosite"
        }
    },
    {
        "id": "apicotberry",
        "names": {
            "japanese": "ズアのみ",
            "korean": "규살열매",
            "french": "Baie Abriko",
            "german": "Apikobeere",
            "spanish": "Baya Aricoc",
            "italian": "Baccacocca",
            "english": "Apicot Berry"
        }
    },
    {
        "id": "armorfossil",
        "names": {
            "japanese": "たてのカセキ",
            "korean": "방패의화석",
            "french": "Fossile Armure",
            "german": "Panzerfossil",
            "spanish": "Fósil Coraza",
            "italian": "Fossilscudo",
            "english": "Armor Fossil"
        }
    },
    {
        "id": "aspearberry",
        "names": {
            "japanese": "ナナシのみ",
            "korean": "배리열매",
            "french": "Baie Willia",
            "german": "Wilbirbeere",
            "spanish": "Baya Perasi",
            "italian": "Baccaperina",
            "english": "Aspear Berry"
        }
    },
    {
        "id": "assaultvest",
        "names": {
            "japanese": "とつげきチョッキ",
            "korean": "돌격조끼",
            "french": "Veste de Combat",
            "german": "Offensivweste",
            "spanish": "Chaleco Asalto",
            "italian": "Corpetto assalto",
            "english": "Assault Vest"
        }
    },
    {
        "id": "audinite"
    },
    {
        "id": "babiriberry",
        "names": {
            "japanese": "リリバのみ",
            "korean": "바리비열매",
            "french": "Baie Babiri",
            "german": "Babiribeere",
            "spanish": "Baya Baribá",
            "italian": "Baccababiri",
            "english": "Babiri Berry"
        }
    },
    {
        "id": "banettite",
        "names": {
            "japanese": "ジュペッタナイト",
            "korean": "다크펫나이트",
            "french": "Branettite",
            "german": "Banetteonit",
            "spanish": "Banettita",
            "italian": "Banettite",
            "english": "Banettite"
        }
    },
    {
        "id": "beastball"
    },
    {
        "id": "beedrillite"
    },
    {
        "id": "belueberry",
        "names": {
            "japanese": "ベリブのみ",
            "korean": "루베열매",
            "french": "Baie Myrte",
            "german": "Myrtilbeere",
            "spanish": "Baya Andano",
            "italian": "Baccartillo",
            "english": "Belue Berry"
        }
    },
    {
        "id": "berryjuice",
        "names": {
            "japanese": "きのみジュース",
            "korean": "나무열매쥬스",
            "french": "Jus de Baie",
            "german": "Beerensaft",
            "spanish": "Zumo de Baya",
            "italian": "Succodibacca",
            "english": "Berry Juice"
        }
    },
    {
        "id": "bigroot",
        "names": {
            "japanese": "おおきなねっこ",
            "korean": "큰뿌리",
            "french": "Grosse Racine",
            "german": "Großwurzel",
            "spanish": "Raíz Grande",
            "italian": "Granradice",
            "english": "Big Root"
        }
    },
    {
        "id": "bindingband",
        "names": {
            "japanese": "しめつけバンド",
            "korean": "조임밴드",
            "french": "Bande Étreinte",
            "german": "Klammerband",
            "spanish": "Banda Atadura",
            "italian": "Legafascia",
            "english": "Binding Band"
        }
    },
    {
        "id": "blackbelt",
        "names": {
            "japanese": "くろおび",
            "korean": "검은띠",
            "french": "Ceinture Noire",
            "german": "Schwarzgurt",
            "spanish": "Cinturón Negro",
            "italian": "Cinturanera",
            "english": "Black Belt"
        }
    },
    {
        "id": "blacksludge",
        "names": {
            "japanese": "くろいヘドロ",
            "korean": "검은진흙",
            "french": "Boue Noire",
            "german": "Giftschleim",
            "spanish": "Lodo Negro",
            "italian": "Fangopece",
            "english": "Black Sludge"
        }
    },
    {
        "id": "blackglasses",
        "names": {
            "japanese": "くろいメガネ",
            "korean": "검은안경",
            "french": "Lunettes Noires",
            "german": "Schattenglas",
            "spanish": "Gafas de Sol",
            "italian": "Occhialineri",
            "english": "Black Glasses"
        }
    },
    {
        "id": "blastoisinite",
        "names": {
            "japanese": "カメックスナイト",
            "korean": "거북왕나이트",
            "french": "Tortankite",
            "german": "Turtoknit",
            "spanish": "Blastoisita",
            "italian": "Blastoisite",
            "english": "Blastoisinite"
        }
    },
    {
        "id": "blazikenite",
        "names": {
            "japanese": "バシャーモナイト",
            "korean": "번치코나이트",
            "french": "Braségalite",
            "german": "Lohgocknit",
            "spanish": "Blazikenita",
            "italian": "Blazikenite",
            "english": "Blazikenite"
        }
    },
    {
        "id": "blueorb",
        "names": {
            "japanese": "あいいろのたま",
            "korean": "쪽빛구슬",
            "french": "Orbe Bleu",
            "german": "Blaue Kugel",
            "spanish": "Esfera Azul",
            "italian": "Sfera Blu",
            "english": "Blue Orb"
        }
    },
    {
        "id": "blukberry",
        "names": {
            "japanese": "ブリーのみ",
            "korean": "블리열매",
            "french": "Baie Remu",
            "german": "Morbbeere",
            "spanish": "Baya Oram",
            "italian": "Baccamora",
            "english": "Bluk Berry"
        }
    },
    {
        "id": "brightpowder",
        "names": {
            "japanese": "ひかりのこな",
            "korean": "반짝가루",
            "french": "Poudre Claire",
            "german": "Blendpuder",
            "spanish": "Polvo Brillo",
            "italian": "Luminpolvere",
            "english": "Bright Powder"
        }
    },
    {
        "id": "buggem",
        "names": {
            "japanese": "むしのジュエル",
            "korean": "벌레주얼",
            "french": "Joyau Insecte",
            "german": "Käferjuwel",
            "spanish": "Gema Bicho",
            "italian": "Bijoucoleottero",
            "english": "Bug Gem"
        }
    },
    {
        "id": "bugmemory"
    },
    {
        "id": "buginiumz"
    },
    {
        "id": "burndrive",
        "names": {
            "japanese": "ブレイズカセット",
            "korean": "블레이즈카세트",
            "french": "Module Pyro",
            "german": "Flammenmodul",
            "spanish": "PiroROM",
            "italian": "Piromodulo",
            "english": "Burn Drive"
        }
    },
    {
        "id": "cameruptite"
    },
    {
        "id": "cellbattery",
        "names": {
            "japanese": "じゅうでんち",
            "korean": "충전지",
            "french": "Pile",
            "german": "Akku",
            "spanish": "Pila",
            "italian": "Ricaripila",
            "english": "Cell Battery"
        }
    },
    {
        "id": "charcoal",
        "names": {
            "japanese": "もくたん",
            "korean": "목탄",
            "french": "Charbon",
            "german": "Holzkohle",
            "spanish": "Carbón",
            "italian": "Carbonella",
            "english": "Charcoal"
        }
    },
    {
        "id": "charizarditex",
        "names": {
            "japanese": "リザードナイトＸ",
            "korean": "리자몽나이트X",
            "french": "Dracaufite X",
            "german": "Gluraknit X",
            "spanish": "Charizardita X",
            "italian": "Charizardite X",
            "english": "Charizardite X"
        }
    },
    {
        "id": "charizarditey",
        "names": {
            "japanese": "リザードナイトＹ",
            "korean": "리자몽나이트Y",
            "french": "Dracaufite Y",
            "german": "Gluraknit Y",
            "spanish": "Charizardita Y",
            "italian": "Charizardite Y",
            "english": "Charizardite Y"
        }
    },
    {
        "id": "chartiberry",
        "names": {
            "japanese": "ヨロギのみ",
            "korean": "루미열매",
            "french": "Baie Charti",
            "german": "Chiaribeere",
            "spanish": "Baya Alcho",
            "italian": "Baccaciofo",
            "english": "Charti Berry"
        }
    },
    {
        "id": "cheriberry",
        "names": {
            "japanese": "クラボのみ",
            "korean": "버치열매",
            "french": "Baie Ceriz",
            "german": "Amrenabeere",
            "spanish": "Baya Zreza",
            "italian": "Baccaliegia",
            "english": "Cheri Berry"
        }
    },
    {
        "id": "cherishball",
        "names": {
            "japanese": "プレシャスボール",
            "korean": "프레셔스볼",
            "french": "Mémoire Ball",
            "german": "Jubelball",
            "spanish": "Gloria Ball",
            "italian": "Pregio Ball",
            "english": "Cherish Ball"
        }
    },
    {
        "id": "chestoberry",
        "names": {
            "japanese": "カゴのみ",
            "korean": "유루열매",
            "french": "Baie Maron",
            "german": "Maronbeere",
            "spanish": "Baya Atania",
            "italian": "Baccastagna",
            "english": "Chesto Berry"
        }
    },
    {
        "id": "chilanberry",
        "names": {
            "japanese": "ホズのみ",
            "korean": "카리열매",
            "french": "Baie Zalis",
            "german": "Latchibeere",
            "spanish": "Baya Chilan",
            "italian": "Baccacinlan",
            "english": "Chilan Berry"
        }
    },
    {
        "id": "chilldrive",
        "names": {
            "japanese": "フリーズカセット",
            "korean": "프리즈카세트",
            "french": "Module Cryo",
            "german": "Gefriermodul",
            "spanish": "CrioROM",
            "italian": "Gelomodulo",
            "english": "Chill Drive"
        }
    },
    {
        "id": "choiceband",
        "names": {
            "japanese": "こだわりハチマキ",
            "korean": "구애머리띠",
            "french": "Bandeau Choix",
            "german": "Wahlband",
            "spanish": "Cinta Elegida",
            "italian": "Bendascelta",
            "english": "Choice Band"
        }
    },
    {
        "id": "choicescarf",
        "names": {
            "japanese": "こだわりスカーフ",
            "korean": "구애스카프",
            "french": "Mouchoir Choix",
            "german": "Wahlschal",
            "spanish": "Pañuelo Elegido",
            "italian": "Stolascelta",
            "english": "Choice Scarf"
        }
    },
    {
        "id": "choicespecs",
        "names": {
            "japanese": "こだわりメガネ",
            "korean": "구애안경",
            "french": "Lunettes Choix",
            "german": "Wahlglas",
            "spanish": "Gafas Elegid",
            "italian": "Lentiscelta",
            "english": "Choice Specs"
        }
    },
    {
        "id": "chopleberry",
        "names": {
            "japanese": "ヨプのみ",
            "korean": "로플열매",
            "french": "Baie Pomroz",
            "german": "Rospelbeere",
            "spanish": "Baya Pomaro",
            "italian": "Baccarosmel",
            "english": "Chople Berry"
        }
    },
    {
        "id": "clawfossil",
        "names": {
            "japanese": "ツメのカセキ",
            "korean": "발톱화석",
            "french": "Fossile Griffe",
            "german": "Klauenfossil",
            "spanish": "Fósil Garra",
            "italian": "Fossilunghia",
            "english": "Claw Fossil"
        }
    },
    {
        "id": "cobaberry",
        "names": {
            "japanese": "バコウのみ",
            "korean": "바코열매",
            "french": "Baie Cobaba",
            "german": "Kobabeere",
            "spanish": "Baya Kouba",
            "italian": "Baccababa",
            "english": "Coba Berry"
        }
    },
    {
        "id": "colburberry",
        "names": {
            "japanese": "ナモのみ",
            "korean": "마코열매",
            "french": "Baie Lampou",
            "german": "Burleobeere",
            "spanish": "Baya Dillo",
            "italian": "Baccaxan",
            "english": "Colbur Berry"
        }
    },
    {
        "id": "cornnberry",
        "names": {
            "japanese": "モコシのみ",
            "korean": "수숙열매",
            "french": "Baie Siam",
            "german": "Saimbeere",
            "spanish": "Baya Mais",
            "italian": "Baccavena",
            "english": "Cornn Berry"
        }
    },
    {
        "id": "coverfossil",
        "names": {
            "japanese": "ふたのカセキ",
            "korean": "덮개화석",
            "french": "Fossile Plaque",
            "german": "Schildfossil",
            "spanish": "Fósil Tapa",
            "italian": "Fossiltappo",
            "english": "Cover Fossil"
        }
    },
    {
        "id": "custapberry",
        "names": {
            "japanese": "イバンのみ",
            "korean": "애슈열매",
            "french": "Baie Chérim",
            "german": "Eipfelbeere",
            "spanish": "Baya Chiri",
            "italian": "Baccacrela",
            "english": "Custap Berry"
        }
    },
    {
        "id": "damprock",
        "names": {
            "japanese": "しめったいわ",
            "korean": "축축한바위",
            "french": "Roche Humide",
            "german": "Nassbrocken",
            "spanish": "Roca Lluvia",
            "italian": "Rocciaumida",
            "english": "Damp Rock"
        }
    },
    {
        "id": "darkgem",
        "names": {
            "japanese": "あくのジュエル",
            "korean": "악주얼",
            "french": "Joyau Ténèbres",
            "german": "Unlichtjuwel",
            "spanish": "Gema Siniestra",
            "italian": "Bijoubuio",
            "english": "Dark Gem"
        }
    },
    {
        "id": "darkmemory"
    },
    {
        "id": "darkiniumz"
    },
    {
        "id": "decidiumz"
    },
    {
        "id": "deepseascale",
        "names": {
            "japanese": "しんかいのウロコ",
            "korean": "심해의비늘",
            "french": "Écaille Océan",
            "german": "Abyssplatte",
            "spanish": "Escama Marina",
            "italian": "Squamabissi",
            "english": "Deep Sea Scale"
        }
    },
    {
        "id": "deepseatooth",
        "names": {
            "japanese": "しんかいのキバ",
            "korean": "심해의이빨",
            "french": "Dent Océan",
            "german": "Abysszahn",
            "spanish": "Diente Marino",
            "italian": "Dente Abissi",
            "english": "Deep Sea Tooth"
        }
    },
    {
        "id": "destinyknot",
        "names": {
            "japanese": "あかいいと",
            "korean": "빨간실",
            "french": "Nœud Destin",
            "german": "Fatumknoten",
            "spanish": "Lazo Destino",
            "italian": "Destincomune",
            "english": "Destiny Knot"
        }
    },
    {
        "id": "diancite"
    },
    {
        "id": "diveball",
        "names": {
            "japanese": "ダイブボール",
            "korean": "다이브볼",
            "french": "Scuba Ball",
            "german": "Tauchball",
            "spanish": "Buceo Ball",
            "italian": "Sub Ball",
            "english": "Dive Ball"
        }
    },
    {
        "id": "domefossil",
        "names": {
            "japanese": "こうらのカセキ",
            "korean": "껍질화석",
            "french": "Fossile Dôme",
            "german": "Domfossil",
            "spanish": "Fósil Domo",
            "italian": "Domofossile",
            "english": "Dome Fossil"
        }
    },
    {
        "id": "dousedrive",
        "names": {
            "japanese": "アクアカセット",
            "korean": "아쿠아카세트",
            "french": "Module Aqua",
            "german": "Aquamodul",
            "spanish": "HidroROM",
            "italian": "Idromodulo",
            "english": "Douse Drive"
        }
    },
    {
        "id": "dracoplate",
        "names": {
            "japanese": "りゅうのプレート",
            "korean": "용의플레이트",
            "french": "Plaque Draco",
            "german": "Dracotafel",
            "spanish": "Tabla Draco",
            "italian": "Lastradrakon",
            "english": "Draco Plate"
        }
    },
    {
        "id": "dragonfang",
        "names": {
            "japanese": "りゅうのキバ",
            "korean": "용의이빨",
            "french": "Croc Dragon",
            "german": "Drachenzahn",
            "spanish": "Colmillo Dragón",
            "italian": "Dentedidrago",
            "english": "Dragon Fang"
        }
    },
    {
        "id": "dragongem",
        "names": {
            "japanese": "ドラゴンジュエル",
            "korean": "드래곤주얼",
            "french": "Joyau Dragon",
            "german": "Drakojuwel",
            "spanish": "Gema Dragón",
            "italian": "Bijoudrago",
            "english": "Dragon Gem"
        }
    },
    {
        "id": "dragonmemory"
    },
    {
        "id": "dragoniumz"
    },
    {
        "id": "dreadplate",
        "names": {
            "japanese": "こわもてプレート",
            "korean": "공포플레이트",
            "french": "Plaque Ombre",
            "german": "Furchttafel",
            "spanish": "Tabla Oscura",
            "italian": "Lastratimore",
            "english": "Dread Plate"
        }
    },
    {
        "id": "dreamball",
        "names": {
            "japanese": "ドリームボール",
            "korean": "드림볼",
            "french": "Rêve Ball",
            "german": "Traumball",
            "spanish": "Ensueño Ball",
            "italian": "Dream Ball",
            "english": "Dream Ball"
        }
    },
    {
        "id": "durinberry",
        "names": {
            "japanese": "ドリのみ",
            "korean": "두리열매",
            "french": "Baie Durin",
            "german": "Durinbeere",
            "spanish": "Baya Rudion",
            "italian": "Baccadurian",
            "english": "Durin Berry"
        }
    },
    {
        "id": "duskball",
        "names": {
            "japanese": "ダークボール",
            "korean": "다크볼",
            "french": "Sombre Ball",
            "german": "Finsterball",
            "spanish": "Ocaso Ball",
            "italian": "Scuro Ball",
            "english": "Dusk Ball"
        }
    },
    {
        "id": "earthplate",
        "names": {
            "japanese": "だいちのプレート",
            "korean": "대지플레이트",
            "french": "Plaque Terre",
            "german": "Erdtafel",
            "spanish": "Tabla Terrax",
            "italian": "Lastrageo",
            "english": "Earth Plate"
        }
    },
    {
        "id": "eeviumz"
    },
    {
        "id": "ejectbutton",
        "names": {
            "japanese": "だっしゅつボタン",
            "korean": "탈출버튼",
            "french": "Bouton Fuite",
            "german": "Fluchtknopf",
            "spanish": "Botón Escape",
            "italian": "Pulsantefuga",
            "english": "Eject Button"
        }
    },
    {
        "id": "electirizer",
        "names": {
            "japanese": "エレキブースター",
            "korean": "에레키부스터",
            "french": "Électriseur",
            "german": "Stromisierer",
            "spanish": "Electrizador",
            "italian": "Elettritore",
            "english": "Electirizer"
        }
    },
    {
        "id": "electricgem",
        "names": {
            "japanese": "でんきのジュエル",
            "korean": "전기주얼",
            "french": "Joyau Électrik",
            "german": "Elektrojuwel",
            "spanish": "Gema Eléctrica",
            "italian": "Bijouelettro",
            "english": "Electric Gem"
        }
    },
    {
        "id": "electricmemory"
    },
    {
        "id": "electricseed"
    },
    {
        "id": "electriumz"
    },
    {
        "id": "energypowder",
        "names": {
            "japanese": "ちからのこな",
            "korean": "힘의가루",
            "french": "Poudrénergie",
            "german": "Energiestaub",
            "spanish": "Polvo Energía",
            "italian": "Polvenergia",
            "english": "Energy Powder"
        }
    },
    {
        "id": "enigmaberry",
        "names": {
            "japanese": "ナゾのみ",
            "korean": "의문열매",
            "french": "Baie Enigma",
            "german": "Enigmabeere",
            "spanish": "Baya Enigma",
            "italian": "Baccaenigma",
            "english": "Enigma Berry"
        }
    },
    {
        "id": "eviolite",
        "names": {
            "japanese": "しんかのきせき",
            "korean": "진화의휘석",
            "french": "Évoluroc",
            "german": "Evolith",
            "spanish": "Mineral Evol",
            "italian": "Evolcondensa",
            "english": "Eviolite"
        }
    },
    {
        "id": "expertbelt",
        "names": {
            "japanese": "たつじんのおび",
            "korean": "달인의띠",
            "french": "Ceinture Pro",
            "german": "Expertengurt",
            "spanish": "Cinta Experto",
            "italian": "Abilcintura",
            "english": "Expert Belt"
        }
    },
    {
        "id": "fairiumz"
    },
    {
        "id": "fairygem",
        "names": {
            "japanese": "ようせいジュエル",
            "korean": "페어리주얼",
            "french": "Joyau Fée",
            "german": "Feenjuwel",
            "spanish": "Gema Hada",
            "italian": "Bijoufolletto",
            "english": "Fairy Gem"
        }
    },
    {
        "id": "fairymemory"
    },
    {
        "id": "fastball",
        "names": {
            "japanese": "スピードボール",
            "korean": "스피드볼",
            "french": "Speed Ball",
            "german": "Turboball",
            "spanish": "Rapid Ball",
            "italian": "Rapid Ball",
            "english": "Fast Ball"
        }
    },
    {
        "id": "fightinggem",
        "names": {
            "japanese": "かくとうジュエル",
            "korean": "격투주얼",
            "french": "Joyau Combat",
            "german": "Kampfjuwel",
            "spanish": "Gema Lucha",
            "italian": "Bijoulotta",
            "english": "Fighting Gem"
        }
    },
    {
        "id": "fightingmemory"
    },
    {
        "id": "fightiniumz"
    },
    {
        "id": "figyberry",
        "names": {
            "japanese": "フィラのみ",
            "korean": "무화열매",
            "french": "Baie Figuy",
            "german": "Giefebeere",
            "spanish": "Baya Higog",
            "italian": "Baccafico",
            "english": "Figy Berry"
        }
    },
    {
        "id": "firegem",
        "names": {
            "japanese": "ほのおのジュエル",
            "korean": "불꽃주얼",
            "french": "Joyau Feu",
            "german": "Feuerjuwel",
            "spanish": "Gema Fuego",
            "italian": "Bijoufuoco",
            "english": "Fire Gem"
        }
    },
    {
        "id": "firememory"
    },
    {
        "id": "firiumz"
    },
    {
        "id": "fistplate",
        "names": {
            "japanese": "こぶしのプレート",
            "korean": "주먹플레이트",
            "french": "Plaque Poing",
            "german": "Fausttafel",
            "spanish": "Tabla Fuerte",
            "italian": "Lastrapugno",
            "english": "Fist Plate"
        }
    },
    {
        "id": "flameorb",
        "names": {
            "japanese": "かえんだま",
            "korean": "화염구슬",
            "french": "Orbe Flamme",
            "german": "Heiß-Orb",
            "spanish": "Llamasfera",
            "italian": "Fiammosfera",
            "english": "Flame Orb"
        }
    },
    {
        "id": "flameplate",
        "names": {
            "japanese": "ひのたまプレート",
            "korean": "불구슬플레이트",
            "french": "Plaque Flamme",
            "german": "Feuertafel",
            "spanish": "Tabla Llama",
            "italian": "Lastrarogo",
            "english": "Flame Plate"
        }
    },
    {
        "id": "floatstone",
        "names": {
            "japanese": "かるいし",
            "korean": "가벼운돌",
            "french": "Pierrallégée",
            "german": "Leichtstein",
            "spanish": "Piedra Pómez",
            "italian": "Pietralieve",
            "english": "Float Stone"
        }
    },
    {
        "id": "flyinggem",
        "names": {
            "japanese": "ひこうのジュエル",
            "korean": "비행주얼",
            "french": "Joyau Vol",
            "german": "Flugjuwel",
            "spanish": "Gema Voladora",
            "italian": "Bijouvolante",
            "english": "Flying Gem"
        }
    },
    {
        "id": "flyingmemory"
    },
    {
        "id": "flyiniumz"
    },
    {
        "id": "focusband",
        "names": {
            "japanese": "きあいのハチマキ",
            "korean": "기합의머리띠",
            "french": "Bandeau",
            "german": "Fokus-Band",
            "spanish": "Cinta Focus",
            "italian": "Bandana",
            "english": "Focus Band"
        }
    },
    {
        "id": "focussash",
        "names": {
            "japanese": "きあいのタスキ",
            "korean": "기합의띠",
            "french": "Ceinture Force",
            "german": "Fokusgurt",
            "spanish": "Banda Focus",
            "italian": "Focalnastro",
            "english": "Focus Sash"
        }
    },
    {
        "id": "friendball",
        "names": {
            "japanese": "フレンドボール",
            "korean": "프렌드볼",
            "french": "Copain Ball",
            "german": "Freundesball",
            "spanish": "Amigo Ball",
            "italian": "Friend Ball",
            "english": "Friend Ball"
        }
    },
    {
        "id": "fullincense",
        "names": {
            "japanese": "まんぷくおこう",
            "korean": "만복향로",
            "french": "Encens Plein",
            "german": "Lahmrauch",
            "spanish": "Incienso Lento",
            "italian": "Gonfioaroma",
            "english": "Full Incense"
        }
    },
    {
        "id": "galladite"
    },
    {
        "id": "ganlonberry",
        "names": {
            "japanese": "リュガのみ",
            "korean": "용아열매",
            "french": "Baie Lingan",
            "german": "Linganbeere",
            "spanish": "Baya Gonlan",
            "italian": "Baccalongan",
            "english": "Ganlon Berry"
        }
    },
    {
        "id": "garchompite",
        "names": {
            "japanese": "ガブリアスナイト",
            "korean": "한카리아스나이트",
            "french": "Carchacrokite",
            "german": "Knakracknit",
            "spanish": "Garchompita",
            "italian": "Garchompite",
            "english": "Garchompite"
        }
    },
    {
        "id": "gardevoirite",
        "names": {
            "japanese": "サーナイトナイト",
            "korean": "가디안나이트",
            "french": "Gardevoirite",
            "german": "Guardevoirnit",
            "spanish": "Gardevoirita",
            "italian": "Gardevoirite",
            "english": "Gardevoirite"
        }
    },
    {
        "id": "gengarite",
        "names": {
            "japanese": "ゲンガナイト",
            "korean": "팬텀나이트",
            "french": "Ectoplasmite",
            "german": "Gengarnit",
            "spanish": "Gengarita",
            "italian": "Gengarite",
            "english": "Gengarite"
        }
    },
    {
        "id": "ghostgem",
        "names": {
            "japanese": "ゴーストジュエル",
            "korean": "고스트주얼",
            "french": "Joyau Spectre",
            "german": "Geistjuwel",
            "spanish": "Gema Fantasma",
            "italian": "Bijouspettro",
            "english": "Ghost Gem"
        }
    },
    {
        "id": "ghostmemory"
    },
    {
        "id": "ghostiumz"
    },
    {
        "id": "glalitite"
    },
    {
        "id": "grassgem",
        "names": {
            "japanese": "くさのジュエル",
            "korean": "풀주얼",
            "french": "Joyau Plante",
            "german": "Pflanzjuwel",
            "spanish": "Gema Planta",
            "italian": "Bijouerba",
            "english": "Grass Gem"
        }
    },
    {
        "id": "grassmemory"
    },
    {
        "id": "grassiumz"
    },
    {
        "id": "grassyseed"
    },
    {
        "id": "greatball",
        "names": {
            "japanese": "スーパーボール",
            "korean": "수퍼볼",
            "french": "Super Ball",
            "german": "Superball",
            "spanish": "Super Ball",
            "italian": "Mega Ball",
            "english": "Great Ball"
        }
    },
    {
        "id": "grepaberry",
        "names": {
            "japanese": "ウブのみ",
            "korean": "또뽀열매",
            "french": "Baie Résin",
            "german": "Labrusbeere",
            "spanish": "Baya Uvav",
            "italian": "Baccauva",
            "english": "Grepa Berry"
        }
    },
    {
        "id": "gripclaw",
        "names": {
            "japanese": "ねばりのかぎづめ",
            "korean": "끈기갈고리손톱",
            "french": "Accro Griffe",
            "german": "Griffklaue",
            "spanish": "Garra Garfio",
            "italian": "Presartigli",
            "english": "Grip Claw"
        }
    },
    {
        "id": "griseousorb",
        "names": {
            "japanese": "はっきんだま",
            "korean": "백금옥",
            "french": "Orbe Platiné",
            "german": "Platinum-Orb",
            "spanish": "Griseosfera",
            "italian": "Grigiosfera",
            "english": "Griseous Orb"
        }
    },
    {
        "id": "groundgem",
        "names": {
            "japanese": "じめんのジュエル",
            "korean": "땅주얼",
            "french": "Joyau Sol",
            "german": "Bodenjuwel",
            "spanish": "Gema Tierra",
            "italian": "Bijouterra",
            "english": "Ground Gem"
        }
    },
    {
        "id": "groundmemory"
    },
    {
        "id": "groundiumz"
    },
    {
        "id": "gyaradosite",
        "names": {
            "japanese": "ギャラドスナイト",
            "korean": "갸라도스나이트",
            "french": "Léviatorite",
            "german": "Garadosnit",
            "spanish": "Gyaradosita",
            "italian": "Gyaradosite",
            "english": "Gyaradosite"
        }
    },
    {
        "id": "habanberry",
        "names": {
            "japanese": "ハバンのみ",
            "korean": "하반열매",
            "french": "Baie Fraigo",
            "german": "Terirobeere",
            "spanish": "Baya Anjiro",
            "italian": "Baccahaban",
            "english": "Haban Berry"
        }
    },
    {
        "id": "hardstone",
        "names": {
            "japanese": "かたいいし",
            "korean": "딱딱한돌",
            "french": "Pierre Dure",
            "german": "Granitstein",
            "spanish": "Piedra Dura",
            "italian": "Pietradura",
            "english": "Hard Stone"
        }
    },
    {
        "id": "healball",
        "names": {
            "japanese": "ヒールボール",
            "korean": "힐볼",
            "french": "Soin Ball",
            "german": "Heilball",
            "spanish": "Sana Ball",
            "italian": "Cura Ball",
            "english": "Heal Ball"
        }
    },
    {
        "id": "heatrock",
        "names": {
            "japanese": "あついいわ",
            "korean": "뜨거운바위",
            "french": "Roche Chaude",
            "german": "Heißbrocken",
            "spanish": "Roca Calor",
            "italian": "Rocciacalda",
            "english": "Heat Rock"
        }
    },
    {
        "id": "heavyball",
        "names": {
            "japanese": "ヘビーボール",
            "korean": "헤비볼",
            "french": "Masse Ball",
            "german": "Schwerball",
            "spanish": "Peso Ball",
            "italian": "Peso Ball",
            "english": "Heavy Ball"
        }
    },
    {
        "id": "helixfossil",
        "names": {
            "japanese": "かいのカセキ",
            "korean": "조개화석",
            "french": "Nautile",
            "german": "Helixfossil",
            "spanish": "Fósil Hélix",
            "italian": "Helixfossile",
            "english": "Helix Fossil"
        }
    },
    {
        "id": "heracronite",
        "names": {
            "japanese": "ヘラクロスナイト",
            "korean": "헤라크로스나이트",
            "french": "Scarhinoïte",
            "german": "Skarabornit",
            "spanish": "Heracrossita",
            "italian": "Heracrossite",
            "english": "Heracronite"
        }
    },
    {
        "id": "hondewberry",
        "names": {
            "japanese": "ロメのみ",
            "korean": "로매열매",
            "french": "Baie Lonme",
            "german": "Honmelbeere",
            "spanish": "Baya Meluce",
            "italian": "Baccamelon",
            "english": "Hondew Berry"
        }
    },
    {
        "id": "houndoominite",
        "names": {
            "japanese": "ヘルガナイト",
            "korean": "헬가나이트",
            "french": "Démolossite",
            "german": "Hundemonit",
            "spanish": "Houndoomita",
            "italian": "Houndoomite",
            "english": "Houndoominite"
        }
    },
    {
        "id": "iapapaberry",
        "names": {
            "japanese": "イアのみ",
            "korean": "파야열매",
            "french": "Baie Papaya",
            "german": "Yapabeere",
            "spanish": "Baya Pabaya",
            "italian": "Baccapaia",
            "english": "Iapapa Berry"
        }
    },
    {
        "id": "icegem",
        "names": {
            "japanese": "こおりのジュエル",
            "korean": "얼음주얼",
            "french": "Joyau Glace",
            "german": "Eisjuwel",
            "spanish": "Gema Hielo",
            "italian": "Bijoughiaccio",
            "english": "Ice Gem"
        }
    },
    {
        "id": "icememory"
    },
    {
        "id": "icicleplate",
        "names": {
            "japanese": "つららのプレート",
            "korean": "고드름플레이트",
            "french": "Plaque Glace",
            "german": "Frosttafel",
            "spanish": "Tabla Helada",
            "italian": "Lastragelo",
            "english": "Icicle Plate"
        }
    },
    {
        "id": "iciumz"
    },
    {
        "id": "icyrock",
        "names": {
            "japanese": "つめたいいわ",
            "korean": "차가운바위",
            "french": "Roche Glace",
            "german": "Eisbrocken",
            "spanish": "Roca Helada",
            "italian": "Rocciafredda",
            "english": "Icy Rock"
        }
    },
    {
        "id": "inciniumz"
    },
    {
        "id": "insectplate",
        "names": {
            "japanese": "たまむしプレート",
            "korean": "비단벌레플레이트",
            "french": "Plaque Insecte",
            "german": "Käfertafel",
            "spanish": "Tabla Bicho",
            "italian": "Lastrabaco",
            "english": "Insect Plate"
        }
    },
    {
        "id": "ironball",
        "names": {
            "japanese": "くろいてっきゅう",
            "korean": "검은철구",
            "french": "Balle Fer",
            "german": "Eisenkugel",
            "spanish": "Bola Férrea",
            "italian": "Ferropalla",
            "english": "Iron Ball"
        }
    },
    {
        "id": "ironplate",
        "names": {
            "japanese": "こうてつプレート",
            "korean": "강철플레이트",
            "french": "Plaque Fer",
            "german": "Eisentafel",
            "spanish": "Tabla Acero",
            "italian": "Lastraferro",
            "english": "Iron Plate"
        }
    },
    {
        "id": "jabocaberry",
        "names": {
            "japanese": "ジャポのみ",
            "korean": "자보열매",
            "french": "Baie Jaboca",
            "german": "Jabocabeere",
            "spanish": "Baya Jaboca",
            "italian": "Baccajaba",
            "english": "Jaboca Berry"
        }
    },
    {
        "id": "kasibberry",
        "names": {
            "japanese": "カシブのみ",
            "korean": "수불열매",
            "french": "Baie Sédra",
            "german": "Zitarzbeere",
            "spanish": "Baya Drasi",
            "italian": "Baccacitrus",
            "english": "Kasib Berry"
        }
    },
    {
        "id": "kebiaberry",
        "names": {
            "japanese": "ビアーのみ",
            "korean": "으름열매",
            "french": "Baie Kébia",
            "german": "Grarzbeere",
            "spanish": "Baya Kebia",
            "italian": "Baccakebia",
            "english": "Kebia Berry"
        }
    },
    {
        "id": "keeberry",
        "names": {
            "japanese": "アッキのみ",
            "korean": "악키열매",
            "french": "Baie Éka",
            "german": "Akibeere",
            "spanish": "Baya Biglia",
            "italian": "Baccalighia",
            "english": "Kee Berry"
        }
    },
    {
        "id": "kelpsyberry",
        "names": {
            "japanese": "ネコブのみ",
            "korean": "시마열매",
            "french": "Baie Alga",
            "german": "Setangbeere",
            "spanish": "Baya Algama",
            "italian": "Baccalga",
            "english": "Kelpsy Berry"
        }
    },
    {
        "id": "kangaskhanite",
        "names": {
            "japanese": "ガルーラナイト",
            "korean": "캥카나이트",
            "french": "Kangourexite",
            "german": "Kangamanit",
            "spanish": "Kangaskhanita",
            "italian": "Kangaskhanite",
            "english": "Kangaskhanite"
        }
    },
    {
        "id": "kingsrock",
        "names": {
            "japanese": "おうじゃのしるし",
            "korean": "왕의징표석",
            "french": "Roche Royale",
            "german": "King-Stein",
            "spanish": "Roca del Rey",
            "italian": "Roccia di Re",
            "english": "King's Rock"
        }
    },
    {
        "id": "laggingtail",
        "names": {
            "japanese": "こうこうのしっぽ",
            "korean": "느림보꼬리",
            "french": "Ralentiqueue",
            "german": "Schwerschwf.",
            "spanish": "Cola Plúmbea",
            "italian": "Rallentocoda",
            "english": "Lagging Tail"
        }
    },
    {
        "id": "lansatberry",
        "names": {
            "japanese": "サンのみ",
            "korean": "랑사열매",
            "french": "Baie Lansat",
            "german": "Lansatbeere",
            "spanish": "Baya Zonlan",
            "italian": "Baccalangsa",
            "english": "Lansat Berry"
        }
    },
    {
        "id": "latiasite"
    },
    {
        "id": "latiosite"
    },
    {
        "id": "laxincense",
        "names": {
            "japanese": "のんきのおこう",
            "korean": "무사태평향로",
            "french": "Encens Doux",
            "german": "Laxrauch",
            "spanish": "Incienso Suave",
            "italian": "Distraroma",
            "english": "Lax Incense"
        }
    },
    {
        "id": "leftovers",
        "names": {
            "japanese": "たべのこし",
            "korean": "먹다남은음식",
            "french": "Restes",
            "german": "Überreste",
            "spanish": "Restos",
            "italian": "Avanzi",
            "english": "Leftovers"
        }
    },
    {
        "id": "leppaberry",
        "names": {
            "japanese": "ヒメリのみ",
            "korean": "과사열매",
            "french": "Baie Mepo",
            "german": "Jonagobeere",
            "spanish": "Baya Zanama",
            "italian": "Baccamela",
            "english": "Leppa Berry"
        }
    },
    {
        "id": "levelball",
        "names": {
            "japanese": "レベルボール",
            "korean": "레벨볼",
            "french": "Niveau Ball",
            "german": "Levelball",
            "spanish": "Nivel Ball",
            "italian": "Level Ball",
            "english": "Level Ball"
        }
    },
    {
        "id": "liechiberry",
        "names": {
            "japanese": "チイラのみ",
            "korean": "치리열매",
            "french": "Baie Lichii",
            "german": "Lydzibeere",
            "spanish": "Baya Lichi",
            "italian": "Baccalici",
            "english": "Liechi Berry"
        }
    },
    {
        "id": "lifeorb",
        "names": {
            "japanese": "いのちのたま",
            "korean": "생명의구슬",
            "french": "Orbe Vie",
            "german": "Leben-Orb",
            "spanish": "Vidasfera",
            "italian": "Assorbisfera",
            "english": "Life Orb"
        }
    },
    {
        "id": "lightball",
        "names": {
            "japanese": "でんきだま",
            "korean": "전기구슬",
            "french": "Balle Lumière",
            "german": "Kugelblitz",
            "spanish": "Bolaluminosa",
            "italian": "Elettropalla",
            "english": "Light Ball"
        }
    },
    {
        "id": "lightclay",
        "names": {
            "japanese": "ひかりのねんど",
            "korean": "빛의점토",
            "french": "Lumargile",
            "german": "Lichtlehm",
            "spanish": "Refleluz",
            "italian": "Creta Luce",
            "english": "Light Clay"
        }
    },
    {
        "id": "lopunnite"
    },
    {
        "id": "loveball",
        "names": {
            "japanese": "ラブラブボール",
            "korean": "러브러브볼",
            "french": "Love Ball",
            "german": "Sympaball",
            "spanish": "Amor Ball",
            "italian": "Love Ball",
            "english": "Love Ball"
        }
    },
    {
        "id": "lucarionite",
        "names": {
            "japanese": "ルカリオナイト",
            "korean": "루카리오나이트",
            "french": "Lucarite",
            "german": "Lucarionit",
            "spanish": "Lucarita",
            "italian": "Lucarite",
            "english": "Lucarionite"
        }
    },
    {
        "id": "luckypunch",
        "names": {
            "japanese": "ラッキーパンチ",
            "korean": "럭키펀치",
            "french": "Poing Chance",
            "german": "Lucky Punch",
            "spanish": "Puño Suerte",
            "italian": "Fortunpugno",
            "english": "Lucky Punch"
        }
    },
    {
        "id": "lumberry",
        "names": {
            "japanese": "ラムのみ",
            "korean": "리샘열매",
            "french": "Baie Prine",
            "german": "Prunusbeere",
            "spanish": "Baya Ziuela",
            "italian": "Baccaprugna",
            "english": "Lum Berry"
        }
    },
    {
        "id": "luminousmoss",
        "names": {
            "japanese": "ひかりごけ",
            "korean": "빛이끼",
            "french": "Lichen Lumineux",
            "german": "Leuchtmoos",
            "spanish": "Musgo Brillante",
            "italian": "Muschioluce",
            "english": "Luminous Moss"
        }
    },
    {
        "id": "lureball",
        "names": {
            "japanese": "ルアーボール",
            "korean": "루어볼",
            "french": "Appât Ball",
            "german": "Köderball",
            "spanish": "Cebo Ball",
            "italian": "Esca Ball",
            "english": "Lure Ball"
        }
    },
    {
        "id": "lustrousorb",
        "names": {
            "japanese": "しらたま",
            "korean": "백옥",
            "french": "Orbe Perlé",
            "german": "Weiß-Orb",
            "spanish": "Lustresfera",
            "italian": "Splendisfera",
            "english": "Lustrous Orb"
        }
    },
    {
        "id": "luxuryball",
        "names": {
            "japanese": "ゴージャスボール",
            "korean": "럭셔리볼",
            "french": "Luxe Ball",
            "german": "Luxusball",
            "spanish": "Lujo Ball",
            "italian": "Chic Ball",
            "english": "Luxury Ball"
        }
    },
    {
        "id": "machobrace",
        "names": {
            "japanese": "きょうせいギプス",
            "korean": "교정깁스",
            "french": "Bracelet Macho",
            "german": "Machoband",
            "spanish": "Brazal Firme",
            "italian": "Crescicappa",
            "english": "Macho Brace"
        }
    },
    {
        "id": "magnet",
        "names": {
            "japanese": "じしゃく",
            "korean": "자석",
            "french": "Aimant",
            "german": "Magnet",
            "spanish": "Imán",
            "italian": "Calamita",
            "english": "Magnet"
        }
    },
    {
        "id": "magoberry",
        "names": {
            "japanese": "マゴのみ",
            "korean": "마고열매",
            "french": "Baie Mago",
            "german": "Magobeere",
            "spanish": "Baya Ango",
            "italian": "Baccamango",
            "english": "Mago Berry"
        }
    },
    {
        "id": "magostberry",
        "names": {
            "japanese": "ゴスのみ",
            "korean": "고스티열매",
            "french": "Baie Mangou",
            "german": "Magostbeere",
            "spanish": "Baya Aostan",
            "italian": "Baccagostan",
            "english": "Magost Berry"
        }
    },
    {
        "id": "mail"
    },
    {
        "id": "manectite",
        "names": {
            "japanese": "ライボルトナイト",
            "korean": "썬더볼트나이트",
            "french": "Élecsprintite",
            "german": "Voltensonit",
            "spanish": "Manectricita",
            "italian": "Manectricite",
            "english": "Manectite"
        }
    },
    {
        "id": "marangaberry",
        "names": {
            "japanese": "タラプのみ",
            "korean": "타라프열매",
            "french": "Baie Rangma",
            "german": "Tarabeere",
            "spanish": "Baya Maranga",
            "italian": "Baccapane",
            "english": "Maranga Berry"
        }
    },
    {
        "id": "marshadiumz"
    },
    {
        "id": "masterball",
        "names": {
            "japanese": "マスターボール",
            "korean": "마스터볼",
            "french": "Master Ball",
            "german": "Meisterball",
            "spanish": "Master Ball",
            "italian": "Master Ball",
            "english": "Master Ball"
        }
    },
    {
        "id": "mawilite",
        "names": {
            "japanese": "クチートナイト",
            "korean": "입치트나이트",
            "french": "Mysdibulite",
            "german": "Flunkifernit",
            "spanish": "Mawilita",
            "italian": "Mawilite",
            "english": "Mawilite"
        }
    },
    {
        "id": "meadowplate",
        "names": {
            "japanese": "みどりのプレート",
            "korean": "초록플레이트",
            "french": "Plaque Herbe",
            "german": "Wiesentafel",
            "spanish": "Tabla Pradal",
            "italian": "Lastraprato",
            "english": "Meadow Plate"
        }
    },
    {
        "id": "medichamite",
        "names": {
            "japanese": "チャーレムナイト",
            "korean": "요가램나이트",
            "french": "Charminite",
            "german": "Meditalisnit",
            "spanish": "Medichamita",
            "italian": "Medichamite",
            "english": "Medichamite"
        }
    },
    {
        "id": "mentalherb",
        "names": {
            "japanese": "メンタルハーブ",
            "korean": "멘탈허브",
            "french": "Herbe Mental",
            "german": "Mentalkraut",
            "spanish": "Hierba Mental",
            "italian": "Mentalerba",
            "english": "Mental Herb"
        }
    },
    {
        "id": "metagrossite"
    },
    {
        "id": "metalcoat",
        "names": {
            "japanese": "メタルコート",
            "korean": "금속코트",
            "french": "Peau Métal",
            "german": "Metallmantel",
            "spanish": "Revest. Metálico",
            "italian": "Metalcoperta",
            "english": "Metal Coat"
        }
    },
    {
        "id": "metalpowder",
        "names": {
            "japanese": "メタルパウダー",
            "korean": "금속파우더",
            "french": "Poudre Métal",
            "german": "Metallstaub",
            "spanish": "Polvo Metálico",
            "italian": "Metalpolvere",
            "english": "Metal Powder"
        }
    },
    {
        "id": "metronome",
        "names": {
            "japanese": "メトロノーム",
            "korean": "메트로놈",
            "french": "Métronome",
            "german": "Metronom",
            "spanish": "Metrónomo",
            "italian": "Plessimetro",
            "english": "Metronome"
        }
    },
    {
        "id": "mewniumz"
    },
    {
        "id": "mewtwonitex",
        "names": {
            "japanese": "ミュウツナイトＸ",
            "korean": "뮤츠나이트X",
            "french": "Mewtwoïte X",
            "german": "Mewtunit X",
            "spanish": "Mewtwoita X",
            "italian": "Mewtwoite X",
            "english": "Mewtwonite X"
        }
    },
    {
        "id": "mewtwonitey",
        "names": {
            "japanese": "ミュウツナイトＹ",
            "korean": "뮤츠나이트Y",
            "french": "Mewtwoïte Y",
            "german": "Mewtunit Y",
            "spanish": "Mewtwoita Y",
            "italian": "Mewtwoite Y",
            "english": "Mewtwonite Y"
        }
    },
    {
        "id": "micleberry",
        "names": {
            "japanese": "ミクルのみ",
            "korean": "미클열매",
            "french": "Baie Micle",
            "german": "Wunfrubeere",
            "spanish": "Baya Lagro",
            "italian": "Baccaracolo",
            "english": "Micle Berry"
        }
    },
    {
        "id": "mindplate",
        "names": {
            "japanese": "ふしぎのプレート",
            "korean": "이상한플레이트",
            "french": "Plaque Esprit",
            "german": "Hirntafel",
            "spanish": "Tabla Mental",
            "italian": "Lastramente",
            "english": "Mind Plate"
        }
    },
    {
        "id": "miracleseed",
        "names": {
            "japanese": "きせきのタネ",
            "korean": "기적의씨",
            "french": "Grain Miracle",
            "german": "Wundersaat",
            "spanish": "Semilla Milagro",
            "italian": "Miracolseme",
            "english": "Miracle Seed"
        }
    },
    {
        "id": "mistyseed"
    },
    {
        "id": "moonball",
        "names": {
            "japanese": "ムーンボール",
            "korean": "문볼",
            "french": "Lune Ball",
            "german": "Mondball",
            "spanish": "Luna Ball",
            "italian": "Luna Ball",
            "english": "Moon Ball"
        }
    },
    {
        "id": "muscleband",
        "names": {
            "japanese": "ちからのハチマキ",
            "korean": "힘의머리띠",
            "french": "Bandeau Muscle",
            "german": "Muskelband",
            "spanish": "Cinta Fuerte",
            "italian": "Muscolbanda",
            "english": "Muscle Band"
        }
    },
    {
        "id": "mysticwater",
        "names": {
            "japanese": "しんぴのしずく",
            "korean": "신비의물방울",
            "french": "Eau Mystique",
            "german": "Zauberwasser",
            "spanish": "Agua Mística",
            "italian": "Acqua Magica",
            "english": "Mystic Water"
        }
    },
    {
        "id": "nanabberry",
        "names": {
            "japanese": "ナナのみ",
            "korean": "나나열매",
            "french": "Baie Nanab",
            "german": "Nanabbeere",
            "spanish": "Baya Latano",
            "italian": "Baccabana",
            "english": "Nanab Berry"
        }
    },
    {
        "id": "nestball",
        "names": {
            "japanese": "ネストボール",
            "korean": "네스트볼",
            "french": "Faiblo Ball",
            "german": "Nestball",
            "spanish": "Nido Ball",
            "italian": "Minor Ball",
            "english": "Nest Ball"
        }
    },
    {
        "id": "netball",
        "names": {
            "japanese": "ネットボール",
            "korean": "넷트볼",
            "french": "Filet Ball",
            "german": "Netzball",
            "spanish": "Malla Ball",
            "italian": "Rete Ball",
            "english": "Net Ball"
        }
    },
    {
        "id": "nevermeltice",
        "names": {
            "japanese": "とけないこおり",
            "korean": "녹지않는얼음",
            "french": "Glace Éternelle",
            "german": "Ewiges Eis",
            "spanish": "Antiderretir",
            "italian": "Gelomai",
            "english": "Never-Melt Ice"
        }
    },
    {
        "id": "nomelberry",
        "names": {
            "japanese": "ノメルのみ",
            "korean": "노멜열매",
            "french": "Baie Tronci",
            "german": "Tronzibeere",
            "spanish": "Baya Monli",
            "italian": "Baccalemon",
            "english": "Nomel Berry"
        }
    },
    {
        "id": "normalgem",
        "names": {
            "japanese": "ノーマルジュエル",
            "korean": "노말주얼",
            "french": "Joyau Normal",
            "german": "Normaljuwel",
            "spanish": "Gema Normal",
            "italian": "Bijounormale",
            "english": "Normal Gem"
        }
    },
    {
        "id": "normaliumz"
    },
    {
        "id": "occaberry",
        "names": {
            "japanese": "オッカのみ",
            "korean": "오카열매",
            "french": "Baie Chocco",
            "german": "Koakobeere",
            "spanish": "Baya Caoca",
            "italian": "Baccacao",
            "english": "Occa Berry"
        }
    },
    {
        "id": "oddincense",
        "names": {
            "japanese": "あやしいおこう",
            "korean": "괴상한향로",
            "french": "Encens Bizarre",
            "german": "Schrägrauch",
            "spanish": "Incienso Raro",
            "italian": "Bizzoaroma",
            "english": "Odd Incense"
        }
    },
    {
        "id": "oldamber",
        "names": {
            "japanese": "ひみつのコハク",
            "korean": "비밀의호박",
            "french": "Vieil Ambre",
            "german": "Altbernstein",
            "spanish": "Ámbar Viejo",
            "italian": "Ambra Antica",
            "english": "Old Amber"
        }
    },
    {
        "id": "oranberry",
        "names": {
            "japanese": "オレンのみ",
            "korean": "오랭열매",
            "french": "Baie Oran",
            "german": "Sinelbeere",
            "spanish": "Baya Aranja",
            "italian": "Baccarancia",
            "english": "Oran Berry"
        }
    },
    {
        "id": "pamtreberry",
        "names": {
            "japanese": "シーヤのみ",
            "korean": "자야열매",
            "french": "Baie Palma",
            "german": "Pallmbeere",
            "spanish": "Baya Plama",
            "italian": "Baccapalma",
            "english": "Pamtre Berry"
        }
    },
    {
        "id": "parkball",
        "names": {
            "japanese": "パークボール",
            "korean": "파크볼",
            "french": "Parc Ball",
            "german": "Parkball",
            "spanish": "Parque Ball",
            "italian": "Parco Ball",
            "english": "Park Ball"
        }
    },
    {
        "id": "passhoberry",
        "names": {
            "japanese": "イトケのみ",
            "korean": "꼬시개열매",
            "french": "Baie Pocpoc",
            "german": "Foepasbeere",
            "spanish": "Baya Pasio",
            "italian": "Baccapasflo",
            "english": "Passho Berry"
        }
    },
    {
        "id": "payapaberry",
        "names": {
            "japanese": "ウタンのみ",
            "korean": "야파열매",
            "french": "Baie Yapap",
            "german": "Pyapabeere",
            "spanish": "Baya Payapa",
            "italian": "Baccapayapa",
            "english": "Payapa Berry"
        }
    },
    {
        "id": "pechaberry",
        "names": {
            "japanese": "モモンのみ",
            "korean": "복슝열매",
            "french": "Baie Pêcha",
            "german": "Pirsifbeere",
            "spanish": "Baya Meloc",
            "italian": "Baccapesca",
            "english": "Pecha Berry"
        }
    },
    {
        "id": "persimberry",
        "names": {
            "japanese": "キーのみ",
            "korean": "시몬열매",
            "french": "Baie Kika",
            "german": "Persimbeere",
            "spanish": "Baya Caquic",
            "italian": "Baccaki",
            "english": "Persim Berry"
        }
    },
    {
        "id": "petayaberry",
        "names": {
            "japanese": "ヤタピのみ",
            "korean": "야타비열매",
            "french": "Baie Pitaye",
            "german": "Tahaybeere",
            "spanish": "Baya Yapati",
            "italian": "Baccapitaya",
            "english": "Petaya Berry"
        }
    },
    {
        "id": "pidgeotite"
    },
    {
        "id": "pikaniumz"
    },
    {
        "id": "pikashuniumz"
    },
    {
        "id": "pinapberry",
        "names": {
            "japanese": "パイルのみ",
            "korean": "파인열매",
            "french": "Baie Nanana",
            "german": "Sananabeere",
            "spanish": "Baya Pinia",
            "italian": "Baccananas",
            "english": "Pinap Berry"
        }
    },
    {
        "id": "pinsirite",
        "names": {
            "japanese": "カイロスナイト",
            "korean": "쁘사이저나이트",
            "french": "Scarabruite",
            "german": "Pinsirnit",
            "spanish": "Pinsirita",
            "italian": "Pinsirite",
            "english": "Pinsirite"
        }
    },
    {
        "id": "pixieplate",
        "names": {
            "japanese": "せいれいプレート",
            "korean": "정령플레이트",
            "french": "Plaque Pixie",
            "german": "Feentafel",
            "spanish": "Tabla Duende",
            "italian": "Lastraspiritello",
            "english": "Pixie Plate"
        }
    },
    {
        "id": "plumefossil",
        "names": {
            "japanese": "はねのカセキ",
            "korean": "날개화석",
            "french": "Fossile Plume",
            "german": "Federfossil",
            "spanish": "Fósil Pluma",
            "italian": "Fossilpiuma",
            "english": "Plume Fossil"
        }
    },
    {
        "id": "poisonbarb",
        "names": {
            "japanese": "どくバリ",
            "korean": "독바늘",
            "french": "Pic Venin",
            "german": "Giftstich",
            "spanish": "Flecha Venenosa",
            "italian": "Velenaculeo",
            "english": "Poison Barb"
        }
    },
    {
        "id": "poisongem",
        "names": {
            "japanese": "どくのジュエル",
            "korean": "독주얼",
            "french": "Joyau Poison",
            "german": "Giftjuwel",
            "spanish": "Gema Veneno",
            "italian": "Bijouveleno",
            "english": "Poison Gem"
        }
    },
    {
        "id": "poisonmemory"
    },
    {
        "id": "poisoniumz"
    },
    {
        "id": "pokeball",
        "names": {
            "japanese": "モンスターボール",
            "korean": "몬스터볼",
            "french": "Poké Ball",
            "german": "Pokéball",
            "spanish": "Poké Ball",
            "italian": "Poké Ball",
            "english": "Poké Ball"
        }
    },
    {
        "id": "pomegberry",
        "names": {
            "japanese": "ザロクのみ",
            "korean": "유석열매",
            "french": "Baie Grena",
            "german": "Granabeere",
            "spanish": "Baya Grana",
            "italian": "Baccagrana",
            "english": "Pomeg Berry"
        }
    },
    {
        "id": "poweranklet",
        "names": {
            "japanese": "パワーアンクル",
            "korean": "파워앵클릿",
            "french": "Chaîne Pouvoir",
            "german": "Machtkette",
            "spanish": "Franja Recia",
            "italian": "Vigorgliera",
            "english": "Power Anklet"
        }
    },
    {
        "id": "powerband",
        "names": {
            "japanese": "パワーバンド",
            "korean": "파워밴드",
            "french": "Bandeau Pouvoir",
            "german": "Machtband",
            "spanish": "Banda Recia",
            "italian": "Vigorbanda",
            "english": "Power Band"
        }
    },
    {
        "id": "powerbelt",
        "names": {
            "japanese": "パワーベルト",
            "korean": "파워벨트",
            "french": "Ceinture Pouvoir",
            "german": "Machtgurt",
            "spanish": "Cinto Recio",
            "italian": "Vigorfascia",
            "english": "Power Belt"
        }
    },
    {
        "id": "powerbracer",
        "names": {
            "japanese": "パワーリスト",
            "korean": "파워리스트",
            "french": "Poignée Pouvoir",
            "german": "Machtreif",
            "spanish": "Brazal Recio",
            "italian": "Vigorcerchio",
            "english": "Power Bracer"
        }
    },
    {
        "id": "powerherb",
        "names": {
            "japanese": "パワフルハーブ",
            "korean": "파워풀허브",
            "french": "Herbe Pouvoir",
            "german": "Energiekraut",
            "spanish": "Hierba Única",
            "italian": "Vigorerba",
            "english": "Power Herb"
        }
    },
    {
        "id": "powerlens",
        "names": {
            "japanese": "パワーレンズ",
            "korean": "파워렌즈",
            "french": "Lentille Pouvoir",
            "german": "Machtlinse",
            "spanish": "Lente Recia",
            "italian": "Vigorlente",
            "english": "Power Lens"
        }
    },
    {
        "id": "powerweight",
        "names": {
            "japanese": "パワーウエイト",
            "korean": "파워웨이트",
            "french": "Poids Pouvoir",
            "german": "Machtgewicht",
            "spanish": "Pesa Recia",
            "italian": "Vigorpeso",
            "english": "Power Weight"
        }
    },
    {
        "id": "premierball",
        "names": {
            "japanese": "プレミアボール",
            "korean": "프레미어볼",
            "french": "Honor Ball",
            "german": "Premierball",
            "spanish": "Honor Ball",
            "italian": "Premier Ball",
            "english": "Premier Ball"
        }
    },
    {
        "id": "primariumz"
    },
    {
        "id": "protectivepads"
    },
    {
        "id": "psychicgem",
        "names": {
            "japanese": "エスパージュエル",
            "korean": "에스퍼주얼",
            "french": "Joyau Psy",
            "german": "Psychojuwel",
            "spanish": "Gema Psíquica",
            "italian": "Bijoupsico",
            "english": "Psychic Gem"
        }
    },
    {
        "id": "psychicmemory"
    },
    {
        "id": "psychicseed"
    },
    {
        "id": "psychiumz"
    },
    {
        "id": "qualotberry",
        "names": {
            "japanese": "タポルのみ",
            "korean": "파비열매",
            "french": "Baie Qualot",
            "german": "Qualotbeere",
            "spanish": "Baya Ispero",
            "italian": "Baccaloquat",
            "english": "Qualot Berry"
        }
    },
    {
        "id": "quickball",
        "names": {
            "japanese": "クイックボール",
            "korean": "퀵볼",
            "french": "Rapide Ball",
            "german": "Flottball",
            "spanish": "Veloz Ball",
            "italian": "Velox Ball",
            "english": "Quick Ball"
        }
    },
    {
        "id": "quickclaw",
        "names": {
            "japanese": "せんせいのツメ",
            "korean": "선제공격손톱",
            "french": "Vive Griffe",
            "german": "Flinkklaue",
            "spanish": "Garra Rápida",
            "italian": "Rapidartigli",
            "english": "Quick Claw"
        }
    },
    {
        "id": "quickpowder",
        "names": {
            "japanese": "スピードパウダー",
            "korean": "스피드파우더",
            "french": "Poudre Vite",
            "german": "Flottstaub",
            "spanish": "Polvo Veloz",
            "italian": "Velopolvere",
            "english": "Quick Powder"
        }
    },
    {
        "id": "rabutaberry",
        "names": {
            "japanese": "ラブタのみ",
            "korean": "라부탐열매",
            "french": "Baie Rabuta",
            "german": "Rabutabeere",
            "spanish": "Baya Rautan",
            "italian": "Baccambutan",
            "english": "Rabuta Berry"
        }
    },
    {
        "id": "rarebone",
        "names": {
            "japanese": "きちょうなホネ",
            "korean": "귀중한뼈",
            "french": "Os Rare",
            "german": "Steinknochen",
            "spanish": "Hueso Raro",
            "italian": "Osso Raro",
            "english": "Rare Bone"
        }
    },
    {
        "id": "rawstberry",
        "names": {
            "japanese": "チーゴのみ",
            "korean": "복분열매",
            "french": "Baie Fraive",
            "german": "Fragiabeere",
            "spanish": "Baya Safre",
            "italian": "Baccafrago",
            "english": "Rawst Berry"
        }
    },
    {
        "id": "razorclaw",
        "names": {
            "japanese": "するどいツメ",
            "korean": "예리한손톱",
            "french": "Griffe Rasoir",
            "german": "Scharfklaue",
            "spanish": "Garra Afilada",
            "italian": "Affilartigli",
            "english": "Razor Claw"
        }
    },
    {
        "id": "razorfang",
        "names": {
            "japanese": "するどいキバ",
            "korean": "예리한이빨",
            "french": "Croc Rasoir",
            "german": "Scharfzahn",
            "spanish": "Colmillo Agudo",
            "italian": "Affilodente",
            "english": "Razor Fang"
        }
    },
    {
        "id": "razzberry",
        "names": {
            "japanese": "ズリのみ",
            "korean": "라즈열매",
            "french": "Baie Framby",
            "german": "Himmihbeere",
            "spanish": "Baya Frambu",
            "italian": "Baccalampon",
            "english": "Razz Berry"
        }
    },
    {
        "id": "redcard",
        "names": {
            "japanese": "レッドカード",
            "korean": "레드카드",
            "french": "Carton Rouge",
            "german": "Rote Karte",
            "spanish": "Tarjeta Roja",
            "italian": "Cartelrosso",
            "english": "Red Card"
        }
    },
    {
        "id": "redorb",
        "names": {
            "japanese": "べにいろのたま",
            "korean": "주홍구슬",
            "french": "Orbe Rouge",
            "german": "Rote Kugel",
            "spanish": "Esfera Roja",
            "italian": "Sfera Rossa",
            "english": "Red Orb"
        }
    },
    {
        "id": "repeatball",
        "names": {
            "japanese": "リピートボール",
            "korean": "리피드볼",
            "french": "Bis Ball",
            "german": "Wiederball",
            "spanish": "Acopio Ball",
            "italian": "Bis Ball",
            "english": "Repeat Ball"
        }
    },
    {
        "id": "rindoberry",
        "names": {
            "japanese": "リンドのみ",
            "korean": "린드열매",
            "french": "Baie Ratam",
            "german": "Grindobeere",
            "spanish": "Baya Tamar",
            "italian": "Baccarindo",
            "english": "Rindo Berry"
        }
    },
    {
        "id": "ringtarget",
        "names": {
            "japanese": "ねらいのまと",
            "korean": "겨냥표적",
            "french": "Point de Mire",
            "german": "Zielscheibe",
            "spanish": "Blanco",
            "italian": "Facilsaglio",
            "english": "Ring Target"
        }
    },
    {
        "id": "rockgem",
        "names": {
            "japanese": "いわのジュエル",
            "korean": "바위주얼",
            "french": "Joyau Roche",
            "german": "Gesteinjuwel",
            "spanish": "Gema Roca",
            "italian": "Bijouroccia",
            "english": "Rock Gem"
        }
    },
    {
        "id": "rockincense",
        "names": {
            "japanese": "がんせきおこう",
            "korean": "암석향로",
            "french": "Encens Roc",
            "german": "Steinrauch",
            "spanish": "Incienso Roca",
            "italian": "Roccioaroma",
            "english": "Rock Incense"
        }
    },
    {
        "id": "rockmemory"
    },
    {
        "id": "rockiumz"
    },
    {
        "id": "rockyhelmet",
        "names": {
            "japanese": "ゴツゴツメット",
            "korean": "울퉁불퉁멧",
            "french": "Casque Brut",
            "german": "Beulenhelm",
            "spanish": "Casco Dentado",
            "italian": "Bitorzolelmo",
            "english": "Rocky Helmet"
        }
    },
    {
        "id": "rootfossil",
        "names": {
            "japanese": "ねっこのカセキ",
            "korean": "뿌리화석",
            "french": "Fossile Racine",
            "german": "Wurzelfossil",
            "spanish": "Fósil Raíz",
            "italian": "Radifossile",
            "english": "Root Fossil"
        }
    },
    {
        "id": "roseincense",
        "names": {
            "japanese": "おはなのおこう",
            "korean": "꽃향로",
            "french": "Encens Fleur",
            "german": "Rosenrauch",
            "spanish": "Incienso Floral",
            "italian": "Rosaroma",
            "english": "Rose Incense"
        }
    },
    {
        "id": "roseliberry",
        "names": {
            "japanese": "ロゼルのみ",
            "korean": "로셀열매",
            "french": "Baie Selro",
            "german": "Hibisbeere",
            "spanish": "Baya Hibis",
            "italian": "Baccarcadè",
            "english": "Roseli Berry"
        }
    },
    {
        "id": "rowapberry",
        "names": {
            "japanese": "レンブのみ",
            "korean": "애터열매",
            "french": "Baie Pommo",
            "german": "Roselbeere",
            "spanish": "Baya Magua",
            "italian": "Baccaroam",
            "english": "Rowap Berry"
        }
    },
    {
        "id": "sablenite"
    },
    {
        "id": "safariball",
        "names": {
            "japanese": "サファリボール",
            "korean": "사파리볼",
            "french": "Safari Ball",
            "german": "Safariball",
            "spanish": "Safari Ball",
            "italian": "Safari Ball",
            "english": "Safari Ball"
        }
    },
    {
        "id": "safetygoggles",
        "names": {
            "japanese": "ぼうじんゴーグル",
            "korean": "방진고글",
            "french": "Lunettes Filtre",
            "german": "Schutzbrille",
            "spanish": "Gafa Protectora",
            "italian": "Visierantisabbia",
            "english": "Safety Goggles"
        }
    },
    {
        "id": "salacberry",
        "names": {
            "japanese": "カムラのみ",
            "korean": "캄라열매",
            "french": "Baie Sailak",
            "german": "Salkabeere",
            "spanish": "Baya Aslac",
            "italian": "Baccasalak",
            "english": "Salac Berry"
        }
    },
    {
        "id": "salamencite"
    },
    {
        "id": "sceptilite"
    },
    {
        "id": "scizorite",
        "names": {
            "japanese": "ハッサムナイト",
            "korean": "핫삼나이트",
            "french": "Cizayoxite",
            "german": "Scheroxnit",
            "spanish": "Scizorita",
            "italian": "Scizorite",
            "english": "Scizorite"
        }
    },
    {
        "id": "scopelens",
        "names": {
            "japanese": "ピントレンズ",
            "korean": "초점렌즈",
            "french": "Lentilscope",
            "german": "Scope-Linse",
            "spanish": "Periscopio",
            "italian": "Mirino",
            "english": "Scope Lens"
        }
    },
    {
        "id": "seaincense",
        "names": {
            "japanese": "うしおのおこう",
            "korean": "바닷물향로",
            "french": "Encens Mer",
            "german": "Seerauch",
            "spanish": "Incienso Marino",
            "italian": "Marearoma",
            "english": "Sea Incense"
        }
    },
    {
        "id": "sharpbeak",
        "names": {
            "japanese": "するどいくちばし",
            "korean": "예리한부리",
            "french": "Bec Pointu",
            "german": "Hackattack",
            "spanish": "Pico Afilado",
            "italian": "Beccaffilato",
            "english": "Sharp Beak"
        }
    },
    {
        "id": "sharpedonite"
    },
    {
        "id": "shedshell",
        "names": {
            "japanese": "きれいなぬけがら",
            "korean": "아름다운허물",
            "french": "Carapace Mue",
            "german": "Wechselhülle",
            "spanish": "Muda Concha",
            "italian": "Disfoguscio",
            "english": "Shed Shell"
        }
    },
    {
        "id": "shellbell",
        "names": {
            "japanese": "かいがらのすず",
            "korean": "조개껍질방울",
            "french": "Grelot Coque",
            "german": "Seegesang",
            "spanish": "Campana Concha",
            "italian": "Conchinella",
            "english": "Shell Bell"
        }
    },
    {
        "id": "shockdrive",
        "names": {
            "japanese": "イナズマカセット",
            "korean": "번개카세트",
            "french": "Module Choc",
            "german": "Blitzmodul",
            "spanish": "FulgoROM",
            "italian": "Voltmodulo",
            "english": "Shock Drive"
        }
    },
    {
        "id": "shucaberry",
        "names": {
            "japanese": "シュカのみ",
            "korean": "슈캐열매",
            "french": "Baie Jouca",
            "german": "Schukebeere",
            "spanish": "Baya Acardo",
            "italian": "Baccanaca",
            "english": "Shuca Berry"
        }
    },
    {
        "id": "silkscarf",
        "names": {
            "japanese": "シルクのスカーフ",
            "korean": "실크스카프",
            "french": "Mouchoir Soie",
            "german": "Seidenschal",
            "spanish": "Pañuelo Seda",
            "italian": "Sciarpa Seta",
            "english": "Silk Scarf"
        }
    },
    {
        "id": "silverpowder",
        "names": {
            "japanese": "ぎんのこな",
            "korean": "은빛가루",
            "french": "Poudre Argentée",
            "german": "Silberstaub",
            "spanish": "Polvo Plata",
            "italian": "Argenpolvere",
            "english": "Silver Powder"
        }
    },
    {
        "id": "sitrusberry",
        "names": {
            "japanese": "オボンのみ",
            "korean": "자뭉열매",
            "french": "Baie Sitrus",
            "german": "Tsitrubeere",
            "spanish": "Baya Zidra",
            "italian": "Baccacedro",
            "english": "Sitrus Berry"
        }
    },
    {
        "id": "skullfossil",
        "names": {
            "japanese": "ずがいのカセキ",
            "korean": "두개의화석",
            "french": "Fossile Crâne",
            "german": "Kopffossil",
            "spanish": "Fósil Cráneo",
            "italian": "Fossilcranio",
            "english": "Skull Fossil"
        }
    },
    {
        "id": "skyplate",
        "names": {
            "japanese": "あおぞらプレート",
            "korean": "푸른하늘플레이트",
            "french": "Plaque Ciel",
            "german": "Wolkentafel",
            "spanish": "Tabla Cielo",
            "italian": "Lastracielo",
            "english": "Sky Plate"
        }
    },
    {
        "id": "slowbronite"
    },
    {
        "id": "smoothrock",
        "names": {
            "japanese": "さらさらいわ",
            "korean": "보송보송바위",
            "french": "Roche Lisse",
            "german": "Glattbrocken",
            "spanish": "Roca Suave",
            "italian": "Roccialiscia",
            "english": "Smooth Rock"
        }
    },
    {
        "id": "snorliumz"
    },
    {
        "id": "snowball",
        "names": {
            "japanese": "ゆきだま",
            "korean": "눈덩이",
            "french": "Boule de Neige",
            "german": "Schneeball",
            "spanish": "Bola de Nieve",
            "italian": "Palla di neve",
            "english": "Snowball"
        }
    },
    {
        "id": "softsand",
        "names": {
            "japanese": "やわらかいすな",
            "korean": "부드러운모래",
            "french": "Sable Doux",
            "german": "Pudersand",
            "spanish": "Arena Fina",
            "italian": "Sabbia Soffice",
            "english": "Soft Sand"
        }
    },
    {
        "id": "souldew",
        "names": {
            "japanese": "こころのしずく",
            "korean": "마음의물방울",
            "french": "Rosée Âme",
            "german": "Seelentau",
            "spanish": "Rocío Bondad",
            "italian": "Cuorugiada",
            "english": "Soul Dew"
        }
    },
    {
        "id": "spelltag",
        "names": {
            "japanese": "のろいのおふだ",
            "korean": "저주의부적",
            "french": "Rune Sort",
            "german": "Bannsticker",
            "spanish": "Hechizo",
            "italian": "Spettrotarga",
            "english": "Spell Tag"
        }
    },
    {
        "id": "spelonberry",
        "names": {
            "japanese": "ノワキのみ",
            "korean": "메호키열매",
            "french": "Baie Kiwan",
            "german": "Kiwanbeere",
            "spanish": "Baya Wikano",
            "italian": "Baccamelos",
            "english": "Spelon Berry"
        }
    },
    {
        "id": "splashplate",
        "names": {
            "japanese": "しずくプレート",
            "korean": "물방울플레이트",
            "french": "Plaque Hydro",
            "german": "Wassertafel",
            "spanish": "Tabla Linfa",
            "italian": "Lastraidro",
            "english": "Splash Plate"
        }
    },
    {
        "id": "spookyplate",
        "names": {
            "japanese": "もののけプレート",
            "korean": "원령플레이트",
            "french": "Plaque Fantô",
            "german": "Spuktafel",
            "spanish": "Tabla Terror",
            "italian": "Lastratetra",
            "english": "Spooky Plate"
        }
    },
    {
        "id": "sportball",
        "names": {
            "japanese": "コンペボール",
            "korean": "콤페볼",
            "french": "Compét'Ball",
            "german": "Turnierball",
            "spanish": "Competi Ball",
            "italian": "Gara Ball",
            "english": "Sport Ball"
        }
    },
    {
        "id": "starfberry",
        "names": {
            "japanese": "スターのみ",
            "korean": "스타열매",
            "french": "Baie Frista",
            "german": "Krambobeere",
            "spanish": "Baya Arabol",
            "italian": "Baccambola",
            "english": "Starf Berry"
        }
    },
    {
        "id": "steelixite"
    },
    {
        "id": "steelgem",
        "names": {
            "japanese": "はがねのジュエル",
            "korean": "강철주얼",
            "french": "Joyau Acier",
            "german": "Stahljuwel",
            "spanish": "Gema Acero",
            "italian": "Bijouacciaio",
            "english": "Steel Gem"
        }
    },
    {
        "id": "steelmemory"
    },
    {
        "id": "steeliumz"
    },
    {
        "id": "stick",
        "names": {
            "japanese": "ながねぎ",
            "korean": "대파",
            "french": "Bâton",
            "german": "Lauchstange",
            "spanish": "Palo",
            "italian": "Gambo",
            "english": "Stick"
        }
    },
    {
        "id": "stickybarb",
        "names": {
            "japanese": "くっつきバリ",
            "korean": "끈적끈적바늘",
            "french": "Piquants",
            "german": "Klettdorn",
            "spanish": "Toxiestrella",
            "italian": "Vischiopunta",
            "english": "Sticky Barb"
        }
    },
    {
        "id": "stoneplate",
        "names": {
            "japanese": "がんせきプレート",
            "korean": "암석플레이트",
            "french": "Plaque Roc",
            "german": "Steintafel",
            "spanish": "Tabla Pétrea",
            "italian": "Lastrapietra",
            "english": "Stone Plate"
        }
    },
    {
        "id": "swampertite"
    },
    {
        "id": "tamatoberry",
        "names": {
            "japanese": "マトマのみ",
            "korean": "토망열매",
            "french": "Baie Tamato",
            "german": "Tamotbeere",
            "spanish": "Baya Tamate",
            "italian": "Baccamodoro",
            "english": "Tamato Berry"
        }
    },
    {
        "id": "tangaberry",
        "names": {
            "japanese": "タンガのみ",
            "korean": "리체열매",
            "french": "Baie Panga",
            "german": "Tanigabeere",
            "spanish": "Baya Yecana",
            "italian": "Baccaitan",
            "english": "Tanga Berry"
        }
    },
    {
        "id": "tapuniumz"
    },
    {
        "id": "terrainextender"
    },
    {
        "id": "thickclub",
        "names": {
            "japanese": "ふといホネ",
            "korean": "굵은뼈",
            "french": "Masse Os",
            "german": "Kampfknochen",
            "spanish": "Hueso Grueso",
            "italian": "Ossospesso",
            "english": "Thick Club"
        }
    },
    {
        "id": "timerball",
        "names": {
            "japanese": "タイマーボール",
            "korean": "타이마볼",
            "french": "Chrono Ball",
            "german": "Timerball",
            "spanish": "Turno Ball",
            "italian": "Timer Ball",
            "english": "Timer Ball"
        }
    },
    {
        "id": "toxicorb",
        "names": {
            "japanese": "どくどくだま",
            "korean": "맹독구슬",
            "french": "Orbe Toxique",
            "german": "Toxik-Orb",
            "spanish": "Toxisfera",
            "italian": "Tossicsfera",
            "english": "Toxic Orb"
        }
    },
    {
        "id": "toxicplate",
        "names": {
            "japanese": "もうどくプレート",
            "korean": "맹독플레이트",
            "french": "Plaque Toxik",
            "german": "Gifttafel",
            "spanish": "Tabla Tóxica",
            "italian": "Lastrafiele",
            "english": "Toxic Plate"
        }
    },
    {
        "id": "twistedspoon",
        "names": {
            "japanese": "まがったスプーン",
            "korean": "휘어진스푼",
            "french": "Cuiller Tordue",
            "german": "Krummlöffel",
            "spanish": "Cuchara Torcida",
            "italian": "Cucchiaio Torto",
            "english": "Twisted Spoon"
        }
    },
    {
        "id": "tyranitarite",
        "names": {
            "japanese": "バンギラスナイト",
            "korean": "마기라스나이트",
            "french": "Tyranocivite",
            "german": "Despotarnit",
            "spanish": "Tyranitarita",
            "italian": "Tyranitarite",
            "english": "Tyranitarite"
        }
    },
    {
        "id": "ultraball",
        "names": {
            "japanese": "ハイパーボール",
            "korean": "하이퍼볼",
            "french": "Hyper Ball",
            "german": "Hyperball",
            "spanish": "Ultra Ball",
            "italian": "Ultra Ball",
            "english": "Ultra Ball"
        }
    },
    {
        "id": "venusaurite",
        "names": {
            "japanese": "フシギバナイト",
            "korean": "이상해꽃나이트",
            "french": "Florizarrite",
            "german": "Bisaflornit",
            "spanish": "Venusaurita",
            "italian": "Venusaurite",
            "english": "Venusaurite"
        }
    },
    {
        "id": "wacanberry",
        "names": {
            "japanese": "ソクノのみ",
            "korean": "초나열매",
            "french": "Baie Parma",
            "german": "Kerzalbeere",
            "spanish": "Baya Gualot",
            "italian": "Baccaparmen",
            "english": "Wacan Berry"
        }
    },
    {
        "id": "watergem",
        "names": {
            "japanese": "みずのジュエル",
            "korean": "물주얼",
            "french": "Joyau Eau",
            "german": "Wasserjuwel",
            "spanish": "Gema Agua",
            "italian": "Bijouacqua",
            "english": "Water Gem"
        }
    },
    {
        "id": "watermemory"
    },
    {
        "id": "wateriumz"
    },
    {
        "id": "watmelberry",
        "names": {
            "japanese": "カイスのみ",
            "korean": "슈박열매",
            "french": "Baie Stekpa",
            "german": "Wasmelbeere",
            "spanish": "Baya Sambia",
            "italian": "Baccacomero",
            "english": "Watmel Berry"
        }
    },
    {
        "id": "waveincense",
        "names": {
            "japanese": "さざなみのおこう",
            "korean": "잔물결향로",
            "french": "Encens Vague",
            "german": "Wellenrauch",
            "spanish": "Incienso Aqua",
            "italian": "Ondaroma",
            "english": "Wave Incense"
        }
    },
    {
        "id": "weaknesspolicy",
        "names": {
            "japanese": "じゃくてんほけん",
            "korean": "약점보험",
            "french": "Vulné-Assurance",
            "german": "Schwächenschutz",
            "spanish": "Seguro Debilidad",
            "italian": "Vulneropolizza",
            "english": "Weakness Policy"
        }
    },
    {
        "id": "wepearberry",
        "names": {
            "japanese": "セシナのみ",
            "korean": "서배열매",
            "french": "Baie Repoi",
            "german": "Nirbebeere",
            "spanish": "Baya Peragu",
            "italian": "Baccapera",
            "english": "Wepear Berry"
        }
    },
    {
        "id": "whiteherb",
        "names": {
            "japanese": "しろいハーブ",
            "korean": "하양허브",
            "french": "Herbe Blanche",
            "german": "Schlohkraut",
            "spanish": "Hierba Blanca",
            "italian": "Erbachiara",
            "english": "White Herb"
        }
    },
    {
        "id": "widelens",
        "names": {
            "japanese": "こうかくレンズ",
            "korean": "광각렌즈",
            "french": "Loupe",
            "german": "Großlinse",
            "spanish": "Lupa",
            "italian": "Grandelente",
            "english": "Wide Lens"
        }
    },
    {
        "id": "wikiberry",
        "names": {
            "japanese": "ウイのみ",
            "korean": "위키열매",
            "french": "Baie Wiki",
            "german": "Wikibeere",
            "spanish": "Baya Wiki",
            "italian": "Baccakiwi",
            "english": "Wiki Berry"
        }
    },
    {
        "id": "wiseglasses",
        "names": {
            "japanese": "ものしりメガネ",
            "korean": "박식안경",
            "french": "Lunettes Sages",
            "german": "Schlauglas",
            "spanish": "Gafas Especiales",
            "italian": "Saviocchiali",
            "english": "Wise Glasses"
        }
    },
    {
        "id": "yacheberry",
        "names": {
            "japanese": "ヤチェのみ",
            "korean": "플카열매",
            "french": "Baie Nanone",
            "german": "Kiroyabeere",
            "spanish": "Baya Rimoya",
            "italian": "Baccamoya",
            "english": "Yache Berry"
        }
    },
    {
        "id": "zapplate",
        "names": {
            "japanese": "いかずちプレート",
            "korean": "우뢰플레이트",
            "french": "Plaque Volt",
            "german": "Blitztafel",
            "spanish": "Tabla Trueno",
            "italian": "Lastrasaetta",
            "english": "Zap Plate"
        }
    },
    {
        "id": "zoomlens",
        "names": {
            "japanese": "フォーカスレンズ",
            "korean": "포커스렌즈",
            "french": "Lentille Zoom",
            "german": "Zoomlinse",
            "spanish": "Telescopio",
            "italian": "Zoomlente",
            "english": "Zoom Lens"
        }
    },
    {
        "id": "berserkgene"
    },
    {
        "id": "berry"
    },
    {
        "id": "bitterberry"
    },
    {
        "id": "burntberry"
    },
    {
        "id": "dragonscale",
        "names": {
            "japanese": "りゅうのウロコ",
            "korean": "용의비늘",
            "french": "Écaille Draco",
            "german": "Drachenhaut",
            "spanish": "Escamadragón",
            "italian": "Squama Drago",
            "english": "Dragon Scale"
        }
    },
    {
        "id": "goldberry"
    },
    {
        "id": "iceberry"
    },
    {
        "id": "mintberry"
    },
    {
        "id": "miracleberry"
    },
    {
        "id": "mysteryberry"
    },
    {
        "id": "pinkbow"
    },
    {
        "id": "polkadotbow"
    },
    {
        "id": "przcureberry"
    },
    {
        "id": "psncureberry"
    },
    {
        "id": "crucibellite"
    },
    {
        "id": "potion",
        "names": {
            "japanese": "キズぐすり",
            "korean": "상처약",
            "french": "Potion",
            "german": "Trank",
            "spanish": "Poción",
            "italian": "Pozione",
            "english": "Potion"
        }
    },
    {
        "id": "antidote",
        "names": {
            "japanese": "どくけし",
            "korean": "해독제",
            "french": "Antidote",
            "german": "Gegengift",
            "spanish": "Antídoto",
            "italian": "Antidoto",
            "english": "Antidote"
        }
    },
    {
        "id": "burnheal",
        "names": {
            "japanese": "やけどなおし",
            "korean": "화상치료제",
            "french": "Anti-Brûle",
            "german": "Feuerheiler",
            "spanish": "Antiquemar",
            "italian": "Antiscottatura",
            "english": "Burn Heal"
        }
    },
    {
        "id": "iceheal",
        "names": {
            "japanese": "こおりなおし",
            "korean": "얼음상태치료제",
            "french": "Antigel",
            "german": "Eisheiler",
            "spanish": "Antihielo",
            "italian": "Antigelo",
            "english": "Ice Heal"
        }
    },
    {
        "id": "awakening",
        "names": {
            "japanese": "ねむけざまし",
            "korean": "잠깨는약",
            "french": "Réveil",
            "german": "Aufwecker",
            "spanish": "Despertar",
            "italian": "Sveglia",
            "english": "Awakening"
        }
    },
    {
        "id": "paralyzeheal",
        "names": {
            "japanese": "まひなおし",
            "korean": "마비치료제",
            "french": "Anti-Para",
            "german": "Para-Heiler",
            "spanish": "Antiparalizador",
            "italian": "Antiparalisi",
            "english": "Paralyze Heal"
        }
    },
    {
        "id": "fullrestore",
        "names": {
            "japanese": "かいふくのくすり",
            "korean": "회복약",
            "french": "Guérison",
            "german": "Top-Genesung",
            "spanish": "Restaurar Todo",
            "italian": "Ricarica Totale",
            "english": "Full Restore"
        }
    },
    {
        "id": "maxpotion",
        "names": {
            "japanese": "まんたんのくすり",
            "korean": "풀회복약",
            "french": "Potion Max",
            "german": "Top-Trank",
            "spanish": "Poción Máxima",
            "italian": "Pozione Max",
            "english": "Max Potion"
        }
    },
    {
        "id": "hyperpotion",
        "names": {
            "japanese": "すごいキズぐすり",
            "korean": "고급상처약",
            "french": "Hyper Potion",
            "german": "Hypertrank",
            "spanish": "Hiperpoción",
            "italian": "Iperpozione",
            "english": "Hyper Potion"
        }
    },
    {
        "id": "superpotion",
        "names": {
            "japanese": "いいキズぐすり",
            "korean": "좋은상처약",
            "french": "Super Potion",
            "german": "Supertrank",
            "spanish": "Superpoción",
            "italian": "Superpozione",
            "english": "Super Potion"
        }
    },
    {
        "id": "fullheal",
        "names": {
            "japanese": "なんでもなおし",
            "korean": "만병통치제",
            "french": "Total Soin",
            "german": "Hyperheiler",
            "spanish": "Cura Total",
            "italian": "Cura Totale",
            "english": "Full Heal"
        }
    },
    {
        "id": "revive",
        "names": {
            "japanese": "げんきのかけら",
            "korean": "기력의조각",
            "french": "Rappel",
            "german": "Beleber",
            "spanish": "Revivir",
            "italian": "Revitalizzante",
            "english": "Revive"
        }
    },
    {
        "id": "maxrevive",
        "names": {
            "japanese": "げんきのかたまり",
            "korean": "기력의덩어리",
            "french": "Rappel Max",
            "german": "Top-Beleber",
            "spanish": "Revivir Máximo",
            "italian": "Revitalizz. Max",
            "english": "Max Revive"
        }
    },
    {
        "id": "freshwater",
        "names": {
            "japanese": "おいしいみず",
            "korean": "맛있는물",
            "french": "Eau Fraîche",
            "german": "Tafelwasser",
            "spanish": "Agua Fresca",
            "italian": "Acqua Fresca",
            "english": "Fresh Water"
        }
    },
    {
        "id": "sodapop",
        "names": {
            "japanese": "サイコソーダ",
            "korean": "미네랄사이다",
            "french": "Soda Cool",
            "german": "Sprudel",
            "spanish": "Refresco",
            "italian": "Gassosa",
            "english": "Soda Pop"
        }
    },
    {
        "id": "lemonade",
        "names": {
            "japanese": "ミックスオレ",
            "korean": "후르츠밀크",
            "french": "Limonade",
            "german": "Limonade",
            "spanish": "Limonada",
            "italian": "Lemonsucco",
            "english": "Lemonade"
        }
    },
    {
        "id": "moomoomilk",
        "names": {
            "japanese": "モーモーミルク",
            "korean": "튼튼밀크",
            "french": "Lait Meumeu",
            "german": "Kuhmuh-Milch",
            "spanish": "Leche Mu-mu",
            "italian": "Latte Mumu",
            "english": "Moomoo Milk"
        }
    },
    {
        "id": "energyroot",
        "names": {
            "japanese": "ちからのねっこ",
            "korean": "힘의뿌리",
            "french": "Racinénergie",
            "german": "Kraftwurzel",
            "spanish": "Raíz Energía",
            "italian": "Radicenergia",
            "english": "Energy Root"
        }
    },
    {
        "id": "healpowder",
        "names": {
            "japanese": "ばんのうごな",
            "korean": "만능가루",
            "french": "Poudre Soin",
            "german": "Heilpuder",
            "spanish": "Polvo Curación",
            "italian": "Polvocura",
            "english": "Heal Powder"
        }
    },
    {
        "id": "revivalherb",
        "names": {
            "japanese": "ふっかつそう",
            "korean": "부활초",
            "french": "Herbe Rappel",
            "german": "Vitalkraut",
            "spanish": "Hierba Revivir",
            "italian": "Vitalerba",
            "english": "Revival Herb"
        }
    },
    {
        "id": "ether",
        "names": {
            "japanese": "ピーピーエイド",
            "korean": "PP에이드",
            "french": "Huile",
            "german": "Äther",
            "spanish": "Éter",
            "italian": "Etere",
            "english": "Ether"
        }
    },
    {
        "id": "maxether",
        "names": {
            "japanese": "ピーピーリカバー",
            "korean": "PP회복",
            "french": "Huile Max",
            "german": "Top-Äther",
            "spanish": "Éter Máximo",
            "italian": "Etere Max",
            "english": "Max Ether"
        }
    },
    {
        "id": "elixir",
        "names": {
            "japanese": "ピーピーエイダー",
            "korean": "PP에이더",
            "french": "Élixir",
            "german": "Elixier",
            "spanish": "Elixir",
            "italian": "Elisir",
            "english": "Elixir"
        }
    },
    {
        "id": "maxelixir",
        "names": {
            "japanese": "ピーピーマックス",
            "korean": "PP맥스",
            "french": "Max Élixir",
            "german": "Top-Elixier",
            "spanish": "Elixir Máximo",
            "italian": "Elisir Max",
            "english": "Max Elixir"
        }
    },
    {
        "id": "lavacookie",
        "names": {
            "japanese": "フエンせんべい",
            "korean": "용암전병",
            "french": "Lava Cookie",
            "german": "Lavakeks",
            "spanish": "Galleta Lava",
            "italian": "Lavottino",
            "english": "Lava Cookie"
        }
    },
    {
        "id": "sacredash",
        "names": {
            "japanese": "せいなるはい",
            "korean": "성스러운분말",
            "french": "Cendre Sacrée",
            "german": "Zauberasche",
            "spanish": "Ceniza Sagrada",
            "italian": "Ceneremagica",
            "english": "Sacred Ash"
        }
    },
    {
        "id": "hpup",
        "names": {
            "japanese": "マックスアップ",
            "korean": "맥스업",
            "french": "PV Plus",
            "german": "KP-Plus",
            "spanish": "Más PS",
            "italian": "PS-Su",
            "english": "HP Up"
        }
    },
    {
        "id": "protein",
        "names": {
            "japanese": "タウリン",
            "korean": "타우린",
            "french": "Protéine",
            "german": "Protein",
            "spanish": "Proteína",
            "italian": "Proteina",
            "english": "Protein"
        }
    },
    {
        "id": "iron",
        "names": {
            "japanese": "ブロムヘキシン",
            "korean": "사포닌",
            "french": "Fer",
            "german": "Eisen",
            "spanish": "Hierro",
            "italian": "Ferro",
            "english": "Iron"
        }
    },
    {
        "id": "carbos",
        "names": {
            "japanese": "インドメタシン",
            "korean": "알칼로이드",
            "french": "Carbone",
            "german": "Carbon",
            "spanish": "Carburante",
            "italian": "Carburante",
            "english": "Carbos"
        }
    },
    {
        "id": "calcium",
        "names": {
            "japanese": "リゾチウム",
            "korean": "리보플라빈",
            "french": "Calcium",
            "german": "Kalzium",
            "spanish": "Calcio",
            "italian": "Calcio",
            "english": "Calcium"
        }
    },
    {
        "id": "rarecandy",
        "names": {
            "japanese": "ふしぎなアメ",
            "korean": "이상한사탕",
            "french": "Super Bonbon",
            "german": "Sonderbonbon",
            "spanish": "Caramelo Raro",
            "italian": "Caramella Rara",
            "english": "Rare Candy"
        }
    },
    {
        "id": "ppup",
        "names": {
            "japanese": "ポイントアップ",
            "korean": "포인트업",
            "french": "PP Plus",
            "german": "AP-Plus",
            "spanish": "Más PP",
            "italian": "PP-Su",
            "english": "PP Up"
        }
    },
    {
        "id": "zinc",
        "names": {
            "japanese": "キトサン",
            "korean": "키토산",
            "french": "Zinc",
            "german": "Zink",
            "spanish": "Zinc",
            "italian": "Zinco",
            "english": "Zinc"
        }
    },
    {
        "id": "ppmax",
        "names": {
            "japanese": "ポイントマックス",
            "korean": "포인트맥스",
            "french": "PP Max",
            "german": "AP-Top",
            "spanish": "PP Máximos",
            "italian": "PP-Max",
            "english": "PP Max"
        }
    },
    {
        "id": "oldgateau",
        "names": {
            "japanese": "もりのヨウカン",
            "korean": "숲의양갱",
            "french": "Vieux Gâteau",
            "german": "Spezialität",
            "spanish": "Barrita Plus",
            "italian": "Dolce Gateau",
            "english": "Old Gateau"
        }
    },
    {
        "id": "guardspec",
        "names": {
            "japanese": "エフェクトガード",
            "korean": "이펙트가드",
            "french": "Défense Spéciale",
            "german": "Megablock",
            "spanish": "Protec. Especial",
            "italian": "Superguardia",
            "english": "Guard Spec."
        }
    },
    {
        "id": "direhit",
        "names": {
            "japanese": "クリティカット",
            "korean": "크리티컬커터",
            "french": "Muscle +",
            "german": "Angriffplus",
            "spanish": "Directo",
            "italian": "Supercolpo",
            "english": "Dire Hit"
        }
    },
    {
        "id": "xattack",
        "names": {
            "japanese": "プラスパワー",
            "korean": "플러스파워",
            "french": "Attaque +",
            "german": "X-Angriff",
            "spanish": "Ataque X",
            "italian": "Attacco X",
            "english": "X Attack"
        }
    },
    {
        "id": "xdefense",
        "names": {
            "japanese": "ディフェンダー",
            "korean": "디펜드업",
            "french": "Défense +",
            "german": "X-Abwehr",
            "spanish": "Defensa X",
            "italian": "Difesa X",
            "english": "X Defense"
        }
    },
    {
        "id": "xspeed",
        "names": {
            "japanese": "スピーダー",
            "korean": "스피드업",
            "french": "Vitesse +",
            "german": "X-Tempo",
            "spanish": "Velocidad X",
            "italian": "Velocità X",
            "english": "X Speed"
        }
    },
    {
        "id": "xaccuracy",
        "names": {
            "japanese": "ヨクアタール",
            "korean": "잘-맞히기",
            "french": "Précision +",
            "german": "X-Treffer",
            "spanish": "Precisión X",
            "italian": "Precisione X",
            "english": "X Accuracy"
        }
    },
    {
        "id": "xspatk",
        "names": {
            "japanese": "スペシャルアップ",
            "korean": "스페셜업",
            "french": "Spécial +",
            "german": "X-Spezial",
            "spanish": "Especial X",
            "italian": "Special X",
            "english": "X Sp. Atk"
        }
    },
    {
        "id": "xspdef",
        "names": {
            "japanese": "スペシャルガード",
            "korean": "스페셜가드",
            "french": "Déf. Spé. +",
            "german": "X-SpezialVer",
            "spanish": "Def. Especial X",
            "italian": "Dif. Speciale X",
            "english": "X Sp. Def"
        }
    },
    {
        "id": "pokedoll",
        "names": {
            "japanese": "ピッピにんぎょう",
            "korean": "삐삐인형",
            "french": "Poképoupée",
            "german": "Poképuppe",
            "spanish": "Poké Muñeco",
            "italian": "Poké Bambola",
            "english": "Poké Doll"
        }
    },
    {
        "id": "fluffytail",
        "names": {
            "japanese": "エネコのシッポ",
            "korean": "에나비꼬리",
            "french": "Queue Skitty",
            "german": "Eneco-Rute",
            "spanish": "Cola Skitty",
            "italian": "Coda Skitty",
            "english": "Fluffy Tail"
        }
    },
    {
        "id": "blueflute",
        "names": {
            "japanese": "あおいビードロ",
            "korean": "파랑비드로",
            "french": "Flûte Bleue",
            "german": "Blaue Flöte",
            "spanish": "Flauta Azul",
            "italian": "Flauto Blu",
            "english": "Blue Flute"
        }
    },
    {
        "id": "yellowflute",
        "names": {
            "japanese": "きいろビードロ",
            "korean": "노랑비드로",
            "french": "Flûte Jaune",
            "german": "Gelbe Flöte",
            "spanish": "Flauta Amarilla",
            "italian": "Flauto Giallo",
            "english": "Yellow Flute"
        }
    },
    {
        "id": "redflute",
        "names": {
            "japanese": "あかいビードロ",
            "korean": "빨강비드로",
            "french": "Flûte Rouge",
            "german": "Rote Flöte",
            "spanish": "Flauta Roja",
            "italian": "Flauto Rosso",
            "english": "Red Flute"
        }
    },
    {
        "id": "blackflute",
        "names": {
            "japanese": "くろいビードロ",
            "korean": "검정비드로",
            "french": "Flûte Noire",
            "german": "Schw. Flöte",
            "spanish": "Flauta Negra",
            "italian": "Flauto Nero",
            "english": "Black Flute"
        }
    },
    {
        "id": "whiteflute",
        "names": {
            "japanese": "しろいビードロ",
            "korean": "하양비드로",
            "french": "Flûte Blanche",
            "german": "Weiße Flöte",
            "spanish": "Flauta Blanca",
            "italian": "Flauto Bianco",
            "english": "White Flute"
        }
    },
    {
        "id": "shoalsalt",
        "names": {
            "japanese": "あさせのしお",
            "korean": "여울소금",
            "french": "Sel Tréfonds",
            "german": "Küstensalz",
            "spanish": "Sal Cardumen",
            "italian": "Sale Ondoso",
            "english": "Shoal Salt"
        }
    },
    {
        "id": "shoalshell",
        "names": {
            "japanese": "あさせのかいがら",
            "korean": "여울조개껍질",
            "french": "CoquilleTréfonds",
            "german": "Küstenschale",
            "spanish": "Concha Cardumen",
            "italian": "Gusciondoso",
            "english": "Shoal Shell"
        }
    },
    {
        "id": "redshard",
        "names": {
            "japanese": "あかいかけら",
            "korean": "빨강조각",
            "french": "Tesson Rouge",
            "german": "Purpurstück",
            "spanish": "Parte Roja",
            "italian": "Coccio Rosso",
            "english": "Red Shard"
        }
    },
    {
        "id": "blueshard",
        "names": {
            "japanese": "あおいかけら",
            "korean": "파랑조각",
            "french": "Tesson Bleu",
            "german": "Indigostück",
            "spanish": "Parte Azul",
            "italian": "Coccio Blu",
            "english": "Blue Shard"
        }
    },
    {
        "id": "yellowshard",
        "names": {
            "japanese": "きいろいかけら",
            "korean": "노랑조각",
            "french": "Tesson Jaune",
            "german": "Gelbstück",
            "spanish": "Parte Amarilla",
            "italian": "Coccio Giallo",
            "english": "Yellow Shard"
        }
    },
    {
        "id": "greenshard",
        "names": {
            "japanese": "みどりのかけら",
            "korean": "초록조각",
            "french": "Tesson Vert",
            "german": "Grünstück",
            "spanish": "Parte Verde",
            "italian": "Coccio Verde",
            "english": "Green Shard"
        }
    },
    {
        "id": "superrepel",
        "names": {
            "japanese": "シルバースプレー",
            "korean": "실버스프레이",
            "french": "Superepousse",
            "german": "Superschutz",
            "spanish": "Superrepelente",
            "italian": "Superrepellente",
            "english": "Super Repel"
        }
    },
    {
        "id": "maxrepel",
        "names": {
            "japanese": "ゴールドスプレー",
            "korean": "골드스프레이",
            "french": "Max Repousse",
            "german": "Top-Schutz",
            "spanish": "Repelente Máximo",
            "italian": "Repellente Max",
            "english": "Max Repel"
        }
    },
    {
        "id": "escaperope",
        "names": {
            "japanese": "あなぬけのヒモ",
            "korean": "동굴탈출로프",
            "french": "Corde Sortie",
            "german": "Fluchtseil",
            "spanish": "Cuerda Huida",
            "italian": "Fune di Fuga",
            "english": "Escape Rope"
        }
    },
    {
        "id": "repel",
        "names": {
            "japanese": "むしよけスプレー",
            "korean": "벌레회피스프레이",
            "french": "Repousse",
            "german": "Schutz",
            "spanish": "Repelente",
            "italian": "Repellente",
            "english": "Repel"
        }
    },
    {
        "id": "sunstone",
        "names": {
            "japanese": "たいようのいし",
            "korean": "태양의돌",
            "french": "Pierre Soleil",
            "german": "Sonnenstein",
            "spanish": "Piedra Solar",
            "italian": "Pietrasolare",
            "english": "Sun Stone"
        }
    },
    {
        "id": "moonstone",
        "names": {
            "japanese": "つきのいし",
            "korean": "달의돌",
            "french": "Pierre Lune",
            "german": "Mondstein",
            "spanish": "Piedra Lunar",
            "italian": "Pietralunare",
            "english": "Moon Stone"
        }
    },
    {
        "id": "firestone",
        "names": {
            "japanese": "ほのおのいし",
            "korean": "불꽃의돌",
            "french": "Pierre Feu",
            "german": "Feuerstein",
            "spanish": "Piedra Fuego",
            "italian": "Pietrafocaia",
            "english": "Fire Stone"
        }
    },
    {
        "id": "thunderstone",
        "names": {
            "japanese": "かみなりのいし",
            "korean": "천둥의돌",
            "french": "Pierre Foudre",
            "german": "Donnerstein",
            "spanish": "Piedra Trueno",
            "italian": "Pietratuono",
            "english": "Thunder Stone"
        }
    },
    {
        "id": "waterstone",
        "names": {
            "japanese": "みずのいし",
            "korean": "물의돌",
            "french": "Pierre Eau",
            "german": "Wasserstein",
            "spanish": "Piedra Agua",
            "italian": "Pietraidrica",
            "english": "Water Stone"
        }
    },
    {
        "id": "leafstone",
        "names": {
            "japanese": "リーフのいし",
            "korean": "리프의돌",
            "french": "Pierre Plante",
            "german": "Blattstein",
            "spanish": "Piedra Hoja",
            "italian": "Pietrafoglia",
            "english": "Leaf Stone"
        }
    },
    {
        "id": "tinymushroom",
        "names": {
            "japanese": "ちいさなキノコ",
            "korean": "작은버섯",
            "french": "Petit Champi",
            "german": "Minipilz",
            "spanish": "Mini Seta",
            "italian": "Minifungo",
            "english": "Tiny Mushroom"
        }
    },
    {
        "id": "bigmushroom",
        "names": {
            "japanese": "おおきなキノコ",
            "korean": "큰버섯",
            "french": "Gros Champi",
            "german": "Riesenpilz",
            "spanish": "Seta Grande",
            "italian": "Grande Fungo",
            "english": "Big Mushroom"
        }
    },
    {
        "id": "pearl",
        "names": {
            "japanese": "しんじゅ",
            "korean": "진주",
            "french": "Perle",
            "german": "Perle",
            "spanish": "Perla",
            "italian": "Perla",
            "english": "Pearl"
        }
    },
    {
        "id": "bigpearl",
        "names": {
            "japanese": "おおきなしんじゅ",
            "korean": "큰진주",
            "french": "Grande Perle",
            "german": "Riesenperle",
            "spanish": "Perla Grande",
            "italian": "Grande Perla",
            "english": "Big Pearl"
        }
    },
    {
        "id": "stardust",
        "names": {
            "japanese": "ほしのすな",
            "korean": "별의모래",
            "french": "Poussière Étoile",
            "german": "Sternenstaub",
            "spanish": "Polvoestelar",
            "italian": "Polvostella",
            "english": "Stardust"
        }
    },
    {
        "id": "starpiece",
        "names": {
            "japanese": "ほしのかけら",
            "korean": "별의조각",
            "french": "Morceau d'Étoile",
            "german": "Sternenstück",
            "spanish": "Trozo Estrella",
            "italian": "Pezzo Stella",
            "english": "Star Piece"
        }
    },
    {
        "id": "nugget",
        "names": {
            "japanese": "きんのたま",
            "korean": "금구슬",
            "french": "Pépite",
            "german": "Nugget",
            "spanish": "Pepita",
            "italian": "Pepita",
            "english": "Nugget"
        }
    },
    {
        "id": "heartscale",
        "names": {
            "japanese": "ハートのウロコ",
            "korean": "하트비늘",
            "french": "Écaille Cœur",
            "german": "Herzschuppe",
            "spanish": "Escama Corazón",
            "italian": "Squama Cuore",
            "english": "Heart Scale"
        }
    },
    {
        "id": "honey",
        "names": {
            "japanese": "あまいミツ",
            "korean": "달콤한꿀",
            "french": "Miel",
            "german": "Honig",
            "spanish": "Miel",
            "italian": "Miele",
            "english": "Honey"
        }
    },
    {
        "id": "growthmulch",
        "names": {
            "japanese": "すくすくこやし",
            "korean": "무럭무럭비료",
            "french": "Fertipousse",
            "german": "Wachsmulch",
            "spanish": "Abono Rápido",
            "italian": "Fertilrapido",
            "english": "Growth Mulch"
        }
    },
    {
        "id": "dampmulch",
        "names": {
            "japanese": "じめじめこやし",
            "korean": "축축이비료",
            "french": "Fertihumide",
            "german": "Feuchtmulch",
            "spanish": "Abono Lento",
            "italian": "Fertilidro",
            "english": "Damp Mulch"
        }
    },
    {
        "id": "stablemulch",
        "names": {
            "japanese": "ながながこやし",
            "korean": "오래오래비료",
            "french": "Fertistable",
            "german": "Stabilmulch",
            "spanish": "Abono Fijador",
            "italian": "Fertilsaldo",
            "english": "Stable Mulch"
        }
    },
    {
        "id": "gooeymulch",
        "names": {
            "japanese": "ねばねばこやし",
            "korean": "끈적끈적비료",
            "french": "Fertiglu",
            "german": "Neumulch",
            "spanish": "Abono Brote",
            "italian": "Fertilcolla",
            "english": "Gooey Mulch"
        }
    },
    {
        "id": "shinystone",
        "names": {
            "japanese": "ひかりのいし",
            "korean": "빛의돌",
            "french": "Pierre Éclat",
            "german": "Leuchtstein",
            "spanish": "Piedra Día",
            "italian": "Pietrabrillo",
            "english": "Shiny Stone"
        }
    },
    {
        "id": "duskstone",
        "names": {
            "japanese": "やみのいし",
            "korean": "어둠의돌",
            "french": "Pierre Nuit",
            "german": "Finsterstein",
            "spanish": "Piedra Noche",
            "italian": "Neropietra",
            "english": "Dusk Stone"
        }
    },
    {
        "id": "dawnstone",
        "names": {
            "japanese": "めざめいし",
            "korean": "각성의돌",
            "french": "Pierre Aube",
            "german": "Funkelstein",
            "spanish": "Piedra Alba",
            "italian": "Pietralbore",
            "english": "Dawn Stone"
        }
    },
    {
        "id": "ovalstone",
        "names": {
            "japanese": "まんまるいし",
            "korean": "동글동글돌",
            "french": "Pierre Ovale",
            "german": "Ovaler Stein",
            "spanish": "Piedra Oval",
            "italian": "Pietraovale",
            "english": "Oval Stone"
        }
    },
    {
        "id": "oddkeystone",
        "names": {
            "japanese": "かなめいし",
            "korean": "쐐기돌",
            "french": "Clé de Voûte",
            "german": "Spiritkern",
            "spanish": "Piedra Espíritu",
            "italian": "Roccianima",
            "english": "Odd Keystone"
        }
    },
    {
        "id": "grassmail",
        "names": {
            "japanese": "グラスメール",
            "french": "Lettre Herbe",
            "german": "Wiesenbrief",
            "spanish": "Carta Hierba",
            "italian": "Mess. Erba",
            "english": "Grass Mail"
        }
    },
    {
        "id": "flamemail",
        "names": {
            "japanese": "フレイムメール",
            "french": "Lettre Feu",
            "german": "Feuerbrief",
            "spanish": "Carta Fuego",
            "italian": "Mess. Fiamma",
            "english": "Flame Mail"
        }
    },
    {
        "id": "bubblemail",
        "names": {
            "japanese": "ブルーメール",
            "french": "Lettre Mer",
            "german": "Wasserbrief",
            "spanish": "Carta Pompas",
            "italian": "Mess. Bolla",
            "english": "Bubble Mail"
        }
    },
    {
        "id": "bloommail",
        "names": {
            "japanese": "ブルームメール",
            "french": "Lett. Pétale",
            "german": "Blütenbrief",
            "spanish": "Carta Flores",
            "italian": "Mess. Petalo",
            "english": "Bloom Mail"
        }
    },
    {
        "id": "tunnelmail",
        "names": {
            "japanese": "トンネルメール",
            "french": "Lettre Mine",
            "german": "Minenbrief",
            "spanish": "Carta Mina",
            "italian": "Mess. Tunnel",
            "english": "Tunnel Mail"
        }
    },
    {
        "id": "steelmail",
        "names": {
            "japanese": "スチールメール",
            "french": "Lettre Acier",
            "german": "Stahlbrief",
            "spanish": "Carta Acero",
            "italian": "Mess. Lega",
            "english": "Steel Mail"
        }
    },
    {
        "id": "heartmail",
        "names": {
            "japanese": "ラブラブメール",
            "french": "Lettre Coeur",
            "german": "Rosabrief",
            "spanish": "Car. Corazón",
            "italian": "Mess. Cuore",
            "english": "Heart Mail"
        }
    },
    {
        "id": "snowmail",
        "names": {
            "japanese": "ブリザードメール",
            "french": "Lettre Neige",
            "german": "Schneebrief",
            "spanish": "Carta Nieve",
            "italian": "Mess. Neve",
            "english": "Snow Mail"
        }
    },
    {
        "id": "spacemail",
        "names": {
            "japanese": "スペースメール",
            "french": "Lettre Cosmo",
            "german": "Sternbrief",
            "spanish": "Car. Sideral",
            "italian": "Mess. Spazio",
            "english": "Space Mail"
        }
    },
    {
        "id": "airmail",
        "names": {
            "japanese": "エアメール",
            "french": "Lettre Avion",
            "german": "Luftbrief",
            "spanish": "Carta Aérea",
            "italian": "Mess. Aereo",
            "english": "Air Mail"
        }
    },
    {
        "id": "mosaicmail",
        "names": {
            "japanese": "モザイクメール",
            "french": "Lettremosaïk",
            "german": "Mosaikbrief",
            "spanish": "Car. Mosaico",
            "italian": "Mess. Iride",
            "english": "Mosaic Mail"
        }
    },
    {
        "id": "brickmail",
        "names": {
            "japanese": "ブリックメール",
            "french": "Lettre Brik",
            "german": "Ziegelbrief",
            "spanish": "Carta Pared",
            "italian": "Mess. Muro",
            "english": "Brick Mail"
        }
    },
    {
        "id": "expshare",
        "names": {
            "japanese": "がくしゅうそうち",
            "korean": "학습장치",
            "french": "Multi Exp",
            "german": "EP-Teiler",
            "spanish": "Repartir Exp",
            "italian": "Condividi Esp.",
            "english": "Exp. Share"
        }
    },
    {
        "id": "soothebell",
        "names": {
            "japanese": "やすらぎのすず",
            "korean": "평온의방울",
            "french": "Grelot Zen",
            "german": "Sanftglocke",
            "spanish": "Campana Alivio",
            "italian": "Calmanella",
            "english": "Soothe Bell"
        }
    },
    {
        "id": "amuletcoin",
        "names": {
            "japanese": "おまもりこばん",
            "korean": "부적금화",
            "french": "Pièce Rune",
            "german": "Münzamulett",
            "spanish": "Moneda Amuleto",
            "italian": "Monetamuleto",
            "english": "Amulet Coin"
        }
    },
    {
        "id": "cleansetag",
        "names": {
            "japanese": "きよめのおふだ",
            "korean": "순결의부적",
            "french": "Rune Purifiante",
            "german": "Schutzband",
            "spanish": "Amuleto",
            "italian": "Velopuro",
            "english": "Cleanse Tag"
        }
    },
    {
        "id": "smokeball",
        "names": {
            "japanese": "けむりだま",
            "korean": "연막탄",
            "french": "Boule Fumée",
            "german": "Rauchball",
            "spanish": "Bola Humo",
            "italian": "Palla Fumo",
            "english": "Smoke Ball"
        }
    },
    {
        "id": "everstone",
        "names": {
            "japanese": "かわらずのいし",
            "korean": "변함없는돌",
            "french": "Pierre Stase",
            "german": "Ewigstein",
            "spanish": "Piedra Eterna",
            "italian": "Pietrastante",
            "english": "Everstone"
        }
    },
    {
        "id": "luckyegg",
        "names": {
            "japanese": "しあわせタマゴ",
            "korean": "행복의알",
            "french": "Œuf Chance",
            "german": "Glücks-Ei",
            "spanish": "Huevo Suerte",
            "italian": "Fortunuovo",
            "english": "Lucky Egg"
        }
    },
    {
        "id": "upgrade",
        "names": {
            "japanese": "アップグレード",
            "korean": "업그레이드",
            "french": "Améliorator",
            "german": "Up-Grade",
            "spanish": "Mejora",
            "italian": "Upgrade",
            "english": "Up-Grade"
        }
    },
    {
        "id": "redscarf",
        "names": {
            "japanese": "あかいバンダナ",
            "korean": "빨강밴드",
            "french": "Foulard Rouge",
            "german": "Roter Schal",
            "spanish": "Pañuelo Rojo",
            "italian": "Fascia Rossa",
            "english": "Red Scarf"
        }
    },
    {
        "id": "bluescarf",
        "names": {
            "japanese": "あおいバンダナ",
            "korean": "파랑밴드",
            "french": "Foulard Bleu",
            "german": "Blauer Schal",
            "spanish": "Pañuelo Azul",
            "italian": "Fascia Blu",
            "english": "Blue Scarf"
        }
    },
    {
        "id": "pinkscarf",
        "names": {
            "japanese": "ピンクのバンダナ",
            "korean": "분홍밴드",
            "french": "Foulard Rose",
            "german": "Rosa Schal",
            "spanish": "Pañuelo Rosa",
            "italian": "Fascia Rosa",
            "english": "Pink Scarf"
        }
    },
    {
        "id": "greenscarf",
        "names": {
            "japanese": "みどりのバンダナ",
            "korean": "초록밴드",
            "french": "Foulard Vert",
            "german": "Grüner Schal",
            "spanish": "Pañuelo Verde",
            "italian": "Fascia Verde",
            "english": "Green Scarf"
        }
    },
    {
        "id": "yellowscarf",
        "names": {
            "japanese": "きいろのバンダナ",
            "korean": "노랑밴드",
            "french": "Foulard Jaune",
            "german": "Gelber Schal",
            "spanish": "Pañuelo Amarillo",
            "italian": "Fascia Gialla",
            "english": "Yellow Scarf"
        }
    },
    {
        "id": "luckincense",
        "names": {
            "japanese": "こううんのおこう",
            "korean": "행운의향로",
            "french": "Encens Veine",
            "german": "Glücksrauch",
            "spanish": "Incienso Duplo",
            "italian": "Fortunaroma",
            "english": "Luck Incense"
        }
    },
    {
        "id": "pureincense",
        "names": {
            "japanese": "きよめのおこう",
            "korean": "순결의향로",
            "french": "Encens Pur",
            "german": "Scheuchrauch",
            "spanish": "Incienso Puro",
            "italian": "Puroaroma",
            "english": "Pure Incense"
        }
    },
    {
        "id": "protector",
        "names": {
            "japanese": "プロテクター",
            "korean": "프로텍터",
            "french": "Protecteur",
            "german": "Schützer",
            "spanish": "Protector",
            "italian": "Copertura",
            "english": "Protector"
        }
    },
    {
        "id": "magmarizer",
        "names": {
            "japanese": "マグマブースター",
            "korean": "마그마부스터",
            "french": "Magmariseur",
            "german": "Magmaisierer",
            "spanish": "Magmatizador",
            "italian": "Magmatore",
            "english": "Magmarizer"
        }
    },
    {
        "id": "dubiousdisc",
        "names": {
            "japanese": "あやしいパッチ",
            "korean": "괴상한패치",
            "french": "CD Douteux",
            "german": "Dubiosdisc",
            "spanish": "Disco Extraño",
            "italian": "Dubbiodisco",
            "english": "Dubious Disc"
        }
    },
    {
        "id": "reapercloth",
        "names": {
            "japanese": "れいかいのぬの",
            "korean": "영계의천",
            "french": "Tissu Fauche",
            "german": "Düsterumhang",
            "spanish": "Tela Terrible",
            "italian": "Terrorpanno",
            "english": "Reaper Cloth"
        }
    },
    {
        "id": "tm01",
        "names": {
            "japanese": "わざマシン０１",
            "korean": "기술머신01",
            "french": "CT01",
            "german": "TM01",
            "spanish": "MT01",
            "italian": "MT01",
            "english": "TM01"
        }
    },
    {
        "id": "tm02",
        "names": {
            "japanese": "わざマシン０２",
            "korean": "기술머신02",
            "french": "CT02",
            "german": "TM02",
            "spanish": "MT02",
            "italian": "MT02",
            "english": "TM02"
        }
    },
    {
        "id": "tm03",
        "names": {
            "japanese": "わざマシン０３",
            "korean": "기술머신03",
            "french": "CT03",
            "german": "TM03",
            "spanish": "MT03",
            "italian": "MT03",
            "english": "TM03"
        }
    },
    {
        "id": "tm04",
        "names": {
            "japanese": "わざマシン０４",
            "korean": "기술머신04",
            "french": "CT04",
            "german": "TM04",
            "spanish": "MT04",
            "italian": "MT04",
            "english": "TM04"
        }
    },
    {
        "id": "tm05",
        "names": {
            "japanese": "わざマシン０５",
            "korean": "기술머신05",
            "french": "CT05",
            "german": "TM05",
            "spanish": "MT05",
            "italian": "MT05",
            "english": "TM05"
        }
    },
    {
        "id": "tm06",
        "names": {
            "japanese": "わざマシン０６",
            "korean": "기술머신06",
            "french": "CT06",
            "german": "TM06",
            "spanish": "MT06",
            "italian": "MT06",
            "english": "TM06"
        }
    },
    {
        "id": "tm07",
        "names": {
            "japanese": "わざマシン０７",
            "korean": "기술머신07",
            "french": "CT07",
            "german": "TM07",
            "spanish": "MT07",
            "italian": "MT07",
            "english": "TM07"
        }
    },
    {
        "id": "tm08",
        "names": {
            "japanese": "わざマシン０８",
            "korean": "기술머신08",
            "french": "CT08",
            "german": "TM08",
            "spanish": "MT08",
            "italian": "MT08",
            "english": "TM08"
        }
    },
    {
        "id": "tm09",
        "names": {
            "japanese": "わざマシン０９",
            "korean": "기술머신09",
            "french": "CT09",
            "german": "TM09",
            "spanish": "MT09",
            "italian": "MT09",
            "english": "TM09"
        }
    },
    {
        "id": "tm10",
        "names": {
            "japanese": "わざマシン１０",
            "korean": "기술머신10",
            "french": "CT10",
            "german": "TM10",
            "spanish": "MT10",
            "italian": "MT10",
            "english": "TM10"
        }
    },
    {
        "id": "tm11",
        "names": {
            "japanese": "わざマシン１１",
            "korean": "기술머신11",
            "french": "CT11",
            "german": "TM11",
            "spanish": "MT11",
            "italian": "MT11",
            "english": "TM11"
        }
    },
    {
        "id": "tm12",
        "names": {
            "japanese": "わざマシン１２",
            "korean": "기술머신12",
            "french": "CT12",
            "german": "TM12",
            "spanish": "MT12",
            "italian": "MT12",
            "english": "TM12"
        }
    },
    {
        "id": "tm13",
        "names": {
            "japanese": "わざマシン１３",
            "korean": "기술머신13",
            "french": "CT13",
            "german": "TM13",
            "spanish": "MT13",
            "italian": "MT13",
            "english": "TM13"
        }
    },
    {
        "id": "tm14",
        "names": {
            "japanese": "わざマシン１４",
            "korean": "기술머신14",
            "french": "CT14",
            "german": "TM14",
            "spanish": "MT14",
            "italian": "MT14",
            "english": "TM14"
        }
    },
    {
        "id": "tm15",
        "names": {
            "japanese": "わざマシン１５",
            "korean": "기술머신15",
            "french": "CT15",
            "german": "TM15",
            "spanish": "MT15",
            "italian": "MT15",
            "english": "TM15"
        }
    },
    {
        "id": "tm16",
        "names": {
            "japanese": "わざマシン１６",
            "korean": "기술머신16",
            "french": "CT16",
            "german": "TM16",
            "spanish": "MT16",
            "italian": "MT16",
            "english": "TM16"
        }
    },
    {
        "id": "tm17",
        "names": {
            "japanese": "わざマシン１７",
            "korean": "기술머신17",
            "french": "CT17",
            "german": "TM17",
            "spanish": "MT17",
            "italian": "MT17",
            "english": "TM17"
        }
    },
    {
        "id": "tm18",
        "names": {
            "japanese": "わざマシン１８",
            "korean": "기술머신18",
            "french": "CT18",
            "german": "TM18",
            "spanish": "MT18",
            "italian": "MT18",
            "english": "TM18"
        }
    },
    {
        "id": "tm19",
        "names": {
            "japanese": "わざマシン１９",
            "korean": "기술머신19",
            "french": "CT19",
            "german": "TM19",
            "spanish": "MT19",
            "italian": "MT19",
            "english": "TM19"
        }
    },
    {
        "id": "tm20",
        "names": {
            "japanese": "わざマシン２０",
            "korean": "기술머신20",
            "french": "CT20",
            "german": "TM20",
            "spanish": "MT20",
            "italian": "MT20",
            "english": "TM20"
        }
    },
    {
        "id": "tm21",
        "names": {
            "japanese": "わざマシン２１",
            "korean": "기술머신21",
            "french": "CT21",
            "german": "TM21",
            "spanish": "MT21",
            "italian": "MT21",
            "english": "TM21"
        }
    },
    {
        "id": "tm22",
        "names": {
            "japanese": "わざマシン２２",
            "korean": "기술머신22",
            "french": "CT22",
            "german": "TM22",
            "spanish": "MT22",
            "italian": "MT22",
            "english": "TM22"
        }
    },
    {
        "id": "tm23",
        "names": {
            "japanese": "わざマシン２３",
            "korean": "기술머신23",
            "french": "CT23",
            "german": "TM23",
            "spanish": "MT23",
            "italian": "MT23",
            "english": "TM23"
        }
    },
    {
        "id": "tm24",
        "names": {
            "japanese": "わざマシン２４",
            "korean": "기술머신24",
            "french": "CT24",
            "german": "TM24",
            "spanish": "MT24",
            "italian": "MT24",
            "english": "TM24"
        }
    },
    {
        "id": "tm25",
        "names": {
            "japanese": "わざマシン２５",
            "korean": "기술머신25",
            "french": "CT25",
            "german": "TM25",
            "spanish": "MT25",
            "italian": "MT25",
            "english": "TM25"
        }
    },
    {
        "id": "tm26",
        "names": {
            "japanese": "わざマシン２６",
            "korean": "기술머신26",
            "french": "CT26",
            "german": "TM26",
            "spanish": "MT26",
            "italian": "MT26",
            "english": "TM26"
        }
    },
    {
        "id": "tm27",
        "names": {
            "japanese": "わざマシン２７",
            "korean": "기술머신27",
            "french": "CT27",
            "german": "TM27",
            "spanish": "MT27",
            "italian": "MT27",
            "english": "TM27"
        }
    },
    {
        "id": "tm28",
        "names": {
            "japanese": "わざマシン２８",
            "korean": "기술머신28",
            "french": "CT28",
            "german": "TM28",
            "spanish": "MT28",
            "italian": "MT28",
            "english": "TM28"
        }
    },
    {
        "id": "tm29",
        "names": {
            "japanese": "わざマシン２９",
            "korean": "기술머신29",
            "french": "CT29",
            "german": "TM29",
            "spanish": "MT29",
            "italian": "MT29",
            "english": "TM29"
        }
    },
    {
        "id": "tm30",
        "names": {
            "japanese": "わざマシン３０",
            "korean": "기술머신30",
            "french": "CT30",
            "german": "TM30",
            "spanish": "MT30",
            "italian": "MT30",
            "english": "TM30"
        }
    },
    {
        "id": "tm31",
        "names": {
            "japanese": "わざマシン３１",
            "korean": "기술머신31",
            "french": "CT31",
            "german": "TM31",
            "spanish": "MT31",
            "italian": "MT31",
            "english": "TM31"
        }
    },
    {
        "id": "tm32",
        "names": {
            "japanese": "わざマシン３２",
            "korean": "기술머신32",
            "french": "CT32",
            "german": "TM32",
            "spanish": "MT32",
            "italian": "MT32",
            "english": "TM32"
        }
    },
    {
        "id": "tm33",
        "names": {
            "japanese": "わざマシン３３",
            "korean": "기술머신33",
            "french": "CT33",
            "german": "TM33",
            "spanish": "MT33",
            "italian": "MT33",
            "english": "TM33"
        }
    },
    {
        "id": "tm34",
        "names": {
            "japanese": "わざマシン３４",
            "korean": "기술머신34",
            "french": "CT34",
            "german": "TM34",
            "spanish": "MT34",
            "italian": "MT34",
            "english": "TM34"
        }
    },
    {
        "id": "tm35",
        "names": {
            "japanese": "わざマシン３５",
            "korean": "기술머신35",
            "french": "CT35",
            "german": "TM35",
            "spanish": "MT35",
            "italian": "MT35",
            "english": "TM35"
        }
    },
    {
        "id": "tm36",
        "names": {
            "japanese": "わざマシン３６",
            "korean": "기술머신36",
            "french": "CT36",
            "german": "TM36",
            "spanish": "MT36",
            "italian": "MT36",
            "english": "TM36"
        }
    },
    {
        "id": "tm37",
        "names": {
            "japanese": "わざマシン３７",
            "korean": "기술머신37",
            "french": "CT37",
            "german": "TM37",
            "spanish": "MT37",
            "italian": "MT37",
            "english": "TM37"
        }
    },
    {
        "id": "tm38",
        "names": {
            "japanese": "わざマシン３８",
            "korean": "기술머신38",
            "french": "CT38",
            "german": "TM38",
            "spanish": "MT38",
            "italian": "MT38",
            "english": "TM38"
        }
    },
    {
        "id": "tm39",
        "names": {
            "japanese": "わざマシン３９",
            "korean": "기술머신39",
            "french": "CT39",
            "german": "TM39",
            "spanish": "MT39",
            "italian": "MT39",
            "english": "TM39"
        }
    },
    {
        "id": "tm40",
        "names": {
            "japanese": "わざマシン４０",
            "korean": "기술머신40",
            "french": "CT40",
            "german": "TM40",
            "spanish": "MT40",
            "italian": "MT40",
            "english": "TM40"
        }
    },
    {
        "id": "tm41",
        "names": {
            "japanese": "わざマシン４１",
            "korean": "기술머신41",
            "french": "CT41",
            "german": "TM41",
            "spanish": "MT41",
            "italian": "MT41",
            "english": "TM41"
        }
    },
    {
        "id": "tm42",
        "names": {
            "japanese": "わざマシン４２",
            "korean": "기술머신42",
            "french": "CT42",
            "german": "TM42",
            "spanish": "MT42",
            "italian": "MT42",
            "english": "TM42"
        }
    },
    {
        "id": "tm43",
        "names": {
            "japanese": "わざマシン４３",
            "korean": "기술머신43",
            "french": "CT43",
            "german": "TM43",
            "spanish": "MT43",
            "italian": "MT43",
            "english": "TM43"
        }
    },
    {
        "id": "tm44",
        "names": {
            "japanese": "わざマシン４４",
            "korean": "기술머신44",
            "french": "CT44",
            "german": "TM44",
            "spanish": "MT44",
            "italian": "MT44",
            "english": "TM44"
        }
    },
    {
        "id": "tm45",
        "names": {
            "japanese": "わざマシン４５",
            "korean": "기술머신45",
            "french": "CT45",
            "german": "TM45",
            "spanish": "MT45",
            "italian": "MT45",
            "english": "TM45"
        }
    },
    {
        "id": "tm46",
        "names": {
            "japanese": "わざマシン４６",
            "korean": "기술머신46",
            "french": "CT46",
            "german": "TM46",
            "spanish": "MT46",
            "italian": "MT46",
            "english": "TM46"
        }
    },
    {
        "id": "tm47",
        "names": {
            "japanese": "わざマシン４７",
            "korean": "기술머신47",
            "french": "CT47",
            "german": "TM47",
            "spanish": "MT47",
            "italian": "MT47",
            "english": "TM47"
        }
    },
    {
        "id": "tm48",
        "names": {
            "japanese": "わざマシン４８",
            "korean": "기술머신48",
            "french": "CT48",
            "german": "TM48",
            "spanish": "MT48",
            "italian": "MT48",
            "english": "TM48"
        }
    },
    {
        "id": "tm49",
        "names": {
            "japanese": "わざマシン４９",
            "korean": "기술머신49",
            "french": "CT49",
            "german": "TM49",
            "spanish": "MT49",
            "italian": "MT49",
            "english": "TM49"
        }
    },
    {
        "id": "tm50",
        "names": {
            "japanese": "わざマシン５０",
            "korean": "기술머신50",
            "french": "CT50",
            "german": "TM50",
            "spanish": "MT50",
            "italian": "MT50",
            "english": "TM50"
        }
    },
    {
        "id": "tm51",
        "names": {
            "japanese": "わざマシン５１",
            "korean": "기술머신51",
            "french": "CT51",
            "german": "TM51",
            "spanish": "MT51",
            "italian": "MT51",
            "english": "TM51"
        }
    },
    {
        "id": "tm52",
        "names": {
            "japanese": "わざマシン５２",
            "korean": "기술머신52",
            "french": "CT52",
            "german": "TM52",
            "spanish": "MT52",
            "italian": "MT52",
            "english": "TM52"
        }
    },
    {
        "id": "tm53",
        "names": {
            "japanese": "わざマシン５３",
            "korean": "기술머신53",
            "french": "CT53",
            "german": "TM53",
            "spanish": "MT53",
            "italian": "MT53",
            "english": "TM53"
        }
    },
    {
        "id": "tm54",
        "names": {
            "japanese": "わざマシン５４",
            "korean": "기술머신54",
            "french": "CT54",
            "german": "TM54",
            "spanish": "MT54",
            "italian": "MT54",
            "english": "TM54"
        }
    },
    {
        "id": "tm55",
        "names": {
            "japanese": "わざマシン５５",
            "korean": "기술머신55",
            "french": "CT55",
            "german": "TM55",
            "spanish": "MT55",
            "italian": "MT55",
            "english": "TM55"
        }
    },
    {
        "id": "tm56",
        "names": {
            "japanese": "わざマシン５６",
            "korean": "기술머신56",
            "french": "CT56",
            "german": "TM56",
            "spanish": "MT56",
            "italian": "MT56",
            "english": "TM56"
        }
    },
    {
        "id": "tm57",
        "names": {
            "japanese": "わざマシン５７",
            "korean": "기술머신57",
            "french": "CT57",
            "german": "TM57",
            "spanish": "MT57",
            "italian": "MT57",
            "english": "TM57"
        }
    },
    {
        "id": "tm58",
        "names": {
            "japanese": "わざマシン５８",
            "korean": "기술머신58",
            "french": "CT58",
            "german": "TM58",
            "spanish": "MT58",
            "italian": "MT58",
            "english": "TM58"
        }
    },
    {
        "id": "tm59",
        "names": {
            "japanese": "わざマシン５９",
            "korean": "기술머신59",
            "french": "CT59",
            "german": "TM59",
            "spanish": "MT59",
            "italian": "MT59",
            "english": "TM59"
        }
    },
    {
        "id": "tm60",
        "names": {
            "japanese": "わざマシン６０",
            "korean": "기술머신60",
            "french": "CT60",
            "german": "TM60",
            "spanish": "MT60",
            "italian": "MT60",
            "english": "TM60"
        }
    },
    {
        "id": "tm61",
        "names": {
            "japanese": "わざマシン６１",
            "korean": "기술머신61",
            "french": "CT61",
            "german": "TM61",
            "spanish": "MT61",
            "italian": "MT61",
            "english": "TM61"
        }
    },
    {
        "id": "tm62",
        "names": {
            "japanese": "わざマシン６２",
            "korean": "기술머신62",
            "french": "CT62",
            "german": "TM62",
            "spanish": "MT62",
            "italian": "MT62",
            "english": "TM62"
        }
    },
    {
        "id": "tm63",
        "names": {
            "japanese": "わざマシン６３",
            "korean": "기술머신63",
            "french": "CT63",
            "german": "TM63",
            "spanish": "MT63",
            "italian": "MT63",
            "english": "TM63"
        }
    },
    {
        "id": "tm64",
        "names": {
            "japanese": "わざマシン６４",
            "korean": "기술머신64",
            "french": "CT64",
            "german": "TM64",
            "spanish": "MT64",
            "italian": "MT64",
            "english": "TM64"
        }
    },
    {
        "id": "tm65",
        "names": {
            "japanese": "わざマシン６５",
            "korean": "기술머신65",
            "french": "CT65",
            "german": "TM65",
            "spanish": "MT65",
            "italian": "MT65",
            "english": "TM65"
        }
    },
    {
        "id": "tm66",
        "names": {
            "japanese": "わざマシン６６",
            "korean": "기술머신66",
            "french": "CT66",
            "german": "TM66",
            "spanish": "MT66",
            "italian": "MT66",
            "english": "TM66"
        }
    },
    {
        "id": "tm67",
        "names": {
            "japanese": "わざマシン６７",
            "korean": "기술머신67",
            "french": "CT67",
            "german": "TM67",
            "spanish": "MT67",
            "italian": "MT67",
            "english": "TM67"
        }
    },
    {
        "id": "tm68",
        "names": {
            "japanese": "わざマシン６８",
            "korean": "기술머신68",
            "french": "CT68",
            "german": "TM68",
            "spanish": "MT68",
            "italian": "MT68",
            "english": "TM68"
        }
    },
    {
        "id": "tm69",
        "names": {
            "japanese": "わざマシン６９",
            "korean": "기술머신69",
            "french": "CT69",
            "german": "TM69",
            "spanish": "MT69",
            "italian": "MT69",
            "english": "TM69"
        }
    },
    {
        "id": "tm70",
        "names": {
            "japanese": "わざマシン７０",
            "korean": "기술머신70",
            "french": "CT70",
            "german": "TM70",
            "spanish": "MT70",
            "italian": "MT70",
            "english": "TM70"
        }
    },
    {
        "id": "tm71",
        "names": {
            "japanese": "わざマシン７１",
            "korean": "기술머신71",
            "french": "CT71",
            "german": "TM71",
            "spanish": "MT71",
            "italian": "MT71",
            "english": "TM71"
        }
    },
    {
        "id": "tm72",
        "names": {
            "japanese": "わざマシン７２",
            "korean": "기술머신72",
            "french": "CT72",
            "german": "TM72",
            "spanish": "MT72",
            "italian": "MT72",
            "english": "TM72"
        }
    },
    {
        "id": "tm73",
        "names": {
            "japanese": "わざマシン７３",
            "korean": "기술머신73",
            "french": "CT73",
            "german": "TM73",
            "spanish": "MT73",
            "italian": "MT73",
            "english": "TM73"
        }
    },
    {
        "id": "tm74",
        "names": {
            "japanese": "わざマシン７４",
            "korean": "기술머신74",
            "french": "CT74",
            "german": "TM74",
            "spanish": "MT74",
            "italian": "MT74",
            "english": "TM74"
        }
    },
    {
        "id": "tm75",
        "names": {
            "japanese": "わざマシン７５",
            "korean": "기술머신75",
            "french": "CT75",
            "german": "TM75",
            "spanish": "MT75",
            "italian": "MT75",
            "english": "TM75"
        }
    },
    {
        "id": "tm76",
        "names": {
            "japanese": "わざマシン７６",
            "korean": "기술머신76",
            "french": "CT76",
            "german": "TM76",
            "spanish": "MT76",
            "italian": "MT76",
            "english": "TM76"
        }
    },
    {
        "id": "tm77",
        "names": {
            "japanese": "わざマシン７７",
            "korean": "기술머신77",
            "french": "CT77",
            "german": "TM77",
            "spanish": "MT77",
            "italian": "MT77",
            "english": "TM77"
        }
    },
    {
        "id": "tm78",
        "names": {
            "japanese": "わざマシン７８",
            "korean": "기술머신78",
            "french": "CT78",
            "german": "TM78",
            "spanish": "MT78",
            "italian": "MT78",
            "english": "TM78"
        }
    },
    {
        "id": "tm79",
        "names": {
            "japanese": "わざマシン７９",
            "korean": "기술머신79",
            "french": "CT79",
            "german": "TM79",
            "spanish": "MT79",
            "italian": "MT79",
            "english": "TM79"
        }
    },
    {
        "id": "tm80",
        "names": {
            "japanese": "わざマシン８０",
            "korean": "기술머신80",
            "french": "CT80",
            "german": "TM80",
            "spanish": "MT80",
            "italian": "MT80",
            "english": "TM80"
        }
    },
    {
        "id": "tm81",
        "names": {
            "japanese": "わざマシン８１",
            "korean": "기술머신81",
            "french": "CT81",
            "german": "TM81",
            "spanish": "MT81",
            "italian": "MT81",
            "english": "TM81"
        }
    },
    {
        "id": "tm82",
        "names": {
            "japanese": "わざマシン８２",
            "korean": "기술머신82",
            "french": "CT82",
            "german": "TM82",
            "spanish": "MT82",
            "italian": "MT82",
            "english": "TM82"
        }
    },
    {
        "id": "tm83",
        "names": {
            "japanese": "わざマシン８３",
            "korean": "기술머신83",
            "french": "CT83",
            "german": "TM83",
            "spanish": "MT83",
            "italian": "MT83",
            "english": "TM83"
        }
    },
    {
        "id": "tm84",
        "names": {
            "japanese": "わざマシン８４",
            "korean": "기술머신84",
            "french": "CT84",
            "german": "TM84",
            "spanish": "MT84",
            "italian": "MT84",
            "english": "TM84"
        }
    },
    {
        "id": "tm85",
        "names": {
            "japanese": "わざマシン８５",
            "korean": "기술머신85",
            "french": "CT85",
            "german": "TM85",
            "spanish": "MT85",
            "italian": "MT85",
            "english": "TM85"
        }
    },
    {
        "id": "tm86",
        "names": {
            "japanese": "わざマシン８６",
            "korean": "기술머신86",
            "french": "CT86",
            "german": "TM86",
            "spanish": "MT86",
            "italian": "MT86",
            "english": "TM86"
        }
    },
    {
        "id": "tm87",
        "names": {
            "japanese": "わざマシン８７",
            "korean": "기술머신87",
            "french": "CT87",
            "german": "TM87",
            "spanish": "MT87",
            "italian": "MT87",
            "english": "TM87"
        }
    },
    {
        "id": "tm88",
        "names": {
            "japanese": "わざマシン８８",
            "korean": "기술머신88",
            "french": "CT88",
            "german": "TM88",
            "spanish": "MT88",
            "italian": "MT88",
            "english": "TM88"
        }
    },
    {
        "id": "tm89",
        "names": {
            "japanese": "わざマシン８９",
            "korean": "기술머신89",
            "french": "CT89",
            "german": "TM89",
            "spanish": "MT89",
            "italian": "MT89",
            "english": "TM89"
        }
    },
    {
        "id": "tm90",
        "names": {
            "japanese": "わざマシン９０",
            "korean": "기술머신90",
            "french": "CT90",
            "german": "TM90",
            "spanish": "MT90",
            "italian": "MT90",
            "english": "TM90"
        }
    },
    {
        "id": "tm91",
        "names": {
            "japanese": "わざマシン９１",
            "korean": "기술머신91",
            "french": "CT91",
            "german": "TM91",
            "spanish": "MT91",
            "italian": "MT91",
            "english": "TM91"
        }
    },
    {
        "id": "tm92",
        "names": {
            "japanese": "わざマシン９２",
            "korean": "기술머신92",
            "french": "CT92",
            "german": "TM92",
            "spanish": "MT92",
            "italian": "MT92",
            "english": "TM92"
        }
    },
    {
        "id": "hm01",
        "names": {
            "japanese": "ひでんマシン０１",
            "korean": "비전머신01",
            "french": "CS01",
            "german": "VM01",
            "spanish": "MO01",
            "italian": "MN01",
            "english": "HM01"
        }
    },
    {
        "id": "hm02",
        "names": {
            "japanese": "ひでんマシン０２",
            "korean": "비전머신02",
            "french": "CS02",
            "german": "VM02",
            "spanish": "MO02",
            "italian": "MN02",
            "english": "HM02"
        }
    },
    {
        "id": "hm03",
        "names": {
            "japanese": "ひでんマシン０３",
            "korean": "비전머신03",
            "french": "CS03",
            "german": "VM03",
            "spanish": "MO03",
            "italian": "MN03",
            "english": "HM03"
        }
    },
    {
        "id": "hm04",
        "names": {
            "japanese": "ひでんマシン０４",
            "korean": "비전머신04",
            "french": "CS04",
            "german": "VM04",
            "spanish": "MO04",
            "italian": "MN04",
            "english": "HM04"
        }
    },
    {
        "id": "hm05",
        "names": {
            "japanese": "ひでんマシン０５",
            "korean": "비전머신05",
            "french": "CS05",
            "german": "VM05",
            "spanish": "MO05",
            "italian": "MN05",
            "english": "HM05"
        }
    },
    {
        "id": "hm06",
        "names": {
            "japanese": "ひでんマシン０６",
            "korean": "비전머신06",
            "french": "CS06",
            "german": "VM06",
            "spanish": "MO06",
            "italian": "MN06",
            "english": "HM06"
        }
    },
    {
        "id": "hm07",
        "names": {
            "japanese": "ひでんマシン０７",
            "french": "CS07",
            "german": "VM07",
            "spanish": "MO07",
            "italian": "MN07",
            "english": "HM07"
        }
    },
    {
        "id": "hm08",
        "names": {
            "japanese": "ひでんマシン０８",
            "french": "CS08",
            "german": "VM08",
            "spanish": "MO08",
            "italian": "MN08",
            "english": "HM08"
        }
    },
    {
        "id": "explorerkit",
        "names": {
            "japanese": "たんけんセット",
            "korean": "탐험세트",
            "french": "Explorakit",
            "german": "Forschersack",
            "spanish": "Kit Explorador",
            "italian": "Esplorokit",
            "english": "Explorer Kit"
        }
    },
    {
        "id": "lootsack",
        "names": {
            "japanese": "たからぶくろ",
            "korean": "보물주머니",
            "french": "Sac Butin",
            "german": "Beutesack",
            "spanish": "Saca Botín",
            "italian": "Bottinosacca",
            "english": "Loot Sack"
        }
    },
    {
        "id": "rulebook",
        "names": {
            "japanese": "ルールブック",
            "korean": "룰북",
            "french": "Livre Règles",
            "german": "Regelbuch",
            "spanish": "Reglamento",
            "italian": "Libro Regole",
            "english": "Rule Book"
        }
    },
    {
        "id": "pokeradar",
        "names": {
            "japanese": "ポケトレ",
            "korean": "포켓트레",
            "french": "Poké Radar",
            "german": "Pokéradar",
            "spanish": "Pokéradar",
            "italian": "Poké Radar",
            "english": "Poké Radar"
        }
    },
    {
        "id": "pointcard",
        "names": {
            "japanese": "ポイントカード",
            "korean": "포인트카드",
            "french": "Carte Points",
            "german": "Punktekarte",
            "spanish": "Tarjeta Puntos",
            "italian": "Scheda Punti",
            "english": "Point Card"
        }
    },
    {
        "id": "journal",
        "names": {
            "japanese": "ぼうけんノート",
            "korean": "모험노트",
            "french": "Journal",
            "german": "Tagebuch",
            "spanish": "Diario",
            "italian": "Agenda",
            "english": "Journal"
        }
    },
    {
        "id": "sealcase",
        "names": {
            "japanese": "シールいれ",
            "korean": "실상자",
            "french": "Boîte Sceaux",
            "german": "Stick.Koffer",
            "spanish": "Caja Sellos",
            "italian": "Portabolli",
            "english": "Seal Case"
        }
    },
    {
        "id": "fashioncase",
        "names": {
            "japanese": "アクセサリーいれ",
            "korean": "액세서리상자",
            "french": "Coffret Mode",
            "german": "Modekoffer",
            "spanish": "Caja Corazón",
            "italian": "Scatola Chic",
            "english": "Fashion Case"
        }
    },
    {
        "id": "sealbag",
        "names": {
            "japanese": "シールぶくろ",
            "korean": "실주머니",
            "french": "Sac Sceaux",
            "german": "Stickertüte",
            "spanish": "Bolsa Sellos",
            "italian": "Bollosacca",
            "english": "Seal Bag"
        }
    },
    {
        "id": "palpad",
        "names": {
            "japanese": "ともだちてちょう",
            "korean": "친구수첩",
            "french": "Registre Ami",
            "german": "Adressbuch",
            "spanish": "Bloc amigos",
            "italian": "Blocco Amici",
            "english": "Pal Pad"
        }
    },
    {
        "id": "workskey",
        "names": {
            "japanese": "はつでんしょキー",
            "korean": "발전소키",
            "french": "Clé Centrale",
            "german": "K-Schlüssel",
            "spanish": "Llave Central",
            "italian": "Turbinchiave",
            "english": "Works Key"
        }
    },
    {
        "id": "oldcharm",
        "names": {
            "japanese": "こだいのおまもり",
            "korean": "고대의부적",
            "french": "Vieux Grigri",
            "german": "Talisman",
            "spanish": "Talismán",
            "italian": "Arcamuleto",
            "english": "Old Charm"
        }
    },
    {
        "id": "galactickey",
        "names": {
            "japanese": "ギンガだんのカギ",
            "korean": "갤럭시단의열쇠",
            "french": "Clé Galaxie",
            "german": "G-Schlüssel",
            "spanish": "Llave Galaxia",
            "italian": "Galachiave",
            "english": "Galactic Key"
        }
    },
    {
        "id": "redchain",
        "names": {
            "japanese": "あかいくさり",
            "korean": "빨강쇠사슬",
            "french": "Chaîne Rouge",
            "german": "Rote Kette",
            "spanish": "Cadena Roja",
            "italian": "Rossocatena",
            "english": "Red Chain"
        }
    },
    {
        "id": "townmap",
        "names": {
            "japanese": "タウンマップ",
            "korean": "타운맵",
            "french": "Carte",
            "german": "Karte",
            "spanish": "Mapa",
            "italian": "Mappa Città",
            "english": "Town Map"
        }
    },
    {
        "id": "vsseeker",
        "names": {
            "japanese": "バトルサーチャー",
            "korean": "배틀서처",
            "french": "Cherche VS",
            "german": "Kampffahnder",
            "spanish": "Buscapelea",
            "italian": "Cercasfide",
            "english": "Vs. Seeker"
        }
    },
    {
        "id": "coincase",
        "names": {
            "japanese": "コインケース",
            "korean": "동전케이스",
            "french": "Boîte Jetons",
            "german": "Münzkorb",
            "spanish": "Monedero",
            "italian": "Salvadanaio",
            "english": "Coin Case"
        }
    },
    {
        "id": "oldrod",
        "names": {
            "japanese": "ボロのつりざお",
            "korean": "낡은낚싯대",
            "french": "Canne",
            "german": "Angel",
            "spanish": "Caña Vieja",
            "italian": "Amo Vecchio",
            "english": "Old Rod"
        }
    },
    {
        "id": "goodrod",
        "names": {
            "japanese": "いいつりざお",
            "korean": "좋은낚싯대",
            "french": "Super Canne",
            "german": "Profiangel",
            "spanish": "Caña Buena",
            "italian": "Amo Buono",
            "english": "Good Rod"
        }
    },
    {
        "id": "superrod",
        "names": {
            "japanese": "すごいつりざお",
            "korean": "대단한낚싯대",
            "french": "Méga Canne",
            "german": "Superangel",
            "spanish": "Supercaña",
            "italian": "Super Amo",
            "english": "Super Rod"
        }
    },
    {
        "id": "sprayduck",
        "names": {
            "japanese": "コダックじょうろ",
            "korean": "고라파덕물뿌리개",
            "french": "Kwakarrosoir",
            "german": "Entonkanne",
            "spanish": "Psydugadera",
            "italian": "Sprayduck",
            "english": "Sprayduck"
        }
    },
    {
        "id": "poffincase",
        "names": {
            "japanese": "ポフィンケース",
            "korean": "포핀케이스",
            "french": "Boîte Poffin",
            "german": "Knurspbox",
            "spanish": "Pokochera",
            "italian": "Portapoffin",
            "english": "Poffin Case"
        }
    },
    {
        "id": "bicycle",
        "names": {
            "japanese": "じてんしゃ",
            "korean": "자전거",
            "french": "Bicyclette",
            "german": "Fahrrad",
            "spanish": "Bici",
            "italian": "Bicicletta",
            "english": "Bicycle"
        }
    },
    {
        "id": "suitekey",
        "names": {
            "japanese": "ルームキー",
            "korean": "룸키",
            "french": "Clé Chambre",
            "german": "B-Schlüssel",
            "spanish": "Llave Suite",
            "italian": "Chiave Suite",
            "english": "Suite Key"
        }
    },
    {
        "id": "oaksletter",
        "names": {
            "japanese": "オーキドのてがみ",
            "korean": "오박사의편지",
            "french": "Lettre Chen",
            "german": "Eichs Brief",
            "spanish": "Carta Prof. Oak",
            "italian": "Lettera Oak",
            "english": "Oak's Letter"
        }
    },
    {
        "id": "lunarwing",
        "names": {
            "japanese": "みかづきのはね",
            "korean": "초승달날개",
            "french": "Lun'Aile",
            "german": "Lunarfeder",
            "spanish": "Pluma Lunar",
            "italian": "Alalunare",
            "english": "Lunar Wing"
        }
    },
    {
        "id": "membercard",
        "names": {
            "japanese": "メンバーズカード",
            "korean": "멤버스카드",
            "french": "Carte Membre",
            "german": "Mitgl.Karte",
            "spanish": "Carné Socio",
            "italian": "Scheda Soci",
            "english": "Member Card"
        }
    },
    {
        "id": "azureflute",
        "names": {
            "japanese": "てんかいのふえ",
            "korean": "천계의피리",
            "french": "Flûte Azur",
            "german": "Azurflöte",
            "spanish": "Flauta Azur",
            "italian": "Flauto Cielo",
            "english": "Azure Flute"
        }
    },
    {
        "id": "ssticket",
        "names": {
            "japanese": "ふねのチケット",
            "korean": "승선티켓",
            "french": "Passe Bateau",
            "german": "Bootsticket",
            "spanish": "Ticket Barco",
            "italian": "Biglietto Nave",
            "english": "S.S. Ticket"
        }
    },
    {
        "id": "contestpass",
        "names": {
            "japanese": "コンテストパス",
            "korean": "콘테스트패스",
            "french": "Passe Concours",
            "german": "Wettb.-Karte",
            "spanish": "Pase Concurso",
            "italian": "Tessera Gare",
            "english": "Contest Pass"
        }
    },
    {
        "id": "magmastone",
        "names": {
            "japanese": "かざんのおきいし",
            "korean": "화산의돌",
            "french": "Pierre Magma",
            "german": "Magmastein",
            "spanish": "Piedra Magma",
            "italian": "Magmapietra",
            "english": "Magma Stone"
        }
    },
    {
        "id": "parcel",
        "names": {
            "japanese": "おとどけもの",
            "korean": "전해줄물건",
            "french": "Colis",
            "german": "Paket",
            "spanish": "Paquete",
            "italian": "Pacco",
            "english": "Parcel"
        }
    },
    {
        "id": "coupon1",
        "names": {
            "japanese": "ひきかえけん１",
            "korean": "교환권1",
            "french": "Bon 1",
            "german": "Kupon 1",
            "spanish": "Cupón 1",
            "italian": "Coupon 1",
            "english": "Coupon 1"
        }
    },
    {
        "id": "coupon2",
        "names": {
            "japanese": "ひきかえけん２",
            "korean": "교환권2",
            "french": "Bon 2",
            "german": "Kupon 2",
            "spanish": "Cupón 2",
            "italian": "Coupon 2",
            "english": "Coupon 2"
        }
    },
    {
        "id": "coupon3",
        "names": {
            "japanese": "ひきかえけん３",
            "korean": "교환권3",
            "french": "Bon 3",
            "german": "Kupon 3",
            "spanish": "Cupón 3",
            "italian": "Coupon 3",
            "english": "Coupon 3"
        }
    },
    {
        "id": "storagekey",
        "names": {
            "japanese": "そうこのカギ",
            "korean": "창고열쇠",
            "french": "Clé Stockage",
            "german": "L-Schlüssel",
            "spanish": "Llave Almacén",
            "italian": "Depochiave",
            "english": "Storage Key"
        }
    },
    {
        "id": "secretpotion",
        "names": {
            "japanese": "ひでんのくすり",
            "korean": "비전신약",
            "french": "Potion Secrète",
            "german": "Geheimtrank",
            "spanish": "Poción Secreta",
            "italian": "Pozione Segreta",
            "english": "Secret Potion"
        }
    },
    {
        "id": "vsrecorder",
        "names": {
            "japanese": "バトルレコーダー",
            "korean": "배틀레코더",
            "french": "Magnéto VS",
            "german": "Kampfkamera",
            "spanish": "Cámara Lucha",
            "italian": "Registradati",
            "english": "Vs. Recorder"
        }
    },
    {
        "id": "gracidea",
        "names": {
            "japanese": "グラシデアのはな",
            "korean": "그라시데아꽃",
            "french": "Gracidée",
            "german": "Gracidea",
            "spanish": "Gracídea",
            "italian": "Gracidea",
            "english": "Gracidea"
        }
    },
    {
        "id": "secretkey",
        "names": {
            "japanese": "ひみつのカギ",
            "korean": "비밀의열쇠",
            "french": "Clé Secrète",
            "german": "?-Öffner",
            "spanish": "Llave Secreta",
            "italian": "Chiave Segreta",
            "english": "Secret Key"
        }
    },
    {
        "id": "apricornbox",
        "names": {
            "japanese": "ぼんぐりケース",
            "korean": "규토리케이스",
            "french": "Boîte Noigrume",
            "german": "Aprikokobox",
            "spanish": "Caja Bonguri",
            "italian": "Ghicobox",
            "english": "Apricorn Box"
        }
    },
    {
        "id": "berrypots",
        "names": {
            "japanese": "きのみプランター",
            "korean": "나무열매플랜터",
            "french": "Plante-Baies",
            "german": "Pflanzset",
            "spanish": "Plantabayas",
            "italian": "Piantabacche",
            "english": "Berry Pots"
        }
    },
    {
        "id": "squirtbottle",
        "names": {
            "japanese": "ゼニガメじょうろ",
            "korean": "꼬부기물뿌리개",
            "french": "Carapuce à O",
            "german": "Schiggykanne",
            "spanish": "Regadera",
            "italian": "Annaffiatoio",
            "english": "Squirt Bottle"
        }
    },
    {
        "id": "redapricorn",
        "names": {
            "japanese": "あかぼんぐり",
            "korean": "빨간규토리",
            "french": "Noigrume Rge",
            "german": "Aprikoko Rot",
            "spanish": "Bonguri Rojo",
            "italian": "Ghicocca Rossa",
            "english": "Red Apricorn"
        }
    },
    {
        "id": "blueapricorn",
        "names": {
            "japanese": "あおぼんぐり",
            "korean": "파란규토리",
            "french": "Noigrume Blu",
            "german": "Aprikoko Blu",
            "spanish": "Bonguri Azul",
            "italian": "Ghicocca Blu",
            "english": "Blue Apricorn"
        }
    },
    {
        "id": "yellowapricorn",
        "names": {
            "japanese": "きぼんぐり",
            "korean": "노랑규토리",
            "french": "Noigrume Jne",
            "german": "Aprikoko Glb",
            "spanish": "Bonguri Amarillo",
            "italian": "Ghicocca Gialla",
            "english": "Yellow Apricorn"
        }
    },
    {
        "id": "greenapricorn",
        "names": {
            "japanese": "みどぼんぐり",
            "korean": "초록규토리",
            "french": "Noigrume Ver",
            "german": "Aprikoko Grn",
            "spanish": "Bonguri Verde",
            "italian": "Ghicocca Verde",
            "english": "Green Apricorn"
        }
    },
    {
        "id": "pinkapricorn",
        "names": {
            "japanese": "ももぼんぐり",
            "korean": "담홍규토리",
            "french": "Noigrume Ros",
            "german": "Aprikoko Pnk",
            "spanish": "Bonguri Rosa",
            "italian": "Ghicocca Rosa",
            "english": "Pink Apricorn"
        }
    },
    {
        "id": "whiteapricorn",
        "names": {
            "japanese": "しろぼんぐり",
            "korean": "하얀규토리",
            "french": "Noigrume Blc",
            "german": "Aprikoko Wss",
            "spanish": "Bonguri Blanco",
            "italian": "Ghicocca Bianca",
            "english": "White Apricorn"
        }
    },
    {
        "id": "blackapricorn",
        "names": {
            "japanese": "くろぼんぐり",
            "korean": "검은규토리",
            "french": "Noigrume Noi",
            "german": "Aprikoko Swz",
            "spanish": "Bonguri Negro",
            "italian": "Ghicocca Nera",
            "english": "Black Apricorn"
        }
    },
    {
        "id": "dowsingmachine",
        "names": {
            "japanese": "ダウジングマシン",
            "korean": "다우징머신",
            "french": "Cherch'Objet",
            "german": "Itemradar",
            "spanish": "Zahorí",
            "italian": "Ricerca Strum.",
            "english": "Dowsing Machine"
        }
    },
    {
        "id": "ragecandybar",
        "names": {
            "japanese": "いかりまんじゅう",
            "korean": "분노의호두과자",
            "french": "Bonbon Rage",
            "german": "Wutkeks",
            "spanish": "Caramelo Furia",
            "italian": "Iramella",
            "english": "Rage Candy Bar"
        }
    },
    {
        "id": "jadeorb",
        "names": {
            "japanese": "もえぎいろのたま",
            "korean": "연둣빛구슬",
            "french": "Orbe Vert",
            "german": "Grüne Kugel",
            "spanish": "Esfera Verde",
            "italian": "Sfera Verde",
            "english": "Jade Orb"
        }
    },
    {
        "id": "enigmastone",
        "names": {
            "japanese": "なぞのすいしょう",
            "korean": "수수께끼의수정",
            "french": "Mystécristal",
            "german": "Mytokristall",
            "spanish": "Misticristal",
            "italian": "Misticristal",
            "english": "Enigma Stone"
        }
    },
    {
        "id": "unownreport",
        "names": {
            "japanese": "アンノーンノート",
            "korean": "안농노트",
            "french": "Carnet Zarbi",
            "german": "Icognitoheft",
            "spanish": "Bloc Unown",
            "italian": "UnownBloc",
            "english": "Unown Report"
        }
    },
    {
        "id": "bluecard",
        "names": {
            "japanese": "ブルーカード",
            "korean": "블루카드",
            "french": "Carte Bleue",
            "german": "Blaue Karte",
            "spanish": "Tarjeta Azul",
            "italian": "Carta Blu",
            "english": "Blue Card"
        }
    },
    {
        "id": "slowpoketail",
        "names": {
            "japanese": "おいしいシッポ",
            "korean": "맛있는꼬리",
            "french": "Queueramolos",
            "german": "Flegmon-Rute",
            "spanish": "Colaslowpoke",
            "italian": "Codaslowpoke",
            "english": "Slowpoke Tail"
        }
    },
    {
        "id": "clearbell",
        "names": {
            "japanese": "とうめいなスズ",
            "korean": "크리스탈방울",
            "french": "Glas Transparent",
            "german": "Klarglocke",
            "spanish": "Campana Clara",
            "italian": "Campana Chiara",
            "english": "Clear Bell"
        }
    },
    {
        "id": "cardkey",
        "names": {
            "japanese": "カードキー",
            "korean": "카드키",
            "french": "Carte Magnétique",
            "german": "Türöffner",
            "spanish": "Llave Magnética",
            "italian": "Apriporta",
            "english": "Card Key"
        }
    },
    {
        "id": "basementkey",
        "names": {
            "japanese": "ちかのかぎ",
            "korean": "지하의열쇠",
            "french": "Clé Sous-Sol",
            "german": "Kelleröffner",
            "spanish": "Llave Sótano",
            "italian": "Chiave Sotterr.",
            "english": "Basement Key"
        }
    },
    {
        "id": "redscale",
        "names": {
            "japanese": "あかいウロコ",
            "korean": "빨간비늘",
            "french": "Écaillerouge",
            "german": "Rote Haut",
            "spanish": "Escama Roja",
            "italian": "Squama Rossa",
            "english": "Red Scale"
        }
    },
    {
        "id": "lostitem",
        "names": {
            "japanese": "おとしもの",
            "korean": "분실물",
            "french": "Objet perdu",
            "german": "Fundsache",
            "spanish": "Objeto Perdido",
            "italian": "Strumento perso",
            "english": "Lost Item"
        }
    },
    {
        "id": "pass",
        "names": {
            "japanese": "リニアパス",
            "korean": "리니어패스",
            "french": "Passe Train",
            "german": "Fahrschein",
            "spanish": "Magnetopase",
            "italian": "Superpass",
            "english": "Pass"
        }
    },
    {
        "id": "machinepart",
        "names": {
            "japanese": "きかいのぶひん",
            "korean": "기계부품",
            "french": "Partie Machine",
            "german": "Spule",
            "spanish": "Maquinaria",
            "italian": "Pezzo macch.",
            "english": "Machine Part"
        }
    },
    {
        "id": "silverwing",
        "names": {
            "japanese": "ぎんいろのはね",
            "korean": "은빛날개",
            "french": "Argent'Aile",
            "german": "Silberflügel",
            "spanish": "Ala Plateada",
            "italian": "Aladargento",
            "english": "Silver Wing"
        }
    },
    {
        "id": "rainbowwing",
        "names": {
            "japanese": "にじいろのはね",
            "korean": "무지갯빛날개",
            "french": "Arcenci'Aile",
            "german": "Buntschwinge",
            "spanish": "Ala Arcoíris",
            "italian": "Ala d'Iride",
            "english": "Rainbow Wing"
        }
    },
    {
        "id": "mysteryegg",
        "names": {
            "japanese": "ふしぎなタマゴ",
            "korean": "이상한알",
            "french": "Œuf Mystère",
            "german": "Rätsel-Ei",
            "spanish": "Huevo Misterioso",
            "italian": "Uovo Mistero",
            "english": "Mystery Egg"
        }
    },
    {
        "id": "gbsounds",
        "names": {
            "japanese": "ＧＢプレイヤー",
            "korean": "GB플레이어",
            "french": "Lecteur GB",
            "german": "GB-Player",
            "spanish": "Lector GB",
            "italian": "Lettore GB",
            "english": "GB Sounds"
        }
    },
    {
        "id": "tidalbell",
        "names": {
            "japanese": "うみなりのスズ",
            "korean": "해명의방울",
            "french": "Glas Tempête",
            "german": "Gischtglocke",
            "spanish": "Campana Oleaje",
            "italian": "Campana Onda",
            "english": "Tidal Bell"
        }
    },
    {
        "id": "datacard01",
        "names": {
            "japanese": "データカード０１",
            "korean": "데이터카드01",
            "french": "Carte Mémo01",
            "german": "Datenkarte01",
            "spanish": "Tarjeta Datos 01",
            "italian": "Scheda Dati 01",
            "english": "Data Card 01"
        }
    },
    {
        "id": "datacard02",
        "names": {
            "japanese": "データカード０２",
            "korean": "데이터카드02",
            "french": "Carte Mémo02",
            "german": "Datenkarte02",
            "spanish": "Tarjeta Datos 02",
            "italian": "Scheda Dati 02",
            "english": "Data Card 02"
        }
    },
    {
        "id": "datacard03",
        "names": {
            "japanese": "データカード０３",
            "korean": "데이터카드03",
            "french": "Carte Mémo03",
            "german": "Datenkarte03",
            "spanish": "Tarjeta Datos 03",
            "italian": "Scheda Dati 03",
            "english": "Data Card 03"
        }
    },
    {
        "id": "datacard04",
        "names": {
            "japanese": "データカード０４",
            "korean": "데이터카드04",
            "french": "Carte Mémo04",
            "german": "Datenkarte04",
            "spanish": "Tarjeta Datos 04",
            "italian": "Scheda Dati 04",
            "english": "Data Card 04"
        }
    },
    {
        "id": "datacard05",
        "names": {
            "japanese": "データカード０５",
            "korean": "데이터카드05",
            "french": "Carte Mémo05",
            "german": "Datenkarte05",
            "spanish": "Tarjeta Datos 05",
            "italian": "Scheda Dati 05",
            "english": "Data Card 05"
        }
    },
    {
        "id": "datacard06",
        "names": {
            "japanese": "データカード０６",
            "korean": "데이터카드06",
            "french": "Carte Mémo06",
            "german": "Datenkarte06",
            "spanish": "Tarjeta Datos 06",
            "italian": "Scheda Dati 06",
            "english": "Data Card 06"
        }
    },
    {
        "id": "datacard07",
        "names": {
            "japanese": "データカード０７",
            "korean": "데이터카드07",
            "french": "Carte Mémo07",
            "german": "Datenkarte07",
            "spanish": "Tarjeta Datos 07",
            "italian": "Scheda Dati 07",
            "english": "Data Card 07"
        }
    },
    {
        "id": "datacard08",
        "names": {
            "japanese": "データカード０８",
            "korean": "데이터카드08",
            "french": "Carte Mémo08",
            "german": "Datenkarte08",
            "spanish": "Tarjeta Datos 08",
            "italian": "Scheda Dati 08",
            "english": "Data Card 08"
        }
    },
    {
        "id": "datacard09",
        "names": {
            "japanese": "データカード０９",
            "korean": "데이터카드09",
            "french": "Carte Mémo09",
            "german": "Datenkarte09",
            "spanish": "Tarjeta Datos 09",
            "italian": "Scheda Dati 09",
            "english": "Data Card 09"
        }
    },
    {
        "id": "datacard10",
        "names": {
            "japanese": "データカード１０",
            "korean": "데이터카드10",
            "french": "Carte Mémo10",
            "german": "Datenkarte10",
            "spanish": "Tarjeta Datos 10",
            "italian": "Scheda Dati 10",
            "english": "Data Card 10"
        }
    },
    {
        "id": "datacard11",
        "names": {
            "japanese": "データカード１１",
            "korean": "데이터카드11",
            "french": "Carte Mémo11",
            "german": "Datenkarte11",
            "spanish": "Tarjeta Datos 11",
            "italian": "Scheda Dati 11",
            "english": "Data Card 11"
        }
    },
    {
        "id": "datacard12",
        "names": {
            "japanese": "データカード１２",
            "korean": "데이터카드12",
            "french": "Carte Mémo12",
            "german": "Datenkarte12",
            "spanish": "Tarjeta Datos 12",
            "italian": "Scheda Dati 12",
            "english": "Data Card 12"
        }
    },
    {
        "id": "datacard13",
        "names": {
            "japanese": "データカード１３",
            "korean": "데이터카드13",
            "french": "Carte Mémo13",
            "german": "Datenkarte13",
            "spanish": "Tarjeta Datos 13",
            "italian": "Scheda Dati 13",
            "english": "Data Card 13"
        }
    },
    {
        "id": "datacard14",
        "names": {
            "japanese": "データカード１４",
            "korean": "데이터카드14",
            "french": "Carte Mémo14",
            "german": "Datenkarte14",
            "spanish": "Tarjeta Datos 14",
            "italian": "Scheda Dati 14",
            "english": "Data Card 14"
        }
    },
    {
        "id": "datacard15",
        "names": {
            "japanese": "データカード１５",
            "korean": "데이터카드15",
            "french": "Carte Mémo15",
            "german": "Datenkarte15",
            "spanish": "Tarjeta Datos 15",
            "italian": "Scheda Dati 15",
            "english": "Data Card 15"
        }
    },
    {
        "id": "datacard16",
        "names": {
            "japanese": "データカード１６",
            "korean": "데이터카드16",
            "french": "Carte Mémo16",
            "german": "Datenkarte16",
            "spanish": "Tarjeta Datos 16",
            "italian": "Scheda Dati 16",
            "english": "Data Card 16"
        }
    },
    {
        "id": "datacard17",
        "names": {
            "japanese": "データカード１７",
            "korean": "데이터카드17",
            "french": "Carte Mémo17",
            "german": "Datenkarte17",
            "spanish": "Tarjeta Datos 17",
            "italian": "Scheda Dati 17",
            "english": "Data Card 17"
        }
    },
    {
        "id": "datacard18",
        "names": {
            "japanese": "データカード１８",
            "korean": "데이터카드18",
            "french": "Carte Mémo18",
            "german": "Datenkarte18",
            "spanish": "Tarjeta Datos 18",
            "italian": "Scheda Dati 18",
            "english": "Data Card 18"
        }
    },
    {
        "id": "datacard19",
        "names": {
            "japanese": "データカード１９",
            "korean": "데이터카드19",
            "french": "Carte Mémo19",
            "german": "Datenkarte19",
            "spanish": "Tarjeta Datos 19",
            "italian": "Scheda Dati 19",
            "english": "Data Card 19"
        }
    },
    {
        "id": "datacard20",
        "names": {
            "japanese": "データカード２０",
            "korean": "데이터카드20",
            "french": "Carte Mémo20",
            "german": "Datenkarte20",
            "spanish": "Tarjeta Datos 20",
            "italian": "Scheda Dati 20",
            "english": "Data Card 20"
        }
    },
    {
        "id": "datacard21",
        "names": {
            "japanese": "データカード２１",
            "korean": "데이터카드21",
            "french": "Carte Mémo21",
            "german": "Datenkarte21",
            "spanish": "Tarjeta Datos 21",
            "italian": "Scheda Dati 21",
            "english": "Data Card 21"
        }
    },
    {
        "id": "datacard22",
        "names": {
            "japanese": "データカード２２",
            "korean": "데이터카드22",
            "french": "Carte Mémo22",
            "german": "Datenkarte22",
            "spanish": "Tarjeta Datos 22",
            "italian": "Scheda Dati 22",
            "english": "Data Card 22"
        }
    },
    {
        "id": "datacard23",
        "names": {
            "japanese": "データカード２３",
            "korean": "데이터카드23",
            "french": "Carte Mémo23",
            "german": "Datenkarte23",
            "spanish": "Tarjeta Datos 23",
            "italian": "Scheda Dati 23",
            "english": "Data Card 23"
        }
    },
    {
        "id": "datacard24",
        "names": {
            "japanese": "データカード２４",
            "korean": "데이터카드24",
            "french": "Carte Mémo24",
            "german": "Datenkarte24",
            "spanish": "Tarjeta Datos 24",
            "italian": "Scheda Dati 24",
            "english": "Data Card 24"
        }
    },
    {
        "id": "datacard25",
        "names": {
            "japanese": "データカード２５",
            "korean": "데이터카드25",
            "french": "Carte Mémo25",
            "german": "Datenkarte25",
            "spanish": "Tarjeta Datos 25",
            "italian": "Scheda Dati 25",
            "english": "Data Card 25"
        }
    },
    {
        "id": "datacard26",
        "names": {
            "japanese": "データカード２６",
            "korean": "데이터카드26",
            "french": "Carte Mémo26",
            "german": "Datenkarte26",
            "spanish": "Tarjeta Datos 26",
            "italian": "Scheda Dati 26",
            "english": "Data Card 26"
        }
    },
    {
        "id": "datacard27",
        "names": {
            "japanese": "データカード２７",
            "korean": "데이터카드27",
            "french": "Carte Mémo27",
            "german": "Datenkarte27",
            "spanish": "Tarjeta Datos 27",
            "italian": "Scheda Dati 27",
            "english": "Data Card 27"
        }
    },
    {
        "id": "lockcapsule",
        "names": {
            "japanese": "ロックカプセル",
            "korean": "록캡슐",
            "french": "Poké Écrin",
            "german": "Tresorkapsel",
            "spanish": "Cápsula Candado",
            "italian": "Capsula Scrigno",
            "english": "Lock Capsule"
        }
    },
    {
        "id": "photoalbum",
        "names": {
            "japanese": "フォトアルバム",
            "korean": "포토앨범",
            "french": "Album Photo",
            "german": "Fotoalbum",
            "spanish": "Álbum",
            "italian": "Album",
            "english": "Photo Album"
        }
    },
    {
        "id": "orangemail",
        "names": {
            "japanese": "オレンジメール",
            "french": "Lettre Oranj",
            "german": "Zigzagbrief",
            "spanish": "Car. Naranja",
            "italian": "Mess. Agrume",
            "english": "Orange Mail"
        }
    },
    {
        "id": "harbormail",
        "names": {
            "japanese": "ハーバーメール",
            "french": "Lettre Port",
            "german": "Hafenbrief",
            "spanish": "Carta Puerto",
            "italian": "Mess. Porto",
            "english": "Harbor Mail"
        }
    },
    {
        "id": "glittermail",
        "names": {
            "japanese": "キラキラメール",
            "french": "Lettre Brill",
            "german": "Glitzerbrief",
            "spanish": "Carta Brillo",
            "italian": "Mess. Luci",
            "english": "Glitter Mail"
        }
    },
    {
        "id": "mechmail",
        "names": {
            "japanese": "メカニカルメール",
            "french": "Lettre Méca",
            "german": "Eilbrief",
            "spanish": "Carta Imán",
            "italian": "Mess. Tecno",
            "english": "Mech Mail"
        }
    },
    {
        "id": "woodmail",
        "names": {
            "japanese": "ウッディメール",
            "french": "Lettre Bois",
            "german": "Waldbrief",
            "spanish": "Carta Madera",
            "italian": "Mess. Bosco",
            "english": "Wood Mail"
        }
    },
    {
        "id": "wavemail",
        "names": {
            "japanese": "クロスメール",
            "french": "Lettre Vague",
            "german": "Wellenbrief",
            "spanish": "Carta Ola",
            "italian": "Mess. Onda",
            "english": "Wave Mail"
        }
    },
    {
        "id": "beadmail",
        "names": {
            "japanese": "トレジャーメール",
            "french": "Lettre Bulle",
            "german": "Perlenbrief",
            "spanish": "Carta Imagen",
            "italian": "Mess. Perle",
            "english": "Bead Mail"
        }
    },
    {
        "id": "shadowmail",
        "names": {
            "japanese": "シャドーメール",
            "french": "Lettre Ombre",
            "german": "Dunkelbrief",
            "spanish": "Carta Sombra",
            "italian": "Mess. Ombra",
            "english": "Shadow Mail"
        }
    },
    {
        "id": "tropicmail",
        "names": {
            "japanese": "トロピカルメール",
            "french": "Lettre Tropi",
            "german": "Tropenbrief",
            "spanish": "Car. Tropic.",
            "italian": "Mess. Tropic",
            "english": "Tropic Mail"
        }
    },
    {
        "id": "dreammail",
        "names": {
            "japanese": "ドリームメール",
            "french": "Lettre Songe",
            "german": "Traumbrief",
            "spanish": "Carta Sueño",
            "italian": "Mess. Sogno",
            "english": "Dream Mail"
        }
    },
    {
        "id": "fabmail",
        "names": {
            "japanese": "ミラクルメール",
            "french": "Lettre Cool",
            "german": "Edelbrief",
            "spanish": "Carta Fab.",
            "italian": "Mess. Lusso",
            "english": "Fab Mail"
        }
    },
    {
        "id": "retromail",
        "names": {
            "japanese": "レトロメール",
            "french": "Lettre Retro",
            "german": "Retrobrief",
            "spanish": "Carta Retro.",
            "italian": "Mess. Rétro",
            "english": "Retro Mail"
        }
    },
    {
        "id": "machbike",
        "names": {
            "japanese": "マッハじてんしゃ",
            "french": "Vélo Course",
            "german": "Eilrad",
            "spanish": "Bici Carrera",
            "italian": "Bici Corsa",
            "english": "Mach Bike"
        }
    },
    {
        "id": "acrobike",
        "names": {
            "japanese": "ダートじてんしゃ",
            "french": "Vélo Cross",
            "german": "Kunstrad",
            "spanish": "Bici Acrob.",
            "italian": "Bici Cross",
            "english": "Acro Bike"
        }
    },
    {
        "id": "wailmerpail",
        "names": {
            "japanese": "ホエルコじょうろ",
            "french": "Seau Wailmer",
            "german": "Wailmerkanne",
            "spanish": "Cubo Wailmer",
            "italian": "Vaso Wailmer",
            "english": "Wailmer Pail"
        }
    },
    {
        "id": "devongoods",
        "names": {
            "japanese": "デボンのにもつ",
            "french": "Pack Devon",
            "german": "Devon-Waren",
            "spanish": "Piezas Devon",
            "italian": "Merce Devon",
            "english": "Devon Goods"
        }
    },
    {
        "id": "sootsack",
        "names": {
            "japanese": "はいぶくろ",
            "french": "Sac à Suie",
            "german": "Aschetasche",
            "spanish": "Saco Hollín",
            "italian": "Sacco Cenere",
            "english": "Soot Sack"
        }
    },
    {
        "id": "pokeblockcase",
        "names": {
            "japanese": "ポロックケース",
            "french": "Boîte Pokéblocs",
            "german": "Pokériegelbox",
            "spanish": "Tubo Pokécubos",
            "italian": "PortaPokémelle",
            "english": "Pokéblock Case"
        }
    },
    {
        "id": "letter",
        "names": {
            "japanese": "ダイゴへのてがみ",
            "french": "Lettre",
            "german": "Brief",
            "spanish": "Carta",
            "italian": "Lettera",
            "english": "Letter"
        }
    },
    {
        "id": "eonticket",
        "names": {
            "japanese": "むげんのチケット",
            "french": "Passe Éon",
            "german": "Äon-Ticket",
            "spanish": "Ticket Eón",
            "italian": "Bigl. Eone",
            "english": "Eon Ticket"
        }
    },
    {
        "id": "scanner",
        "names": {
            "japanese": "たんちき",
            "french": "Scanner",
            "german": "Scanner",
            "spanish": "Escáner",
            "italian": "Scanner",
            "english": "Scanner"
        }
    },
    {
        "id": "gogoggles",
        "names": {
            "japanese": "ゴーゴーゴーグル",
            "french": "Lunet. Sable",
            "german": "Wüstenglas",
            "spanish": "Gaf. Aislan.",
            "italian": "Occhialoni",
            "english": "Go-Goggles"
        }
    },
    {
        "id": "meteorite",
        "names": {
            "japanese": "いんせき",
            "french": "Météorite",
            "german": "Meteorit",
            "spanish": "Meteorito",
            "italian": "Meteorite",
            "english": "Meteorite"
        }
    },
    {
        "id": "rm1key",
        "names": {
            "japanese": "１ごうしつのカギ",
            "french": "Clé Salle 1",
            "german": "K1-Schlüssel",
            "spanish": "Ll. Cabina 1",
            "italian": "Chiave Cab.1",
            "english": "Rm. 1 Key"
        }
    },
    {
        "id": "rm2key",
        "names": {
            "japanese": "２ごうしつのカギ",
            "french": "Clé Salle 2",
            "german": "K2-Schlüssel",
            "spanish": "Ll. Cabina 2",
            "italian": "Chiave Cab.2",
            "english": "Rm. 2 Key"
        }
    },
    {
        "id": "rm4key",
        "names": {
            "japanese": "４ごうしつのカギ",
            "french": "Clé Salle 4",
            "german": "K4-Schlüssel",
            "spanish": "Ll. Cabina 4",
            "italian": "Chiave Cab.4",
            "english": "Rm. 4 Key"
        }
    },
    {
        "id": "rm6key",
        "names": {
            "japanese": "６ごうしつのカギ",
            "french": "Clé Salle 6",
            "german": "K6-Schlüssel",
            "spanish": "Ll. Cabina 6",
            "italian": "Chiave Cab.6",
            "english": "Rm. 6 Key"
        }
    },
    {
        "id": "devonscope",
        "names": {
            "japanese": "デボンスコープ",
            "french": "Devon Scope",
            "german": "Devon-Scope",
            "spanish": "Detec. Devon",
            "italian": "Devonscopio",
            "english": "Devon Scope"
        }
    },
    {
        "id": "oaksparcel",
        "names": {
            "japanese": "おとどけもの",
            "french": "Colis Chen",
            "german": "Eichs Paket",
            "spanish": "Correo-Oak",
            "italian": "Pacco di Oak",
            "english": "Oak's Parcel"
        }
    },
    {
        "id": "pokeflute",
        "names": {
            "japanese": "ポケモンのふえ",
            "korean": "포켓몬피리",
            "french": "Pokéflûte",
            "german": "Pokéflöte",
            "spanish": "Poké Flauta",
            "italian": "Poké Flauto",
            "english": "Poké Flute"
        }
    },
    {
        "id": "bikevoucher",
        "names": {
            "japanese": "ひきかえけん",
            "french": "Bon Commande",
            "german": "Rad-Coupon",
            "spanish": "Bono Bici",
            "italian": "Buono Bici",
            "english": "Bike Voucher"
        }
    },
    {
        "id": "goldteeth",
        "names": {
            "japanese": "きんのいれば",
            "french": "Dent d'Or",
            "german": "Goldzähne",
            "spanish": "Dientes Oro",
            "italian": "Denti d'Oro",
            "english": "Gold Teeth"
        }
    },
    {
        "id": "liftkey",
        "names": {
            "japanese": "エレベータのカギ",
            "french": "Clé Asc.",
            "german": "Liftöffner",
            "spanish": "Ll. Ascensor",
            "italian": "Chiave Asc.",
            "english": "Lift Key"
        }
    },
    {
        "id": "silphscope",
        "names": {
            "japanese": "シルフスコープ",
            "french": "Scope Sylphe",
            "german": "Silph Scope",
            "spanish": "Scope Silph",
            "italian": "Spettrosonda",
            "english": "Silph Scope"
        }
    },
    {
        "id": "famechecker",
        "names": {
            "japanese": "ボイスチェッカー",
            "french": "Memorydex",
            "german": "Ruhmesdatei",
            "spanish": "Memorín",
            "italian": "PokéVIP",
            "english": "Fame Checker"
        }
    },
    {
        "id": "tmcase",
        "names": {
            "japanese": "わざマシンケース",
            "french": "Boîte CT",
            "german": "VM/TM-Box",
            "spanish": "Tubo MT-MO",
            "italian": "Porta-MT",
            "english": "TM Case"
        }
    },
    {
        "id": "berrypouch",
        "names": {
            "japanese": "きのみぶくろ",
            "french": "Sac à Baies",
            "german": "Beerentüte",
            "spanish": "Saco Bayas",
            "italian": "Portabacche",
            "english": "Berry Pouch"
        }
    },
    {
        "id": "teachytv",
        "names": {
            "japanese": "おしえテレビ",
            "french": "TV ABC",
            "german": "Lehrkanal",
            "spanish": "Poké Tele",
            "italian": "Pokétivù",
            "english": "Teachy TV"
        }
    },
    {
        "id": "tripass",
        "names": {
            "japanese": "トライパス",
            "french": "Tri-Passe",
            "german": "Tri-Pass",
            "spanish": "Tri-Ticket",
            "italian": "Tris Pass",
            "english": "Tri-Pass"
        }
    },
    {
        "id": "rainbowpass",
        "names": {
            "japanese": "レインボーパス",
            "french": "Passe Prisme",
            "german": "Bunt-Pass",
            "spanish": "Iris-Ticket",
            "italian": "Sette Pass",
            "english": "Rainbow Pass"
        }
    },
    {
        "id": "tea",
        "names": {
            "japanese": "おちゃ",
            "french": "Thé",
            "german": "Tee",
            "spanish": "Té",
            "italian": "Tè",
            "english": "Tea"
        }
    },
    {
        "id": "mysticticket",
        "names": {
            "japanese": "しんぴのチケット",
            "french": "Ticketmystik",
            "german": "Geheimticket",
            "spanish": "Misti-Ticket",
            "italian": "Bigl. Magico",
            "english": "MysticTicket"
        }
    },
    {
        "id": "auroraticket",
        "names": {
            "japanese": "オーロラチケット",
            "french": "Ticketaurora",
            "german": "Auroraticket",
            "spanish": "Ori-Ticket",
            "italian": "Bigl. Aurora",
            "english": "AuroraTicket"
        }
    },
    {
        "id": "powderjar",
        "names": {
            "japanese": "こないれ",
            "french": "Pot Poudre",
            "german": "Puderdöschen",
            "spanish": "Bote Polvos",
            "italian": "Portafarina",
            "english": "Powder Jar"
        }
    },
    {
        "id": "ruby",
        "names": {
            "japanese": "ルビー",
            "french": "Rubis",
            "german": "Rubin",
            "spanish": "Rubí",
            "italian": "Rubino",
            "english": "Ruby"
        }
    },
    {
        "id": "sapphire",
        "names": {
            "japanese": "サファイア",
            "french": "Saphir",
            "german": "Saphir",
            "spanish": "Zafiro",
            "italian": "Zaffiro",
            "english": "Sapphire"
        }
    },
    {
        "id": "magmaemblem",
        "names": {
            "japanese": "マグマのしるし",
            "french": "Sceau Magma",
            "german": "Magmaemblem",
            "spanish": "Signo Magma",
            "italian": "Stemma Magma",
            "english": "Magma Emblem"
        }
    },
    {
        "id": "oldseamap",
        "names": {
            "japanese": "ふるびたかいず",
            "french": "Vieillecarte",
            "german": "Alte Karte",
            "spanish": "Mapa Viejo",
            "italian": "Mappa Stinta",
            "english": "Old Sea Map"
        }
    },
    {
        "id": "sweetheart",
        "names": {
            "japanese": "ハートスイーツ",
            "korean": "하트스위트",
            "french": "Chococœur",
            "german": "Herzkonfekt",
            "spanish": "Corazón Dulce",
            "italian": "Dolcecuore",
            "english": "Sweet Heart"
        }
    },
    {
        "id": "greetmail",
        "names": {
            "japanese": "はじめてメール",
            "korean": "첫메일",
            "french": "Lettre Salut",
            "german": "Grußbrief",
            "spanish": "Carta Inicial",
            "italian": "Messaggio Inizio",
            "english": "Greet Mail"
        }
    },
    {
        "id": "favoredmail",
        "names": {
            "japanese": "だいすきメール",
            "korean": "애호메일",
            "french": "Lettre Fan",
            "german": "Faiblebrief",
            "spanish": "Carta Favoritos",
            "italian": "Messaggio TVB",
            "english": "Favored Mail"
        }
    },
    {
        "id": "rsvpmail",
        "names": {
            "japanese": "おさそいメール",
            "korean": "권유메일",
            "french": "Lettre Invit",
            "german": "Einladebrief",
            "spanish": "Carta Invitar",
            "italian": "Messaggio Invito",
            "english": "RSVP Mail"
        }
    },
    {
        "id": "thanksmail",
        "names": {
            "japanese": "かんしゃメール",
            "korean": "감사메일",
            "french": "Lettre Merci",
            "german": "Dankesbrief",
            "spanish": "Carta Gracias",
            "italian": "Messaggio Grazie",
            "english": "Thanks Mail"
        }
    },
    {
        "id": "inquirymail",
        "names": {
            "japanese": "しつもんメール",
            "korean": "질문메일",
            "french": "Lettre Demande",
            "german": "Fragebrief",
            "spanish": "Carta Pregunta",
            "italian": "Messaggio Chiedi",
            "english": "Inquiry Mail"
        }
    },
    {
        "id": "likemail",
        "names": {
            "japanese": "おすすめメール",
            "korean": "추천메일",
            "french": "Lettre Avis",
            "german": "Insiderbrief",
            "spanish": "Carta Gustos",
            "italian": "Mess. Sugg.",
            "english": "Like Mail"
        }
    },
    {
        "id": "replymail",
        "names": {
            "japanese": "おかえしメール",
            "korean": "답장메일",
            "french": "Lettre Réponse",
            "german": "Rückbrief",
            "spanish": "Carta Respuesta",
            "italian": "Mess. Risp.",
            "english": "Reply Mail"
        }
    },
    {
        "id": "bridgemails",
        "names": {
            "japanese": "ブリッジメールＳ",
            "korean": "브리지메일S",
            "french": "Lettre PontS",
            "german": "Brückbrief H",
            "spanish": "Carta Puente S",
            "italian": "Mess. Frec.",
            "english": "Bridge Mail S"
        }
    },
    {
        "id": "bridgemaild",
        "names": {
            "japanese": "ブリッジメールＨ",
            "korean": "브리지메일M",
            "french": "Lettre PontY",
            "german": "Brückbrief M",
            "spanish": "Carta Puente F",
            "italian": "Mess. Libec.",
            "english": "Bridge Mail D"
        }
    },
    {
        "id": "bridgemailt",
        "names": {
            "japanese": "ブリッジメールＣ",
            "korean": "브리지메일C",
            "french": "Lettre PontF",
            "german": "Brückbrief Z",
            "spanish": "Carta Puente A",
            "italian": "Mess. Prop.",
            "english": "Bridge Mail T"
        }
    },
    {
        "id": "bridgemailv",
        "names": {
            "japanese": "ブリッジメールＶ",
            "korean": "브리지메일V",
            "french": "Lettre PontH",
            "german": "Brückbrief D",
            "spanish": "Carta Puente V",
            "italian": "Mess. Vill.",
            "english": "Bridge Mail V"
        }
    },
    {
        "id": "bridgemailm",
        "names": {
            "japanese": "ブリッジメールＷ",
            "korean": "브리지메일W",
            "french": "Lettre PontI",
            "german": "Brückbrief W",
            "spanish": "Carta Puente P",
            "italian": "Mess. Merav.",
            "english": "Bridge Mail M"
        }
    },
    {
        "id": "prismscale",
        "names": {
            "japanese": "きれいなウロコ",
            "korean": "고운비늘",
            "french": "Bel'Écaille",
            "german": "Schönschuppe",
            "spanish": "Escama Bella",
            "italian": "Squama Bella",
            "english": "Prism Scale"
        }
    },
    {
        "id": "healthwing",
        "names": {
            "japanese": "たいりょくのハネ",
            "korean": "체력날개",
            "french": "Aile Santé",
            "german": "Heilfeder",
            "spanish": "Pluma Vigor",
            "italian": "Piumsalute",
            "english": "Health Wing"
        }
    },
    {
        "id": "musclewing",
        "names": {
            "japanese": "きんりょくのハネ",
            "korean": "근력날개",
            "french": "Aile Force",
            "german": "Kraftfeder",
            "spanish": "Pluma Músculo",
            "italian": "Piumpotenza",
            "english": "Muscle Wing"
        }
    },
    {
        "id": "resistwing",
        "names": {
            "japanese": "ていこうのハネ",
            "korean": "저항력날개",
            "french": "Aile Armure",
            "german": "Abwehrfeder",
            "spanish": "Pluma Aguante",
            "italian": "Piumtutela",
            "english": "Resist Wing"
        }
    },
    {
        "id": "geniuswing",
        "names": {
            "japanese": "ちりょくのハネ",
            "korean": "지력날개",
            "french": "Aile Esprit",
            "german": "Geniefeder",
            "spanish": "Pluma Intelecto",
            "italian": "Piumingegno",
            "english": "Genius Wing"
        }
    },
    {
        "id": "cleverwing",
        "names": {
            "japanese": "せいしんのハネ",
            "korean": "정신력날개",
            "french": "Aile Mental",
            "german": "Espritfeder",
            "spanish": "Pluma Mente",
            "italian": "Piumintuito",
            "english": "Clever Wing"
        }
    },
    {
        "id": "swiftwing",
        "names": {
            "japanese": "しゅんぱつのハネ",
            "korean": "순발력날개",
            "french": "Aile Sprint",
            "german": "Flinkfeder",
            "spanish": "Pluma Ímpetu",
            "italian": "Piumreazione",
            "english": "Swift Wing"
        }
    },
    {
        "id": "prettywing",
        "names": {
            "japanese": "きれいなハネ",
            "korean": "고운날개",
            "french": "Jolie Aile",
            "german": "Prachtfeder",
            "spanish": "Pluma Bella",
            "italian": "Piumabella",
            "english": "Pretty Wing"
        }
    },
    {
        "id": "libertypass",
        "names": {
            "japanese": "リバティチケット",
            "korean": "리버티티켓",
            "french": "Pass Liberté",
            "german": "Gartenpass",
            "spanish": "Tarjeta Libertad",
            "italian": "Liberticket",
            "english": "Liberty Pass"
        }
    },
    {
        "id": "passorb",
        "names": {
            "japanese": "デルダマ",
            "korean": "딜구슬",
            "french": "Offrisphère",
            "german": "Transferorb",
            "spanish": "Regalosfera",
            "italian": "Passabilia",
            "english": "Pass Orb"
        }
    },
    {
        "id": "poketoy",
        "names": {
            "japanese": "ポケじゃらし",
            "korean": "포켓풀",
            "french": "Poképlumet",
            "german": "Pokéwedel",
            "spanish": "Pokéseñuelo",
            "italian": "Pokégingillo",
            "english": "Poké Toy"
        }
    },
    {
        "id": "propcase",
        "names": {
            "japanese": "グッズケース",
            "korean": "굿즈케이스",
            "french": "Boîte Parure",
            "german": "Deko-Box",
            "spanish": "Neceser",
            "italian": "Portagadget",
            "english": "Prop Case"
        }
    },
    {
        "id": "dragonskull",
        "names": {
            "japanese": "ドラゴンのホネ",
            "korean": "드래곤의뼈",
            "french": "Crâne Dragon",
            "german": "Drakoschädel",
            "spanish": "Cráneo Dragón",
            "italian": "Teschio",
            "english": "Dragon Skull"
        }
    },
    {
        "id": "balmmushroom",
        "names": {
            "japanese": "かおるキノコ",
            "korean": "향기버섯",
            "french": "Champi Suave",
            "german": "Duftpilz",
            "spanish": "Seta Aroma",
            "italian": "Profumfungo",
            "english": "Balm Mushroom"
        }
    },
    {
        "id": "bignugget",
        "names": {
            "japanese": "でかいきんのたま",
            "korean": "큰금구슬",
            "french": "Maxi Pépite",
            "german": "Riesennugget",
            "spanish": "Maxipepita",
            "italian": "Granpepita",
            "english": "Big Nugget"
        }
    },
    {
        "id": "pearlstring",
        "names": {
            "japanese": "おだんごしんじゅ",
            "korean": "경단진주",
            "french": "Perle Triple",
            "german": "Triperle",
            "spanish": "Sarta Perlas",
            "italian": "Trittiperla",
            "english": "Pearl String"
        }
    },
    {
        "id": "cometshard",
        "names": {
            "japanese": "すいせいのかけら",
            "korean": "혜성조각",
            "french": "Morceau Comète",
            "german": "Kometstück",
            "spanish": "Fragmento Cometa",
            "italian": "Pezzo Cometa",
            "english": "Comet Shard"
        }
    },
    {
        "id": "reliccopper",
        "names": {
            "japanese": "こだいのどうか",
            "korean": "고대의동화",
            "french": "Vieux Sou",
            "german": "Alter Heller",
            "spanish": "Real Cobre",
            "italian": "Soldantico",
            "english": "Relic Copper"
        }
    },
    {
        "id": "relicsilver",
        "names": {
            "japanese": "こだいのぎんか",
            "korean": "고대의은화",
            "french": "Vieil Écu",
            "german": "Alter Taler",
            "spanish": "Real Plata",
            "italian": "Ducatantico",
            "english": "Relic Silver"
        }
    },
    {
        "id": "relicgold",
        "names": {
            "japanese": "こだいのきんか",
            "korean": "고대의금화",
            "french": "Vieux Ducat",
            "german": "Alter Dukat",
            "spanish": "Real Oro",
            "italian": "Doblonantico",
            "english": "Relic Gold"
        }
    },
    {
        "id": "relicvase",
        "names": {
            "japanese": "こだいのツボ",
            "korean": "고대의항아리",
            "french": "Vieux Vase",
            "german": "Alte Vase",
            "spanish": "Ánfora",
            "italian": "Vasantico",
            "english": "Relic Vase"
        }
    },
    {
        "id": "relicband",
        "names": {
            "japanese": "こだいのうでわ",
            "korean": "고대의팔찌",
            "french": "Vieux Bijou",
            "german": "Alter Reif",
            "spanish": "Brazal",
            "italian": "Bracciantico",
            "english": "Relic Band"
        }
    },
    {
        "id": "relicstatue",
        "names": {
            "japanese": "こだいのせきぞう",
            "korean": "고대의석상",
            "french": "Vieux Santon",
            "german": "Alte Statue",
            "spanish": "Efigie Antigua",
            "italian": "Statuantica",
            "english": "Relic Statue"
        }
    },
    {
        "id": "reliccrown",
        "names": {
            "japanese": "こだいのおうかん",
            "korean": "고대의왕관",
            "french": "Vieux Tortil",
            "german": "Alte Krone",
            "spanish": "Corona Antigua",
            "italian": "Coronantica",
            "english": "Relic Crown"
        }
    },
    {
        "id": "casteliacone",
        "names": {
            "japanese": "ヒウンアイス",
            "korean": "구름아이스",
            "french": "Glace Volute",
            "german": "Stratos-Eis",
            "spanish": "Porcehelado",
            "italian": "Conostropoli",
            "english": "Casteliacone"
        }
    },
    {
        "id": "direhit2",
        "names": {
            "japanese": "クリティカット２",
            "korean": "크리티컬커터2",
            "french": "Muscle 2",
            "german": "Angriffplus2",
            "spanish": "Directo 2",
            "italian": "Supercolpo 2",
            "english": "Dire Hit 2"
        }
    },
    {
        "id": "xspeed2",
        "names": {
            "japanese": "スピーダー２",
            "korean": "스피드업2",
            "french": "Vitesse 2",
            "german": "X-Tempo2",
            "spanish": "Velocidad X 2",
            "italian": "Velocità X-2",
            "english": "X Speed 2"
        }
    },
    {
        "id": "xspatk2",
        "names": {
            "japanese": "ＳＰアップ２",
            "korean": "스페셜업2",
            "french": "Atq. Spé. 2",
            "german": "X-Spezial2",
            "spanish": "Especial X 2",
            "italian": "Special X-2",
            "english": "X Sp. Atk 2"
        }
    },
    {
        "id": "xspdef2",
        "names": {
            "japanese": "ＳＰガード２",
            "korean": "스페셜가드2",
            "french": "Déf. Spé. 2",
            "german": "X-Sp. Ver.2",
            "spanish": "Def. Esp. X 2",
            "italian": "Dif. Spec. X-2",
            "english": "X Sp. Def 2"
        }
    },
    {
        "id": "xdefense2",
        "names": {
            "japanese": "ディフェンダー２",
            "korean": "디펜드업2",
            "french": "Défense 2",
            "german": "X-Abwehr2",
            "spanish": "Defensa X 2",
            "italian": "Difesa X-2",
            "english": "X Defense 2"
        }
    },
    {
        "id": "xattack2",
        "names": {
            "japanese": "プラスパワー２",
            "korean": "플러스파워2",
            "french": "Attaque 2",
            "german": "X-Angriff2",
            "spanish": "Ataque X 2",
            "italian": "Attacco X-2",
            "english": "X Attack 2"
        }
    },
    {
        "id": "xaccuracy2",
        "names": {
            "japanese": "ヨクアタール２",
            "korean": "잘-맞히기2",
            "french": "Précision 2",
            "german": "X-Treffer2",
            "spanish": "Precisión X 2",
            "italian": "Precisione X-2",
            "english": "X Accuracy 2"
        }
    },
    {
        "id": "xspeed3",
        "names": {
            "japanese": "スピーダー３",
            "korean": "스피드업3",
            "french": "Vitesse 3",
            "german": "X-Tempo3",
            "spanish": "Velocidad X 3",
            "italian": "Velocità X-3",
            "english": "X Speed 3"
        }
    },
    {
        "id": "xspatk3",
        "names": {
            "japanese": "ＳＰアップ３",
            "korean": "스페셜업3",
            "french": "Atq. Spé. 3",
            "german": "X-Spezial3",
            "spanish": "Especial X 3",
            "italian": "Special X-3",
            "english": "X Sp. Atk 3"
        }
    },
    {
        "id": "xspdef3",
        "names": {
            "japanese": "ＳＰガード３",
            "korean": "스페셜가드3",
            "french": "Déf. Spé. 3",
            "german": "X-Sp. Ver.3",
            "spanish": "Def. Esp. X 3",
            "italian": "Dif. Spec. X-3",
            "english": "X Sp. Def 3"
        }
    },
    {
        "id": "xdefense3",
        "names": {
            "japanese": "ディフェンダー３",
            "korean": "디펜드업3",
            "french": "Défense 3",
            "german": "X-Abwehr3",
            "spanish": "Defensa X 3",
            "italian": "Difesa X-3",
            "english": "X Defense 3"
        }
    },
    {
        "id": "xattack3",
        "names": {
            "japanese": "プラスパワー３",
            "korean": "플러스파워3",
            "french": "Attaque 3",
            "german": "X-Angriff3",
            "spanish": "Ataque X 3",
            "italian": "Attacco X-3",
            "english": "X Attack 3"
        }
    },
    {
        "id": "xaccuracy3",
        "names": {
            "japanese": "ヨクアタール３",
            "korean": "잘-맞히기3",
            "french": "Précision 3",
            "german": "X-Treffer3",
            "spanish": "Precisión X 3",
            "italian": "Precisione X-3",
            "english": "X Accuracy 3"
        }
    },
    {
        "id": "xspeed6",
        "names": {
            "japanese": "スピーダー６",
            "korean": "스피드업6",
            "french": "Vitesse 6",
            "german": "X-Tempo6",
            "spanish": "Velocidad X 6",
            "italian": "Velocità X-6",
            "english": "X Speed 6"
        }
    },
    {
        "id": "xspatk6",
        "names": {
            "japanese": "ＳＰアップ６",
            "korean": "스페셜업6",
            "french": "Atq. Spé. 6",
            "german": "X-Spezial6",
            "spanish": "Especial X 6",
            "italian": "Special X-6",
            "english": "X Sp. Atk 6"
        }
    },
    {
        "id": "xspdef6",
        "names": {
            "japanese": "ＳＰガード６",
            "korean": "스페셜가드6",
            "french": "Déf. Spé. 6",
            "german": "X-Sp. Ver.6",
            "spanish": "Def. Esp. X 6",
            "italian": "Dif. Spec. X-6",
            "english": "X Sp. Def 6"
        }
    },
    {
        "id": "xdefense6",
        "names": {
            "japanese": "ディフェンダー６",
            "korean": "디펜드업6",
            "french": "Défense 6",
            "german": "X-Abwehr6",
            "spanish": "Defensa X 6",
            "italian": "Difesa X-6",
            "english": "X Defense 6"
        }
    },
    {
        "id": "xattack6",
        "names": {
            "japanese": "プラスパワー６",
            "korean": "플러스파워6",
            "french": "Attaque 6",
            "german": "X-Angriff6",
            "spanish": "Ataque X 6",
            "italian": "Attacco X-6",
            "english": "X Attack 6"
        }
    },
    {
        "id": "xaccuracy6",
        "names": {
            "japanese": "ヨクアタール６",
            "korean": "잘-맞히기6",
            "french": "Précision 6",
            "german": "X-Treffer6",
            "spanish": "Precisión X 6",
            "italian": "Precisione X-6",
            "english": "X Accuracy 6"
        }
    },
    {
        "id": "abilityurge",
        "names": {
            "japanese": "スキルコール",
            "korean": "스킬콜",
            "french": "Appel Talent",
            "german": "Fäh.-Appell",
            "spanish": "Habilitador",
            "italian": "Chiamabilità",
            "english": "Ability Urge"
        }
    },
    {
        "id": "itemdrop",
        "names": {
            "japanese": "アイテムドロップ",
            "korean": "아이템드롭",
            "french": "Jette Objet",
            "german": "Itemabwurf",
            "spanish": "Tiraobjeto",
            "italian": "Lascioggetto",
            "english": "Item Drop"
        }
    },
    {
        "id": "itemurge",
        "names": {
            "japanese": "アイテムコール",
            "korean": "아이템콜",
            "french": "Appel Objet",
            "german": "Itemappell",
            "spanish": "Activaobjeto",
            "italian": "Chiamoggetto",
            "english": "Item Urge"
        }
    },
    {
        "id": "reseturge",
        "names": {
            "japanese": "フラットコール",
            "korean": "플랫콜",
            "french": "Réamorçage",
            "german": "Umkehrappell",
            "spanish": "Quitaestado",
            "italian": "Ripristino",
            "english": "Reset Urge"
        }
    },
    {
        "id": "direhit3",
        "names": {
            "japanese": "クリティカット３",
            "korean": "크리티컬커터3",
            "french": "Muscle 3",
            "german": "Angriffplus3",
            "spanish": "Directo 3",
            "italian": "Supercolpo 3",
            "english": "Dire Hit 3"
        }
    },
    {
        "id": "lightstone",
        "names": {
            "japanese": "ライトストーン",
            "korean": "라이트스톤",
            "french": "Galet Blanc",
            "german": "Lichtstein",
            "spanish": "Orbe Claro",
            "italian": "Chiarolite",
            "english": "Light Stone"
        }
    },
    {
        "id": "darkstone",
        "names": {
            "japanese": "ダークストーン",
            "korean": "다크스톤",
            "french": "Galet Noir",
            "german": "Dunkelstein",
            "spanish": "Orbe Oscuro",
            "italian": "Scurolite",
            "english": "Dark Stone"
        }
    },
    {
        "id": "tm93",
        "names": {
            "japanese": "わざマシン９３",
            "korean": "기술머신93",
            "french": "CT93",
            "german": "TM93",
            "spanish": "MT93",
            "italian": "MT93",
            "english": "TM93"
        }
    },
    {
        "id": "tm94",
        "names": {
            "japanese": "わざマシン９４",
            "korean": "기술머신94",
            "french": "CT94",
            "german": "TM94",
            "spanish": "MT94",
            "italian": "MT94",
            "english": "TM94"
        }
    },
    {
        "id": "tm95",
        "names": {
            "japanese": "わざマシン９５",
            "korean": "기술머신95",
            "french": "CT95",
            "german": "TM95",
            "spanish": "MT95",
            "italian": "MT95",
            "english": "TM95"
        }
    },
    {
        "id": "xtransceiver",
        "names": {
            "japanese": "ライブキャスター",
            "korean": "라이브캐스터",
            "french": "Vokit",
            "german": "Viso-Caster",
            "spanish": "Videomisor",
            "italian": "Interpoké",
            "english": "Xtransceiver"
        }
    },
    {
        "id": "godstone",
        "names": {
            "japanese": "ゴッドストーン",
            "english": "god stone"
        }
    },
    {
        "id": "gram1",
        "names": {
            "japanese": "はいたつぶつ１",
            "korean": "배달물1",
            "french": "Courrier 1",
            "german": "Briefpost 1",
            "spanish": "Envío 1",
            "italian": "Missiva 1",
            "english": "Gram 1"
        }
    },
    {
        "id": "gram2",
        "names": {
            "japanese": "はいたつぶつ２",
            "korean": "배달물2",
            "french": "Courrier 2",
            "german": "Briefpost 2",
            "spanish": "Envío 2",
            "italian": "Missiva 2",
            "english": "Gram 2"
        }
    },
    {
        "id": "gram3",
        "names": {
            "japanese": "はいたつぶつ３",
            "korean": "배달물3",
            "french": "Courrier 3",
            "german": "Briefpost 3",
            "spanish": "Envío 3",
            "italian": "Missiva 3",
            "english": "Gram 3"
        }
    },
    {
        "id": "gram4"
    },
    {
        "id": "medalbox",
        "names": {
            "japanese": "メダルボックス",
            "korean": "메달박스",
            "french": "Boîte Médailles",
            "german": "Medaillenbox",
            "spanish": "Caja Insignias",
            "italian": "Box Premi",
            "english": "Medal Box"
        }
    },
    {
        "id": "dnasplicers",
        "names": {
            "japanese": "いでんしのくさび",
            "korean": "유전자쐐기",
            "french": "Pointeau ADN",
            "german": "DNS-Keil",
            "spanish": "Punta ADN",
            "italian": "Cuneo DNA",
            "english": "DNA Splicers"
        }
    },
    {
        "id": "permit",
        "names": {
            "japanese": "きょかしょう",
            "korean": "허가증",
            "french": "Permis",
            "german": "Genehmigung",
            "spanish": "Pase",
            "italian": "Permesso",
            "english": "Permit"
        }
    },
    {
        "id": "ovalcharm",
        "names": {
            "japanese": "まるいおまもり",
            "korean": "둥근부적",
            "french": "Charme Ovale",
            "german": "Ovalpin",
            "spanish": "Amuleto Oval",
            "italian": "Ovamuleto",
            "english": "Oval Charm"
        }
    },
    {
        "id": "shinycharm",
        "names": {
            "japanese": "ひかるおまもり",
            "korean": "빛나는부적",
            "french": "Charme Chroma",
            "german": "Schillerpin",
            "spanish": "Amuleto Iris",
            "italian": "Cromamuleto",
            "english": "Shiny Charm"
        }
    },
    {
        "id": "plasmacard",
        "names": {
            "japanese": "プラズマカード",
            "korean": "플라스마카드",
            "french": "Carte Plasma",
            "german": "Plasmakarte",
            "spanish": "Tarjeta Plasma",
            "italian": "Carta Plasma",
            "english": "Plasma Card"
        }
    },
    {
        "id": "grubbyhanky",
        "names": {
            "japanese": "よごれたハンカチ",
            "korean": "더러운손수건",
            "french": "Mouchoir Sale",
            "german": "Schnäuztuch",
            "spanish": "Pañuelo Sucio",
            "italian": "Pezza Sporca",
            "english": "Grubby Hanky"
        }
    },
    {
        "id": "colressmachine",
        "names": {
            "japanese": "アクロママシーン",
            "korean": "아크로마머신",
            "french": "Nikodule",
            "german": "Achromat",
            "spanish": "Acromáquina",
            "italian": "Acrocongegno",
            "english": "Colress Machine"
        }
    },
    {
        "id": "droppeditem",
        "names": {
            "japanese": "わすれもの",
            "korean": "잊은물건",
            "french": "Objet Trouvé",
            "german": "Fundsache",
            "spanish": "Objeto Perdido",
            "italian": "Oggetto perso",
            "english": "Dropped Item"
        }
    },
    {
        "id": "revealglass",
        "names": {
            "japanese": "うつしかがみ",
            "korean": "비추는거울",
            "french": "Miroir Sacré",
            "german": "Wahrspiegel",
            "spanish": "Espejo Veraz",
            "italian": "Verispecchio",
            "english": "Reveal Glass"
        }
    },
    {
        "id": "abilitycapsule",
        "names": {
            "japanese": "とくせいカプセル",
            "korean": "특성캡슐",
            "french": "Pilule Talent",
            "german": "Fähigk.-Kapsel",
            "spanish": "Cáps. Habilidad",
            "italian": "Capsula abilità",
            "english": "Ability Capsule"
        }
    },
    {
        "id": "whippeddream",
        "names": {
            "japanese": "ホイップポップ",
            "korean": "휘핑팝",
            "french": "Chantibonbon",
            "german": "Sahnehäubchen",
            "spanish": "Dulce de Nata",
            "italian": "Dolcespuma",
            "english": "Whipped Dream"
        }
    },
    {
        "id": "sachet",
        "names": {
            "japanese": "においぶくろ",
            "korean": "향기주머니",
            "french": "Sachet Senteur",
            "german": "Duftbeutel",
            "spanish": "Saquito Fragante",
            "italian": "Bustina aromi",
            "english": "Sachet"
        }
    },
    {
        "id": "richmulch",
        "names": {
            "japanese": "たわわこやし",
            "korean": "주렁주렁비료",
            "french": "Fertibondance",
            "german": "Sprießmulch",
            "spanish": "Abono Fértil",
            "italian": "Fertilflorido",
            "english": "Rich Mulch"
        }
    },
    {
        "id": "surprisemulch",
        "names": {
            "japanese": "びっくりこやし",
            "korean": "깜놀비료",
            "french": "Fertistantané",
            "german": "Wundermulch",
            "spanish": "Abono Sorpresa",
            "italian": "Fertilsorpresa",
            "english": "Surprise Mulch"
        }
    },
    {
        "id": "boostmulch",
        "names": {
            "japanese": "ぐんぐんこやし",
            "korean": "부쩍부쩍비료",
            "french": "Fertibérance",
            "german": "Wuchermulch",
            "spanish": "Abono Fructífero",
            "italian": "Fertilcopioso",
            "english": "Boost Mulch"
        }
    },
    {
        "id": "amazemulch",
        "names": {
            "japanese": "とんでもこやし",
            "korean": "기절초풍비료",
            "french": "Fertiprodige",
            "german": "Ultramulch",
            "spanish": "Abono Insólito",
            "italian": "Fertilprodigio",
            "english": "Amaze Mulch"
        }
    },
    {
        "id": "discountcoupon",
        "names": {
            "japanese": "バーゲンチケット",
            "korean": "바겐세일티켓",
            "french": "Bon Réduction",
            "german": "Rabattmarke",
            "spanish": "Vale Descuento",
            "italian": "Buono sconto",
            "english": "Discount Coupon"
        }
    },
    {
        "id": "strangesouvenir",
        "names": {
            "japanese": "ふしぎなおきもの",
            "korean": "이상한장식품",
            "french": "Bibelot Bizarre",
            "german": "Skurriloskulptur",
            "spanish": "Estatuilla Rara",
            "italian": "Strano ninnolo",
            "english": "Strange Souvenir"
        }
    },
    {
        "id": "lumiosegalette",
        "names": {
            "japanese": "ミアレガレット",
            "korean": "미르갈레트",
            "french": "Galette Illumis",
            "german": "Illumina-Galette",
            "spanish": "Crêpe Luminalia",
            "italian": "Pan di Lumi",
            "english": "Lumiose Galette"
        }
    },
    {
        "id": "jawfossil",
        "names": {
            "japanese": "アゴのカセキ",
            "korean": "턱화석",
            "french": "Fossile Mâchoire",
            "german": "Kieferfossil",
            "spanish": "Fósil Mandíbula",
            "italian": "Fossilmascella",
            "english": "Jaw Fossil"
        }
    },
    {
        "id": "sailfossil",
        "names": {
            "japanese": "ヒレのカセキ",
            "korean": "지느러미화석",
            "french": "Fossile Nageoire",
            "german": "Flossenfossil",
            "spanish": "Fósil Aleta",
            "italian": "Fossilpinna",
            "english": "Sail Fossil"
        }
    },
    {
        "id": "adventurerules",
        "names": {
            "japanese": "たんけんこころえ",
            "korean": "탐험수칙",
            "french": "ABC Aventure",
            "german": "Abenteuerfibel",
            "spanish": "Guía de Máximas",
            "italian": "Guida Avventura",
            "english": "Adventure Rules"
        }
    },
    {
        "id": "elevatorkey",
        "names": {
            "japanese": "エレベータのキー",
            "korean": "엘리베이터키",
            "french": "Clé Ascenseur",
            "german": "Liftschlüssel",
            "spanish": "Llave Ascensor",
            "italian": "Chiave ascensore",
            "english": "Elevator Key"
        }
    },
    {
        "id": "holocaster",
        "names": {
            "japanese": "ホロキャスター",
            "korean": "홀로캐스터",
            "french": "Holokit",
            "german": "Holo-Log",
            "spanish": "Holomisor",
            "italian": "Holovox",
            "english": "Holo Caster"
        }
    },
    {
        "id": "honorofkalos",
        "names": {
            "japanese": "カロスエンブレム",
            "korean": "칼로스엠블럼",
            "french": "Insigne de Kalos",
            "german": "Kalos-Emblem",
            "spanish": "Emblema Kalos",
            "italian": "Emblema di Kalos",
            "english": "Honor of Kalos"
        }
    },
    {
        "id": "intriguingstone",
        "names": {
            "japanese": "すごそうないし",
            "korean": "대단할듯한돌",
            "french": "Pierre Insolite",
            "german": "Kurioser Stein",
            "spanish": "Piedra Insólita",
            "italian": "Sasso suggestivo",
            "english": "Intriguing Stone"
        }
    },
    {
        "id": "lenscase",
        "names": {
            "japanese": "レンズケース",
            "korean": "렌즈케이스",
            "french": "Boîte Lentilles",
            "german": "Linsenetui",
            "spanish": "Portalentillas",
            "italian": "Portalenti",
            "english": "Lens Case"
        }
    },
    {
        "id": "lookerticket",
        "names": {
            "japanese": "ハンサムチケット",
            "korean": "핸섬티켓",
            "french": "Ticket Beladonis",
            "german": "LeBelle-Ticket",
            "spanish": "Boleto Handsome",
            "italian": "Carta Bellocchio",
            "english": "Looker Ticket"
        }
    },
    {
        "id": "megaring",
        "names": {
            "japanese": "メガリング",
            "korean": "메가링",
            "french": "Méga-Anneau",
            "german": "Mega-Ring",
            "spanish": "Mega-Aro",
            "italian": "Megacerchio",
            "english": "Mega Ring"
        }
    },
    {
        "id": "powerplantpass",
        "names": {
            "japanese": "はつでんしょパス",
            "korean": "발전소패스",
            "french": "Passe Centrale",
            "german": "Kraftwerks-Pass",
            "spanish": "Pase Central",
            "italian": "Pass Centrale",
            "english": "Power Plant Pass"
        }
    },
    {
        "id": "profsletter",
        "names": {
            "japanese": "はかせのてがみ",
            "korean": "박사의편지",
            "french": "Lettre du Prof",
            "german": "Brief vom Prof",
            "spanish": "Carta Profesor",
            "italian": "Lettera del Prof",
            "english": "Prof's Letter"
        }
    },
    {
        "id": "rollerskates",
        "names": {
            "japanese": "ローラースケート",
            "korean": "롤러스케이트",
            "french": "Rollers",
            "german": "Rollerskates",
            "spanish": "Patines",
            "italian": "Pattini",
            "english": "Roller Skates"
        }
    },
    {
        "id": "sprinklotad",
        "names": {
            "japanese": "ハスボーじょうろ",
            "korean": "연꽃몬물뿌리개",
            "french": "Nénurrosoir",
            "german": "Loturzelkanne",
            "spanish": "Lotadgadera",
            "italian": "Irrigalotad",
            "english": "Sprinklotad"
        }
    },
    {
        "id": "tmvpass",
        "names": {
            "japanese": "ＴＭＶパス",
            "korean": "TMV패스",
            "french": "Passe TMV",
            "german": "TMV-Pass",
            "spanish": "Abono del TMV",
            "italian": "Pass TMV",
            "english": "TMV Pass"
        }
    },
    {
        "id": "tm96",
        "names": {
            "japanese": "わざマシン９６",
            "korean": "기술머신96",
            "french": "CT96",
            "german": "TM96",
            "spanish": "MT96",
            "italian": "MT96",
            "english": "TM96"
        }
    },
    {
        "id": "tm97",
        "names": {
            "japanese": "わざマシン９７",
            "korean": "기술머신97",
            "french": "CT97",
            "german": "TM97",
            "spanish": "MT97",
            "italian": "MT97",
            "english": "TM97"
        }
    },
    {
        "id": "tm98",
        "names": {
            "japanese": "わざマシン９８",
            "korean": "기술머신98",
            "french": "CT98",
            "german": "TM98",
            "spanish": "MT98",
            "italian": "MT98",
            "english": "TM98"
        }
    },
    {
        "id": "tm99",
        "names": {
            "japanese": "わざマシン９９",
            "korean": "기술머신99",
            "french": "CT99",
            "german": "TM99",
            "spanish": "MT99",
            "italian": "MT99",
            "english": "TM99"
        }
    },
    {
        "id": "tm100",
        "names": {
            "japanese": "わざマシン１００",
            "korean": "기술머신100",
            "french": "CT100",
            "german": "TM100",
            "spanish": "MT100",
            "italian": "MT100",
            "english": "TM100"
        }
    }
]