/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~logga.js~
 * Logging methods, it's pretty terrible. 
 * Log.temp(...) will probably do what you want, but just add your own logging seriously. 
 */


const Util = require("./Util");

const TEMP = -1;
const CRASH = 1;
const SEVERE = 2;
const ERROR = 3;
const WARNING = 4;
const INFO = 5;
const DEBUG = 6;
const DETAIL = 7;
const TEST = 8;

var level = ERROR;
var alwayslevel = ERROR;

//log anything coming from anything containing this.
var packs = ["battle-updates"]

var messages = ["*"];

function log(level,levell,...args){
	args = Util.flatList(args);

	let stack = new Error().stack.split("at ")[3];

	if(match(stack)||alwayslevel>=levell){
		let filename = regex(stack);
		process.stdout.write("["+hour()+"]["+filename+"]["+level+"]: ");
		for(let i in args){
			if(typeof(args[i])=='object'){
				process.stdout.write(JSON.stringify(args[i],null,4))
			}
			else if(typeof(args[i])!='string'){
				process.stdout.write(""+args[i]);
			}
			else{
				process.stdout.write(args[i]);
			}
		}
		process.stdout.write("\n");
	} else {	
	}
}

exports.crash = function(...args){
	if(level>=CRASH){
		log("CRASH",CRASH,args);
	}
}

exports.severe = function(...args){
	if(level>=SEVERE){
		log("SEVERE",SEVERE,args);
	}
}

exports.error = function(...args){
	if(level>=ERROR){
		log("ERROR",ERROR,args);
	}
}


exports.warn = function(...args){
	if(level>=WARNING){
		log("WARNING",WARNING,args);
	}
}


exports.info = function(...args){
	if(level>=INFO){
		log("INFO",INFO,args);
	}
}

exports.crash = function(...args){
	if(level>=CRASH){
		log("CRASH",CRASH,args);
	}
}

exports.debug = function(...args){
	if(level>DEBUG){
		log("DEBUG",DEBUG,args);
	}
}

exports.detail = function(...args){
	if(level>=DETAIL){
		log("DETAIL",DETAIL,args);
	}
}

exports.test = function(...args){
	if(level>=TEST){
		log("TEST",TEST,args);
	}
}

exports.temp = function(...args){
	log("[TEMP]",TEMP,args);
}


//prettyprint
exports.update = function(update){
	for(let i=0;i<update["updates"].length;i++){
		s+=JSON.stringify(update["updates"][i])+"\n";
	}

	s+="\n\nChoice1: "+JSON.stringify(update["p1choice"])
	s+="\n\nChoice2: "+JSON.stringify(update["p2choice"])

	s+="\n\nSide1: "+JSON.stringify(update["p1side"])
	s+="\n\nSide2: "+JSON.stringify(update["p2side"])

	s+="\n\nHoldup: "+JSON.stringify(update["holdup"]);
	return s;
}

function match(pack){
	for(let i=0;i<packs.length;i++){
		let testpack = packs[i];
		if(testpack.includes("*")||pack.includes(testpack)){
			return true;
		}
	}
	return false;
}

//it's a great language innit
function date() {
    var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;
}

function hour(){
	var date = new Date();
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;
    return hour + ":" + min + ":" + sec;
}

//That's not regex
function regex(input){
	let i = input.length-1;
	let match = "none";
	while(i>0){
		if(input[i]==':'&&i>1){
			let j=i-1;
			match = "";
			while(j>0){
				if(input[j]=="\\"||input[j]=="/"){
					return match;
				}
				match = input[j]+match;
				j--;
			}
		}
		i--;
	}
	return match;
}

global.Log = {}
global.Log.here = function(...args){
	let stack = new Error().stack.split("at ")[2];
	let filename = regex(stack);	
	
	if(args.length==0){
		args.push("Here~")
	}
	args.unshift("["+hour()+"]["+filename+"]:");
	console.log.apply(null,args);
}