/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~Pokemon.js~
 *
 * The class managing individual Pokemon. 
 * It aims to simulate most Pokemon-centered functionality from the games.
 *
 * For what fields to modify/read, see 'static default()'
 *
 * For how SaveClass works, see save.js
 * 
 */

const WorldData = require("./save");
const Evolution = require('./evolution');
const Gen = require("./generation");
const Random = Gen.Random
const Settings = require("./settings");
const Util = require('./util');
const dex = require('./dex');

class Pokemon{
	static classLoader(cls){
		cls.updateOn("gainExp","addMove","setOwner","setHp","handleRequest")
	}

	constructor(){
		this.species = Random.Int(1,650)

		this.forme = 0;
		this.ability = 0;
		this.nature = Random.Int(0,24);
		this.shiny = Random.Int(0,Settings.shinyChance)
		this.ivs = {
			atk: Random.Int(0,31),
			def: Random.Int(0,31),
			hp: Random.Int(0,31),
			spa: Random.Int(0,31),
			spd: Random.Int(0,31),
			spe: Random.Int(0,31)
		};
		this.evs = {
			atk: 0,
			def: 0,
			hp: 0,
			spa: 0,
			spd: 0,
			spe: 0
		};
		this.pid = Random.Int(0,4294967295)
		this.moves = [];
		this.status = "";
		this.name = "";
		this.item = null;
		this.otid = -1;
		this.otsecretid = -1;
		this.caughtat ="";
		this.contest = {beautiful:0,cool:0,cute:0,clever:0,tough:0};
		this.amie = {fullness:0,enjoyment:0,affection:0};
		this.randomizeMoves = true;
		this.requests = [];
	}

	onSave(file){
		Util.shallowClone(file,this,
			'experience','forme','ability','nature','shiny',
			'ivs','evs','pid','happiness','status','item','otid',
			'otsecretid','otguid','caughtat','contest','amie','requests'
        );

        file.species = this.species.id;
        file.level = this.getLevel(); //for queries

        file.moves = [];
        for(let move of this.moves){
        	file.moves.push({
        		movedex: move.movedex.id,
        		pp: move.pp,
        		ppups:move.ppups
        	})
        }
	}

	onCreate(){
		this.loadSpecies();
	
		if(this.experience==null){
			if(this.level!=null)
				this.setLevel(this.level,false);
			else
				this.setLevel(Random.Int(1,100),false);
		}

		this.loadStats();
		this.setHpRatio(1);

		if(this.randomizeMoves){
			let learnset = this.species.movesToLevel(this.level);

			let count = Random.Int(1,Math.min(learnset.length,4-this.moves.length))
			for(let i=0;i<count;i++){
				let idx = Random.Index(learnset);
				let move = learnset[idx];
				learnset.splice(idx,1);
				this.moves.push(move);
			}
		}

		delete this.randomizeMoves;
		delete this.ensureOffensiveMove;

		this.loadMoves();
	}

	onLoad(file,cb){
		Util.shallowClone(this,file,
			'experience','forme','ability','nature','shiny',
			'ivs','evs','pid','happiness','status','item','otid',
			'otsecretid','otguid','caughtat','contest','amie','requests'
        );

		this.moves = file.moves;

		//Special loads.
		this.loadSpecies();
		this.loadMoves();
		cb(this)
	}

	loadStats(){
		this.stats = this.species.getStats(this);
	}

	/**
	 * Called whenever species might not be properly loaded, 
	 * (init scripts, db loads or species change)
	 */
	loadSpecies(){
		if(typeof(this.species)!='object')
			this.species = dex.load("pokemon",this.species);

		if(this.name==null||this.name.length==0){
			this.name = this.species.name;
		}
	}

	/*
	 * Called whenever moves might not be properly loaded 
	 * (poorly written init scripts or db loads)
	 */
	loadMoves(){
		for(let moveSlot in this.moves){
			let move = this.moves[moveSlot];

			//if moves looks like [1,3,382,8] or ["thundershock","seismic toss"] etc.
			if(typeof(move)!='object'){
				this.setMove(moveSlot,move);
				continue;
			}

			//if moves looks like [{movedex:"thundershock",pp:2,ppratio:undefined}] etc.
			if(typeof(move.movedex)!='object'){
				//undefined because null is bad.
				this.setMove(moveSlot,
					move.movedex,
					move.ppups||undefined,
					move.ppratio||undefined,
					move.pp||undefined
				);
			}
		}
	}

	static getMaxPPR(dexpp,ppups){
		return Math.floor(dexpp*(1+0.2*ppups))
	}

	static getMaxPP(move){
		let rat = 1+0.2*move.ppups;
		move.maxpp = move.movedex.pp*rat;
		return Math.floor(move.movedex.pp*rat);
	}

	/*
	 * By default, only savemons gain exp. 
	 * This also means that if you save wild pokemon as 'save', they WILL gain exp. 
	 * Mod this method if that's not your intent (wild pokemon are never saved in the games, that's why we do this)
	 */
	gainsExp(){
		return !this.temp;
	}

	canBattle(){
		return this.hp>0&&this.moves.length>0;
	}

	addRequest(type,value){
		this.requests.push({type:type,value:value});
	}

	handleRequest(requestNo,response){
		if(requestNo>this.requests.length){
			throw IllegalError("NoMatchingRequest","No request at index "+requestNo);
		}

		let events = [];
		let request = this.requests[requestNo];
		if(request.type=="evolution"){
			if(response===true||response==="true"){
				this.setSpecies(request.value.id);
				events.push({event:"evolution",pokemon:this._id,name:this.name,species:this.species.species})
			}

			else if(response===false||response==="false"){
				events.push({event:"cancelEvo",pokemon:this._id,name:this.name});
			}
			
			else{
				throw IllegalError("ResponseMissMatch","Received invalid response to evolution request at"+requestNo+": "+response)
			}
		}

		else if(request.type=="move"){
			if(response=="false"||response==false){
				events.push({event:"cancelMove",move:request.value.name,pokemon:this._id,name:this.name});
			} else {
				this.replaceMove(response,request.value.id);
			}
		}
		else {
			throw InternalError("UnhandledRequest","Request "+request.type+" has no registered handler. This is an internal bug");
		}

		this.requests.splice(requestNo,1);
		return {events:events,requests:this.requests};
	}

	onLevelup(oldlevel,ignoreMoves=false){
		let newlevel = this.getLevel();
		let hprat = this.hp/this.stats.hp;
		this.stats=null;
		this.loadStats();
		this.setHpRatio(hprat);

		//check evolution and new moves. 
		let evoqueue = Evolution.checkEvolutions("levelup",this);
		
		//This causes evolution requests to be handled before move requests.
		//Is that accurate to the games?
		for(let i in evoqueue){
			let species = dex.load("pokemon",evoqueue[i].num);
			this.addRequest('evolution',{id:species.id,name:species.species});
		}

		for(let i=oldlevel+1;i<=newlevel;i++){
			if(!ignoreMoves){
				let newmoves = this.species.movesAt(i);
				for(let i in newmoves){
					if(!this.hasMove(newmoves[i])){
						this.addMove(newmoves[i]);
					}
				}					
			}
		}
	}

	hasMove(move){
		return this.moves.map(x=>(x.movedex.id)).indexOf(move)!=-1;
	}

	levelup(){
		if(this.level<=99){
			let lv = this.getLevel()
			this.setLevel(this.getLevel()+1);
		}
	}

	setLevel(level,doCheck=true){
		let oldlevel = this.level;
		this.experience = this.species.expAtLevel(level);
		this.level=level;

		if(doCheck)
			this.onLevelup(oldlevel,false);
	}

	/**
	 * @causesRequest
	 * @param  {[type]}   exp          - The final exp applied to the pokemon
	 * @param  {boolean}  ignoreMoves  - Pass this flag as true if you're coming from a battle
	 * @return {[type]}   new level and (possibly) requests
	 */
	gainExp(exp,ignoreMoves=false){
		let oldlevel = this.getLevel();

		this.experience+=exp;
		this.level=null;

		let newlevel = this.getLevel();

		if(oldlevel!=newlevel){
			this.onLevelup(oldlevel,ignoreMoves);
		}
		return {level:newlevel-oldlevel,requests:this.requests};
	}

	checkRequests(){
		if(this.requests!=null&&this.requests.length!=0){
			throw IllegalError("UnhandledRequest",this.name+" has unhandled requests.");
		}
	}

	gainEvs(stat,amount){
		amount = Math.min(252-this.evs[stat],amount)
		amount = Math.min(510-this.totalEvs(),amount);
		this.evs[stat]+=amount;
	}

	isOutsider(){
		if(this.trainer==null){
			return false;
		}
		return this.trainer._id != this.otguid;
	}

	//Conversion for modern IVs to old (gen1/gen2). This is just a suggestion.
	//Internally, IVs should always be stored in the new format,
	//and only presented legacy when necessary. 
	oldIvs(){
		let old = {}
		//An alternative would be to roof and subtract by 1, which means ONLY ivs 31 gives 15. 	
		//I stuck with this (iv 30 also gives 15) because it means the probability is correct to the old gens.
		// (iv 31 is harder than iv 15, and ivs WAS easier in gen1 and gen2)
		old.defiv = Math.max(Math.floor(this.ivs.def/2),0)
		old.atkiv = Math.max(Math.floor(this.ivs.atk/2),0)
		old.speiv = Math.max(Math.floor(this.ivs.spe/2),0)
		//spc is averaged from spa/spd.
		old.spciv = Math.max(Math.floor((this.ivs.spa+this.ivs.spd)/4),0)

		//hp probability is based on other probabilities 
		//and becomes correct if other ones are correct. 
		old.hpiv=0;
		old.hpiv+=old.atkiv%2!=0?8:0;
		old.hpiv+=old.defiv%2!=0?4:0;
		old.hpiv+=old.speiv%2!=0?2:0;
		old.hpiv+=old.spciv%2!=0?1:0;
		return old;
	}

	totalEvs(){
		let tot = 0;
		for(let i in this.evs){
			tot+=this.evs[i];
		}
		return tot;
	}

	setHpRatio(hpratio){
		this.setHp(this.stats.hp*hpratio);
		return this.hp;
	}

	//master 'sethp' method.
	setHp(hp){
		if(hp==-1){
			this.hp = this.stats.hp;
			//never allow invalid hp
			if(isNaN(this.hp)||this.hp==null) this.hp=0;
			return;
		}
		
		hp = Math.floor(hp); //no floaties. 

		hp = Math.min(this.stats.hp,hp);
		hp = Math.max(hp,0);
		//never allow invalid hp
		if(isNaN(hp)||hp==null) hp=0;
		this.hp=hp;
	}

	getLevel(){
		let obj = this.species.levelAndPercentAtExp(this.experience);
		this.level=obj.level;
		this.percent=obj.percent;
		return this.level;
	}

	checkAction(type){
		if(this.requests.length!=0){
			throw IllegalError("UnhandledRequest","Pokemon "+this._id+" has unhandled requests.");
		}
	}

	isWild(){
		return this.trainer == null;
	}

	getAbility(){
		if(this.species.abilities[this.ability]==null){
			let i=0;
			while(true){
				let ab = this.species.abilities[i];
				if(ab==null) throw InternalError("Cannot get a valid ability for species "+this.species.species);
				this.ability = ab;
				return this.ability;
			}
		}
		return this.species.abilities[this.ability];			
	}

	setOT(trainer){
		this.otguid = trainer._id;
		this.otid = trainer.trainerid;
		this.otsecretid = trainer.secretid;
		this.caughtat = trainer.location;
	}

	setOwner(trainer){
		if(trainer!=null&&!trainer.temp){
			this.temp=false;
		}
		if(trainer==null){
			this.trainer=null;
			return;
		}
		if(this.isWild()){
			this.setOT(trainer);
		}
		this.trainer=trainer;
	}

	hasOffensiveMove(){
		for(let i in this.moves){
			if(this.moves[i].movedex.basePower>0) return true;
		}
		return false;
	}

	setMove(slot,move,ppups=0,ppratio=1,exactpp=undefined){
		let movedex = dex.load("move",move)
		this.moves[slot] = {
			movedex:movedex,
			ppups:ppups,
			pp:exactpp||Pokemon.getMaxPPR(movedex.pp,ppups)*(ppratio||1)
		}
	}

	duplicateMove(newMove){
		for(let move of this.moves){
			if((typeof(move.movedex)=='object'&&move.movedex.id==newMove)
				||newMove==move.movedex){
				throw IllegalError("DuplicateMove","Pokemon already knows move "+newMove);
			}
		}
	}

	//if exactpp is set, it overrides ppratio.
	addMove(movedex,ppups=0,ppratio=1,exactpp=null){
		if(this.moves.length>=4){
			this.duplicateMove();
			this.addRequest("move",{id:movedex.id,name:movedex.name});
			return this.requests;
		}
		this.moves.push(null);
		this.setMove(this.moves.length-1,movedex,ppups,ppratio,exactpp);
		return this.moves[this.moves.length-1];
	}
	
	heal(amount){
		let old = this.hp;
		this.setHp(this.hp+amount);
		return this.hp-old;
	}

	/**
	 * Fully heals the pokemon and all that.
	 * @return {[type]} [description]
	 */
	pokecenter(){
		this.setHp(-1);
		this.status = null;
		for(let i in this.moves){
        	this.moves[i].pp = Pokemon.getMaxPP(this.moves[i]);
		}
	}

	/**
	 * Generator method to read gender of this species gender values.
	 * Call this if you want normal gender ratios in generators using custom 
	 * @return {[type]} [description]
	 */
	decideGender(){
		if(this.species==null){
			throw FormatError("Tried calling 'decidegender' with null species.")
		}

		if(this.species.gender!=null&&this.species.gender!=""){
			return this.species.gender;
		} else {
			if(this.species.genderRatio==null){
				this.gender = Util.randomElem(["M","F"]);
			} else {
				let mr = this.species.genderRatio["M"]*100;
				if(Util.random(0,100)<=mr){
					this.gender='M'
				}	
				this.gender='F'
			}
		}
		return this.gender;
	}
};

WorldData.register(Pokemon)
module.exports = Pokemon;