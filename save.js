/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~save.js~
 */
const Util = require("./util");
const DuskGull = require('./duskgull')
const DB = DuskGull.DB; //@todo: bugs out if I require it directly. No idea why. 
const Generator = DuskGull.Generator;

const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
Classes = {}

/**
 * Registers a class as worlddata.
 */
function register(cls){
	Classes[cls.name] = cls;
	
	//Prepare update methods for classloader
	let update = []
	cls.updateOn = function(...names){
		for(name of names){
			update.push(name)		
		}
	}

	//Run classloader	
	if(cls.classLoader)
		cls.classLoader(cls)

	//These can only be registered once. 
	delete cls.updateOn;

	//wrap registered functions
	for(method of update){
		let fun = cls.prototype[method]
		cls.prototype[method] = function(){
			let x = fun.apply(this,arguments)
			this.update()
			return x
		}
	}

    /*
	 * Load some heavily used prototypes functions directly.  
	 */
	cls.prototype.save = function(cb){
		save(this,cb);
	}

	cls.prototype.update = function(cb){
		if(!this.temp){
			this.save(cb)
		}
	}

	cls.prototype.generateId = function(){
		if(this.hasOwnProperty("_id")){
			cache[this._id] = null; 
		}

		let text = "";
	   	for(let i=0; i < 16; i++ )
	       	text += possible.charAt(Math.floor(Math.random() * possible.length));
	   	this._id = text;

	   	return this._id;
	}

	cls.prototype.toJson = function(){
		return this.onSave({});
	}

	cls.prototype.with = function(obj,val){
		if(val!=null){
			this[obj] = val;
			return
		}
		if(typeof(obj)!='object'||Array.isArray(obj)){
			throw FormatError("InvalidWithParameter","'with' must take a (non-array) object as its sole parameter, got "+typeof(obj));
		}
		Object.assign(this,obj);
		return this;
	}

	cls.prototype.run = function(fun, ...args){
		this[fun].apply(this,args);
		return this;
	}

	cls.prototype.classname = cls.name

	// generator
	cls.create = function(generator=function(){}){
		let obj = new cls()
		if(typeof(generator)=='function'){
			generator(obj)
		} else {
			Object.assign(obj,Generator.generate(generator))
		}

		if(obj._id==null)
			obj.generateId()
	
		if(obj.onCreate)
			obj.onCreate()
		return obj
	}
}

function create(classname,generator){
	return Classes[classname].create(generator)
}

/**
 * Saves an object to the database, based on its classname. 
 * @return {[type]}       [description]
 */
function save(obj,cb=Util.defcb()){
	//the object, by definition, is no longer temporary.
	if(obj.hasOwnProperty("temp")){
		delete obj.temp;
	}

	let saveobj = {}
	obj.onSave(saveobj)
	saveobj._savedAt = Util.timestamp();
	saveobj._id = obj._id

	let _obj = obj
	DB.put(saveobj,obj.classname,function(err,doc){
		if(err!=null){
		} else {
			_obj._savedAt = saveobj._savedAt
		}		
		cb()
	});
}

/**
 * Loads a class either from cache or db.
 * If a generator is passed, a new object is instead generated
 * @param  {[type]}   cname     The class of the object to be loaded (correlates to a collection in the database backend)
 * @param  {[type]}   id        The id of the object to be loaded
 * @param  {Function} cb        The function to be called once an object has been provided.
 * @param  {[type]}   generator An optional default generator object to apply if no object was found. 
 * @return {[type]}             [description]
 */
function loadOrCreate(cname,id=null,generator=null,cb){
	if(id==null){
		return cb(create(cname,generator))
	}
	console.log("id: ",id,"cname: ",cname)
	DB.giveFirst({_id:id},cname,function(err,doc){
		if(doc==null){
			if(generator==null){
				return cb("No match in database, and no generator",null)
			}

			let obj = create(cname,generator)
			return save(obj,function(err){
				cb("No match in database, new object generated",obj)
			})
		}

		let obj = new Classes[cname]();
		obj._id=id;
		obj.onLoad(doc,cb);
	})
}

/**
 * Loads a class and returns null if it's not found in the database. 
 */
function load(cname,id,cb){
	loadOrCreate(cname,id,null,cb);
}

module.exports = {
	load:load,
	loadOrCreate:loadOrCreate,
	save:save,
	load:load,
	register:register
}