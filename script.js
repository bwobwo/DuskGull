let DuskGull = require("./duskgull");
let Pokemon = DuskGull.Pokemon;
let Battle = DuskGull.Battle;

DuskGull.init(function(){
	let p = Pokemon();
	p.controller="player";
	let b = Battle.create(p,Pokemon(),{obedience:false});	
	//b.choose("1",p._id);

	//get requests for the non-ai player (which is the pokemon itself, because it has no trainer)
	let requests = b.state().requests.filter(x=>(x.player==p._id));

	//We summon our pokemon to battle
	b.choose(requests[0].rqid,"switch",p._id)

	requests = b.state().requests.filter(x=>(x.player==p._id));

	b.choose(requests[0].rqid,"run");

	console.log(b.state().updates);


}); 