/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~server.js~
 * This is the current state of DuskGulls external api - eval'd javascript commands.
 * Obviously, this is not really optimal. 
 * 
 * It manages commands either from the shell or by setting up a webserver (by startApi())
 * Because it uses eval, i've added a few macros to simplify loading objects.
 *
 * To access a Pokemon from the database, you can instead of working with callbacks, do:
 * Load(Pokemon,myPokemonId2392)
 * 
 * To access a Trainer:
 * Load(Trainer,myTrainerdfjhdjf)
 *
 * So for example:
 * Load(Trainer,myTrainer).with('name','newName').party.length
 *
 * See? It still KIND of looks like a query. 
 * But seriously, you probably just want to implement this yourself. 
 * I can't know every single implementation in existence. 
 */


const Util = require("./Util");
const http = require("http");
const Log = require("./logga");

//API here
const Dex = require("./dex");
const Battle = require("./battle");
const Pokemon = require("./pokemon");
const Trainer = require("./trainer");
const DB = require('./db');
const Generator = require('./duskgull').Generator;
const WorldData = require('./save');
const Tools = require('./showdown/tools');
const Validate = require('./validation-wrapper');
const Algorithms = require('./algorithms');

const temp = {}
const async = require('async');

acceptedExternal=false;
macros = {}

function query(req,cb){	
	try{
		//Grab all macros.
		let calls = Util.findCalls(req,Object.keys(macros)).map(load=>(
			{position:load,orig:req.substring(load[0],load[2]+1),statement:"macros."+req.substring(load[0],load[1])+"callback,"+req.substring(load[1],load[2]+1)}
		));

		macrovalues = []

		//Execute macros.
		async.each(calls,(call,cb)=>{
			function callback(retVal){
				macrovalues.push(retVal)
				cb()
			}
			eval(call.statement)
		},()=>{
			for(callNo in calls){
				req = req.replace(calls[callNo].orig,"macrovalues["+callNo+"]")
			}
			cb(tryEval(req,macrovalues))
		})
	} catch(error){
		if(IsDuskGullError(error)){
			cb(error);
		} else {
			cb(InternalError("WrappedError","",error))
		}
	}
}

/**
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */
function tryEval(str,macrovalues){
	try{
		let x = eval(str);
		if(x==null){
			return x;
		}
		if(typeof(x)=='object'){
			if(x.toJSONString!=null){
				return x.toJSONString();
			}
			return JSONString(x,4,2);
		}
		if(typeof(x)!='string'){
			return ''+x;
		}
	} catch(error){
		Log.temp("Error:");
		console.log(error.stack);
		if(IsDuskGullError(error)){
			return error;
		}
		return InternalError("WrappedError","",error);
	}
}

function loadMacros(){
	let fs = require('fs');
	fs.readdirSync('macros').forEach(file=>{
		let str = fs.readFileSync(__dirname+"/macros/"+file).toString()
		eval(str)
		try{
			macros[file.replace(".js","")] = run
		} catch(error){
			console.log("Failed to load macro ",file)
		}
	})
}
loadMacros()

exports.startApi = function(port=8081,host="127.0.0.1"){
	if(host!='localhost'&&host!='127.0.0.1'){
		if(!acceptedExternal){
			acceptedExternal=true;
			console.log("Warning: You're trying to open up complete eval access outside of localhost"
				+"\nPlease make sure this port is behind a firewall if you care for your system"
				+"and run this command again.")
			return;
		}
	}

	http.createServer(function (request, response) {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		let req = request.url.substring(1);

		req = decodeURI(req);

		query(req,function(res){
			//find error (idk lol)
			let ret = {
				data:null,
				error:null
			}
			//@todo: actual error callback handling (now errors never return anything.)
			if(typeof(res)=='object'&&res["errcat"]!=null){
				ret.error = res;
			} else {
				ret.data = res; 
			}
			response.end(JSONString(ret,0,-1));			
		});
		return;
	}).listen(port,host);
	console.log("HTTP API started at port "+port+" for "+host);
}

exports.query = query;