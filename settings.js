/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

exports.loadedPokemon = 721;
exports.generation = 6;
exports.expRate = 3;
exports.babyLevel = 5;
exports.shinyChance = 8192;
exports.maxlevel = 100;	//not implemented
exports.boxSize = 30;
exports.boxCount = 30;
exports.disableDisobedience = false;
exports.obedienceInRated=true;

exports.obedienceBadges = [ 
	20,
	30,
	40,
	50,
	60,
	70,
	80,
	90,
	100
]

exports.dbAddress = "mongodb://localhost:27017/duskgull"

//The rate of disobedient moves for overleveled Pokemon.
exports.disobedienceRate = 1;
//The rate of disobedient moves being no move at all. 	
exports.obedienceNoMoveRate = 0.5 	
//Disables inventory checks when using items. 
exports.externalInventory = true; //internal inventory is not implemented
exports.mods = [
	"samplemod",
]
