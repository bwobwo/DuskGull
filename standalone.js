/*
 * DuskGull - Pokemon Game Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Standalone.js - Starts up a web api along with an interpreter. 
 */
const async = require('async');
const DuskGull = require('./duskgull');
const cJSON = require("circular-json");

const rl = require('readline').createInterface({
	input: process.stdin,
	output: process.stdout
});

DuskGull.init(function(){
	DuskGull.Server.startApi();
	interpreter();
})

/**
 * Passing commands directly to the shell. 
 */
function interpreter(){
	rl.question('> ',(answer)=>{
		if(answer=='exit'){
			process.exit(0);
		}

		else if(answer=='cls'||answer=='clear'){
			DuskGull.Util.cls();
			console.log('~Welcome to Pokemon World~');
			interpreter();
		}
		
		else {
			let g = DuskGull.Util.timestamp();
			DuskGull.Server.query(answer,function(result){
				console.log('('+DuskGull.Util.timestamp(g)+'ms)');
				if(result!=undefined&&result!='undefined'){
					
					if(typeof(result)=='object'){
						if(result.toJSONString!=null){
							result = result.toJSONString();
						} else {
							result = cJSON.stringify(result,null,4);
						}
					}
					console.log(result);
				}
				interpreter();
			});
		}
	});
}