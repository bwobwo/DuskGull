describe('Battle', function(){
	describe('create', function(){
		function testBattle(t1,t2){
	  		assert.equal(DuskGull.Battle.create(t1,t2).finished,true)
  		}

	  	it("should take any weird amount of default battle entities and finish",function(){
	  		let pk = ()=>DuskGull.Pokemon.create({});
	  		let tr = ()=>DuskGull.Trainer.create({});

	  		testBattle(pk(),tr());
	  		testBattle(pk(),pk());
	  		testBattle(pk(),[pk(),pk(),pk()]);

	  		//too many, should still finish. 
	  		testBattle(pk(),[pk(),pk(),pk(),pk(),pk(),pk(),pk()]);
			testBattle(tr(),[pk(),pk(),pk(),pk(),pk(),pk(),pk()]);
			testBattle([tr(),tr(),tr(),tr(),tr(),tr(),tr()],pk());
	  	})
	})
});