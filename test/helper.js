global.assert = require('assert');
global.DuskGull = require('./../duskgull');

DuskGull.Dex.init();
before(function(done){
	DuskGull.DB.init(function(){		

		global.workingMax=0;
		for(let i=1;i<802;i++){
		  	try{
			  	DuskGull.Pokemon.create({species:i});
		  		global.workingMax=i;
		  	} catch(error){
				break;
			}
		}
		done();
	});
})