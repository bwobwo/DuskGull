function testPokemon(start=1,max=802,fun){
	for(let i=start;i<max;i++){
		try{
			fun(i);
		} catch(error){
			error.message = "Error at "+i+": "+error.message;
			throw error;
		}
	}
}

describe('Pokemon', function(){
	let types = []
	types[1] = DuskGull.Dex.findFirst("pokemon",{growthRate:1});
	types[2] = DuskGull.Dex.findFirst("pokemon",{growthRate:2});
	types[3] = DuskGull.Dex.findFirst("pokemon",{growthRate:3});
	types[4] = DuskGull.Dex.findFirst("pokemon",{growthRate:4});
	types[5] = DuskGull.Dex.findFirst("pokemon",{growthRate:5});
	types[6] = DuskGull.Dex.findFirst("pokemon",{growthRate:6});

	describe('#setLevel',function(){
		it('have its level set properly from 1 to 100',function(){
			for(let i in types){
				let p = DuskGull.Pokemon.create({species:types[i].species});
				for(let j=1;j<=100;j++){
					p.setLevel(j);
					assert.equal(p.getLevel(),j);
				}
			}
		})
	})

	describe("#levelup",function(){
		it('should levelup properly from 1 to 100',function(){
			for(let i in types){
				let p = DuskGull.Pokemon.create({species:types[i].species});
				for(let j=1;j<=100;j++){
					p.setLevel(j);
					assert.equal(p.getLevel(),j);
				}
			}
		})
	})

	describe('create', function(){
	  	it('should (ideally) not crash loading all 802 pokemon species', function(){
		  	testPokemon(1,802,function(no){
		  		let p = DuskGull.Pokemon.create({species:no});
		  	})
	  	})


	  	it('should always max out pokemons hp by default',function(){
	  		testPokemon(1,workingMax,function(no){
	  			let pokemon = DuskGull.Pokemon.create({species:no});
	  			assert.equal(pokemon.hp,pokemon.stats.hp);
	  		})
	  	});

	  	it('should have its species exact name if it is not set',function(){
	  		testPokemon(1,workingMax,function(no){
	  			let pokemon = DuskGull.Pokemon.create({species:no});
				assert.equal(pokemon.name,pokemon.species.species);
	  		})
		})

		it('should NOT have its parents name if its set',function(){
			testPokemon(1,workingMax,function(no){
				let pokemon = DuskGull.Pokemon.create({species:no,name:"customname"});
				assert.notEqual(pokemon.name,pokemon.species.species);
			})
		})

		it('should not have a null ability',function(){
			testPokemon(1,workingMax,function(no){
				let pokemon = DuskGull.Pokemon.create({species:no,name:"customname"});
				assert.notEqual(pokemon.getAbility(),null);				
			})
		})

		it('should not start out with no moves',function(){
			testPokemon(1,workingMax,function(no){
				let pokemon = DuskGull.Pokemon.create({species:no,name:"customname"});
				assert(pokemon.moves!=null&&pokemon.moves.length!=0);				
			})
		})
	})
});