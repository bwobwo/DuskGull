/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~trainer.js~
 * The class that managers trainer objects. 
 * A Trainer is an entity that can carry 6 Pokemon in their party, and store them in boxes. 
 * Trainers are valid battle entities. 
 * They also have most stuff a trainer would have in the games (badges, items, enabled pokedex entries etc.)
 */
const PARTYSIZE = 6;
const SAVED_ENCOUNTERS = 15;
const Gen = require("./generation");
const Random = Gen.Random
const WorldData = require("./save");
const Util = require('./Util');
const Settings = require('./Settings');
const Pokemon = require('./Pokemon');
const async = require('async');
const Save = require('./save');
const Validate = require('./validation-wrapper');
const Dex = require('./dex');

class Trainer{
    static classLoader(cls){
        cls.updateOn("gainPoke","withdrawPokemon","depositPokemon","useItem")
    }
    
    constructor(){
        this.party = []
        this.trainerid = Random.Int(0,65535);
        this.secretid = Random.Int(0,65535);
        this.boxCount = Settings.boxCount;
        this.boxSize = Settings.boxSize;
        this.pokedex = {}
        this.steps = 0;
        this.flags = {};
        this.badges = [];
        this.battles = [];
        this.money = 0;
        this.obedience = 20;
        this.weather = "";
        this.upsidedown = false;
        this.battle = null;
        this.inventory = {};
        this.gender = Random.Item("M","F")
        this.name = "Red";
        this.title = "Pokemon Trainer";
        this.controller = "internal";
        this.lastRepel = 0;
        this.requests = [];
    }

    onCreate(){
        let psize = this.partySize || Random.Int(1,6);
        for(let i=0;i<psize;++i){
            this.party.push(Pokemon.create(this.partyGen||{}));
        }

        delete this.partySize;
        delete this.partyGen;  
    }

    onLoad(file,cb){
        Util.shallowClone(this,file,
            'trainerid','secretid','boxCount','boxSize','pokedex','steps',
            'flags','badges','money','obedience','weather','upsidedown',
            'battle','inventory','gender','name','title','controller','lastRepel'
        );

        this.battles = [];

        let party = [];
        this.party = party;

        //Load all party pokemon.
        async.each(
            file.party,
            function(pokeid,cba){
                WorldData.load('Pokemon',pokeid,function(err,pokemon){
                    if(!err)
                        this.party.push(pokemon)
                    cba();
                })
            },
            function(){
                cb(this)
            }
        )
    }

    onSave(file){
        Util.shallowClone(file,this,
            'trainerid','secretid','boxCount','boxSize','pokedex','steps',
            'flags','badges','money','obedience','weather','upsidedown',
            'battle','inventory','gender','name','title','controller','lastRepel'
        );

        file.party = [];
        for(let pokemon of this.party){
            file.party.push(pokemon._id);
            pokemon.save();
        }
    }

    canBattle(){
        this.checkRequests();
        return this.party.filter(x=>x.canBattle()).length>0;
    }

    checkRequests(){
        for(let i in this.party){
            this.party[i].checkRequests();
        }
    }

    addEncounter(encounter){
        if(SAVED_ENCOUNTERS!=-1&&this.encounters.length>=SAVED_ENCOUNTERS){
            this.encounters.splice(0,1);
        }
        this.encounters.push(encounter);
    }
    
    hasEncounter(encounter){
        return this.encounters.indexOf(encounter)!=-1;
    }

    setFlag(flag,cb=Util.defcb()){
        if(this.flags==null) this.flags={}
        this.flags[flag]=true;
        this.save(cb);
    }
    removeFlag(flag,cb=Util.defcb()){
        if(this.flags==null) this.flags={}
        delete this.flags[flag];
        this.save(cb);
    }
    hasFlag(flag){
        if(this.flags==null) this.flags = {}
        return this.flags[flag]!=null;
    }
    useItem(itemid,target){
        target=this.pokeFromParty(target);
        return Item.useOw(itemid,this,target);
    }
    swap(partyid,boxno,boxpos,cb=Util.defcb()){
        //we have to check this
        if(this.pokeFromParty(partyid)==null) cb("Missing from party: "+id);

        withdrawPokemon(boxno,boxpos, function(poke){
            if(poke!=null){
                this.addToParty(poke);
            }
            depositPokemon(this.pokeFromParty(partyid));
            cb(poke);
        })
    }
    withdrawPokemon(boxno,boxpos,cb=Util.defcb()){
        if(this.isPartyFull()){
            cb(PokemonError("Party is full!"));
            throw PokemonError("Party is full!");
        }

        if(this.boxes[boxno]==null||this.boxes[boxno].storage[boxpos]==null){
            cb("No pokemon in slot");
            throw PokemonError("No pokemon in slot");
        }
        let p = this.boxes[boxno].storage[boxpos];

        if(Util.isPointer(p)){
            let thiz = this;

            Save.load("Pokemon",p._id,function(err,poke){

                if(poke==null){
                    cb("Pokemon was not successfully loaded, leaving it");
                    return;
                }                    
                if(thiz.addToParty(poke)){
                    delete thiz.boxes[boxno].storage[boxpos];
                    thiz.boxes[boxno].stored--;
                    thiz.save(function(){
                        cb(null,poke);
                    })
                } else {
                    cb("Party became full during db load, reverting");
                }
            });
        } else {
            if(this.addToParty(poke)){
                delete this.boxes[boxno].storage[boxpos];      
                this.boxes[boxno].stored--;
                this.save(function(){
                    cb(null,p);
                });
            } else {
                cb("Party is full (shouldn't happen here, report report!");
            }
        }
    }

    boxWallpaper(boxno,wallpaper){
        if(this.boxes[boxno]==null){
            this.boxes[boxno]={wallpaper:wallpaper,storage:{},stored:0};
        } else {
            this.boxes[boxno].wallpaper = wallpaper;
        }
    }

    depositPokemon(pokemon,boxnoarg=-1,boxpos=-1,cb=Util.defcb()){
        let partymon = this.pokeFromParty(pokemon);
        
        if(typeof(pokemon)=='object'){
            partymon = pokemon;
            pokemon = pokemon._id;
        }

        let boxno = boxnoarg;
        if(partymon==null||partymon=="null"){
            Log.here("Partymon: "+partymon);
            cb("Missing from party: "+pokemon);
            return;

        }

        if(boxno==-1){
            for(let i=0;i<this.boxCount;i++){
                let box = this.boxes[i];
                if(box==null){
                    boxno = i;
                    break;
                }

                if(this.boxes[i].stored<this.boxSize){
                    boxno = i;
                    break;
                }
            }
            if(boxno==-1){
                cb("All boxes are full");
            }
        }

        let box = this.boxes[boxno];
        if(box==null){
            box = {wallpaper:"",storage:{},stored:0};
            this.boxes[boxno] = box;
        }

        if(box.stored>this.boxSize){
            cb("Requested box is full");
        }

        Log.here("Partymon: "+partymon);

        if(boxpos==-1){
            for(let i=0;i<this.boxSize;i++){
                if(box.storage[i]==null){
                    boxpos=i;
                    break;
                }
            }
            if(boxpos==-1){
                cb("Internal error: Requested box is not full but has no free slots");
            }
        }

        if(boxpos>=this.boxSize||boxpos<0){
            cb("Storage position out of range: "+boxnoarg);
        }

        if(box.storage[boxpos]!=null){
            cb("Requested position is occupied by another pokemon: "+boxno+":"+boxpos);
        }

        box.stored++;
        //idk, i'll just fake immediate pointers for now. What should actually be in these? 

        box.storage[boxpos] = partymon;

        if(!this.temp){
            let thiz = this;
            partymon.save(function(){
                thiz.removeFromParty(partymon);
                thiz.save(function(){
                    cb({boxno:boxno,boxpos:boxpos});
                });
            })
        } else {
            cb({boxno:boxno,boxpos:boxpos});
        }
    }

    fixParty(){
        this.party = Util.cleanArray(this.party,null);
    }

    hasRepel(){
        return this.lastRepel>=this.steps;
    }

    applyRepel(amount){
        this.lastRepel = this.steps+amount;
    }

    hasRequests(){
        return this.party.filter(poke=>(poke.hasRequests())).length!=0
    }

    gainBadge(badge){
        this.badges[badge]=true;
    }

    addSteps(num){
        this.steps+=num;
    }

    seePokemon(num){
        if(this.pokedex[num]==null){
            this.pokedex[num] = false;
        }
    }

    catchPokemon(num){
        this.pokedex[num] = true;
    }

    setAi(){
        this.controller = "internal";
    }

    removeAi(){
        this.controller = "Player";
    }

    pokeFromParty(id){
        if(id==null) return null;

        if(typeof(id)=='object'){
            id=id._id;
        }

        if(id==null) return null;

        for(let i in this.party){
            if(this.party[i]._id==id) return this.party[i];
        }
        return null;
    }

    validate(){
        let formats = Dex.formats();

        let frafrafra = {}
        for(let i in formats){
            frafrafra[formats[i].id] = Validate(this.party,formats[i].id);
        }
        console.log(frafrafra);
    }


    battleCheck(){
        for(let i in this.party){
            if(this.party[i].hp >0 && this.party[i].moves.length>0){
                return;
            }
        }
        throw InternalError("NoValidPokemon","We have no valid pokemon on our team.");
    }

    isPartyFull() {
        for (let i = 0; i < PARTYSIZE; i++) {
            if (this.party[i] == null) return false;
        }
        return true;
    }

    findInParty(pokeid) {
        for (let i = 0; i < this.party.length; i++) {
            if (this.party[i] == null) continue;
            if (this.party[i]._id == pokeid) return this.party[i];
        }
        return null;
    }

    numericParty() {
        return Util.fetch(this.party,"_id");
    }

    addToParty(pokemon) {
        if(this.party.length>=PARTYSIZE){
        	return false;
        }

        this.party.push(pokemon);
        return true;
    }

    removeFromParty(pokemon) {
        if(typeof(pokemon)=='object'){
            pokemon = pokemon._id
        }
        for (let i = 0; i < this.party.length; i++) {
            if (this.party[i]._id == pokemon) {
                this.party[i] = null;
                this.fixParty();
                this.save();
                return true;
            }
        }
        return false;
    }

    partyid(id) {
        if (id < 0 || id > 5) return -1;
        if (this.party[id] == null) return -1;
        return this.party[id]._id;
    }

    obedientLevel(){
        return this.obedience;
    }

    /**
     * @description 
     * @param  {[type]} pokemon [description]
     * @return {[type]}         [description]
     */
    gainPoke(poke) {
        poke.temp=this.temp;
        poke.setOwner(this);
        if(!this.addToParty(poke)){
            this.depositPokemon(poke);
        }
        return poke;
    }

    /**
     * Fully heals your party.
     * @return {[type]} [description]
     */
    pokecenter(){
        for(let i in this.party){
            this.party[i].pokecenter();   
        }
    }
    
    losePoke(pokemon) {
        pokemon = this.pokeFromParty(pokemon);
        if(pokemon==null){
            throw InternalError("losePokemon","Cant lose pokemon not in your party.");
        }
        pokemon.setOwner(null);
        this.removeFromParty(pokemon._id);
    }



    static trade(trainer1, trainer2, pokemon1, pokemon2) {
        trainer1.losePoke(pokemon1);
        trainer2.losePoke(pokemon2);

        trainer1.gainPoke(pokemon2);
        trainer2.gainPoke(pokemon1);
    }
};

WorldData.register(Trainer)
module.exports = Trainer;