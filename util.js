/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * util.js
 * Where functions go if they don't fit in anywhere else. Kind of like me.  
 */
class Utile{
	replaceAll(str,replacement,...replace) {
    	for(let i in replace){
    		str = str.split(replace[i]).join(replacement);
    	}
    	return str;
   	};

   	deepclone(obj){
   		
   	}

   	stringInt(obj){
   		if(typeof(obj)=='string'){
   			return parseInt(obj);
   		}
   		return obj;
   	}
   	count(obj){
   		let size=0;
   		for(let i in obj){
   			if(obj.hasOwnProperty(i)) size++;
   		}
   		return size;
   	}
   	getClass(obj){
   		if(typeof(obj)!='object'){
   			return typeof(obj);
   		}
   		if(Array.isArray(obj)) return 'array';
   		if(obj.clazz==null) return 'object';
   		return obj.clazz;
   	}

   	isSameClass(obj,obj2){
   		if(typeof(obj)!='object'){
   			return typeof(obj)==typeof(obj2);
   		}
   		if(Array.isArray(obj)){
   			return Array.isArray(obj2);
   		}
   		return obj.clazz==obj2.clazz;
   	}

   	isClass(obj,clazz){
   		if(typeof(obj)!='object'){
   			return typeof(obj)==clazz;
   		}
   		if(Array.isArray(obj)){
   			if(clazz=='array') return true;
			return false;
		}

		if(obj.clazz==null){
			if(clazz=='object') return true;
			return false;
		}
		return obj.clazz==clazz;
   	}


	/**
	 * @description Generates a random integer between min and max. 
	 * @rant (Why does no stlibs include this, seriously?)
	 * @param  {number} min
	 * @param  {number} max
	 * @return {integer}
	 */
	random(min, max) {
	    return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	/**
	 * @description Converts a number to a binary string (1's and 0's) 
	 * @param  {number} dec 
	 * @return {string}
	 */
	randomElem(obj){
		if(Array.isArray(obj)){
			return obj[this.random(0,obj.length-1)];		
		}
		var result;
	    var count = 0;
	    for (var prop in obj)
			if (Math.random() < 1/++count)
				result = obj[prop];

		return result;
	}

	spliceRandom(obj){
		if(Array.isArray(obj)){
			return obj.splice(this.random(0,obj.length-1),1)[0];
		}
		return null;
	}

	randomIdx(obj){
		return this.random(0,obj.length-1);
	}


	//mapping for objects
	map(obj,func){
		let newo = {}
		for(let i in obj){
			newo[i] = func(obj);
		}
		return newo;
	}

	/**
	 * @deprecated - Use Util.isIn()
	 */
	equalsAny(objin,...compares){
		if(typeof(objin)=='object'){
			for(let i in objin){
				if(this.equalsAny(objin[i],compares)){
					return true;
				}
			}

		} else {
			for(let i in compares){
				if(typeof(compares[i])=='object'){
					for(let j in compares[i]){
						if(compares[i][j]==objin){
							return true;
						}
					}
				}
				else {
					if(compares[i]==objin){
						return true;
					}					
				}
			}
		}


		return false;
	}

	toBin(dec){
	    return (dec >>> 0).toString(2);
	}

	copyFields(source,target,...ignores){
		ignores = this.flatList(ignores);
		for(let i in source){
			if(ignores.indexOf(i)!=-1) continue;
			target[i] = source[i];
		}
		return target;
	}


	pick(obj,...fields){
		let out = {}
		for(let i in obj){
			if(fields.indexOf(i)!=-1){
				out[i] = obj[i];
			}
		}
		return out;
	}

	fetch(obj,field){
		field = field.split(".");

		return obj.map(function(a){
			let curobj = a;
			for(let i in field){
				curobj = curobj[field[i]]
			}
			return curobj
		});
	}


	/**
	 * @description Adds leading characters (defaults to 0s)
	 * @param  {number|string} 	input
	 * @param  {number} 		size length of the new string
	 * @return {string}   		
	 */
	pad(num, size,char='0') {
	    var s = num+"";
	    while (s.length < size) s = char + s;
	    return s;
	}


	/**
	 * Removes an element from an array at a specified index. 
	 * @param  {[type]} arr   [description]
	 * @param  {[type]} index [description]
	 * @return {[type]}       [description]
	 */
	removeAt(arr,index){
		arr.splice(index,1);
		return arr;
	}

	/**
	 * Removes all null or invalid values from an object. Is there built-in functionality for this?
	 * @param  {object} obj 
	 * @return {[type]} obj without null fields.
	 */
	removeNull(obj){
		let deletes = []
		for(let i in obj){
			if(obj[i]==undefined || obj[i]==null || isNaN(obj[i]) ||obj==""){
				deletes.push(i);
			} 
		}

		for(let i in deletes){
			let id = deletes[i]
			delete obj[id];
		}
		return obj;
	}

	shuffle(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;

		// While there remain elements to shuffle...
		while (0 !== currentIndex) {
			// Pick a remaining element...
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;

			// And swap it with the current element.
			temporaryValue = array[currentIndex];
			array[currentIndex] = array[randomIndex];
			array[randomIndex] = temporaryValue;
		}

		return array;
	}

	/**
	 * Replaces a substring with another string, this can change the strings total length. 
	 * @param  {string} string      - full string
	 * @param  {number} start       - substring end
	 * @param  {number} end         - substring end
	 * @param  {string} replacement
	 * @return {string} replaced string             
	 */
	replaceAt(string,start,end,replacement) {
    	return string.substr(0, start) + replacement+ string.substr(end);
	}


	find(obj,fun){
		if(Array.isArray(obj)){
			return obj.find(fun);
		}
		for(let i in obj){
			if(fun(obj[i])){
				return obj[i];
			}
		}
	}

	/**
	 * Filter function that also works for objects
	 */
	filter(obj,fun){
		if(Array.isArray(obj)){
			return obj.filter(fun);
		}
		let no = {}
		for(let i in obj){
			if(fun(obj[i])){
				no[i] = obj[i];
			}
		}
		return no;
	}




	/**
	 * @description Ensures an input list matches another list of types.
	 * @param  {[type]} arr   The matching list (ex. ["i","love","pokemon"])
	 * @param  {[type]} types The list of matched values (ex. ["string","number","string"])
	 * @return {[type]}       The original array if it passed, with numbers autoconverted.
	 */
	ensure(arr, types){
		if(arr.length<types.length) return null;
		let vars = []
		for(let i=0;i<types.length;i++){
			if(typeof(arr[i])!=types[i]){
				//try converting numbers
				if(types[i]=="number"){
					let number = parseInt(arr[i]);
					if(isNaN(number)) return null;
					
					vars.push(number);
					continue;
				} 
				return null;
			}
			vars.push(arr[i]);
		}
		return vars;
	}

	/**
	 * @deprecated Use {@link jsonizer} instead. 
	 * @param {[type]} inobj [description]
	 */
	JSONClone(inobj){
		return JSON.parse(JSON.stringify(inobj));
	}


	/**
	 * @description Calculates exp gain for a single foe when a pokemon faints in battle. 
	 * @param  {number} 			gen        - The generation for deciding exp formula.
	 * @param  {BattlePokemon} 		loser      - The pokemon that fainted. 
	 * @param  {BattlePokemon} 		winner     - The pokemon gaining exp
	 * @param  {number} 			shareCount - How many pokemon shared the exp
	 * @TESTME
	 * @return {number}            	Exp gain.
	 */
	calculateExp(gen,loser,speciesexp,winner,shareCount){
		let a = (loser.wild||gen==7) ? 1:1.5;
		let b = speciesexp

		let e = 1; //@FIXME: fix. It's for lucky egg. 
		let f = (winner.affection>=2)?1.2:1;
		let L = loser.level;
		let Lp = winner.level;
		let p = 1; //@FIXME : check docs better idk what this does. 
		let s = shareCount; //@FIXME: generation fixes. 
		let t = winner.outsider?1.5:1;

		//@FIXME: This should be read at battle start.
		//let v = (gen>6&&winner.checkEvo("levelup").length>0)?1.2:1
		let v = 1;

		if(gen==5||gen==7){
			return Math.floor((
					(a*b*L)
					/(5*s)
					*Math.pow(2*L+10,2.5)
					/Math.pow(L+Lp+10,2.5)
					+ 1
				)
				*(t*e*p));
		} 
	

		return Math.floor((a*t*b*e*L*p*f*v)/(7*s))
		
	}

	cleanArray(arr,value){
		for (var i = 0; i < arr.length; i++) {
		    if (arr[i] == value) {         
		      arr.splice(i, 1);
		      i--;
    		}
  		}
  		return arr;
	}

	defcb(message=""){
		return function(){
			if(message!=""){
				console.log(message);
			}
		}
	}

	capitalizeFirst(string){
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	/**
	 * @description Because Date().getTime() is fugly. Also supports an offset. 
	 * @param  {number} value - Subtracted value (default to 0)
	 * @return {number} timestamp from unixtime or provided timestamp. 
	 */
	timestamp(subtract=0){
		return new Date().getTime()-subtract;
	}
	
	/*
	 * @param  {[type]} val [description]
	 * @param  {[type]} min [description]
	 * @param  {[type]} max [description]
	 * @return {[type]}     [description]
	 */
	clamp(val,min,max){
		val = Math.max(val,min);
		val = Math.min(val,max);
		return val;
	}

	/**
	 * @description  Allows you to measure the times it takes to execute a function n times. 
	 * @note To take IO into consideration, you have to supply and call a callback. 
	 * @param  {[type]} iterations - How many times yes. 
	 * @param  {[type]} fun1       - The function you wanna test. 
	 * @return {[type]}            - The time it took for all the functions to execute. 
	 */
	measureAsync(iterations,fun1){
		let starttime = new Date().getTime();
		async.each(
			function(cb){
				fun1(cb);
			},

			function(error,cb){
				//why the fuck aren't you using your own fucking utility functions
				return new Date().getTime()-starttime;
			}
		)
	}

	/**
	 * Finds all indices in an array.
	 * @param  {[type]} arr [description]
	 * @param  {[type]} val [description]
	 * @return {[type]}     [description]
	 */
	getAllIndices(arr, val) {
	    var indexes = [], i;




	    for(i = 0; i < arr.length; i++)
	        if (arr[i] === val)
	            indexes.push(i);
	    return indexes;
	}	


	/**
	 * Exactly what it reads like. 
	 * @return {[type]} [description]
	 */
	cls(){
		this.clear();
	}

	/**
	 * unix
	 * @return {[type]} [description]
	 */
	clear(){
		console.log('\x1Bc'); // Clear screen
	}

	/**
	 * @description  Allows you to measure the times it takes to execute a function n times. 
	 * @param  {number} iterations   - How many times. 
	 * @param  {function} fun1       - The function you want to test. 
	 * @return {number}              - The time in ms it took for all the functions to execute. 
	 */
	measure(iterations,fun1){
		//@NOTE: always runs the function once first to rule out compilation time. 
		fun1();
		let starttime = new Date().getTime();
		for(let i=0;i<iterations;i++){
			fun1();
		}
		return new Date().getTime()-starttime;
	}

	compare(iterations,funs){
		for(let i in funs){
			//@NOTE: compile 'measure' and the function. 
			if(typeof(funs[i])!='string'){
				this.measure(1,funs[i]);
			}
		}
		let measurements = []
		for(let i in funs){	
			if(typeof(funs[i])!='string'){
				let mes = this.measure(iterations,funs[i]);
				measurements.push({fun:i,time:mes,name:funs[i-1]});
			}
		}
		measurements = measurements.sort(this.compareTime);
		for(let i in measurements){
			let magnitude = measurements[i].time/measurements[0].time;
			console.log((parseInt(i)+1)+". "+measurements[i].name+": "+measurements[i].time+"ms ("+magnitude.toFixed(2)+"x)");
		
		}
		return measurements;
	}


	compareTime(a,b) {
	  if (a.time < b.time)
	    return -1;
	  if (a.time > b.time)
	    return 1;
	  return 0;
	}

	removeFile(filename,cb){
		fs.unlink(filename,function(err){
			cb(); //@NOTE: cb either way, ignore errors. 
		})
	}

	getMatches(theString, theRegex){
	    return theString.match(theRegex).map(function(el) {
	        var index = theString.indexOf(el);
	        return [index, index + el.length - 1];
	    });
	}

	splitWithClosures(stringin){
		let op = ['(','[','{']
		let cl = [')',']','}']
		let brs = 0;

		let vals = []

		let cur=""

		let sqstring =false;
		let dqstring = false;

		for(let i in stringin){
			if(stringin[i]=="'"&&!dqstring){
				sqstring=!sqstring;
			}
			if(stringin[i]=='"'&&!sqstring){
				dqstring=!dqstring;
			}

			if(sqstring||dqstring){
				cur+=stringin[i];
				continue;
			}

			if(stringin[i]==','&&brs==0){
				vals.push(cur.replace(" ",""));
				cur=""
			} else {
				cur+=stringin[i];
			}

			if(op.indexOf(stringin[i])!=-1){
				brs++;
			}

			if(cl.indexOf(stringin[i])!=-1){
				brs--;
			}
		}
		vals.push(cur);
		return vals;
	}

	findEndingBracket(stringin,off){
		let op = ['(','[','{']
		let cl = [')',']','}']
		
		let sqstring =false;
		let dqstring = false;

		let brs = 0;
		for(let i in stringin){
			i=parseInt(i)+off;

			if(stringin[i]=='"'&&!sqstring){
				dqstring=!dqstring;
			}

			if(stringin[i]=="'"&&!dqstring){
				sqstring=!sqstring;
			}

			if(sqstring||dqstring){
				continue;
			}

			if(op.indexOf(stringin[i])!=-1){
				brs++;
			}

			if(cl.indexOf(stringin[i])!=-1){
				if(brs==0) return i;
				brs--;
			}
		}
		return null;
	}


	tryCB(cb){
		if(cb!=null && typeof(cb)=='function'){
			cb();
		}
	}

	findCalls(stringin,calls){
		let indices = []
		for(let i in stringin){
			i = parseInt(i);
			for(let call of calls){
				if(this.reads(stringin,i,call+"(")){
					indices.push([i,i+5,this.findEndingBracket(stringin,i+5)])
				}				
			}
		}
		return indices
	}

	reads(stringin,off,match){
		for(let i in match){
			i = parseInt(i)
			if(stringin[off+i]!=match[i]){
				return false;
			}
		}
		return true;
	}


	/**
	 * All properties of query has to be matched by obj. 
	 * @param  {[type]} query [description]
	 * @param  {[type]} obj   [description]
	 * @example matches({'level':25})
	 * @return {[type]}       [description]
	 */
	matches(obj,query){
		try{
			if(typeof(query)!='object'){
				return query==obj;
			}

			if(obj==null){
				return false;
			}

			for(let j in query){
				//equals
				if(j=="$eq") return query[j]==obj;
				
				//greater than
				if(j=="$gt") return query[j]<obj;
				
				//greater than or equal
				if(j=="$gte") return query[j]<=obj;
				
				//lesser than
				if(j=="$lt") return query[j]>obj;
				
				//lesser than or equals
				if(j=="$lte") return query[j]>obj;
				
				//not equal
				if(j=="$ne") return query[j]!=obj;
				
				//in array
				if(j=="$in") return this.isIn(obj,query[j]);
				
				//not in array
				if(j=="$nin") return !this.isIn(obj,query[j]);

				//contains
				if(j=="$ct"){
					return this.isIn(query[j],obj);
				}
				//not contains
				if(j=="$nct") return !this.isIn(query[j],obj); 
				
				//within
				if(j=="$wt") return query[j][0]<obj&&query[j][1]>obj;
				
				//within or equal
				if(j=="$wte") return query[j][0]<=obj&&query[j][1]>=obj;

				

				if(!this.matches(obj[j],query[j])){
					return false;
				}
			}
		} catch(error){
			console.log(error.stack)
			return false; //probably something null. 
		}
		return true;
	}

	anyMatch(obj1,obj2){
		for(let i in obj1){
			for(let j in obj2){
				if(obj1[i]==obj2[j]){
					return true;
				}
			}
		}
		return false;
	}

	shallowClone(target,source,...fields){
		for(let key of fields){
			target[key] = source[key];
		}
		return target
	}


	/**
	 * Scans through an array/object and returns true if any of its values matches the provided object. 
	 * @param  {[type]}  query [description]
	 * @param  {[type]}  obj   [description]
	 * @return {Boolean}       [description]
	 */
	isIn(obj,query){
		for(let i in query){
			if(this.matches(query[i],obj)){
				return true;
			}
		}
		return false;
	}

	findAll(obj,query){
		if(typeof(query)!='object'){
			throw FormatError("QueryError","Searching with non-object query, got type: "+typeof(query));
		}

		let matches = []
		if(obj instanceof Map){
			for(let [key,value] of obj){
				let good=true;
				for(let j in query){
					if(!this.matches(value[j],query[j])){
						good=false;
						break;
					}
				}
				if(good) matches.push(value);
			}
		} else {
			for(let i in obj){
				let child = obj[i];
				let good=true;
				for(let j in query){
					if(!this.matches(child[j],query[j])){
						good=false;
						break;
					}
				}
				if(good) matches.push(child);
			}
		}
		return matches;
	}


	/**
	 * Allows casting types to search-objects by giving them a default value based on its type. 
	 * @param  {[type]} obj      [description]
	 * @param  {[type]} defaults [description]
	 * @return {[type]}          [description]
	 */
	defaultObj(obj,defaults){
		if(obj==null){
			return {}
		}

		if(typeof(defaults)!='object'){
			let r = {}
			r[defaults] = obj;
			return r;
		}

		for(let i in defaults){
			if(typeof(obj)==i){
				let r = {}
				r[defaults[i]] = obj;
				return r;
			}
		}
		return obj;
	}

	//Throw in any number of lists
	flatList(...args){
		if(args.length==1) args = args[0];

		let list =[]
		for(let i in args){
			if(Array.isArray(args[i])){
				let q = this.flatList(args[i]);
				for(let j in q){
					list.push(q[j]);
				}
			} else {
				list.push(args[i]);
			}
		}
		return list;
	}

	/**
	 * 
	 */
	single(arr){
		if(arr.length==1) return arr[0];
		return arr;
	}

	isPointer(ino){
		if(typeof(ino)!='object') return true;

		if(ino.pointer!=null) return true;
		return false;
	}

	toId(text) {
		// this is a duplicate of Dex.getId, for performance reasons
		if (text && text.id) {
			text = text.id;
		} else if (text && text.userid) {
			text = text.userid;
		}
		if (typeof text !== 'string' && typeof text !== 'number') return '';
		return ('' + text).toLowerCase().replace(/[^a-z0-9]+/g, '');
	}

	findFirst(obj,query){
		if(typeof(obj)!='object'){
			throw FormatError("QueryError","Searching with non-object query, got type: "+typeof(query));
		}
		
		if(obj instanceof Map){
			for(let [key,value] of obj){
				let good=true;
				for(let j in query){
					if(value[j]!=query[j]){
						good=false;
						break;
					}
				}
				if(good) return value;
			}
		} else {
			for(let i in obj){
				let child = obj[i];
				let good=true;
				for(let j in query){
					if(!this.matches(child[j],query[j])){
						good=false;
						break;
					}
				}
				if(good) return child;
			}

		}
	}


	associatedPokemon(obj){
		if(Array.isArray(obj)){
			let full = []
			for(let i in obj){
				full.push(this.associatedPokemon(obj[i]));
			}
			return this.flatList(full);
		}

		let arr = []
		if(obj.isPokemon){
			arr.push(obj);
		}

		if(obj.party!=null){
			for(let poke in obj.party){
				poke = obj.party[poke];
				if(poke.isPokemon){
					arr.push(poke);
				}
			}
		}
		return arr;
	}

	prop(property,field){
		if(typeof(field)=='object'){
			return field[property];
		}
		return field;
	}

}

module.exports = new Utile();