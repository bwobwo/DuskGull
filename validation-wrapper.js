/*
 * DuskGull - Pokemon World Simulator
 * Copyright (C) 2017 BwoBwo
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * ~validation-wrapper.js~
 * Takes in any DuskGull battle entity and validates it for a format through Showdown.
 */
const showdownValidator = require("./showdown/team-validator");
const Util = require('./util');

function validate(obj,format){
	obj = Util.associatedPokemon(obj);
	let v = showdownValidator(format,"");

	let textmons = []
	for(let i in obj){
		textmons.push(textifyMon(obj[i]))
	}

	let q = v.validateTeam(textmons);
	return q;
}
function textifyMon(duskgullmon){
	let tmon = {}
	tmon.name = duskgullmon.name;
	tmon.species = duskgullmon.species.species;
	if(duskgullmon.item==null){
		tmon.item="";
	} else {
		tmon.item = duskgullmon.item.id;
	}
	tmon.ivs = duskgullmon.ivs;
	tmon.ability = duskgullmon.getAbility();
	tmon.moves=[]
	for(let i in duskgullmon.moves){
		tmon.moves.push(duskgullmon.moves[i].movedex.id);
	}
	return tmon;
}

module.exports = validate;